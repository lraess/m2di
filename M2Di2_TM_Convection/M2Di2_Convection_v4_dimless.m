% =========================================================================
% Thermo-mechanical convection

% Copyright (C) 2020 Thibault Duretz

% This file is part of M2Di2.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

function M2Di2_Convection_v4_dimless
addpath('./_M2Di2_convection_functions/')
%% Physics
xmin        = -3;            % Min. x coordinate
xmax        =  3;            % Max. x coordinate
ymin        = -0.0;          % Min. y coordinate
ymax        =  1;            % Max. y coordinate
rad         =  .02;          % Inclusion radius
Tpert       = 0.1;           % Magnitude of perturbation
xpert       = -1;            % x location of perturbation
ypert       = 0.25;          % y location of perturbation
Ra          = 1e4;           % Rayleigh number
%% Switches
print_fig   = 0;             % Write figures to disk
noisy       = 2;             % Gives information to command window
SuiteSparse = 0;             % Turn on SuiteSparse if you have it installed!
%% Numerics
nx          = 100;           % Number of cells in x
ny          = 50;            % Number of cells in y
nt          = 100000;        % Number of time steps
nout        = 10;            % Visualise results after every nout steps
% Mechanics solver 
gamma       = 1e2;           % Numerical compressibility                                                            % Max number of linear iterations
nPH         = 10;            % Max. number of Powell-Hestenes iterations
tol_glob    = 1e-6;          % Global nonlinear tolerance
tol_linu    = tol_glob/5000; % Velocity tolerance
ps          = 1/3;           % remove on third of divergence to compute deviators
% Advection
order       = 1;             % Order in time of computation 1 or 2
WENO5       = 1;             % WENO5 if 1 or upwind if 0
dimsplit    = 1;             % Split advection per dimension
%% Preprocessing
tic
dx = (xmax-xmin)/nx;                                                                   % cell size in x
dy = (ymax-ymin)/ny;                                                                   % cell size in y
xv = xmin:dx:xmax;           yv = ymin:dy:ymax;           [xv2, yv2] = ndgrid(xv, yv); % cell coord. grid
xc = xmin+dx/2:dx:xmax-dx/2; yc = ymin+dy/2:dy:ymax-dy/2; [xc2, yc2] = ndgrid(xc, yc); % cell coord. grid
[xvx2,yvx2] = ndgrid(xv,yc);
[xvy2,yvy2] = ndgrid(xc,yv);
%% Initial conditions
Vx     =   0.*xvx2;                                                         % Initial solutions for velocity in x
Vy     =  -0.*yvy2;                                                         % Initial solutions for velocity in y
Pt     =   0.* xc2;
Tc     = ones(nx,ny);
Tc(sqrt((xc2-xpert).^2 + (yc2-ypert).^2) < rad )  =  Tc(sqrt((xc2-xpert).^2 + (yc2-ypert).^2) < rad ) + Tpert.*Tc(sqrt((xc2-xpert).^2 + (yc2-ypert).^2) < rad );
rhoc   = ones(nx  ,ny  );                  % viscosity at cell center
muc    = ones(nx  ,ny  );                  % viscosity at cell center
muv    = ones(nx+1,ny+1);                  % viscosity at vertex
kx     = ones(size(Vx));
ky     = ones(size(Vy));
dT     = 1;                                % top/bottom temperature difference
Ttop   = 0;                                % T at top (Dirichlet)
Tbot   = Ttop + dT;                        % T at bottom (Dirichlet)
%% Numbering Pt and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));    % G stands for Gobal numbering
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));   % G stands for Gobal numbering
cpu(1)=toc;
%% Boundary values - Dirichlets
Vx_W   =  xmin*0.*ones(1 ,ny  )';                           % BC value Vx West
Vx_E   =  xmax*0.*ones(1 ,ny  )';                           % BC value Vx East
Vy_S   = -ymin*0.*ones(nx,1   );                            % BC value Vy South
Vy_N   = -ymax*0.*ones(nx,1   );                            % BC value Vy South
Vx_S   = yv(1)*ones(nx+1,1);  Vx_N =  yv(end)*ones(nx+1,1);
Vy_W   =     zeros(1,ny+1)';  Vy_E =     zeros(1,ny+1)';
%% Define BC's  
% Free slip / no slip setting for x momentum
BC.nsxS    =  zeros(size(Vx)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Vx)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Vx)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Vx)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Vx)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Vx)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Vx)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Vx)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
BC.NeuW    =  zeros(size(Vx));
BC.NeuE    =  zeros(size(Vx));
BC.Dirx    =  zeros(size(Vx)); BC.Dirx([1 end], :) = 1;
% Free slip / no slip setting for y momentum
BC.nsyW    =  zeros(size(Vy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Vy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Vy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Vy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Vy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Vy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Vy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Vy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
BC.NeuS    =  zeros(size(Vy));
BC.NeuN    =  zeros(size(Vy));
BC.Diry    =  zeros(size(Vy)); BC.Diry( :,[1 end]) = 1;
% Temperature BC
BC.PfNeuW  =  zeros(size(Tc)); BC.PfNeuW(  1,:) = 1; BC.qxBCW =  zeros(size(Tc));
BC.PfNeuE  =  zeros(size(Tc)); BC.PfNeuE(end,:) = 1; BC.qxBCE =  zeros(size(Tc));
BC.PfNeuS  =  zeros(size(Tc)); BC.PfNeuS(:,  1) = 0; BC.qyBCS =  zeros(size(Tc));
BC.PfNeuN  =  zeros(size(Tc)); BC.PfNeuN(:,end) = 0; BC.qyBCN =  zeros(size(Tc));
BC.PfDirW  =  zeros(size(Tc)); BC.PfDirW(  1,:) = 0; BC.PfBCW =  0*ones(size(Tc));
BC.PfDirE  =  zeros(size(Tc)); BC.PfDirE(end,:) = 0; BC.PfBCE =  0*ones(size(Tc));
BC.PfDirS  =  zeros(size(Tc)); BC.PfDirS(:,  1) = 1; BC.PfBCS =  Tbot*ones(size(Tc));
BC.PfDirN  =  zeros(size(Tc)); BC.PfDirN(:,end) = 1; BC.PfBCN =  Ttop*ones(size(Tc));
%% Construct BC structure  ---------------- NEW STUFF
BC.Ux_W = Vx_W; BC.Ux_E = Vx_E; BC.Ux_S = Vx_S; BC.Ux_N = Vx_N;
BC.Uy_S = Vy_S; BC.Uy_N = Vy_N; BC.Uy_W = Vy_W; BC.Uy_E = Vy_E;
%% Assemble pressure gradient and divergence operator
tic
[ grad, div, ~ ] = M2Di2_AssembleDivGrad( BC, dx, dy, nx, ny, NumVx, NumVyG, NumPt, SuiteSparse );
cpu(3)=toc;
%% PP block
iPt    = NumPt;  I = iPt(:)';  J = I;                                      % Eq. index center (pressure diagonal)
V      = ones(nx*ny,1)./gamma;                                             % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V); end                                % Matrix assembly
if SuiteSparse==0, PP =  sparse(I,J,V); end
PPI   = spdiags(1./diag(PP),0,PP);                                         % Inverse of pseudo compressible block 
%% Time loop
for it=1:nt
    fprintf('Time step: %05d ---  Ra = %2.2e\n', it, Ra);
    % Store old values 
    Tc0    = Tc;
    Tv     = M2Di2_centroids2vertices( Tc );
    Tvy    = 0.5*(Tv(1:end-1,:)+Tv(2:end-0,:));
    % Initialise mechanical solutions
    Vx     =   0.*xvx2;                                                         % Initial solutions for velocity in x
    Vy     =  -0.*yvy2;                                                         % Initial solutions for velocity in y
    Pt     =   0.* xc2;
    %% Rheological coefficients 
    D.D11c =  2.0*muc; D.D12c =  0.0*muc; D.D13c =  0.0*muc; D.D14c =  0.0*muc;   % Centroids - pressure and normal points
    D.D21c =  0.0*muc; D.D22c =  2.0*muc; D.D23c =  0.0*muc; D.D24c =  0.0*muc;
    D.D31v =  0.0*muv; D.D32v =  0.0*muv; D.D33v =  muv;     D.D34v =  0.0*muv;   % Vertices - nodal points - shear stress points
    %% Assemble Picard operator 
    % Residual
    divV        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
    Exxc        = diff(Vx,1,1)/dx - ps*divV;
    Eyyc        = diff(Vy,1,2)/dy - ps*divV;
    Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
    Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
    dVxdy       = diff(Vx_exp,1,2)/dy;
    dVydx       = diff(Vy_exp,1,1)/dx;
    Exyv        = 0.5*( dVxdy + dVydx );
    % Momentum residual
    tau_xx      = D.D11c.*Exxc;
    tau_yy      = D.D22c.*Eyyc;
    tau_xy      = D.D33v.*2.*Exyv;
    Res_x       = zeros(size(Vx)); Res_y       = zeros(size(Vy));
    Res_x(2:end-1, :)  = diff(-Pt + tau_xx,1,1)/dx + diff(tau_xy(2:end-1,:),1,2)/dy;
    Res_y(:, 2:end-1)  = diff(-Pt + tau_yy,1,2)/dy + diff(tau_xy(:,2:end-1),1,1)/dx + Ra.*(Tvy(:, 2:end-1) - 0*1);
    % Assembly
    if it == 1, [ K ] = M2Di2_GeneralAnisotropicVVAssembly_v3( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, 1, Vx, Vy, ps, SuiteSparse ); end
    cpu(4)=toc; display(['Time Build Blocks = ', num2str(cpu(4))])
    % Linear solver
    fu       = [Res_x(:); Res_y(:)];  fp  = divV(:);
    du       = 0*fu; dp  = 0*fp;
    if it == 1 % Matrix factorization - do it only once since coefficients do not change
        Kt       = K - grad*(PPI*div);                                          % PPI*DivV = PP\DivV
        [Kc,e,s] = chol(Kt,'lower','vector');                                   % Cholesky factorization
        cpu(8)=toc; display(['Time CHOLESKY     = ', num2str(cpu(8))]);
    end
    tol_lin = tol_linu;
    % Powell-Hestenes iterations
    for iPH=1:nPH
        Rhs =      fu  - grad*(PPI*fp + dp);                                % Powell-Hestenes
        if SuiteSparse==1, du(s) = cs_ltsolve(Kc,cs_lsolve(Kc,Rhs(s))); end % Powell-Hestenes
        if SuiteSparse==0, du(s) = Kc'\(Kc\Rhs(s));                     end % Powell-Hestenes Matlab
        dp   = dp + PPI*(fp - div*du);
        ru   = fu -   K*du - grad*dp;
        rp   = fp - div*du;
        if noisy>=1, fprintf('  --- iteration %d --- \n',iPH);
            fprintf('   Res. |ru| = %2.2e \n',norm(ru)/length(du));
            fprintf('   Res. |rp| = %2.2e \n',norm(rp)/length(dp)); end
        if norm(rp)/length(dp) < tol_lin, break, end
    end%it
    up  = [ du ; dp ];
    %% Post-process
    Pt  = Pt + reshape(up(NumPtG(:)),[nx  ,ny  ]); 
    Vx  = Vx + reshape(up(NumVx(:)) ,[nx+1,ny  ]);
    Vy  = Vy + reshape(up(NumVyG(:)),[nx  ,ny+1]);
    %% Diffusion-advection
    dt    = 0.5* min(dx,dy) / max(abs(Vy(:)))/1;
    %% Diffusion
    % Matrix Assemble: K_th
    [K_th]  = M2Di2_DiffusionAssembly_v2( ones(size(Vx)), ones(size(Vy)), ones(size(rhoc)), ones(size(rhoc)), NumPt, BC, dx, dy, dt, nx, ny, Tc, SuiteSparse );
    % Matrix factorization 
    [K_th_fact,e_th,s_th] = chol(K_th,'lower','vector');
    % Compute residual: Ft
    dT    = 0*Tc(:);
    Tx    = [       Tc(1,:); Tc;        Tc(end,:)]; % no flux
    Ty    = [2*Tbot-Tc(:,1)  Tc  2*Ttop-Tc(:,end)]; % dirichlet
    qx    = -kx.*diff(Tx,1,1)/dx;
    qy    = -ky.*diff(Ty,1,2)/dy;
    divqt = diff(qx,1,1)/dx + diff(qy,1,2)/dy;
    Ft    = (Tc-Tc0)/dt + divqt;
    Ft    = Ft(:);
    % Solve for correction: dT = -K_th^(-1) * Ft
    if SuiteSparse==1, dT(s_th) = -cs_ltsolve(K_th_fact,cs_lsolve(K_th_fact,Ft(s_th))); end
 	if SuiteSparse==0, dT(s_th) = -K_th_fact'\(K_th_fact\Ft(s_th)); end
    Tc    = Tc + reshape(dT,nx,ny);
    % Advection
    [Tc] = M2Di2_Advection_for_TM_Convection(Tc,Tc0,Vx,Vy,WENO5,order,dimsplit,dt,dx,dy,Tbot,Ttop);
    %% Visualisation
    if mod(it, nout) == 0
        % Viz
        figure(1),clf,colormap('jet'),set(gcf,'Color','white')
        imagesc(xc,yc,Tc'   ), axis image, colorbar, title('$T$', 'interpreter', 'latex' ), xlabel('$x$', 'interpreter', 'latex' ), ylabel('$y$', 'interpreter', 'latex' ), set(gca,'ydir','normal')
        colormap('hot')
        drawnow
        name = ['Convection_step' num2str(it, '%05d')];
        if print_fig==1, print(name, '-dpng', '-r300'); end
    end
end % time loop
end