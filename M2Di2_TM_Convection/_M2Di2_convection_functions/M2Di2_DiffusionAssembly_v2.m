function [KPf] = M2Di2_DiffusionAssembly_v2( kx, ky, rho, cp, NumPf, BC, dx, dy, dt, nx, ny, Pf, SuiteSparse )
%% Block PF (Pressure Lower) with variable coeff
k11_W = kx(1:end-1,:);
k11_E = kx(2:end-0,:);
k22_S = ky(:,1:end-1);
k22_N = ky(:,2:end-0);
iPf   = NumPf;
iPfW  = ones(size(Pf)); iPfW(2:end  ,:) = NumPf(1:end-1, :     );
iPfE  = ones(size(Pf)); iPfE(1:end-1,:) = NumPf(2:end  , :     );
iPfS  = ones(size(Pf)); iPfS(:,2:end  ) = NumPf( :     ,1:end-1);
iPfN  = ones(size(Pf)); iPfN(:,1:end-1) = NumPf( :     ,2:end  );
NeuW  = BC.PfNeuW;
NeuE  = BC.PfNeuE; 
NeuS  = BC.PfNeuS;
NeuN  = BC.PfNeuN;
DirW  = BC.PfDirW; 
DirE  = BC.PfDirE; 
DirS  = BC.PfDirS;
DirN  = BC.PfDirN;
cPfC = cp.*rho./dt + (k22_N.*(-DirN - 1).*(NeuN - 1)./dy - k22_S.*(DirS + 1).*(NeuS - 1)./dy)./dy + (k11_E.*(-DirE - 1).*(NeuE - 1)./dx - k11_W.*(DirW + 1).*(NeuW - 1)./dx)./dx;
cPfE = k11_E.*(1 - DirE).*(NeuE - 1)./dx.^2;
cPfW = -k11_W.*(DirW - 1).*(NeuW - 1)./dx.^2;
cPfS = -k22_S.*(DirS - 1).*(NeuS - 1)./dy.^2;
cPfN = k22_N.*(1 - DirN).*(NeuN - 1)./dy.^2;
I     = [  iPf(:);   iPf(:);  iPf(:);  iPf(:);  iPf(:); ]';
J     = [  iPf(:);  iPfW(:); iPfS(:); iPfE(:); iPfN(:); ]';
V     = [ cPfC(:);  cPfW(:); cPfS(:); cPfE(:); cPfN(:); ]';
if SuiteSparse==1, KPf = sparse2(I,J,V, nx*ny,nx*ny);
else,              KPf =  sparse(I,J,V, nx*ny,nx*ny); end
end