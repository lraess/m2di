function [phi1] = M2Di2_Advection_for_TM_Convection(phi1,phi0,Vx,Vy,WENO5,order,dimsplit,dt,dx,dy,Tbot,Ttop)
vxm1  = min(Vx(1:end-1,:),0);
vxp1  = max(Vx(2:end  ,:),0);
vym1  = min(Vy(:,1:end-1),0);
vyp1  = max(Vy(:,2:end  ),0);
phi1o = phi0;
phi1t = phi1;
for step=1:order
    if WENO5 == 0 % upwind
        phie  = [1*phi1t(1,:); phi1t; 1*phi1t(end,:)]; % zero flux
        phie  = [2*Tbot-1*phie(:,1)  phie  2*Tbot-phie(:,end)];
        phxm1 = 1/dx*(phie(2:end-1,2:end-1) - phie(1:end-2,2:end-1));
        phxp1 = 1/dx*(phie(3:end,2:end-1)   - phie(2:end-1,2:end-1));
    else
        % WENO5
        [phxm1,phxp1,c,d] = M2Di2_WENO5_v3(phi1t, dx, dy , 1, 0, 5, Tbot, Ttop);
    end
    phi1t = phi1t - dimsplit*dt*(vxp1.*phxm1 + vxm1.*phxp1);
end
phi1t = (1/order)*phi1t + (1-1/order)*phi1o;
phi1o = phi1t;
for step=1:order
    if WENO5 == 0 % upwind
        phie  = [1*phi1t(1,:); phi1t; 1*phi1t(end,  :)]; % no flux
        phie  = [2*Tbot-phie(:  ,1)  phie  2*Ttop-phie(:,end)];
        phym1 = 1/dy*(phie(2:end-1,2:end-1) - phie(2:end-1,1:end-2));
        phyp1 = 1/dy*(phie(2:end-1,3:end)   - phie(2:end-1,2:end-1));
    else
        % WENO5
        [a,b,phym1,phyp1] = M2Di2_WENO5_v3(phi1t, dx, dy , 0, 1, 5, Tbot, Ttop);
    end
    phi1t = phi1t - dt*(vyp1.*phym1 + vym1.*phyp1) - (1-dimsplit)*dt*(vxp1.*phxm1 + vxm1.*phxp1);
end
phi1 = (1/order)*phi1t + (1-1/order)*phi1o;
end