% =========================================================================
% Nonlinear Thermo-mechanical power-law Stokes flow with analytical Newton
% solver in pure shear.

% Copyright (C) 2019  Ludovic Raess, Thibault Duretz, Yury Podladchikov

% This file is part of M2Di. Please refer to:
% Duretz, T., Räss, L., Podladchikov, Y. Y., and Schmalholz, S. M. (2019). 
% Resolving thermomechanical coupling in two and three dimensions: spontaneous 
% strain localisation owing to strain heating.
% Geophysical Journal International, ggy434, https://doi.org/10.1093/gji/ggy434.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

function TM2Di2_Newton_GJI
addpath('./_M2Di2_functions/')
%% Switches
SuiteSparse = 0;                                                             % 0=NO SuiteSparse  1=SuiteSparse
noisy       = 1;                                                             % 0=NO Output       1=Std output     2=Very noisy  3=Very noisy + kspgcr output
Newton      = 1;                                                             % 0=Picard          1=Newton
mat_pert    = 0;                                                             % Thermal or material perturbation
Crank       = 0;
if Crank == 1, ck = 0.5; else ck = 0;  end
%% Physics
npow   = 3;                  % stress exponent for power law rheology
Vbc    = -66.4437;           % boundary velocity difference
L      = 0.86038;            % domain size x
T0     = 49.3269/npow;       % initial temperature
r      = 0.0737;             % radius of initial perturbation
Tamp   = 0.1*49.3269/npow;   % amplitude of initial perturbation
%% Domain
xmin        =-0;                                                             % min x
xmax        = L;                                                             % max x
ymin        =-0;                                                             % min y
ymax        = L;                                                             % max y
%% Numerics
nx          = 94;                                                            % Number of cells in x
ny          = nx;                                                            % Number of cells in y
nt          = 294;                                                           % Number of timesteps
Tred        = 0.5;                                                           % Timestep reduction factor  
time        = 0;
% Non-linear solver settings
nmax_glob   = 294;                                                           % Max. number of non-linear iterations
tol_glob    = 1e-10;                                                         % Global nonlinear tolerance
eps_kspgcr  = 5e-4;                                                          % KSPGCR solver tolerance
LineSearch  = 1;                                                             % Line search algorithm
alpha       = 1.0;                                                           % Default step (in case LineSearch = 0)
% Linear solver settings
lsolver     = -2;                                                            % 0=Backslash 1=Powell-Hestenes
gamma       = 1e-4;                                                          % Numerical compressibility
nPH         = 100;                                                           % Max. number of linear iterations
tol_linu    = tol_glob/5000;                                                 % Velocity tolerance
tol_linp    = tol_glob/1000;                                                 % Divergence tolerance
%% Preprocessing
tic
Lx = xmax-xmin;     dx = Lx/nx;                                              % Grid spacing x
Ly = ymax-ymin;     dy = Ly/ny;                                              % Grid spacing y
xn = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;                             % Nodal coordinates x
yn = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;                             % Nodal coordinates y
[xn2,  yn2] = ndgrid(xn,yn);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xn,yc);
[xvy2,yvy2] = ndgrid(xc,yn);
rxvec  = zeros(nmax_glob,1);
rzvec  = zeros(nmax_glob,1); cpu = zeros(9,1);
Wv        = zeros(nt,1); 
Ev        = zeros(nt,1); 
tv        = zeros(nt,1); 
mT        = zeros(nt,1);
mE0       = zeros(nt,1);
mE1       = zeros(nt,1);
mE2       = zeros(nt,1);
E         = 0;
W         = 0;
%% Initial arrays
Vx     = -Vbc/Lx.*xvx2;                                                      % Initial solutions for velocity in x
Vy     =  Vbc/Lx.*yvy2;                                                      % Initial solutions for velocity in y
Pt     =    0.* xc2;                                                         % Initial solutions for pressure
Tc     =    0.* xc2;                                                         % Initial solutions for temperature
phc    =     ones(nx  ,ny  );                                                % Material phase on centroids
phv    =     ones(nx+1,ny+1);                                                % Material phase on vertices
kt     = ones(size(xc2));
N1     = 1; N2 = 1;
if mat_pert==0, Tc((xc2.^2+yc2.^2)<r^2) = Tamp;                              % Thermal perturbation
else            phc((xc2+0*Lx/2).^2+(yc2-0*Ly/2).^2<r^2) = 2;                % Material perturbation
                phv((xn2+0*Lx/2).^2+(yn2-0*Ly/2).^2<r^2) = 2;                % Material perturbation
end
%% Numbering Pt and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));
NumTe  = reshape(1:nx*ny    ,nx  ,ny  ); NumTeG = NumTe + max(NumPtG(:));
cpu(1)=toc;
%% Define BC's ---------------- NEW STUFF
% Free slip / no slip setting for x momentum
BC.nsxS    =  zeros(size(Vx)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Vx)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Vx)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Vx)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Vx)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Vx)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Vx)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Vx)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
% Free slip / no slip setting for y momentum
BC.nsyW    =  zeros(size(Vy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Vy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Vy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Vy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Vy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Vy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Vy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Vy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
% Free surface
BC.frxN    =  zeros(size(Vx)); %BC.frxN(2:end-1,end) = 1;
BC.fryN    =  zeros(size(Vy)); %BC.fryN( :     ,end) = 1;
%% Boundary values - Dirichlets
Vx_W   = -Vbc/Lx.*xvx2(1  ,:)'.*ones(1 ,ny  )';                                 % BC value Vx West
Vx_E   = -Vbc/Lx.*xvx2(end,:)'.*ones(1 ,ny  )';                                 % BC value Vx East
Vy_S   =  Vbc/Lx.*yvy2(:,  1) .*ones(nx,1   );                                  % BC value Vy South
Vy_N   =  Vbc/Lx.*yvy2(:,end) .*ones(nx,1   );                                  % BC value Vy North
Vx_S = zeros(nx+1,1);  Vx_N = zeros(nx+1,1);
Vy_W = zeros(1,ny+1)'; Vy_E = zeros(1,ny+1)';
%% Construct BC structure 
BC.Ux_W = Vx_W; BC.Ux_E = Vx_E; BC.Ux_S = Vx_S; BC.Ux_N = Vx_N;
BC.Uy_S = Vy_S; BC.Uy_N = Vy_N; BC.Uy_W = Vy_W; BC.Uy_E = Vy_E;
%% BC on Te
BC.Te_W   = 0*ones(1 ,ny-2)';
BC.Te_E   = 0*ones(1 ,ny-2)';
BC.Te_S   = 0*ones(nx,1   );
BC.Te_N   = 0*ones(nx,1   );
cpu(2)=toc; tic;
%% Assemble pressure gradient and divergence operator
[ grad, div, BcD ] = M2Di2_AssembleDivGrad( BC, dx, dy, nx, ny,NumVx, NumVyG, NumPt, SuiteSparse );
%% Assemble Block PP
iPt   = NumPt;  I = iPt(:)';  J = I;                                     % Eq. index center (pressure diagonal)
V     = gamma*ones(size(Pt(:)));                                         % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V);                                  % Matrix assembly
else               PP = sparse (I,J,V); end
PPI   = spdiags(1./diag(PP),0,PP);                                       % Trivial inverse of diagonal PP matrix
cpu(3)=toc;
%% -------------------------- Physical timesteps
for it = 1:nt 
    fprintf('\n------------- Time step #%03d -------------\n',it);
    Toc = Tc;
    % Reset to solutions homogeneous pure shear 
    Vx     = -Vbc/Lx.*xvx2;                                                         % Initial solutions for velocity in x
    Vy     =  Vbc/Lx.*yvy2;                                                         % Initial solutions for velocity in y
    Pt     =    0.* xc2;
    %% Non linear iterations
    iter=0; resnlu=2*tol_glob; resnlu0=2*tol_glob; resnlT0=2*tol_glob;
    % Save previous solutions
    Vx0=Vx; Vy0=Vy; Pt0=Pt; Tc0=Tc; restart=1; failed=0;
    while( resnlu/resnlu0 > tol_glob && iter < nmax_glob )
        iter = iter+1; tic; if noisy>=1, fprintf('\n *** Nonlin iter %d *** \n', iter ); end        
        % Reload old solutions if restart - Timestep selection
        if (iter==1 && restart==1)
            Vx=Vx0; Vy=Vy0; Pt=Pt0; Tc=Tc0;
            dt   = Tred*dx*dx*(N1)/4.1/(1 + failed)/1;
            time = time + dt;
            fprintf('--> Selected dt = %2.2e\n', dt);
        end
        % Initial guess or iterative solution for strain increments
        divV        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
        Exxc        = diff(Vx,1,1)/dx - 1/3*divV;
        Eyyc        = diff(Vy,1,2)/dy - 1/3*divV;
        Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
        Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
        dVxdy       = diff(Vx_exp,1,2)/dy;
        dVydx       = diff(Vy_exp,1,1)/dx;
        Exyv        = 0.5*( dVxdy + dVydx );
        % Extrapolate trial strain components
        Exyc     = 0.25*(Exyv(1:end-1,1:end-1) + Exyv(2:end,1:end-1) + Exyv(1:end-1,2:end) + Exyv(2:end,2:end));
        [ Exxv ] = M2Di2_centroids2vertices( Exxc );
        [ Eyyv ] = M2Di2_centroids2vertices( Eyyc );
        [ Tv   ] = M2Di2_centroids2vertices( Tc   );
        % Engineering convention
        Gxyc = 2*Exyc;
        Gxyv = 2*Exyv;
        % Invariants
        Eiic2    = 1/2*(Exxc.^2 + Eyyc.^2 + 1/2*Gxyc.^2);
        Eiiv2    = 1/2*(Exxv.^2 + Eyyv.^2 + 1/2*Gxyv.^2);
        % Viscosity
        mc   = 1/2*( 1./npow - 1);
        mv   = 1/2*( 1./npow - 1);
        Bc   = exp( -Tc.*(1./(1 + Tc./T0)) );
        Bv   = exp( -Tv.*(1./(1 + Tv./T0)) );
        etac = Bc.*Eiic2.^mc;
        etav = Bv.*Eiiv2.^mv;
        %% Rheological coefficients for the Picard operator
        % Centroids
        Dv.D11c = 2*etac;       Dv.D12c = 0*etac;        Dv.D13c = 0*etac;        Dv.D41c = 0*etac;
        Dv.D21c = 0*etac;       Dv.D22c = 2*etac;        Dv.D23c = 0*etac;        Dv.D42c = 0*etac;
        Dv.D31c = 0*etac;       Dv.D32c = 0*etac;        Dv.D33c = 1*etac;        Dv.D43c = 0*etac;
        Dv.D41c = 2*etac.*Exxc; Dv.D42c = 2*etac.*Eyyc;  Dv.D43c = 1*etac.*Gxyc;  Dv.D44c = 0*etac;
        % Vertices
        Dv.D11v = 2*etav;       Dv.D12v = 0*etav;        Dv.D13v = 0*etav;        Dv.D14v = 0*etav;
        Dv.D21v = 0*etav;       Dv.D22v = 2*etav;        Dv.D23v = 0*etav;        Dv.D24v = 0*etav;
        Dv.D31v = 0*etav;       Dv.D32v = 0*etav;        Dv.D33v = 1*etav;        Dv.D34v = 0*etav;
        Dv.D41v = 2*etav.*Exxv; Dv.D42v = 2*etav.*Eyyv;  Dv.D43v = 1*etav.*Gxyv;  Dv.D44v = 0*etav;
        %% Assemble mechanical block (Picard mechanical operator): KM operator 
        [ KM, BcK ] = M2Di2_GeneralAnisotropicVVAssembly( Dv, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, 1, Vx, Vy, SuiteSparse );
        %% Assemble thermal block
        [ TM, BcT ] = M2Di2_AssembleDiffusionOperator( BC, dt, dx, dy, ck, nx, ny, kt, NumTe, Tc, Toc, N1, N2, SuiteSparse);
        %% Assemble thermo-mechanical coupling block (shear heating): T2M block
        T2M         = M2Di2_T2MAssembly( BC, Dv, dx, dy, nx, ny, ck, N2, NumVx, NumVyG, NumTe, Tc, SuiteSparse );
        if Newton == 1
            %% Derivative of viscosity versus solution fields
            detadexxc =     Bc.*Eiic2.^(mc-1).*mc.*Exxc;          detadexxv =     Bv.*Eiiv2.^(mv-1).*mv.*Exxv;
            detadeyyc =     Bc.*Eiic2.^(mc-1).*mc.*Eyyc;          detadeyyv =     Bv.*Eiiv2.^(mv-1).*mv.*Eyyv;
            detadgxyc = 1/2*Bc.*Eiic2.^(mc-1).*mc.*Gxyc;          detadgxyv = 1/2*Bv.*Eiiv2.^(mv-1).*mv.*Gxyv;
            detadTc   = -(Bc .* Eiic2.^(mc)) ./(1 + Tc./T0).^2;     detadTv   = -(Bv .* Eiiv2.^(mv)) ./(1 + Tv./T0).^2;
            %% Rheological coefficients for the Jacobian
            % Centroids
            Dv.D11c = 2*etac +   2*detadexxc.*Exxc;       Dv.D12c =            2*detadeyyc.*Exxc;       Dv.D13c =            2*detadgxyc.*Exxc;       Dv.D14c = 2*detadTc.*Exxc;
            Dv.D21c =            2*detadexxc.*Eyyc;       Dv.D22c = 2*etac +   2*detadeyyc.*Eyyc;       Dv.D23c =            2*detadgxyc.*Eyyc;       Dv.D24c = 2*detadTc.*Eyyc;
            Dv.D31c =            1*detadexxc.*Gxyc;       Dv.D32c =            1*detadeyyc.*Gxyc;       Dv.D33c = 1*etac +   1*detadgxyc.*Gxyc;       Dv.D34v = 1*detadTc.*Gxyc;
            Dv.D41c = 4*etac.*Exxc + 4*detadexxc.*Eiic2;  Dv.D42c = 4*etac.*Eyyc + 4*detadeyyc.*Eiic2;  Dv.D43c = 2*etac.*Gxyc + 4*detadgxyc.*Eiic2;  Dv.D44c = 4*detadTc.*Eiic2;
            % Vertices
            Dv.D11v = 2*etav +   2*detadexxv.*Exxv;       Dv.D12v =            2*detadeyyv.*Exxv;       Dv.D13v =            2*detadgxyv.*Exxv;       Dv.D14v = 2*detadTv.*Exxv;
            Dv.D21v =            2*detadexxv.*Eyyv;       Dv.D22v = 2*etav +   2*detadeyyv.*Eyyv;       Dv.D23v =            2*detadgxyv.*Eyyv;       Dv.D24v = 2*detadTv.*Eyyv;
            Dv.D31v =            1*detadexxv.*Gxyv;       Dv.D32v =            1*detadeyyv.*Gxyv;       Dv.D33v = 1*etav +   1*detadgxyv.*Gxyv;       Dv.D34v = 1*detadTv.*Gxyv;
            Dv.D41v = 4*etav.*Exxv + 4*detadexxv.*Eiiv2;  Dv.D42v = 4*etav.*Eyyv + 4*detadeyyv.*Eiiv2;  Dv.D43v = 2*etav.*Gxyv + 4*detadgxyv.*Eiiv2;  Dv.D44v = 4*detadTv.*Eiiv2;
            %% Jacobian for the mechanical block: JM operator
            [ JM,  ~  ] = M2Di2_GeneralAnisotropicVVAssembly( Dv, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, 1, Vx, Vy, SuiteSparse );
            %% Jacobian of thermo-mechanical coupling: M2TJ operator
            T2MJ        = M2Di2_T2MAssembly( BC, Dv, dx, dy, nx, ny, ck, N2, NumVx, NumVyG, NumTe, Tc, SuiteSparse );
            %% Jacobian of thermo-mechanical coupling: T2MJ operator
            M2TJ        = M2Di2_M2TAssembly( BC, Dv, dx, dy, nx, ny, ck, N2, NumVx, NumVyG, NumTe, Vx, Vy, SuiteSparse );
            %% Jacobian of thermal block: TJ operator
            cTC = -(1-ck)*N2.*Dv.D44c;
            Itj = NumTe(:);
            Jtj = NumTe(:);
            Vtj = cTC(:);
            TMJ = sparse2( Itj, Jtj, Vtj );
        else
            JM   = KM;
            TMJ  = 0*TM;
            M2TJ = 0*grad;
            T2MJ = 0*div;
        end
        %% Evaluate non-Linear residual using matrix-vector products
        bu = BcK; bp = BcD; bt = BcT; u = [Vx(:); Vy(:)]; p = Pt(:); T = Tc(:);          % Assemble velocity/pressure vectors
        fu = -(  KM*u + grad*p - bu );  resnlu = norm(fu)/length(fu); % Velocity non-linear residuals
        fp = -( div*u          - bp );  resnlp = norm(fp)/length(fp); % Pressure non-linear residuals
        ft = -( T2M*u +   TM*T - bt );  resnlT = norm(ft)/length(ft); % Pressure non-linear residuals
        if iter==1, resnlu0=resnlu; resnlp0=resnlp; resnlT0=resnlT; end
        if noisy>=1,fprintf('Chk: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', resnlu, resnlu/resnlu0 ); rxvec(iter) = resnlu/resnlu0;
                    fprintf('Chk: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', resnlp, resnlp/resnlp0 );
                    fprintf('Chk: NonLin res. ||res.t||=%2.4e, ||res.t||/||res.t0||=%2.4e\n', resnlT, resnlT/resnlT0 ); rzvec(iter) = resnlT/resnlT0; end
        if (resnlu/resnlu0 < tol_glob), break; end
        cpu(4)=cpu(4)+toc; tic
        %% Linear solver - obtain velocity/pressure/temperature corrections for current non-linear iteration
        [du,dp,dT,fu1,fp1,ft1, failed,restart ] = M2Di2_LinearSolvers(lsolver,Newton,tol_linu,tol_linp,eps_kspgcr,nPH,noisy,SuiteSparse, BcK,BcD,BcT, KM,grad,div,PP,PPI,T2M,TM, JM, M2TJ,T2MJ,TMJ, fu,fp,ft, failed, NumVyG,NumPtG,NumTeG);
        if noisy>=1, fprintf('Linear solver took %2.2e s\n', toc);
                     fprintf(' - Linear res. ||res.u||=%2.2e\n', norm(fu1)/length(fu1) );
                     fprintf(' - Linear res. ||res.p||=%2.2e\n', norm(fp1)/length(fp1) ); 
                     fprintf(' - Linear res. ||res.T||=%2.2e\n', norm(ft1)/length(ft1) ); end
        cpu(6)=cpu(6)+toc; tic
        %% Call line search - globalization procedure
        if LineSearch==1 && lsolver>-1
            [alpha,~] = M2Di2_LineSearch_Direct_M(BC,nx,ny,dx,dy,u,p,du,dp,Tc,Tv,phc,phv,npow,T0,Newton,0,1/3,noisy,0,resnlu0,resnlp0,resnlu,resnlp); 
        elseif LineSearch==1 && lsolver<0
            [alpha,~] = M2Di2_LineSearch_Direct_TM(BC,nx,ny,dx,dy,u,p,T,du,dp,dT,Tc,Tv,phc,phv,npow,T0,Newton,0,1/3,noisy,0,resnlu0,resnlp0,resnlT0,resnlu,resnlp,resnlT,Toc,N1,N2,dt);
        end
        u    = u + alpha*du;                                                     % Velocity update from non-linear iterations
        p    = p + alpha*dp;                                                     % Pressure update from non-linear iterations
        T    = T + alpha*dT;
        % Reshape solutions back to 2D matrices
        Vx   = reshape(u(NumVx(:)) ,[nx+1,ny  ]);                                % Velocity in x (2D array)
        Vy   = reshape(u(NumVyG(:)),[nx  ,ny+1]);                                % Velocity in y (2D array)
        Pt   = reshape(p(NumPt(:)) ,[nx  ,ny  ]);                                % Pressure      (2D array)
        Tc   = reshape(T(NumTe(:)) ,[nx  ,ny  ]); 
        cpu(7)=cpu(7)+toc;     
    end
    %% Post-processing
    tic
    % Fields away from box corners
    Tvis   = T;     Tvis  ((xc2.^2+yc2.^2)<(r+0.5*r)^2) = 0; Tvis  (((xc2-Lx).^2+(yc2-Ly).^2)<(r+0.5*r)^2) = 0;
    Eiivis = Eiic2; Eiivis((xc2.^2+yc2.^2)<(r+0.5*r)^2) = 0; Eiivis(((xc2-Lx).^2+(yc2-Ly).^2)<(r+0.5*r)^2) = 0;
    % Conservation test
    rhoc   = 1;
    Edot   = rhoc*(Tc(:)-Toc(:))/dt;
    E      = E + dx*dy*dt*sum(Edot);
    % Volume integral
%     Wdot   = (4.*Eiic2.*etac);
%     W      = W + dx*dy*dt*sum(Wdot(:));
    %%%
    % Surface integral
    [ Pv ] = M2Di2_centroids2vertices( Pt );
    Sxxv   = -Pv + 2*etav.*Exxv;
    Syyv   = -Pv + 2*etav.*Eyyv;
    SxxVx  = 0.5*(Sxxv(:,2:end) + Sxxv(:,1:end-1)); 
    SyyVy  = 0.5*(Syyv(2:end,:) + Syyv(1:end-1,:));
    west   = SxxVx(1,:).*Vx(1,:)*dy;
    east   = SxxVx(end,:).*Vx(end,:)*dy;
    south  = SyyVy(:,1).*Vy(:,1)*dx;
    north  = SyyVy(:,end).*Vy(:,end)*dx;
    W      = W + dt*( sum(west) + sum(east) + sum(south) + sum(north) );
    Wv(it) = W;
    Ev(it) = E;
    tv(it) = time;
    mT(it) = max( Tvis(:) );
    mE(it) = max( sqrt( Eiivis(:) ) ) / abs(Vbc/Lx);
    FS = 20;
    figure(1),clf,colormap('jet'),set(gcf,'Color','white')
    plot(1:iter, log10(rxvec(1:iter)/rxvec(1)),'b.-',1:iter, log10(rzvec(1:iter)/rzvec(1)),'r.-')
    ylabel('$\| f_{u} \|_{\mathrm{rel}}$, $\| f_{T} \|_{\mathrm{rel}}$', 'interpreter', 'latex', 'FontSize', FS), xlabel(  'Iterations', 'interpreter', 'latex', 'FontSize', FS)
    leg=legend('$\| f_{u} \|_{\mathrm{rel}}$', '$\| f_{T} \|_{\mathrm{rel}}$','Location', 'SouthWest'); legend boxoff, set(leg,'interpreter', 'latex'), set(gca, 'FontSize', FS,'Linewidth',1.6 )
    drawnow
    figure(2),clf,colormap('jet'),set(gcf,'Color','white')
    subplot(211), plot(tv(1:it)*1e3, mE(1:it), 'b.'),axis([0 3 0 16])
    ylabel('$\max$ $\dot{\epsilon}_{II}$ / $\dot{\epsilon}_{BG}$', 'interpreter', 'latex', 'FontSize', FS), set(gca, 'FontSize', FS,'Linewidth',1.6 )
    subplot(212), plot(tv(1:it)*1e3, mT (1:it), 'b.'),axis([0 3 0.02 4])
    ylabel('$\max$ $T$', 'interpreter', 'latex', 'FontSize', FS), xlabel('$t \times 10^{-3}$', 'interpreter', 'latex', 'FontSize', FS), set(gca, 'FontSize', FS,'Linewidth',1.6 )
    drawnow
    figure(3),clf,colormap('jet'),set(gcf,'Color','white')
    hold on, plot(tv(1:it)*1e3, Ev(1:it), 'b.', tv(1:it)*1e3, Wv(1:it), 'dr'), box on
    ylabel('$E, W$', 'interpreter', 'latex', 'FontSize', FS),xlabel('$t \times 10^{-3}$', 'interpreter', 'latex', 'FontSize', FS), axis([0 3 0 1])
    leg=legend('E, DI.', 'W, DI.','Location', 'SouthEast'); legend boxoff, set(leg,'interpreter', 'latex'), set(gca, 'FontSize', FS,'Linewidth',1.6 )
    drawnow
    figure(4),clf,colormap('jet'),set(gcf,'Color','white')
    subplot(121), imagesc(xc,yc,flipud(   Tc')),colorbar,axis image,
    title ('$T$', 'interpreter', 'latex', 'FontSize', FS),xlabel('$x$', 'interpreter', 'latex', 'FontSize', FS),ylabel('$y$', 'interpreter', 'latex', 'FontSize', FS), set(gca, 'FontSize', FS,'Linewidth',1.6 )
    subplot(122), imagesc(xc,yc,flipud(log10(sqrt(Eiic2))')),colorbar,axis image 
    title ('$\dot{\epsilon}_{II}$ / $\dot{\epsilon}_{BG}$', 'interpreter', 'latex', 'FontSize', FS),xlabel('$x$', 'interpreter', 'latex', 'FontSize', 20), set(gca, 'FontSize', FS,'Linewidth',1.6 )
    drawnow 
    cpu(8)=toc; cpu(9)=sum(cpu(1:7));
    display([' Time preprocess   = ', num2str(cpu(1))]);
    display([' Time BC           = ', num2str(cpu(2))]);
    display([' Time cst block    = ', num2str(cpu(3))]);
    display([' Time assemble     = ', num2str(cpu(4))]);
    display([' Time precondition = ', num2str(cpu(5))]);
    display([' Time solve        = ', num2str(cpu(6))]);
    display([' => WALLTIME      == ', num2str(cpu(9))]);
end
end