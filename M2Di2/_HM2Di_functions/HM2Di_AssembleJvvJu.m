function [ KJd, KJu ] = HM2Di_AssembleJvvJu( Newton, SuiteSparse, CN, divV, divVo, phio, dx, dy, dt, phi, rhofg, rhosg, NumVx, NumVyG  )
% AssembleJvvJu

nx       = size(phi,1);
ny       = size(phi,2);
dRogdphi = (rhofg - rhosg)*ones(nx,ny+1); dRogdphi(:,[1 end]) = 0;
iVyC   = NumVyG;
iVyS   = ones(nx,ny+1); iVyS(:,2:end-0) = NumVyG(:,1:end-1);
iVyN   = ones(nx,ny+1); iVyN(:,1:end-1) = NumVyG(:,2:end-0);
iVxSW  = ones(nx,ny+1); iVxSW(1:end-0,2:end-1) = NumVx(1:end-1,1:end-1);
iVxSE  = ones(nx,ny+1); iVxSE(1:end-0,2:end-1) = NumVx(2:end-0,1:end-1);
iVxNW  = ones(nx,ny+1); iVxNW(1:end-0,2:end-1) = NumVx(1:end-1,2:end-0);
iVxNE  = ones(nx,ny+1); iVxNE(1:end-0,2:end-1) = NumVx(2:end-0,2:end-0);
%
phioN  = zeros(nx,ny+1); phioN(:,1:end-1)  = phio;
phioS  = zeros(nx,ny+1); phioS(:,2:end-0)  = phio;
divVoN = zeros(nx,ny+1); divVoN(:,1:end-1) = divVo;
divVoS = zeros(nx,ny+1); divVoS(:,2:end-0) = divVo;
divN   = zeros(nx,ny+1); divN(:,1:end-1)   = divV;
divS   = zeros(nx,ny+1); divS(:,2:end-0)   = divV;
%
cVxNE = Newton.*(dRogdphi).*(0.5*dt.*(-CN + 1)./(dx.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1)) - 0.5*dt.*(-CN + 1).*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + phioN)./(dx.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1).^2));
cVxNW = Newton.*(dRogdphi).*(-0.5*dt.*(-CN + 1)./(dx.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1)) + 0.5*dt.*(-CN + 1).*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + phioN)./(dx.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1).^2));
cVyN  = Newton.*(dRogdphi).*(0.5*dt.*(-CN + 1)./(dy.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1)) - 0.5*dt.*(-CN + 1).*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + phioN)./(dy.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1).^2));
cVyC  = Newton.*(dRogdphi).*(0.5*dt.*(-CN + 1)./(dy.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1)) - 0.5*dt.*(-CN + 1).*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + phioS)./(dy.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1).^2) - 0.5*dt.*(-CN + 1)./(dy.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1)) + 0.5*dt.*(-CN + 1).*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + phioN)./(dy.*(CN.*divVoN.*dt + dt.*(-CN + 1).*(divN) + 1).^2));
cVxSE = Newton.*(dRogdphi).*(0.5*dt.*(-CN + 1)./(dx.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1)) - 0.5*dt.*(-CN + 1).*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + phioS)./(dx.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1).^2));
cVxSW = Newton.*(dRogdphi).*(-0.5*dt.*(-CN + 1)./(dx.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1)) + 0.5*dt.*(-CN + 1).*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + phioS)./(dx.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1).^2));
cVyS  = Newton.*(dRogdphi).*(-0.5*dt.*(-CN + 1)./(dy.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1)) + 0.5*dt.*(-CN + 1).*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + phioS)./(dy.*(CN.*divVoS.*dt + dt.*(-CN + 1).*(divS) + 1).^2));
IvvJ  = [  iVyC(:);  iVyC(:);  iVyC(:);  iVyC(:);  iVyC(:);  iVyC(:) ]';
JvvJ  = [ iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:);  iVyS(:);  iVyN(:) ]';
VvvJ  = [ cVxSW(:); cVxSE(:); cVxNW(:); cVxNE(:);  cVyS(:);  cVyN(:) ];
% Assemble unsymmetric contributions
if SuiteSparse==1, KJu = sparse2( [IvvJ(:)], [JvvJ(:)], [VvvJ(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
else               KJu =  sparse( [IvvJ(:)], [JvvJ(:)], [VvvJ(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx ); end
IvvJ  = [  iVyC(:); ]';
JvvJ  = [  iVyC(:); ]';
VvvJ  = [  cVyC(:); ];
% Assemble diagonal contribution
if SuiteSparse==1, KJd = sparse2( [IvvJ(:)], [JvvJ(:)], [VvvJ(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
else               KJd =  sparse( [IvvJ(:)], [JvvJ(:)], [VvvJ(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx ); end
end

