function [Kpf, KpfJ, BcPf] = HM2Di_AssembleJpf(SuiteSparse, nx, ny, dx, dy, phi, Pf, Pt, kx, ky, kgy, eta_phi, detadPf, NumPf, qDy_S, qDy_N)
% HM2Di_AssembleJpf

%% Block PF (Pressure Lower) with variable coeff
k_W   = kx(1:end-1,:);
k_E   = kx(2:end-0,:);
k_S   = ky(:,1:end-1);
k_N   = ky(:,2:end-0);
iPf   = NumPf;
iPfW  = ones(size(Pf)); iPfW(2:end  ,:) = NumPf(1:end-1, :     );
iPfE  = ones(size(Pf)); iPfE(1:end-1,:) = NumPf(2:end  , :     );
iPfS  = ones(size(Pf)); iPfS(:,2:end  ) = NumPf( :     ,1:end-1);
iPfN  = ones(size(Pf)); iPfN(:,1:end-1) = NumPf( :     ,2:end  );
NeuW  = zeros(nx,ny); NeuW(  1,:) = 1;
NeuE  = zeros(nx,ny); NeuE(end,:) = 1;
NeuS  = zeros(nx,ny); NeuS(:,  1) = 1;
NeuN  = zeros(nx,ny); NeuN(:,end) = 1;
qxBCW = zeros(nx,ny);
qxBCE = zeros(nx,ny);
qyBCS = zeros(nx,ny); qyBCS(:,  1)    = qDy_S;
qyBCN = zeros(nx,ny); qyBCN(:,end)    = qDy_N;
cPfC  = -NeuE.*k_E./dx.^2 - NeuN.*k_N./dy.^2 - NeuS.*k_S./dy.^2 - NeuW.*k_W./dx.^2 + k_N./dy.^2 + k_S./dy.^2 + k_E./dx.^2 + k_W./dx.^2  + 1./(eta_phi.*(1-phi));
cPfW  =  NeuW.*k_W./dx.^2 - k_W./dx.^2;
cPfE  =  NeuE.*k_E./dx.^2 - k_E./dx.^2;
cPfS  =  NeuS.*k_S./dy.^2 - k_S./dy.^2;
cPfN  =  NeuN.*k_N./dy.^2 - k_N./dy.^2;
crhs  = -NeuE.*qxBCE./dx - NeuN.*qyBCN./dy + NeuS.*qyBCS./dy + NeuW.*qxBCW./dx + kgy;
I     = [  iPf(:);   iPf(:);  iPf(:);  iPf(:);  iPf(:); ]';
J     = [  iPf(:);  iPfW(:); iPfS(:); iPfE(:); iPfN(:); ]';
V     = [ cPfC(:);  cPfW(:); cPfS(:); cPfE(:); cPfN(:); ]';
if SuiteSparse==1, Kpf = sparse2(I,J,V, nx*ny,nx*ny);
else,              Kpf =  sparse(I,J,V, nx*ny,nx*ny); end
cPfC  = detadPf.*(-Pf + Pt)./(eta_phi.^2.*(-phi + 1));
I     =  iPf(:)';
J     =  iPf(:)';
V     = cPfC(:)';
if SuiteSparse==1, KpfJ = sparse2(I,J,V, nx*ny,nx*ny);
else,              KpfJ =  sparse(I,J,V, nx*ny,nx*ny); end
%% BCs on Pf
BcPf = zeros(nx*ny,1);
BcPf = BcPf + crhs(:);
end

