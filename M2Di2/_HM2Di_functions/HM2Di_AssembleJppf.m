function [Kppf, KppfJ] = HM2Di_AssembleJppf(SuiteSparse, phi, Pt, Pf, eta_phi, detadPf, NumPt)
% HM2Di_AssembleJppf

iPt   = NumPt;  I = iPt(:);  J = I;
cPtPf = - 1./(eta_phi.*(-phi + 1));
if SuiteSparse==1, Kppf = sparse2(I,J,cPtPf(:));
else,              Kppf =  sparse(I,J,cPtPf(:)); end
cPtPf = -detadPf.*(-Pf + Pt)./(eta_phi.^2.*(-phi + 1));
if SuiteSparse==1, KppfJ = sparse2(I,J,cPtPf(:));
else,              KppfJ =  sparse(I,J,cPtPf(:)); end
end

