function [ VPfJ ] = HM2Di_AssembleJpfv( Newton, SuiteSparse, CN, k_x, k_y, divV, phi_x, phi_y, divVo, phio, dx, dy, dt, Pf, nperm, NumPt, NumVx, NumVyG  )
% AssembleJpfv

iPt   = NumPt(:);
iVxW  = NumVx(1:end-1, :     );
iVxE  = NumVx(2:end  , :     );
iVyS  = NumVyG( :    ,1:end-1);
iVyN  = NumVyG( :    ,2:end  );
iVxWW = ones(size(Pf)); iVxWW(2:end  , :     ) = NumVx(1:end-2   , :     );
iVxEE = ones(size(Pf)); iVxEE(1:end-1, :     ) = NumVx(3:end     , :     );
iVySS = ones(size(Pf)); iVySS( :     ,2:end  ) = NumVyG( :       ,1:end-2);
iVyNN = ones(size(Pf)); iVyNN( :     ,1:end-1) = NumVyG( :       ,3:end  );
iVySW = ones(size(Pf)); iVySW(2:end  , :     ) = NumVyG( 1:end-1 ,1:end-1);
iVyNW = ones(size(Pf)); iVyNW(2:end  , :     ) = NumVyG( 1:end-1 ,2:end  );
iVySE = ones(size(Pf)); iVySE(1:end-1, :     ) = NumVyG( 2:end   ,1:end-1);
iVyNE = ones(size(Pf)); iVyNE(1:end-1, :     ) = NumVyG( 2:end   ,2:end  );
iVxSW = ones(size(Pf)); iVxSW( :     ,2:end  ) = NumVx(1:end-1   ,1:end-1);
iVxSE = ones(size(Pf)); iVxSE( :     ,2:end  ) = NumVx(2:end     ,1:end-1);
iVxNW = ones(size(Pf)); iVxNW( :     ,1:end-1) = NumVx(1:end-1   ,2:end  );
iVxNE = ones(size(Pf)); iVxNE( :     ,1:end-1) = NumVx(2:end     ,2:end  );
%
PC = Pf;
PE = zeros(size(Pf));  PE(1:end-1, :     ) = Pf(2:end  , :     );  PE(end, : ) =  PE(end-1, :   );
PW = zeros(size(Pf));  PW(2:end  , :     ) = Pf(1:end-1, :     );  PW( 1 , : ) =  PW( 2   , :   );
PN = zeros(size(Pf));  PN( :     ,1:end-1) = Pf( :     ,2:end  );  PN( : ,end) =  PN( :   ,end-1);
PS = zeros(size(Pf));  PS( :     ,2:end  ) = Pf( :     ,1:end-1);  PS( : ,1  ) =  PS( :   , 2   );
% Divergence
divVN = zeros(size(Pf)); divVN(:,1:end-1) = divV(:,2:end-0);
divVS = zeros(size(Pf)); divVS(:,2:end-0) = divV(:,1:end-1);
divVE = zeros(size(Pf)); divVE(1:end-1,:) = divV(2:end-0,:);
divVW = zeros(size(Pf)); divVW(2:end-0,:) = divV(1:end-1,:);
% Porosity
phiN   = phi_y(:,2:end-0);
phiS   = phi_y(:,1:end-1);
phiE   = phi_x(2:end-0,:);
phiW   = phi_x(1:end-1,:);
% Effective permeability
kN   = k_y(:,2:end-0); kN(:,end) = 0; % ACHTUNG: there was a problem here
kS   = k_y(:,1:end-1); kS(:,  1) = 0;
kE   = k_x(2:end-0,:); kE(end,:) = 0;
kW   = k_x(1:end-1,:); kW(  1,:) = 0;
% Beauty
phi_oN = zeros(size(Pf)); phi_oN(:,1:end-1) = phio(:,2:end-0);
phi_oS = zeros(size(Pf)); phi_oS(:,2:end-0) = phio(:,1:end-1);
phi_oE = zeros(size(Pf)); phi_oE(1:end-1,:) = phio(2:end-0,:);
phi_oW = zeros(size(Pf)); phi_oW(2:end-0,:) = phio(1:end-1,:);
divVoN = zeros(size(Pf)); divVoN(:,1:end-1) = divVo(:,2:end-0);
divVoS = zeros(size(Pf)); divVoS(:,2:end-0) = divVo(:,1:end-1);
divVoE = zeros(size(Pf)); divVoE(1:end-1,:) = divVo(2:end-0,:);
divVoW = zeros(size(Pf)); divVoW(2:end-0,:) = divVo(1:end-1,:);
dphi_ddiv   = (1-CN)*dt*(1-phio ) ./ ((1-CN)*divV *dt + CN*divVo *dt +1).^2; % ACHTUNG: there was a problem here
dphiN_ddivN = (1-CN)*dt*(1-phi_oN) ./ ((1-CN)*divVN*dt + CN*divVoN*dt +1).^2;
dphiS_ddivS = (1-CN)*dt*(1-phi_oS) ./ ((1-CN)*divVS*dt + CN*divVoS*dt +1).^2;
dphiN_ddivE = (1-CN)*dt*(1-phi_oE) ./ ((1-CN)*divVE*dt + CN*divVoE*dt +1).^2;
dphiS_ddivW = (1-CN)*dt*(1-phi_oW) ./ ((1-CN)*divVW*dt + CN*divVoW*dt +1).^2;
D23S   = nperm.*kS./phiS.*(PC - PS)/dy.*dphiS_ddivS;
D23N   = nperm.*kN./phiN.*(PN - PC)/dy.*dphiN_ddivN;
D13W   = nperm.*kW./phiW.*(PC - PW)/dx.*dphiS_ddivW;
D13E   = nperm.*kE./phiE.*(PE - PC)/dx.*dphiN_ddivE;
D23N_1 = -(PN - PC)/dy.* -dphi_ddiv.*kN.*nperm./(phiN) + (PC - PS)/dy.*kS.*nperm.*(dphiS_ddivS - dphi_ddiv)./phiS;
D23S_2 = -(PC - PS)/dy.* -dphi_ddiv.*kS.*nperm./(phiS) + (PN - PC)/dy.*kN.*nperm.*(dphiN_ddivN - dphi_ddiv)./phiN;
D13E_1 = -(PE - PC)/dx.* -dphi_ddiv.*kE.*nperm./(phiE) + (PC - PW)/dx.*kW.*nperm.*(dphiS_ddivW - dphi_ddiv)./phiW;
D13W_2 = -(PC - PW)/dx.* -dphi_ddiv.*kW.*nperm./(phiW) + (PE - PC)/dx.*kE.*nperm.*(dphiN_ddivE - dphi_ddiv)./phiE;
cVxE  = Newton.*0.5*D13W_2./dx.^2 - 0.5*D23N_1./(dx.*dy) + 0.5*D23S_2./(dx.*dy);
cVxW  = Newton.*0.5*D13E_1./dx.^2 + 0.5*D23N_1./(dx.*dy) - 0.5*D23S_2./(dx.*dy);
cVyN  = Newton.*-0.5*D13E_1./(dx.*dy) + 0.5*D13W_2./(dx.*dy) + 0.5*D23S_2./dy.^2;
cVyS  = Newton.*0.5*D13E_1./(dx.*dy) - 0.5*D13W_2./(dx.*dy) + 0.5*D23N_1./dy.^2;
cVxWW = Newton.*-0.5*D13W./dx.^2;
cVxEE = Newton.*-0.5*D13E./dx.^2;
cVxNW = Newton.*0.5*D23N./(dx.*dy);
cVxSW = Newton.*-0.5*D23S./(dx.*dy);
cVxNE = Newton.*-0.5*D23N./(dx.*dy);
cVxSE = Newton.*0.5*D23S./(dx.*dy);
cVySS = Newton.*-0.5*D23S./dy.^2;
cVyNN = Newton.*-0.5*D23N./dy.^2;
cVyNW = Newton.*0.5*D13W./(dx.*dy);
cVySW = Newton.*-0.5*D13W./(dx.*dy);
cVyNE = Newton.*-0.5*D13E./(dx.*dy);
cVySE = Newton.*0.5*D13E./(dx.*dy);
%
I     = [ iPt(:);  iPt(:);  iPt(:);  iPt(:);  iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   ]';
J     = [ iVyN(:); iVyS(:); iVxW(:); iVxE(:); iVyNN(:); iVySS(:); iVxNW(:); iVxNE(:); iVxSW(:); iVxSE(:); iVxWW(:); iVxEE(:); iVyNW(:); iVySW(:); iVyNE(:); iVySE(:); ]';
V     = [ cVyN(:); cVyS(:); cVxW(:); cVxE(:); cVyNN(:); cVySS(:); cVxNW(:); cVxNE(:); cVxSW(:); cVxSE(:); cVxWW(:); cVxEE(:); cVyNW(:); cVySW(:); cVyNE(:); cVySE(:); ]';
%
if SuiteSparse==1, VPfJ  = sparse2(I,J,V);
else,              VPfJ  =  sparse(I,J,V); end
end
