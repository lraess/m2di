function [alpha] = HM2Di_LineSearch_Newt(phi,phi0bc,nx,ny,dx,dy,dt,CN,divVo,v,pt,pf,x,y,z,rhosg,rhofg,RogBG,phio,phi0,k_muf0,nperm,eta2mus,mus,musv,R,lamPe,noisy,resnlu0,resnlpt0,resnlpf0,nRMe0,nRCTe0,nRCFe0,qDx_W,qDx_E,qDy_N,qDy_S)
% Linesearch function for TwoPhase2D
% Thibault Duretz, Ludovic Raess and Yuri Podladchikov, Unil 2018
%   Direct search to find best relaxation factor
nsteps = 20;
amin   = 0.0;
amax   = 1.0;
dalpha = (amax-amin)/(nsteps-1);
alphav = amin:dalpha:amax;
nVx    = (nx+1)*ny;
nRMe   = zeros(nsteps,1);
nRCTe  = zeros(nsteps,1);
nRCFe  = zeros(nsteps,1);
v0=v; pt0=pt; pf0=pf;
% Compute non linear residual for different steps
for ils=1:nsteps
    v    =  v0+alphav(ils).*x; %alphav(ils)
    pt   = pt0+alphav(ils).*y;
    pf   = pf0+alphav(ils).*z;
    Pt   = reshape(pt          ,[nx  ,ny  ]);
    Pf   = reshape(pf          ,[nx  ,ny  ]);
    Vx   = reshape(v(1:nVx)    ,[nx+1,ny  ]);
    Vy   = reshape(v(nVx+1:end),[nx  ,ny+1]);
    % Divergence                      % Inner Vy
    divV                = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
    % NonLinearities
    phi     = (phio + (1-CN).*divV.*dt + CN.*divVo.*dt)./( (1-CN).*divV.*dt + CN.*divVo.*dt + 1);
    eta_phi = mus./phi*eta2mus .* (1+0.5*(1./R -1).*(1+tanh(-(Pt-Pf)./lamPe)));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    phi_NS  = [2*phi0bc-phi(:,1), phi, 2*phi0bc-phi(:,end)];
    phi_y   = 0.5*(phi_NS(:,1:end-1)+phi_NS(:,2:end-0));
    ky      = k_muf0.*(phi_y/phi0).^nperm;
    phi_EW  = [2*phi0bc-phi(1,:); phi; 2*phi0bc-phi(end,:)];
    phi_x   = 0.5*(phi_EW(1:end-1,:)+phi_EW(2:end-0,:));
    kx      = k_muf0.*(phi_x/phi0).^nperm;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    kgy     = (rhofg - RogBG)*diff(ky,1,2)/dy; % gravity
    Rogy    = rhofg.*phi_y + (1-phi_y).*rhosg - RogBG;
    % Momentum residual
    Vx_e        = zeros(nx+1,ny+2); Vx_e(:,2:end-1) = Vx;
    Vy_e        = zeros(nx+2,ny+1); Vy_e(2:end-1,:) = Vy;
    Vx_e(:,  1) = Vx(:,  1); % Free slip
    Vx_e(:,end) = Vx(:,end); % Free slip
    Vy_e(  1,:) = Vy(  1,:); % Free slip
    Vy_e(end,:) = Vy(end,:); % Free slip
    tau_xx      = 2*mus.*(diff(Vx,1,1)/dx - 1/3*divV);
    tau_yy      = 2*mus.*(diff(Vy,1,2)/dy - 1/3*divV);
    tau_xy      =  musv.*(diff(Vx_e,1,2)/dy + diff(Vy_e,1,1)/dx );
    Res_x       = [zeros(1,ny); diff(-Pt + tau_xx,1,1)/dx + diff(tau_xy(2:end-1,:),1,2)/dy; zeros(1,ny)];
    Res_y       = [zeros(nx,1), diff(-Pt + tau_yy,1,2)/dy + diff(tau_xy(:,2:end-1),1,1)/dx - Rogy(:,2:end-1), zeros(nx,1)];
    RMe         = [Res_x(:) ; Res_y(:)];
    nRMe(ils)   = norm(RMe)/((nx+1)*ny + (ny+1)*nx);
    % Total continuity residual
    RCTe        = -(diff(Vx,1,1)/dx+diff(Vy,1,2)/dy) - (Pt-Pf)./(eta_phi.*(1-phi));
    RCTe        = RCTe - mean(RCTe(:));
    nRCTe(ils)  = norm(RCTe(:))/(nx*ny);
    % Fluid continuity residual
    qDx = zeros(nx+1,ny); qDy = zeros(nx,ny+1); % zero fluxes
    qDx(2:end-1,:) = -kx(2:end-1,:).*diff(Pf,1,1)/dx;
    qDx([1 end],:) = [qDx_W';qDx_E'];
    qDy(:,2:end-1) = -ky(:,2:end-1).*diff(Pf,1,2)/dy;
    qDy(:,[1 end]) = [qDy_S qDy_N];
    divqD          = diff(qDx,1,1)/dx + diff(qDy,1,2)/dy - kgy;
    RCFe        = -divqD + (Pt-Pf)./(eta_phi.*(1-phi));
    nRCFe(ils)  = norm(RCFe(:))/(nx*ny);
%     if ils==1, fprintf(' LS: Selected alpha = %2.2f\n', alphav(ils) );
%     fprintf(' LS: NonLin res. ||res.v ||=%2.4e, ||res.v /res.v0 ||=%2.4e\n', nRMe (ils), nRMe (ils)/resnlu0  );
%     fprintf(' LS: NonLin res. ||res.pt||=%2.4e, ||res.pt/res.pt0||=%2.4e\n', nRCTe(ils), nRCTe(ils)/resnlpt0 );
%     fprintf(' LS: NonLin res. ||res.pf||=%2.4e, ||res.pf/res.pf0||=%2.4e\n', nRCFe(ils), nRCFe(ils)/resnlpf0 ); 
%     end
end
[~,ibest] = min(nRCFe);
if ibest==1, LSsuccess=0; alpha=0; if noisy>=1, fprintf('No descent found - continuing ...'); end
    nRCFe(1) = 2*max(nRCFe); [~,ibest] = min(nRCFe); alpha = alphav(ibest);
else         LSsuccess=1; alpha = alphav(ibest); end

if noisy>=1, fprintf(' LS: Selected alpha = %2.2f\n', alpha );
    fprintf(' LS: NonLin res. ||res.v ||=%2.4e, ||res.v /res.v0 ||=%2.4e\n', nRMe (ibest), nRMe (ibest)/resnlu0  );
    fprintf(' LS: NonLin res. ||res.pt||=%2.4e, ||res.pt/res.pt0||=%2.4e\n', nRCTe(ibest), nRCTe(ibest)/resnlpt0 );
    fprintf(' LS: NonLin res. ||res.pf||=%2.4e, ||res.pf/res.pf0||=%2.4e\n', nRCFe(ibest), nRCFe(ibest)/resnlpf0 ); end
end
