function [ VPfJ2 ] = HM2Di_AssembleJpfv2( Newton, SuiteSparse, CN, ky, phiy, divV, divVo, phio, dx, dy, dt, Pf, nperm, RogBG, rhofg, NumPt, NumVx, NumVyG  )
% AssembleJpfv2
% Divergences
divVN = zeros(size(Pf)); divVN(:,1:end-1) = divV(:,2:end-0);
divVS = zeros(size(Pf)); divVS(:,2:end-0) = divV(:,1:end-1);
% Indices
iPt   = NumPt(:);
iVxW  = NumVx(1:end-1, :     );
iVxE  = NumVx(2:end  , :     );
iVyS  = NumVyG( :    ,1:end-1);
iVyN  = NumVyG( :    ,2:end  );
iVxWW = ones(size(Pf)); iVxWW(2:end  , :     ) = NumVx(1:end-2   , :     );
iVxEE = ones(size(Pf)); iVxEE(1:end-1, :     ) = NumVx(3:end     , :     );
iVySS = ones(size(Pf)); iVySS( :     ,2:end  ) = NumVyG( :       ,1:end-2);
iVyNN = ones(size(Pf)); iVyNN( :     ,1:end-1) = NumVyG( :       ,3:end  );
iVySW = ones(size(Pf)); iVySW(2:end  , :     ) = NumVyG( 1:end-1 ,1:end-1);
iVyNW = ones(size(Pf)); iVyNW(2:end  , :     ) = NumVyG( 1:end-1 ,2:end  );
iVySE = ones(size(Pf)); iVySE(1:end-1, :     ) = NumVyG( 2:end   ,1:end-1);
iVyNE = ones(size(Pf)); iVyNE(1:end-1, :     ) = NumVyG( 2:end   ,2:end  );
iVxSW = ones(size(Pf)); iVxSW( :     ,2:end  ) = NumVx(1:end-1   ,1:end-1);
iVxSE = ones(size(Pf)); iVxSE( :     ,2:end  ) = NumVx(2:end     ,1:end-1);
iVxNW = ones(size(Pf)); iVxNW( :     ,1:end-1) = NumVx(1:end-1   ,2:end  );
iVxNE = ones(size(Pf)); iVxNE( :     ,1:end-1) = NumVx(2:end     ,2:end  );
% Effective permeability
kN        = ky(:,2:end-0);  kN(:,end) = 0; % ACHTUNG: there was a problem here
kS        = ky(:,1:end-1);  kS(:,  1) = 0;
phiN      = phiy(:,2:end-0);
phiS      = phiy(:,1:end-1);
% Beauty
phi_oN = ones(size(Pf)); phi_oN(:,1:end-1) = phio(:,2:end-0); 
phi_oS = ones(size(Pf)); phi_oS(:,2:end-0) = phio(:,1:end-1); 
divVoN = ones(size(Pf)); divVoN(:,1:end-1) = divVo(:,2:end-0); 
divVoS = ones(size(Pf)); divVoS(:,2:end-0) = divVo(:,1:end-1); 
dphi_ddiv   = (1-CN)*dt*(1-phio ) ./ ((1-CN)*divV *dt + CN*divVo *dt +1).^2; % ACHTUNG: there was a problem here
dphiN_ddivN = (1-CN)*dt*(1-phi_oN) ./ ((1-CN)*divVN*dt + CN*divVoN*dt +1).^2;
dphiS_ddivS = (1-CN)*dt*(1-phi_oS) ./ ((1-CN)*divVS*dt + CN*divVoS*dt +1).^2;
D23S        = -nperm.*kS./phiS*(rhofg - RogBG).*dphiS_ddivS;
D23N        = -nperm.*kN./phiN*(rhofg - RogBG).*dphiN_ddivN;
D23N_1      = -(RogBG - rhofg).*( -dphi_ddiv.*kN.*nperm./(phiN) - kS.*nperm.*(dphiS_ddivS - dphi_ddiv)./phiS);
D23S_2      = -(RogBG - rhofg).*( -dphi_ddiv.*kS.*nperm./(phiS) - kN.*nperm.*(dphiN_ddivN - dphi_ddiv)./phiN);
cVxE  = Newton.*0.5*D23N_1./(dx.*dy) - 0.5*D23S_2./(dx.*dy);
cVxW  = Newton.*-0.5*D23N_1./(dx.*dy) + 0.5*D23S_2./(dx.*dy);
cVyN  = Newton.*-0.5*D23S_2./dy.^2; 
cVyS  = Newton.*-0.5*D23N_1./dy.^2;
cVxNW = Newton.*-0.5*D23N./(dx.*dy);
cVxSW = Newton.*0.5*D23S./(dx.*dy);
cVxNE = Newton.*0.5*D23N./(dx.*dy);
cVxSE = Newton.*-0.5*D23S./(dx.*dy);
cVySS = Newton.*0.5*D23S./dy.^2;
cVyNN = Newton.*0.5*D23N./dy.^2;
%
I     = [ iPt(:);  iPt(:);  iPt(:);  iPt(:);  iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);   iPt(:);  ]';
J     = [ iVyS(:); iVyN(:); iVxW(:); iVxE(:); iVySS(:); iVyNN(:); iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:) ]';
V     = [ cVyS(:); cVyN(:); cVxW(:); cVxE(:); cVySS(:); cVyNN(:); cVxSW(:); cVxSE(:); cVxNW(:); cVxNE(:) ]';
if SuiteSparse==1, VPfJ2  = sparse2(I,J,V);
else,              VPfJ2  = sparse(I,J,V); end
end
