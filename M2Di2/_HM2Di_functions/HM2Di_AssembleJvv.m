function [K, BcK] = HM2Di_AssembleJvv(SuiteSparse, dx, dy, nx, ny, Vx, Vy, mus, musv, Rogy, NumVx, NumVyG, Vx_W, Vx_E, Vy_S, Vy_N, ibcVxW, ibcVxE, ibcVyS, ibcVyN)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
comp   = 1;
% x momentum
NeuxS  = zeros(size(Vx)); NeuxS(:,  1) = 1;
NeuxN  = zeros(size(Vx)); NeuxN(:,end) = 1;
ExyBCS = zeros(size(Vx)); % free slip --> 0
ExyBCN = zeros(size(Vx)); % free slip --> 0
DirxS  = zeros(size(Vx));
DirxN  = zeros(size(Vx));
VxBCS  = zeros(size(Vx));
VxBCN  = zeros(size(Vx));
etaW   = zeros(size(Vx)); etaW(2:end-1,:) = mus(1:end-1,:);
etaE   = zeros(size(Vx)); etaE(2:end-1,:) = mus(2:end-0,:);
etaS   = musv(:,1:end-1); etaS(:,1) = 0;
etaN   = musv(:,2:end-0); etaN(:,end) = 0;
inner  = ones(size(Vx));  inner([1 end],:) =0;
cVxC   = -1.0*DirxN.*NeuxN.*etaN./dy.^2 + 1.0*DirxN.*etaN./dy.^2 - 1.0*DirxS.*NeuxS.*etaS./dy.^2 + 1.0*DirxS.*etaS./dy.^2 - 1.0*NeuxN.*etaN./dy.^2 - 1.0*NeuxS.*etaS./dy.^2 - 2*comp.*etaE./(3*dx.^2) - 2*comp.*etaW./(3*dx.^2) + 1.0*etaN./dy.^2 + 1.0*etaS./dy.^2 + 2*etaE./dx.^2 + 2*etaW./dx.^2;
cVxW   = inner.*(2*comp.*etaW./(3*dx.^2) - 2*etaW./dx.^2);
cVxE   = inner.*(2*comp.*etaE./(3*dx.^2) - 2*etaE./dx.^2);
cVxS   = inner.*(-1.0*DirxS.*NeuxS.*etaS./dy.^2 + 1.0*DirxS.*etaS./dy.^2 + 1.0*NeuxS.*etaS./dy.^2 - 1.0*etaS./dy.^2);
cVxN   = inner.*(-1.0*DirxN.*NeuxN.*etaN./dy.^2 + 1.0*DirxN.*etaN./dy.^2 + 1.0*NeuxN.*etaN./dy.^2 - 1.0*etaN./dy.^2);
cVyNE  = inner.*(1.0*NeuxN.*etaN./(dx.*dy) + 2*comp.*etaE./(3*dx.*dy) - 1.0*etaN./(dx.*dy));
cVySW  = inner.*(1.0*NeuxS.*etaS./(dx.*dy) + 2*comp.*etaW./(3*dx.*dy) - 1.0*etaS./(dx.*dy));
cVySE  = inner.*(-1.0*NeuxS.*etaS./(dx.*dy) - 2*comp.*etaE./(3*dx.*dy) + 1.0*etaS./(dx.*dy));
cVyNW  = inner.*(-1.0*NeuxN.*etaN./(dx.*dy) - 2*comp.*etaW./(3*dx.*dy) + 1.0*etaN./(dx.*dy));
crx    = inner.*(2.0*DirxN.*NeuxN.*VxBCN.*etaN./dy.^2 - 2.0*DirxN.*VxBCN.*etaN./dy.^2 + 2.0*DirxS.*NeuxS.*VxBCS.*etaS./dy.^2 - 2.0*DirxS.*VxBCS.*etaS./dy.^2 - 2*ExyBCN.*NeuxN.*etaN./dy + 2*ExyBCS.*NeuxS.*etaS./dy);
% symmetrise (need to move corresponding entries to RHS)
cVxW(    2,:) = 0;
cVxE(end-1,:) = 0;
cVySW(:,  1)  = 0; cVySE(:,  1) = 0;
cVyNW(:,end)  = 0; cVyNE(:,end) = 0;
iVx     = NumVx(:);
iVxW    = ones(size(Vx));   iVxW(2:end-1, :     ) =  NumVx(1:end-2, :     );
iVxE    = ones(size(Vx));   iVxE(2:end-1, :     ) =  NumVx(3:end  , :     );
iVxS    = ones(size(Vx));   iVxS(2:end-1,2:end  ) =  NumVx(2:end-1,1:end-1);
iVxN    = ones(size(Vx));   iVxN(2:end-1,1:end-1) =  NumVx(2:end-1,2:end  );
iVySW   = ones(size(Vx));  iVySW(2:end-1, :     ) = NumVyG(1:end-1,1:end-1);
iVySE   = ones(size(Vx));  iVySE(2:end-1, :     ) = NumVyG(2:end-0,1:end-1);
iVyNW   = ones(size(Vx));  iVyNW(2:end-1, :     ) = NumVyG(1:end-1,2:end  );
iVyNE   = ones(size(Vx));  iVyNE(2:end-1, :     ) = NumVyG(2:end-0,2:end  );
Iu      = [   iVx(:);   iVx(:);   iVx(:);   iVx(:);   iVx(:);    iVx(:);    iVx(:);    iVx(:);    iVx(:); ];
Ju      = [   iVx(:);  iVxW(:);  iVxE(:);  iVxS(:);  iVxN(:);  iVySW(:);  iVySE(:);  iVyNW(:);  iVyNE(:); ];
Vu      = [  cVxC(:);  cVxW(:);  cVxE(:);  cVxS(:);  cVxN(:);  cVySW(:);  cVySE(:);  cVyNW(:);  cVyNE(:); ];
% y momentum
NeuyW  = zeros(size(Vy)); NeuyW(   1, :) = 1;
NeuyE  = zeros(size(Vy)); NeuyE( end, :) = 1;
DiryW  = zeros(size(Vy));
DiryE  = zeros(size(Vy));
ExyBCW = zeros(size(Vy));
ExyBCE = zeros(size(Vy));
VyBCW  = zeros(size(Vy));
VyBCE  = zeros(size(Vy));
etaS   = zeros(size(Vy)); etaS(:,2:end-1,:) = mus(:,1:end-1);
etaN   = zeros(size(Vy)); etaN(:,2:end-1,:) = mus(:,2:end-0);
etaW   = musv(1:end-1,:); etaW(  1,:) = 0;
etaE   = musv(2:end-0,:); etaE(end,:) = 0;
inner  = ones(size(Vy));  inner(:,[1 end]) =0;
cVyC   = -1.0*DiryE.*NeuyE.*etaE./dx.^2 + 1.0*DiryE.*etaE./dx.^2 - 1.0*DiryW.*NeuyW.*etaW./dx.^2 + 1.0*DiryW.*etaW./dx.^2 - 1.0*NeuyE.*etaE./dx.^2 - 1.0*NeuyW.*etaW./dx.^2 - 2*comp.*etaN./(3*dy.^2) - 2*comp.*etaS./(3*dy.^2) + 2*etaN./dy.^2 + 2*etaS./dy.^2 + 1.0*etaE./dx.^2 + 1.0*etaW./dx.^2;
cVyW   = inner.*(-1.0*DiryW.*NeuyW.*etaW./dx.^2 + 1.0*DiryW.*etaW./dx.^2 + 1.0*NeuyW.*etaW./dx.^2 - 1.0*etaW./dx.^2);
cVyE   = inner.*(-1.0*DiryE.*NeuyE.*etaE./dx.^2 + 1.0*DiryE.*etaE./dx.^2 + 1.0*NeuyE.*etaE./dx.^2 - 1.0*etaE./dx.^2);
cVyS   = inner.*( 2*comp.*etaS./(3*dy.^2) - 2*etaS./dy.^2);
cVyN   = inner.*( 2*comp.*etaN./(3*dy.^2) - 2*etaN./dy.^2);
cVxNE  = inner.*( 1.0*NeuyE.*etaE./(dx.*dy) + 2*comp.*etaN./(3*dx.*dy) - 1.0*etaE./(dx.*dy));
cVxSW  = inner.*( 1.0*NeuyW.*etaW./(dx.*dy) + 2*comp.*etaS./(3*dx.*dy) - 1.0*etaW./(dx.*dy));
cVxSE  = inner.*(-1.0*NeuyE.*etaE./(dx.*dy) - 2*comp.*etaS./(3*dx.*dy) + 1.0*etaE./(dx.*dy));
cVxNW  = inner.*(-1.0*NeuyW.*etaW./(dx.*dy) - 2*comp.*etaN./(3*dx.*dy) + 1.0*etaW./(dx.*dy));
cry    = inner.*(2.0*DiryE.*NeuyE.*VyBCE.*etaE./dx.^2 - 2.0*DiryE.*VyBCE.*etaE./dx.^2 + 2.0*DiryW.*NeuyW.*VyBCW.*etaW./dx.^2 - 2.0*DiryW.*VyBCW.*etaW./dx.^2 - 2*ExyBCE.*NeuyE.*etaE./dx + 2*ExyBCW.*NeuyW.*etaW./dx);
% symmetrise (need to move corresponding entries to RHS)
cVyS(:,    2) = 0;
cVyN(:,end-1) = 0;
cVxNW(  1,:)  = 0; cVxNE(end,:) = 0;
cVxSW(  1,:)  = 0; cVxSE(end,:) = 0;
iVy     = NumVyG(:);
iVyW    = ones(size(Vy));   iVyW(2:end  ,2:end-1) = NumVyG(1:end-1,2:end-1);
iVyE    = ones(size(Vy));   iVyE(1:end-1,2:end-1) = NumVyG(2:end  ,2:end-1);
iVyS    = ones(size(Vy));   iVyS( :     ,2:end-1) = NumVyG( :     ,1:end-2);
iVyN    = ones(size(Vy));   iVyN( :     ,2:end-1) = NumVyG( :     ,3:end  );
iVxSW   = ones(size(Vy));  iVxSW( :     ,2:end-1) = NumVx (1:end-1,1:end-1);
iVxNW   = ones(size(Vy));  iVxNW( :     ,2:end-1) = NumVx (1:end-1,2:end-0);
iVxSE   = ones(size(Vy));  iVxSE( :     ,2:end-1) = NumVx (2:end  ,1:end-1);
iVxNE   = ones(size(Vy));  iVxNE( :     ,2:end-1) = NumVx (2:end  ,2:end-0);
Iv      = [   iVy(:);   iVy(:);   iVy(:);   iVy(:);   iVy(:);    iVy(:);    iVy(:);    iVy(:);    iVy(:); ];
Jv      = [   iVy(:);  iVyW(:);  iVyE(:);  iVyS(:);  iVyN(:);  iVxSW(:);  iVxSE(:);  iVxNW(:);  iVxNE(:); ];
Vv      = [  cVyC(:);  cVyW(:);  cVyE(:);  cVyS(:);  cVyN(:);  cVxSW(:);  cVxSE(:);  cVxNW(:);  cVxNE(:); ];
if SuiteSparse==1, K = sparse2([Iu;Iv],[Ju;Jv],[Vu;Vv], (nx+1)*ny + (ny+1)*nx,(nx+1)*ny + (ny+1)*nx);
else,              K =  sparse([Iu;Iv],[Ju;Jv],[Vu;Vv], (nx+1)*ny + (ny+1)*nx,(nx+1)*ny + (ny+1)*nx); end
% RHS
BcK             = zeros(size(K,1),1);
BcK(NumVyG(:))  = BcK(NumVyG(:)) - Rogy(:);  % Gravity term in rhs
BcK([ibcVxW; ibcVxE; ibcVyS; ibcVyN]) = [Vx_W; Vx_E; Vy_S; Vy_N];
end

