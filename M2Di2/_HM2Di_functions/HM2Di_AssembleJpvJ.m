function [ P2V ] = HM2Di_AssembleJpvJ(Newton, SuiteSparse, CN, div, divo, phio, dx, dy, dt, phi, Pt, Pf, eta_phi, detadphi, NumPt, NumVx, NumVyG  )
% AssembleJpvJ

nx       = size(phi,1);
ny       = size(phi,2);
dtcdphi = (-detadphi.*(Pf - Pt).*(phi - 1) + eta_phi.*(-Pf + Pt))./(eta_phi.^2.*(phi - 1).^2);
cVxE = dtcdphi.*Newton.*dt.*(-CN + 1)./(dx.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1)) - dt.*(-CN + 1).*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + phio)./(dx.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1).^2);
cVxW = dtcdphi.*Newton.*-dt.*(-CN + 1)./(dx.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1)) + dt.*(-CN + 1).*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + phio)./(dx.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1).^2);
cVyN = dtcdphi.*Newton.*dt.*(-CN + 1)./(dy.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1)) - dt.*(-CN + 1).*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + phio)./(dy.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1).^2);
cVyS = dtcdphi.*Newton.*-dt.*(-CN + 1)./(dy.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1)) + dt.*(-CN + 1).*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + phio)./(dy.*(CN.*divo.*dt + dt.*(-CN + 1).*(div) + 1).^2);
% Needs to be canceled out!  % ACHTUNG: there was a problem here
cVxE(end,:) = -0*cVxE(end-1,:);
cVxW(  1,:) = -0*cVxW(  2,:);
cVyN(:,end) = -0*cVyN(:,end-1);
cVyS(:,  1) = -0*cVyS(:,  2);
iPt   = NumPt;
iVxW  = NumVx(1:end-1,:);
iVxE  = NumVx(2:end-0,:);
iVyS  = NumVyG(:,1:end-1);
iVyN  = NumVyG(:,2:end-0);
I   = [  iPt(:);  iPt(:);  iPt(:);  iPt(:) ]';
J   = [ iVxW(:); iVxE(:); iVyS(:); iVyN(:) ]';
V   = [ cVxW(:); cVxE(:); cVyS(:); cVyN(:) ]';
if SuiteSparse==1, P2V = sparse2( I(:), J(:), V(:), ny*nx, (nx+1)*ny+(ny+1)*nx );
else,              P2V =  sparse( I(:), J(:), V(:), ny*nx, (nx+1)*ny+(ny+1)*nx ); end
end

