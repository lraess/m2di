function [resnlv, resnlpt, resnlpf, fv, fpt, fpf] = HM2Di_MFreeresid(phi, Vx, Vy, Pt, Pf, divV, eta_phi, mus, musv, Rogy, kgy, kx, ky, qDx_W, qDx_E, qDy_N, qDy_S, nx, ny, dx, dy)
% MFreeresid

% Momentum residual
Vx_e        = zeros(nx+1,ny+2); Vx_e(:,2:end-1) = Vx;
Vy_e        = zeros(nx+2,ny+1); Vy_e(2:end-1,:) = Vy;
Vx_e(:,  1) = Vx(:,  1); % Free slip
Vx_e(:,end) = Vx(:,end); % Free slip
Vy_e(  1,:) = Vy(  1,:); % Free slip
Vy_e(end,:) = Vy(end,:); % Free slip
tau_xx      = 2*mus.*(diff(Vx,1,1)/dx - 1/3*divV);
tau_yy      = 2*mus.*(diff(Vy,1,2)/dy - 1/3*divV);
tau_xy      =  musv.*(diff(Vx_e,1,2)/dy + diff(Vy_e,1,1)/dx );
Res_x       = [zeros(1,ny); diff(-Pt + tau_xx,1,1)/dx + diff(tau_xy(2:end-1,:),1,2)/dy; zeros(1,ny)];
Res_y       = [zeros(nx,1), diff(-Pt + tau_yy,1,2)/dy + diff(tau_xy(:,2:end-1),1,1)/dx - Rogy(:,2:end-1), zeros(nx,1)];
RMe         = [Res_x(:) ; Res_y(:)];
fv          = RMe(:);
resnlv      = norm(fv)/length(fv);
% Total continuity residual
RCTe        = -(diff(Vx,1,1)/dx+diff(Vy,1,2)/dy) - (Pt-Pf)./(eta_phi.*(1-phi));
RCTe        = RCTe - mean(RCTe(:));
fpt         = RCTe(:);
resnlpt     = norm(fpt)/length(fpt);
% Fluid continuity residual
qDx = zeros(nx+1,ny); qDy = zeros(nx,ny+1); % zero fluxes
qDx(2:end-1,:) = -kx(2:end-1,:).*diff(Pf,1,1)/dx;
qDx([1 end],:) = [qDx_W';qDx_E'];
qDy(:,2:end-1) = -ky(:,2:end-1).*diff(Pf,1,2)/dy;
qDy(:,[1 end]) = [qDy_S qDy_N];
divqD       = diff(qDx,1,1)/dx + diff(qDy,1,2)/dy - kgy;
RCFe        = -divqD + (Pt-Pf)./(eta_phi.*(1-phi));
fpf         = RCFe(:);
resnlpf     = norm(fpf)/length(fpf);
end

