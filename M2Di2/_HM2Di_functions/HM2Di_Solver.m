function [dv, dpt, dpf, itPH] = HM2Di_Solver(SuiteSparse, noisy, nfail, Jvv, Jpv, Jpfv, Jvp, Jpp, Jpfp, Jppf, Jpf, Kvv, Kpf, resnlv, resnlpt, resnlpf, nPH, dv, dpt, dpf, fv, fpt, fpf, tol_linv, tol_linpt, tol_linpf, tol_glob, BcPf, Kpv)
% HM2Di_Solver

coupled = 0;
itPH    = 0;
if coupled==1
    bpf  = BcPf;
    f    = [fv;   fpt;    fpf   ];
    Ms   = [Jvv    Jvp    0*Kpv';...
            Jpv    Jpp    Jppf  ;...
            Jpfv   Jpfp   Jpf   ];
    scm  = spdiags(mean(abs(Ms),2), 0, size(Ms,1), size(Ms,2));
    if exist('umfpack')==0
        ds = scm*( umfpack2((scm*Ms*scm),'\',(scm*f)) ); % UMFPACK \ solve
    else
        ds = scm*( umfpack((scm*Ms*scm),'\',(scm*f)) ); % UMFPACK \ solve
    end
    rJ   = Ms*ds-f;
    dv   = ds(1:length(dv));
    dpt  = ds(length(dv)+1:length(dv)+length(dpt));
    dpf  = ds(length(dv)+length(dpt)+1:end);
    ddpf = Kpf\bpf;
    if noisy>=1, fprintf('\n Resid Jac = %2.2e, Resid Pf = %2.2e \n\n', norm(rJ) ,norm(Kpf*ddpf-bpf)); end
else
    % Pre-conditionning (~Jacobi)
    Jpv_t = Jpv  - Jppf*(spdiags(diag(Jpf  ),0,size(Kpf,1),size(Kpf,2))\Jpfv);
    Jpp_t = Jpp  - Jppf*(spdiags(diag(Jpf  ),0,size(Kpf,1),size(Kpf,2))\Jpfp);
    Jvv_t = Kvv  - Jvp *(spdiags(diag(Jpp_t),0,size(Kpf,1),size(Kpf,2))\Jpv );
    [Jpf_h ,~,sI] = chol(Jpf  ,'lower','vector');                       % Cholesky factors
    [Jvv_th,~,sA] = chol(Jvv_t,'lower','vector');                       % Cholesky factors
    Jpp_th        = spdiags(1./diag(Jpp_t),0,size(Jpp,1),size(Jpp,2));  % trivial inverse
    rv0=resnlv; rpt0=resnlpt; rpf0=resnlpf;
    for itPH=1:nPH
        rv  = -( Jvv*dv  + Jvp*dpt             - fv  );
        rpt = -( Jpv*dv  + Jpp*dpt  + Jppf*dpf - fpt ); rpt = rpt - mean(rpt(:));
        rpf = -( Jpfv*dv + Jpfp*dpt + Jpf*dpf  - fpf );
        if SuiteSparse==1, s(sI,1) = cs_ltsolve(Jpf_h , cs_lsolve(Jpf_h , rpf(sI)));   % Backsubstitutions
        else,              s(sI,1) = Jpf_h'\(Jpf_h\rpf(sI)); end                       % Backsubstitutions
        rpt_t = -( Jppf*s - rpt);
        s     =    Jpp_th*rpt_t;
        rv_t  = -( Jvp*s  - rv );
        if SuiteSparse==1, ddv(sA,1) = cs_ltsolve(Jvv_th, cs_lsolve(Jvv_th, rv_t(sA)));
        else,              ddv(sA,1) = Jvv_th'\(Jvv_th\rv_t(sA)); end
        s    = -( Jpv_t*ddv - rpt_t );
        ddpt =    Jpp_th*s;
        s    = -( Jpfp*ddpt + Jpfv*ddv - rpf );
        if SuiteSparse==1, ddpf(sI,1) = cs_ltsolve(Jpf_h , cs_lsolve(Jpf_h , s(sI)));
        else,              ddpf(sI,1) = Jpf_h'\(Jpf_h\s(sI)); end
        dv  = dv  + ddv; % Updates
        dpt = dpt + ddpt;
        dpf = dpf + ddpf;
        if noisy>1, fprintf('  --- iteration %d --- \n',itPH);
            fprintf('  ||res.v ||=%2.2e\n', norm(rv)/length(rv));
            fprintf('  ||res.pt||=%2.2e\n', norm(rpt)/length(rpt));
            fprintf('  ||res.pf||=%2.2e\n', norm(rpf)/length(rpf));
        end
        if ((norm(rv)/length(rv)) < tol_linv) && ((norm(rpt)/length(rpt)) < tol_linpt) && ((norm(rpf)/length(rpf)) < tol_linpf), break; end
        if ((norm(rv)/length(rv)) > (norm(rv0)/length(rv0)) && norm(rv)/length(rv) < tol_glob && (norm(rpt)/length(rpt)) > (norm(rpt0)/length(rpt0)) && norm(rpt)/length(rpt) < tol_glob && (norm(rpf)/length(rpf)) > (norm(rpf0)/length(rpf0)) && norm(rpf)/length(rpf) < tol_glob),
            if noisy>=1, fprintf(' > Linear residuals do no converge further:\n'); break; end
        end
        rv0=rv; rpt0=rpt; rpf0=rpf; if (itPH==nPH), nfail=nfail+1; end
    end%itPH
    if noisy>=1
        fprintf(' - Linear res. ||res.v ||=%2.2e\n', norm(rv)/length(rv) );
        fprintf(' - Linear res. ||res.pt||=%2.2e\n', norm(rpt)/length(rpt) );
        fprintf(' - Linear res. ||res.pf||=%2.2e\n', norm(rpf)/length(rpf) );
    end
end%coupled

end

