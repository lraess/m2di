function [Kpp, KppJ] = HM2Di_AssembleJpp(SuiteSparse, phi, Pt ,Pf, eta_phi, detadPt, NumPt)
% AssembleJpp

iPt   = NumPt;  I = iPt(:);  J = I;
cPtPt = 1./(eta_phi.*(-phi + 1));
if SuiteSparse==1, Kpp = sparse2(I,J,cPtPt(:));
else,              Kpp =  sparse(I,J,cPtPt(:));  end
cPtPt = -detadPt.*(-Pf + Pt)./(eta_phi.^2.*(-phi + 1));
if SuiteSparse==1, KppJ = sparse2(I,J,cPtPt(:));
else,              KppJ =  sparse(I,J,cPtPt(:));  end
end

