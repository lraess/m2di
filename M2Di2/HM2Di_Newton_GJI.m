% =========================================================================
% Nonlinear Hydro-mechanical Two-Phase flow with analytical Newton solver.

% Copyright (C) 2019  Ludovic Raess, Thibault Duretz, Yury Podladchikov

% This file is part of M2Di. Please refer to:
% Räss, L., Duretz, T. and Podladchikov, Y. Y. (2019). 
% Resolving hydromechanical coupling in two and three dimensions: 
% spontaneous channelling of porous fluids owing to decompaction weakening
% eophysical Journal international, ggz239, https://doi.org/10.1093/gji/ggz239.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
function HM2Di_Newton_GJI()
addpath('./_HM2Di_functions/'), clc
noisy       = 1;                 % verbose level: no output (0), std output (1), full output (2)
SuiteSparse = 0;                 % Use optimised SuiteSparse library (1) or std Matlab (0)
Newton      = 1;                 % Newton linearisation (1) or Picard linearisation (0)
rednoise    = 0;                 % ellipse setup as in GJI paper (0) or random noise setup (1)
%% Physics
% Physics - scales
rhofg     = 1;                   % fluid density*gravity
k_muf0    = 1;                   % BG permeability
etaC0     = 1;                   % BG bulk viscosity
% Physics - non-dimensional parameters
eta2mus   = 10;                  % bulk to shear viscosity ratio
R         = 500;                 % bilin visc reduction
nperm     = 3;                   % nonlin Karman-Cozeny permeability
phi0      = 1e-2;                % BG porosity
ra        = 2;                   % domain W H ratio
lam0      = 1;                   % perutbation std devition
ttot      = 0.02;                % total simulation time
% Physics - dependent scales
rhosg     = 2*rhofg;             % Solid density*gravity
Lx        = 20;                  % domain W
Ly        = Lx*ra;               % domain H
phiA      = 2*phi0;              % porosity amplitude
lamPe     = 0.01;                % tanh amplitude
%% Numerics
nx        = 64;                  % number of grid points in x (W) dimension
ny        = 64*ra;               % number of grid points in y (H) dimension
nt        = 1;                   % number of timesteps
dt_red    = 1e-3;                % reduction of explicit timestep for porosity update
dt        = 1e-4;                % initial timestep
% Non-linear solver
CN        = 0.5;                 % the Crank-Nicolson switch [0 0.5]
tol_glob  = 1e-13;               % nonlinear tolerance
nmax_glob = 100;                 % max number of nonlinear iterations
LinSearch = 1;                   % Line search globalisatino procedure
alpha     = 0;                   % Line search globalisatino parameter if LinSearch = 0
% Linear solver
tol_linv  = tol_glob/1e3;        % linear solve tolerance momentum
tol_linpt = tol_glob/1e3;        % linear solve tolerance P_total
tol_linpf = tol_glob/1e3;        % linear solve tolerance P_fluid
nPH       = 150;
%% Preprocessing
mus0      = etaC0*phi0/eta2mus;  % BG shear viscosity
lam       = lam0*sqrt(etaC0*k_muf0);
RogBG     = rhofg.*phi0 + (1-phi0).*rhosg;
dx        = Lx/nx;
dy        = Ly/ny;
%% Initial conditions
divV      = zeros(nx  ,ny  );
Pf        = zeros(nx  ,ny  );
Pt        = zeros(nx  ,ny  );
Vx        = zeros(nx+1,ny  );
Vy        = zeros(nx  ,ny+1);
if rednoise==1
    [XC2,YC2] = ndgrid(+dx/2:dx:Lx-dx/2,+dy/2:dy:Ly-dy/2);
    radc = ( (XC2 - Lx*.5 ) / 6*lam).^2 + ( (YC2 - Ly*.3 ) / 8*lam).^2;
    phi  = detrend(cumsum(detrend(cumsum(rand(nx,ny)))'))';
    phi  = phi.*exp(-radc);
    phi  = phi - min(phi(:)); phi = phi/max(phi(:))-0.5; phi = 2*phi0 + phi.*phi0;
%     phie(:,[1,end])=mean(phie(:,1));
else
    arx  = 0.25;
    ary  = 1.0;
    [XC2,YC2] = ndgrid(+dx/2:dx:Lx-dx/2,+dy/2:dy:Ly-dy/2);
    radc = ( (XC2 - Lx*.5 ) / lam*arx).^2 + ( (YC2 - Ly*.2 ) / lam*ary).^2;
    phi       = phi0*ones(size(XC2));
    phi(radc<1) = phi(radc<1) + phiA;
end
% figure(100),clf,imagesc(phie'),axis xy,axis image, colorbar,colormap(jet)
%% Numbering Pt and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));
NumPf  = reshape(1:nx*ny    ,nx  ,ny  ); NumPfG = NumPf + max(NumPtG(:));
%% Boundary Conditions on velocities
ibcVxW = NumVx(1      ,:)';                 ibcVxE = NumVx(end    , : )';
ibcVxS = NumVx(2:end-1,1);                  ibcVxN = NumVx(2:end-1,end);
ibcVyS = NumVy( :     ,1)  + max(NumVx(:)); ibcVyN = NumVy( :     ,end)  + max(NumVx(:)); % global Numbering for Vy !
ibcVyW = NumVy(1,2:end-1)' + max(NumVx(:)); ibcVyE = NumVy(end,2:end-1)' + max(NumVx(:)); % global Numbering for Vy !
Vx_W   = 0*ones(1 ,ny  )';
Vx_E   = 0*ones(1 ,ny  )';
Vy_S   = 0*ones(nx,1   );
Vy_N   = 0*ones(nx,1   );
%% BC on Pf
Pf_W  = 0*ones(1 ,ny)'; % BC Dirichlet values
Pf_E  = 0*ones(1 ,ny)';
Pf_S  = 0*ones(nx,1 );
Pf_N  = 0*ones(nx,1 );
% Flux BC value
phi0bc = mean(phi(:,end));
% K_muf staggering for correct BC !!!
phi_NS = [2*phi0bc-phi(:,1), phi, 2*phi0bc-phi(:,end)];
phi_y  = 0.5*(phi_NS(:,1:end-1)+phi_NS(:,2:end-0));
ky     = k_muf0.*(phi_y/phi0).^nperm;
val    = (rhofg - RogBG)*diff(ky,1,2)/dy; % gravity
qDx_W = 0*ones(1 ,ny)'; % BC Neumann flux values
qDx_E = 0*ones(1 ,ny)';
qDy_S = val(:,1  );%val*ones(nx, 1);
qDy_N = val(:,end);%val*ones(nx, 1);
% BC Type: 1=Dirichlet (Default) , 2=Neumann
tbcW  = ones(1 ,ny)';  tbcW(:) = 2;
tbcE  = ones(1 ,ny)';  tbcE(:) = 2;
tbcS  = ones(nx,1 );   tbcS(:) = 2;
tbcN  = ones(nx,1 );   tbcN(:) = 2;
% [W E S N]
ibcPfW = NumPf(1,:)'; ibcPfE = NumPf(end,:)'; ibcPfS = NumPf(:,1); ibcPfN = NumPf(:,end);
id    = [1,length(ibcPfW),length(ibcPfW)+length(ibcPfE),length(ibcPfW)+length(ibcPfE)+length(ibcPfS),length(ibcPfW)+length(ibcPfE)+length(ibcPfS)+length(ibcPfN)];
tbc   = [ tbcW;  tbcE;  tbcS;  tbcN];
vBcD  = [ Pf_W;  Pf_E;  Pf_S;  Pf_N];
vBcN  = [qDx_W; qDx_E; qDy_S; qDy_N];
vBc   = zeros(size(tbc)); vBc(tbc==1)=vBcD(tbc==1); vBc(tbc==2)=vBcN(tbc==2);
% %% Assemble constant blocs & BC's
[Kvp, Kpv] = HM2Di_AssembleJpv(SuiteSparse, nx, ny, dx, dy, NumVx, NumVyG, NumPt, Vx_W, Vx_E, Vy_S, Vy_N);
% Linear shear viscosity
mus     = mus0*ones(nx,ny);
mus_axy = 0.25*(mus(1:end-1,1:end-1)+mus(2:end,1:end-1)+mus(1:end-1,2:end)+mus(2:end,2:end));
musv    = zeros(nx+1,ny+1); musv(2:end-1,2:end-1) = mus_axy; musv([1,end],:)=musv([2,end-1],:); musv(:,[1,end])=musv(:,[2,end-1]);
v = [Vx(:);Vy(:)]; pt = Pt(:); pf = Pf(:);
%% Timeloop
time=0; it=0; nfail=0;
while (time < ttot)
% for it=1:nt
    it = it+1;
    iter=0; resnlv=2*tol_glob; resnlpt=2*tol_glob; resnlpf=2*tol_glob;
    dv = zeros(max(NumVyG(:)),1); dpt = zeros(max(NumPt(:)),1); dpf = zeros(max(NumPf(:)),1); % prepare decoupled solve
    alphaV = zeros(nmax_glob,1);  niterV = zeros(nmax_glob,1); residV = zeros(nmax_glob,3);
    % Save previous solutions    
    phio  = phi;
    divVo = divV; % CN    
    % Non-linear iterations
    while (resnlv>tol_glob || resnlpt>tol_glob || resnlpf>tol_glob) && (iter < nmax_glob)
        iter = iter+1; fprintf('\n**** Nonlin iter %d **** \n', iter );
        % Divergence
        divV    = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
        dphidt  = (1-phi).*( (1-CN)*divV + CN*divVo );
        % Timestep selection
        if (it>1 && iter==1)
            dt_vol  = dt_red/(1e-10 + max(abs( dphidt(:) )));
            dt_advS = 0.5*dy/(1e-10 + max(abs( Vy(:)     )));
            dt      = min(dt_vol , dt_advS);
            fprintf('--> Selected dt = %2.2e\n', dt);
        end
        %% Non linearities        
        phi     = (phio + (1-CN).*divV.*dt + CN.*divVo.*dt)./( (1-CN).*divV.*dt + CN.*divVo.*dt + 1);
        eta_phi = mus./phi.*eta2mus .* (1+0.5*(1./R -1).*(1+tanh(-(Pt-Pf)./lamPe)));
        % K_muf staggering for correct BC !!!
        phi_NS  = [2*phi0bc-phi(:,1), phi, 2*phi0bc-phi(:,end)];
        phi_y   = 0.5*(phi_NS(:,1:end-1)+phi_NS(:,2:end-0));
        ky      = k_muf0.*(phi_y/phi0).^nperm;
        phi_EW  = [2*phi0bc-phi(1,:); phi; 2*phi0bc-phi(end,:)];
        phi_x   = 0.5*(phi_EW(1:end-1,:)+phi_EW(2:end-0,:));
        kx      = k_muf0.*(phi_x/phi0).^nperm;
        kgy     = (rhofg - RogBG)*diff(ky,1,2)/dy;
        Rogy    = rhofg.*phi_y + (1-phi_y).*rhosg - RogBG;
        % Derivatives of eta_phi
        detadPt  = -eta2mus.*mus.*(-0.5 + 0.5./R).*(-tanh((Pf - Pt)./lamPe).^2 + 1)./(lamPe.*phi);
        detadPf  =  eta2mus.*mus.*(-0.5 + 0.5./R).*(-tanh((Pf - Pt)./lamPe).^2 + 1)./(lamPe.*phi);
        detadphi = -eta2mus.*mus.*((-0.5 + 0.5./R).*(tanh((Pf - Pt)./lamPe) + 1) + 1)./phi.^2;
        %% Assemble blocks
        % Total Continuity block
        [Kpp, KppJ]       = HM2Di_AssembleJpp(SuiteSparse, phi, Pt, Pf, eta_phi, detadPt, NumPt);
        % Total-Fluid coupling block
        [Kppf, KppfJ]     = HM2Di_AssembleJppf(SuiteSparse, phi, Pt, Pf, eta_phi, detadPf, NumPt);
        % Fluid Continuity block
        [Kpf, KpfJ, BcPf] = HM2Di_AssembleJpf(SuiteSparse, nx, ny, dx, dy, phi, Pf, Pt, kx, ky, kgy, eta_phi, detadPf, NumPf, qDy_S, qDy_N);
        % Total Momentum Balance block
        [Kvv, ~]          = HM2Di_AssembleJvv(SuiteSparse, dx, dy, nx, ny, Vx, Vy, mus, musv, Rogy, NumVx, NumVyG, Vx_W, Vx_E, Vy_S, Vy_N, ibcVxW, ibcVxE, ibcVyS, ibcVyN);
        %% Jacobian additional blocks
        if Newton == 1
            % Assemble Jacobian arising non-linear permeability law (k_muf)
            [KpfvJ]       = HM2Di_AssembleJpfv(Newton, SuiteSparse, CN, kx, ky, divV, phi_x, phi_y, divVo, phio, dx, dy, dt, Pf, nperm, NumPt, NumVx, NumVyG );
            % Assemble Jacobian arising non-linear permeability law (kgy)
            [KpfvJ2]      = HM2Di_AssembleJpfv2(Newton, SuiteSparse, CN, ky, phi_y, divV, divVo, phio, dx, dy, dt, Pf, nperm, RogBG, rhofg, NumPt, NumVx, NumVyG  );
            % Assemble Jacobian contribution arising from non-linear body force
            [KvvJ, KvvJu] = HM2Di_AssembleJvvJu(Newton, SuiteSparse, CN, divV, divVo, phio, dx, dy, dt, phi, rhofg, rhosg, NumVx, NumVyG  );
            % Assemble Jacobian arising from 1/phi in continuity equations
            [KpvJ]        = HM2Di_AssembleJpvJ(Newton, SuiteSparse, CN, divV, divVo, phio, dx, dy, dt, phi, Pt, Pf, eta_phi, detadphi, NumPt, NumVx, NumVyG  );
        else
            KpfvJ = 0*Kpv; KpfvJ2 = 0*Kpv; KpvJ = 0*Kpv; KvvJ = 0*Kvv; KvvJu = 0*Kvv;
        end
        % Matrix-free residuals evaluation
        [resnlv, resnlpt, resnlpf, fv, fpt, fpf] = HM2Di_MFreeresid(phi, Vx, Vy, Pt, Pf, divV, eta_phi, mus, musv, Rogy, kgy, kx, ky, qDx_W, qDx_E, qDy_N, qDy_S, nx, ny, dx, dy);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if iter==1, resnlv0=resnlv; resnlpt0=resnlpt; resnlpf0=resnlpf; end
        if noisy>=1, fprintf('Chk: NonLin res. ||res.v ||=%2.4e, ||res.v /res.v0 ||=%2.4e\n', resnlv,  resnlv/resnlv0   );
                     fprintf('Chk: NonLin res. ||res.pt||=%2.4e, ||res.pt/res.pt0||=%2.4e\n', resnlpt, resnlpt/resnlpt0 );
                     fprintf('Chk: NonLin res. ||res.pf||=%2.4e, ||res.pf/res.pf0||=%2.4e\n', resnlpf, resnlpf/resnlpf0 ); end
        if (resnlv/resnlv0 < tol_glob && resnlpt/resnlpt0 < tol_glob && resnlpf/resnlpf0 < tol_glob), break; end
        %% Jacobian operator
        Jvv  = Kvv  + Newton*(KvvJu+KvvJ);  Jvp  = Kvp;
        Jpv  = Kpv  + Newton*KpvJ;          Jpp  = Kpp  + Newton*KppJ ;  Jppf = Kppf + Newton*KppfJ;
        Jpfv =-KpvJ + KpfvJ+ KpfvJ2;        Jpfp = Kppf + Newton*KppfJ;  Jpf  = Kpf  + Newton*KpfJ;
        %% Solver
        [dv, dpt, dpf, itPH] = HM2Di_Solver(SuiteSparse, noisy, nfail, Jvv, Jpv, Jpfv, Jvp, Jpp, Jpfp, Jppf, Jpf, Kvv, Kpf, resnlv, resnlpt, resnlpf, nPH, dv, dpt, dpf, fv, fpt, fpf, tol_linv, tol_linpt, tol_linpf, tol_glob, BcPf, Kpv);
        if LinSearch==1
            [alpha] = HM2Di_LineSearch_Newt(phi,phi0bc,nx,ny,dx,dy,dt,CN,divVo,v,pt,pf,dv,dpt,dpf,rhosg,rhofg,RogBG,phio,phi0,k_muf0,nperm,eta2mus,mus,musv,R,lamPe,noisy,resnlv0,resnlpt0,resnlpf0,resnlv,resnlpt,resnlpf,qDx_W,qDx_E,qDy_N,qDy_S);
        end
        alphaV(iter)   = alpha;
        niterV(iter)   = itPH;
        residV(iter,:) = [resnlv,resnlpt,resnlpf]';
        v   = v  + alpha*dv; % Solution from Newton iters
        pt  = pt + alpha*dpt;
        pf  = pf + alpha*dpf;
        % Recover solution
        Vx  = reshape(v(NumVx(:)) ,[nx+1,ny  ]);
        Vy  = reshape(v(NumVyG(:)),[nx  ,ny+1]);
        Pt  = reshape(pt          ,[nx  ,ny  ]);
        Pf  = reshape(pf          ,[nx  ,ny  ]);
    end%while nonlin
%     save(['evol_' int2str(Newton)],'alphaV','niterV','residV','iter');
    time = time + dt;
    fprintf('\n>------------------- TIMESTEP %d  -> dt=%2.2e  -> time=%2.2e ----------------------------------------\n',it,dt,time)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% Post-processing
    figure(1),clf,set(gcf,'color','white'),FS=20;
    subplot(121),semilogy(1:iter,residV(1:iter,1)/residV(1,1),'-ok',1:iter,residV(1:iter,2)/residV(1,3),'-xk',1:iter,residV(1:iter,3)/residV(1,3),'sk')
    leg=legend('$v$','${\bar{p}}$','${p^f}$','interpreter','latex','fontsize',FS,'location','SouthWest'); set(leg,'box','off')
    set(gca,'linewidth',1.5,'fontsize',FS-2)
    xlabel('$n_\mathrm{iter}^\mathrm{nonlin}$','interpreter','latex','fontsize',FS)
    ylabel('$||f||/||f||^0$','interpreter','latex','fontsize',FS)
    subplot(122), plot(1:iter,niterV(1:iter),'-*k')
    set(gca,'linewidth',1.5,'fontsize',FS-2)
    xlabel('$n_\mathrm{iter}^\mathrm{nonlin}$','interpreter','latex','fontsize',FS)
    ylabel('$n_\mathrm{iter}^\mathrm{lin}$','interpreter','latex','fontsize',FS+2)
    drawnow
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    qDx = -kx(2:end-1,:).* diff(Pf,1,1)/dx;
    qDy = -ky(:,2:end-1).*(diff(Pf,1,2)/dy + rhofg-RogBG);
    Pe  = Pt-Pf;
    k_muf = 0.25*(kx(1:end-1,:) + kx(2:end-0,:) + ky(:,1:end-1) + ky(:,2:end-0));
    figure(2),clf,set(gcf,'color','white'),colormap(jet), tp ={'Pt';'Pf';'Vx';'Vy';'qDx';'qDy'};
    for ipl=1:size(tp,1), eval(['Aname = ',tp{ipl},';']); subplot(3,2,ipl), imagesc(flipud(Aname')), title(tp{ipl}), colorbar, axis image; end
    drawnow
    figure(4),clf,set(gcf,'color','white'),colormap(jet), tp ={'phi';'log10(k_muf)';'Pe';'eta_phi'};
    for ipl=1:size(tp,1), eval(['Aname = ',tp{ipl},';']); subplot(2,2,ipl), imagesc(flipud(Aname')), title(tp{ipl}), colorbar, axis image; end
    drawnow
end%it

end
