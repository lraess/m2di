
————————————————————————————————————————————
 M2Di2 README | 06.06.2019  M2Di version 2.0
————————————————————————————————————————————

M2Di2: concise and efficient MATLAB 2D Stokes solver for linear and power
law viscous flow, thermo-mechanical and hydro-mechanical coupling.

Copyright (C) 2019  Ludovic Raess, Thibault Duretz, Yury Podladchikov.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

================================================================================

https://bitbucket.org/lraess/m2di/
http://www.unil.ch/geocomputing/software/


Please cite us if you use our routine:
Duretz, T., Räss, L., Podladchikov, Y. Y., and Schmalholz, S. M. (2019). 
Resolving thermomechanical coupling in two and three dimensions: spontaneous 
strain localisation owing to strain heating.
Geophysical Journal International, ggy434, https://doi.org/10.1093/gji/ggy434.

Räss, L., Duretz, T. and Podladchikov, Y. Y. (2019). Resolving hydromechanical 
coupling in two and three dimensions: spontaneous channelling of porous fluids 
owing to de-compaction weakening.
Geophysical Journal international, ggz239, https://doi.org/10.1093/gji/ggz239.

================================================================================
Distributed software, files in this directory:
================================================================================

_M2Di2_functions   Contains the M2Di2 support functions called by the
		   TM2Di2_Newton_GJI.m routine.

TM2Di2_Newton_GJI  Matlab routine to solve thermo-mechanical coupled power law
		   Stokes flow using analytical Jacobian for Newton iterations. 
		   This code reproduce the main figures of the GJI paper.

_HM2Di_functions   Contains the HM2Di support functions called by the
		   HM2Di_Newton_GJI.m routine.

HM2Di_Newton_GJI   Matlab routine to solve hydro-mechanical coupled Two-Phase flow
		   using analytical Jacobian for Newton iterations. This code reproduce 		   the figure of the GJI paper's Appendix.

HM2Di_CodeGen      Symbolic Python notebook containing the analytical Jacobian
	 	   derivation used in the HM2Di_Newton_GJI.m routine.

================================================================================
QUICK START: Open Matlab, select the routine and enjoy!

SuiteSparse: For optimal performance, download the SuiteSparse package at
http://www.suitesparse.com, run the Matlab install and set the
SuiteSparse switch to 1.

—————————————————————————————————
Detailed infos and main switches:
—————————————————————————————————

SuiteSparse	0=use the Matlab functions
		1=use the SuiteSparse optimised version

noisy		Choose how talkative the algorithm should be

solver		-2=use the decoupled iterative thermo-mechanical solver V-P-T
		-1=use the coupled direct solver "\" V-P-T
		 0=use the coupled direct solver "\" V-P
		 1=use the decoupled iterative mechanical solver V-P

Newton		0=use the Picard iterations to resolve nonlinearities
		1=use the Newton (analytical Jacobian) to solve the nonlinearities

================================================================================

Contact: ludovic.rass@gmail.com, thibault.duretz@univ-rennes1.fr
