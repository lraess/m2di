% =========================================================================
% Visco-Elasto-Viscoplastic solver for crustal scale shear banding.

% Copyright (C) 2019  Thibault Duretz, Ren? de Borst, Laetitia Le Pourhiet

% This file is part of M2Di.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

% Here I put the pressure constrain in the center of the model - such
% that it can not trigger any asymmetry.

function M2Di2_GRL_VEVP_shear_band_sym
% close all
addpath('./_M2Di2_functions/')
addpath('../_M2Di2_functions/')
addpath('../')
%% Switches
SuiteSparse = 0;                                                           % 0=NO SuiteSparse  1=SuiteSparse
noisy       = 2;                                                           % 0=NO Output       1=Std output     2=Very noisy  3=Very noisy + kspgcr output
Newton      = 1;                                                           % 0=Picard          1=Newton
mat_pert    = 1;                                                           % Thermal or material perturbation
SaveMatrices= 0;                                                           % Save matrices?
SaveFigs    = 10;                                                          % Set figures frequency
ShowFigs    = 1;                                                           % Print figures to screen
SaveData    = 10;                                                          % Set output file frequency
restart     = 0;                                                           % Restart from breakpoint?
%% Physics
xmin        =-5e4;                                                         % min x
xmax        = 5e4;                                                         % max x
ymin        =-3e4;                                                         % min y
ymax        = 0e3;                                                         % max y
Rg          = 8.314;                                                       % Gas constant
r           = 2e3;                                                         % Inclusion radius
Ebg         = 1e-15; % NEGATIVE for EXTENSION                              % Background strain rate
Tbg         = 1;                                                           % Background temperature
g           = 10; 
% Material 1 - MATRIX
ndis(1)     = 3.3;                                                         % Power Law exponent
Qdis(1)     = 186.5e3;                                                     % Activation energy
Adis(1)     = 3.1623e-26;                                                  % Pre-exponent (Power law model)
G(1)        = 1e10;                                                        % Shear modulus
C(1)        = 5.0e7;                                                       % Cohesion
phi(1)      = atan(0.6);                                                   % Friction angle
rho(1)      = 2700;                                                        % Density
% Material 2 - INCLUSION
ndis(2)     = 1;
Qdis(2)     = 0;
Adis(2)     = (1/1e20)^ndis(2)*exp(Qdis(2)/Rg/Tbg);
G(2)        = 1e10;
C(2)        = 1000*10;
phi(2)      = 0;
rho(2)      = 2700;
% Visco-plastic model (not supported yet)
eta_vp      = 1*1e21;                                                      % Reference viscosity for background strain rate
n_vp        = 2.0;                                                         % Power law exponent
ds          = 1e6;                                                         % Stress deviation from pure shear state
eta0_vp     = ds/abs(Ebg)^(1/n_vp);
fprintf('Overstress 1e-15: %2.2e\n', eta0_vp*1e-15^(1/n_vp))
fprintf('Overstress 1e-14: %2.2e\n', eta0_vp*1e-14^(1/n_vp))
fprintf('Overstress 1e-13: %2.2e\n', eta0_vp*1e-13^(1/n_vp))
%% Numerics
nx          = 2*100+1;                                                     % Number of cells in x
ny          = 2*30+1;                                                      % Number of cells in y
nt          = 1/1*100;                                                     % Number of time steps
dt          = 1/1*4e11;                                                    % Time step value
% Non-linear solver settings
nmax_glob   = 10;                                                          % Max. number of non-linear iterations
tol_glob    = 5e-10;                                                       % Global nonlinear tolerance
LineSearch  = 1;                                                           % Line search algorithm
alpha       = 1.0;                                                         % Default step (in case LineSearch = 0)
toljac      = 1e-8;                                                        % Step for numerical Jacobian
% Linear solver setting
gamma       = 1e-7;                                                        % Numerical compressibility
% Local iterations setting
litmax      = 100;                                                         % Max. number of local ietrations
littol      = 1e-11;                                                       % Tolerance of local iterations
%% Dimensionalisation
% Characterictic values
muc   = 1e22;
Tc    = 1;                                                                 % Kelvins
Lc    = (xmax-xmin);                                                       % Meters
tc    = 1/abs(Ebg);                                                        % Seconds
tauc  = muc*(1/tc);                                                        % Pascals
mc    = tauc*Lc*tc^2;                                                      % Kilograms
Jc    = mc*Lc^2/tc^2;                                                      % Joules
% Scaling
Adis    = Adis./(tauc.^-ndis.*1./tc);
Qdis    = Qdis./Jc;
Rg      = Rg/(Jc/Tc);
Ebg     = Ebg/(1/tc);
Tbg     = Tbg/Tc;
r       = r/Lc;
g       = g/(Lc/tc^2);
rho     = rho/(mc/Lc^3);
eta_vp  = eta_vp/muc;
G       = G/tauc;
C       = C/tauc;
dt      = dt/tc;
eta0_vp = eta0_vp/(tauc*tc^(1/n_vp));
xmin    = xmin/Lc;  xmax = xmax/Lc;
ymin    = ymin/Lc;  ymax = ymax/Lc;
%% Preprocessing
tic
Lx = xmax-xmin;     dx = Lx/nx;                                              % Grid spacing x
Ly = ymax-ymin;     dy = Ly/ny;                                              % Grid spacing y
xn = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;                             % Nodal coordinates x
yn = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;                             % Nodal coordinates y
[xn2,  yn2] = ndgrid(xn,yn);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xn,yc);
[xvy2,yvy2] = ndgrid(xc,yn);
rxvec  = zeros(nmax_glob,1);     cpu = zeros(9,1);
nitervec  = zeros(nt+1,1);       timev  = zeros(nt+1,1);     stress = zeros(nt+1,1); dtv  = dt*ones(nt+1,1); dt_r_vec = dt*ones(nt+1,1); dt_c_vec = dt*ones(nt+1,1);
rxvec_rel = zeros(nmax_glob,1);  rxvec_abs = zeros(nmax_glob,1);  
rvec_abs  = zeros(nt+1,nmax_glob); rvec_rel  = zeros(nt+1,nmax_glob);
time      = 0;
%% Initial arrays
Vx     = -Ebg.*xvx2;                                                       % Initial solutions for velocity in x
Vy     =  Ebg.*yvy2;                                                       % Initial solutions for velocity in y
Ptc    =    0.* xc2 + 1e-13;                                               % Initial solutions for pressure
phc    =     ones(nx  ,ny  );                                              % Material phase on centroids
phv    =     ones(nx+1,ny+1);                                              % Material phase on vertices
Tsurf  = 293/Tc;                                                           % Surface temperature
gradT  =-15/1e3 / (Tc/Lc);                                                 % Temperature gradient
Tec    =  Tsurf + yc2.*(gradT);                                            % Temperature field
Txxc   =    zeros(nx  ,ny  );                                              % Initial stress
Tyyc   =    zeros(nx  ,ny  );                                              % Initial stress
Txyv   =    zeros(nx+1,ny+1);                                              % Initial stress
if mat_pert==0, Tec((xc2-0*Lx/2).^2+(yc2+Ly/2).^2<r^2) = Tbg + 0.1*Tbg;    % Thermal perturbation
else            phc((xc2-0*Lx/2).^2+(yc2+Ly/2).^2<r^2) = 2;                % Material perturbation
                phv((xn2-0*Lx/2).^2+(yn2+Ly/2).^2<r^2) = 2;                % Material perturbation
end
rhogy  = g*rho(1)*ones(nx  ,ny+1);                                         % Gravity 1
rhogy((xvy2+0*Lx/2).^2+(yvy2-0*Ly/2).^2<r^2)  = g*rho(2);                  % Gravity 2                     
rhogy(:,[1 end]) = 0;                                                      % No body force for Vy Dirichlet nodes        
etaec  = G(phc).*dt.*ones(nx  ,ny  );                                      % elastic viscosity analogy (SHOULD MOVE IN THE TIME LOOP)
etaev  = G(phv).*dt.*ones(nx+1,ny+1);
Cc     = C(phc);             Cv     = C(phv);
phic   = phi(phc);           phiv   = phi(phv);
strc   = zeros(nx  ,ny  );   strv   = zeros(nx+1,ny+1);
%% Numbering Ptc and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));
cpu(1)=toc;
%% Define BC's ---------------- NEW STUFF
% Free slip / no slip setting for x momentum
BC.nsxS    =  zeros(size(Vx)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Vx)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Vx)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Vx)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Vx)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Vx)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Vx)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Vx)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
% Free slip / no slip setting for y momentum
BC.nsyW    =  zeros(size(Vy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Vy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Vy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Vy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Vy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Vy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Vy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Vy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
% Free surface
BC.frxN    =  zeros(size(Vx)); %BC.frxN(2:end-1,end) = 1;
BC.fryN    =  zeros(size(Vy)); %BC.fryN( :     ,end) = 1;
%% Boundary values - Dirichlets
Vx_W   = -Ebg.*xvx2(1  ,:)'.*ones(1 ,ny  )';                                 % BC value Vx West
Vx_E   = -Ebg.*xvx2(end,:)'.*ones(1 ,ny  )';                                 % BC value Vx East
Vy_S   =  Ebg.*yvy2(:,  1) .*ones(nx,1   );                                  % BC value Vy South
Vy_N   =  Ebg.*yvy2(:,end) .*ones(nx,1   );                                  % BC value Vy North
Vx_S = zeros(nx+1,1);  Vx_N = zeros(nx+1,1);
Vy_W = zeros(1,ny+1)'; Vy_E = zeros(1,ny+1)';
%% Construct BC structure ---------------- NEW STUFF
BC.Ux_W = Vx_W; BC.Ux_E = Vx_E; BC.Ux_S = Vx_S; BC.Ux_N = Vx_N;
BC.Uy_S = Vy_S; BC.Uy_N = Vy_N; BC.Uy_W = Vy_W; BC.Uy_E = Vy_E;
cpu(2)=toc;
%% Assemble pressure gradient and divergence operator
tic
[ grad, div, BcD ] = M2Di2_AssembleDivGrad( BC, dx, dy, nx, ny,NumVx, NumVyG, NumPt, SuiteSparse );
%% Assemble Block PP
iPt   = NumPt;  I = iPt(:)';  J = I;                                     % Eq. index center (pressure diagonal)
V     = gamma*ones(size(Ptc(:)));                                         % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V);                                  % Matrix assembly
else               PP = sparse (I,J,V); end
PPI   = spdiags(1./diag(PP),0,PP);                                       % Trivial inverse of diagonal PP matrix
cpu(3)=toc;   

istart = 0;

if restart>0
    load('Breakpoint')
    istart = it + 1;
end

%% Timesteps
for it=istart:nt
    fprintf('\n------------- Time step #%03d -------------\n',it);% Initial solutions for pressure
    Txxco  = Txxc;
    Tyyco  = Tyyc;
    Txyvo  = Txyv;
    %% Non linear iterations
    iter=0; resnlu=2*tol_glob; resnlu0=2*tol_glob;
    du = [0*Vx(:); 0*Vy(:)];         dp = zeros(max(NumPt(:)),1);
    rxvec_rel = zeros(nmax_glob,1);    rxvec_abs = zeros(nmax_glob,1);   
    % Constant 2D field (temperature perturbation) - Interpolate from centers to nodes and extrapolates boundaries
    Tei = 0.25*(Tec(1:end-1,1:end-1) + Tec(2:end,1:end-1) + Tec(1:end-1,2:end) + Tec(2:end,2:end));
    Tev = zeros(nx+1,ny+1);  Tev(2:end-1,2:end-1)=Tei; Tev([1,end],:)=Tev([2,end-1],:); Tev(:,[1,end])=Tev(:,[2,end-1]);
%     while( (resnlu/resnlu0 > tol_glob && resnlu>tol_glob) && iter < nmax_glob )
    while( (resnlu/resnlu0 > tol_glob) && iter < nmax_glob )
        iter = iter+1; if noisy>=1, fprintf('\n *** Nonlin iter %d *** \n', iter ); end
        tic
        % Initial guess or iterative solution for strain increments
        divV        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
        Exxc        = diff(Vx,1,1)/dx - 1/3*divV;
        Eyyc        = diff(Vy,1,2)/dy - 1/3*divV;
        Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
        Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
        dVxdy       = diff(Vx_exp,1,2)/dy;
        dVydx       = diff(Vy_exp,1,1)/dx;
        Exyv        = 0.5*( dVxdy + dVydx );
        % Extrapolate trial strain components
        Exyc      = 0.25*(Exyv (1:end-1,1:end-1) + Exyv (2:end,1:end-1) + Exyv (1:end-1,2:end) + Exyv (2:end,2:end));
        Txyco     = 0.25*(Txyvo(1:end-1,1:end-1) + Txyvo(2:end,1:end-1) + Txyvo(1:end-1,2:end) + Txyvo(2:end,2:end));
        [ Exxv  ] = M2Di2_centroids2vertices( Exxc );
        [ Eyyv  ] = M2Di2_centroids2vertices( Eyyc );
        [ Txxvo ] = M2Di2_centroids2vertices( Txxco );
        [ Tyyvo ] = M2Di2_centroids2vertices( Tyyco );
        [  Ptv  ] = M2Di2_centroids2vertices(  Ptc );
        % Engineering convention
        Gxyc = 2*Exyc;
        Gxyv = 2*Exyv;
        % Invariants
        Eiic2    = 1/2*(Exxc.^2 + Eyyc.^2) + Exyc.^2;
        Eiiv2    = 1/2*(Exxv.^2 + Eyyv.^2) + Exyv.^2;
        % Viscosity
        mc   = 1/2*( 1./ndis(phc) - 1);
        mv   = 1/2*( 1./ndis(phv) - 1);
        Bc   = Adis(phc).^(-1./ndis(phc)) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
        Bv   = Adis(phv).^(-1./ndis(phv)) .* exp(Qdis(phv)/Rg./Tev./ndis(phv));
        % Compute rheology
        [ etac, Txxc, Tyyc, Txyc, detadexxc, detadeyyc, detadexyc, plc, eta_vp1, Eiic_e, Eiic_v, Eiic_vp, F3 ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, 3, Exxc, Eyyc, Exyc, Txxco, Tyyco, Txyco, Bc, mc, etaec, Ptc, phic, Cc, eta_vp, eta0_vp, n_vp  );
        [ etav, Txxv, Tyyv, Txyv, detadexxv, detadeyyv, detadexyv, plv, eta_vp1, Eiic_v, Eiic_v, Eiiv_vp,  ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxv, Eyyv, Exyv, Txxvo, Tyyvo, Txyvo, Bv, mv, etaev, Ptv, phiv, Cv, eta_vp, eta0_vp, n_vp  );
        % Compute Jacobian terms
        % 1. Perturb unknowns - centroids
        Eiic = sqrt(Eiic2); dExx = toljac*Eiic; dEyy = toljac*Eiic; dExy = toljac*Eiic; dP   = toljac*Ptc;
        [ etac_xx, ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxc+dExx, Eyyc     , Exyc     , Txxco, Tyyco, Txyco, Bc, mc, etaec, Ptc     , phic, Cc, eta_vp, eta0_vp, n_vp );
        [ etac_yy, ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxc     , Eyyc+dEyy, Exyc     , Txxco, Tyyco, Txyco, Bc, mc, etaec, Ptc     , phic, Cc, eta_vp, eta0_vp, n_vp  );
        [ etac_xy, ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxc     , Eyyc     , Exyc+dExy, Txxco, Tyyco, Txyco, Bc, mc, etaec, Ptc     , phic, Cc, eta_vp, eta0_vp, n_vp  );
        [ etac_p , ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxc     , Eyyc     , Exyc     , Txxco, Tyyco, Txyco, Bc, mc, etaec, Ptc + dP, phic, Cc, eta_vp, eta0_vp, n_vp  );
        % Viscosity derivatives - centroids
        detadexxc = (etac_xx - etac) ./ dExx;
        detadeyyc = (etac_yy - etac) ./ dEyy;
        detadexyc = (etac_xy - etac) ./ dExy;
        detadgxyc = 1/2*detadexyc;
        detadpc   = (etac_p  - etac) ./ dP;
        % 1. Perturb unknowns - vertices
        Eiiv = sqrt(Eiiv2); dExx = toljac*Eiiv; dEyy = toljac*Eiiv; dExy = toljac*Eiiv; dP   = toljac*Ptv;
        [ etav_xx, ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxv+dExx, Eyyv     , Exyv     , Txxvo, Tyyvo, Txyvo, Bv, mv, etaev, Ptv     , phiv, Cv, eta_vp, eta0_vp, n_vp  );
        [ etav_yy, ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxv     , Eyyv+dEyy, Exyv     , Txxvo, Tyyvo, Txyvo, Bv, mv, etaev, Ptv     , phiv, Cv, eta_vp, eta0_vp, n_vp  );
        [ etav_xy, ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxv     , Eyyv     , Exyv+dExy, Txxvo, Tyyvo, Txyvo, Bv, mv, etaev, Ptv     , phiv, Cv, eta_vp, eta0_vp, n_vp  );
        [ etav_p , ~, ~, ~ ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxv     , Eyyv     , Exyv     , Txxvo, Tyyvo, Txyvo, Bv, mv, etaev, Ptv + dP, phiv, Cv, eta_vp, eta0_vp, n_vp  );
        % Viscosity derivatives - vertices
        detadexxv = (etav_xx - etav) ./ dExx;
        detadeyyv = (etav_yy - etav) ./ dEyy;
        detadexyv = (etav_xy - etav) ./ dExy;
        detadgxyv = 1/2*detadexyv;
        detadpv   = (etav_p  - etav) ./ dP;        
        %% Rheological coefficients for the Picard operator
        % Centroids
        D.D11c = 2*etac; D.D12c = 0*etac; D.D13c = 0*etac; D.D14c = 0*etac;
        D.D21c = 0*etac; D.D22c = 2*etac; D.D23c = 0*etac; D.D24c = 0*etac;
        D.D31c = 0*etac; D.D32c = 0*etac; D.D33c = 1*etac; D.D34c = 0*etac;
        % Vertices
        D.D11v = 2*etav; D.D12v = 0*etav; D.D13v = 0*etav; D.D14v = 0*etav;
        D.D21v = 0*etav; D.D22v = 2*etav; D.D23v = 0*etav; D.D24v = 0*etav;
        D.D31v = 0*etav; D.D32v = 0*etav; D.D33v = 1*etav; D.D34v = 0*etav;
        %% Assemble Picard operator
        [ K, BcK ] = M2Di2_GeneralAnisotropicVVAssembly( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, 1, Vx, Vy, SuiteSparse );
        %% Rheological coefficients for the Jacobian
        if Newton == 1
            Exxc1   = Exxc + Txxco./2./etaec;
            Eyyc1   = Eyyc + Tyyco./2./etaec;
            Gxyc1   = Gxyc + Txyco./1./etaec;
            Exxv1   = Exxv + Txxvo./2./etaev;
            Eyyv1   = Eyyv + Tyyvo./2./etaev;
            Gxyv1   = Gxyv + Txyvo./1./etaev;
            % Centroids
            D.D11c = 2*etac +   2*detadexxc.*Exxc1; D.D12c =            2*detadeyyc.*Exxc1; D.D13c =            2*detadgxyc.*Exxc1; D.D14c = 2*detadpc.*Exxc1;
            D.D21c =            2*detadexxc.*Eyyc1; D.D22c = 2*etac +   2*detadeyyc.*Eyyc1; D.D23c =            2*detadgxyc.*Eyyc1; D.D24c = 2*detadpc.*Eyyc1;
            D.D31c =            1*detadexxc.*Gxyc1; D.D32c =            1*detadeyyc.*Gxyc1; D.D33c = 1*etac +   1*detadgxyc.*Gxyc1; D.D34c = 1*detadpc.*Gxyc1;
            % Vertices
            D.D11v = 2*etav +   2*detadexxv.*Exxv1; D.D12v =            2*detadeyyv.*Exxv1; D.D13v =            2*detadgxyv.*Exxv1; D.D14v = 2*detadpv.*Exxv1;
            D.D21v =            2*detadexxv.*Eyyv1; D.D22v = 2*etav +   2*detadeyyv.*Eyyv1; D.D23v =            2*detadgxyv.*Eyyv1; D.D24v = 2*detadpv.*Eyyv1;
            D.D31v =            1*detadexxv.*Gxyv1; D.D32v =            1*detadeyyv.*Gxyv1; D.D33v = 1*etav +   1*detadgxyv.*Gxyv1; D.D34v = 1*detadpv.*Gxyv1;
        end
        %% Assemble Jacobian
        [ J,  ~  ] = M2Di2_GeneralAnisotropicVVAssembly( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, 1, Vx, Vy, SuiteSparse );        
        %% Assemble Jacobian V2P
        D.D14c = D.D14c;
        D.D24c = D.D24c;
        D.D34v = D.D34v;
        V2PJ   = M2Di2_M2TAssembly( BC, D, dx, dy, nx, ny, 1, 1, NumVx, NumVyG, NumPt, Vx, Vy, SuiteSparse );
        V2PJ   = V2PJ + grad;
        %% Elasticity
        Fex = zeros(size(Vx));
        Fex(2:end-1,:) =       diff(etac./etaec.*Txxco,1,1)./dx;
        Fex            = Fex + diff(etav./etaev.*Txyvo,1,2)./dy;
        Fey = zeros(size(Vy));
        Fey(:,2:end-1) =       diff(etac./etaec.*Tyyco,1,2)./dy;
        Fey            = Fey + diff(etav./etaev.*Txyvo,1,1)./dx;
        % Build right hand sides
        bu = BcK + [Fex(:); Fey(:)]; bp = BcD; u = [Vx(:); Vy(:)]; p = Ptc(:);  % Assemble velocity/pressure vectors
        bu(NumVyG(:)) = bu(NumVyG(:)) - rhogy(:);
        % Set 0 pressure correction at the surface, in the center 
        equ          = NumPt((nx-1)/2+1, end);
        val          = 0*dy/2*rho(1)*g;
        bu           = bu - grad(:,equ)*val;
        grad(:,equ)  = 0;
        div(equ,:)   = 0;
        bp(equ )     = val;
        PPd          = 0*PP;
        PPd(equ,equ) = 1/1000;
        PPI(equ,equ) = 1000;
        V2PJ(:,equ)  = 0;
        % Evaluate non-Linear residual using matrix-vector products
        fu = -(   K*u + grad*p - bu );  resnlu = norm(fu)/length(fu);          % Velocity non-linear residuals
        fp = -( div*u +  PPd*p - bp );  resnlp = norm(fp)/length(fp);          % Pressure non-linear residuals
        if iter==1, resnlu0=resnlu; resnlp0=resnlp; end
        rxvec_abs(iter) = resnlu;
        rxvec_rel(iter) = resnlu/resnlu0;
        % Checks
        if noisy>=1,fprintf('Chk: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', resnlu, resnlu/resnlu0 );
            fprintf('Chk: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', resnlp, resnlp/resnlp0 ); rxvec(iter) = resnlu/resnlu0; end
        if (resnlu/resnlu0 < tol_glob  || resnlu<tol_glob), break; end
        cpu(4)=cpu(4)+toc;
        if SaveMatrices==1, save([  'MatlabStokes_step', num2str(it, '%02d'), '_iter', num2str(iter-1, '%02d')], 'K', 'BcK', 'grad', 'div', 'BcD', 'PP', 'PPI', 'NumVx', 'NumVyG', 'dx', 'dy' ); end
        if SaveMatrices==1, save(['JacobianStokes_step', num2str(it, '%02d'), '_iter', num2str(iter-1, '%02d')], 'J',  'fu', 'V2PJ', 'div',  'fp', 'PP', 'PPI'  ); end   
        %% Linear solver - obtain velocity and pressure corrections for current non linear iteration
        tic
        Ms  = [ J   , V2PJ ; ...
            div , PPd  ];                                                    % Assemble entire Jacobian
        f   = [ fu  ; fp   ];                                                % Assemble entire residual vector
        cpu(5)=cpu(5)+toc;
        tic
        dX  = Ms\f;                                                          % Call direct solver
        du  = dX(1:max(NumVyG(:)));                                          % Extract velocity correction
        dp  = dX(NumPtG(:));                                                 % Extract pressure correction
        f1  = Ms*dX - f;                                                     % Compute entire linear residual
        fu1 = f1(1:max(NumVyG(:)));                                          % Extract velocity linear residual
        fp1 = f1(NumPtG(:));                                                 % Extract pressure linear residual
        fprintf('Time for backslash: %1.1f s\n', toc )
        cpu(6)=cpu(6)+toc;
        tic
        if noisy>=1, fprintf(' - Linear res. ||res.u||=%2.2e\n', norm(fu1)/length(fu1) );
                     fprintf(' - Linear res. ||res.p||=%2.2e\n', norm(fp1)/length(fp1) ); end
        % Call line search - globalization procedure
        if LineSearch==1, [alpha,~] = LineSearch_Direct_pwl_vp(BC,nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,phic,phiv,Cc,Cv,Newton,noisy,resnlu0,resnlp0,etaec,etaev,Txxco,Tyyco,Txyco,Txxvo,Tyyvo,Txyvo,litmax,littol,rhogy,eta_vp,eta0_vp,n_vp); end
        u    = u + alpha*du;                                                     % Velocity update from non-linear iterations
        p    = p + alpha*dp;                                                     % Pressure update from non-linear iterations
        Vx   = reshape(u(NumVx(:)) ,[nx+1,ny  ]);                                % Velocity in x (2D array)
        Vy   = reshape(u(NumVyG(:)),[nx  ,ny+1]);                                % Velocity in y (2D array)
        Ptc  = reshape(p(NumPt(:)) ,[nx  ,ny  ]);                                % Pressure      (2D array)
        cpu(7)=cpu(7)+toc; 
    end
    Tiic             = sqrt(1/2*Txxc.^2 + 1/2*Tyyc.^2 + Txyc.^2);
    time             = time + dt;
    timev(it+1)      = time;
    stress(it+1)     = sum(Tiic(:)*dx*dy);
    rvec_rel(it+1,:) = rxvec_rel(:);
    rvec_abs(it+1,:) = rxvec_abs(:);
    nitervec(it+1)   = iter;
    tic
    %% Post-processing - be a cool cat and use Fabio Crameri's colorbars (http://www.fabiocrameri.ch) -->  % load('roma.mat')
    if ShowFigs==0, figure('visible','off'), clf; end
    if ShowFigs==1, figure(1              ), clf; end
    subplot(311), set(gcf,'Color','white') %,colormap(flipud(roma))
    imagesc(xc*Lc/1e3, yc*Lc/1e3,(log10(sqrt(Eiic2)'*(1/tc)))),colorbar,axis image,title(['max Eii = ', num2str(max(sqrt(Eiic2(:))*(1/tc)), '%2.4e')]), caxis([-16 -14])
    ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex'), set(gca, 'ydir', 'normal')
    title(['$\dot{\epsilon}_{II}$  [s$^{-1}$] --- $t = $', num2str(time*tc / (1e6*365*24*3600), '%1.4e My')], 'interpreter', 'latex'), set(gca,'FontSize',15),
    subplot(312),set(gcf,'Color','white')
    imagesc(xc*Lc/1e3, yc*Lc/1e3,Tiic'*tauc/1e6),colorbar,axis image,title(['max tau = ', num2str(max(Tiic(:)*tauc), '%2.4e')]), caxis([20 300]) %caxis([20 450])
    ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex') , set(gca, 'ydir', 'normal')
    title(['$\tau_{II}$ [MPa]'], 'interpreter', 'latex'),set(gca, 'FontSize', 15)
    subplot(313), set(gcf,'Color','white')
    imagesc(xc*Lc/1e3, yc*Lc/1e3,Ptc'*tauc/1e6),colorbar,axis image,title(['max Ptc = ', num2str(max(Ptc(:)*tauc), '%2.4e')]), caxis([0 800])%caxis([0 1000])
    xlabel('$x$ [km]','FontSize', 15, 'interpreter', 'latex'),ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex') , set(gca, 'ydir', 'normal')
    title(['$P$ [MPa]'], 'interpreter', 'latex'),set(gca, 'FontSize', 15)
    if SaveFigs > 0 && mod(it,SaveFigs) == 0, print(['Output_res' num2str(nx,'%04d') '_step' num2str(it,'%04d')],'-dpng','-r300'); end
    if ShowFigs==1,
        figure(2),clf,colormap('jet'),set(gcf,'Color','white')
        plot(1:iter, log10(rxvec(1:iter)/rxvec(1)),'rx-')
        xlabel('Iterations'),ylabel('Residuals'),drawnow
    end  
    if SaveData > 0 && mod(it,SaveData) == 0
        save(['Data_res' num2str(nx,'%04d') '_step' num2str(it,'%04d')], 'xc', 'yc', 'Ptc', 'Tiic', 'Eiic2', 'Vx', 'Vy', 'tc', 'Tc', 'tauc', 'Lc', 'Eiic_e', 'Eiic_v', 'Eiic_vp', 'timev', 'stress', 'Txyv', 'Txxc', 'Tyyc', 'nitervec', 'rvec_abs', 'rvec_rel')
        save('Breakpoint');
    end
    cpu(8)=toc; cpu(9)=sum(cpu(1:7));
    display([' Time preprocess   = ', num2str(cpu(1))]);
    display([' Time BC           = ', num2str(cpu(2))]);
    display([' Time cst block    = ', num2str(cpu(3))]);
    display([' Time assemble     = ', num2str(cpu(4))]);
    display([' Time precondition = ', num2str(cpu(5))]);
    display([' Time solve        = ', num2str(cpu(6))]);
    display([' => WALLTIME      == ', num2str(cpu(9))]);
end
cpu(8)=toc; cpu(9)=sum(cpu(1:7));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    FUNCTIONS USED IN MAIN CODE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [alpha, LSsuccess] = LineSearch_Direct_pwl_vp(BC,nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,phic,phiv,Cc,Cv,Newton,noisy,resnlu0,resnlp0,etaec,etaev,Txxco,Tyyco,Txyco,Txxvo,Tyyvo,Txyvo,litmax,littol,rhogy,eta_vp,eta0_vp,n_vp)
% Line search explicit algorithm
nsteps = 6;                                   % number of steps
amin   = 0.05; 
if Newton==1, amax = 1.0; else amax = 2.0; end % maximum step
dalpha = (amax-amin)/(nsteps-1);
alphav = amin:dalpha:amax;
nVx    = (nx+1)*ny; nRMe   = zeros(nsteps,1); nRCTe  = zeros(nsteps,1);
u0 = u; p0 = p;
% Compute non linear residual for different steps
for ils=1:nsteps;
    % Primitive variables with correction step
    u       = u0 + alphav(ils).*du;
    p       = p0 + alphav(ils).*dp;
    % Evaluate non-Linear residual in matrix-free form
    Ptc     = reshape(p           ,[nx  ,ny  ]);
    Vx      = reshape(u(1:nVx)    ,[nx+1,ny  ]);
    Vy      = reshape(u(nVx+1:end),[nx  ,ny+1]);
    % Initial guess or iterative solution for strain increments
    divV        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
    Exxc        = diff(Vx,1,1)/dx - 1/3*divV;
    Eyyc        = diff(Vy,1,2)/dy - 1/3*divV;
    Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
    Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
    dVxdy       = diff(Vx_exp,1,2)/dy;
    dVydx       = diff(Vy_exp,1,1)/dx;
    Exyv        = 0.5*( dVxdy + dVydx );
    % Extrapolate trial strain components
    Exyc     = 0.25*(Exyv(1:end-1,1:end-1) + Exyv(2:end,1:end-1) + Exyv(1:end-1,2:end) + Exyv(2:end,2:end));
    [ Exxv ] = M2Di2_centroids2vertices( Exxc );
    [ Eyyv ] = M2Di2_centroids2vertices( Eyyc );
    [ Ptv  ] = M2Di2_centroids2vertices( Ptc  );
    % Viscosity
    mc   = 1/2*( 1./ndis(phc) - 1);
    mv   = 1/2*( 1./ndis(phv) - 1);
    Bc   = Adis(phc).^(-1./ndis(phc)) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
    Bv   = Adis(phv).^(-1./ndis(phv)) .* exp(Qdis(phv)/Rg./Tev./ndis(phv));
    [ etac, Txxc, Tyyc, Txyc, detadexxc, detadeyyc, detadexyc, plc, eta_vp1 ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxc, Eyyc, Exyc, Txxco, Tyyco, Txyco, Bc, mc, etaec, Ptc, phic, Cc, eta_vp,eta0_vp,n_vp  );
    [ etav, Txxv, Tyyv, Txyv, detadexxv, detadeyyv, detadexyv, plv          ] = M2Di2_LocalIteration_VEP_pwl_vp_v2( litmax, littol, noisy, Exxv, Eyyv, Exyv, Txxvo, Tyyvo, Txyvo, Bv, mv, etaev, Ptv, phiv, Cv, eta_vp,eta0_vp,n_vp  );
    % Momentum residual
    Txxc     = 2*etac.*Exxc + etac./etaec.*Txxco;
    Tyyc     = 2*etac.*Eyyc + etac./etaec.*Tyyco;
    Txyv     = 2*etav.*Exyv + etav./etaev.*Txyvo;
    % Momentum residual
    Res_x      = diff(-Ptc + Txxc,1,1)/dx + diff(Txyv(2:end-1,:),1,2)/dy;
    Res_y      = diff(-Ptc + Tyyc,1,2)/dy + diff(Txyv(:,2:end-1),1,1)/dx - rhogy(:,2:end-1);
    RMe        = [Res_x(:) ; Res_y(:)];
    nRMe(ils)  = norm(RMe)/((nx+1)*ny + (ny+1)*nx);
    % Continuity residual
    RCTe       = -(diff(Vx,1,1)/dx+diff(Vy,1,2)/dy);
    nRCTe(ils) = norm(RCTe(:))/(nx*ny);
    
    if ils == 1
        fprintf(' LS: it. = %02d\n', ils );
        fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ils), nRMe (ils)/resnlu0 );
        fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ils), nRCTe(ils)/resnlp0 );
    end
end
% Find optimal step (i.e. yielding to lowest residuals)
[~,ibestM] = min(nRMe);
if ibestM==1, LSsuccess=0; alpha=0; fprintf('No descent found - continuing ...')
    nRMe(1) = 2*max(nRMe); [~,ibestM] = min(nRMe);
    LSsuccess=1; alpha = alphav(ibestM);
else          LSsuccess=1; alpha = alphav(ibestM); end
if noisy>=1
    fprintf(' LS: Selected alpha = %2.2f\n', alpha );
    fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ibestM), nRMe (ibestM)/resnlu0 );
    fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ibestM), nRCTe(ibestM)/resnlp0 );
end
end
