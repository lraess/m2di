# M2Di2 

## 2D Visco-Elasto-Viscoplastic solver for crustal scale shear banding.

Copyright (C) 2019  Thibault Duretz, René de Borst, Laetitia Le Pourhiet

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

### Contact: ludovic.rass@gmail.com, thibault.duretz@univ-rennes1.fr
