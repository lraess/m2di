% 2D ELASTO-VISCO-PLASTIC M2Di
clear, clf, clc, %close all

rnd_seed = 0;
file = 'Noise_cohesion_1601_nstep500';

% pconf1 = 132435000;
%pconf1 = 132.435e6;
pconf1 = 250.00e6;

% Link to Suitesparse
SuiteSparse   = 0;           % Use Suitesparse routines
addpath('./../SuiteSparse/') % Link to your SuiteSparse routines
% Physics
xmin   =-10e3;
if rnd_seed == 0
    xmax   = 4.1e3;
else
    xmax   = 10e3;
end
ymax   =-pconf1/2700/9.81;
ymin   = ymax-10e3;
ymax = ymax + 400;
dt     = 2e9;%1e10;
eta0   = 1e50;       % Shear viscosity (DEACTIVATED)
etai   = 1e50;       % Inclusion shear viscosity (DEACTIVATED)
K0     = 2e10;        % Bulk modulus
G0     = 1e10;        % Shear modulus
Gi     = 1/4*G0;        % Inclusion shear modulus
rad    = 5e2;       % Inclusion radius
incr0  = 1e-5;        % Incremental displacement
coh0   = 10e6;         % Cohesion
phi0   = 30*pi/180;   % Friction angle
psi0   = 10*pi/180;   % Dilatancy angle
rho0   = 2700;        % Density
pconf  = 0.0e8;       % Confining pressure
gy     = -0*9.81;     % Vertical gravity component
vm     = 1;           % factor 3 in von Mises - set to 1 for Drucker-Prager
eta_vp = 1*1e19;
h      = -1e9;
Cmin   = 1e-3;
imp_hard = 1;
nout     = 20;
saveStiffness = 0;
% pconf1  = -(ymax)*rho0*9.81
fprintf('running with ref. Poisson ratio of %2.2f\n', (3*K0-2*G0) / 2 / (3*K0+G0));
fprintf('running with inc. Poisson ratio of %2.2f\n', (3*K0-2*Gi) / 2 / (3*K0+Gi));
% Numerics
nx            = 801;    % Number of nodes x
ny            = 801;    % Number of nodes y
ncx           = nx-1;   % Number of cells x
ncy           = ny-1;   % Number of cells y
ninc          = 5000;    % Number of increments
gitmax        = 50;     % Max. number of global iterations
tol_plast     = 1e-6;   % Tolerance of global iterations
nitPicNewt    = 0;      % Number of Picard steps before Newton
LineSearch    = 1;      % Activates line search
alpha         = 1.0;    % Default correction step
alpha_min     = 0.5;    % Minimum correction step
trial         = 2;      % 0: elastic trial -- 2: last guess
increase_step = 1;      % Allow for increasing increment
safe_mode     = 0;      % 0: Constant increment -- 1: Adaptative -- 2: Adaptative
linear_solver = 0;      % 0: Backslash -- 1: KSP_GCR
symmetry      = 0;      % Uses symmetrized elastic stiffness
saveRunData   = 1;      % Save simulation data to disk
restart       = 0;      % If restart == 1
from_step     = 1260;   % Then set from_step = last recorded step
strain_max    = 5e-2;
% Initialize
if SuiteSparse == 1, symmetry = 1; end
strain = 0; last_num_its = 0; sin_phi = sin(phi0); cos_phi = cos(phi0); sin_psi = sin(psi0);
Kv   =   K0*ones(nx ,ny ); % Elastic bulk modulus
Gv   =   G0*ones(nx ,ny ); % Elastic shear modulus
etav = eta0*ones(nx ,ny ); % Viscous shear modulus
Cv   = coh0*ones(nx ,ny ); % cohesion
phic = phi0*ones(ncx,ncy); % phi
phiv = phi0*ones(nx ,ny ); % phi
psic = psi0*ones(ncx,ncy); % psi
psiv = psi0*ones(nx ,ny ); % psi
Kc   =   K0*ones(ncx,ncy); % Elastic bulk modulus
Gc   =   G0*ones(ncx,ncy); % Elastic shear modulus
etac = eta0*ones(ncx,ncy); % Elastic shear modulus
Cc   = coh0*ones(ncx,ncy); % cohesion
Sxxc =     zeros(ncx,ncy); % Normal stress
Syyc =     zeros(ncx,ncy); % Normal stress
Txyc =     zeros(ncx,ncy); % Shear stress
Szzc =     zeros(ncx,ncy); % Normal stress
Sxxv =     zeros(nx ,ny ); % Normal stress
Syyv =     zeros(nx ,ny ); % Normal stress
Txyv =     zeros(nx ,ny ); % Shear stress
Szzv =     zeros(nx ,ny ); % Normal stress
Exxc =     zeros(ncx,ncy); % Normal strain
Eyyc =     zeros(ncx,ncy); % Normal strain
Exyc =     zeros(ncx,ncy); % Normal strain
Ezzc =     zeros(ncx,ncy); % Normal strain
Exxv =     zeros(nx ,ny ); % Normal strain
Eyyv =     zeros(nx ,ny ); % Normal strain
Ezzv =     zeros(nx ,ny ); % Normal strain
Ekkc =     zeros(ncx,ncy); % Normal strain
Exyv =     zeros(nx ,ny ); % Shear strain
Ux   =     zeros(nx ,ncy); % Horizontal displacement
Uy   =     zeros(ncx,ny ); % Vertical displacement
Fc1  =     zeros(ncx,ncy); %
Fv1  =     zeros(nx ,ny ); %
Fc   =     zeros(ncx,ncy); %
Fv   =     zeros(nx ,ny ); %
plc  =     zeros(ncx,ncy); % Plastic flag centroids
plv  =     zeros(nx ,ny ); % Plastic flag vertices
plc2 =     zeros(ncx,ncy); % Plastic flag centroids
plv2 =     zeros(nx ,ny ); % Plastic flag vertices
dgc  =     zeros(ncx,ncy); % Plastic increment centroids
dgv  =     zeros(nx ,ny ); % Plastic increment vertices
Eii  =     zeros(ncx,ncy); % accumulated strain
Ep_acc_c   = zeros(ncx,ncy); % accumulated strain
Ep_acc_v   = zeros(nx ,ny ); % accumulated strain
hc         = h*ones(ncx,ncy); % hardening/softening modulus
hv         = h*ones(nx ,ny ); % hardening/softening modulus
h1c         = h*ones(ncx,ncy); % hardening/softening modulus
h1v         = h*ones(nx ,ny ); % hardening/softening modulus
Ep_acc_c0  = Ep_acc_c;
Ep_acc_v0  = Ep_acc_v;
% For post-processing
Txxc   =    zeros(ncx,ncy);
Tyyc   =    zeros(ncx,ncy);
Tzzc   =    zeros(ncx,ncy);
Txxc0  =    zeros(ncx,ncy) - 1/2*pconf1;
Tyyc0  =    zeros(ncx,ncy) + 1/2*pconf1;
Txyc0  =    zeros(ncx,ncy);
Tzzc0  =    zeros(ncx,ncy);
Txxv0  =    zeros(nx ,ny ) - 1/2*pconf1;
Tyyv0  =    zeros(nx ,ny ) + 1/2*pconf1;
Txyv0  =    zeros(nx ,ny );
Tzzv0  =    zeros(nx ,ny );
Txyv   =    zeros(nx ,ny );
Exxdc  =    zeros(ncx,ncy); % Normal strain
Eyydc  =    zeros(ncx,ncy); % Normal strain
Ezzdc  =    zeros(ncx,ncy); % Normal strain
Exxdc0 =    zeros(ncx,ncy);
Eyydc0 =    zeros(ncx,ncy);
Ezzdc0 =    zeros(ncx,ncy);
Pc0    =    zeros(ncx,ncy) + pconf1;
Pv0    =    zeros(nx ,ny ) + pconf1;
% For monitoring
increment = zeros(ninc,1);
timevec   = zeros(ninc,1);
Pvec      = zeros(ninc,1);
Tiivec    = zeros(ninc,1);
epvec     = zeros(ninc,1);
strvec    = zeros(ninc,1);
nitervec  = zeros(ninc,1);
rxvec_rel = zeros(gitmax,1);    rxvec_abs = zeros(gitmax,1);
rvec_abs  = zeros(ninc,gitmax); rvec_rel  = zeros(ninc,gitmax);
% 2D model domain
Lx   = xmax - xmin;
Ly   = ymax - ymin;
dx   = Lx/(ncx);
dy   = Ly/(ncy);
incr0  =  dy*(1e-5/200);
xv = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;
yv = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;
[xv2,  yv2] = ndgrid(xv,yv);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xv,yc);
[xvy2,yvy2] = ndgrid(xc,yv);

if rnd_seed == 0
    % Initial configuration - Set perturbation of shear modulus
    y0  = 0.5*(ymin + ymax);
    x0 = xmin;
    y0 = ymin;
    Gc((xc2-x0).^2+(yc2-y0).^2<rad^2) = Gi;
    Kdiff     = 1e-4;
    dt_diff   = dx^2/Kdiff/4.1/5;
    diff_time = 2*2e7;
    nt_diff   = ceil(diff_time/dt_diff);
    dt_diff   = diff_time/nt_diff;
    for i=1:nt_diff
        Gc_x = [Gc(1,:); Gc; Gc(end,:)];
        Gc_y = [Gc(:,1), Gc, Gc(:,end)];
        Gc   =  Gc + Kdiff*dt_diff/dx^2 * diff(Gc_x,2,1) + Kdiff*dt_diff/dy^2 * diff(Gc_y,2,2);
    end
    fprintf('diffusion length %2.2e of jump over a duration of %2.2e and using %02d steps\n', sqrt(Kdiff*diff_time), nt_diff*dt_diff, nt_diff);
    [ Gv ] = M2Di_EP9_centroids2vertices( Gc );
end

if rnd_seed == 1
    anom   = detrend(cumsum(detrend(cumsum(coh0*0.0005*(rand(nx,ny)-0.5)))'))';
    % radius = exp(- ( (xv2 - Lx*.5)/(Lx/3) ).^2 ...
    %               - ( (yv2 - Ly*.2)/(Ly/6) ).^2 );
    radius  = 1;
    anom    = anom.*radius;
    % anom    = anom - min(anom(:));
    Cv      = Cv + anom;
    if saveRunData==1, save(['Noise_cohesion_', num2str(nx,'%3d'),'_nstep', num2str(ninc,'%3d')],'Cv','xv2','yv2'); end
elseif rnd_seed == 2
    noiseHR = load(file);
    ncxHR = 1600; ncyHR = 1600;
    dxHR   = Lx/(ncxHR);
    dyHR   = Ly/(ncyHR);
    noiseHR.xv = xmin:dxHR:xmax;
    noiseHR.yv = ymin:dyHR:ymax;
    [noiseHR.xv2,  noiseHR.yv2] = ndgrid(noiseHR.xv,noiseHR.yv);
    Fcoh = griddedInterpolant(noiseHR.xv2, noiseHR.yv2, noiseHR.Cv);
    Cv   = Fcoh(xv2,yv2);
    Cc   = 0.25*(Cv(1:end-1,1:end-1) + Cv(2:end,1:end-1) + Cv(1:end-1,2:end) + Cv(2:end,2:end)); % Yield stress
    
    K         = 1e-6;
    dt_diff   = dx^2/K/4.1/5;
    diff_time = 30;
    nt_diff   = ceil(diff_time/dt_diff);
    dt_diff   = diff_time/nt_diff;
    for i=1:nt_diff
        Cc_x = [Cc(1,:); Cc; Cc(end,:)];
        Cc_y = [Cc(:,1), Cc, Cc(:,end)];
        Cc   =  Cc + K*dt_diff/dx^2 * diff(Cc_x,2,1) + K*dt_diff/dy^2 * diff(Cc_y,2,2);
    end
    fprintf('diffusion of jump over a duration of %2.2f and using %02d steps\n', nt_diff*dt_diff, nt_diff);
    [ Cv ] = M2Di_EP9_centroids2vertices( Cc );
end


% Elasto-viscous moduli
Gvev  = 1./(dt./etav + 1./Gv);
Gvec  = 1./(dt./etac + 1./Gc);
VEv   = Gvev./Gv;
VEc   = Gvec./Gc;
% Initial old stresses
time  = 0;
Exxc0 = Exxc; Exxv0 = Exxv;
Eyyc0 = Eyyc; Eyyv0 = Eyyv;
Exyv0 = Exyv; Exyc0 = Exyc;
Ezzc0 = Ezzc; Ezzv0 = Ezzv;
Ux0   = Ux;
Uy0   = Uy;
%% Numbering Pt and Vx,Vy
NumUx  = reshape(1:nx*ncy,nx,ncy  );
NumUy  = reshape(1:ncx*ny,ncx  ,ny); NumUyG = NumUy + max(NumUx(:));
% Free slip / no slip setting for x momentum
BC.nsxS    =  zeros(size(Ux)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Ux)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Ux)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Ux)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Ux)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Ux)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Ux)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Ux)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
% Free slip / no slip setting for y momentum
BC.nsyW    =  zeros(size(Uy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Uy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Uy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Uy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Uy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Uy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Uy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Uy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
% Free surface
BC.frxN    =  zeros(size(Ux)); %BC.frxN(2:end-1,end) = 1;
BC.fryN    =  zeros(size(Uy)); %BC.fryN( :     ,end) = 1;
%% Boundary Conditions on velocities tangential to boundaries (non-conforming)
BC.Ux_S =   -incr0*xv';
BC.Ux_N =   -incr0*xv';
BC.Uy_W =    incr0*yv';
BC.Uy_E =    incr0*yv';
% Incremental BC displacements
BC.Ux_W   = -incr0.*xvx2(1  ,:)'.*ones(1 ,ncy  )';
BC.Ux_E   = -incr0.*xvx2(end,:)'.*ones(1 ,ncy  )';
BC.Uy_S   =  incr0.*yvy2(:,  1) .*ones(ncx,1   );
BC.Uy_N   =  incr0.*yvy2(:,end) .*ones(ncx,1   );
% Assemble elastic matrix and RHS
% [Ke, BcK] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cv,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc  ,dgv  ,Sxxc,  Syyc,  Txyc  ,Szzc,  Sxxv,  Syyv,  Txyv,  Szzv,  dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,     0,vm, BC,sin_phi,sin_psi, cos_phi, coh0, symmetry,rho0,gy, 0, 0, 0, h, eta_vp, dt, 0, 0, 0 );
[Ke, BcK] = M2Di_EP13_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc,Syyc,Txyc,Szzc,Sxxv,Syyv,Txyv,Szzv,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,0,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , -Pc0+Txxc0, -Pc0+Tyyc0, Txyv0, h, eta_vp, dt, 0, 0, 0, 0, 0 );
if symmetry == 1
    % Cholesky
    [Kf, nok, p]     = chol(Ke,'lower','vector');
    if nok ~= 0, error('Cholesky failed'); end
    dFs      = BcK;
    dU(p,1)  = cs_ltsolve(Kf,cs_lsolve(Kf,dFs(p)));
else
    dU = Ke\BcK;
end
dUx_e    = dU(NumUx);
dUy_e    = dU(NumUyG);
ddU      = 0*dU;       % Initialise correction vector
%------------------------ Incremental loop ------------------------ %
inc       = 0;
plastic   = 0;
success   = 0;
finish    = 0;
incr0_ini = incr0;
if restart==1, load(['run_M2Di_EP_step', num2str(from_step, '%04d'), '.mat']); end
while inc<ninc && finish == 0
    inc     = inc + 1;
    fail    = 0;
    new_BC  = 0;
    plc     =  zeros(ncx,ncy); % Plastic flag centroids
    plv     =  zeros(nx ,ny ); % Plastic flag vertices
    dgc     =  zeros(ncx,ncy); % Plastic correction centroids
    dgv     =  zeros(nx ,ny ); % Plastic correction vertices
    dQdsxxc = zeros(ncx,ncy);
    dQdsyyc = zeros(ncx,ncy);
    dQdszzc = zeros(ncx,ncy);
    dQdsxyc = zeros(ncx,ncy);
    dQdsxxv = zeros(nx,ny);
    dQdsyyv = zeros(nx,ny);
    dQdszzv = zeros(nx,ny);
    dQdsxyv = zeros(nx,ny);
    gamdotc = zeros(ncx,ncy);
    gamdotv = zeros(nx,ny);
    depc    = zeros(ncx,ncy);
    depv    = zeros(nx,ny);
    % Accumulated strain
    Ep_acc_c0  = Ep_acc_c;
    Ep_acc_v0  = Ep_acc_v;
    Cc0        = Cc;
    Cv0        = Cv;
    % Total Strain increment - the trial stress is elastic
    if plastic == 0
        fprintf('ELASTIC TRIAL\n');
        dUxc_t = dUx_e;
        dUyc_t = dUy_e;
    else
        if success == 0 %&& safe_mode > 1,
            incr0 = incr0/1.5;
            new_BC = 1;
        elseif success == -1
            LineSearch = 1;
        else
            fprintf('USING OLD EP SOLUTION AS INITIAL GUESS\n');
            if increase_step==1 && incr0<incr0_ini && success == 1,
                incr0 = incr0*1.25;
                if incr0>incr0_ini, incr0=incr0_ini; end
                new_BC = 1;
            end
        end
        if new_BC == 1
            fprintf('USING NEW ELASTIC SOLUTION (increment = %2.2e) \n',incr0);
            % Incremental BC displacements
            BC.Ux_W   = -incr0.*xvx2(1  ,:)'.*ones(1 ,ncy  )';
            BC.Ux_E   = -incr0.*xvx2(end,:)'.*ones(1 ,ncy  )';
            BC.Uy_S   =  incr0.*yvy2(:,  1) .*ones(ncx,1   );
            BC.Uy_N   =  incr0.*yvy2(:,end) .*ones(ncx,1   );
            BC.Vx_S =   -incr0*xv';
            BC.Vx_N =   -incr0*xv';
            BC.Vy_W =    incr0*yv';
            BC.Vy_E =    incr0*yv';
            % Assemble elastic matrix and RHS
%             [Ke, BcK] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc,Syyc,Txyc,Szzc,Sxxv,Syyv,Txyv,Szzv,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,0,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy, 0, 0, 0, h, eta_vp, dt, syc, syv, 0 );
            [Ke, BcK] = M2Di_EP13_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc,Syyc,Txyc,Szzc,Sxxv,Syyv,Txyv,Szzv,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,0,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , -Pc0+Txxc0, -Pc0+Tyyc0, Txyv0, h, eta_vp, dt, 0, 0, 0, 0, 0 );
            % ELASTIC PREDICATOR
            dFs     = BcK;
            if symmetry == 0, dU      = Ke\BcK;                              end
            if symmetry == 1, dU(p,1) = cs_ltsolve(Kf,cs_lsolve(Kf,dFs(p))); end
            dUx_e   = dU(NumUx);
            dUy_e   = dU(NumUyG);
            dUxc_t  = dUx_e;
            dUyc_t  = dUy_e;
            fail    = 1;
        end
    end
    fprintf('\n#######################################\n');
    fprintf(  '########## Loading step %04d ##########\n', inc);
    fprintf(  '#######################################\n');
    dU        = [dUxc_t(:); dUyc_t(:)];
    rxvec_rel = zeros(gitmax,1);    rxvec_abs = zeros(gitmax,1);    corvec    = zeros(gitmax,1);    alpvec    = zeros(gitmax,1);
    % ------------------------ Global equilibrium: Newton iterations ------------------------ %
    Newton = 0;
    LSsuccess = 1; LIsuccessc=1; LIsuccessv = 1;
    for plast_it = 1:gitmax
        if plast_it>nitPicNewt,  Newton=2; end
        % Initial guess or iterative solution for strain increments
        dExxc_t  = diff(dUxc_t,1,1)/dx;
        dEyyc_t  = diff(dUyc_t,1,2)/dy;
        dEzzc_t  = 0; dEzzv_t  = 0;
        dUx_exp  = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*dUxc_t(:,1) + BC.fsxS(:,1).*dUxc_t(:,1), dUxc_t, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*dUxc_t(:,end) + BC.fsxN(:,end).*dUxc_t(:,end)];
        dUy_exp  = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*dUyc_t(1,:) + BC.fsyW(1,:).*dUyc_t(1,:); dUyc_t; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*dUyc_t(end,:) + BC.fsyE(end,:).*dUyc_t(end,:)];
        dUxdy    = diff(dUx_exp,1,2)/dy;
        dUydx    = diff(dUy_exp,1,1)/dx;
        dExyv_t  = 0.5*( dUxdy + dUydx );
        % Extrapolate trial strain components
        dExyc_t     = 0.25*(dExyv_t(1:end-1,1:end-1) + dExyv_t(2:end,1:end-1) + dExyv_t(1:end-1,2:end) + dExyv_t(2:end,2:end));
        [ dExxv_t ] = M2Di_EP9_centroids2vertices( dExxc_t );
        [ dEyyv_t ] = M2Di_EP9_centroids2vertices( dEyyc_t );
        % Strains
        Exxc_t = Exxc + dExxc_t;
        Eyyc_t = Eyyc + dEyyc_t;
        Exyv_t = Exyv + dExyv_t;
        Ezzc_t = Ezzc + 0;
        % Save
        dExxc_0 = dExxc_t;  dExxv_0 = dExxv_t;
        dEyyc_0 = dEyyc_t;  dEyyv_0 = dEyyv_t;
        dExyc_0 = dExyc_t;  dExyv_0 = dExyv_t;
        dEzzc_0 = dEzzc_t;  dEzzv_0 = dEzzv_t;
        % Total stress increments
        dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
        dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
        dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
        dTxyv = 2*Gvev.*dExyv_t ;
        dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
        dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
        dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
        dTxyc = 2*Gvec.*dExyc_t;
        % Total stresses
        dPc    =-1/3*(dSxxc + dSyyc + dSzzc);
        dPv    =-1/3*(dSxxv + dSyyv + dSzzv);
        Sxxc_t = VEc .* Txxc0 - Pc0 + dSxxc;
        Syyc_t = VEc .* Tyyc0 - Pc0 + dSyyc;
        Txyv_t = VEv .* Txyv0       + dTxyv;
        Szzc_t = VEc .* Tzzc0 - Pc0 + dSzzc;
        Sxxv_t = VEv .* Txxv0 - Pv0 + dSxxv;
        Syyv_t = VEv .* Tyyv0 - Pv0 + dSyyv;
        Txyc_t = VEc .* Txyc0       + dTxyc;
        Szzv_t = VEv .* Tzzv0 - Pv0 + dSzzv;
        Pc     = Pc0 + dPc;
        Pv     = Pv0 + dPv;
        Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
        Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
        J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
        J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
        
        
        Cc     = Cc0;% + imp_hard*hc.*Ep_acc_c0;
        Cv     = Cv0;% + imp_hard*hv.*Ep_acc_v0;
        syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
        syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
        Fc     =  sqrt(vm*J2c) - syc;
        Fv     =  sqrt(vm*J2v) - syv;
        fprintf('max. trial     Fc = %2.2e\n', max(Fc(:)))
        fprintf('max. trial     Fv = %2.2e\n', max(Fv(:)))
        J2c_t = J2c; J2v_t = J2v; Pc_t = Pc; Pv_t = Pv;
        % Plastic corrections
        if max(Fc(:))>0 || max(Fv(:))>0
            % Plastic flags
            plc = Fc>0;
            plv = Fv>0;
            % dQds centers
            txx = Pc+Sxxc_t; tyy = Pc+Syyc_t; txy = Txyc_t; tzz = Pc+Szzc_t; J2 = J2c;
            dQdsxxc = vm * txx .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsyyc = vm * tyy .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsxyc = 0.1000000000e1 * vm * txy .* (vm * J2) .^ (-0.1e1 / 0.2e1);
            dQdszzc = vm * tzz .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            % dQds vertices
            txx = Pv+Sxxv_t; tyy = Pv+Syyv_t; txy = Txyv_t; tzz = Pv+Szzv_t; J2 = J2v;
            dQdsxxv = vm * txx .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsyyv = vm * tyy .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsxyv = 0.1000000000e1 * vm * txy .* (vm * J2) .^ (-0.1e1 / 0.2e1);
            dQdszzv = vm * tzz .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            % dgamma
            h1c = cos_phi.*hc.*sqrt(2/3).*sqrt((dQdsxxc.^2 + 2*dQdsxyc.^2 + dQdsyyc.^2 + dQdszzc.^2));
            h1v = cos_phi.*hv.*sqrt(2/3).*sqrt((dQdsxxv.^2 + 2*dQdsxyv.^2 + dQdsyyv.^2 + dQdszzv.^2));
            dgc = plc.*(Fc./(vm.*Gvec + Kc.*sin_phi*sin_psi + eta_vp/dt + h1c ));
            dgv = plv.*(Fv./(vm.*Gvev + Kv.*sin_phi*sin_psi + eta_vp/dt + h1v ));
            % Corrected strain on centers
            dExxc_t = dExxc_0 - plc.*dgc.*dQdsxxc;
            dEyyc_t = dEyyc_0 - plc.*dgc.*dQdsyyc;
            dExyc_t = dExyc_0 - plc.*dgc.*dQdsxyc/2;
            dEzzc_t = dEzzc_0 - plc.*dgc.*dQdszzc;
            % Corrected strain on vertices
            dExxv_t = dExxv_0 - plv.*dgv.*dQdsxxv;
            dEyyv_t = dEyyv_0 - plv.*dgv.*dQdsyyv;
            dExyv_t = dExyv_0 - plv.*dgv.*dQdsxyv/2;
            dEzzv_t = dEzzv_0 - plv.*dgv.*dQdszzv;
            % Total stress increments
            dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
            dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
            dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
            dTxyv = 2*Gvev.*dExyv_t ;
            dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
            dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
            dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
            dTxyc = 2*Gvec.*dExyc_t;
            % Total stresses
            dPc     =-1/3*(dSxxc + dSyyc + dSzzc);
            dPv     =-1/3*(dSxxv + dSyyv + dSzzv);
            Sxxc_t = VEc .* Txxc0 - Pc0 + dSxxc;
            Syyc_t = VEc .* Tyyc0 - Pc0 + dSyyc;
            Txyv_t = VEv .* Txyv0       + dTxyv;
            Szzc_t = VEc .* Tzzc0 - Pc0 + dSzzc;
            Sxxv_t = VEv .* Txxv0 - Pv0 + dSxxv;
            Syyv_t = VEv .* Tyyv0 - Pv0 + dSyyv;
            Txyc_t = VEc .* Txyc0       + dTxyc;
            Szzv_t = VEv .* Tzzv0 - Pv0 + dSzzv;
            Pc     = Pc0 + dPc;
            Pv     = Pv0 + dPv;
            Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
            Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
            J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
            J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
            
            %             % Update trial plastic strain
            %             dexxp = plc.*dgc.*dQdsxxc/dt; deyyp = plc.*dgc.*dQdsyyc/dt; dexyp = plc.*dgc.*dQdsxyc/2/dt; dezzp = plc.*dgc.*dQdszzc/dt;
            %             Ep_dot_c = sqrt(dexxp.^2 + deyyp.^2 + 2*dexyp.^2 + dezzp.^2);
            %             dexxp = plv.*dgv.*dQdsxxv/dt; deyyp = plv.*dgv.*dQdsyyv/dt; dexyp = plv.*dgv.*dQdsxyv/2/dt; dezzp = plv.*dgv.*dQdszzv/dt;
            %             Ep_dot_v = sqrt(dexxp.^2 + deyyp.^2 + 2*dexyp.^2 + dezzp.^2);
            %             % Update trial viscoplastic strain
            %             dexxp = plc.*gamdotc.*dQdsxxc; deyyp = plc.*gamdotc.*dQdsyyc; dexyp = plc.*gamdotc.*dQdsxyc/2; dezzp = plc.*gamdotc.*dQdszzc;
            %             Evp_dot_c = sqrt(dexxp.^2 + deyyp.^2 + dexyp.^2 + dezzp.^2);
            %             dexxp = plv.*gamdotv.*dQdsxxv; deyyp = plv.*gamdotv.*dQdsyyv; dexyp = plv.*gamdotv.*dQdsxyv/2; dezzp = plv.*gamdotv.*dQdszzv;
            %             Evp_dot_v = sqrt(dexxp.^2 + deyyp.^2 + dexyp.^2 + dezzp.^2);
            
            %             depc = dgc.*sqrt(2/3)*sqrt((dQdsxxc.^2 + 2*dQdsxyc.^2 + dQdsyyc.^2 + dQdszzc.^2));
            %             depv = dgv.*sqrt(2/3)*sqrt((dQdsxxv.^2 + 2*dQdsxyv.^2 + dQdsyyv.^2 + dQdszzv.^2));
            depc     = sqrt(2/3).*sqrt((dgc.*dQdsxxc).^2+(dgc.*dQdsyyc).^2+(dgc.*dQdszzc).^2+2*(dgc.*dQdsxyc).^2);
            depv     = sqrt(2/3).*sqrt((dgv.*dQdsxxv).^2+(dgv.*dQdsyyv).^2+(dgv.*dQdszzv).^2+2*(dgv.*dQdsxyv).^2);
            Cc     = Cc0 + imp_hard*hc.*depc;
            Cv     = Cv0 + imp_hard*hv.*depv;
            syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
            syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
            Fc     =  sqrt(vm*J2c) - syc;
            Fv     =  sqrt(vm*J2v) - syv;
            fprintf('    Backbone EP max. Fc = %2.2e \n', max(Fc(:)))
            fprintf('    Backbone EP max. Fv = %2.2e \n', max(Fv(:)))
            syc     = Cc.*cos_phi + (Pc+pconf).*sin_phi;
            syv     = Cv.*cos_phi + (Pv+pconf).*sin_phi;
            gamdotc = plc.*(sqrt(vm*J2c) - syc) ./ eta_vp;
            gamdotv = plv.*(sqrt(vm*J2v) - syv) ./ eta_vp;
            %             Fc_vp   =  sqrt(vm*J2c) - syc - gamdotc.*eta_vp;
            %             Fv_vp   =  sqrt(vm*J2v) - syv - gamdotv.*eta_vp;
            Fc_vp   =  sqrt(vm*J2c) - syc - (dgc./dt).*eta_vp;
            Fv_vp   =  sqrt(vm*J2v) - syv - (dgv./dt).*eta_vp;
            fprintf('    Viscoplast. max. Fc = %2.2e \n', max(Fc_vp(:)))
            fprintf('    Viscoplast. max. Fv = %2.2e \n', max(Fv_vp(:)))
            
            %             figure(12),clf
            %             subplot(211)
            %             imagesc(xc,yc,Ep_dot_c), colorbar
            %             subplot(212)
            %             imagesc(xc,yc,gamdotc), colorbar
            %             drawnow
            
        end
        % Evaluate global residual
        Rx = [zeros(1,ncy); (diff(Sxxc_t,1,1)/dx + diff(Txyv_t(2:end-1,:),1,2)/dy)          ; zeros(1,ncy)];
        Ry = [zeros(ncx,1), (diff(Syyc_t,1,2)/dy + diff(Txyv_t(:,2:end-1),1,1)/dx) + rho0*gy, zeros(ncx,1)];
        R  = [Rx(:); Ry(:)];
        nR = norm(R,1)/length(R);
        if plast_it==1, nR0 = nR; end
        fprintf('Iteration %03d ||F|| = %2.8e - ||F0|| = %2.8e - ||F/F0|| = %2.8e - Newton = %d\n', plast_it, nR, nR0, nR/nR0, Newton);
        rxvec_abs(plast_it) = nR;
        rxvec_rel(plast_it) = nR/nR0;
        % Exit if return mapping failed
        if LIsuccessc==0 || LIsuccessv==0 || nR/nR0>1e3, break; end
        % Exit if convergence criteria is met
        if nR<tol_plast
            break;
        else
            % Identify plastic nodes
            plastic = 1;
            % Assemble elasto-plastic tangent matrix
            if Newton>0
                [Kep,~] = M2Di_EP13_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , -Pc0+Txxc0, -Pc0+Tyyc0, Txyv0, h, eta_vp, dt, syc, syv, 0, h1c, h1v );
            end
            if Newton == 0
                % Picard step
                ddU(p,1)  = cs_ltsolve(Kf,cs_lsolve(Kf,R(p)));
                %             elseif Newton == 1
                %                 [Kepc,~] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , Sxxc0, Syyc0, Txyv0, h, eta_vp, dt, syc, syv, 1 );
                %                 [Ksf, nok, p] = chol(Kepc,'lower','vector');
                %                 % Picard step
                %                 ddU(p,1)  = cs_ltsolve(Ksf,cs_lsolve(Ksf,R(p)));
            elseif Newton == 1 || Newton == 2
                % Newton step
                tic
                if linear_solver == 0
                    ddU       = Kep\R;
                    fprintf('BACKSLASH solve took = %2.2e s\n', toc)
                else
                    % Preconditioner
                    [Kep,~] = M2Di_EP13_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , -Pc0+Txxc0, -Pc0+Tyyc0, Txyv0, h, eta_vp, dt, syc, syv, 1, h1c, h1v );                    
                    %Kepc = 1/2*(Kepc + Kepc');
                    Kepc = Kepc;% -0*diag(diag(Kepc)) + diag(diag(Ke));
                    %                     [i,j]=min(diag(Kepc))
                    %                     Kepc(j,:)
                    %                     Ke(j,:)
                    %                     max(diag(Kepc))
                    %                     figure(2), clf, spy(Kepc-Kepc')
                    %                     Kdiff = (Ke-Ke');
                    %                     Kdiff(Kdiff<1e-10) = 0;
                    % Cholesky
                    [Ksf, nok, p] = chol(Kepc,'lower','vector');
                    if nok ~= 0, error('Cholesky failed'); end
                    ddU           = M2Di_EP5_kspgcr_m(Kep,R,ddU,Ksf,p,tol_plast,3,SuiteSparse);
                    fprintf('KSP_GCR solve took = %2.2e s\n', toc)
                end
            end
            % Line search algorithm
            if LineSearch == 1
                [alpha,LSsuccess] = M2Di_EP13_LineSearchEP(dU,ddU,NumUx,NumUyG,Gvec,VEc,Kc,Cc,phic,psic,Gvev,VEv,Kv,Cv,phiv,psiv,dx,dy,ncx,ncy,Pc0,Pv0,Txxc0,Tyyc0,Txyc0,Tzzc0,Txxv0,Tyyv0,Txyv0,Tzzv0, vm, BC, sin_phi,cos_phi,sin_psi, safe_mode, nR, rho0,gy,pconf, eta_vp, dt, alpha_min, Ep_acc_c0, Ep_acc_v0, hc, hv, Cc0, Cv0, imp_hard );
            end
            if LSsuccess == 1
                dU     = dU + alpha*ddU;
                dUxc_t = dU(NumUx);
                dUyc_t = dU(NumUyG);
                nC     = norm(ddU)/length(ddU);
            else
                % If line search failed: exit
                plast_it = gitmax;
                break;
            end
            
        end
        alpvec(plast_it) = alpha;
        corvec(plast_it) = nC;
    end
    if  nR<tol_plast
        % Update total strains and displacements
        Exxc = Exxc_t;
        Eyyc = Eyyc_t;
        Ezzc = Ezzc_t;
        Exyv = Exyv_t;
        Ekkc = Exxc_t + Eyyc_t + Ezzc_t;
        dExxdc_0 = dExxc_0 - 1/3*(dExxc_0+dEyyc_0);
        dEyydc_0 = dEyyc_0 - 1/3*(dExxc_0+dEyyc_0);
        dEzzdc_0 = dEzzc_0 - 1/3*(dExxc_0+dEyyc_0);
        dExydv_0 = dExyv_0;
        Ux   = Ux0 + dUxc_t;
        Uy   = Uy0 + dUyc_t;
        Ux0  = Ux;
        Uy0  = Uy;
        
        % Update trial plastic strain
%         dexxp = plc.*dgc.*dQdsxxc/dt; deyyp = plc.*dgc.*dQdsyyc/dt; dexyp = plc.*dgc.*dQdsxyc/2/dt; dezzp = plc.*dgc.*dQdszzc/dt;
%         Ep_dot_c = sqrt(dexxp.^2 + deyyp.^2 + dexyp.^2 + dezzp.^2);
%         dexxp = plv.*dgv.*dQdsxxv/dt; deyyp = plv.*dgv.*dQdsyyv/dt; dexyp = plv.*dgv.*dQdsxyv/2/dt; dezzp = plv.*dgv.*dQdszzv/dt;
%         Ep_dot_v = sqrt(dexxp.^2 + deyyp.^2 + dexyp.^2 + dezzp.^2);
        
%         depc     = sqrt(2/3).*sqrt((dgc.*dQdsxxc).^2+(dgc.*dQdsyyc).^2+(dgc.*dQdszzc).^2+2*(dgc.*dQdsxyc).^2);
%         depv     = sqrt(2/3).*sqrt((dgv.*dQdsxxv).^2+(dgv.*dQdsyyv).^2+(dgv.*dQdszzv).^2+2*(dgv.*dQdsxyv).^2);
        Ep_acc_c = Ep_acc_c0 + depc;
        Ep_acc_v = Ep_acc_v0 + depv;
%         
%         Ep_acc_c = Ep_acc_c0   + (1-imp_hard)*dt*sqrt(2/3)*Ep_dot_c;
%         Ep_acc_v = Ep_acc_v0   + (1-imp_hard)*dt*sqrt(2/3)*Ep_dot_v;
        % Softening (explicit form)
        if imp_hard==0
            Cc     = Cc0 + hc.*depc ;
            Cv     = Cv0 + hv.*depv ;
        end
        
        
         % limit softening
        hc(Cc<=Cmin) = 0;
        hv(Cv<=Cmin) = 0;
        Cc(Cc<=Cmin) = Cmin;
        Cv(Cv<=Cmin) = Cmin;
        
        strain = strain + incr0;
        time   = time + dt;
        timevec(inc)    = time;
        increment(inc)  = incr0;
        Tiivec(inc)     = mean(sqrt(vm*J2c(:)));
        strvec(inc)     = strain;
        rvec_rel(inc,:) = rxvec_rel(:);
        rvec_abs(inc,:) = rxvec_abs(:);
        nitervec(inc)   = plast_it;
        Pvec(inc)       = Pc(fix(nx/2), fix(ny/2));
        epvec(inc)      = norm( Ep_acc_c );
        % Post-processing
        % Deviatoric strain invariant
        Exxdc = Exxc - 1/3*Ekkc;
        Eyydc = Eyyc - 1/3*Ekkc;
        Ezzdc = Ezzc - 1/3*Ekkc;
        Eii   = M2Di_EP10_InvariantOnCentroids(Exxdc   , Eyydc,   Ezzdc,   Exyv   );
        % Deviatoric strain rate invariant
        Exxcr = (Exxc-Exxc0)/dt; Eyycr = (Eyyc-Eyyc0)/dt; Ezzcr = (Ezzc-Ezzc0)/dt; Exyvr = (Exyv-Exyv0)/dt;
        Ekkcr = Exxcr + Eyycr + Ezzcr;
        Exxdr = Exxcr - 1/3*Ekkcr;
        Eyydr = Eyycr - 1/3*Ekkcr;
        Ezzdr = Ezzcr - 1/3*Ekkcr;
        Eiir  = M2Di_EP10_InvariantOnCentroids(Exxdr   , Eyydr,   Ezzdr,   Exyvr   );
        % Deviatoric strain rates decomposed
        Ekkp    = dgc.*(dQdsxxc + dQdsyyc + dQdszzc);
        Exxcr   = dExxdc_0/dt;                     Eyycr   = dEyydc_0/dt;                     Ezzcr   = dEzzdc_0/dt;                     Exyvr   = dExyv_0/dt;
        Exxcr_e = (Txxc-Txxc0)./2./Gc/dt;          Eyycr_e = (Tyyc-Tyyc0)./2./Gc/dt;          Ezzcr_e = (Tzzc-Tzzc0)./2./Gc/dt;          Exyvr_e = (Txyv-Txyv0)./2./Gv/dt;
        Exxcr_v = Txxc./2./etac;                   Eyycr_v = Tyyc./2./etac;                   Ezzcr_v = Tzzc./2./etac;                   Exyvr_v = Txyv./2./etav;
        Exxcr_p = 1/dt*(dgc.*dQdsxxc - 1/3.*Ekkp); Eyycr_p = 1/dt*(dgc.*dQdsyyc - 1/3.*Ekkp); Ezzcr_p = 1/dt*(dgc.*dQdszzc - 1/3.*Ekkp); Exyvr_p = 1/dt*dgv.*dQdsxyv / 2;
        Eiicr   = M2Di_EP10_InvariantOnCentroids(Exxcr   , Eyycr,   Ezzcr,   Exyvr   );
        Eiicr_e = M2Di_EP10_InvariantOnCentroids(Exxcr_e , Eyycr_e, Ezzcr_e, Exyvr_e );
        Eiicr_v = M2Di_EP10_InvariantOnCentroids(Exxcr_v , Eyycr_v, Ezzcr_v, Exyvr_v );
        Eiicr_p = M2Di_EP10_InvariantOnCentroids(Exxcr_p , Eyycr_p, Ezzcr_p, Exyvr_p );
        Exx_net = Exxcr-Exxcr_e-Exxcr_v-Exxcr_p;
        Eyy_net = Eyycr-Eyycr_e-Eyycr_v-Eyycr_p;
        Ezz_net = Ezzcr-Ezzcr_e-Ezzcr_v-Ezzcr_p;
        Exy_net = Exyvr-Exyvr_e-Exyvr_v-Exyvr_p;
        Eii_net = M2Di_EP10_InvariantOnCentroids(Exx_net   , Eyy_net,   Ezz_net,   Exy_net   );
        % Update old stresses
        Pc0 = Pc; Pv0 = Pv;
        Sxxc0 = Sxxc_t; Sxxv0 = Sxxv_t;
        Syyc0 = Syyc_t; Syyv0 = Syyv_t;
        Txyv0 = Txyv_t; Txyc0 = Txyc_t;
        Szzc0 = Szzc_t; Szzv0 = Szzv_t;
        Txxc0 = Txxc  ; Tyyc0 = Tyyc  ; Tzzc0 = Tzzc;
        Txxv0 = Txxv  ; Tyyv0 = Tyyv  ; Tzzv0 = Tzzv;
        Exxdc0 = Exxdc;Eyydc0 = Eyydc;Ezzdc0 = Ezzdc; Exyv0 = Exyv;
        last_num_its = plast_it;
        success = 1;
    else
        % Then try with smaller increment
        success = 0;
        if (nR/nR0)>100; success=0; end
        inc = inc-1;
    end
    
    % Visualize (only after elasto-plastic iterations)
    if plast_it>1 && success==1
        
        Emin = 0.001;
        Emax = 0.005;
        
        Pmin =-6e6;
        Pmax = 6e6;
        
        figure(2), clf
        subplot(211),
        imagesc(xc, yc, log10(abs(Eii'))), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. Eii = ', num2str(min(Eii(:))), ' max. Eii = ', num2str(max(Eii(:)))]), colormap('jet')
        %         caxis([log10(Emin) log10(Emax)])
        subplot(212),
        imagesc(xc, yc, (abs(Pc'))), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. P = ', num2str(min(Pc(:))), ' max. P = ', num2str(max(Pc(:)))]), colormap('jet')
        %         caxis([(Pmin) (Pmax)])
        if saveRunData==1 && mod(inc,nout)==0, print(['Strain',num2str(inc,'%04d')], '-r300','-dpng'); end
        
        
        figure(4), clf
        subplot(211),
        imagesc(xc, yc, Cc'), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. C = ', num2str(min(Cc(:))), ' max. C = ', num2str(max(Cc(:)))]), colormap('jet')
        subplot(212),
        imagesc(xc, yc, hc'), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. h = ', num2str(min(hc(:))), ' max. h = ', num2str(max(hc(:)))]), colormap('jet')
        
        
        
        %         figure(5), clf
        %         subplot(511)
        %         imagesc(xc,yc,log10(Eiicr)'), colorbar, axis image, set(gca, 'ydir', 'normal')
        %         title('Total deviatoric strain rate invariant')
        %         subplot(512)
        %         imagesc(xc,yc,log10(Eiicr_e)'), colorbar, axis image, set(gca, 'ydir', 'normal')
        %         title('Elastic deviatoric strain rate invariant')
        %         subplot(513)
        %         imagesc(xc,yc,log10(Eiicr_v)'), colorbar, axis image, set(gca, 'ydir', 'normal')
        %         title('Viscous deviatoric strain rate invariant')
        %         subplot(514)
        %         imagesc(xc,yc,log10((Eiicr_p))'), colorbar, axis image, set(gca, 'ydir', 'normal')
        %         title('Plastic deviatoric strain rate invariant')
        %         subplot(515)
        %         Exx_net = Exxcr-Exxcr_e-Exxcr_v-Exxcr_p;
        %         Eyy_net = Eyycr-Eyycr_e-Eyycr_v-Eyycr_p;
        %         Ezz_net = Ezzcr-Ezzcr_e-Ezzcr_v-Ezzcr_p;
        %         Exy_net = Exyvr-Exyvr_e-Exyvr_v-Exyvr_p;
        %         Eii_net = M2Di_EP10_InvariantOnCentroids(Exx_net   , Eyy_net,   Ezz_net,   Exy_net   );
        %         imagesc(xc,yc,log10(abs(Eii_net))'), colorbar, axis image, set(gca, 'ydir', 'normal')
        %         title('Net deviatoric strain rate invariant')
        
        figure(3), clf
        subplot(411)
        plot(strvec(1:inc),log10(abs(increment(1:inc))), 'xb')
        title('Increment vs. strain')
        subplot(412), hold on
        plot(strvec(1:inc),Tiivec(1:inc), 'dr')
        title('Stress vs. strain')
        subplot(413)
        plot(1:plast_it, log10(rxvec_abs(1:plast_it)),'ok')
        title('Global residual vs. iterations')
        subplot(414), hold on
        plot(strvec(1:inc),epvec(1:inc), 'dr')
        title('pl. strain vs. strain')
        drawnow
        
        if saveRunData==1 && success==1 && mod(inc,nout)==0
            fprintf('Saving data to disk\n')
            evol.stress = Tiivec(1:inc);
            evol.strain = strvec(1:inc);
            evol.nit    = 1:plast_it;
            evol.resid  = rxvec_abs(1:plast_it);
            if saveStiffness==1, 
                evol.Kep    = Kep;
                evol.R      = R;
            end
            save(['run_M2Di_EP_step', num2str(inc,'%04d')],'Sxxc_t','Syyc_t','Txyc_t','Szzc_t','Sxxv_t','Syyv_t','Txyv_t','Szzv_t','Ux','Uy','Exxc','Eyyc','Exyv','Ezzc','xc','yc','xv','yv','dx','dy','strain', 'inc', 'plastic','success','dUx_e','dUy_e','incr0','Txxc0','Tyyc0','Txyc0','Tzzc0','Txxv0','Tyyv0','Txyv0','Tzzv0','Eii_net','Eiicr_p','Eiicr_v','Eiicr_e','Eiicr','dUxc_t','dUyc_t', 'Pc0', 'Pv0','Cc','Cv', 'evol');
        end
        
        if  strain> strain_max, finish = 1; end
    end
end
fprintf('Strain rate = %2.2e, flow stress = %2.2e\n\n', incr0*(xmax-xmin)/dt, incr0*(xmax-xmin)/dt*eta0*2)
if saveRunData==1, save(['run_M2Di_EP_res', num2str(nx,'%3d'),'_nstep', num2str(ninc,'%3d')],'Tiivec','strvec','rvec_rel','rvec_abs','nitervec','increment'); end
