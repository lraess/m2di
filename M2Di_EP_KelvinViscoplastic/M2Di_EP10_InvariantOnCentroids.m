function [Eiic]   = M2Di_EP10_InvariantOnCentroids(Exxc   , Eyyc,   Ezzc,   Exyv   )
Exyc = 0.25*(Exyv(1:end-1,1:end-1) + Exyv(2:end,1:end-1) + Exyv(1:end-1,2:end) + Exyv(2:end,2:end));
Eiic  = sqrt(1/2*(Exxc).^2 + 1/2*(Eyyc).^2 + 1/2*(Ezzc).^2 + Exyc.^2);
end

