————————————————————————————————————————-------------------
 M2Di_EP README | 06.11.2019. M2Di_EP G3 KelvinViscoplastic
————————————————————————————————————————-------------------

M2Di with Elasto-Visco-ViscoPlastic rheology using a compressible 
displacement-based formulation consistent tangent linearisation.  

Copyright (C) 2019 Thibault Duretz, René de Borst, Laetitia
Le Pourhiet

This file is part of M2Di. Please refer to Duretz et al. (2019) for details about
the implementation.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

================================================================================
Distributed software, files in this directory:
================================================================================

M2Di_EP_KelvinViscoplastic		M2Di with Elasto-Visco-ViscoPlastic rheology using a
			                    compressible displacement-based formulation
			                    consistent tangent linearisation.

================================================================================

Contact: thibault.duretz@univ-rennes1.fr