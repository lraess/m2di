function  [alpha,LSsuccess] = M2Di_EP13_LineSearchEP(dU,ddU,NumUx,NumUyG,Gvec,VEc,Kc,Cc,phic,psic,Gvev,VEv,Kv,Cv,phiv,psiv,dx,dy,ncx,ncy,Pc0,Pv0,Txxc0,Tyyc0,Txyc0,Tzzc0,Txxv0,Tyyv0,Txyv0,Tzzv0, vm, BC, sin_phi, cos_phi, sin_psi, safe, res0, rho0, gy, pconf, eta_vp, dt, alpha_min, Ep_acc_c0, Ep_acc_v0, hc, hv, Cc0, Cv0, imp_hard  )

% Line search
dUo      = dU;
alpha    = 1;
alphamax = alpha;
if safe == 1
    alphamin = 0.0;
else
    alphamin = alpha_min;
end
nls      = 11;

dalpha   = (alphamax-alphamin)/(nls-1);
alphav   = alphamin:dalpha:alphamax;
resi     = zeros(nls,1);
LIsuccessc = 1;
LIsuccessv = 1;

for ils = 1:nls
    
    % Apply Newton step
    dU       = dUo + alphav(ils)*ddU;
    dUxc_t   = dU(NumUx);
    dUyc_t   = dU(NumUyG);
    
    % Initial guess or iterative solution for strain increments
        dExxc_t  = diff(dUxc_t,1,1)/dx;
        dEyyc_t  = diff(dUyc_t,1,2)/dy;
        dEzzc_t  = 0; dEzzv_t  = 0;
        dUx_exp  = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*dUxc_t(:,1) + BC.fsxS(:,1).*dUxc_t(:,1), dUxc_t, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*dUxc_t(:,end) + BC.fsxN(:,end).*dUxc_t(:,end)]; 
        dUy_exp  = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*dUyc_t(1,:) + BC.fsyW(1,:).*dUyc_t(1,:); dUyc_t; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*dUyc_t(end,:) + BC.fsyE(end,:).*dUyc_t(end,:)]; 
        dUxdy    = diff(dUx_exp,1,2)/dy;
        dUydx    = diff(dUy_exp,1,1)/dx;
        dExyv_t  = 0.5*( dUxdy + dUydx );
        % Extrapolate trial strain components
        dExyc_t     = 0.25*(dExyv_t(1:end-1,1:end-1) + dExyv_t(2:end,1:end-1) + dExyv_t(1:end-1,2:end) + dExyv_t(2:end,2:end));
        [ dExxv_t ] = M2Di_EP9_centroids2vertices( dExxc_t );
        [ dEyyv_t ] = M2Di_EP9_centroids2vertices( dEyyc_t );
        % Save
        dExxc_0 = dExxc_t;  dExxv_0 = dExxv_t; 
        dEyyc_0 = dEyyc_t;  dEyyv_0 = dEyyv_t; 
        dExyc_0 = dExyc_t;  dExyv_0 = dExyv_t; 
        dEzzc_0 = dEzzc_t;  dEzzv_0 = dEzzv_t; 
        % Total stress increments
        dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
        dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
        dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
        dTxyv = 2*Gvev.*dExyv_t ;
        dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
        dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
        dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
        dTxyc = 2*Gvec.*dExyc_t;
        % Total stresses
        dPc     =-1/3*(dSxxc + dSyyc + dSzzc); 
        dPv     =-1/3*(dSxxv + dSyyv + dSzzv); 
        Sxxc_t = VEc .* Txxc0 - Pc0 + dSxxc;
        Syyc_t = VEc .* Tyyc0 - Pc0 + dSyyc;
        Txyv_t = VEv .* Txyv0       + dTxyv;
        Szzc_t = VEc .* Tzzc0 - Pc0 + dSzzc;
        Sxxv_t = VEv .* Txxv0 - Pv0 + dSxxv;
        Syyv_t = VEv .* Tyyv0 - Pv0 + dSyyv;
        Txyc_t = VEc .* Txyc0       + dTxyc;
        Szzv_t = VEv .* Tzzv0 - Pv0 + dSzzv;
        Pc     = Pc0 + dPc;
        Pv     = Pv0 + dPv;
        Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
        Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
        J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
        J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
        Ceffc  = Cc0;
        Ceffv  = Cv0;
        syc    = Ceffc.*cos_phi + (Pc+pconf).*sin_phi;
        syv    = Ceffv.*cos_phi + (Pv+pconf).*sin_phi;
        Fc     =  sqrt(vm*J2c) - syc;
        Fv     =  sqrt(vm*J2v) - syv;
%         fprintf('max. Fc = %2.2e\n', max(Fc(:)))
%         fprintf('max. Fv = %2.2e\n', max(Fv(:)))
        J2c_t = J2c; J2v_t = J2v; Pc_t = Pc; Pv_t = Pv;
        % Plastic corrections
        if max(Fc(:))>0 || max(Fv(:))>0
            plc = Fc>0;
            plv = Fv>0;
            % dQds centers
            sxx = Sxxc_t; syy = Syyc_t; sxy = Txyc_t; szz = Szzc_t;
            dQdsxxc = (0.2e1 .* sqrt(0.3e1) .* vm .* sxx - sqrt(0.3e1) .* vm .* syy - sqrt(0.3e1) .* vm .* szz + 0.2e1 .* sin_psi .* sqrt(vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2)))) .* (vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2))) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1;
            dQdsyyc = -(sqrt(0.3e1) .* vm .* sxx - 0.2e1 .* sqrt(0.3e1) .* vm .* syy + sqrt(0.3e1) .* vm .* szz - 0.2e1 .* sin_psi .* sqrt(vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2)))) .* (vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2))) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1;
            dQdsxyc = sqrt(0.3e1) .* ((vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + 3 .* sxy .^ 2)) .^ (-0.1e1 ./ 0.2e1)) .* vm .* sxy;
            dQdszzc = -(sqrt(0.3e1) .* vm .* sxx + sqrt(0.3e1) .* vm .* syy - 0.2e1 .* sqrt(0.3e1) .* vm .* szz - 0.2e1 .* sin_psi .* sqrt(vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2)))) .* (vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2))) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1;
            % dQds vertices
            sxx = Sxxv_t; syy = Syyv_t; sxy = Txyv_t; szz = Szzv_t;
            dQdsxxv = (0.2e1 .* sqrt(0.3e1) .* vm .* sxx - sqrt(0.3e1) .* vm .* syy - sqrt(0.3e1) .* vm .* szz + 0.2e1 .* sin_psi .* sqrt(vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2)))) .* (vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2))) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1;
            dQdsyyv = -(sqrt(0.3e1) .* vm .* sxx - 0.2e1 .* sqrt(0.3e1) .* vm .* syy + sqrt(0.3e1) .* vm .* szz - 0.2e1 .* sin_psi .* sqrt(vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2)))) .* (vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2))) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1;
            dQdsxyv = sqrt(0.3e1) .* ((vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + 3 .* sxy .^ 2)) .^ (-0.1e1 ./ 0.2e1)) .* vm .* sxy;
            dQdszzv = -(sqrt(0.3e1) .* vm .* sxx + sqrt(0.3e1) .* vm .* syy - 0.2e1 .* sqrt(0.3e1) .* vm .* szz - 0.2e1 .* sin_psi .* sqrt(vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2)))) .* (vm .* (sxx .^ 2 - sxx .* syy - sxx .* szz + syy .^ 2 - syy .* szz + szz .^ 2 + (3 .* sxy .^ 2))) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1;
            % dgamma
            h1c = cos_phi.*hc.*sqrt(2/3).*sqrt((dQdsxxc.^2 + 2*dQdsxyc.^2 + dQdsyyc.^2 + dQdszzc.^2));
            h1v = cos_phi.*hv.*sqrt(2/3).*sqrt((dQdsxxv.^2 + 2*dQdsxyv.^2 + dQdsyyv.^2 + dQdszzv.^2));
            dgc = plc.*(Fc./(vm.*Gvec + Kc.*sin_phi*sin_psi + eta_vp/dt + h1c ));
            dgv = plv.*(Fv./(vm.*Gvev + Kv.*sin_phi*sin_psi + eta_vp/dt + h1v ));
            % Corrected strain rates on centers
            dExxc_t = dExxc_0 - plc.*dgc.*dQdsxxc;
            dEyyc_t = dEyyc_0 - plc.*dgc.*dQdsyyc;
            dExyc_t = dExyc_0 - plc.*dgc.*dQdsxyc/2;
            dEzzc_t = dEzzc_0 - plc.*dgc.*dQdszzc;
            % Corrected strain rates on vertices
            dExxv_t = dExxv_0 - plv.*dgv.*dQdsxxv;
            dEyyv_t = dEyyv_0 - plv.*dgv.*dQdsyyv;
            dExyv_t = dExyv_0 - plv.*dgv.*dQdsxyv/2;
            dEzzv_t = dEzzv_0 - plv.*dgv.*dQdszzv;
            % Total stress increments
            dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
            dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
            dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
            dTxyv = 2*Gvev.*dExyv_t ;
            dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
            dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
            dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
            dTxyc = 2*Gvec.*dExyc_t;
            % Total stresses
            dPc     =-1/3*(dSxxc + dSyyc + dSzzc);
            dPv     =-1/3*(dSxxv + dSyyv + dSzzv);
            Sxxc_t = VEc .* Txxc0 - Pc0 + dSxxc;
            Syyc_t = VEc .* Tyyc0 - Pc0 + dSyyc;
            Txyv_t = VEv .* Txyv0       + dTxyv;
            Szzc_t = VEc .* Tzzc0 - Pc0 + dSzzc;
            Sxxv_t = VEv .* Txxv0 - Pv0 + dSxxv;
            Syyv_t = VEv .* Tyyv0 - Pv0 + dSyyv;
            Txyc_t = VEc .* Txyc0       + dTxyc;
            Szzv_t = VEv .* Tzzv0 - Pv0 + dSzzv;
            Pc     = Pc0 + dPc;
            Pv     = Pv0 + dPv;
            Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
            Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
            J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
            J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
            depc     = sqrt(2/3).*sqrt((dgc.*dQdsxxc).^2+(dgc.*dQdsyyc).^2+(dgc.*dQdszzc).^2+2*(dgc.*dQdsxyc).^2);
            depv     = sqrt(2/3).*sqrt((dgv.*dQdsxxv).^2+(dgv.*dQdsyyv).^2+(dgv.*dQdszzv).^2+2*(dgv.*dQdsxyv).^2);
            Cc     = Cc0 + imp_hard*hc.*depc;
            Cv     = Cv0 + imp_hard*hv.*depv;
            syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
            syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
            Fc     =  sqrt(vm*J2c) - syc;
            Fv     =  sqrt(vm*J2v) - syv;
%             fprintf('max. Fc = %2.2e\n', max(Fc(:)))
%             fprintf('max. Fv = %2.2e\n', max(Fv(:)))
            syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
            syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
            gamdotc = (sqrt(vm*J2c) - syc) ./ eta_vp;
            gamdotv = (sqrt(vm*J2v) - syv) ./ eta_vp;
            Fc_vp  =  sqrt(vm*J2c) - syc - gamdotc.*eta_vp;
            Fv_vp  =  sqrt(vm*J2v) - syv - gamdotv.*eta_vp;
%             fprintf('max. Fc = %2.2e\n', max(Fc_vp(:)))
%             fprintf('max. Fv = %2.2e\n', max(Fv_vp(:)))
%             % Check validity of return mapping
%             a_cone_c = sqrt(vm*J2c_t) - dgc.*Gc;
%             a_cone_v = sqrt(vm*J2v_t) - dgv.*Gv;
%             if min(a_cone_c(:)) < 0 || min(a_cone_v(:)) < 0
%                 plc2(a_cone_c(:) < 0) = 1;
%                 plv2(a_cone_v(:) < 0) = 1;
%                 ksi     = cos(phi0);     % cos(phi)
%                 eta     = sin(phi0);     % sin(phi)
%                 eta_bar = sin(psi0);     % sin(psi)
%                 alpha   = ksi/eta;
%                 beta    = ksi/eta_bar;
%                 dEpc    = -plc2 .* (Pc_t - coh0*beta) ./ Kc;
%                 dEpv    = -plv2 .* (Pv_t - coh0*beta) ./ Kv;
%                 % Cancel return mapping
% %                 plc(plc2==1) = 0;
% %                 plv(plv2==1) = 0;
%                 % Corrected strain rates on centers
%                 dExxc_t = dExxc_0 - plc.*dgc.*dQdsxxc   - plc2.*alpha.*dEpc;
%                 dEyyc_t = dEyyc_0 - plc.*dgc.*dQdsyyc   - plc2.*alpha.*dEpc;
%                 dExyc_t = dExyc_0 - plc.*dgc.*dQdsxyc/2;
%                 dEzzc_t = dEzzc_0 - plc.*dgc.*dQdszzc   - plc2.*alpha.*dEpc;
%                 % Corrected strain rates on vertices
%                 dExxv_t = dExxv_0 - plv.*dgv.*dQdsxxv   - plv2.*alpha.*dEpv;
%                 dEyyv_t = dEyyv_0 - plv.*dgv.*dQdsyyv   - plv2.*alpha.*dEpv;
%                 dExyv_t = dExyv_0 - plv.*dgv.*dQdsxyv/2;
%                 dEzzv_t = dEzzv_0 - plv.*dgv.*dQdszzv   - plv2.*alpha.*dEpv;
%                 % Total stress increments
%                 dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
%                 dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
%                 dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
%                 dTxyv = 2*Gvev.*dExyv_t ;
%                 dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
%                 dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
%                 dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
%                 dTxyc = 2*Gvec.*dExyc_t;
%                 % Total stresses
%                 Sxxc_t = VEc .* Sxxc0 + dSxxc;
%                 Syyc_t = VEc .* Syyc0 + dSyyc;
%                 Txyv_t = VEv .* Txyv0 + dTxyv;
%                 Szzc_t = VEc .* Szzc0 + dSzzc;
%                 Sxxv_t = VEv .* Sxxv0 + dSxxv;
%                 Syyv_t = VEv .* Syyv0 + dSyyv;
%                 Txyc_t = VEc .* Txyc0 + dTxyc;
%                 Szzv_t = VEv .* Szzv0 + dSzzv;
%                 Pc     =-1/3*(Sxxc_t + Syyc_t + Szzc_t);
%                 Pv     =-1/3*(Sxxv_t + Syyv_t + Szzv_t);
%                 Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
%                 Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
%                 J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
%                 J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
%                 syc    = coh0.*cos_phi + (Pc+pconf).*sin_phi;
%                 syv    = coh0.*cos_phi + (Pv+pconf).*sin_phi;
%                 Fc     =  sqrt(vm*J2c) - syc;
%                 Fv     =  sqrt(vm*J2v) - syv;
%                 fprintf('max. Fc = %2.2e\n', max(Fc(:)))
%                 fprintf('max. Fv = %2.2e\n', max(Fv(:)))
% 
%                 fprintf('Invalid return mapping - Need to return to the apex\n');
% 
%                 error ('Invalid return mapping - Need to return to the apex');
%             end
        end
    
    if LIsuccessc==0 || LIsuccessv==0, break; end
    
    % Evaluate global residual
    Rx = [zeros(1,ncy); (diff(Sxxc_t,1,1)/dx + diff(Txyv_t(2:end-1,:),1,2)/dy)          ; zeros(1,ncy)];
    Ry = [zeros(ncx,1), (diff(Syyc_t,1,2)/dy + diff(Txyv_t(:,2:end-1),1,1)/dx) + rho0*gy, zeros(ncx,1)];
    R  = [Rx(:); Ry(:)];
    nR = norm(R,1)/length(R);
    resi(ils) = nR;
    if ils==1,  
        fprintf('LS It. %03d - alpha = %2.2f --- ||F|| = %2.8e\n', ils, alphav(ils), nR); 
    end
end
[val,ind] = min(resi);
alpha     = alphav(ind);
fprintf('Selected alpha = %1.2f yields||F|| = %2.8e\n',alpha, resi(ind));
LSsuccess = 1;
if safe==1 && (ind == 1 || LIsuccessc==0 || LIsuccessv==0), fprintf('no convergence\n'); LSsuccess = 0; end
if safe==2 && val>1.25*res0, fprintf('no convergence\n'); LSsuccess = 0; end 
end