
——————————————————————————————————————————————————
 M2Di README EXTRA | 06.05.2018. M2Di version 2.2
—————————————————————————————————————————————————-

M2Di: concise and efficient MATLAB 2D Stokes solver for linear and power
law viscous flow.

Copyright (C) 2017  Ludovic Raess, Thibault Duretz, Yury Podladchikov.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

================================================================================
Additional software derived from M2Di codes in M2Di_extra directory:
================================================================================

M2Di_extra		Contains the M2Di_Newton_Grav.m routine to solve power law
			Stokes flow under gravity (buoyancy driven) using analytical
			Jacobian for Newton iterations. A modified power-law definition
			is implemented compared to original M2Di_Newton, for more clear
			use of standard values.

================================================================================
QUICK START: Open Matlab, select the routine and enjoy!

SuiteSparse: For optimal performance, download the SuiteSparse package at
http://www.suitesparse.com, run the Matlab install and set the
SuiteSparse switch to 1.

—————————————————————————————————
Detailed infos and main switches:
—————————————————————————————————

1) Linear and Nonlinear routines switches:
SuiteSparse	0=use the Matlab functions
		1=use the SuiteSparse optimised version

solver		0=use the direct solver \
		1=use the decoupled iterative solver (Cholesky)

noisy		Choose how talkative the algorithm should be

2) Nonlinear only routines switches:
Newton		0=use the Picard iterations to resolve nonlinearities
		1=use the Newton (analytical Jacobian) to solve the nonlinearities

comp		0=incompressible Stokes formulation
		1=compressible Stokes formulation (divergence in strain rates)

pls		2=1/2*divergence if comp=1
		3=1/3*divergence if comp=1

KRO		0=no regularisation in the flow law for infinite and zero strain rate
		1=Carreau fluid model for infinite and zero strain rate

INV		0=averaging neighbouring strain rate components
		1=averaging of the second invariant contributions
================================================================================

Contact: ludovic.rass@gmail.com
