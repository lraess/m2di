
————————————————————————————————————————————
 M2Di README | 06.05.2018. M2Di version 1.3
————————————————————————————————————————————

M2Di: concise and efficient MATLAB 2D Stokes solver for linear and power
law viscous flow.

Copyright (C) 2017  Ludovic Raess, Thibault Duretz, Yury Podladchikov.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

================================================================================

https://bitbucket.org/lraess/m2di/
http://www.unil.ch/geocomputing/software/m2di/


Please cite us if you use our routine:
Räss, L., Duretz, T., Podladchikov, Y. Y., and Schmalholz, S. M. (2017), 
M2Di: Concise and efficient MATLAB 2-D Stokes solvers using the Finite 
Difference Method, Geochem. Geophys. Geosyst., 18, doi:10.1002/2016GC006727.

================================================================================
Distributed software, files in this directory:
================================================================================

JacobianStokes.mw	Maple file containing derivation of the analytical Jacobian for
			the Newton scheme to solve power law Stokes flow.

M2Di_Bench_Folder	Including M2Di_Newton_Folding.m and M2Di_Newton_Necking.m routines
			to reproduce the Fig. 3 benchmark results. The scripts load the
			included Folder_F.mat or Folder_N.mat (analytical and FEM solutions).

M2Di_crystals		Contains M2Di_crystals.m routine to reproduce Fig. 5. Select the 
			setup size to run by loading the according CrystalsXXxXX.mat file.

M2Di_cyl_slab		Contains the M2Di_cyl_slab.m routine to reproduce Fig. 8 linear
			Stokes flow in cylindrical coordinates.

M2Di_Dani		Contains the M2Di_Dani.m routine that solves linear Stokes flow
			around an inclusion and compares it to the analytical solution.
			The routine performs the convergence test and reproduces the Fig. 1
			of the paper.

M2Di_Dani_vardxy	Contains the M2Di_Linear_vardxy.m routine that extend the M2Di_Dani.m
			to take variable grid spacing into account. The routines compare
			the numerical solution to analytical and performs the convergence
			test Fig. 6c.

M2Di_Newton		Contains the M2Di_Newton.m routine to solve power law Stokes flow
			using analytical Jacobian for Newton iterations. This codes are used
			to realise the timing test (Fig. 3) and to resolve the power law 
			inclusion setup from Fig. 7.

================================================================================
QUICK START: Open Matlab, select the routine and enjoy!

SuiteSparse: For optimal performance, download the SuiteSparse package at
http://www.suitesparse.com, run the Matlab install and set the
SuiteSparse switch to 1.

—————————————————————————————————
Detailed infos and main switches:
—————————————————————————————————

1) Linear and Nonlinear routines switches:
SuiteSparse	0=use the Matlab functions
		1=use the SuiteSparse optimised version

solver		0=use the direct solver "\"
		1=use the decoupled iterative solver (Cholesky)

noisy		Choose how talkative the algorithm should be

2) Nonlinear only routines switches:
Newton		0=use the Picard iterations to resolve nonlinearities
		1=use the Newton (analytical Jacobian) to solve the nonlinearities

comp		0=incompressible Stokes formulation
		1=compressible Stokes formulation (divergence in strain rates)

pls		2=1/2*divergence if comp=1
		3=1/3*divergence if comp=1

KRO		0=no regularisation in the flow law for infinite and zero strain rate
		1=Carreau fluid model for infinite and zero strain rate

INV		0=averaging neighbouring strain rate components
		1=averaging of the second invariant contributions
================================================================================

Contact: ludovic.rass@gmail.com, thibault.duretz@univ-rennes1.fr
