% =========================================================================
% M2Di LinearStokes: Detached slab setup in cylindrical coordinates.

% Copyright (C) 2017  Ludovic Raess, Thibault Duretz, Yury podladchikov

% This file is part of M2Di.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

clear
SuiteSparse = 0;                                                            % 0=NO SuiteSparse     1=SuiteSparse
solver      = 1;                                                            % 0=Backslash          1=Cholesky
noisy       = 1;                                                            % 0=NO residual print  1=Print residuals
%% Domain
ymin    = 6371e3-1000e3;                                                    % Domain bottom
ymax    = 6371e3;                                                           % Domain top
xmin    = 250*pi/180 + pi;                                                  % Domain left extend
xmax    = 290*pi/180 + pi;                                                  % Domaine right extend
%% Scaling
Lc      = (ymax-ymin);                                                      % Char. length
Ec      = 1e-15;                                                            % Char. train rate
tc      = 1/Ec;                                                             % Char. time
Vc      = Lc/tc;                                                            % Char. velocity
etac    = 1e20;                                                             % Char. viscosity
tauc    = etac*Ec;                                                          % Char. stress
mc      = tauc*Lc*tc^2;                                                     % Char. mass
rhoc    = mc/Lc^3;                                                          % Char. density
ac      = Lc/tc/tc;                                                         % Char. acceleration
%% Physics
xmin = xmin/1;  xmax = xmax/1;  Lx = (xmax-xmin);                           % Scaled domain x extend
ymin = ymin/Lc; ymax = ymax/Lc; Ly = (ymax-ymin);                           % Scaled domain y extend
Earth_r = 6371e3/Lc;                                                        % Earth radius
gr      = -9.81/ac;                                                         % gravity acceleration on Earth
%% Numerics
nx      = 1000;                                                             % Grid points in x
ny      = nx/2;                                                             % Grid points in y
gam     = 1e5;                                                              % Numerical compressibility
tol_lin = 1e-12;                                                            % Linear solver tolerance
niter   = 50;                                                               % Max number of linear iterations
%% Preprocessing
tic
dx = Lx/nx;                                                                 % cell size in x
dy = Ly/ny;                                                                 % cell size in y
xv = xmin:dx:xmax;                  yv = ymin:dy:ymax;                      % vertex coord.
xc = 0.5*(xv(1:end-1) + xv(2:end)); yc = 0.5*(yv(1:end-1) + yv(2:end));     % center coord.
[XVX2, YVX2] = ndgrid(xv, yc); [XV2, YV2] = ndgrid(xv, yv);                 % coord. 2D grid
[XVY2, YVY2] = ndgrid(xc, yv); [XC2, YC2] = ndgrid(xc, yc);                 % coord. 2D grid
cpu = zeros(9,1);
cpu(1)=toc;
%% Initial arrays
tic
musv       = 1e19/etac*ones(nx+1,ny+1);                                     % viscosity at vertex
mus        = 1e19/etac*ones(nx  ,ny  );                                     % viscosity at center
ro_n       = 3250/rhoc*ones(nx, ny);                                        % density at center
[XC3,YC3]  = pol2cart(XC2,YC2);                                             % cylindrical coord.
[XV3,YV3]  = pol2cart(XV2,YV2);                                             % cylindrical coord.
% Background
mus ( YC2 < yv(end) - 330e3/Lc ) = 1e20/etac;                               % Setup init.
musv( YV2 < yv(end) - 330e3/Lc ) = 1e20/etac;
mus ( YC2 < yv(end) - 660e3/Lc ) = 1e22/etac;
musv( YV2 < yv(end) - 660e3/Lc ) = 1e22/etac;
mus ( YC2 > yv(end) - 120e3/Lc & (YC2-Earth_r) > deg2rad(110)*XC2 -2.2*Earth_r-1.6 & (YC2-Earth_r) > -deg2rad(110)*XC2 +2.5*Earth_r-1.13 ) = 1e23/etac;
musv( YV2 > yv(end) - 120e3/Lc & (YV2-Earth_r) > deg2rad(110)*XV2 -2.2*Earth_r-1.6 & (YV2-Earth_r) > -deg2rad(110)*XV2 +2.5*Earth_r-1.13 ) = 1e23/etac;
ro_n( YC2 > yv(end) - 120e3/Lc & (YC2-Earth_r) > deg2rad(110)*XC2 -2.2*Earth_r-1.6 & (YC2-Earth_r) > -deg2rad(110)*XC2 +2.5*Earth_r-1.13 ) = 3245/rhoc;
% Slab
mus ( (YC3-Earth_r) > -deg2rad(150)*XC3+1.9 & (YC3-Earth_r) < -deg2rad(150)*XC3+2.3 & YC2 < yv(end) - 170e3/Lc & YC2 > yv(end) - 610e3/Lc) = 1e23/etac;
musv( (YV3-Earth_r) > -deg2rad(150)*XV3+1.9 & (YV3-Earth_r) < -deg2rad(150)*XV3+2.3 & YV2 < yv(end) - 170e3/Lc & YV2 > yv(end) - 610e3/Lc) = 1e23/etac;
ro_n( (YC3-Earth_r) > -deg2rad(150)*XC3+1.9 & (YC3-Earth_r) < -deg2rad(150)*XC3+2.3 & YC2 < yv(end) - 170e3/Lc & YC2 > yv(end) - 610e3/Lc) = 3275/rhoc;
Pt         = zeros(nx  ,ny  );                                              % Pressure
Vx         = zeros(nx+1,ny  );                                              % x velocity
Vy         = zeros(nx  ,ny+1);                                              % y velocity
mus_axy    = musv(2:end-1,2:end-1);                                         
%% Numbering Pt and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));    % G stands for Gobal numbering
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));   % G stands for Gobal numbering
cpu(2)=toc;
%% Boundary Conditions on velocities [W E S N]
tic
ibcVxW = NumVx(1,:)';        ibcVxE = NumVx(end,:)';                        % Indexes of BC nodes for Vx East/West
ibcVxS = NumVx(2:end-1,1);   ibcVxN = NumVx(2:end-1,end);                   % Indexes of BC nodes for Vx North/South
ibcVyS = NumVyG(:,1);        ibcVyN = NumVyG(:,end);                        % Indexes of BC nodes for Vy North/South
ibcVyW = NumVyG(1,2:end-1)'; ibcVyE = NumVyG(end,2:end-1)';                 % Indexes of BC nodes for Vy East/West
ibc    = [ ibcVxW; ibcVxE; ibcVyS; ibcVyN ];                                % Group all indexes
ibcNC  = [ ibcVxS; ibcVxN; ibcVyW; ibcVyE ];                                % Non Confroming to the physical boundary
Vx_W   = 0*ones(1 ,ny  )';                                                  % Dirichlet BC values Vx(W,E) Vy(S,N)
Vx_E   = 0*ones(1 ,ny  )';
Vy_S   = 0*ones(nx,1   );
Vy_N   = 0*ones(nx,1   );
vBc    = [ Vx_W;  Vx_E;  Vy_S;  Vy_N ];                                     % Group all values
% vBcNC  = [ Vx_S(2:end-1); Vx_N(2:end-1); Vy_W(2:end-1); Vy_E(2:end-1)];   % Non Confroming to the physical boundary
cpu(3)=toc;
%% grad and div blocs
tic
iVxC   = NumVx;                                                             % dP/dx
iPtW   =  ones(size(iVxC));    iPtW(1:end-1,:) = NumPt;
iPtE   =  ones(size(iVxC));    iPtE(2:end  ,:) = NumPt;
cPtW   = -ones(size(iVxC))/dx; cPtW([1 end],:) = 0;
cPtE   =  ones(size(iVxC))/dx; cPtE([1 end],:) = 0;
Idx    = [ iVxC(:); iVxC(:) ]';
Jdx    = [ iPtW(:); iPtE(:) ]';
Vdx    = [ cPtW(:); cPtE(:) ]';
iVyC   = NumVyG;                                                            % dP/dy
rVrC   = YVY2;    
iPtS   =  ones(size(iVyC));    iPtS(:,1:end-1) = NumPt;
iPtN   =  ones(size(iVyC));    iPtN(:,2:end  ) = NumPt;
cPtS   =-rVrC./dy; cPtS(:,[1 end]) = 0;
cPtN   = rVrC./dy; cPtN(:,[1 end]) = 0;
Idy    = [ iVyC(:); iVyC(:) ]';
Jdy    = [ iPtS(:); iPtN(:) ]';
Vdy    = [ cPtS(:); cPtN(:) ]';
%% PP block
iPt    = NumPt;  I = iPt(:)';  J = I;                                       % Eq. index center (pressure diagonal)
V      = ones(nx*ny,1)./gam;                                                % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V); else                                % Matrix assembly
                   PP =  sparse(I,J,V); end
%% Block UU
mus_W  = zeros(size(Vx));  mus_W(2:end  , :     ) = mus;                    % Viscosities (W,E,S,N)
mus_E  = zeros(size(Vx));  mus_E(1:end-1, :     ) = mus;
mus_S  = zeros(size(Vx));  mus_S(2:end-1,1:end-1) = mus_axy;
mus_N  = zeros(size(Vx));  mus_N(2:end-1,2:end  ) = mus_axy;
rVpC   = YVX2;
rVpS   = ones(size(Vx));  rVpS(:     ,1:end-1) = YVX2(:,2:end);
rcW    = ones(size(Vx));  rcW(2:end  , :     ) = YC2;
rcE    = ones(size(Vx));  rcE(1:end-1, :     ) = YC2;
rvS    = ones(size(Vx));  rvS(2:end-1,1:end-1) = YV2(2:end-1,2:end-1);
rvN    = ones(size(Vx));  rvN(2:end-1,2:end  ) = YV2(2:end-1,2:end-1);
iVx    = NumVx;                                                             % Eq. index for Vx (C,W,E,S,N)
iVxW   = NumVx(1:end-1, :     );
iVxE   = NumVx(2:end  , :     );
iVxS   = NumVx(2:end-1,1:end-1);
iVxN   = NumVx(2:end-1,2:end  );
cVxC   = -(-2*mus_E./(rcE*dx)-2*mus_W./(rcW*dx))/dx+rVpC.*(mus_N.*(-2/dy+rVpC./(rvN*dy))-mus_S.*(2/dy-rVpC./(rvS*dy)))/dy-(2*(rvN.*mus_N.*(-2/dy+rVpC./(rvN*dy))-rvS.*mus_S.*(2/dy-rVpC./(rvS*dy))))/dy; % Center coeff.
scVx   = max(cVxC(:));                           cVxC([1,end],:) = scVx;    % Scaling factor for Vx Dirichlet values
cVxW   = -2*mus_W(2:end,:)/dx/dx./rcW(2:end,:);  cVxW([1,end],:) = 0;       % West coeff. 
cVxS   = -rVpC(2:end-1,1:end-1).*mus_S(2:end-1,1:end-1).*(-2/dy+rVpS(2:end-1,1:end-1)./(rvS(2:end-1,1:end-1)*dy))/dy+2*rvS(2:end-1,1:end-1).*mus_S(2:end-1,1:end-1).*(-2/dy+rVpS(2:end-1,1:end-1)./(rvS(2:end-1,1:end-1)*dy))/dy; % South coeff.
Iuu    = [  iVx(:); iVxE(:); iVxN(:) ]';                                    % Triplets [I,J,V]
Juu    = [  iVx(:); iVxW(:); iVxS(:) ]';
Vuu    = [ cVxC(:); cVxW(:); cVxS(:) ]';
%% Block VV
mus_W  = zeros(size(Vy));  mus_W(1:end-1,2:end-1) = mus_axy;                % Viscosities (W,E,S,N)
mus_E  = zeros(size(Vy));  mus_E(2:end  ,2:end-1) = mus_axy;
mus_S  = zeros(size(Vy));  mus_S( :     ,2:end  ) = mus;
mus_N  = zeros(size(Vy));  mus_N( :     ,1:end-1) = mus;
Rogy   = zeros(size(Vy));  Rogy(  :     ,2:end-1) = 0.5*(ro_n(:,1:end-1)+ro_n(:,2:end))*gr.*YVY2(:,2:end-1); % Gravity
rVrC   = YVY2;
rVrS   = ones(size(Vy));  rVrS(:     ,2:end  ) = YVY2(:,1:end-1);
rvW    = ones(size(Vy));  rvW(1:end-1,2:end-1) = YV2(2:end-1,2:end-1);
rvE    = ones(size(Vy));  rvE(2:end  ,2:end-1) = YV2(2:end-1,2:end-1);
rcS    = ones(size(Vy));  rcS( :     ,2:end  ) = YC2;
rcN    = ones(size(Vy));  rcN( :     ,1:end-1) = YC2;
iVy    = NumVyG;                                                            % Eq. index for Vy (C,W,E,S,N)
iVyW   = NumVyG(1:end-1,2:end-1);
iVyE   = NumVyG(2:end  ,2:end-1);
iVyS   = NumVyG( :     ,1:end-1);
iVyN   = NumVyG( :     ,2:end  );
cVyC   = -(-mus_E./(rvE*dx)-mus_W./(rvW*dx))/dx+(2*rcN.*mus_N.*(-rVrC./(rcN*dy)+1/dy)-2*rcS.*mus_S.*(rVrC./(rcS*dy)-1/dy))/dy-rVrC.*(2*mus_N.*(-rVrC./(rcN*dy)+1/dy)-2*mus_S.*(rVrC./(rcS*dy)-1/dy))/dy-(-2*rcN.*mus_N./dy-2*rcS.*mus_S/dy)/dy; % Center coeff.
scVy   = max(cVyC(:));                           cVyC(:,[1,end]) = scVy;    % Scaling factor for Vy Dirichlet values
cVyW   = -mus_W(1:end-1,2:end-1)./(dx^2.*rvW(1:end-1,2:end-1));             % West coeff.
cVyS   = -2*rcS(:,2:end).*mus_S(:,2:end).*(-rVrS(:,2:end)./(rcS(:,2:end)*dy)+1/dy)/dy+2*rVrC(:,2:end).*mus_S(:,2:end).*(-rVrS(:,2:end)./(rcS(:,2:end)*dy)+1/dy)/dy-2*rcS(:,2:end).*mus_S(:,2:end)/dy^2;  cVyS(:,[1,end]) = 0; % South coeff.
Ivv    = [  iVy(:); iVyE(:); iVyN(:) ]';                                    % Triplets [I,J,V]
Jvv    = [  iVy(:); iVyW(:); iVyS(:) ]';
Vvv    = [ cVyC(:); cVyW(:); cVyS(:) ]';
%% Block VU
mus_W  = zeros(size(Vy));  mus_W( :, :     ) = musv(1:end-1, :);            % Viscosities (W,E,S,N)
mus_E  = zeros(size(Vy));  mus_E( :, :     ) = musv(2:end  , :);
mus_S  = zeros(size(Vy));  mus_S( :,2:end  ) = mus;
mus_N  = zeros(size(Vy));  mus_N( :,1:end-1) = mus;
rVrC   = YVY2;
rvW    = zeros(size(Vy));  rvW( :, :     )   = YV2(1:end-1, :);
rvE    = zeros(size(Vy));  rvE( :, :     )   = YV2(2:end  , :);
rcS    = zeros(size(Vy));  rcS( :,2:end  )   = YC2;
rcN    = zeros(size(Vy));  rcN( :,1:end-1)   = YC2;
rVpSW  = YVX2(1:end-1,1:end-1);
rVpSE  = YVX2(2:end  ,1:end-1);
rVpNW  = YVX2(1:end-1,2:end  );
rVpNE  = YVX2(2:end  ,2:end  );
iVy    = NumVyG( :    ,2:end-1);                                            % Eq. index for VyC
iVxSW  = NumVx(1:end-1,1:end-1);                                            % Eq. index for Vx (SW,SE,NW,NE)
iVxSE  = NumVx(2:end  ,1:end-1);
iVxNW  = NumVx(1:end-1,2:end  );
iVxNE  = NumVx(2:end  ,2:end  );
cVxSW  = mus_W(:,2:end-1).*(-2/dy+rVpSW./(rvW(:,2:end-1)*dy))/dx+2*mus_S(:,2:end-1)/(dy*dx)-2*rVrC(:,2:end-1).*mus_S(:,2:end-1)./(dy*rcS(:,2:end-1)*dx);  cVxSW(1  ,:) = 0; % Coeff. for Vx (SW,SE,NW,NE)
cVxSE  =-mus_E(:,2:end-1).*(-2/dy+rVpSE./(rvE(:,2:end-1)*dy))/dx-2*mus_S(:,2:end-1)/(dy*dx)+2*rVrC(:,2:end-1).*mus_S(:,2:end-1)./(dy*rcS(:,2:end-1)*dx);  cVxSE(end,:) = 0;
cVxNW  = mus_W(:,2:end-1).*( 2/dy-rVpNW./(rvW(:,2:end-1)*dy))/dx-2*mus_N(:,2:end-1)/(dy*dx)+2*rVrC(:,2:end-1).*mus_N(:,2:end-1)./(dy*rcN(:,2:end-1)*dx);  cVxNW(1  ,:) = 0;
cVxNE  =-mus_E(:,2:end-1).*( 2/dy-rVpNE./(rvE(:,2:end-1)*dy))/dx+2*mus_N(:,2:end-1)/(dy*dx)-2*rVrC(:,2:end-1).*mus_N(:,2:end-1)./(dy*rcN(:,2:end-1)*dx);  cVxNE(end,:) = 0;
Ivu    = [   iVy(:);   iVy(:);   iVy(:);   iVy(:) ]';                       % Triplets [I,J,V]
Jvu    = [ iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:) ]';
Vvu    = [ cVxSW(:); cVxSE(:); cVxNW(:); cVxNE(:) ];
cpu(4)=toc; display(['Time Build Blocks = ', num2str(cpu(4))]);
%% Assemble Blocs
tic
if SuiteSparse==1, K    = sparse2( [Iuu(:); Ivv(:); Ivu(:)], [Juu(:); Jvv(:); Jvu(:)], [Vuu(:); Vvv(:); Vvu(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
                   grad = sparse2( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny );                                      else
                   K    =  sparse( [Iuu(:); Ivv(:); Ivu(:)], [Juu(:); Jvv(:); Jvu(:)], [Vuu(:); Vvv(:); Vvu(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
                   grad =  sparse( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny );                                      end
div   = -grad';
cpu(5)=toc; display(['Time Assemble     = ', num2str(cpu(5))]);
%% BC's on K and div
tic
BcK                  = zeros(size(K,1),1);
BcK(NumVx(2    ,:))  = BcK(NumVx(2    ,:))  + mus(1  ,:  )'/dx/dx.*Vx_W;
BcK(NumVx(end-1,:))  = BcK(NumVx(end-1,:))  + mus(end,:  )'/dx/dx.*Vx_E;
BcK(NumVyG(:, 2   )) = BcK(NumVyG(:, 2   )) + mus(:  ,1  ) /dy/dy.*Vy_S;
BcK(NumVyG(:,end-1)) = BcK(NumVyG(:,end-1)) + mus(:  ,end) /dy/dy.*Vy_N;
BcK(ibcVxN) = BcK(ibcVxN) +  (  mus(1:end-1,end) - musv(2:end-1,end) )/dx/dy.*Vy_N(1:end-1) ;   % VyNW
BcK(ibcVxN) = BcK(ibcVxN) +  ( -mus(2:end  ,end) + musv(2:end-1,end) )/dx/dy.*Vy_N(2:end  ) ;   % VyNE
BcK(ibcVxS) = BcK(ibcVxS) +  ( -mus(1:end-1,1  ) + musv(2:end-1, 1 ) )/dx/dy.*Vy_S(1:end-1) ;   % VySW
BcK(ibcVxS) = BcK(ibcVxS) +  (  mus(2:end  ,1  ) - musv(2:end-1, 1 ) )/dx/dy.*Vy_S(2:end  ) ;   % VySE
BcK(ibcVyE) = BcK(ibcVyE) + ((  mus(end,1:end-1) - musv(end,2:end-1) )/dx/dy.*Vx_E(1:end-1)')'; % VxSE
BcK(ibcVyE) = BcK(ibcVyE) + (( -mus(end,2:end  ) + musv(end,2:end-1) )/dx/dy.*Vx_E(2:end  )')'; % VxNE
BcK(ibcVyW) = BcK(ibcVyW) + (( -mus(1  ,1:end-1) + musv(1  ,2:end-1) )/dx/dy.*Vx_W(1:end-1)')'; % VxSW
BcK(ibcVyW) = BcK(ibcVyW) + ((  mus(1  ,2:end  ) - musv(1  ,2:end-1) )/dx/dy.*Vx_W(2:end  )')'; % VxNW 
% Conforming Dirichlets
BcK(ibc)    = [Vx_W*scVx; Vx_E*scVx; Vy_S*scVy; Vy_N*scVy];
% Gravity
BcK(NumVyG(:)) = BcK(NumVyG(:)) - Rogy(:); % Gravity term in rhs
% Non-conforming Dirichlets
% cNC        = [mus_axy(:,1)/dy/dy; mus_axy(:,end)/dy/dy; mus_axy(1,:)'/dx/dx; mus_axy(end,:)'/dx/dx];
% d0         = spdiags(K,0);
% d0(ibcNC)  = d0(ibcNC)  + 2*cNC;
% BcK(ibcNC) = BcK(ibcNC) + 2*cNC.*vBcNC;
% K          = spdiags(d0,0,K);
% BC on DivV
BcD                 = zeros(size(div,1),1);
BcD(NumPt(1  ,:  )) = BcD(NumPt(1  ,:  )) + 1/dx*Vx_W;
BcD(NumPt(end,:  )) = BcD(NumPt(end,:  )) - 1/dx*Vx_E;
BcD(NumPt( : ,1  )) = BcD(NumPt(:  ,1  )) + 1/dy*Vy_S;
BcD(NumPt( : ,end)) = BcD(NumPt(:  ,end)) - 1/dy*Vy_N;
cpu(6)=toc; display(['Time BC           = ', num2str(cpu(6))]);
%% Prepare solve
tic
K = K + K' - diag(diag(K));                                                 % Build full from tri-lower (for checking)
cpu(7)=toc;
tic
if solver==0                                                                % Build M_Stokes
    Ms  = [ K   , -div' ; ...
            div ,  PP   ];
    cpu(8)=toc; display(['Time Build Ms     = ', num2str(cpu(8))]);
else
    PPI      = spdiags(1./diag(PP),0,PP);
    Kt       = K - grad*(PPI*div);                                          % PPI*DivV = PP\DivV
    [Kc,e,s] = chol(Kt,'lower','vector');                                   % Cholesky factorization
    cpu(8)=toc; display(['Time CHOLESKY     = ', num2str(cpu(8))]);
end
tic
u   = [Vx(:) ; Vy(:)];
p   =  Pt(:);
up  = [ u ; p ];
cpu(9)=toc;
%% Solve
tic
for it=1:niter
    if solver==0,
        up  = Ms\[ BcK  ; BcD  + PP *up(NumPtG(:))];                        % Backslash
        du  = BcK -   K*up([NumVx(:); NumVyG(:)]) - grad*up(NumPtG(:));
        dp  = BcD - div*up([NumVx(:); NumVyG(:)]); 
    else
        Rhs =      BcK  - grad*(PPI*BcD + p);                               % Powell-Hestenes
        if SuiteSparse==1, u(s) = cs_ltsolve(Kc,cs_lsolve(Kc,Rhs(s))); else % Powell-Hestenes
                           u(s) = Kc'\(Kc\Rhs(s));                     end  % Powell-Hestenes Matlab
        p   = p + PPI*(BcD - div*u);
        du  = BcK -   K*u - grad*p;
        dp  = BcD - div*u;
    end
    if noisy==1, fprintf('  --- iteration %d --- \n',it);
                 fprintf('   Res. |du| = %2.2e \n',norm(du)/length(du));
                 fprintf('   Res. |dp| = %2.2e \n',norm(dp)/length(dp)); end
    if norm(dp)/length(dp) < tol_lin, break, end
end%it
cpu(10)=toc; display(['Time Backsubs     = ', num2str(cpu(10))]);
tic
XPH = [u ; p];
XBS = up; 
%% Post-processing
if solver==0, up = XBS; else up = XPH; end
Pt  = reshape(up(NumPtG(:)),[nx  ,ny  ]); Pt = Pt - repmat(Pt(:,end),1,ny);
Vx  = reshape(up(NumVx(:)) ,[nx+1,ny  ]);
Vy  = reshape(up(NumVyG(:)),[nx  ,ny+1]);
VxC = 0.5*(Vx(1:end-1,:) + Vx(2:end,:));
VyC = 0.5*(Vy(:,1:end-1) + Vy(:,2:end));
DivV_expl = diff(Vx,1,1)/dx + diff(YVY2.*Vy,1,2)/dy;
Plith     = cumsum(dy*(ro_n)*gr, 2); Plith = Plith - repmat(Plith(:,end),1,ny); Pdyn = Pt - Plith;
% Convert velocity to Cartesian coordinates
VyC1    = 1./(cos(XC2).^2 + sin(XC2).^2).*(cos(XC2).*VxC - sin(XC2).*VyC);
VxC1    = 1./(cos(XC2).^2 + sin(XC2).^2).*(sin(XC2).*VxC + cos(XC2).*VyC);
eps_rr  = diff(Vy,1,2)/dy;
eps_tt  = 1./YC2.*( diff(YVY2.*Vy,1,2)/dy -  YC2.*diff(Vy,1,2)/dy + diff(Vx,1,1)/dx); 
eps_rt  = 2*(diff(Vx(2:end-1,:),1,2)/dy) + 1./YV2(2:end-1,2:end-1).*diff(Vy(:,2:end-1),1,1)/dx - 1./YV2(2:end-1,2:end-1).*diff(YVX2(2:end-1,:).*Vx(2:end-1,:),1,2)/dy;
eps_rt1 = zeros(size(musv)); eps_rt1(2:end-1,2:end-1) = eps_rt; eps_rt1(1,:) = eps_rt1(2,:); eps_rt1(end,:) = eps_rt1(end-1,:); eps_rt1(:,1) = eps_rt1(:,2); eps_rt1(:,end) = eps_rt1(:,end-1);
eps_rt2 = 0.25*(eps_rt1(1:end-1,1:end-1)+eps_rt1(1:end-1,2:end)+eps_rt1(2:end,1:end-1)+eps_rt1(2:end,2:end));
tau_rr  = 2.*mus.*eps_rr;
sig_rr  = -Pt + tau_rr;
eII     = sqrt(eps_rr.^2+eps_tt.^2+eps_rt2.^2);
cmy = 100*3600*24*365; km = 1e3;
% Figure 1
figure(1),clf,set(gcf,'Color','white')
subplot(211)
plot(XC3(:,end)*Lc/km, VxC(:,end)*Vc*cmy, '-k','linewidth',1.6)
title('a) Longitudinal surface velocity', 'interpreter', 'latex','FontSize', 16)
ylabel('$v_{\theta}$ [cm/y]', 'interpreter', 'latex', 'FontSize', 16)
set(gca, 'FontSize', 16,'linewidth',1.1)
axis([-2400 2400 -21 11]); set(gca,'Xtick',[-2000 -1000 0 1000 2000],'Xticklabel',[-20 -10 0 10 20])
pos = get(gca,'pos'); set(gca,'pos',[pos(1) pos(2) pos(3) pos(4)*0.6]); set(gca, 'FontSize', 14,'linewidth',1.1)
subplot(212)
hd = -sig_rr(:,end)./gr./ro_n(:,end);
plot(XC3(:,end)*Lc/km, hd*Lc, '-k','linewidth',1.6)
title('b) Dynamic topography', 'interpreter', 'latex', 'FontSize', 16)
ylabel('$h_d$ [m]', 'interpreter', 'latex', 'FontSize', 16)
xlabel('Longitude [$^{\circ}$]', 'interpreter', 'latex', 'FontSize', 16)
set(gca, 'FontSize', 14,'linewidth',1.1)
axis([-2400 2400 -90 190])
set(gca,'Xtick',[-2000 -1000 0 1000 2000],'Xticklabel',[-20 -10 0 10 20])
pos = get(gca,'pos'); set(gca,'pos',[pos(1) pos(2)+0.8*pos(2) pos(3) pos(4)*0.6]);
% Figure 2
figure(2),clf,colormap('jet'),set(gcf,'Color','white')
st = 40;
p1 = pcolor(XC3*Lc, YC3*Lc, log10(eII*Ec)); shading flat, axis equal, alpha(p1,0.8),
hc = colorbar('location','SouthOutside','position',[0.4 0.28 0.23 0.04]); caxis([-18 -12])
ax = gca; pos = get(gca,'pos'); set(gca,'pos',[pos(1) pos(2) pos(3) pos(4)*0.95]);
hold on
quiver(XC3(1:st:end,1:st:end)*Lc, YC3(1:st:end,1:st:end)*Lc, VxC1(1:st:end,1:st:end)*Vc, VyC1(1:st:end,1:st:end)*Vc,'k')
[c,h] = contour(XC3*Lc, (YC3-0*Earth_r)*Lc, ro_n*rhoc, [ 3249 3251 ], 'Color', [0 0 0.5], 'LineWidth', 1.0); 
[c,h] = contour(XC3*Lc, (YC3-0*Earth_r)*Lc, ro_n*rhoc, [ 2899 2901 ], 'Color', [0 0 0.5], 'LineWidth', 1.0);
plot(XV3(1,  :)*Lc, YV3(1,  :)*Lc, '-k',XV3(end,:)*Lc, YV3(end,:)*Lc, '-k',XV3(:,  1)*Lc, YV3(:,  1)*Lc, '-k',XV3(:,end)*Lc, YV3(:,end)*Lc, '-k')
text(-5.4e5  ,5.1e6  ,'$\log_{10} \dot{\epsilon}_{II}$ [s$^{-1}$]','interpreter','latex','FontSize', 18)
text(-2.26e6 ,6.2e6  ,'$-20^{\circ}$','interpreter','latex','FontSize', 16)
text( 1.94e6 ,6.2e6  ,'$ 20^{\circ}$','interpreter','latex','FontSize', 16)
text(-1.0e5  ,6.5e6  ,'$  0^{\circ}$','interpreter','latex','FontSize', 16)
text(-2.38e6 ,5.95e6 ,'$ 0$'   ,'interpreter','latex','FontSize', 16)
text(-2.40e6 ,5.05e6 ,'$-1000$','interpreter','latex','FontSize', 16)
t1=text(-2.6e6,5.1e6 ,'Depth [km]', 'interpreter', 'latex', 'FontSize', 16); set(t1,'rotation', 90)
text(-1.1e6  ,6.7e6  ,'c) Strain rate and flow field', 'interpreter', 'latex', 'FontSize', 16)
hold off
box off
axis off
set(gca,'Xtick',[],'XTicklabel',[],'Ytick',[],'YTicklabel',[])
set(gca, 'FontSize', 16,'linewidth',1.1)
