% =========================================================================
% Main function used to run the resoution test M2Di Linear Stokes for the
% Dani inclusion test with variable grid spacing.

% Copyright (C) 2017  Ludovic Raess, Thibault Duretz, Yury podladchikov

% This file is part of M2Di.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

function M2Di_Linear_vardxy

SuiteSparse = 0;                                                            % 0=NO SuiteSparse     1=SuiteSparse
solver      = 1;                                                            % 0=Backslash          1=Cholesky
noisy       = 1;                                                            % 0=NO residual print  1=Print residuals
%%
resolution  = [25 50 100 200 400];                                          % resolution at which test is performed
plot1       = 1;                                                            % 0=NO plotting        1=Figure

Converg = zeros(2,size(resolution,2),3);
if plot1==1, figure(5),clf,set(gcf,'Color','white'); end
for var = 1:2
    varxy = var-1;                                                          % 0=NO variable grid   1=Variable grid
    for i=1:length(resolution)
        resol = resolution(i);
        nDoF  = 3*resol^2;
        % Run Stokes solver
        [e_p, e_u] = M2Di_Linear_var(resol,solver, SuiteSparse,noisy,varxy);
        % Store results
        Converg(var,i,:) = [e_p, e_u, nDoF];
    end
    if plot1==1
        % Extract fit
        P1  = polyfit(log10(Converg(var,:,3)),log10(Converg(var,:,1)),1);
        L1  = log10(Converg(var,:,3))*P1(1) + P1(2);
        P2  = polyfit(log10(Converg(var,:,3)),log10(Converg(var,:,2)),1);
        L2  = log10(Converg(var,:,3))*P2(1) + P2(2);
        % Plot convergence test
        figure(5),hold on
        if var==1
            h1=plot(log10(Converg(var,:,3)),L1,'--b','linewidth',1.3);
            h2=plot(log10(Converg(var,:,3)),L2,'-b' ,'linewidth',1.3);
            plot(log10(Converg(var,:,3)),log10(Converg(var,:,1)),'xb','MarkerSize',6,'linewidth',2)
            plot(log10(Converg(var,:,3)),log10(Converg(var,:,2)),'ob','MarkerSize',6,'linewidth',2)
        else
            b1=plot(log10(Converg(var,:,3)),L1,'--r','linewidth',1.3);
            b2=plot(log10(Converg(var,:,3)),L2,'-r' ,'linewidth',1.3);
            plot(log10(Converg(var,:,3)),log10(Converg(var,:,1)),'xr','MarkerSize',6,'linewidth',2)
            plot(log10(Converg(var,:,3)),log10(Converg(var,:,2)),'or','MarkerSize',6,'linewidth',2)
        end
        hold off
    end
end
if plot1==1
    leg = legend([h1,h2,b1,b2], '$\left|\left|e_{\mathrm{p}}\right|\right|_1$ regular','$\left|\left|e_{\mathrm{u}\;}\right|\right|_1$ regular', '$\left|\left|e_{\mathrm{p}}\right|\right|_1$ variable','$\left|\left|e_{\mathrm{u}\;}\right|\right|_1$ variable');
    set(leg,'interpreter','latex','box','off')
    set(gca, 'FontSize', 20,'linewidth',1.2); box on
    xlabel('log$_{10}$(DoF)', 'interpreter', 'latex', 'FontSize', 20)
    ylabel('log$_{10}$ $\left|\left|err\right|\right|_1$', 'interpreter', 'latex', 'FontSize', 20)
    ax  = gca; pos = get(gca,'pos'); set(gca,'pos',[pos(1) pos(2)+0.15*pos(2) pos(3)*0.6 pos(4)*0.9]); pos = get(gca,'pos');
    axis([2.9 7.1 -4.6 0.1])
    set(gca, 'FontSize', 16,'linewidth',1.1)
    title('c) Convergence history', 'interpreter', 'latex', 'FontSize', 18)
end
end

function [L1PrErr2,L1VxErr2] = M2Di_Linear_var(resol,solver, SuiteSparse,noisy,varxy)
% M2Di Linear Stokes: Thibault Duretz, Ludovic Raess, Yuri Podladchikov  -  Unil 2017
%% Physics
Lx      = 6;                                                                % Box width
Ly      = 6;                                                                % Box height
mus0    = 1;                                                                % Background viscosity
mus_i   = 1e4;                                                              % Inclusion viscosity
rad     = 1;                                                                % Inclusion radius
%% Numerics
nx      = resol;                                                            % Grid points in x
ny      = nx;                                                               % Grid points in y
gamma   = 1e9;                                                              % Numerical compressibility
tol_lin = 1e-16;                                                            % Linear solver tolerance
niter   = 15;                                                               % Max number of linear iterations
%% Preprocessing
tic
dx = Lx/nx;                                                                 % cell size in x
dy = Ly/ny;                                                                 % cell size in y
xvR = 0:dx:Lx;         yvR = 0:dy:Ly;                                       % cell vertex coord. grid
xcR = dx/2:dx:Lx-dx/2; ycR = dy/2:dy:Ly-dy/2;                               % cell center coord. grid
% Create variable mesh in x,y
a      = 1e-1;                                                              % Diffusion parameter
qfact  = 0.03;                                                              % Max. spacing ratio allowed dxE/dxW < qfact
nmax   = 100000;                                                            % Max grid diffusion (smoothing) iters
nx_ref = nx+1 -2;                                                           % total amount of grid points in x
xlim1  = 0.55;                                                              % x coord min  - refined zone 
xlim2  = 1.5;                                                               % x coord msx  - refined zone 
dx_ref = (xlim2-xlim1)/double((nx_ref-1));
x_ref  = xlim1:dx_ref:xlim2;
xv     = [0 x_ref Lx];                                                      % x vertex coord.
for it=1:nmax
    xv(2:end-1) = xv(2:end-1) + a*(xv(1:end-2)+xv(3:end)-2*xv(2:end-1));
    dxv         = xv(2:end)-xv(1:end-1);
    quality     = abs(dxv(2:end) - dxv(1:end-1))./(0.5*(dxv(2:end) + dxv(1:end-1)))*2;
    if max(quality)<qfact, disp('Mesh success'), break; end
end
yv  = xv; dxmin = min(dxv); dxmax = max(dxv);                               % variable grid size vertex coord. x=y
xc  = 0.5*(xv(2:end) + xv(1:end-1));                                        % variable grid size x center coord.
yc  = 0.5*(yv(2:end) + yv(1:end-1));                                        % variable grid size y center coord.
xv = varxy*xv + (1-varxy)*xvR; yv = varxy*yv + (1-varxy)*yvR; [XV2 YV2] = ndgrid(xv, yv);
xc = varxy*xc + (1-varxy)*xcR; yc = varxy*yc + (1-varxy)*ycR; [XC2 YC2] = ndgrid(xc, yc);
XV2v = XV2; xvv = XV2v(:,1)';
YV2v = YV2; yvv = YV2v(1,:) ;
XC2v = XC2; xcv = XC2v(:,1)';
YC2v = YC2; ycv = YC2v(1,:) ;
dxv = diff(XV2v,1,1); dxvyc = 0.5*(dxv(:,1:end-1)+dxv(:,2:end));            % variable dx vertex
dyv = diff(YV2v,1,2); dyvxc = 0.5*(dyv(1:end-1,:)+dyv(2:end,:));            % variable dy vertex
dxc = diff(XC2v,1,1); dxcyv = 0.5*(dxc(:,1:end-1)+dxc(:,2:end));            % variable dx center
dyc = diff(YC2v,1,2); dycxv = 0.5*(dyc(1:end-1,:)+dyc(2:end,:));            % variable dy center
%% Initial conditions
Pt      =      zeros(nx  ,ny  );                                            % Pressure
Vx      =      zeros(nx+1,ny  );                                            % x velocity
Vy      =      zeros(nx  ,ny+1);                                            % y velocity
mus     =  mus0*ones(nx  ,ny  );                                            % viscosity at cell center
musv    =  mus0*ones(nx+1,ny+1);                                            % viscosity at vertex
mus( sqrt(XC2v.^2 + YC2v.^2) < rad) = mus_i;                                % define inclusion
musv(sqrt(XV2v.^2 + YV2v.^2) < rad) = mus_i;
mus_axy = musv(2:end-1, 2:end-1);
%% Numbering Pt and Vx,Vy
NumVx   = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy   = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));   % G stands for Gobal numbering
NumPt   = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));  % G stands for Gobal numbering
cpu(1)=toc;
%% DANI analytical solution
tic
[ Vx_W, Vx_E, Vx_S, Vx_N, Vy_W, Vy_E, Vy_S, Vy_N, Pa, Vxa, Vya ] = Dani_Solution_vec(xvv,yvv,xcv,ycv,rad,mus_i ,nx,ny);
cpu(2)=toc; display(['Time Dani sol     = ', num2str(cpu(2))]);
%% Boundary Conditions on velocities [W E S N]
tic
ibcVxW = NumVx(1,:)';        ibcVxE = NumVx(end,:)';                        % Indexes of BC nodes for Vx East/West
ibcVxS = NumVx(2:end-1,1);   ibcVxN = NumVx(2:end-1,end);                   % Indexes of BC nodes for Vx North/South
ibcVyS = NumVyG(:,1);        ibcVyN = NumVyG(:,end);                        % Indexes of BC nodes for Vy North/South
ibcVyW = NumVyG(1,2:end-1)'; ibcVyE = NumVyG(end,2:end-1)';                 % Indexes of BC nodes for Vy East/West
ibc    = [ ibcVxW; ibcVxE; ibcVyS; ibcVyN ];                                % Group all indexes
ibcNC  = [ ibcVxS; ibcVxN; ibcVyW; ibcVyE ];                                % Non Confroming to the physical boundary
vBc    = [ Vx_W;          Vx_E;          Vy_S;          Vy_N         ];     % Group all values
vBcNC  = [ Vx_S(2:end-1); Vx_N(2:end-1); Vy_W(2:end-1); Vy_E(2:end-1)];     % Non Confroming to the physical boundary
cpu(3)=toc;
%% grad and div blocs
tic
iVxC   = NumVx;                                                             % dP/dx
iPtW   =  ones(size(iVxC)); iPtW(1:end-1,:) = NumPt;
iPtE   =  ones(size(iVxC)); iPtE(2:end  ,:) = NumPt;
dxW    =  ones(size(iVxC));  dxW(2:end-1,:) = dxc;
dxE    =  ones(size(iVxC));  dxE(2:end-1,:) = dxc;
dxC    =  ones(size(Vx));    dxC(2:end-1,:) = dxc;
dyC    =  ones(size(Vx));    dyC            = dyv;
cvgx   = dxC.*dyC;
cPtW   =  ones(size(iVxC))./dxW.*cvgx; cPtW([1 end],:) = 0;
cPtE   = -ones(size(iVxC))./dxE.*cvgx; cPtE([1 end],:) = 0;
Idx    = [ iVxC(:); iVxC(:) ]';
Jdx    = [ iPtW(:); iPtE(:) ]';
Vdx    = [ cPtW(:); cPtE(:) ]';
iVyC   = NumVyG;                                                            % dP/dy
iPtS   =  ones(size(iVyC)); iPtS(:,1:end-1) = NumPt;
iPtN   =  ones(size(iVyC)); iPtN(:,2:end  ) = NumPt;
dyS    =  ones(size(iVyC));  dyS(:,2:end-1) = dyc;
dyN    =  ones(size(iVyC));  dyN(:,2:end-1) = dyc;
dxC    =  ones(size(Vy));    dxC            = dxv;
dyC    =  ones(size(Vy));    dyC(:,2:end-1) = dyc;
cvgy   = dyC.*dxC;
cPtS   =  ones(size(iVyC))./dyS.*cvgy; cPtS(:,[1 end]) = 0;
cPtN   = -ones(size(iVyC))./dyN.*cvgy; cPtN(:,[1 end]) = 0;
Idy    = [ iVyC(:); iVyC(:) ]';
Jdy    = [ iPtS(:); iPtN(:) ]';
Vdy    = [ cPtS(:); cPtN(:) ]';
%% PP block
iPt    = NumPt;  I = iPt(:)';  J = I;                                       % Eq. index center (pressure diagonal)
V      = ones(nx*ny,1)./gamma;                                              % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V); else                                % Matrix assembly
                   PP =  sparse(I,J,V); end
%% Block UU
mus_W  = zeros(size(Vx));  mus_W(2:end  , :     ) = mus;                    % Viscosities (W,E,S,N)
mus_E  = zeros(size(Vx));  mus_E(1:end-1, :     ) = mus;
mus_S  = zeros(size(Vx));  mus_S(2:end-1,2:end  ) = mus_axy;
mus_N  = zeros(size(Vx));  mus_N(2:end-1,1:end-1) = mus_axy;
dxW    =  ones(size(Vx));    dxW(2:end  , :     ) = dxvyc;                  % dx arrays (W,E,C)
dxE    =  ones(size(Vx));    dxE(1:end-1, :     ) = dxvyc;
dxC    =  ones(size(Vx));    dxC(2:end-1, :     ) = dxc;
dyS    =  ones(size(Vx));    dyS(2:end-1,2:end  ) = dycxv;                  % dy arrays (S,N,C)
dyN    =  ones(size(Vx));    dyN(2:end-1,1:end-1) = dycxv;
dyC    =  ones(size(Vx));    dyC                  = dyv;
iVx    = NumVx;                                                             % Eq. index for Vx (C,W,E,S,N)
iVxW   =  ones(size(Vx));  iVxW(2:end  ,:) = NumVx(1:end-1,:);
iVxS   =  ones(size(Vx));  iVxS(:,2:end  ) = NumVx(:,1:end-1);
cvx    = dxC.*dyC;                                                          % cell area scaling coeff.
cVxC   = ((mus_W./dxW + mus_E./dxE)./dxC + (mus_S./dyS + mus_N./dyN)./dyC).*cvx; % Center coeff.
scVx   = max(cVxC(:));                      cVxC([1,end],:) = scVx;         % Scaling factor for Vx Dirichlet values
cVxW   = -mus_W./dxW./dxC.*cvx;             cVxW([2,end],:) = 0;            % West coeff.
cVxS   = -mus_S./dyS./dyC.*cvx;             cVxS(:      ,1) = 0;            % South coeff.
Iuu    = [  iVx(:);  iVx(:);  iVx(:) ]';                                    % Triplets [I,J,V]
Juu    = [  iVx(:); iVxW(:); iVxS(:) ]';
Vuu    = [ cVxC(:); cVxW(:); cVxS(:) ]';
%% Block VV
mus_W  = zeros(size(Vy));  mus_W(2:end  ,2:end-1) = mus_axy;                % Viscosities (W,E,S,N)
mus_E  = zeros(size(Vy));  mus_E(1:end-1,2:end-1) = mus_axy;
mus_S  = zeros(size(Vy));  mus_S( :     ,2:end  ) = mus;
mus_N  = zeros(size(Vy));  mus_N( :     ,1:end-1) = mus;
dxW    =  ones(size(Vy));    dxW(2:end  ,2:end-1) = dxcyv;                  % dx arrays (W,E,C)
dxE    =  ones(size(Vy));    dxE(1:end-1,2:end-1) = dxcyv;
dxC    =  ones(size(Vy));    dxC                  = dxv;
dyS    =  ones(size(Vy));    dyS( :     ,2:end  ) = dyvxc;                  % dy arrays (S,N,C)
dyN    =  ones(size(Vy));    dyN( :     ,1:end-1) = dyvxc;
dyC    =  ones(size(Vy));    dyC( :     ,2:end-1) = dyc;
iVy    = NumVyG;                                                            % Eq. index for Vy (C,W,E,S,N)
iVyW   =  ones(size(Vy));  iVyW(2:end  ,:) = NumVyG(1:end-1,:);
iVyS   =  ones(size(Vy));  iVyS(:,2:end  ) = NumVyG(:,1:end-1);
cvy    = dxC.*dyC;                                                          % cell area scaling coeff.
cVyC   = ((mus_W./dxW + mus_E./dxE)./dxC + (mus_S./dyS + mus_N./dyN)./dyC).*cvy; % Center coeff.
scVy   = max(cVyC(:));                      cVyC(:,[1,end]) = scVy;         % Scaling factor for Vx Dirichlet values
cVyW   = -mus_W./dxW./dxC.*cvy;             cVyW(1, :     ) = 0;            % West coeff.
cVyS   = -mus_S./dyS./dyC.*cvy;             cVyS(:,[2 end]) = 0;            % South coeff.
Ivv    = [  iVy(:);  iVy(:);  iVy(:) ]';                                    % Triplets [I,J,V]
Jvv    = [  iVy(:); iVyW(:); iVyS(:) ]';
Vvv    = [ cVyC(:); cVyW(:); cVyS(:) ]';
%% Block VU
mus_W  = zeros(size(Vy));  mus_W( : , :     ) = musv(1:end-1, :);           % Viscosities (W,E,S,N)
mus_E  = zeros(size(Vy));  mus_E( : , :     ) = musv(2:end  , :);
mus_S  = zeros(size(Vy));  mus_S( : ,2:end  ) = mus;
mus_N  = zeros(size(Vy));  mus_N( : ,1:end-1) = mus;
dxyC   = dxv( : ,2:end-1).*dyc;                                             % dxy arrays (C)
iVy    = NumVyG( :    ,2:end-1);                                            % Eq. index for VyC
iVxSW  = NumVx(1:end-1,1:end-1);                                            % Eq. index for Vx (SW,SE,NW,NE)
iVxSE  = NumVx(2:end  ,1:end-1);
iVxNW  = NumVx(1:end-1,2:end  );
iVxNE  = NumVx(2:end  ,2:end  );
cvyx   = dxyC;                                                              % cell area scaling coeff.
cVxSW  = (-mus_W(:,2:end-1) + mus_S(:,2:end-1))./dxyC.*cvyx;  cVxSW(1  ,:) = 0; % Coeff. for Vx (SW,SE,NW,NE)
cVxSE  = ( mus_E(:,2:end-1) - mus_S(:,2:end-1))./dxyC.*cvyx;  cVxSE(end,:) = 0;
cVxNW  = ( mus_W(:,2:end-1) - mus_N(:,2:end-1))./dxyC.*cvyx;  cVxNW(1  ,:) = 0;
cVxNE  = (-mus_E(:,2:end-1) + mus_N(:,2:end-1))./dxyC.*cvyx;  cVxNE(end,:) = 0;
Ivu    = [   iVy(:);   iVy(:);   iVy(:);   iVy(:) ]';                       % Triplets [I,J,V]
Jvu    = [ iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:) ]';
Vvu    = [ cVxSW(:); cVxSE(:); cVxNW(:); cVxNE(:) ];
cpu(4)=toc; display(['Time Build Blocks = ', num2str(cpu(4))]);
%% Assemble Blocs
tic
if SuiteSparse==1, K    = sparse2( [Iuu(:); Ivv(:); Ivu(:)], [Juu(:); Jvv(:); Jvu(:)], [Vuu(:); Vvv(:); Vvu(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
                   grad = sparse2( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny );                                      else
                   K    =  sparse( [Iuu(:); Ivv(:); Ivu(:)], [Juu(:); Jvv(:); Jvu(:)], [Vuu(:); Vvv(:); Vvu(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
                   grad =  sparse( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny );                                      end
div  = -grad';
cpu(5)=toc; display(['Time Assemble     = ', num2str(cpu(5))]);
%% BC's on K and DivV
tic
dxW   = ones(size(Vx));  dxW(2:end  , :     ) = dxvyc;                      % dx arrays (W,E,C)
dxE   = ones(size(Vx));  dxE(1:end-1, :     ) = dxvyc;
dxC   = ones(size(Vx));  dxC(2:end-1, :     ) = dxc;
dyS   = ones(size(Vy));  dyS( :     ,2:end  ) = dyvxc;                      % dy arrays (S,N,C)
dyN   = ones(size(Vy));  dyN( :     ,1:end-1) = dyvxc;
dyC   = ones(size(Vy));  dyC( :     ,2:end-1) = dyc;
dxyCx = dxv( : ,2:end-1).*dyc;                                              % cell area scaling coeff.
dxyCy = dyv(2:end-1, : ).*dxc;                                              % cell area scaling coeff.
BcK                  = zeros(size(K,1),1);
BcK(NumVx(2    ,:))  = BcK(NumVx(2    ,:))  + mus(1  ,:  )'./dxW(2    ,:)'./dxC(2    ,:)'.*Vx_W.*cvx(2    ,:)';
BcK(NumVx(end-1,:))  = BcK(NumVx(end-1,:))  + mus(end,:  )'./dxE(end-1,:)'./dxC(end-1,:)'.*Vx_E.*cvx(end-1,:)';
BcK(NumVyG(:, 2   )) = BcK(NumVyG(:, 2   )) + mus(:  ,1  ) ./dyS(:,2    ) ./dyC(:,2    ) .*Vy_S.*cvy(:,2    );
BcK(NumVyG(:,end-1)) = BcK(NumVyG(:,end-1)) + mus(:  ,end) ./dyN(:,end-1) ./dyC(:,end-1) .*Vy_N.*cvy(:,end-1);
BcK(ibcVxN) = BcK(ibcVxN) +  (  mus(1:end-1,end) - musv(2:end-1,end) )./dxyCy(:,end).*Vy_N(1:end-1) .*dxyCy(:,end);   % VyNW  % DEBUG: Needed if variable visc on boundaries
BcK(ibcVxN) = BcK(ibcVxN) +  ( -mus(2:end  ,end) + musv(2:end-1,end) )./dxyCy(:,end).*Vy_N(2:end  ) .*dxyCy(:,end);   % VyNE
BcK(ibcVxS) = BcK(ibcVxS) +  ( -mus(1:end-1,1  ) + musv(2:end-1, 1 ) )./dxyCy(:,1  ).*Vy_S(1:end-1) .*dxyCy(:,1  );   % VySW
BcK(ibcVxS) = BcK(ibcVxS) +  (  mus(2:end  ,1  ) - musv(2:end-1, 1 ) )./dxyCy(:,1  ).*Vy_S(2:end  ) .*dxyCy(:,1  );   % VySE
BcK(ibcVyE) = BcK(ibcVyE) + ((  mus(end,1:end-1) - musv(end,2:end-1) )./dxyCx(end,:).*Vx_E(1:end-1)'.*dxyCx(end,:))'; % VxSE
BcK(ibcVyE) = BcK(ibcVyE) + (( -mus(end,2:end  ) + musv(end,2:end-1) )./dxyCx(end,:).*Vx_E(2:end  )'.*dxyCx(end,:))'; % VxNE
BcK(ibcVyW) = BcK(ibcVyW) + (( -mus(1  ,1:end-1) + musv(1  ,2:end-1) )./dxyCx(1  ,:).*Vx_W(1:end-1)'.*dxyCx(1  ,:))'; % VxSW
BcK(ibcVyW) = BcK(ibcVyW) + ((  mus(1  ,2:end  ) - musv(1  ,2:end-1) )./dxyCx(1  ,:).*Vx_W(2:end  )'.*dxyCx(1,  :))'; % VxNW
% Conforming Dirichlets
BcK([ibcVxW; ibcVxE; ibcVyS; ibcVyN]) = [Vx_W*scVx; Vx_E*scVx; Vy_S*scVy; Vy_N*scVy];
% Non-conforming Dirichlets
cNC        = [mus_axy(:,1)./dyv(2:end-1,1)./dyv(2:end-1,1).*dxyCy(:,1);    mus_axy(:,end)./dyv(2:end-1,end)./dyv(2:end-1,end).*dxyCy(:,end)    ; ...
             (mus_axy(1,:)./dxv(1,2:end-1)./dxv(1,2:end-1).*dxyCx(1,:))'; (mus_axy(end,:)./dxv(end,2:end-1)./dxv(end,2:end-1).*dxyCx(end,:) )' ];
d0         = spdiags(K,0);
d0(ibcNC)  = d0(ibcNC)  + 2*cNC;
BcK(ibcNC) = BcK(ibcNC) + 2*cNC.*vBcNC;
K          = spdiags(d0,0,K);
% BC on div
dxy = dxvyc.*dyvxc;
BcD                 = zeros(size(div,1),1);
BcD(NumPt(1  ,:  )) = BcD(NumPt(1  ,:  )) + 1./dxvyc(1  ,:)'.*Vx_W.*dxy(1  ,:)';
BcD(NumPt(end,:  )) = BcD(NumPt(end,:  )) - 1./dxvyc(end,:)'.*Vx_E.*dxy(end,:)';
BcD(NumPt( : ,1  )) = BcD(NumPt(:  ,1  )) + 1./dyvxc(:,1  ) .*Vy_S.*dxy(:  ,1) ;
BcD(NumPt( : ,end)) = BcD(NumPt(:  ,end)) - 1./dyvxc(:,end) .*Vy_N.*dxy(:,end) ;
cpu(6)=toc; display(['Time BC           = ', num2str(cpu(6))]);
%% Prepare solve
tic
K = K + K' - diag(diag(K));                                                 % Build full from tri-lower (for checking)
cpu(7)=toc;
tic
if solver==0                                                                % Build M_Stokes
    Ms  = [ K   , -div' ; ...
            div ,  PP   ];
    cpu(8)=toc; display(['Time Build Ms     = ', num2str(cpu(8))]);
else
    PPI      = spdiags(1./diag(PP),0,PP);
    Kt       = K - grad*(PPI*div);                                          % PPI*DivV = PP\DivV
    [Kc,e,s] = chol(Kt,'lower','vector');                                   % Cholesky factorization
    if e~=0, error('non symmetric system'); end;
    cpu(8)=toc; display(['Time CHOLESKY     = ', num2str(cpu(8))]);
end
tic
u   = [Vx(:) ; Vy(:)];
p   =  Pt(:);
up  = [ u ; p ];
cpu(9)=toc;
%% Solve
tic
for it=1:niter
    if solver==0,
        up  = Ms\[ BcK  ; BcD  + PP *up(NumPtG(:))];                        % Backslash
        du  = BcK -   K*up([NumVx(:); NumVyG(:)]) - grad*up(NumPtG(:));
        dp  = BcD - div*up([NumVx(:); NumVyG(:)]);
    else
        Rhs =      BcK  - grad*(PPI*BcD + p);                               % Powell-Hestenes
        if SuiteSparse==1, u(s) = cs_ltsolve(Kc,cs_lsolve(Kc,Rhs(s))); else % Powell-Hestenes
                           u(s) = Kc'\(Kc\Rhs(s));                     end  % Powell-Hestenes Matlab
        dp  = BcD - div*u;
        dp  = dp - mean(dp(:));
        p   = p + PPI*(dp);
        du  = BcK -   K*u - grad*p;
    end
    if noisy==1, fprintf('  --- iteration %d --- \n',it);
                 fprintf('   Res. |du| = %2.2e \n',norm(du)/length(du));
                 fprintf('   Res. |dp| = %2.2e \n',norm(dp)/length(dp)); end
    if norm(dp)/length(dp) < tol_lin, break, end
end%it
cpu(10)=toc; display(['Time Backsubs     = ', num2str(cpu(10))]);
tic
XPH = [u ; p];
XBS = up; 
%% Post-processing
if solver==0, up = XBS; else up = XPH; end
Pt  = reshape(up(NumPtG(:)),[nx  ,ny  ]); Pt = Pt - mean(Pt(:));
Vx  = reshape(up(NumVx(:)) ,[nx+1,ny  ]);
Vy  = reshape(up(NumVyG(:)),[nx  ,ny+1]);
u   = up([NumVx(:);NumVyG(:)]);
% Check residuals % errors
fuv = BcK -   K*u - grad*Pt(:);
fpt = BcD - div*u;
error_mom = norm(fuv)/length(fuv); display([' Error momentum        = ', num2str(error_mom)]);
error_div = norm(fpt)/length(fpt); display([' Error divergence      = ', num2str(error_div)]);
Pre = abs(Pt-Pa);
Vxe = abs(Vx-Vxa);
Vye = abs(Vy-Vya);
L1PrErr2 = sum(sum(Pre.*dxvyc.*dyvxc))                  ./sum(sum(ones(size(Pre)).*dxvyc.*dyvxc));                   display([' Check Pressure vs ANALYTICAL = ', num2str(L1PrErr2)]);
L1VxErr2 = sum(sum(Vxe.*[dxc(1,:);dxc;dxc(end,:)].*dyv))./sum(sum(ones(size(Vxe)).*[dxc(1,:);dxc;dxc(end,:)].*dyv)); display([' Check Vx vs ANALYTICAL       = ', num2str(L1VxErr2)]);
L1VyErr2 = sum(sum(Vye.*dxv.*[dyc(:,1),dyc,dyc(:,end)]))./sum(sum(ones(size(Vye)).*dxv.*[dyc(:,1),dyc,dyc(:,end)])); display([' Check Vy vs ANALYTICAL       = ', num2str(L1VyErr2)]);
%% Vizualization
% Generate vertex coordinates for Vx control volumes
[xvx2,yvx2] = ndgrid([xcv(1)-dxv(1,1) xcv xcv(end)-dxv(end,1)],yvv);
X3 = xvx2(2:end  ,2:end  ); Y3 = yvx2(2:end  ,2:end  );
X4 = xvx2(1:end-1,2:end  ); Y4 = yvx2(1:end-1,2:end  );
X1 = xvx2(1:end-1,1:end-1); Y1 = yvx2(1:end-1,1:end-1);
X2 = xvx2(2:end  ,1:end-1); Y2 = yvx2(2:end  ,1:end-1);
Vx_x = [X1(:) X2(:) X3(:) X4(:)];
Vx_y = [Y1(:) Y2(:) Y3(:) Y4(:)];
% Generate vertex coordinates for Vy control volumes
[xvy2,yvy2] = ndgrid(xvv,[ycv(1)-dyv(1,1) ycv ycv(end)-dyv(1,end)]);
X3 = xvy2(2:end  ,2:end  ); Y3 = yvy2(2:end  ,2:end  );
X4 = xvy2(1:end-1,2:end  ); Y4 = yvy2(1:end-1,2:end  );
X1 = xvy2(1:end-1,1:end-1); Y1 = yvy2(1:end-1,1:end-1);
X2 = xvy2(2:end  ,1:end-1); Y2 = yvy2(2:end  ,1:end-1);
Vy_x = [X1(:) X2(:) X3(:) X4(:)];
Vy_y = [Y1(:) Y2(:) Y3(:) Y4(:)];
% Generate vertex coordinates for pressure control volumes
X1 = XV2v(1:end-1,1:end-1); Y1 = YV2v(1:end-1, 1:end-1);
X2 = XV2v(2:end  ,1:end-1); Y2 = YV2v(2:end  , 1:end-1);
X3 = XV2v(2:end  ,2:end  ); Y3 = YV2v(2:end  , 2:end  );
X4 = XV2v(1:end-1,2:end  ); Y4 = YV2v(1:end-1, 2:end  );
Pr_x = [X1(:) X2(:) X3(:) X4(:)];
Pr_y = [Y1(:) Y2(:) Y3(:) Y4(:)];

figure(1),clf,colormap('jet'),set(gcf,'Color','white')
subplot(331),patch(Pr_x', Pr_y', Pa(:)' ),shading flat,colorbar,axis image,ylabel('Pressure'),title('Analytic')
subplot(334),patch(Vx_x', Vx_y', Vxa(:)'),shading flat,colorbar,axis image,ylabel('X velocity')
subplot(337),patch(Vy_x', Vy_y', Vya(:)'),shading flat,colorbar,axis image,ylabel('Y velocity')
subplot(332),patch(Pr_x', Pr_y', Pt(:)' ),shading flat,colorbar,axis image,caxis([min(Pa(:)) max(Pa(:))]),title('Numeric')
subplot(335),patch(Vx_x', Vx_y', Vx(:)' ),shading flat,colorbar,axis image
subplot(338),patch(Vy_x', Vy_y', Vy(:)' ),shading flat,colorbar,axis image
subplot(333),patch(Pr_x', Pr_y', Pre(:)'),shading flat,colorbar,axis image,title('error')
subplot(336),patch(Vx_x', Vx_y', Vxe(:)'),shading flat,colorbar,axis image
subplot(339),patch(Vy_x', Vy_y', Vye(:)'),shading flat,colorbar,axis image,drawnow

cpu(11)=toc; display(['WALL TIME = ', num2str(sum(cpu(1:10))),' seconds (without postprocessing)']);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    FUNCTION USED IN MAIN CODE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [ Vx_W, Vx_E, Vx_S, Vx_N, Vy_W, Vy_E, Vy_S, Vy_N, Pa, Vxa, Vya ] = Dani_Solution_vec(xv,yv,xc,yc,rad,mus_i,nx,ny)
% -------------------------------------------------------------------------
% ANALYTICAL SOLUTION - PRESSURE AND VELOCITY AROUND A CIRCULAR INCLUSION
% BASED ON DANI SCHMID'S 2002 CYL_P_MATRIX.M
% Vectorised version by:
% Thibault Duretz, Ludovic Raess - Unil 2016
% -------------------------------------------------------------------------
% INPUT:
gr  =  0;                       % Simple shear: gr=1, er=0
er  = -1;                       % Strain rate
mm  =  1;                       % Viscosity of matrix
mc  = mus_i;
A   = mm.*(mc-mm)./(mc+mm);
i   = sqrt(-1);
%-------------------------
Pa  = zeros(nx  ,ny  );
Vxa = zeros(nx+1,ny  );
Vya = zeros(nx  ,ny+1);
% PRESSURE
[XC2, YC2]   = ndgrid(xc, yc);
Z            = XC2 + i*YC2;
PH           = zeros(nx,ny);
PH( XC2.^2 + YC2.^2 <= rad.^2 ) = 1;
P            = -2.*mm.*(mc-mm)./(mc+mm).*real(rad^2./Z.^2.*(i*gr+2*er));  % outside inclusion
Pa(PH==0)    = P(PH==0);
% Conforming Nodes --------------------------------------------------------
% VELOCITY X
[XV2, YC2]   = ndgrid(xv, yc);
Z            = XV2 + i*YC2;
PH           = zeros(nx+1,ny);
PH( XV2.^2 + YC2.^2 <= rad.^2 ) = 1;
V_tot        = (mm/(mc+mm))*(i*gr+2*er)*conj(Z)-(i/2)*gr*Z; % inside inclusion
Vxa(PH==1)   = real(V_tot(PH==1));
phi_z        = -(i/2)*mm*gr*Z-(i*gr+2*er)*A*rad^2*Z.^(-1);  % outside inclusion
d_phi_z      = -(i/2)*mm*gr + (i*gr+2*er)*A*rad^2./Z.^2;
conj_d_phi_z = conj(d_phi_z);
psi_z        = (i*gr-2*er)*mm*Z-(i*gr+2*er)*A*rad^4*Z.^(-3);
conj_psi_z   = conj(psi_z);
V_tot        = (phi_z- Z.*conj_d_phi_z - conj_psi_z) / (2*mm);
Vxa(PH==0)   = real(V_tot(PH==0));
% VELOCITY Y
[XC2, YV2]   = ndgrid(xc, yv);
Z            = XC2 + i*YV2;
PH           = zeros(nx,ny+1);
PH( XC2.^2 + YV2.^2 <= rad.^2 ) = 1;
V_tot        = (mm/(mc+mm))*(i*gr+2*er)*conj(Z)-(i/2)*gr*Z; % inside inclusion
Vya(PH==1)   = imag(V_tot(PH==1));
phi_z        = -(i/2)*mm*gr*Z-(i*gr+2*er)*A*rad^2*Z.^(-1);  % outside inclusion
d_phi_z      = -(i/2)*mm*gr + (i*gr+2*er)*A*rad^2./Z.^2;
conj_d_phi_z = conj(d_phi_z);
psi_z        = (i*gr-2*er)*mm*Z-(i*gr+2*er)*A*rad^4*Z.^(-3);
conj_psi_z   = conj(psi_z);
V_tot        = (phi_z- Z.*conj_d_phi_z - conj_psi_z) / (2*mm);
Vya(PH==0)   = imag(V_tot(PH==0));
% Get BC
Vx_W  = Vxa(1   , :  )';
Vx_E  = Vxa(end , :  )';
Vy_S  = Vya(:   ,1   );
Vy_N  = Vya(:   ,end );
% Non Conforming Nodes ----------------------------------------------------
Vx_NC = zeros(nx+1,ny+1);
Vy_NC = zeros(nx+1,ny+1);
Vx_S  = zeros(nx+1,1   );
Vx_N  = zeros(nx+1,1   );
Vy_W  = zeros(1   ,ny+1)';
Vy_E  = zeros(1   ,ny+1)';
% VELOCITY X & Y -
[XV2, YV2]   = ndgrid(xv, yv);
Z            = XV2 + i*YV2;
PH           = zeros(nx+1,ny+1);
PH( XV2.^2 + YV2.^2 <= rad.^2 ) = 1;
V_tot        = (mm/(mc+mm))*(i*gr+2*er)*conj(Z)-(i/2)*gr*Z; % inside inclusion
Vx_NC(PH==1) = real(V_tot(PH==1));
Vy_NC(PH==1) = imag(V_tot(PH==1));
phi_z        = -(i/2)*mm*gr*Z-(i*gr+2*er)*A*rad^2*Z.^(-1);  % outside inclusion
d_phi_z      = -(i/2)*mm*gr + (i*gr+2*er)*A*rad^2./Z.^2;
conj_d_phi_z = conj(d_phi_z);
psi_z        = (i*gr-2*er)*mm*Z-(i*gr+2*er)*A*rad^4*Z.^(-3);
conj_psi_z   = conj(psi_z);
V_tot        = (phi_z- Z.*conj_d_phi_z - conj_psi_z) / (2*mm);
Vx_NC(PH==0) = real(V_tot(PH==0));
Vy_NC(PH==0) = imag(V_tot(PH==0));
% Get BC
Vx_S(2:end-1,1) = Vx_NC(2:end-1,1  );
Vx_N(2:end-1,1) = Vx_NC(2:end-1,end);
Vy_W(2:end-1,1) = Vy_NC(1  ,2:end-1)'; 
Vy_E(2:end-1,1) = Vy_NC(end,2:end-1)'; 
end
