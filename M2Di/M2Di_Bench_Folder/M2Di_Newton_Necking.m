% =========================================================================
% Main function used to run the M2Di Newton Necking benchmark.

% Copyright (C) 2017  Ludovic Raess, Thibault Duretz, Yury podladchikov

% This file is part of M2Di.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
function M2Di_Newton_Necking

SuiteSparse = 0;                                                             % 0=NO SuiteSparse  1=SuiteSparse
noisy       = 2;                                                             % 0=NO Output       1=Std output   2=Very noisy  3=Very noisy + kspgcr output
KRO         = 1;                                                             % Carreau switch
INV         = 1;                                                             % eII invariant definition: 0=sum_of_squares  1=square_of_sum
%%
fact    = 1/2;                                                               % Resolution reduction
ny_in   = 480*fact;                                                          % Initial resolution
Newt    = 1;                                                                 % 0=Picard    1=Newton
H       = 1;                                                                 % Height of the layer
A0      = 0.05;                                                              % Layer amplitude perturbation
L       = [3 5 10 20 30];                                                    % Lambda at which growthrate is evaluated
q       = zeros(size(L));
q2      = zeros(size(L));

for i = 1:length(L)
    fprintf('***************************\n');
    fprintf('********** L = %2.0f ********* \n', L(i));
    fprintf('***************************\n');
    nx_in = floor(ny_in*L(i)/3/2);                                           % Aspect ration of the box
    fprintf('\n> nx_in = %2.2f , ny_in = %2.2f \n', nx_in, ny_in);
    % call flow solver
    [cpu, iter, q(i), q2(i)] = M2Di_Newton(nx_in,ny_in,Newt,L(i),H,A0, SuiteSparse,noisy,KRO,INV);
    
end
% Load Folder solution
GrowthRatesFolder = load('Folder_N.mat');
% Postprocessing
figure(10),clf,colormap('jet'),set(gcf,'Color','white')
hold on
plot(GrowthRatesFolder.GR_Neck_N3_Nm1.Numerics.x,        GrowthRatesFolder.GR_Neck_N3_Nm1.Numerics.y,        'xk','MarkerSize',8)
plot(GrowthRatesFolder.GR_Neck_N3_Nm1.Thick_plate_FMT.x, GrowthRatesFolder.GR_Neck_N3_Nm1.Thick_plate_FMT.y, '-k')
plot(L./H, q,  '*','MarkerSize',8);
plot(L./H, q2, '+','MarkerSize',8);
xlabel('\lambda')
ylabel('\alpha')
drawnow

end

function [ cpu,iter,q,q2 ] = M2Di_Newton(nx_in,ny_in,Newt,L,Lth,A0, SuiteSparse,noisy,KRO,INV)
% M2Di Newton: Thibault Duretz, Ludovic Raess, Yuri Podladchikov  -  Unil 2017
Newton      = Newt;                                                          % 0=Picard          1=Newton
comp        = 0;                                                             % 1=Compressible    0=Incompressible
pls         = 2;                                                             % 2= 1/2*div        3= 1/3*div  (plane strain-rate or stess)
%% Physics
xmin        =-L/2;                                                           % min x
xmax        = L/2;                                                           % max x
ymin        =-3;                                                             % min y
ymax        = 3;                                                             % max y
Rg          = 1;                                                             % Gas constant
r           = 0.05;                                                          % Inclusion radius
Ebg         = 1;                                                             % Background strain rate
Tbg         = 1;                                                             % Background temperature
% Material 1 - MATRIX
ndis(1)     = 2;                                                             % Power Law exponent
Qdis(1)     = 1;                                                             % Activation energy
Adis(1)     = 1^ndis(1)*exp(Qdis(1)/Rg/Tbg);                                 % Pre-exponent (Power law model)
mu_r(1)     = 1*exp(-Qdis(1)/ndis(1)/Rg/Tbg);                                % Reference viscosity (Carreau model) 
mu_0(1)     = 1e0*mu_r(1);                                                   % Viscosity for zero strain rate (Carreau model) 
mu_i(1)     = 1e0*mu_r(1);                                                   % Viscosity for infinite strain rate (Carreau model) 
ksi(1)      = (mu_r(1)/(mu_0(1)-mu_i(1)))^(1/(1/ndis(1)-1));
% Material 2 - INCLUSION
ndis(2)     = 3;
Qdis(2)     = 1;
Adis(2)     = (1/200)^ndis(2)*exp(Qdis(2)/Rg/Tbg);
mu_r(2)     = 200*exp(-Qdis(2)/ndis(2)/Rg/Tbg);
mu_0(2)     = 1e3*mu_r(2);
mu_i(2)     = 1e-10*mu_r(2);
ksi(2)      = (mu_r(2)/(mu_0(2)-mu_i(2)))^(1/(1/ndis(2)-1));
%% Numerics
nx          = nx_in;                                                         % Number of cells in x
ny          = ny_in;                                                         % Number of cells in y
% Non-linear solver settings
nmax_glob   = 100;                                                           % Max. number of non-linear iterations
tol_glob    = 1e-8;                                                          % Global nonlinear tolerance
eps_kspgcr  = 5e-4;                                                          % KSPGCR solver tolerance
nitPicNewt  = 0;                                                             % Number of Picard iterations prior to Newton iterations
LineSearch  = 1;                                                             % Line search algorithm
alpha       = 1.0;                                                           % Default step (in case LineSearch = 0)
% Linear solver settings
lsolver     = 1;                                                             % 0=Backslash 1=Powell-Hestenes
gamma       = 1e-5;                                                          % Numerical compressibility
nPH         = 30;                                                            % Max. number of linear iterations 
tol_linu    = tol_glob/5000;                                                 % Velocity tolerance  
tol_linp    = tol_glob/1000;                                                 % Divergence tolerance 
%% Dimensionalisation 
h     = 1; % Material phase used for scaling
% Characterictic values
if KRO==0, muc = Adis(h)^(-1/ndis(h))*Ebg^(1/ndis(h)-1)*exp(Qdis(h)/ndis(h)/Rg/Tbg); end
if KRO==1, muc = mu_i(h) + (mu_0(h) - mu_i(h)) * ( 1  + (ksi(h)*Ebg).^2 ).^(0.5*(1/ndis(h)-1))*exp(Qdis(h)/ndis(h)/Rg/Tbg); end
Tc    = Qdis(h)/Rg;                                                          % Kelvins
Lc    = 1;%(xmax-xmin);                                                      % Meters
tc    = 1/Ebg;                                                               % Seconds
tauc  = muc*(1/tc);                                                          % Pascals
mc    = tauc*Lc*tc^2;                                                        % Kilograms
Jc    = mc*Lc^2/tc^2;                                                        % Joules
% Scaling
Adis  = Adis./(tauc.^-ndis.*1./tc);
Qdis  = Qdis./Jc;
Rg    = Rg/(Jc/Tc);
Ebg   = Ebg/(1/tc);
Tbg   = Tbg/Tc;
r     = r/Lc;
mu_0  = mu_0/muc;
mu_i  = mu_i/muc;
xmin  = xmin/Lc;  xmax = xmax/Lc; 
ymin  = ymin/Lc;  ymax = ymax/Lc;  
%% Preprocessing
tic 
Lx = xmax-xmin;     dx = Lx/nx;                                              % Grid spacing x
Ly = ymax-ymin;     dy = Ly/ny;                                              % Grid spacing y
xn = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;                             % Nodal coordinates x
yn = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;                             % Nodal coordinates y
[xn2,  yn2] = ndgrid(xn,yn);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xn,yc);
[xvy2,yvy2] = ndgrid(xc,yn);
rxvec  = zeros(nmax_glob,1); cpu = zeros(9,1);
%% Initial arrays
Vx     =  Ebg.*xvx2;                                                         % Initial solutions for velocity in x
Vy     = -Ebg.*yvy2;                                                         % Initial solutions for velocity in y
Pt     =    0.* xc2;                                                         % Initial solutions for pressure 
phc    =     ones(nx  ,ny  );                                                % Material phase on centroids
phv    =     ones(nx+1,ny+1);                                                % Material phase on vertices
Tec    = Tbg*ones(nx  ,ny  );                                                % Temperature on centroids
phc(yc2 > -Lth/2 + A0*cos(2*pi*xc2/L) & yc2 < Lth/2 - A0*cos(2*pi*xc2/L)) = 2;
phv(yn2 > -Lth/2 + A0*cos(2*pi*xn2/L) & yn2 < Lth/2 - A0*cos(2*pi*xn2/L)) = 2;
%% Numbering Pt and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));
cpu(1)=toc;
%% Boundary conditions on velocities [W E S N]
tic
ibcVxW = NumVx ( 1     ,:)';  ibcVxE = NumVx(end    , : )' ;                 % Indexes of BC nodes for Vx East/West
ibcVxS = NumVx (2:end-1,1);   ibcVxN = NumVx(2:end-1,end)  ;                 % Indexes of BC nodes for Vx North/South
ibcVyS = NumVyG(:,1)       ;  ibcVyN = NumVyG(:,end)       ;                 % Indexes of BC nodes for Vy North/South
ibcVyW = NumVyG(1,2:end-1)';  ibcVyE = NumVyG(end,2:end-1)';                 % Indexes of BC nodes for Vy East/West
ibc    = [ ibcVxW; ibcVxE; ibcVyS; ibcVyN ];                                 % Group all indexes
Vx_W   =  Ebg.*xvx2(1  ,:)'.*ones(1 ,ny  )';                                 % BC value Vx West
Vx_E   =  Ebg.*xvx2(end,:)'.*ones(1 ,ny  )';                                 % BC value Vx East
Vy_S   = -Ebg.*yvy2(:,  1) .*ones(nx,1   );                                  % BC value Vy South
Vy_N   = -Ebg.*yvy2(:,end) .*ones(nx,1   );                                  % BC value Vy North
cpu(2)=toc;
%% Assemble pressure gradient matrix operator
tic
iVxC   = NumVx;                                                              % dP/dx
iPtW   =  ones(size(iVxC));    iPtW(1:end-1,:) = NumPt;
iPtE   =  ones(size(iVxC));    iPtE(2:end  ,:) = NumPt;
cPtW   =  ones(size(iVxC))/dx; cPtW([1 end],:) = 0;     
cPtE   = -ones(size(iVxC))/dx; cPtE([1 end],:) = 0;
Idx    = [ iVxC(:); iVxC(:) ]';
Jdx    = [ iPtW(:); iPtE(:) ]';
Vdx    = [ cPtW(:); cPtE(:) ]';
iVyC   = NumVyG;                                                             % dP/dy
iPtS   =  ones(size(iVyC));    iPtS(:,1:end-1) = NumPt;
iPtN   =  ones(size(iVyC));    iPtN(:,2:end  ) = NumPt;
cPtS   =  ones(size(iVyC))/dy; cPtS(:,[1 end]) = 0;
cPtN   = -ones(size(iVyC))/dy; cPtN(:,[1 end]) = 0;
Idy    = [ iVyC(:); iVyC(:) ]'; 
Jdy    = [ iPtS(:); iPtN(:) ]';
Vdy    = [ cPtS(:); cPtN(:) ]';
%% Assemble grad and divV
if SuiteSparse==1, grad = sparse2( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny ); 
else               grad = sparse ( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny ); end
div    = -grad';
%% Build BC's for divergence
BcD = zeros(size(div,1),1);
BcD(NumPt( 1 , : )) = BcD(NumPt(1  ,:  )) + 1/dx*Vx_W;
BcD(NumPt(end, : )) = BcD(NumPt(end,:  )) - 1/dx*Vx_E;
BcD(NumPt(:  , 1 )) = BcD(NumPt(:  ,1  )) + 1/dy*Vy_S;
BcD(NumPt(:  ,end)) = BcD(NumPt(:  ,end)) - 1/dy*Vy_N;
cpu(3)=toc;
%% Non linear iterations
iter=0; resnlu=2*tol_glob; resnlu0=2*tol_glob;
du = [0*Vx(:); 0*Vy(:)];         dp = zeros(max(NumPt(:)),1);
% Constant 2D field (temperature perturbation) - Interpolate from centers to nodes and extrapolates boundaries
Tei = 0.25*(Tec(1:end-1,1:end-1) + Tec(2:end,1:end-1) + Tec(1:end-1,2:end) + Tec(2:end,2:end));
Tev = zeros(nx+1,ny+1);  Tev(2:end-1,2:end-1)=Tei; Tev([1,end],:)=Tev([2,end-1],:); Tev(:,[1,end])=Tev(:,[2,end-1]);
while( resnlu/resnlu0 > tol_glob && iter < nmax_glob )
    iter = iter+1; if noisy>=1, fprintf('\n *** Nonlin iter %d *** \n', iter ); end
    tic
    % Strain rate tensor components
    divV = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;      
    exx  = diff(Vx,1,1)/dx - comp/pls*divV;        
    eyy  = diff(Vy,1,2)/dy - comp/pls*divV;       
    Vx_e = [ Vx(:,1), Vx, Vx(:,end) ];                                       % expand array using BC's - Free slip
    Vy_e = [ Vy(1,:); Vy; Vy(end,:) ];                                       % expand array using BC's - Free slip
    exy  = 0.5*( diff(Vx_e,1,2)/dy + diff(Vy_e,1,1)/dx );
    % Strain rate tensor invariant II
    if INV==0
        exyn  = 0.25*( exy(1:end-1,1:end-1)+ exy(2:end,1:end-1)+ exy(1:end-1,2:end)+ exy(2:end,2:end));
        exxsi = 0.25*( exx(1:end-1,1:end-1)+ exx(2:end,1:end-1)+ exx(1:end-1,2:end)+ exx(2:end,2:end));
        eyysi = 0.25*( eyy(1:end-1,1:end-1)+ eyy(2:end,1:end-1)+ eyy(1:end-1,2:end)+ eyy(2:end,2:end));
        exxs  = zeros(nx+1,ny+1);  exxs(2:end-1,2:end-1) = exxsi;
        eyys  = zeros(nx+1,ny+1);  eyys(2:end-1,2:end-1) = eyysi;
        eiic  = sqrt( 0.5*exx .^2 + 0.5*eyy .^2 + exyn.^2 );
        eiiv  = sqrt( 0.5*exxs.^2 + 0.5*eyys.^2 + exy .^2 );
    else
        exxSW = zeros(nx+1,ny+1);  exxSW(2:end-1,2:end-1) = exx(1:end-1,1:end-1);
        exxSE = zeros(nx+1,ny+1);  exxSE(2:end-1,2:end-1) = exx(2:end,1:end-1);
        exxNW = zeros(nx+1,ny+1);  exxNW(2:end-1,2:end-1) = exx(1:end-1,2:end);
        exxNE = zeros(nx+1,ny+1);  exxNE(2:end-1,2:end-1) = exx(2:end,2:end);
        eyySW = zeros(nx+1,ny+1);  eyySW(2:end-1,2:end-1) = eyy(1:end-1,1:end-1);
        eyySE = zeros(nx+1,ny+1);  eyySE(2:end-1,2:end-1) = eyy(2:end,1:end-1);
        eyyNW = zeros(nx+1,ny+1);  eyyNW(2:end-1,2:end-1) = eyy(1:end-1,2:end);
        eyyNE = zeros(nx+1,ny+1);  eyyNE(2:end-1,2:end-1) = eyy(2:end,2:end);
        eiic  = sqrt( 0.5*exx .^2 + 0.5*eyy .^2 + 0.25*(exy(1:end-1,1:end-1).^2 + exy(2:end,1:end-1).^2 + exy(1:end-1,2:end).^2 + exy(2:end,2:end).^2) );
        eiiv  = sqrt( exy .^2 + 0.5*0.25*(exxSW.^2 + exxSE.^2 + exxNW.^2 + exxNE.^2) + 0.5*0.25*(eyySW.^2 + eyySE.^2 + eyyNW.^2 + eyyNE.^2) );
    end
    % Viscosity
    if KRO==0, etac = Adis(phc).^(-1./ndis(phc)) .* eiic.^((1./ndis(phc))-1) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
               etav = Adis(phv).^(-1./ndis(phv)) .* eiiv.^((1./ndis(phv))-1) .* exp(Qdis(phv)/Rg./Tev./ndis(phv)); end
    if KRO==1, etac = (mu_i(phc) + (mu_0(phc) - mu_i(phc)) .* ( 1  + (ksi(phc).*eiic).^2 ).^(0.5*(1./ndis(phc)-1))) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
               etav = (mu_i(phv) + (mu_0(phv) - mu_i(phv)) .* ( 1  + (ksi(phv).*eiiv).^2 ).^(0.5*(1./ndis(phv)-1))) .* exp(Qdis(phv)/Rg./Tev./ndis(phv)); end
    etav([1,end],:) = etav([2,end-1],:); etav(:,[1,end]) = etav(:,[2,end-1]);
    etai = etav(2:end-1,2:end-1);
    %% Assemble Block PP
    iPt   = NumPt;  I = iPt(:)';  J = I;                                     % Eq. index center (pressure diagonal)
    V     = gamma*ones(size(Pt(:)));                                         % Center coeff.
    if SuiteSparse==1, PP = sparse2(I,J,V);                                  % Matrix assembly
    else               PP = sparse (I,J,V); end
    PPI   = spdiags(1./diag(PP),0,PP);                                       % Trivial inverse of diagonal PP matrix
    %% Stokes Picard Operator: Block UU
    mus_W = zeros(size(Vx));  mus_W(2:end  , :     ) = etac;                 % Viscosities (W,E,S,N)
    mus_E = zeros(size(Vx));  mus_E(1:end-1, :     ) = etac;
    mus_S = zeros(size(Vx));  mus_S(2:end-1,1:end-1) = etai;
    mus_N = zeros(size(Vx));  mus_N(2:end-1,2:end  ) = etai;
    iVx   = NumVx;                                                           % Eq. index for Vx (C,W,E,S,N)
    iVxW  = NumVx(1:end-1, :     );
    iVxE  = NumVx(2:end  , :     );
    iVxS  = NumVx(2:end-1,1:end-1);
    iVxN  = NumVx(2:end-1,2:end  );
    cVxC  = ((2-comp)*mus_W+(2-comp)*mus_E)/dx/dx + (mus_S+mus_N)/dy/dy;     % Center coeff.
    scVx  = max(cVxC(:));                            cVxC([1,end],:) = scVx; % Scaling factor for Vx Dirichlet values
    cVxW  = -(2-comp)*mus_W(2:end  , :     )/dx/dx;  cVxW([1,end],:) = 0;    % West coeff.
    cVxS  =         - mus_S(2:end-1,1:end-1)/dy/dy;                          % South coeff.
    Iuu   = [  iVx(:); iVxE(:); iVxN(:) ]';                                  % Triplets [I,J,V]
    Juu   = [  iVx(:); iVxW(:); iVxS(:) ]';
    Vuu   = [ cVxC(:); cVxW(:); cVxS(:) ]';
    %% Stokes Picard Operator: Block VV
    mus_W = zeros(size(Vy));  mus_W(1:end-1,2:end-1) = etai;                 % Viscosities (W,E,S,N)
    mus_E = zeros(size(Vy));  mus_E(2:end  ,2:end-1) = etai;
    mus_S = zeros(size(Vy));  mus_S( :     ,2:end  ) = etac;
    mus_N = zeros(size(Vy));  mus_N( :     ,1:end-1) = etac;
    iVy   = NumVyG;                                                          % Eq. index for Vy (C,W,E,S,N)
    iVyW  = NumVyG(1:end-1,2:end-1);
    iVyE  = NumVyG(2:end  ,2:end-1);
    iVyS  = NumVyG( :     ,1:end-1);
    iVyN  = NumVyG( :     ,2:end  );
    cVyC  = (mus_W+mus_E)/dx/dx + ((2-comp)*mus_S+(2-comp)*mus_N)/dy/dy;     % Center coeff.
    scVy  = max(cVyC(:));                            cVyC(:,[1,end]) = scVy; % Scaling factor for Vy Dirichlet values
    cVyW  =          -mus_W(1:end-1,2:end-1)/dx/dx;                          % West coeff.
    cVyS  = -(2-comp)*mus_S( :     ,2:end  )/dy/dy;  cVyS(:,[1,end]) = 0;    % South coeff.
    Ivv   = [  iVy(:); iVyE(:); iVyN(:) ]';                                  % Triplets [I,J,V]
    Jvv   = [  iVy(:); iVyW(:); iVyS(:) ]';
    Vvv   = [ cVyC(:); cVyW(:); cVyS(:) ]';
    %% Stokes Picard Operator: Block VU
    mus_W = zeros(size(Vy));  mus_W( : , :     ) = etav(1:end-1, :);         % Viscosities (W,E,S,N)
    mus_E = zeros(size(Vy));  mus_E( : , :     ) = etav(2:end  , :);
    mus_S = zeros(size(Vy));  mus_S( : ,2:end  ) = etac;
    mus_N = zeros(size(Vy));  mus_N( : ,1:end-1) = etac;
    iVy   = NumVyG( :    ,2:end-1);                                          % Eq. index for VyC
    iVxSW = NumVx(1:end-1,1:end-1);                                          % Eq. index for Vx (SW,SE,NW,NE)
    iVxSE = NumVx(2:end  ,1:end-1);
    iVxNW = NumVx(1:end-1,2:end  );
    iVxNE = NumVx(2:end  ,2:end  );
    cVxSW = (-mus_W(:,2:end-1) + comp*mus_S(:,2:end-1))/(dx*dy); cVxSW(1  ,:) = 0; % Coeff. for Vx (SW,SE,NW,NE)
    cVxSE = ( mus_E(:,2:end-1) - comp*mus_S(:,2:end-1))/(dx*dy); cVxSE(end,:) = 0;
    cVxNW = ( mus_W(:,2:end-1) - comp*mus_N(:,2:end-1))/(dx*dy); cVxNW(1  ,:) = 0;
    cVxNE = (-mus_E(:,2:end-1) + comp*mus_N(:,2:end-1))/(dx*dy); cVxNE(end,:) = 0;
    Ivu   = [   iVy(:);   iVy(:);   iVy(:);   iVy(:) ]';                     % Triplets [I,J,V]
    Jvu   = [ iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:) ]';
    Vvu   = [ cVxSW(:); cVxSE(:); cVxNW(:); cVxNE(:) ];
    if Newton==1
        %% Stokes Newton Operator: Derivatives Vx stencil
        % Construct strain-rate stencil
        ExxW  = zeros(size(Vx)); ExxE  = zeros(size(Vx)); ExxSW = zeros(size(Vx)); ExxSE = zeros(size(Vx)); ExxNW = zeros(size(Vx)); ExxNE = zeros(size(Vx));
        EyyW  = zeros(size(Vx)); EyyE  = zeros(size(Vx)); EyySW = zeros(size(Vx)); EyySE = zeros(size(Vx)); EyyNW = zeros(size(Vx)); EyyNE = zeros(size(Vx));
        ExySW = zeros(size(Vx)); ExySE = zeros(size(Vx)); ExyNW = zeros(size(Vx)); ExyNE = zeros(size(Vx));
        ExxW(2:end,:)        = exx;                   ExxE(1:end-1,:)        = exx;
        ExxSW(2:end,2:end)   = exx(:,1:end-1);        ExxSE(1:end-1,2:end)   = exx(:,1:end-1);
        ExxNW(2:end,1:end-1) = exx(:,2:end);          ExxNE(1:end-1,1:end-1) = exx(:,2:end);
        EyyW(2:end,:)        = eyy;                   EyyE(1:end-1,:)        = eyy;
        EyySW(2:end,2:end)   = eyy(:,1:end-1);        EyySE(1:end-1,2:end)   = eyy(:,1:end-1);
        EyyNW(2:end,1:end-1) = eyy(:,2:end);          EyyNE(1:end-1,1:end-1) = eyy(:,2:end);
        ExyS                 = exy(:,1:end-1);        ExyN                   = exy(:,2:end);
        ExySW(2:end,:)       = exy(1:end-1,1:end-1);  ExySE(1:end-1,:)       = exy(2:end,1:end-1);
        ExyNW(2:end,:)       = exy(1:end-1,2:end);    ExyNE(1:end-1,:)       = exy(2:end,2:end);
        ExyW = 0.25*(ExyS+ExyN+ExySW+ExyNW); ExyE = 0.25*(ExyS+ExyN+ExySE+ExyNE); ExxS = 0.25*(ExxW+ExxE+ExxSW+ExxSE); ExxN = 0.25*(ExxW+ExxE+ExxNW+ExxNE); EyyS = 0.25*(EyyW+EyyE+EyySW+EyySE); EyyN = 0.25*(EyyW+EyyE+EyyNW+EyyNE);
        % Viscosities
        mus_W = zeros(size(Vx));  mus_W(2:end  , :     ) = etac;
        mus_E = zeros(size(Vx));  mus_E(1:end-1, :     ) = etac;
        mus_S = zeros(size(Vx));  mus_S(2:end-1,2:end  ) = etai;
        mus_N = zeros(size(Vx));  mus_N(2:end-1,1:end-1) = etai;
        % West
        n  = zeros(size(Vx));   n(2:end,:) = ndis(phc);
        ei = zeros(size(Vx));  ei(2:end,:) = mu_i(phc);
        ks = zeros(size(Vx));  ks(2:end,:) =  ksi(phc);
        if INV==0,  EiiW = sqrt( (2-comp)/2*ExxW.^2 + comp/2*EyyW.^2 + (ExyW).^2);
        else        EiiW = sqrt( (2-comp)/2*ExxW.^2 + comp/2*EyyW.^2 + 0.25*(ExyS.^2+ExyN.^2+ExySW.^2+ExyNW.^2)); end
        A                = evalA1(KRO,mus_W,EiiW,n,ei,ks);
        if INV==0
            dmuWdVxC   = A.*((2-comp)*ExxW*( 1/dx-comp/(pls*dx))-comp^2*EyyW/(pls*dx));
            dmuWdVxW   = A.*((2-comp)*ExxW*(-1/dx+comp/(pls*dx))+comp^2*EyyW/(pls*dx));
            dmuWdVxS   =-(1/4)*A.*ExyW/dy;
            dmuWdVxN   = (1/4)*A.*ExyW/dy;
            dmuWdVxSW  =-(1/4)*A.*ExyW/dy;
            dmuWdVxNW  = (1/4)*A.*ExyW/dy;
            dmuWdVySW  = A.*( (2-comp)*ExxW*comp/(pls*dy)+comp*EyyW*(-1/dy+comp/(pls*dy)));
            dmuWdVySE  = (1/4)*A.*ExyW/dx;
            dmuWdVyNW  = A.*(-(2-comp)*ExxW*comp/(pls*dy)+comp*EyyW*( 1/dy-comp/(pls*dy)));
            dmuWdVyNE  = (1/4)*A.*ExyW/dx;
            dmuWdVySWW =-(1/4)*A.*ExyW/dx;
            dmuWdVyNWW =-(1/4)*A.*ExyW/dx;
        else
            dmuWdVxC   = A.*((2-comp)*ExxW*( 1/dx-comp/(pls*dx))-comp^2*EyyW/(pls*dx)+(1/4)*ExyS /dy-(1/4)*ExyN /dy);
            dmuWdVxW   = A.*((2-comp)*ExxW*(-1/dx+comp/(pls*dx))+comp^2*EyyW/(pls*dx)+(1/4)*ExySW/dy-(1/4)*ExyNW/dy);
            dmuWdVxS   =-(1/4)*A.*ExyS/dy;
            dmuWdVxN   = (1/4)*A.*ExyN/dy;
            dmuWdVxSW  =-(1/4)*A.*ExySW/dy;
            dmuWdVxNW  = (1/4)*A.*ExyNW/dy;
            dmuWdVySW  = A.*( (2-comp)*ExxW*comp/(pls*dy)+comp*EyyW*(-1/dy+comp/(pls*dy))-(1/4)*ExyS/dx+(1/4)*ExySW/dx);
            dmuWdVySE  = (1/4)*A.*ExyS/dx;
            dmuWdVyNW  = A.*(-(2-comp)*ExxW*comp/(pls*dy)+comp*EyyW*( 1/dy-comp/(pls*dy))-(1/4)*ExyN/dx+(1/4)*ExyNW/dx);
            dmuWdVyNE  = (1/4)*A.*ExyN/dx;
            dmuWdVySWW =-(1/4)*A.*ExySW/dx;
            dmuWdVyNWW =-(1/4)*A.*ExyNW/dx;
        end
        % East
        n  = zeros(size(Vx));   n(1:end-1,:) = ndis(phc);
        ei = zeros(size(Vx));  ei(1:end-1,:) = mu_i(phc);
        ks = zeros(size(Vx));  ks(1:end-1,:) =  ksi(phc);
        if INV==0, EiiE = sqrt((2-comp)/2*ExxE.^2 + (comp)/2*EyyE.^2 + (ExyE).^2);
        else       EiiE = sqrt((2-comp)/2*ExxE.^2 + (comp)/2*EyyE.^2 + 0.25*(ExyS.^2+ExyN.^2+ExySE.^2+ExyNE.^2)); end
        A               = evalA1(KRO,mus_E,EiiE,n,ei,ks);
        if INV==0
            dmuEdVxC   = A.*((2-comp)*ExxE*(-1/dx+comp/(pls*dx))+comp^2*EyyE/(pls*dx));
            dmuEdVxE   = A.*((2-comp)*ExxE*( 1/dx-comp/(pls*dx))-comp^2*EyyE/(pls*dx));
            dmuEdVxS   =-(1/4)*A.*ExyE/dy;
            dmuEdVxN   = (1/4)*A.*ExyE/dy;
            dmuEdVxSE  =-(1/4)*A.*ExyE/dy;
            dmuEdVxNE  = (1/4)*A.*ExyE/dy;
            dmuEdVySW  =-(1/4)*A.*ExyE/dx;
            dmuEdVySE  = A.*( (2-comp)*ExxE*comp/(pls*dy)+comp*EyyE*(-1/dy+comp/(pls*dy)));
            dmuEdVyNW  =-(1/4)*A.*ExyE/dx;
            dmuEdVyNE  = A.*(-(2-comp)*ExxE*comp/(pls*dy)+comp*EyyE*( 1/dy-comp/(pls*dy)));
            dmuEdVySEE = (1/4)*A.*ExyE/dx;
            dmuEdVyNEE = (1/4)*A.*ExyE/dx;
        else
            dmuEdVxC   = A.*((2-comp)*ExxE*(-1/dx+comp/(pls*dx))+comp^2*EyyE/(pls*dx)+(1/4)*ExyS /dy-(1/4)*ExyN /dy);
            dmuEdVxE   = A.*((2-comp)*ExxE*( 1/dx-comp/(pls*dx))-comp^2*EyyE/(pls*dx)+(1/4)*ExySE/dy-(1/4)*ExyNE/dy);
            dmuEdVxS   =-(1/4)*A.*ExyS/dy;
            dmuEdVxN   = (1/4)*A.*ExyN/dy;
            dmuEdVxSE  =-(1/4)*A.*ExySE/dy;
            dmuEdVxNE  = (1/4)*A.*ExyNE/dy;
            dmuEdVySW  =-(1/4)*A.*ExyS/dx;
            dmuEdVySE  = A.*( (2-comp)*ExxE*comp/(pls*dy)+comp*EyyE*(-1/dy+comp/(pls*dy))+(1/4)*ExyS/dx-(1/4)*ExySE/dx);
            dmuEdVyNW  = -(1/4)*A.*ExyN/dx;
            dmuEdVyNE  = A.*(-(2-comp)*ExxE*comp/(pls*dy)+comp*EyyE*( 1/dy-comp/(pls*dy))+(1/4)*ExyN/dx-(1/4)*ExyNE/dx);
            dmuEdVySEE = (1/4)*A.*ExySE/dx;
            dmuEdVyNEE = (1/4)*A.*ExyNE/dx;
        end
        % South
        n1  = ndis(phv);  n  =  n1(:,1:end-1);
        ei1 = mu_i(phv);  ei = ei1(:,1:end-1);   
        ks1 =  ksi(phv);  ks = ks1(:,1:end-1); 
        if INV==0, EiiS = sqrt(ExyS.^2 + (2-comp)/2*(ExxS).^2 + (comp)/2*(EyyS).^2);
        else       EiiS = sqrt(ExyS.^2 + (2-comp)/2*0.25*(ExxW.^2+ExxE.^2+ExxSW.^2+ExxSE.^2) + (comp)/2*0.25*(EyyW.^2+EyyE.^2+EyySW.^2+EyySE.^2));end
        A               = evalA1(KRO,mus_S,EiiS,n,ei,ks);
        if INV==0
            dmuSdVxC   = A.*ExyS/dy;
            dmuSdVxS   =-A.*ExyS/dy;
            dmuSdVxW   = A.*((2-comp)*ExxS*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*comp^2*EyyS/(pls*dx));
            dmuSdVxE   = A.*((2-comp)*ExxS*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*comp^2*EyyS/(pls*dx));
            dmuSdVxSW  = A.*((2-comp)*ExxS*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*comp^2*EyyS/(pls*dx));
            dmuSdVxSE  = A.*((2-comp)*ExxS*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*comp^2*EyyS/(pls*dx));
            dmuSdVySE  = A.*ExyS/dx;
            dmuSdVySW  =-A.*ExyS/dx;
            dmuSdVyNW  = A.*(-(1/4)*(2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuSdVyNE  = A.*(-(1/4)*(2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuSdVySSW = A.*( (1/4)*(2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuSdVySSE = A.*( (1/4)*(2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
        else
            dmuSdVxC   = A.*( ExyS/dy+(1/8*(2-comp))*(2*ExxW *(1/dx-comp/(pls*dx))+2*ExxE *(-1/dx+comp/(pls*dx)))+(1/8)*comp*(-2*EyyW *comp/(pls*dx)+2*EyyE *comp/(pls*dx)));
            dmuSdVxS   = A.*(-ExyS/dy+(1/8*(2-comp))*(2*ExxSW*(1/dx-comp/(pls*dx))+2*ExxSE*(-1/dx+comp/(pls*dx)))+(1/8)*comp*(-2*EyySW*comp/(pls*dx)+2*EyySE*comp/(pls*dx)));
            dmuSdVxW   = A.*((1/4*(2-comp))*ExxW *(-1/dx+comp/(pls*dx))+(1/4)*comp^2*EyyW /(pls*dx));
            dmuSdVxE   = A.*((1/4*(2-comp))*ExxE *( 1/dx-comp/(pls*dx))-(1/4)*comp^2*EyyE /(pls*dx));
            dmuSdVxSW  = A.*((1/4*(2-comp))*ExxSW*(-1/dx+comp/(pls*dx))+(1/4)*comp^2*EyySW/(pls*dx));
            dmuSdVxSE  = A.*((1/4*(2-comp))*ExxSE*( 1/dx-comp/(pls*dx))-(1/4)*comp^2*EyySE/(pls*dx));
            dmuSdVySE  = A.*( ExyS/dx+(1/8*(2-comp))*(2*ExxE*comp/(pls*dy)-2*ExxSE*comp/(pls*dy))+(1/8)*comp*(2*EyyE*(-1/dy+comp/(pls*dy))+2*EyySE*(1/dy-comp/(pls*dy))));
            dmuSdVySW  = A.*(-ExyS/dx+(1/8*(2-comp))*(2*ExxW*comp/(pls*dy)-2*ExxSW*comp/(pls*dy))+(1/8)*comp*(2*EyyW*(-1/dy+comp/(pls*dy))+2*EyySW*(1/dy-comp/(pls*dy))));
            dmuSdVyNW  = A.*(-(1/4)*(2-comp)*ExxW *comp/(pls*dy)+(1/4)*EyyW*comp *( 1/dy-comp/(pls*dy)));
            dmuSdVyNE  = A.*(-(1/4)*(2-comp)*ExxE *comp/(pls*dy)+(1/4)*EyyE*comp *( 1/dy-comp/(pls*dy)));
            dmuSdVySSW = A.*( (1/4)*(2-comp)*ExxSW*comp/(pls*dy)+(1/4)*EyySW*comp*(-1/dy+comp/(pls*dy)));
            dmuSdVySSE = A.*( (1/4)*(2-comp)*ExxSE*comp/(pls*dy)+(1/4)*EyySE*comp*(-1/dy+comp/(pls*dy)));
        end
        % North
        n1  = ndis(phv);  n  =  n1(:,2:end);  
        ei1 = mu_i(phv);  ei = ei1(:,2:end);   
        ks1 =  ksi(phv);  ks = ks1(:,2:end); 
        if INV==0, EiiN = sqrt(ExyN.^2 + (2-comp)/2*(ExxN).^2 + (comp)/2*(EyyN).^2);
        else       EiiN = sqrt(ExyN.^2 + (2-comp)/2*0.25*(ExxW.^2+ExxE.^2+ExxNW.^2+ExxNE.^2) + (comp)/2*0.25*(EyyW.^2+EyyE.^2+EyyNW.^2+EyyNE.^2)); end
        A               = evalA1(KRO,mus_N,EiiN,n,ei,ks);
        if INV==0
            dmuNdVxC   =-A.*ExyN/dy;
            dmuNdVxN   = A.*ExyN/dy;
            dmuNdVxW   = A.*((2-comp)*ExxN*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*comp^2*EyyN/(pls*dx));
            dmuNdVxE   = A.*((2-comp)*ExxN*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*comp^2*EyyN/(pls*dx));
            dmuNdVxNW  = A.*((2-comp)*ExxN*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*comp^2*EyyN/(pls*dx));
            dmuNdVxNE  = A.*((2-comp)*ExxN*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*comp^2*EyyN/(pls*dx));
            dmuNdVyNE  = A.*ExyN/dx;
            dmuNdVyNW  =-A.*ExyN/dx;
            dmuNdVySW  = A.*( (1/4)*(2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuNdVySE  = A.*( (1/4)*(2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuNdVyNNW = A.*(-(1/4)*(2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuNdVyNNE = A.*(-(1/4)*(2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
        else
            dmuNdVxC   = A.*(-ExyN/dy+(1/8*(2-comp))*(2*ExxW *(1/dx-comp/(pls*dx))+2*ExxE *(-1/dx+comp/(pls*dx)))+(1/8)*comp*(-2*EyyW *comp/(pls*dx)+2*EyyE *comp/(pls*dx)));
            dmuNdVxN   = A.*( ExyN/dy+(1/8*(2-comp))*(2*ExxNW*(1/dx-comp/(pls*dx))+2*ExxNE*(-1/dx+comp/(pls*dx)))+(1/8)*comp*(-2*EyyNW*comp/(pls*dx)+2*EyyNE*comp/(pls*dx)));
            dmuNdVxW   = A.*((1/4*(2-comp))*ExxW *(-1/dx+comp/(pls*dx))+(1/4)*comp^2*EyyW /(pls*dx));
            dmuNdVxE   = A.*((1/4*(2-comp))*ExxE *( 1/dx-comp/(pls*dx))-(1/4)*comp^2*EyyE /(pls*dx));
            dmuNdVxNW  = A.*((1/4*(2-comp))*ExxNW*(-1/dx+comp/(pls*dx))+(1/4)*comp^2*EyyNW/(pls*dx));
            dmuNdVxNE  = A.*((1/4*(2-comp))*ExxNE*( 1/dx-comp/(pls*dx))-(1/4)*comp^2*EyyNE/(pls*dx));
            dmuNdVyNE  = A.*( ExyN/dx+(1/8*(2-comp))*(-2*ExxE*comp/(pls*dy)+2*ExxNE*comp/(pls*dy))+(1/8)*comp*(2*EyyE*(1/dy-comp/(pls*dy))+2*EyyNE*(-1/dy+comp/(pls*dy))));
            dmuNdVyNW  = A.*(-ExyN/dx+(1/8*(2-comp))*(-2*ExxW*comp/(pls*dy)+2*ExxNW*comp/(pls*dy))+(1/8)*comp*(2*EyyW*(1/dy-comp/(pls*dy))+2*EyyNW*(-1/dy+comp/(pls*dy))));
            dmuNdVySW  = A.*( (1/4)*(2-comp)*ExxW *comp/(pls*dy)+(1/4)*comp*EyyW*(-1/dy+comp/(pls*dy)));
            dmuNdVySE  = A.*( (1/4)*(2-comp)*ExxE *comp/(pls*dy)+(1/4)*comp*EyyE*(-1/dy+comp/(pls*dy)));
            dmuNdVyNNW = A.*(-(1/4)*(2-comp)*ExxNW*comp/(pls*dy)+(1/4)*comp*EyyNW*(1/dy-comp/(pls*dy)));
            dmuNdVyNNE = A.*(-(1/4)*(2-comp)*ExxNE*comp/(pls*dy)+(1/4)*comp*EyyNE*(1/dy-comp/(pls*dy)));
        end
        %% Stokes Newton Operator: Block [UU UV] - with contributions from BC's
        mus_W  = zeros(size(Vx));  mus_W(2:end  , :     ) = etac;
        mus_E  = zeros(size(Vx));  mus_E(1:end-1, :     ) = etac;
        mus_S  = zeros(size(Vx));  mus_S(2:end-1,2:end  ) = etai;
        mus_N  = zeros(size(Vx));  mus_N(2:end-1,1:end-1) = etai;
        % Coefficients
        c1VxC = -(2*dmuEdVxC.*ExxE+2*mus_E*(-1/dx+comp/(pls*dx))-2*dmuWdVxC.*ExxW-2*mus_W*(1/dx-comp/(pls*dx)))/dx-(2*dmuNdVxC.*ExyN-mus_N/dy-2*dmuSdVxC.*ExyS-mus_S/dy)/dy;
        c1VxC([1,end],:) = scVx;
        c1VxW   = -(-2*dmuWdVxW.*ExxW - 2*mus_W*(-1/dx+comp/pls/dx))/dx - (2*dmuNdVxW.*ExyN-2*dmuSdVxW.*ExyS)/dy;                                    c1VxW  ([1 end],:) = 0;  c1VxW( 2   , : ) = 0;
        c1VxE   = -( 2*dmuEdVxE.*ExxE + 2*mus_E*( 1/dx-comp/pls/dx))/dx - (2*dmuNdVxE.*ExyN-2*dmuSdVxE.*ExyS)/dy;                                    c1VxE  ([1 end],:) = 0;  c1VxE(end-1, : ) = 0;
        c1VxS   = -( 2*dmuEdVxS.*ExxE - 2*dmuWdVxS.*ExxW)/dx -(-2*dmuSdVxS.*ExyS+mus_S/dy)/dy;                                                       c1VxS  ([1 end],:) = 0;  c1VxS( :   , 1 ) = 0;
        c1VxN   = -( 2*dmuEdVxN.*ExxE - 2*dmuWdVxN.*ExxW)/dx -( 2*dmuNdVxN.*ExyN+mus_N/dy)/dy;                                                       c1VxN  ([1 end],:) = 0;  c1VxN( :   ,end) = 0;
        c1VxSW  =   2*dmuWdVxSW.*ExxW/dx + 2*dmuSdVxSW.*ExyS/dy;                                                                                     c1VxSW ([1 end],:) = 0;  c1VxSW( :  , 1 ) = 0;  c1VxSW (2    ,:) = 0;
        c1VxSE  =  -2*dmuEdVxSE.*ExxE/dx + 2*dmuSdVxSE.*ExyS/dy;                                                                                     c1VxSE ([1 end],:) = 0;  c1VxSE( :  , 1 ) = 0;  c1VxSE (end-1,:) = 0;
        c1VxNW  =   2*dmuWdVxNW.*ExxW/dx - 2*dmuNdVxNW.*ExyN/dy;                                                                                     c1VxNW ([1 end],:) = 0;  c1VxNW( :  ,end) = 0;  c1VxNW (2    ,:) = 0;
        c1VxNE  =  -2*dmuEdVxNE.*ExxE/dx - 2*dmuNdVxNE.*ExyN/dy;                                                                                     c1VxNE ([1 end],:) = 0;  c1VxNE( :  ,end) = 0;  c1VxNE (end-1,:) = 0;
        c1VySW  = -(2*dmuEdVySW.*ExxE - 2*dmuWdVySW.*ExxW - 1*2*mus_W*comp/(pls*dy))/dx - ( 1*2*dmuNdVySW.*ExyN - 2*dmuSdVySW.*ExyS + mus_S/dx)/dy;  c1VySW ([1 end],:) = 0;  c1VySW( :  ,1  ) = 0;
        c1VySE  = -(2*dmuEdVySE.*ExxE - 2*dmuWdVySE.*ExxW + 1*2*mus_E*comp/(pls*dy))/dx - ( 1*2*dmuNdVySE.*ExyN - 2*dmuSdVySE.*ExyS - mus_S/dx)/dy;  c1VySE ([1 end],:) = 0;  c1VySE( :  ,1  ) = 0;
        c1VyNW  = -(2*dmuEdVyNW.*ExxE - 2*dmuWdVyNW.*ExxW + 1*2*mus_W*comp/(pls*dy))/dx - (-1*2*dmuSdVyNW.*ExyS + 2*dmuNdVyNW.*ExyN - mus_N/dx)/dy;  c1VyNW ([1 end],:) = 0;  c1VyNW( :  ,end) = 0;
        c1VyNE  = -(2*dmuEdVyNE.*ExxE - 2*dmuWdVyNE.*ExxW - 1*2*mus_E*comp/(pls*dy))/dx - (-1*2*dmuSdVyNE.*ExyS + 2*dmuNdVyNE.*ExyN + mus_N/dx)/dy;  c1VyNE ([1 end],:) = 0;  c1VyNE( :  ,end) = 0;
        c1VySWW =  2*dmuWdVySWW.*ExxW/dx;                                                                                                            c1VySWW([1 2     end],:) = 0;
        c1VySEE = -2*dmuEdVySEE.*ExxE/dx;                                                                                                            c1VySEE([1 end-1 end],:) = 0;
        c1VyNWW =  2*dmuWdVyNWW.*ExxW/dx;                                                                                                            c1VyNWW([1 2     end],:) = 0;
        c1VyNEE = -2*dmuEdVyNEE.*ExxE/dx;                                                                                                            c1VyNEE([1 end-1 end],:) = 0;
        c1VySSW =  2*dmuSdVySSW.*ExyS/dy;                                                                                                            c1VySSW(:,[1     2  ])   = 0;
        c1VySSE =  2*dmuSdVySSE.*ExyS/dy;                                                                                                            c1VySSE(:,[1     2  ])   = 0;
        c1VyNNW = -2*dmuNdVyNNW.*ExyN/dy;                                                                                                            c1VyNNW(:,[end-1 end])   = 0;
        c1VyNNE = -2*dmuNdVyNNE.*ExyN/dy;                                                                                                            c1VyNNE(:,[end-1 end])   = 0;
        % Equation indexes
        iVx     = NumVx(:);
        iVxW    = ones(size(Vx));   iVxW(2:end  , :     ) =  NumVx(1:end-1, :     );
        iVxE    = ones(size(Vx));   iVxE(1:end-1, :     ) =  NumVx(2:end  , :     );
        iVxS    = ones(size(Vx));   iVxS( :     ,2:end  ) =  NumVx( :     ,1:end-1);
        iVxN    = ones(size(Vx));   iVxN( :     ,1:end-1) =  NumVx( :     ,2:end  );
        iVxSW   = ones(size(Vx));  iVxSW(2:end  ,2:end  ) =  NumVx(1:end-1,1:end-1);
        iVxSE   = ones(size(Vx));  iVxSE(1:end-1,2:end  ) =  NumVx(2:end  ,1:end-1);
        iVxNW   = ones(size(Vx));  iVxNW(2:end  ,1:end-1) =  NumVx(1:end-1,2:end  );
        iVxNE   = ones(size(Vx));  iVxNE(1:end-1,1:end-1) =  NumVx(2:end  ,2:end  );
        iVySW   = ones(size(Vx));  iVySW(2:end  , :     ) = NumVyG( :     ,1:end-1);
        iVySE   = ones(size(Vx));  iVySE(1:end-1, :     ) = NumVyG( :     ,1:end-1);
        iVyNW   = ones(size(Vx));  iVyNW(2:end  , :     ) = NumVyG( :     ,2:end  );
        iVyNE   = ones(size(Vx));  iVyNE(1:end-1, :     ) = NumVyG( :     ,2:end  );
        iVySWW  = ones(size(Vx)); iVySWW(3:end  , :     ) = NumVyG(1:end-1,1:end-1);
        iVySEE  = ones(size(Vx)); iVySEE(1:end-2, :     ) = NumVyG(2:end  ,1:end-1);
        iVyNWW  = ones(size(Vx)); iVyNWW(3:end  , :     ) = NumVyG(1:end-1,2:end  );
        iVyNEE  = ones(size(Vx)); iVyNEE(1:end-2, :     ) = NumVyG(2:end  ,2:end  );
        iVySSW  = ones(size(Vx)); iVySSW(2:end  , 2:end ) = NumVyG( :     ,1:end-2);
        iVySSE  = ones(size(Vx)); iVySSE(1:end-1, 2:end ) = NumVyG( :     ,1:end-2);
        iVyNNW  = ones(size(Vx)); iVyNNW(2:end  ,1:end-1) = NumVyG( :     ,3:end  );
        iVyNNE  = ones(size(Vx)); iVyNNE(1:end-1,1:end-1) = NumVyG( :     ,3:end  );
        % Triplets
        IuuJ    = [   iVx(:);   iVx(:);   iVx(:);   iVx(:);   iVx(:);    iVx(:);    iVx(:);    iVx(:);    iVx(:);    iVx(:);    iVx(:);    iVx(:);    iVx(:);     iVx(:);     iVx(:);     iVx(:);     iVx(:);     iVx(:);     iVx(:);     iVx(:);     iVx(:) ]';
        JuuJ    = [   iVx(:);  iVxW(:);  iVxS(:);  iVxE(:);  iVxN(:);  iVxSW(:);  iVxSE(:);  iVxNW(:);  iVxNE(:);  iVySW(:);  iVySE(:);  iVyNW(:);  iVyNE(:);  iVySWW(:);  iVySEE(:);  iVyNWW(:);  iVyNEE(:);  iVySSW(:);  iVySSE(:);  iVyNNW(:);  iVyNNE(:) ]';
        VuuJ    = [ c1VxC(:); c1VxW(:); c1VxS(:); c1VxE(:); c1VxN(:); c1VxSW(:); c1VxSE(:); c1VxNW(:); c1VxNE(:); c1VySW(:); c1VySE(:); c1VyNW(:); c1VyNE(:); c1VySWW(:); c1VySEE(:); c1VyNWW(:); c1VyNEE(:); c1VySSW(:); c1VySSE(:); c1VyNNW(:); c1VyNNE(:) ]';
        %% Stokes Newton Operator: Derivatives Vy stencil
        % Construct strain-rate stencil
        ExxS  = zeros(size(Vy)); ExxN  = zeros(size(Vy)); ExxSW = zeros(size(Vy)); ExxSE = zeros(size(Vy)); ExxNW = zeros(size(Vy)); ExxNE = zeros(size(Vy));
        EyyS  = zeros(size(Vy)); EyyN  = zeros(size(Vy)); EyySW = zeros(size(Vy)); EyySE = zeros(size(Vy)); EyyNW = zeros(size(Vy)); EyyNE = zeros(size(Vy));
        ExySW = zeros(size(Vy)); ExySE = zeros(size(Vy)); ExyNW = zeros(size(Vy)); ExyNE = zeros(size(Vy));
        ExxS(:,2:end)        = exx;                  ExxN(:,1:end-1)        = exx;
        ExxSW(2:end,2:end)   = exx(1:end-1,:);       ExxNW(2:end,1:end-1)   = exx(1:end-1,:);
        ExxSE(1:end-1,2:end) = exx(2:end,:);         ExxNE(1:end-1,1:end-1) = exx(2:end,:);
        EyyS(:,2:end)        = eyy;                  EyyN(:,1:end-1)        = eyy;
        EyySW(2:end,2:end)   = eyy(1:end-1,:);       EyyNW(2:end,1:end-1)   = eyy(1:end-1,:);
        EyySE(1:end-1,2:end) = eyy(2:end,:);         EyyNE(1:end-1,1:end-1) = eyy(2:end,:);
        ExyW                 = exy(1:end-1,:);       ExyE                   = exy(2:end,:);
        ExySW(:,2:end)       = exy(1:end-1,1:end-1); ExySE(:,2:end)         = exy(2:end,1:end-1);
        ExyNW(:,1:end-1)     = exy(1:end-1,2:end);   ExyNE(:,1:end-1)       = exy(2:end,2:end);
        ExyS = 0.25*(ExyW+ExyE+ExySW+ExySE); ExyN = 0.25*(ExyW+ExyE+ExyNW+ExyNE); EyyW = 0.25*(EyyS+EyyN+EyySW+EyyNW); EyyE = 0.25*(EyyS+EyyN+EyySE+EyyNE);  ExxW = 0.25*(ExxS+ExxN+ExxSW+ExxNW); ExxE = 0.25*(ExxS+ExxN+ExxSE+ExxNE);
        % Viscosities
        mus_W  = zeros(size(Vy));  mus_W(2:end  ,2:end-1) = etai;
        mus_E  = zeros(size(Vy));  mus_E(1:end-1,2:end-1) = etai;
        mus_S  = zeros(size(Vy));  mus_S( :     ,2:end  ) = etac;
        mus_N  = zeros(size(Vy));  mus_N( :     ,1:end-1) = etac;
        % South
        n  = zeros(size(Vy));  n (:,2:end) = ndis(phc);
        ei = zeros(size(Vy));  ei(:,2:end) = mu_i(phc);
        ks = zeros(size(Vy));  ks(:,2:end) =  ksi(phc);
        if INV==0, EiiS = sqrt((ExyS).^2 + (2-comp)/2*EyyS.^2 + (comp)/2*ExxS.^2);
        else       EiiS = sqrt(0.25*(ExyW.^2+ExyE.^2+ExySW.^2+ExySE.^2) + (2-comp)/2*EyyS.^2 + (comp)/2*ExxS.^2); end
        A               = evalA1(KRO,mus_S,EiiS,n,ei,ks);
        if INV==0
            dmuSdVyC   = A.*(-(2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*( 1/dy-comp/(pls*dy)));
            dmuSdVyS   = A.*( (2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*(-1/dy+comp/(pls*dy)));
            dmuSdVyW   =-(1/4)*A.*ExyS/dx;
            dmuSdVyE   = (1/4)*A.*ExyS/dx;
            dmuSdVySW  =-(1/4)*A.*ExyS/dx;
            dmuSdVySE  = (1/4)*A.*ExyS/dx;
            dmuSdVxSSW =-(1/4)*A.*ExyS/dy;
            dmuSdVxSSE =-(1/4)*A.*ExyS/dy;
            dmuSdVxSW  = A.*((2-comp)*ExxS*(-1/dx+comp/(pls*dx))+comp^2*EyyS/(pls*dx));
            dmuSdVxSE  = A.*((2-comp)*ExxS*( 1/dx-comp/(pls*dx))-comp^2*EyyS/(pls*dx));
            dmuSdVxNW  = (1/4)*A.*ExyS/dy;
            dmuSdVxNE  = (1/4)*A.*ExyS/dy;
        else
            dmuSdVyC   = A.*(-(2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*( 1/dy-comp/(pls*dy))+(1/4)*ExyW /dx-(1/4)*ExyE /dx);
            dmuSdVyS   = A.*( (2-comp)*ExxS*comp/(pls*dy)+comp*EyyS*(-1/dy+comp/(pls*dy))+(1/4)*ExySW/dx-(1/4)*ExySE/dx);
            dmuSdVyW   =-(1/4)*A.*ExyW/dx;
            dmuSdVyE   = (1/4)*A.*ExyE/dx;
            dmuSdVySW  =-(1/4)*A.*ExySW/dx;
            dmuSdVySE  = (1/4)*A.*ExySE/dx;
            dmuSdVxSSW =-(1/4)*A.*ExySW/dy;
            dmuSdVxSSE =-(1/4)*A.*ExySE/dy;
            dmuSdVxSW  = A.*((2-comp)*ExxS*(-1/dx+comp/(pls*dx))+comp^2*EyyS/(pls*dx)-(1/4)*ExyW/dy+(1/4)*ExySW/dy);
            dmuSdVxSE  = A.*((2-comp)*ExxS*( 1/dx-comp/(pls*dx))-comp^2*EyyS/(pls*dx)-(1/4)*ExyE/dy+(1/4)*ExySE/dy);
            dmuSdVxNW  = (1/4)*A.*ExyW/dy;
            dmuSdVxNE  = (1/4)*A.*ExyE/dy;
        end
        % North
        n  = zeros(size(Vy));   n(:,1:end-1) = ndis(phc);
        ei = zeros(size(Vy));  ei(:,1:end-1) = mu_i(phc);
        ks = zeros(size(Vy));  ks(:,1:end-1) = ksi(phc);
        if INV==0, EiiN = sqrt((ExyN).^2 + (2-comp)/2*EyyN.^2 + (comp)/2*ExxN.^2);
        else       EiiN = sqrt(0.25*(ExyW.^2+ExyE.^2+ExyNW.^2+ExyNE.^2) + (2-comp)/2*EyyN.^2 + (comp)/2*ExxN.^2); end
        A               = evalA1(KRO,mus_N,EiiN,n,ei,ks);
        if INV==0
            dmuNdVyC   = A.*( (2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(-1/dy+comp/(pls*dy)));
            dmuNdVyN   = A.*(-(2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*( 1/dy-comp/(pls*dy)));
            dmuNdVyW   =-(1/4)*A.*ExyN/dx;
            dmuNdVyE   = (1/4)*A.*ExyN/dx;
            dmuNdVyNW  =-(1/4)*A.*ExyN/dx;
            dmuNdVyNE  = (1/4)*A.*ExyN/dx;
            dmuNdVxNNW = (1/4)*A.*ExyN/dy;
            dmuNdVxNNE = (1/4)*A.*ExyN/dy;
            dmuNdVxSW  =-(1/4)*A.*ExyN/dy;
            dmuNdVxSE  =-(1/4)*A.*ExyN/dy;
            dmuNdVxNW  = A.*((2-comp)*ExxN*(-1/dx+comp/(pls*dx))+comp^2*EyyN/(pls*dx));
            dmuNdVxNE  = A.*((2-comp)*ExxN*( 1/dx-comp/(pls*dx))-comp^2*EyyN/(pls*dx));
        else
            dmuNdVyC   = A.*( (2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(-1/dy+comp/(pls*dy))+(1/4)*ExyW/dx-(1/4)*ExyE/dx);
            dmuNdVyN   = A.*(-(2-comp)*ExxN*comp/(pls*dy)+comp*EyyN*(1/dy-comp/(pls*dy))+(1/4)*ExyNW/dx-(1/4)*ExyNE/dx);
            dmuNdVyW   =-(1/4)*A.*ExyW/dx;
            dmuNdVyE   = (1/4)*A.*ExyE/dx;
            dmuNdVyNW  =-(1/4)*A.*ExyNW/dx;
            dmuNdVyNE  = (1/4)*A.*ExyNE/dx;
            dmuNdVxNNW = (1/4)*A.*ExyNW/dy;
            dmuNdVxNNE = (1/4)*A.*ExyNE/dy;
            dmuNdVxSW  =-(1/4)*A.*ExyW/dy;
            dmuNdVxSE  =-(1/4)*A.*ExyE/dy;
            dmuNdVxNW  = A.*((2-comp)*ExxN*(-1/dx+comp/(pls*dx))+comp^2*EyyN/(pls*dx)+(1/4)*ExyW/dy-(1/4)*ExyNW/dy);
            dmuNdVxNE  = A.*((2-comp)*ExxN*( 1/dx-comp/(pls*dx))-comp^2*EyyN/(pls*dx)+(1/4)*ExyE/dy-(1/4)*ExyNE/dy);
        end
        % West
        n1  = ndis(phv);   n = n1 (1:end-1,:);    
        ei1 = mu_i(phv);  ei = ei1(1:end-1,:);     
        ks1 = ksi (phv);  ks = ks1(1:end-1,:);
        if INV==0, EiiW = sqrt((2-comp)/2*(EyyW).^2 + (comp)/2*(ExxW).^2 + ExyW.^2);  
        else       EiiW = sqrt((2-comp)/2*0.25*(EyyS.^2+EyyN.^2+EyySW.^2+EyyNW.^2) + (comp)/2*0.25*(ExxS.^2+ExxN.^2+ExxSW.^2+ExxNW.^2) + ExyW.^2); end
        A               = evalA1(KRO,mus_W,EiiW,n,ei,ks);
        if INV==0
            dmuWdVyC   = A.*ExyW/dx;
            dmuWdVyW   =-A.*ExyW/dx;
            dmuWdVyS   = A.*( (1/4)*comp^2*ExxW/(pls*dy)+(2-comp)*EyyW*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuWdVyN   = A.*(-(1/4)*comp^2*ExxW/(pls*dy)+(2-comp)*EyyW*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuWdVySW  = A.*( (1/4)*comp^2*ExxW/(pls*dy)+(2-comp)*EyyW*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuWdVyNW  = A.*(-(1/4)*comp^2*ExxW/(pls*dy)+(2-comp)*EyyW*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuWdVxSW  =-A.*ExyW/dy;
            dmuWdVxNW  = A.*ExyW/dy;
            dmuWdVxSE  = A.*(comp*ExxW*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*(2-comp)*EyyW*comp/(pls*dx));
            dmuWdVxNE  = A.*(comp*ExxW*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*(2-comp)*EyyW*comp/(pls*dx));
            dmuWdVxSWW = A.*(comp*ExxW*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*(2-comp)*EyyW*comp/(pls*dx));
            dmuWdVxNWW = A.*(comp*ExxW*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*(2-comp)*EyyW*comp/(pls*dx));
        else
            dmuWdVyC   = A.*( ExyW/dx + (1/8)*comp*(-2*ExxS *comp/(pls*dy) + 2*ExxN *comp/(pls*dy)) + (1/8*(2-comp))*(2*EyyS *(1/dy-comp/(pls*dy))+2*EyyN *(-1/dy+comp/(pls*dy))));
            dmuWdVyW   = A.*(-ExyW/dx + (1/8)*comp*(-2*ExxSW*comp/(pls*dy) + 2*ExxNW*comp/(pls*dy)) + (1/8*(2-comp))*(2*EyySW*(1/dy-comp/(pls*dy))+2*EyyNW*(-1/dy+comp/(pls*dy))));
            dmuWdVyS   = A.*( (1/4)*comp^2*ExxS /(pls*dy)+(1/4*(2-comp))*EyyS *(-1/dy+comp/(pls*dy)));
            dmuWdVyN   = A.*(-(1/4)*comp^2*ExxN /(pls*dy)+(1/4*(2-comp))*EyyN *( 1/dy-comp/(pls*dy)));
            dmuWdVySW  = A.*( (1/4)*comp^2*ExxSW/(pls*dy)+(1/4*(2-comp))*EyySW*(-1/dy+comp/(pls*dy)));
            dmuWdVyNW  = A.*(-(1/4)*comp^2*ExxNW/(pls*dy)+(1/4*(2-comp))*EyyNW*( 1/dy-comp/(pls*dy)));
            dmuWdVxSW  = A.*(-ExyW/dy+(1/8)*comp*(2*ExxS*(-1/dx+comp/(pls*dx))+2*ExxSW*(1/dx-comp/(pls*dx)))+(1/8*(2-comp))*(2*EyyS*comp/(pls*dx)-2*EyySW*comp/(pls*dx)));
            dmuWdVxNW  = A.*( ExyW/dy+(1/8)*comp*(2*ExxN*(-1/dx+comp/(pls*dx))+2*ExxNW*(1/dx-comp/(pls*dx)))+(1/8*(2-comp))*(2*EyyN*comp/(pls*dx)-2*EyyNW*comp/(pls*dx)));
            dmuWdVxSE  = A.*((1/4)*comp*ExxS *( 1/dx-comp/(pls*dx))-(1/4)*(2-comp)*EyyS*comp /(pls*dx));
            dmuWdVxNE  = A.*((1/4)*comp*ExxN *( 1/dx-comp/(pls*dx))-(1/4)*(2-comp)*EyyN*comp /(pls*dx));
            dmuWdVxSWW = A.*((1/4)*comp*ExxSW*(-1/dx+comp/(pls*dx))+(1/4)*(2-comp)*EyySW*comp/(pls*dx));
            dmuWdVxNWW = A.*((1/4)*comp*ExxNW*(-1/dx+comp/(pls*dx))+(1/4)*(2-comp)*EyyNW*comp/(pls*dx));
        end
        % East
        n1  = ndis(phv);  n  =  n1(2:end,:);
        ei1 = mu_i(phv);  ei = ei1(2:end,:);     
        ks1 = ksi (phv);  ks = ks1(2:end,:);
        if INV==0, EiiE = sqrt((2-comp)/2*(EyyE).^2 + (comp)/2*(ExxE).^2 + ExyE.^2);
        else       EiiE = sqrt((2-comp)/2*0.25*(EyyS.^2+EyyN.^2+EyySE.^2+EyyNE.^2) + (comp)/2*0.25*(ExxS.^2+ExxN.^2+ExxSE.^2+ExxNE.^2) + ExyE.^2); end
        A               = evalA1(KRO,mus_E,EiiE,n,ei,ks);
        if INV==0
            dmuEdVyC   =-A.*ExyE/dx;
            dmuEdVyE   = A.*ExyE/dx;
            dmuEdVyS   = A.*( (1/4)*comp^2*ExxE/(pls*dy)+(2-comp)*EyyE*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuEdVyN   = A.*(-(1/4)*comp^2*ExxE/(pls*dy)+(2-comp)*EyyE*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuEdVySE  = A.*( (1/4)*comp^2*ExxE/(pls*dy)+(2-comp)*EyyE*(-1/(4*dy)+(1/4)*comp/(pls*dy)));
            dmuEdVyNE  = A.*(-(1/4)*comp^2*ExxE/(pls*dy)+(2-comp)*EyyE*(-(1/4)*comp/(pls*dy)+1/(4*dy)));
            dmuEdVxSE  =-A.*ExyE/dy;
            dmuEdVxNE  = A.*ExyE/dy;
            dmuEdVxSW  = A.*(comp*ExxE*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*(2-comp)*EyyE*comp/(pls*dx));
            dmuEdVxNW  = A.*(comp*ExxE*(-1/(4*dx)+(1/4)*comp/(pls*dx))+(1/4)*(2-comp)*EyyE*comp/(pls*dx));
            dmuEdVxSEE = A.*(comp*ExxE*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*(2-comp)*EyyE*comp/(pls*dx));
            dmuEdVxNEE = A.*(comp*ExxE*(-(1/4)*comp/(pls*dx)+1/(4*dx))-(1/4)*(2-comp)*EyyE*comp/(pls*dx));
        else
            dmuEdVyC   = A.*(-ExyE/dx+(1/8)*comp*(-2*ExxS *comp/(pls*dy)+2*ExxN *comp/(pls*dy))+(1/8*(2-comp))*(2*EyyS *(1/dy-comp/(pls*dy))+2*EyyN *(-1/dy+comp/(pls*dy))));
            dmuEdVyE   = A.*( ExyE/dx+(1/8)*comp*(-2*ExxSE*comp/(pls*dy)+2*ExxNE*comp/(pls*dy))+(1/8*(2-comp))*(2*EyySE*(1/dy-comp/(pls*dy))+2*EyyNE*(-1/dy+comp/(pls*dy))));
            dmuEdVyS   = A.*( (1/4)*comp^2*ExxS/(pls*dy)+(1/4*(2-comp))*EyyS*(-1/dy+comp/(pls*dy)));
            dmuEdVyN   = A.*(-(1/4)*comp^2*ExxN/(pls*dy)+(1/4*(2-comp))*EyyN*( 1/dy-comp/(pls*dy)));
            dmuEdVySE  = A.*( (1/4)*comp^2*ExxSE/(pls*dy)+(1/4*(2-comp))*EyySE*(-1/dy+comp/(pls*dy)));
            dmuEdVyNE  = A.*(-(1/4)*comp^2*ExxNE/(pls*dy)+(1/4*(2-comp))*EyyNE*( 1/dy-comp/(pls*dy)));
            dmuEdVxSE  = A.*(-ExyE/dy+(1/8)*comp*(2*ExxS*(1/dx-comp/(pls*dx))+2*ExxSE*(-1/dx+comp/(pls*dx)))+(1/8*(2-comp))*(-2*EyyS*comp/(pls*dx)+2*EyySE*comp/(pls*dx)));
            dmuEdVxNE  = A.*( ExyE/dy+(1/8)*comp*(2*ExxN*(1/dx-comp/(pls*dx))+2*ExxNE*(-1/dx+comp/(pls*dx)))+(1/8*(2-comp))*(-2*EyyN*comp/(pls*dx)+2*EyyNE*comp/(pls*dx)));
            dmuEdVxSW  = A.*((1/4)*comp*ExxS*(-1/dx+comp/(pls*dx))+(1/4)*(2-comp)*EyyS*comp/(pls*dx));
            dmuEdVxNW  = A.*((1/4)*comp*ExxN*(-1/dx+comp/(pls*dx))+(1/4)*(2-comp)*EyyN*comp/(pls*dx));
            dmuEdVxSEE = A.*((1/4)*comp*ExxSE*(1/dx-comp/(pls*dx))-(1/4)*(2-comp)*EyySE*comp/(pls*dx));
            dmuEdVxNEE = A.*((1/4)*comp*ExxNE*(1/dx-comp/(pls*dx))-(1/4)*(2-comp)*EyyNE*comp/(pls*dx));
        end
        %% Stokes Newton Operator: Block [VV VU] - with contributions from BC's
        mus_W = zeros(size(Vy));  mus_W(2:end  ,2:end-1) = etai;
        mus_E = zeros(size(Vy));  mus_E(1:end-1,2:end-1) = etai;
        mus_S = zeros(size(Vy));  mus_S( :     ,2:end  ) = etac;
        mus_N = zeros(size(Vy));  mus_N( :     ,1:end-1) = etac;
        % Coefficients
        c2VyC   = -(2*dmuNdVyC.*EyyN+2*mus_N*(-1/dy+comp/(pls*dy))-2*dmuSdVyC.*EyyS-2*mus_S*(1/dy-comp/(pls*dy)))/dy-(2*dmuEdVyC.*ExyE-mus_E/dx-2*dmuWdVyC.*ExyW-mus_W/dx)/dx;
        c2VyC(:,[1,end]) = scVy;
        c2VyW   = -( 2*dmuNdVyW .*EyyN - 2*dmuSdVyW.*EyyS)/dy - (-2*dmuWdVyW.*ExyW+mus_W/dx)/dx;                                      c2VyW  (:,[1 end]) = 0;  c2VyW( 1 , :   ) = 0;
        c2VyE   = -( 2*dmuNdVyE .*EyyN - 2*dmuSdVyE.*EyyS)/dy - ( 2*dmuEdVyE.*ExyE+mus_E/dx)/dx;                                      c2VyE  (:,[1 end]) = 0;  c2VyE(end, :   ) = 0;
        c2VyS   = -(-2*dmuSdVyS .*EyyS-2*mus_S*(-1/dy+comp/pls/dy))/dy - (2*dmuEdVyS.*ExyE-2*dmuWdVyS.*ExyW)/dx;                      c2VyS  (:,[1 end]) = 0;  c2VyS( : , 2   ) = 0;
        c2VyN   = -( 2*dmuNdVyN .*EyyN+2*mus_N*( 1/dy-comp/pls/dy))/dy - (2*dmuEdVyN.*ExyE-2*dmuWdVyN.*ExyW)/dx;                      c2VyN  (:,[1 end]) = 0;  c2VyN( : ,end-1) = 0;
        c2VySW  =   2*dmuSdVySW.*EyyS/dy + 2*dmuWdVySW.*ExyW/dx;                                                                      c2VySW (:,[1 end]) = 0;  c2VySW(1  , :  ) = 0;  c2VySW(:,    2) = 0;
        c2VySE  =   2*dmuSdVySE.*EyyS/dy - 2*dmuEdVySE.*ExyE/dx;                                                                      c2VySE (:,[1 end]) = 0;  c2VySE(end, :  ) = 0;  c2VySE(:,    2) = 0;
        c2VyNW  =  -2*dmuNdVyNW.*EyyN/dy + 2*dmuWdVyNW.*ExyW/dx;                                                                      c2VyNW (:,[1 end]) = 0;  c2VyNW(1  , :  ) = 0;  c2VyNW(:,end-1) = 0;
        c2VyNE  =  -2*dmuNdVyNE.*EyyN/dy - 2*dmuEdVyNE.*ExyE/dx;                                                                      c2VyNE (:,[1 end]) = 0;  c2VyNE(end, :  ) = 0;  c2VyNE(:,end-1) = 0;
        c2VxSW  = -(2*dmuNdVxSW.*EyyN-2*dmuSdVxSW.*EyyS-2*mus_S*comp/(pls*dx))/dy-(2*dmuEdVxSW.*ExyE-2*dmuWdVxSW.*ExyW+mus_W/dy)/dx;  c2VxSW (:,[1 end]) = 0;  c2VxSW (1  ,:) = 0;
        c2VxSE  = -(2*dmuNdVxSE.*EyyN-2*dmuSdVxSE.*EyyS+2*mus_S*comp/(pls*dx))/dy-(2*dmuEdVxSE.*ExyE-mus_E/dy-2*dmuWdVxSE.*ExyW)/dx;  c2VxSE (:,[1 end]) = 0;  c2VxSE (end,:) = 0;
        c2VxNW  = -(2*dmuNdVxNW.*EyyN+2*mus_N*comp/(pls*dx)-2*dmuSdVxNW.*EyyS)/dy-(2*dmuEdVxNW.*ExyE-2*dmuWdVxNW.*ExyW-mus_W/dy)/dx;  c2VxNW (:,[1 end]) = 0;  c2VxNW (1  ,:) = 0;
        c2VxNE  = -(2*dmuNdVxNE.*EyyN-2*mus_N*comp/(pls*dx)-2*dmuSdVxNE.*EyyS)/dy-(2*dmuEdVxNE.*ExyE+mus_E/dy-2*dmuWdVxNE.*ExyW)/dx;  c2VxNE (:,[1 end]) = 0;  c2VxNE (end,:) = 0;
        c2VxSSW =  2*dmuSdVxSSW.*EyyS/dy;                                                                                             c2VxSSW(:,[1 2     end]) = 0;
        c2VxSSE =  2*dmuSdVxSSE.*EyyS/dy;                                                                                             c2VxSSE(:,[1 2     end]) = 0; 
        c2VxNNW = -2*dmuNdVxNNW.*EyyN/dy;                                                                                             c2VxNNW(:,[1 end-1 end]) = 0; 
        c2VxNNE = -2*dmuNdVxNNE.*EyyN/dy;                                                                                             c2VxNNE(:,[1 end-1 end]) = 0;  
        c2VxSWW =  2*dmuWdVxSWW.*ExyW/dx;                                                                                             c2VxSWW([1     2  ],:)   = 0;
        c2VxNWW =  2*dmuWdVxNWW.*ExyW/dx;                                                                                             c2VxNWW([1     2  ],:)   = 0;
        c2VxSEE = -2*dmuEdVxSEE.*ExyE/dx;                                                                                             c2VxSEE([end-1 end],:)   = 0;
        c2VxNEE = -2*dmuEdVxNEE.*ExyE/dx;                                                                                             c2VxNEE([end-1 end],:)   = 0;
        % Equation indexes
        iVy     = NumVyG(:);
        iVyW    = ones(size(Vy));   iVyW(2:end  , :     ) = NumVyG(1:end-1, :     );
        iVyE    = ones(size(Vy));   iVyE(1:end-1, :     ) = NumVyG(2:end  , :     );
        iVyS    = ones(size(Vy));   iVyS( :     ,2:end  ) = NumVyG( :     ,1:end-1);
        iVyN    = ones(size(Vy));   iVyN( :     ,1:end-1) = NumVyG( :     ,2:end  );
        iVySW   = ones(size(Vy));  iVySW(2:end  ,2:end  ) = NumVyG(1:end-1,1:end-1);
        iVySE   = ones(size(Vy));  iVySE(1:end-1,2:end  ) = NumVyG(2:end  ,1:end-1);
        iVyNW   = ones(size(Vy));  iVyNW(2:end  ,1:end-1) = NumVyG(1:end-1,2:end  );
        iVyNE   = ones(size(Vy));  iVyNE(1:end-1,1:end-1) = NumVyG(2:end  ,2:end  );
        iVxSW   = ones(size(Vy));  iVxSW( :     ,2:end  ) = NumVx (1:end-1, :     );
        iVxNW   = ones(size(Vy));  iVxNW( :     ,1:end-1) = NumVx (1:end-1, :     );
        iVxSE   = ones(size(Vy));  iVxSE( :     ,2:end  ) = NumVx (2:end  , :     );
        iVxNE   = ones(size(Vy));  iVxNE( :     ,1:end-1) = NumVx (2:end  , :     );
        iVxSSW  = ones(size(Vy)); iVxSSW( :     ,3:end  ) = NumVx (1:end-1,1:end-1);
        iVxSSE  = ones(size(Vy)); iVxSSE( :     ,3:end  ) = NumVx (2:end  ,1:end-1);
        iVxNNW  = ones(size(Vy)); iVxNNW( :     ,1:end-2) = NumVx (1:end-1,2:end  );
        iVxNNE  = ones(size(Vy)); iVxNNE( :     ,1:end-2) = NumVx (2:end  ,2:end  );
        iVxSWW  = ones(size(Vy)); iVxSWW(2:end  ,2:end  ) = NumVx (1:end-2, :     );
        iVxNWW  = ones(size(Vy)); iVxNWW(2:end  ,1:end-1) = NumVx (1:end-2, :     );
        iVxSEE  = ones(size(Vy)); iVxSEE(1:end-1,2:end  ) = NumVx (3:end  , :     );
        iVxNEE  = ones(size(Vy)); iVxNEE(1:end-1,1:end-1) = NumVx (3:end  , :     );
        % Triplets
        IvvJ    = [   iVy(:);   iVy(:);   iVy(:);   iVy(:);   iVy(:);    iVy(:);    iVy(:);    iVy(:);    iVy(:);    iVy(:);    iVy(:);    iVy(:);    iVy(:);     iVy(:);     iVy(:);     iVy(:);     iVy(:);     iVy(:);     iVy(:);     iVy(:);     iVy(:) ]';
        JvvJ    = [   iVy(:);  iVyW(:);  iVyS(:);  iVyE(:);  iVyN(:);  iVySW(:);  iVySE(:);  iVyNW(:);  iVyNE(:);  iVxSW(:);  iVxSE(:);  iVxNW(:);  iVxNE(:);  iVxSSW(:);  iVxSSE(:);  iVxNNW(:);  iVxNNE(:);  iVxSWW(:);  iVxNWW(:);  iVxSEE(:);  iVxNEE(:) ]';
        VvvJ    = [ c2VyC(:); c2VyW(:); c2VyS(:); c2VyE(:); c2VyN(:); c2VySW(:); c2VySE(:); c2VyNW(:); c2VyNE(:); c2VxSW(:); c2VxSE(:); c2VxNW(:); c2VxNE(:); c2VxSSW(:); c2VxSSE(:); c2VxNNW(:); c2VxNNE(:); c2VxSWW(:); c2VxNWW(:); c2VxSEE(:); c2VxNEE(:) ]';
        % Assemble Jacobian Matrix [UU VV ; VV VU]
        if SuiteSparse==1, J  = sparse2([IuuJ(:);IvvJ(:)], [JuuJ(:);JvvJ(:)], [VuuJ(:);VvvJ(:)]); 
        else               J  = sparse ([IuuJ(:);IvvJ(:)], [JuuJ(:);JvvJ(:)], [VuuJ(:);VvvJ(:)]); end
    end
    % Assemble Picard operator [UU VV ; VV VU]
    if SuiteSparse==1, K  = sparse2([Iuu(:); Ivv(:); Ivu(:)], [Juu(:); Jvv(:); Jvu(:)], [Vuu(:); Vvv(:); Vvu(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx ); 
    else               K  = sparse ([Iuu(:); Ivv(:); Ivu(:)], [Juu(:); Jvv(:); Jvu(:)], [Vuu(:); Vvv(:); Vvu(:)], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx ); end
    %% Contributions from conforming Dirichlets in RHS
    BcK                  = zeros(size(K,1),1);
    BcK(NumVx ( 2  ,: )) = BcK(NumVx(2,:))      + (2-comp)*etac(1  ,:  )'/dx/dx.*Vx_W;                      
    BcK(NumVx (end-1,:)) = BcK(NumVx(end-1,:))  + (2-comp)*etac(end,:  )'/dx/dx.*Vx_E;                             
    BcK(NumVyG(:, 2   )) = BcK(NumVyG(:,2))     + (2-comp)*etac(:  ,1  ) /dy/dy.*Vy_S;
    BcK(NumVyG(:,end-1)) = BcK(NumVyG(:,end-1)) + (2-comp)*etac(:  ,end) /dy/dy.*Vy_N;
    BcK(ibcVxN)          = BcK(ibcVxN) +  ( comp*etac(1:end-1,end) - etav( 2:end-1,end) )/dx/dy.*Vy_N(1:end-1)   ; % VyNW
    BcK(ibcVxN)          = BcK(ibcVxN) +  (-comp*etac(2:end,  end) + etav( 2:end-1,end) )/dx/dy.*Vy_N(2:end  )   ; % VyNE
    BcK(ibcVxS)          = BcK(ibcVxS) +  (-comp*etac(1:end-1, 1 ) + etav( 2:end-1, 1 ) )/dx/dy.*Vy_S(1:end-1)   ; % VySW
    BcK(ibcVxS)          = BcK(ibcVxS) +  ( comp*etac(2:end,   1 ) - etav( 2:end-1, 1 ) )/dx/dy.*Vy_S(2:end  )   ; % VySE
    BcK(ibcVyE)          = BcK(ibcVyE) + (( comp*etac(end,1:end-1) - etav( end,2:end-1) )/dx/dy.*Vx_E(1:end-1)')'; % VxSE
    BcK(ibcVyE)          = BcK(ibcVyE) + ((-comp*etac(end,2:end  ) + etav( end,2:end-1) )/dx/dy.*Vx_E(2:end  )')'; % VxNE
    BcK(ibcVyW)          = BcK(ibcVyW) + ((-comp*etac( 1 ,1:end-1) + etav(  1 ,2:end-1) )/dx/dy.*Vx_W(1:end-1)')'; % VxSW
    BcK(ibcVyW)          = BcK(ibcVyW) + (( comp*etac( 1 ,2:end  ) - etav(  1 ,2:end-1) )/dx/dy.*Vx_W(2:end  )')'; % VxNW
    % Conforming Dirichlets in RHS (scaled)
    BcK(ibc) = [Vx_W*scVx; Vx_E*scVx; Vy_S*scVy; Vy_N*scVy];
    % Build full from lower triangle
    K  = K + K' - diag(diag(K ));
    if Newton==0 || (Newton==1 && iter<nitPicNewt), J=K; end      
    % Evaluate non-Linear residual using matrix-vector products
    bu = BcK; bp = BcD; u = [Vx(:); Vy(:)]; p = Pt(:);            % Assemble velocity/pressure vectors
    fu = -(   K*u + grad*p - bu );  resnlu = norm(fu)/length(fu); % Velocity non-linear residuals
    fp = -( div*u          - bp );  resnlp = norm(fp)/length(fp); % Pressure non-linear residuals
    if iter==1, resnlu0=resnlu; resnlp0=resnlp; end
    if noisy>=1,fprintf('Chk: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', resnlu, resnlu/resnlu0 );
                fprintf('Chk: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', resnlp, resnlp/resnlp0 ); rxvec(iter) = resnlu/resnlu0; end
    if (resnlu/resnlu0 < tol_glob), break; end
    cpu(4)=cpu(4)+toc;
    %% Linear solver - obtain velocity and pressure corrections for current non linear iteration 
    if lsolver==0 % Backslash coupled solver ------------------------------
        tic
        Ms  = [ J   , grad ; ...
                div , 0*PP ];                                                % Assemble entire Jacobian
        f   = [ fu  ; fp   ];                                                % Assemble entire residual vector
        cpu(5)=cpu(5)+toc;
        tic
        dX  = Ms\f;                                                          % Call direct solver
        du  = dX(1:max(NumVyG(:)));                                          % Extract velocity correction
        dp  = dX(NumPtG(:));                                                 % Extract pressure correction
        f1  = Ms*dX - f;                                                     % Compute entire linear residual
        fu1 = f1(1:max(NumVyG(:)));                                          % Extract velocity linear residual
        fp1 = f1(NumPtG(:));                                                 % Extract pressure linear residual
        cpu(6)=cpu(6)+toc;
    elseif lsolver==1 % Powell-Hestenes INCOMPRESSIBLE - Decoupled/segregated solve --------
        if Newton==0, J = K; end
        tic
        Kt  = K - grad*(PPI*div);                                            % Velocity Schur complement of Picard operator (Kt)
        Jt  = J - grad*(PPI*div);                                            % Velocity Schur complement of Jacobian operator
        [Kc,e,s] = chol(Kt,'lower','vector');                                % Choleski factorization of Kt
        cpu(5)=cpu(5)+toc;
        tic
        % Powell-Hestenes iterations
        fu0 = fu;                                                                    % Save linear norm 0
        for itPH=1:nPH
            fut  = fu - grad*dp - grad*PPI*fp;                                       % Iterative right hand side
            [du,norm_r,its] = kspgcr_m(Jt,fut,du,Kc,s,eps_kspgcr,noisy,SuiteSparse); % Apply inverse of Schur complement
            dp   = dp + PPI*(fp -  div*du);                                          % Pressure corrctions
            fu1  = fu -   J*du  - grad*dp;                                           % Compute linear velocity residual
            fp1  = fp - div*du;                                                      % Compute linear pressure residual
            if noisy>1, fprintf('--- iteration %d --- \n',itPH);
                fprintf('  Res. |u| = %2.2e \n',norm(fu1)/length(fu1));
                fprintf('  Res. |p| = %2.2e \n',norm(fp1)/length(fp1));
                fprintf('  KSP GCR: its=%1.4d, Resid=%1.4e \n',its,norm_r); end
            if ((norm(fu1)/length(fu1)) < tol_linu) && ((norm(fp1)/length(fp1)) < tol_linp), break; end
            if ((norm(fu1)/length(fu1)) > (norm(fu0)/length(fu1)) && norm(fu1)/length(fu1) < tol_glob), fprintf(' > Linear residuals do no converge further:\n'); break; end
            fu0 = fu1;
        end
        cpu(6)=cpu(6)+toc;
    end
    tic
    if noisy>=1, fprintf(' - Linear res. ||res.u||=%2.2e\n', norm(fu1)/length(fu1) );
                 fprintf(' - Linear res. ||res.p||=%2.2e\n', norm(fp1)/length(fp1) ); end
    % Call line search - globalization procedure
    if LineSearch==1, [alpha,~] = LineSearch_Direct(nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,mu_i,mu_0,ksi,KRO,Newton,comp,pls,noisy,INV,resnlu0,resnlp0,resnlu,resnlp); end
    u    = u + alpha*du;                                                     % Velocity update from non-linear iterations 
    p    = p + alpha*dp;                                                     % Pressure update from non-linear iterations 
    Vx   = reshape(u(NumVx(:)) ,[nx+1,ny  ]);                                % Velocity in x (2D array)
    Vy   = reshape(u(NumVyG(:)),[nx  ,ny+1]);                                % Velocity in y (2D array)
    Pt   = reshape(p(NumPt(:)) ,[nx  ,ny  ]);                                % Pressure      (2D array)
    cpu(7)=cpu(7)+toc;
end
%% Post-processing
tic
figure(1),clf,colormap('jet'),set(gcf,'Color','white')
plot(1:iter, log10(rxvec(1:iter)/rxvec(1)),'r.-')
xlabel('Iterations'),ylabel('Residuals'),drawnow

figure(2),clf,colormap('jet'),set(gcf,'Color','white')
imagesc(flipud(log10(eiic'*(1/tc)))),colorbar,axis image,title(['max Eii = ',num2str(max(eiic(:)*(1/tc)),'%2.4e')])
xlabel('x','FontSize',15),ylabel('y','FontSize',15)
title(['$\dot{\epsilon}_{II}$',' min = ',num2str((min(eiic(:)*(1/tc))),'%2.2e'),' s$^{-1}$', ' max = ',num2str((max(eiic(:)*(1/tc))),'%2.2e'),' s$^{-1}$'],'interpreter','latex')
set(gca,'FontSize',15)

figure(3),clf,colormap('jet'),set(gcf,'Color','white')
imagesc(flipud(log10(etac'*muc))),colorbar,axis image,title(['max eta = ', num2str(max(etac(:)*muc),'%2.4e')])
xlabel('x','FontSize',15),ylabel('y','FontSize',15)
title(['$\eta$', ' min = ',num2str((min(etac(:)*muc)),'%2.2e'), ' s$^{-1}$',' max = ',num2str((max(etac(:)*muc)),'%2.2e'), ' s$^{-1}$' ],'interpreter','latex')
set(gca, 'FontSize', 15)

cpu(8)=toc; cpu(9)=sum(cpu(1:7));
display([' Time preprocess   = ', num2str(cpu(1))]);
display([' Time BC           = ', num2str(cpu(2))]);
display([' Time cst block    = ', num2str(cpu(3))]);
display([' Time assemble     = ', num2str(cpu(4))]);
display([' Time precondition = ', num2str(cpu(5))]);
display([' Time solve        = ', num2str(cpu(6))]);
display([' => WALLTIME      == ', num2str(cpu(9))]);

Vy  = Vy+Ebg.*yvy2;
VxC = 0.5*(Vx(1:end-1,:) + Vx(2:end,:));
VyC = 0.5*(Vy(:,1:end-1) + Vy(:,2:end));

VyLayer = zeros(size(VyC));
VyLayer(phc==2) = VyC(phc==2);
Vl    = VyLayer(int16(nx/2),:);
vmax  =  min(Vl);
dt    = 1e-5;
At    = A0 + vmax*dt;
q     = log(At/A0)/Ebg/dt - 0*1; %vmax/Ebg/A0 - 1
q2    = vmax/Ebg/A0 - 0*1;
q     = mean(Vl(Vl<0))/A0*2;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    FUNCTIONS USED IN MAIN CODE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [y,norm_r,its] = kspgcr_m(A,b,x,L,perm,eps,noisy,SuiteSparse)
% Originally written by Dave A. May - see reference in main text
restart = 20;
max_it  = 1000;
if SuiteSparse==1, r = cs_gaxpy(-A, x, b);
else               r = -A*x + b; end
norm_r  = norm(r);
ncycles = 1;
its     = 1;
rnorm0  = norm_r;
if noisy==3, fprintf(1,'       %1.4d KSP GCR Residual %1.6e %1.6e\n',0,norm_r,norm_r/rnorm0); end
s   = zeros(size(A,1),1);
VV  = zeros(length(x),restart);
SS  = zeros(length(x),restart);
val = zeros(restart  ,1 );
while its<max_it
    for k = 1:restart
        % Preconditionning
        if SuiteSparse==1, s(perm) = cs_ltsolve(L, cs_lsolve(L,r(perm)));      
        else               s(perm) = L'\(L\r(perm)); end  
        % Action of Jacobian on v
        if SuiteSparse==1, v = cs_gaxpy(A, s, zeros(size(s))); 
        else               v = A*s; end
        % Approximation of the Jv product
        for i = 1:k-1
            val(i) = dot(v, VV(:,i));
        end
        for i = 1:k
            v = v - val(i)*VV(:,i);
            s = s - val(i)*SS(:,i);
        end
        r_dot_v = dot(r, v);
        nrm     = sqrt(dot(v, v));
        r_dot_v = r_dot_v/nrm;
        v       = v/nrm;
        s       = s/nrm;
        x       = x + r_dot_v*s;
        r       = r - r_dot_v*v;
        norm_r  = norm(r);
        its     = its+1;
        if noisy==3, fprintf(1,'[%1.4d] %1.4d KSP GCR Residual %1.6e\n',ncycles,its,norm_r/rnorm0);
                     fprintf(1,'[%1.4d] %1.4d KSP GCR Residual %1.6e %1.6e\n',ncycles,its,norm_r,norm_r/rnorm0); end
        % Check convergence
        if norm_r < eps*rnorm0, break; end
        VV(:,k) = v;
        SS(:,k) = s;
    end
    if k ~= restart, break; end  % terminated restart loop earlier
    ncycles = ncycles+1;
end
y = x;
end

function [alpha, LSsuccess] = LineSearch_Direct(nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,mu_i,mu_0,ksi,KRO,Newton,comp,pls,noisy,INV,resnlu0,resnlp0,resnlu,resnlp)
% Line search explicit algorithm
nsteps = 11;                                   % number of steps
amin   = 0.1;                                  % minimum step
if Newton==1, amax = 1.0; else amax = 2.0; end % maximum step
dalpha = (amax-amin)/(nsteps-1);
alphav = amin:dalpha:amax;
nVx    = (nx+1)*ny; nRMe   = zeros(nsteps,1); nRCTe  = zeros(nsteps,1);
u0 = u; p0 = p;
% Compute non linear residual for different steps
for ils=1:nsteps;
    % Primitive variables with correction step
    u       = u0 + alphav(ils).*du;
    p       = p0 + alphav(ils).*dp;
    % Evaluate non-Linear residual in matrix-free form
    Pt      = reshape(p           ,[nx  ,ny  ]);
    Vx      = reshape(u(1:nVx)    ,[nx+1,ny  ]);
    Vy      = reshape(u(nVx+1:end),[nx  ,ny+1]);
    % Strain rate tensor components
    divV    = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;   
    exx     = diff(Vx,1,1)/dx - comp/pls*divV;      
    eyy     = diff(Vy,1,2)/dy - comp/pls*divV;        
    Vx_e    = [ Vx(:,1), Vx, Vx(:,end) ]; % expand array using BC's - Free slip
    Vy_e    = [ Vy(1,:); Vy; Vy(end,:) ]; % expand array using BC's - Free slip
    exy     = 0.5*( diff(Vx_e,1,2)/dy + diff(Vy_e,1,1)/dx );
    % Strain rate tensor invariant
    if INV==0
        exyn  = 0.25*( exy(1:end-1,1:end-1) + exy(2:end,1:end-1) + exy(1:end-1,2:end) + exy(2:end,2:end));
        exxsi = 0.25*( exx(1:end-1,1:end-1) + exx(2:end,1:end-1) + exx(1:end-1,2:end) + exx(2:end,2:end));
        eyysi = 0.25*( eyy(1:end-1,1:end-1) + eyy(2:end,1:end-1) + eyy(1:end-1,2:end) + eyy(2:end,2:end));
        exxs  = zeros(nx+1,ny+1); exxs(2:end-1,2:end-1) = exxsi;
        eyys  = zeros(nx+1,ny+1); eyys(2:end-1,2:end-1) = eyysi;
        eiic  = sqrt( 0.5*exx .^2 + 0.5*eyy .^2 + exyn.^2 );
        eiiv  = sqrt( 0.5*exxs.^2 + 0.5*eyys.^2 + exy .^2 );
    else
        exxSW = zeros(nx+1,ny+1);  exxSW(2:end-1,2:end-1) = exx(1:end-1,1:end-1);
        exxSE = zeros(nx+1,ny+1);  exxSE(2:end-1,2:end-1) = exx(2:end,1:end-1);
        exxNW = zeros(nx+1,ny+1);  exxNW(2:end-1,2:end-1) = exx(1:end-1,2:end);
        exxNE = zeros(nx+1,ny+1);  exxNE(2:end-1,2:end-1) = exx(2:end,2:end);
        eyySW = zeros(nx+1,ny+1);  eyySW(2:end-1,2:end-1) = eyy(1:end-1,1:end-1);
        eyySE = zeros(nx+1,ny+1);  eyySE(2:end-1,2:end-1) = eyy(2:end,1:end-1);
        eyyNW = zeros(nx+1,ny+1);  eyyNW(2:end-1,2:end-1) = eyy(1:end-1,2:end);
        eyyNE = zeros(nx+1,ny+1);  eyyNE(2:end-1,2:end-1) = eyy(2:end,2:end);
        eiic  = sqrt( 0.5*exx.^2 + 0.5*eyy .^2 + 0.25*(exy(1:end-1,1:end-1).^2 + exy(2:end,1:end-1).^2 + exy(1:end-1,2:end).^2 + exy(2:end,2:end).^2) );
        eiiv  = sqrt(     exy.^2 + 0.5*0.25*(exxSW.^2 + exxSE.^2 + exxNW.^2 + exxNE.^2) + 0.5*0.25*(eyySW.^2 + eyySE.^2 + eyyNW.^2 + eyyNE.^2) );
    end
    % Viscosity
    if KRO==0, etac = Adis(phc).^(-1./ndis(phc)) .* eiic.^((1./ndis(phc))-1) .* exp(Qdis(phc)/Rg./Tec./ndis(phc)); end
    if KRO==0, etav = Adis(phv).^(-1./ndis(phv)) .* eiiv.^((1./ndis(phv))-1) .* exp(Qdis(phv)/Rg./Tev./ndis(phv)); end
    if KRO==1, etac = (mu_i(phc) + (mu_0(phc) - mu_i(phc)) .* ( 1  + (ksi(phc).*eiic).^2 ).^(0.5*(1./ndis(phc)-1))) .* exp(Qdis(phc)/Rg./Tec./ndis(phc)); end
    if KRO==1, etav = (mu_i(phv) + (mu_0(phv) - mu_i(phv)) .* ( 1  + (ksi(phv).*eiiv).^2 ).^(0.5*(1./ndis(phv)-1))) .* exp(Qdis(phv)/Rg./Tev./ndis(phv)); end
    etav([1,end],:) = etav([2,end-1],:); etav(:,[1,end]) = etav(:,[2,end-1]);
    % Momentum residual
    tau_xx     = 2*etac.*exx;
    tau_yy     = 2*etac.*eyy;
    tau_xy     = 2*etav.*exy;
    Res_x      = diff(-Pt + tau_xx,1,1)/dx + diff(tau_xy(2:end-1,:),1,2)/dy;
    Res_y      = diff(-Pt + tau_yy,1,2)/dy + diff(tau_xy(:,2:end-1),1,1)/dx;
    RMe        = [Res_x(:) ; Res_y(:)];
    nRMe(ils)  = norm(RMe)/((nx+1)*ny + (ny+1)*nx);
    % Continuity residual
    RCTe       = -(diff(Vx,1,1)/dx+diff(Vy,1,2)/dy);
    nRCTe(ils) = norm(RCTe(:))/(nx*ny);
end
% Find optimal step (i.e. yielding to lowest residuals)
[~,ibestM] = min(nRMe);
if ibestM==1, LSsuccess=0; alpha=0; fprintf('No descent found - continuing ...')
              nRMe(1) = 2*max(nRMe); [~,ibestM] = min(nRMe);
              LSsuccess=1; alpha = alphav(ibestM);
else          LSsuccess=1; alpha = alphav(ibestM); end

if noisy>=1 
    fprintf(' LS: Selected alpha = %2.2f\n', alpha );
    fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ibestM), nRMe (ibestM)/resnlu0 );
    fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ibestM), nRCTe(ibestM)/resnlp0 );
end
end

function [A] = evalA1(KRO,mus,Eii,n,mu_i,ksi)
% Compute common factor of viscosity derivatives (Power law or Carrreau)
if KRO==0, A = 2*mus.*(1./n-1)./(2*Eii).^2;          
else       A = 1/2*(mus-mu_i).*(1./n-1).*ksi.^2 ./(1+ksi.^2.*Eii.^2); end
A(isnan(A)==1) = 0;
end
