% =========================================================================
% M2Di with Elasto-Visco-Plastic rheology using a compressible 
% displacement-based formulation consistent tangent linearisation  

% Copyright (C) 2018 Thibault Duretz, Alban Souche, Ren� de Borst, Laetitia
% Le Pourhiet

% This file is part of M2Di, please refer to XXXXX for details about
% the implementation.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
clear, clf, clc, close all
% Link to Suitesparse
SuiteSparse   = 0;               % Use Suitesparse routines
addpath('./../SuiteSparse/')     % Link to your SuiteSparse routines
addpath('./_M2Di_EP_functions/') % Link to M2Di_EP functions
% Physics
xmin   =-2e3;
xmax   = 2e3;
ymin   =-2e3;
ymax   = 0;           
dt     = 1e10;        % Time step
eta0   = 1.0e24;      % Shear viscosity
etai   = 1e17;        % Inclusion shear viscosity
K0     = 2e10;        % Bulk modulus
G0     = 1e10;        % Shear modulus
Gi     = G0/4;        % Inclusion shear modulus
rad    = 0.1e3;       % Inclusion radius
incr0  = 1*5e-5;      % Incremental displacement
coh0   = 1*3e7;       % Cohesion
phi0   = 30*pi/180;   % Friction angle
psi0   = 10*pi/180;   % Dilatancy angle
rho0   = 2700;        % Density      
pconf  = 0.0e8;       % Confining pressure
gy     = -0*9.81;     % Vertical gravity component
beta   = 1;           % factor 3 in von Mises - set to 1 for Drucker-Prager
eta_vp = 0;
h      = 0;
fprintf('running with ref. Poisson ratio of %2.2f\n', (3*K0-2*G0) / 2 / (3*K0+G0));
fprintf('running with inc. Poisson ratio of %2.2f\n', (3*K0-2*Gi) / 2 / (3*K0+Gi));
fprintf('Strain rate = %2.2e, flow stress 1 = %2.2e flow stress 2 = %2.2e\n\n', incr0*(xmax-xmin)/dt, incr0*(xmax-xmin)/dt*etai*2, incr0*(xmax-xmin)/dt*eta0*2)
% Numerics
nx            = 101;    % Number of nodes x
ny            = 101;    % Number of nodes y
ncx           = nx-1;   % Number of cells x
ncy           = ny-1;   % Number of cells y
ninc          = 30;     % Number of increments
gitmax        = 100;    % Max. number of global iterations
tol_plast     = 5e-6;   % Tolerance of global iterations
nitPicNewt    = 0;      % Number of Picard steps before Newton
LineSearch    = 1;      % Activates line search
alpha         = 1.0;    % Default correction step
alpha_min     = 0.05;   % Minimum correction step
trial         = 2;      % 0: elastic trial -- 2: last guess
increase_step = 0;      % Allow for increasing increment
safe_mode     = 0;      % 0: Constant increment -- 1: Adaptative -- 2: Adaptative
linear_solver = 0;      % 0: Backslash -- 1: KSP_GCR
symmetry      = 0;      % Uses symmetrized elastic stiffness
saveRunData   = 0;      % Save simulation data to disk
restart       = 0;      % If restart == 1
from_step     = 26;     % Then set from_step = last recorded step
% Initialize
if SuiteSparse == 1, symmetry = 1; end
strain = 0; last_num_its = 0; sin_phi = sin(phi0); cos_phi = cos(phi0); sin_psi = sin(psi0);
Kv   =   K0*ones(nx ,ny ); % Elastic bulk modulus
Gv   =   G0*ones(nx ,ny ); % Elastic shear modulus
etav = eta0*ones(nx ,ny ); % Viscous shear modulus
Cv   = coh0*ones(nx ,ny );  Cv0 = Cv;% cohesion
phic = phi0*ones(ncx,ncy); % phi
phiv = phi0*ones(nx ,ny ); % phi
psic = psi0*ones(ncx,ncy); % psi
psiv = psi0*ones(nx ,ny ); % psi
Kc   =   K0*ones(ncx,ncy); % Elastic bulk modulus
Gc   =   G0*ones(ncx,ncy); % Elastic shear modulus
etac = eta0*ones(ncx,ncy); % Elastic shear modulus
Cc   = coh0*ones(ncx,ncy );  Cc0 = Cc;% cohesion
Sxxc =     zeros(ncx,ncy); % Normal stress
Syyc =     zeros(ncx,ncy); % Normal stress
Txyc =     zeros(ncx,ncy); % Shear stress
Szzc =     zeros(ncx,ncy); % Normal stress
Sxxv =     zeros(nx ,ny ); % Normal stress
Syyv =     zeros(nx ,ny ); % Normal stress
Txyv =     zeros(nx ,ny ); % Shear stress
Szzv =     zeros(nx ,ny ); % Normal stress
Exxc =     zeros(ncx,ncy); % Normal strain
Eyyc =     zeros(ncx,ncy); % Normal strain
Exyc =     zeros(ncx,ncy); % Normal strain
Ezzc =     zeros(ncx,ncy); % Normal strain
Exxv =     zeros(nx ,ny ); % Normal strain
Eyyv =     zeros(nx ,ny ); % Normal strain
Ezzv =     zeros(nx ,ny ); % Normal strain
Ekkc =     zeros(ncx,ncy); % Normal strain
Exyv =     zeros(nx ,ny ); % Shear strain
Ux   =     zeros(nx ,ncy); % Horizontal displacement
Uy   =     zeros(ncx,ny ); % Vertical displacement
Fc1  =     zeros(ncx,ncy); %
Fv1  =     zeros(nx ,ny ); %
Fc   =     zeros(ncx,ncy); %
Fv   =     zeros(nx ,ny ); %
plc  =     zeros(ncx,ncy); % Plastic flag centroids
plv  =     zeros(nx ,ny ); % Plastic flag vertices
plc2 =     zeros(ncx,ncy); % Plastic flag centroids
plv2 =     zeros(nx ,ny ); % Plastic flag vertices
dgc  =     zeros(ncx,ncy); % Plastic increment centroids
dgv  =     zeros(nx ,ny ); % Plastic increment vertices
Eii  =     zeros(ncx,ncy); % accumulated strain
Ep_acc_c   = zeros(ncx,ncy); % accumulated strain
Ep_acc_v   = zeros(nx ,ny ); % accumulated strain   
Ep_acc_c0  = Ep_acc_c;
Ep_acc_v0  = Ep_acc_v;
% For post-processing
Txxc   =    zeros(ncx,ncy);
Tyyc   =    zeros(ncx,ncy);
Tzzc   =    zeros(ncx,ncy);
Txxc0  =    zeros(ncx,ncy);
Tyyc0  =    zeros(ncx,ncy);
Txyc0  =    zeros(ncx,ncy);
Tzzc0  =    zeros(ncx,ncy);
Txxv0  =    zeros(nx ,ny );
Tyyv0  =    zeros(nx ,ny );
Txyv0  =    zeros(nx ,ny );
Tzzv0  =    zeros(nx ,ny );
Txyv   =    zeros(nx ,ny );
Exxdc  =    zeros(ncx,ncy); % Normal strain
Eyydc  =    zeros(ncx,ncy); % Normal strain
Ezzdc  =    zeros(ncx,ncy); % Normal strain
Exxdc0 =    zeros(ncx,ncy); 
Eyydc0 =    zeros(ncx,ncy); 
Ezzdc0 =    zeros(ncx,ncy);
Pc0    =    zeros(ncx,ncy);
Pv0    =    zeros(nx ,ny );
% For monitoring 
increment = zeros(ninc,1);
timevec   = zeros(ninc,1);
Pvec      = zeros(ninc,1);
Tiivec    = zeros(ninc,1);
strvec    = zeros(ninc,1);
nitervec  = zeros(ninc,1);
rxvec_rel = zeros(gitmax,1);    rxvec_abs = zeros(gitmax,1);  
rvec_abs  = zeros(ninc,gitmax); rvec_rel  = zeros(ninc,gitmax);
% 2D model domain
Lx   = xmax - xmin;
Ly   = ymax - ymin;
dx   = Lx/(ncx);
dy   = Ly/(ncy);
xv = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;
yv = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;
[xv2,  yv2] = ndgrid(xv,yv);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xv,yc);
[xvy2,yvy2] = ndgrid(xc,yv);
% Initial configuration - Set perturbation of viscosity
etac((xc2+0*Lx/2).^2+(yc2-ymin/2).^2<rad^2) = etai;  
K         = 1e-4;
dt_diff   = dx^2/K/4.1/5;
diff_time = 1e5;
nt_diff   = ceil(diff_time/dt_diff);
dt_diff   = diff_time/nt_diff;
for i=1:nt_diff
    ec_x = [etac(1,:); etac; etac(end,:)]; 
    ec_y = [etac(:,1), etac, etac(:,end)];
    etac =  etac + K*dt_diff/dx^2 * diff(ec_x,2,1) + K*dt_diff/dy^2 * diff(ec_y,2,2);
end
fprintf('diffusion of jump over a duration of %2.2f and using %02d steps\n', nt_diff*dt_diff, nt_diff);
[ etav ] = M2Di_EP9_centroids2vertices( etac );
% Elasto-viscous moduli
Gvev  = 1./(dt./etav + 1./Gv);
Gvec  = 1./(dt./etac + 1./Gc);
VEv   = Gvev./Gv;
VEc   = Gvec./Gc;
% Initial old stresses
time  = 0;
Exxc0 = Exxc; Exxv0 = Exxv;
Eyyc0 = Eyyc; Eyyv0 = Eyyv;
Exyv0 = Exyv; Exyc0 = Exyc;
Ezzc0 = Ezzc; Ezzv0 = Ezzv;
Ux0   = Ux;
Uy0   = Uy;
%% Numbering Pt and Vx,Vy
NumUx  = reshape(1:nx*ncy,nx,ncy  );
NumUy  = reshape(1:ncx*ny,ncx  ,ny); NumUyG = NumUy + max(NumUx(:));
% Free slip / no slip setting for x momentum
BC.nsxS    =  zeros(size(Ux)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Ux)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Ux)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Ux)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Ux)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Ux)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Ux)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Ux)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
% Free slip / no slip setting for y momentum
BC.nsyW    =  zeros(size(Uy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Uy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Uy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Uy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Uy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Uy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Uy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Uy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
% Free surface
BC.frxN    =  zeros(size(Ux)); %BC.frxN(2:end-1,end) = 1;
BC.fryN    =  zeros(size(Uy)); %BC.fryN( :     ,end) = 1;
%% Boundary Conditions on velocities tangential to boundaries (non-conforming)
BC.Ux_S =   -incr0*xv';
BC.Ux_N =   -incr0*xv';
BC.Uy_W =    incr0*yv';
BC.Uy_E =    incr0*yv';
% Incremental BC displacements
BC.Ux_W   = -incr0.*xvx2(1  ,:)'.*ones(1 ,ncy  )';
BC.Ux_E   = -incr0.*xvx2(end,:)'.*ones(1 ,ncy  )';
BC.Uy_S   =  incr0.*yvy2(:,  1) .*ones(ncx,1   );
BC.Uy_N   =  incr0.*yvy2(:,end) .*ones(ncx,1   );
% Assemble elastic matrix and RHS
[Ke, BcK] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cv,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc  ,dgv  ,Sxxc,  Syyc,  Txyc  ,Szzc,  Sxxv,  Syyv,  Txyv,  Szzv,  dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,     0,beta, BC,sin_phi,sin_psi, cos_phi, coh0, symmetry,rho0,gy, 0, 0, 0, h, eta_vp, dt, 0, 0, 0 );
if symmetry == 1
    % Cholesky
    [Kf, nok, p]     = chol(Ke,'lower','vector');
    if nok ~= 0, error('Cholesky failed'); end
    dFs      = BcK;
    dU(p,1)  = cs_ltsolve(Kf,cs_lsolve(Kf,dFs(p)));
else
    dU = Ke\BcK;
end
dUx_e    = dU(NumUx);
dUy_e    = dU(NumUyG);
ddU      = 0*dU;       % Initialise correction vector
%------------------------ Incremental loop ------------------------ %
inc       = 0;
plastic   = 0;
success   = 0;
if restart==1, load(['run_M2Di_EP_step', num2str(from_step, '%04d'), '.mat']); end
while inc<ninc
    inc = inc + 1;
    fail = 0;
    plc     =  zeros(ncx,ncy); % Plastic flag centroids
    plv     =  zeros(nx ,ny ); % Plastic flag vertices
    dgc     =  zeros(ncx,ncy); % Plastic correction centroids
    dgv     =  zeros(nx ,ny ); % Plastic correction vertices
    gamdotc =  zeros(ncx,ncy); % Plastic correction centroids
    gamdotv =  zeros(nx ,ny ); % Plastic correction vertices
    dQdsxxc =  zeros(ncx,ncy); 
    dQdsyyc =  zeros(ncx,ncy); 
    dQdszzc =  zeros(ncx,ncy); 
    dQdsxyv =  zeros(nx ,ny );

    % Total Strain increment - the trial stress is elastic
    if plastic == 0
        fprintf('ELASTIC TRIAL\n');
        dUxc_t = dUx_e;
        dUyc_t = dUy_e;
    else
        if success == 0 %&& safe_mode > 1,
            incr0 = incr0/1.5;
        else
            fprintf('USING OLD EP SOLUTION AS INITIAL GUESS\n');
            if increase_step==1, incr0 = incr0*1.25; end
        end
        if increase_step==1 || success == 0
            
            fprintf('USING NEW ELASTIC SOLUTION (increment = %2.2e) \n',incr0);
            % Incremental BC displacements
            BC.Ux_W   = -incr0.*xvx2(1  ,:)'.*ones(1 ,ncy  )';
            BC.Ux_E   = -incr0.*xvx2(end,:)'.*ones(1 ,ncy  )';
            BC.Uy_S   =  incr0.*yvy2(:,  1) .*ones(ncx,1   );
            BC.Uy_N   =  incr0.*yvy2(:,end) .*ones(ncx,1   );
            BC.Vx_S =   -incr0*xv';
            BC.Vx_N =   -incr0*xv';
            BC.Vy_W =    incr0*yv';
            BC.Vy_E =    incr0*yv';
            % Assemble elastic matrix and RHS
            [Ke, BcK] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc,Syyc,Txyc,Szzc,Sxxv,Syyv,Txyv,Szzv,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,0,beta, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy, Sxxc0, Syyc0, Txyv0, h, eta_vp, dt, syc, syv, 0 );
            % ELASTIC PREDICATOR
            dFs     = BcK;
            if symmetry == 0, dU      = Ke\BcK;                              end
            if symmetry == 1, dU(p,1) = cs_ltsolve(Kf,cs_lsolve(Kf,dFs(p))); end
            dUx_e   = dU(NumUx);
            dUy_e   = dU(NumUyG);
            dUxc_t  = dUx_e;
            dUyc_t  = dUy_e;
            fail    = 1;
        end
    end
    fprintf('\n#######################################\n');
    fprintf(  '########## Loading step %04d ##########\n', inc);
    fprintf(  '#######################################\n');
    dU        = [dUxc_t(:); dUyc_t(:)];
    rxvec_rel = zeros(gitmax,1);    rxvec_abs = zeros(gitmax,1);    corvec    = zeros(gitmax,1);    alpvec    = zeros(gitmax,1);
    % ------------------------ Global equilibrium: Newton iterations ------------------------ %
    Newton = 0;
    LSsuccess = 1; LIsuccessc=1; LIsuccessv = 1;
    for plast_it = 1:gitmax
        if plast_it>nitPicNewt,  Newton=2; end
        % Initial guess or iterative solution for strain increments
        dExxc_t  = diff(dUxc_t,1,1)/dx;
        dEyyc_t  = diff(dUyc_t,1,2)/dy;
        dEzzc_t  = 0; dEzzv_t  = 0;
        dUx_exp  = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*dUxc_t(:,1) + BC.fsxS(:,1).*dUxc_t(:,1), dUxc_t, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*dUxc_t(:,end) + BC.fsxN(:,end).*dUxc_t(:,end)]; 
        dUy_exp  = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*dUyc_t(1,:) + BC.fsyW(1,:).*dUyc_t(1,:); dUyc_t; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*dUyc_t(end,:) + BC.fsyE(end,:).*dUyc_t(end,:)]; 
        dUxdy    = diff(dUx_exp,1,2)/dy;
        dUydx    = diff(dUy_exp,1,1)/dx;
        dExyv_t  = 0.5*( dUxdy + dUydx );
        % Extrapolate trial strain components
        dExyc_t     = 0.25*(dExyv_t(1:end-1,1:end-1) + dExyv_t(2:end,1:end-1) + dExyv_t(1:end-1,2:end) + dExyv_t(2:end,2:end));
        [ dExxv_t ] = M2Di_EP9_centroids2vertices( dExxc_t );
        [ dEyyv_t ] = M2Di_EP9_centroids2vertices( dEyyc_t );
        % Strains
        Exxc_t = Exxc + dExxc_t;
        Eyyc_t = Eyyc + dEyyc_t;
        Exyv_t = Exyv + dExyv_t;
        Ezzc_t = Ezzc + 0;
        % Save
        dExxc_0 = dExxc_t;  dExxv_0 = dExxv_t; 
        dEyyc_0 = dEyyc_t;  dEyyv_0 = dEyyv_t; 
        dExyc_0 = dExyc_t;  dExyv_0 = dExyv_t; 
        dEzzc_0 = dEzzc_t;  dEzzv_0 = dEzzv_t; 
        % Total stress increments
        dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
        dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
        dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
        dTxyv = 2*Gvev.*dExyv_t ;
        dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
        dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
        dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
        dTxyc = 2*Gvec.*dExyc_t;
        % Total stresses
        dPc     =-1/3*(dSxxc + dSyyc + dSzzc); 
        dPv     =-1/3*(dSxxv + dSyyv + dSzzv); 
        Sxxc_t = VEc .* Txxc0 - Pc0 + dSxxc;
        Syyc_t = VEc .* Tyyc0 - Pc0 + dSyyc;
        Txyv_t = VEv .* Txyv0       + dTxyv;
        Szzc_t = VEc .* Tzzc0 - Pc0 + dSzzc;
        Sxxv_t = VEv .* Txxv0 - Pv0 + dSxxv;
        Syyv_t = VEv .* Tyyv0 - Pv0 + dSyyv;
        Txyc_t = VEc .* Txyc0       + dTxyc;
        Szzv_t = VEv .* Tzzv0 - Pv0 + dSzzv;
        Pc     = Pc0 + dPc;
        Pv     = Pv0 + dPv;
        Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
        Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
        J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
        J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
        syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
        syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
        Fc     =  sqrt(beta*J2c) - syc;
        Fv     =  sqrt(beta*J2v) - syv;
        fprintf('max. trial     Fc = %2.2e\n', max(Fc(:)))
        fprintf('max. trial     Fv = %2.2e\n', max(Fv(:)))
        J2c_t = J2c; J2v_t = J2v; Pc_t = Pc; Pv_t = Pv;
        % Plastic corrections
        if max(Fc(:))>0 || max(Fv(:))>0
            % Plastic flags
            plc = Fc>0;
            plv = Fv>0;
            % dgamma
            dgc = plc.*(Fc./(beta.*Gvec + Kc.*sin_phi*sin_psi + eta_vp/dt + 0*h ));
            dgv = plv.*(Fv./(beta.*Gvev + Kv.*sin_phi*sin_psi + eta_vp/dt + 0*h ));
            % dQds centers
            txx = Pc+Sxxc_t; tyy = Pc+Syyc_t; txy = Txyc_t; tzz = Pc+Szzc_t; J2 = J2c;
            dQdsxxc = beta * txx .* (beta * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsyyc = beta * tyy .* (beta * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsxyc = 0.1000000000e1 * beta * txy .* (beta * J2) .^ (-0.1e1 / 0.2e1);
            dQdszzc = beta * tzz .* (beta * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            % dQds vertices
            txx = Pv+Sxxv_t; tyy = Pv+Syyv_t; txy = Txyv_t; tzz = Pv+Szzv_t; J2 = J2v;
            dQdsxxv = beta * txx .* (beta * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsyyv = beta * tyy .* (beta * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            dQdsxyv = 0.1000000000e1 * beta * txy .* (beta * J2) .^ (-0.1e1 / 0.2e1);
            dQdszzv = beta * tzz .* (beta * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
            % Corrected strain on centers
            dExxc_t = dExxc_0 - plc.*dgc.*dQdsxxc;
            dEyyc_t = dEyyc_0 - plc.*dgc.*dQdsyyc;
            dExyc_t = dExyc_0 - plc.*dgc.*dQdsxyc/2;
            dEzzc_t = dEzzc_0 - plc.*dgc.*dQdszzc;
            % Corrected strain on vertices
            dExxv_t = dExxv_0 - plv.*dgv.*dQdsxxv;
            dEyyv_t = dEyyv_0 - plv.*dgv.*dQdsyyv;
            dExyv_t = dExyv_0 - plv.*dgv.*dQdsxyv/2;
            dEzzv_t = dEzzv_0 - plv.*dgv.*dQdszzv;
            % Update trial plastic strain
            dexxp = plc.*dgc.*dQdsxxc/dt; deyyp = plc.*dgc.*dQdsyyc/dt; dexyp = plc.*dgc.*dQdsxyc/2/dt; dezzp = plc.*dgc.*dQdszzc/dt;
            Ep_dot_c = sqrt(dexxp.^2 + deyyp.^2 + dexyp.^2 + dezzp.^2);
            dexxp = plv.*dgv.*dQdsxxv/dt; deyyp = plv.*dgv.*dQdsyyv/dt; dexyp = plv.*dgv.*dQdsxyv/2/dt; dezzp = plv.*dgv.*dQdszzv/dt;
            Ep_dot_v = sqrt(dexxp.^2 + deyyp.^2 + dexyp.^2 + dezzp.^2);
            Ep_acc_c = Ep_acc_c0   + dt*sqrt(2/3)*Ep_dot_c;
            Ep_acc_v = Ep_acc_v0   + dt*sqrt(2/3)*Ep_dot_v;
            % Total stress increments
            dSxxc = (Kc + 4/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dEzzc_t;
            dSyyc = (Kc + 4/3*Gvec).*dEyyc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEzzc_t;
            dSzzc = (Kc + 4/3*Gvec).*dEzzc_t + (Kc - 2/3*Gvec).*dExxc_t + (Kc - 2/3*Gvec).*dEyyc_t;
            dTxyv = 2*Gvev.*dExyv_t ;
            dSxxv = (Kv + 4/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dEzzv_t;
            dSyyv = (Kv + 4/3*Gvev).*dEyyv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEzzv_t;
            dSzzv = (Kv + 4/3*Gvev).*dEzzv_t + (Kv - 2/3*Gvev).*dExxv_t + (Kv - 2/3*Gvev).*dEyyv_t;
            dTxyc = 2*Gvec.*dExyc_t;
            % Total stresses
            dPc     =-1/3*(dSxxc + dSyyc + dSzzc);
            dPv     =-1/3*(dSxxv + dSyyv + dSzzv);
            Sxxc_t = VEc .* Txxc0 - Pc0 + dSxxc;
            Syyc_t = VEc .* Tyyc0 - Pc0 + dSyyc;
            Txyv_t = VEv .* Txyv0       + dTxyv;
            Szzc_t = VEc .* Tzzc0 - Pc0 + dSzzc;
            Sxxv_t = VEv .* Txxv0 - Pv0 + dSxxv;
            Syyv_t = VEv .* Tyyv0 - Pv0 + dSyyv;
            Txyc_t = VEc .* Txyc0       + dTxyc;
            Szzv_t = VEv .* Tzzv0 - Pv0 + dSzzv;
            Pc     = Pc0 + dPc;
            Pv     = Pv0 + dPv;
            Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
            Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
            J2c    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
            J2v    = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
            syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
            syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
            Fc     =  sqrt(beta*J2c) - syc;
            Fv     =  sqrt(beta*J2v) - syv;
            fprintf('max. Fc = %2.2e\n', max(Fc(:)))
            fprintf('max. Fv = %2.2e\n', max(Fv(:)))
            syc    = Cc.*cos_phi + (Pc+pconf).*sin_phi;
            syv    = Cv.*cos_phi + (Pv+pconf).*sin_phi;
            gamdotc = (sqrt(beta*J2c) - syc) ./ eta_vp;
            gamdotv = (sqrt(beta*J2v) - syv) ./ eta_vp;
            Fc_vp  =  sqrt(beta*J2c) - syc - gamdotc.*eta_vp;
            Fv_vp  =  sqrt(beta*J2v) - syv - gamdotv.*eta_vp;
            fprintf('max. corrected Fc = %2.2e\n', max(Fc(:)))
            fprintf('max. corrected Fv = %2.2e\n', max(Fv(:)))
        end
        
        % Evaluate global residual
        Rx = [zeros(1,ncy); (diff(Sxxc_t,1,1)/dx + diff(Txyv_t(2:end-1,:),1,2)/dy)          ; zeros(1,ncy)];
        Ry = [zeros(ncx,1), (diff(Syyc_t,1,2)/dy + diff(Txyv_t(:,2:end-1),1,1)/dx) + rho0*gy, zeros(ncx,1)];
        R  = [Rx(:); Ry(:)];
        nR = norm(R,1)/length(R);
        if plast_it==1, nR0 = nR; end
        fprintf('Iteration %03d ||F|| = %2.8e - ||F0|| = %2.8e - ||F/F0|| = %2.8e - Newton = %d\n', plast_it, nR, nR0, nR/nR0, Newton);
        rxvec_abs(plast_it) = nR;
        rxvec_rel(plast_it) = nR/nR0;
        
        % Exit if return mapping failed
        if LIsuccessc==0 || LIsuccessv==0, break; end
        % Exit if convergence criteria is met
        if nR<tol_plast
            break;
        else
            % Identify plastic nodes
            plastic = 1;
            % Assemble elasto-plastic tangent matrix
            if Newton>0
                [Kep,~] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,beta, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , Sxxc0, Syyc0, Txyv0, h, eta_vp, dt, syc, syv, 0 );
            end
            if Newton == 0
                % Picard step
                ddU(p,1)  = cs_ltsolve(Kf,cs_lsolve(Kf,R(p)));
            elseif Newton == 1
                [Kepc,~] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,beta, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , Sxxc0, Syyc0, Txyv0, h, eta_vp, dt, syc, syv, 1 );
                [Ksf, nok, p] = chol(Kepc,'lower','vector');
                % Picard step
                ddU(p,1)  = cs_ltsolve(Ksf,cs_lsolve(Ksf,R(p)));
            elseif Newton == 2
                % Newton step
                tic
                if linear_solver == 0
                    ddU       = Kep\R;
                    fprintf('BACKSLASH solve took = %2.2e s\n', toc)
                else
                    [Kepc,~] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gvec,VEc,Cc,phic,psic,Kv,Gvev,VEv,phiv,psiv,dgc,dgv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,beta, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy , Sxxc0, Syyc0, Txyv0, h, eta_vp, dt, syc, syv, 1);
                    Kepc = 1/2*(Kepc + Kepc');
                    % Cholesky
                    [Ksf, nok, p] = chol(Kepc,'lower','vector');
                    if nok ~= 0, error('Cholesky failed'); end
                    ddU           = M2Di_EP5_kspgcr_m(Kep,R,ddU,Ksf,p,tol_plast,3,SuiteSparse);
                    fprintf('KSP_GCR solve took = %2.2e s\n', toc)
                end
            end
            % Line search algorithm
            if LineSearch == 1
                [alpha,LSsuccess] = M2Di_EP12_LineSearchEP(dU,ddU,NumUx,NumUyG,Gvec,VEc,Kc,Cc,phic,psic,Gvev,VEv,Kv,Cv,phiv,psiv,dx,dy,ncx,ncy,Pc0,Pv0,Txxc0,Tyyc0,Txyc0,Tzzc0,Txxv0,Tyyv0,Txyv0,Tzzv0, beta, BC, sin_phi,cos_phi,sin_psi, safe_mode, nR, rho0,gy,pconf, eta_vp, dt, alpha_min);
            end                                         
            if LSsuccess == 1
                dU     = dU + alpha*ddU;
                dUxc_t = dU(NumUx);
                dUyc_t = dU(NumUyG);
                nC     = norm(ddU)/length(ddU);
            else
                % If line search failed: exit
                plast_it = gitmax;
                break;
            end
        end
        alpvec(plast_it) = alpha;
        corvec(plast_it) = nC;        
    end
    if  nR<tol_plast
        % Update total strains and displacements
        Exxc = Exxc_t;
        Eyyc = Eyyc_t;
        Ezzc = Ezzc_t;
        Exyv = Exyv_t;
        Ekkc = Exxc_t + Eyyc_t + Ezzc_t;
        dExxdc_0 = dExxc_0 - 1/3*(dExxc_0+dEyyc_0);
        dEyydc_0 = dEyyc_0 - 1/3*(dExxc_0+dEyyc_0);
        dEzzdc_0 = dEzzc_0 - 1/3*(dExxc_0+dEyyc_0);
        dExydv_0 = dExyv_0;
        Ux   = Ux0 + dUxc_t;
        Uy   = Uy0 + dUyc_t;
        Ux0  = Ux;
        Uy0  = Uy;
        strain = strain + incr0;
        time   = time + dt;
        timevec(inc)    = time;
        increment(inc)  = incr0;
        Tiivec(inc)     = mean(sqrt(beta*J2c(:)));
        strvec(inc)     = strain;
        rvec_rel(inc,:) = rxvec_rel(:);
        rvec_abs(inc,:) = rxvec_abs(:);
        nitervec(inc)   = plast_it;
        Pvec(inc)       = Pc(fix(nx/2), fix(ny/2));
        % Deviatoric strain invariant
        Exxdc = Exxc - 1/3*Ekkc;
        Eyydc = Eyyc - 1/3*Ekkc;
        Ezzdc = Ezzc - 1/3*Ekkc;
        Eii   = M2Di_EP10_InvariantOnCentroids(Exxdc   , Eyydc,   Ezzdc,   Exyv   );      
        % Deviatoric strain rate invariant
        Exxcr = (Exxc-Exxc0)/dt; Eyycr = (Eyyc-Eyyc0)/dt; Ezzcr = (Ezzc-Ezzc0)/dt; Exyvr = (Exyv-Exyv0)/dt;
        Ekkcr = Exxcr + Eyycr + Ezzcr;
        Exxdr = Exxcr - 1/3*Ekkcr;
        Eyydr = Eyycr - 1/3*Ekkcr;
        Ezzdr = Ezzcr - 1/3*Ekkcr;
        Eiir  = M2Di_EP10_InvariantOnCentroids(Exxdr   , Eyydr,   Ezzdr,   Exyvr   );
        % Deviatoric strain rates decomposed
        Ekkp    = dgc.*(dQdsxxc + dQdsyyc + dQdszzc);
        Exxcr   = dExxdc_0/dt;                     Eyycr   = dEyydc_0/dt;                     Ezzcr   = dEzzdc_0/dt;                     Exyvr   = dExyv_0/dt;
        Exxcr_e = (Txxc-Txxc0)./2./Gc/dt;          Eyycr_e = (Tyyc-Tyyc0)./2./Gc/dt;          Ezzcr_e = (Tzzc-Tzzc0)./2./Gc/dt;          Exyvr_e = (Txyv-Txyv0)./2./Gv/dt;
        Exxcr_v = Txxc./2./etac;                   Eyycr_v = Tyyc./2./etac;                   Ezzcr_v = Tzzc./2./etac;                   Exyvr_v = Txyv./2./etav;
        Exxcr_p = 1/dt*(dgc.*dQdsxxc - 1/3.*Ekkp); Eyycr_p = 1/dt*(dgc.*dQdsyyc - 1/3.*Ekkp); Ezzcr_p = 1/dt*(dgc.*dQdszzc - 1/3.*Ekkp); Exyvr_p = 1/dt*dgv.*dQdsxyv / 2;
        Eiicr   = M2Di_EP10_InvariantOnCentroids(Exxcr   , Eyycr,   Ezzcr,   Exyvr   );
        Eiicr_e = M2Di_EP10_InvariantOnCentroids(Exxcr_e , Eyycr_e, Ezzcr_e, Exyvr_e );
        Eiicr_v = M2Di_EP10_InvariantOnCentroids(Exxcr_v , Eyycr_v, Ezzcr_v, Exyvr_v );
        Eiicr_p = M2Di_EP10_InvariantOnCentroids(Exxcr_p , Eyycr_p, Ezzcr_p, Exyvr_p );
        Exx_net = Exxcr-Exxcr_e-Exxcr_v-Exxcr_p;
        Eyy_net = Eyycr-Eyycr_e-Eyycr_v-Eyycr_p;
        Ezz_net = Ezzcr-Ezzcr_e-Ezzcr_v-Ezzcr_p;
        Exy_net = Exyvr-Exyvr_e-Exyvr_v-Exyvr_p;
        Eii_net = M2Di_EP10_InvariantOnCentroids(Exx_net   , Eyy_net,   Ezz_net,   Exy_net   );
        % Update old stresses
        Pc0 = Pc; Pv0 = Pv; 
        Sxxc0 = Sxxc_t; Sxxv0 = Sxxv_t;
        Syyc0 = Syyc_t; Syyv0 = Syyv_t;
        Txyv0 = Txyv_t; Txyc0 = Txyc_t;
        Szzc0 = Szzc_t; Szzv0 = Szzv_t;
        Txxc0 = Txxc  ; Tyyc0 = Tyyc  ; Tzzc0 = Tzzc;
        Txxv0 = Txxv  ; Tyyv0 = Tyyv  ; Tzzv0 = Tzzv;
        Exxdc0 = Exxdc;Eyydc0 = Eyydc;Ezzdc0 = Ezzdc; Exyv0 = Exyv;
        % Accumulated strain
        Ep_acc_c0  = Ep_acc_c;
        Ep_acc_v0  = Ep_acc_v;
        % Softening (explicit form)
        Cc     = Cc0 + h*Ep_acc_c ; Cc(Cc<0.00) = 0.00;
        Cv     = Cv0 + h*Ep_acc_v ; Cv(Cv<0.00) = 0.00; 
        Cc0 = Cc;
        Cv0 = Cv;
        last_num_its = plast_it;
        success = 1;
    else
        % Then try with smaller increment
        success = 0;
        inc = inc-1; 
    end
     
    % Visualize (only after elasto-plastic iterations)
    if plast_it>1 && success==1
        
%         figure(2), clf
%         subplot(311),
%         imagesc(xc, yc, log10(abs(Eii'))), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. Eii = ', num2str(min(Eii(:))), ' max. Eii = ', num2str(max(Eii(:)))])
%         subplot(312),
%         imagesc(xc, yc, log10(abs(Eiicr'))), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. Eiir = ', num2str(min(Eiir(:))), ' max. Eiir = ', num2str(max(Eiir(:)))])
%         subplot(313),
%         imagesc(xc, yc, log10(abs(Pc'))), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. P = ', num2str(min(Pc(:))), ' max. P = ', num2str(max(Pc(:)))])
%         if saveRunData==1, print(['Strain',num2str(inc,'%04d')], '-r300','-dpng'); end
% 
        figure(5), clf
        subplot(511)
        imagesc(xc,yc,log10(Eiicr)'), colorbar, axis image, set(gca, 'ydir', 'normal'); caxis([-16 -12.5]);
        title('Total deviatoric strain rate invariant')
        subplot(512)
        imagesc(xc,yc,log10(Eiicr_e)'), colorbar, axis image, set(gca, 'ydir', 'normal'); caxis([-16 -12.5]);
        title('Elastic deviatoric strain rate invariant')
        subplot(513)
        imagesc(xc,yc,log10(Eiicr_v)'), colorbar, axis image, set(gca, 'ydir', 'normal'); caxis([-16 -12.5]);
        title('Viscous deviatoric strain rate invariant')
        subplot(514)
        imagesc(xc,yc,log10((Eiicr_p))'), colorbar, axis image, set(gca, 'ydir', 'normal'); caxis([-16 -12.5]);
        title('Plastic deviatoric strain rate invariant')
        subplot(515)
        Exx_net = Exxcr-Exxcr_e-Exxcr_v-Exxcr_p;
        Eyy_net = Eyycr-Eyycr_e-Eyycr_v-Eyycr_p;
        Ezz_net = Ezzcr-Ezzcr_e-Ezzcr_v-Ezzcr_p;
        Exy_net = Exyvr-Exyvr_e-Exyvr_v-Exyvr_p;
        Eii_net = M2Di_EP10_InvariantOnCentroids(Exx_net   , Eyy_net,   Ezz_net,   Exy_net   ); 
        imagesc(xc,yc,log10(abs(Eii_net))'), colorbar, axis image, set(gca, 'ydir', 'normal'); %caxis([-16 -12.5]);
        title('Net deviatoric strain rate invariant')
       
        figure(3), clf
        subplot(311)
        plot(strvec(1:inc),log10(abs(increment(1:inc))), 'xb')
        title('Increment vs. strain')
        subplot(312), hold on
        plot(strvec(1:inc),Tiivec(1:inc), 'dr')
        title('Stress vs. strain')
        subplot(313)
        plot(1:plast_it, log10(rxvec_abs(1:plast_it)),'ok')
        title('Global residual vs. iterations')
        drawnow
        
        if saveRunData==1, save(['run_M2Di_EP_step', num2str(inc,'%04d')],'Sxxc_t','Syyc_t','Txyc_t','Szzc_t','Sxxv_t','Syyv_t','Txyv_t','Szzv_t','Ux','Uy','Exxc','Eyyc','Exyv','Ezzc','xc','yc','xv','yv','dx','dy','strain', 'inc', 'plastic','success','dUx_e','dUy_e','incr0','Sxxc0','Syyc0','Txyc0','Szzc0','Sxxv0','Syyv0','Txyv0','Szzv0','Eii_net','Eiicr_p','Eiicr_v','Eiicr_e','Eiicr'); end
    end
end
if saveRunData==1, save(['run_M2Di_EP_res', num2str(nx,'%3d'),'_nstep', num2str(ninc,'%3d')],'Tiivec','strvec','rvec_rel','rvec_abs','nitervec','increment'); end
