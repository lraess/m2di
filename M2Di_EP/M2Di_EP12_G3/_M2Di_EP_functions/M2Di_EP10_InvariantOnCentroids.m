% =========================================================================
% M2Di with Elasto-Visco-Plastic rheology using a compressible 
% displacement-based formulation consistent tangent linearisation  

% Copyright (C) 2018 Thibault Duretz, Alban Souche, Ren� de Borst, Laetitia
% Le Pourhiet

% This file is part of M2Di, please refer to XXXXX for details about
% the implementation.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
function [Eiic]   = M2Di_EP10_InvariantOnCentroids(Exxc   , Eyyc,   Ezzc,   Exyv   )
Exyc = 0.25*(Exyv(1:end-1,1:end-1) + Exyv(2:end,1:end-1) + Exyv(1:end-1,2:end) + Exyv(2:end,2:end));
Eiic  = sqrt(1/2*(Exxc).^2 + 1/2*(Eyyc).^2 + 1/2*(Ezzc).^2 + Exyc.^2);
end

