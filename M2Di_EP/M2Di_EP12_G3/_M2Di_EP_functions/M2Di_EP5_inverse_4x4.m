% =========================================================================
% M2Di with Elasto-Visco-Plastic rheology using a compressible 
% displacement-based formulation consistent tangent linearisation  

% Copyright (C) 2018 Thibault Duretz, Alban Souche, Ren� de Borst, Laetitia
% Le Pourhiet

% This file is part of M2Di, please refer to XXXXX for details about
% the implementation.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
function [ Mi11,Mi12,Mi13,Mi14, Mi21,Mi22,Mi23,Mi24, Mi31,Mi32,Mi33,Mi34, Mi41,Mi42,Mi43,Mi44  ] = M2Di_EP5_inverse_4x4( M11,M12,M13,M14, M21,M22,M23,M24, M31,M32,M33,M34, M41,M42,M43,M44  )
A2323 = M33 .* M44 - M34 .* M43 ;
A1323 = M32 .* M44 - M34 .* M42 ;
A1223 = M32 .* M43 - M33 .* M42 ;
A0323 = M31 .* M44 - M34 .* M41 ;
A0223 = M31 .* M43 - M33 .* M41 ;
A0123 = M31 .* M42 - M32 .* M41 ;
A2313 = M23 .* M44 - M24 .* M43 ;
A1313 = M22 .* M44 - M24 .* M42 ;
A1213 = M22 .* M43 - M23 .* M42 ;
A2312 = M23 .* M34 - M24 .* M33 ;
A1312 = M22 .* M34 - M24 .* M32 ;
A1212 = M22 .* M33 - M23 .* M32 ;
A0313 = M21 .* M44 - M24 .* M41 ;
A0213 = M21 .* M43 - M23 .* M41 ;
A0312 = M21 .* M34 - M24 .* M31 ;
A0212 = M21 .* M33 - M23 .* M31 ;
A0113 = M21 .* M42 - M22 .* M41 ;
A0112 = M21 .* M32 - M22 .* M31 ;

det = M11 .* ( M22 .* A2323 - M23 .* A1323 + M24 .* A1223 ) ...
    - M12 .* ( M21 .* A2323 - M23 .* A0323 + M24 .* A0223 ) ...
    + M13 .* ( M21 .* A1323 - M22 .* A0323 + M24 .* A0123 ) ...
    - M14 .* ( M21 .* A1223 - M22 .* A0223 + M23 .* A0123 ) ;
det = 1 ./ det;


   Mi11 = det .*   ( M22 .* A2323 - M23 .* A1323 + M24 .* A1223 );
   Mi12 = det .* - ( M12 .* A2323 - M13 .* A1323 + M14 .* A1223 );
   Mi13 = det .*   ( M12 .* A2313 - M13 .* A1313 + M14 .* A1213 );
   Mi14 = det .* - ( M12 .* A2312 - M13 .* A1312 + M14 .* A1212 );
   Mi21 = det .* - ( M21 .* A2323 - M23 .* A0323 + M24 .* A0223 );
   Mi22 = det .*   ( M11 .* A2323 - M13 .* A0323 + M14 .* A0223 );
   Mi23 = det .* - ( M11 .* A2313 - M13 .* A0313 + M14 .* A0213 );
   Mi24 = det .*   ( M11 .* A2312 - M13 .* A0312 + M14 .* A0212 );
   Mi31 = det .*   ( M21 .* A1323 - M22 .* A0323 + M24 .* A0123 );
   Mi32 = det .* - ( M11 .* A1323 - M12 .* A0323 + M14 .* A0123 );
   Mi33 = det .*   ( M11 .* A1313 - M12 .* A0313 + M14 .* A0113 );
   Mi34 = det .* - ( M11 .* A1312 - M12 .* A0312 + M14 .* A0112 );
   Mi41 = det .* - ( M21 .* A1223 - M22 .* A0223 + M23 .* A0123 );
   Mi42 = det .*   ( M11 .* A1223 - M12 .* A0223 + M13 .* A0123 );
   Mi43 = det .* - ( M11 .* A1213 - M12 .* A0213 + M13 .* A0113 );
   Mi44 = det .*   ( M11 .* A1212 - M12 .* A0212 + M13 .* A0112 );

end