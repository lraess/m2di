% =========================================================================
% M2Di with Elasto-Visco-Plastic rheology using a compressible 
% displacement-based formulation consistent tangent linearisation  

% Copyright (C) 2018 Thibault Duretz, Alban Souche, Ren� de Borst, Laetitia
% Le Pourhiet

% This file is part of M2Di, please refer to XXXXX for details about
% the implementation.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
function  [Kep,BcK] = M2Di_EP10_AssembleStiffness(plc,plv,plc2,plv2,Kc,Gc,VEc,Cc,phic,psic,Kv,Gv,VEv,phiv,psiv,dlamc,dlamv,Sxxc_t,Syyc_t,Txyc_t,Szzc_t,Sxxv_t,Syyv_t,Txyv_t,Szzv_t,dx,dy,nx,ny,ncx,ncy,NumUx,NumUy,NumUyG,Ux,Uy,Newton,vm, BC,sin_phi,sin_psi, cos_phi, coh0,symmetry,rho0,gy, Sxxc0, Syyc0, Txyv0, h, eta_vp, dt, syc, syv, precond )

% Flags
viscous = 0;
vp      = 1;
H       = 0*h;
% For pre-conditionning purposes
if precond == 2;
    cancel = 0;
else 
    cancel = 1;
end
% RHS
BcK = zeros(max(NumUyG(:)),1);
% Normal stress
frxN = BC.frxN; fryN = BC.fryN;
free_surf = 0;
if (sum(frxN(:,end))>0), free_surf = 1; end
% Boundary condition flags [W E S N]
fsxW = BC.fsxW; fsxE = BC.fsxE;
fsxS = BC.fsxS; fsxN = BC.fsxN;
nsxW = BC.nsxW; nsxE = BC.nsxE;
nsxS = BC.nsxS; nsxN = BC.nsxN;
% Boundary condition flags [W E S N]
fsyW = BC.fsyW; fsyE = BC.fsyE;
fsyS = BC.fsyS; fsyN = BC.fsyN;
nsyW = BC.nsyW; nsyE = BC.nsyE;
nsyS = BC.nsyS; nsyN = BC.nsyN;
% Stencil weights for normal strains interpolation
wSW   = ones(nx+0,ny+0); wSW(end,:) = 2; wSW(:,end) = 2; wSW(end,end) = 4; wSW(1  ,:) = 0; wSW(:,  1) = 0;
wSE   = ones(nx+0,ny+0); wSE(  1,:) = 2; wSE(:,end) = 2; wSE(  1,end) = 4; wSE(end,:) = 0; wSE(:,  1) = 0;
wNW   = ones(nx+0,ny+0); wNW(end,:) = 2; wNW(:,  1) = 2; wNW(end,  1) = 4; wNW(1  ,:) = 0; wNW(:,end) = 0;
wNE   = ones(nx+0,ny+0); wNE(  1,:) = 2; wNE(:,  1) = 2; wNE(  1,  1) = 4; wNE(end,:) = 0; wNE(:,end) = 0;
% Always 2
if Newton==0, consistent = 0; end % Picard
if Newton==1, consistent = 1; end % Continuum tangent
if Newton==2, consistent = 1; end % Consistent tangent
% Standard elastic stiffness matrix - centroids
De11c    = Kc + 4/3.*Gc;  De12c    = Kc - 2/3.*Gc;  De13c    = 0*Kc;  De14c    = Kc - 2/3.*Gc;
De21c    = Kc - 2/3.*Gc;  De22c    = Kc + 4/3.*Gc;  De23c    = 0*Kc;  De24c    = Kc - 2/3.*Gc;
De31c    = 0.*Gc;         De32c    = 0.*Gc;         De33c    = 1.*Gc; De34c    = 0.*Gc;
De41c    = Kc - 2/3.*Gc;  De42c    = Kc - 2/3.*Gc;  De43c    = 0*Kc;  De44c    = Kc + 4/3.*Gc;
% Standard elastic stiffness matrix - vertices
De11v    = Kv + 4/3.*Gv;  De12v    = Kv - 2/3.*Gv;  De13v    = 0*Kv;  De14v    = Kv - 2/3.*Gv;
De21v    = Kv - 2/3.*Gv;  De22v    = Kv + 4/3.*Gv;  De23v    = 0*Kv;  De24v    = Kv - 2/3.*Gv;
De31v    = 0.*Gv;         De32v    = 0.*Gv;         De33v    = 1.*Gv; De34v    = 0.*Gv;
De41v    = Kv - 2/3.*Gv;  De42v    = Kv - 2/3.*Gv;  De43v    = 0*Kv;  De44v    = Kv + 4/3.*Gv;
% Initialise EP matrix - centroids
Dep11c    = Kc + 4/3.*Gc;  Dep12c    = Kc - 2/3.*Gc;  Dep13c    = 0*Kc;  Dep14c    = Kc - 2/3.*Gc;
Dep21c    = Kc - 2/3.*Gc;  Dep22c    = Kc + 4/3.*Gc;  Dep23c    = 0*Kc;  Dep24c    = Kc - 2/3.*Gc;
Dep31c    = 0.*Gc;         Dep32c    = 0.*Gc;         Dep33c    = 1.*Gc; Dep34c    = 0.*Gc;
Dep41c    = Kc - 2/3.*Gc;  Dep42c    = Kc - 2/3.*Gc;  Dep43c    = 0*Kc;  Dep44c    = Kc + 4/3.*Gc;
% Initialise EP matrix - vertices
Dep11v    = Kv + 4/3.*Gv;  Dep12v    = Kv - 2/3.*Gv;  Dep13v    = 0*Kv;  Dep14v    = Kv - 2/3.*Gv;
Dep21v    = Kv - 2/3.*Gv;  Dep22v    = Kv + 4/3.*Gv;  Dep23v    = 0*Kv;  Dep24v    = Kv - 2/3.*Gv;
Dep31v    = 0.*Gv;         Dep32v    = 0.*Gv;         Dep33v    = 1.*Gv; Dep34v    = 0.*Gv;
Dep41v    = Kv - 2/3.*Gv;  Dep42v    = Kv - 2/3.*Gv;  Dep43v    = 0*Kv;  Dep44v    = Kv + 4/3.*Gv;
%% Consistent tangent operator for centroids
dlam = dlamc; 
Pc   = -1/3*(Sxxc_t+Syyc_t+Szzc_t); 
Txxc = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
J2c  = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
txx = Pc+Sxxc_t; tyy = Pc+Syyc_t; txy = Txyc_t; tzz = Pc+Szzc_t; J2 = J2c;
% dQds centers
dQdsxx = vm * txx .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
dQdsyy = vm * tyy .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
dQdsxy = 0.1000000000e1 * vm * txy .* (vm * J2) .^ (-0.1e1 / 0.2e1);
dQdszz = vm * tzz .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
% dFds centers
dFdsxx = vm * txx .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_phi / 0.3e1;
dFdsyy = vm * tyy .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_phi / 0.3e1;
dFdsxy = 0.1000000000e1 * vm * txy .* (vm * J2) .^ (-0.1e1 / 0.2e1);
dFdszz = vm * tzz .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_phi / 0.3e1;
% Reduced elastic stiffness matrix
if consistent==1
    d2Qdsxxdsxx = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 - vm .^ 2 .* txx .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsxxdsyy = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tyy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsxxdsxy = -vm .^ 2 .* txx .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsxxdszz = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsyydsxx = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tyy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsyydsyy = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 - vm .^ 2 .* tyy .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsyydsxy = -vm .^ 2 .* tyy .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsyydszz = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* tyy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsxydsxx = -vm .^ 2 .* txx .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsxydsyy = -vm .^ 2 .* tyy .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsxydsxy = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) - vm .^ 2 .* txy .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1);
    d2Qdsxydszz = -vm .^ 2 .* txy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdszzdsxx = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdszzdsyy = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* tyy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdszzdsxy = -vm .^ 2 .* txy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdszzdszz = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 - vm .^ 2 .* tzz .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    if Newton==1, dlam = 0*dlam; end
    D11 = De11c; D12 = De12c; D13 = De13c; D14 = De14c;
    D21 = De21c; D22 = De22c; D23 = De23c; D24 = De24c;
    D31 = De31c; D32 = De32c; D33 = De33c; D34 = De34c;
    D41 = De41c; D42 = De42c; D43 = De43c; D44 = De44c;
    M11 = 1 + dlam .* (D11 .* d2Qdsxxdsxx + D12 .* d2Qdsyydsxx + D14 .* d2Qdszzdsxx);
    M12 = dlam .* (D11 .* d2Qdsxxdsyy + D12 .* d2Qdsyydsyy + D14 .* d2Qdszzdsyy);
    M13 = dlam .* (D11 .* d2Qdsxxdsxy + D12 .* d2Qdsyydsxy + D14 .* d2Qdszzdsxy);
    M14 = dlam .* (D11 .* d2Qdsxxdszz + D12 .* d2Qdsyydszz + D14 .* d2Qdszzdszz);
    M21 = dlam .* (D21 .* d2Qdsxxdsxx + D22 .* d2Qdsyydsxx + D24 .* d2Qdszzdsxx);
    M22 = 1 + dlam .* (D21 .* d2Qdsxxdsyy + D22 .* d2Qdsyydsyy + D24 .* d2Qdszzdsyy);
    M23 = dlam .* (D21 .* d2Qdsxxdsxy + D22 .* d2Qdsyydsxy + D24 .* d2Qdszzdsxy);
    M24 = dlam .* (D21 .* d2Qdsxxdszz + D22 .* d2Qdsyydszz + D24 .* d2Qdszzdszz);
    M31 = dlam .* D33 .* d2Qdsxydsxx;
    M32 = dlam .* D33 .* d2Qdsxydsyy;
    M33 = 1 + dlam .* D33 .* d2Qdsxydsxy;
    M34 = dlam .* D33 .* d2Qdsxydszz;
    M41 = dlam .* (D41 .* d2Qdsxxdsxx + D42 .* d2Qdsyydsxx + D44 .* d2Qdszzdsxx);
    M42 = dlam .* (D41 .* d2Qdsxxdsyy + D42 .* d2Qdsyydsyy + D44 .* d2Qdszzdsyy);
    M43 = dlam .* (D41 .* d2Qdsxxdsxy + D42 .* d2Qdsyydsxy + D44 .* d2Qdszzdsxy);
    M44 = 1 + dlam .* (D41 .* d2Qdsxxdszz + D42 .* d2Qdsyydszz + D44 .* d2Qdszzdszz);
    [ Mi11,Mi12,Mi13,Mi14, Mi21,Mi22,Mi23,Mi24, Mi31,Mi32,Mi33,Mi34, Mi41,Mi42,Mi43,Mi44  ] = M2Di_EP5_inverse_4x4( M11,M12,M13,M14, M21,M22,M23,M24, M31,M32,M33,M34, M41,M42,M43,M44  );
    den = (dFdsxx .* Mi11 + dFdsyy .* Mi21 + dFdsxy .* Mi31 + dFdszz .* Mi41) .* (D11 .* dQdsxx + D12 .* dQdsyy + D14 .* dQdszz) + (dFdsxx .* Mi12 + dFdsyy .* Mi22 + dFdsxy .* Mi32 + dFdszz .* Mi42) .* (D21 .* dQdsxx + D22 .* dQdsyy + D24 .* dQdszz) + (dFdsxx .* Mi13 + dFdsyy .* Mi23 + dFdsxy .* Mi33 + dFdszz .* Mi43) .* D33 .* dQdsxy + (dFdsxx .* Mi14 + dFdsyy .* Mi24 + dFdsxy .* Mi34 + dFdszz .* Mi44) .* (D41 .* dQdsxx + D42 .* dQdsyy + D44 .* dQdszz);;
    den = den + vp.*eta_vp/dt + H;
    Dep11c = Mi11 .* (D11 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D12 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D14 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) + Mi12 .* (D21 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D22 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D24 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - Mi13 .* D33 ./ den .* (dQdsxy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) + Mi14 .* (D41 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D42 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D44 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)));
    Dep12c = Mi11 .* (-D11 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D12 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D14 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) + Mi12 .* (-D21 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D22 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D24 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - Mi13 .* D33 ./ den .* (dQdsxy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + Mi14 .* (-D41 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D42 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D44 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)));
    Dep13c = Mi11 .* (-D11 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D12 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D14 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33)) + Mi12 .* (-D21 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D22 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D24 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33)) + Mi13 .* D33 .* (1 - 1 ./ den .* (dQdsxy .* dFdsxx .* Mi13 .* D33 + dQdsxy .* dFdsyy .* Mi23 .* D33 + dQdsxy .* dFdsxy .* Mi33 .* D33 + dQdsxy .* dFdszz .* Mi43 .* D33)) + Mi14 .* (-D41 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D42 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D44 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33));
    Dep21c = Mi21 .* (D11 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D12 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D14 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) + Mi22 .* (D21 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D22 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D24 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - Mi23 .* D33 ./ den .* (dQdsxy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) + Mi24 .* (D41 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D42 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D44 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)));
    Dep22c = Mi21 .* (-D11 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D12 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D14 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) + Mi22 .* (-D21 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D22 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D24 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - Mi23 .* D33 ./ den .* (dQdsxy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + Mi24 .* (-D41 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D42 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D44 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)));
    Dep23c = Mi21 .* (-D11 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D12 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D14 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33)) + Mi22 .* (-D21 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D22 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D24 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33)) + Mi23 .* D33 .* (1 - 1 ./ den .* (dQdsxy .* dFdsxx .* Mi13 .* D33 + dQdsxy .* dFdsyy .* Mi23 .* D33 + dQdsxy .* dFdsxy .* Mi33 .* D33 + dQdsxy .* dFdszz .* Mi43 .* D33)) + Mi24 .* (-D41 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D42 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D44 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33));
end
% For pre-conditionning purposes
if precond == 1
    Pc     =-1/3*(Sxxc_t + Syyc_t + Szzc_t);
    Txxc   = Pc + Sxxc_t; Tyyc = Pc + Syyc_t; Tzzc = Pc + Szzc_t; Txyc = Txyc_t;
    J2_t   = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
    Dep11c = (Kc + 0.4e1 ./ 0.3e1 .* Gc) .* (0.1e1 - 0.2e1 ./ 0.3e1 .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1)) + 0.2e1 ./ 0.3e1 .* (Kc - 0.2e1 ./ 0.3e1 .* Gc) .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1);
    Dep12c = (Kc + 0.4e1 ./ 0.3e1 .* Gc) .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 + (Kc - 0.2e1 ./ 0.3e1 .* Gc) .* (0.1e1 - 0.2e1 ./ 0.3e1 .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1)) + (Kc - 0.2e1 ./ 0.3e1 .* Gc) .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1;
    Dep13c = 0*Dep13c;
    Dep21c = (Kc + 0.4e1 ./ 0.3e1 .* Gc) .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 + (Kc - 0.2e1 ./ 0.3e1 .* Gc) .* (0.1e1 - 0.2e1 ./ 0.3e1 .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1)) + (Kc - 0.2e1 ./ 0.3e1 .* Gc) .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1;
    Dep22c = (Kc + 0.4e1 ./ 0.3e1 .* Gc) .* (0.1e1 - 0.2e1 ./ 0.3e1 .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1)) + 0.2e1 ./ 0.3e1 .* (Kc - 0.2e1 ./ 0.3e1 .* Gc) .* dlam .* vm .* Gc .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1);
    Dep23c = 0*Dep23c;
end

%% Consistent tangent operator for vertices
dlam = dlamv;
Pv   = -1/3*(Sxxv_t+Syyv_t+Szzv_t); 
Txxv = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
J2v  = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
txx = Pv+Sxxv_t; tyy = Pv+Syyv_t; txy = Txyv_t; tzz = Pv+Szzv_t; J2 = J2v;
% dQds vertices
dQdsxx = vm * txx .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
dQdsyy = vm * tyy .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
dQdsxy = 0.1000000000e1 * vm * txy .* (vm * J2) .^ (-0.1e1 / 0.2e1);
dQdszz = vm * tzz .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_psi / 0.3e1;
% dFds vertices
dFdsxx = vm * txx .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_phi / 0.3e1;
dFdsyy = vm * tyy .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_phi / 0.3e1;
dFdsxy = 0.1000000000e1 * vm * txy .* (vm * J2) .^ (-0.1e1 / 0.2e1);
dFdszz = vm * tzz .* (vm * J2) .^ (-0.1e1 / 0.2e1) / 0.2e1 + sin_phi / 0.3e1;
% Reduced elastic stiffness matrix
if consistent==1
    d2Qdsxxdsxx = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 - vm .^ 2 .* txx .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsxxdsyy = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tyy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsxxdsxy = -vm .^ 2 .* txx .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsxxdszz = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsyydsxx = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tyy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsyydsyy = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 - vm .^ 2 .* tyy .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsyydsxy = -vm .^ 2 .* tyy .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsyydszz = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* tyy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdsxydsxx = -vm .^ 2 .* txx .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsxydsyy = -vm .^ 2 .* tyy .* txy .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdsxydsxy = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) - vm .^ 2 .* txy .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1);
    d2Qdsxydszz = -vm .^ 2 .* txy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdszzdsxx = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* txx .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdszzdsyy = -vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.6e1 - vm .^ 2 .* tyy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    d2Qdszzdsxy = -vm .^ 2 .* txy .* tzz .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.2e1;
    d2Qdszzdszz = vm .* (vm .* J2) .^ (-0.1e1 ./ 0.2e1) ./ 0.3e1 - vm .^ 2 .* tzz .^ 2 .* (vm .* J2) .^ (-0.3e1 ./ 0.2e1) ./ 0.4e1;
    if Newton==1, dlam = 0*dlam; end
    D11 = De11v; D12 = De12v; D13 = De13v; D14 = De14v;
    D21 = De21v; D22 = De22v; D23 = De23v; D24 = De24v;
    D31 = De31v; D32 = De32v; D33 = De33v; D34 = De34v;
    D41 = De41v; D42 = De42v; D43 = De43v; D44 = De44v;
    M11 = 1 + dlam .* (D11 .* d2Qdsxxdsxx + D12 .* d2Qdsyydsxx + D14 .* d2Qdszzdsxx);
    M12 = dlam .* (D11 .* d2Qdsxxdsyy + D12 .* d2Qdsyydsyy + D14 .* d2Qdszzdsyy);
    M13 = dlam .* (D11 .* d2Qdsxxdsxy + D12 .* d2Qdsyydsxy + D14 .* d2Qdszzdsxy);
    M14 = dlam .* (D11 .* d2Qdsxxdszz + D12 .* d2Qdsyydszz + D14 .* d2Qdszzdszz);
    M21 = dlam .* (D21 .* d2Qdsxxdsxx + D22 .* d2Qdsyydsxx + D24 .* d2Qdszzdsxx);
    M22 = 1 + dlam .* (D21 .* d2Qdsxxdsyy + D22 .* d2Qdsyydsyy + D24 .* d2Qdszzdsyy);
    M23 = dlam .* (D21 .* d2Qdsxxdsxy + D22 .* d2Qdsyydsxy + D24 .* d2Qdszzdsxy);
    M24 = dlam .* (D21 .* d2Qdsxxdszz + D22 .* d2Qdsyydszz + D24 .* d2Qdszzdszz);
    M31 = dlam .* D33 .* d2Qdsxydsxx;
    M32 = dlam .* D33 .* d2Qdsxydsyy;
    M33 = 1 + dlam .* D33 .* d2Qdsxydsxy;
    M34 = dlam .* D33 .* d2Qdsxydszz;
    M41 = dlam .* (D41 .* d2Qdsxxdsxx + D42 .* d2Qdsyydsxx + D44 .* d2Qdszzdsxx);
    M42 = dlam .* (D41 .* d2Qdsxxdsyy + D42 .* d2Qdsyydsyy + D44 .* d2Qdszzdsyy);
    M43 = dlam .* (D41 .* d2Qdsxxdsxy + D42 .* d2Qdsyydsxy + D44 .* d2Qdszzdsxy);
    M44 = 1 + dlam .* (D41 .* d2Qdsxxdszz + D42 .* d2Qdsyydszz + D44 .* d2Qdszzdszz);
    [ Mi11,Mi12,Mi13,Mi14, Mi21,Mi22,Mi23,Mi24, Mi31,Mi32,Mi33,Mi34, Mi41,Mi42,Mi43,Mi44  ] = M2Di_EP5_inverse_4x4( M11,M12,M13,M14, M21,M22,M23,M24, M31,M32,M33,M34, M41,M42,M43,M44  );
    den      = (dFdsxx .* Mi11 + dFdsyy .* Mi21 + dFdsxy .* Mi31 + dFdszz .* Mi41) .* (D11 .* dQdsxx + D12 .* dQdsyy + D14 .* dQdszz) + (dFdsxx .* Mi12 + dFdsyy .* Mi22 + dFdsxy .* Mi32 + dFdszz .* Mi42) .* (D21 .* dQdsxx + D22 .* dQdsyy + D24 .* dQdszz) + (dFdsxx .* Mi13 + dFdsyy .* Mi23 + dFdsxy .* Mi33 + dFdszz .* Mi43) .* D33 .* dQdsxy + (dFdsxx .* Mi14 + dFdsyy .* Mi24 + dFdsxy .* Mi34 + dFdszz .* Mi44) .* (D41 .* dQdsxx + D42 .* dQdsyy + D44 .* dQdszz);
    den      = den + vp.*eta_vp/dt + H;
    Dep31v = Mi31 .* (D11 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D12 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D14 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) + Mi32 .* (D21 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D22 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D24 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - Mi33 .* D33 ./ den .* (dQdsxy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) + Mi34 .* (D41 .* (1 - 1 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsxx .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsxx .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsxx .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41))) - D42 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdsyy .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdsyy .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdsyy .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)) - D44 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D11 + Mi12 .* D21 + Mi14 .* D41) + dQdszz .* dFdsyy .* (Mi21 .* D11 + Mi22 .* D21 + Mi24 .* D41) + dQdszz .* dFdsxy .* (Mi31 .* D11 + Mi32 .* D21 + Mi34 .* D41) + dQdszz .* dFdszz .* (Mi41 .* D11 + Mi42 .* D21 + Mi44 .* D41)));
    Dep32v = Mi31 .* (-D11 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D12 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D14 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) + Mi32 .* (-D21 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D22 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D24 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - Mi33 .* D33 ./ den .* (dQdsxy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + Mi34 .* (-D41 ./ den .* (dQdsxx .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsxx .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsxx .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsxx .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)) + D42 .* (1 - 1 ./ den .* (dQdsyy .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdsyy .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdsyy .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdsyy .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42))) - D44 ./ den .* (dQdszz .* dFdsxx .* (Mi11 .* D12 + Mi12 .* D22 + Mi14 .* D42) + dQdszz .* dFdsyy .* (Mi21 .* D12 + Mi22 .* D22 + Mi24 .* D42) + dQdszz .* dFdsxy .* (Mi31 .* D12 + Mi32 .* D22 + Mi34 .* D42) + dQdszz .* dFdszz .* (Mi41 .* D12 + Mi42 .* D22 + Mi44 .* D42)));
    Dep33v = Mi31 .* (-D11 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D12 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D14 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33)) + Mi32 .* (-D21 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D22 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D24 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33)) + Mi33 .* D33 .* (1 - 1 ./ den .* (dQdsxy .* dFdsxx .* Mi13 .* D33 + dQdsxy .* dFdsyy .* Mi23 .* D33 + dQdsxy .* dFdsxy .* Mi33 .* D33 + dQdsxy .* dFdszz .* Mi43 .* D33)) + Mi34 .* (-D41 ./ den .* (dQdsxx .* dFdsxx .* Mi13 .* D33 + dQdsxx .* dFdsyy .* Mi23 .* D33 + dQdsxx .* dFdsxy .* Mi33 .* D33 + dQdsxx .* dFdszz .* Mi43 .* D33) - D42 ./ den .* (dQdsyy .* dFdsxx .* Mi13 .* D33 + dQdsyy .* dFdsyy .* Mi23 .* D33 + dQdsyy .* dFdsxy .* Mi33 .* D33 + dQdsyy .* dFdszz .* Mi43 .* D33) - D44 ./ den .* (dQdszz .* dFdsxx .* Mi13 .* D33 + dQdszz .* dFdsyy .* Mi23 .* D33 + dQdszz .* dFdsxy .* Mi33 .* D33 + dQdszz .* dFdszz .* Mi43 .* D33));
end

if precond == 1
    Pv     =-1/3*(Sxxv_t + Syyv_t + Szzv_t);
    Txxv   = Pv + Sxxv_t; Tyyv = Pv + Syyv_t; Tzzv = Pv + Szzv_t; Txyv = Txyv_t;
    J2_t   = 1/2*(Txxv.^2 + Tyyv.^2 + Tzzv.^2) + Txyv.^2;
    Dep31v = 0*Dep31v;
    Dep32v = 0*Dep32v;
    Dep33v = Gv .* (1 - dlam .* vm .* Gv .* (vm .* J2_t) .^ (-0.1e1 ./ 0.2e1));
end

%% Elastic or elasto-plastic operators
D11c = plc.*Dep11c + (1-plc).* De11c;
D12c = plc.*Dep12c + (1-plc).* De12c;
D13c = plc.*Dep13c + (1-plc).* De13c;
D21c = plc.*Dep21c + (1-plc).* De21c;
D22c = plc.*Dep22c + (1-plc).* De22c;
D23c = plc.*Dep23c + (1-plc).* De23c;
D31v = plv.*Dep31v + (1-plv).* De31v;
D32v = plv.*Dep32v + (1-plv).* De32v;
D33v = plv.*Dep33v + (1-plv).* De33v;
D11c = (1-plc2).*D11c + 0*plc2;
D12c = (1-plc2).*D12c;
D13c = (1-plc2).*D13c;
D21c = (1-plc2).*D21c + 0*plc2;
D22c = (1-plc2).*D22c;
D23c = (1-plc2).*D23c;
D31v = (1-plv2).*D31v;
D32v = (1-plv2).*D32v;
D33v = (1-plv2).*D33v;

% Stencil weights for normal strains interpolation
wS_SW = wSW(:,1:end-1);
wS_SE = wSE(:,1:end-1);
wS_W  = wNW(:,1:end-1);
wS_E  = wNE(:,1:end-1);
wN_W  = wSW(:,2:end-0);
wN_E  = wSE(:,2:end-0);
wN_NW = wNW(:,2:end-0);
wN_NE = wNE(:,2:end-0);
% Elastic or elasto-plastic operators West-East
D11W    = zeros(size(Ux));  D11W( 2:end-1, : ) = D11c(1:end-1,:);
D11E    = zeros(size(Ux));  D11E( 2:end-1, : ) = D11c(2:end,:);
D12W    = zeros(size(Ux));  D12W( 2:end-1, : ) = D12c(1:end-1,:);
D12E    = zeros(size(Ux));  D12E( 2:end-1, : ) = D12c(2:end,:);
D13W    = zeros(size(Ux));  D13W( 2:end-1, : ) = D13c(1:end-1,:);
D13E    = zeros(size(Ux));  D13E( 2:end-1, : ) = D13c(2:end,:);
% Elastic or elasto-plastic operators South-North
D31S    = zeros(size(Ux));  D31S( 2:end-1 , :  )     = D31v(2:end-1, 1:end-1);  %D31S(:,1) = 0;
D31N    = zeros(size(Ux));  D31N( 2:end-1 , :  )     = D31v(2:end-1, 2:end  );  %D31N(:,end) = 0;
D32S    = zeros(size(Ux));  D32S( 2:end-1 , :  )     = D32v(2:end-1, 1:end-1);  %D32S(:,1) = 0;
D32N    = zeros(size(Ux));  D32N( 2:end-1 , :  )     = D32v(2:end-1, 2:end  );  %D32N(:,end) = 0;
D33S    = zeros(size(Ux));  D33S( 2:end-1 , :  )     = D33v(2:end-1, 1:end-1);  %D33S(:,1) = 0;
D33N    = zeros(size(Ux));  D33N( 2:end-1 , :  )     = D33v(2:end-1, 2:end  );  %D33N(:,end) = 0;
% Finite difference x-momentum balance coefficients
c1UxC = [-0.1e1 ./ dx .* (-D11E ./ dx + D13E .* (((1 - fsxN) .* (-1 - nsxN) ./ dy) ./ 0.4e1 + ((1 - fsxS) .* (1 + nsxS) ./ dy) ./ 0.4e1) - D11W ./ dx - D13W .* (((1 - fsxN) .* (-1 - nsxN) ./ dy) ./ 0.4e1 + ((1 - fsxS) .* (1 + nsxS) ./ dy) ./ 0.4e1)) - 0.1e1 ./ dy .* (D31N .* (-wN_E ./ dx ./ 0.4e1 + wN_W ./ dx ./ 0.4e1) + (D33N .* (1 - fsxN) .* (-1 - nsxN) ./ dy) - D31S .* (-wS_E ./ dx ./ 0.4e1 + wS_W ./ dx ./ 0.4e1) - (D33S .* (1 - fsxS) .* (1 + nsxS) ./ dy))];
c1UxW = [-0.1e1 ./ dx .* (D11W ./ dx - D13W .* (((1 - fsxN) .* (-1 - nsxN) ./ dy) ./ 0.4e1 + ((1 - fsxS) .* (1 + nsxS) ./ dy) ./ 0.4e1)) - 0.1e1 ./ dy .* (-D31N .* wN_W ./ dx ./ 0.4e1 + D31S .* wS_W ./ dx ./ 0.4e1)];
c1UxE = [-0.1e1 ./ dx .* (D11E ./ dx + D13E .* (((1 - fsxN) .* (-1 - nsxN) ./ dy) ./ 0.4e1 + ((1 - fsxS) .* (1 + nsxS) ./ dy) ./ 0.4e1)) - 0.1e1 ./ dy .* (D31N .* wN_E ./ dx ./ 0.4e1 - D31S .* wS_E ./ dx ./ 0.4e1)];
c1UxS = [-0.1e1 ./ dx .* ((D13E .* (1 - fsxS) .* (-1 + nsxS) ./ dy) ./ 0.4e1 - (D13W .* (1 - fsxS) .* (-1 + nsxS) ./ dy) ./ 0.4e1) - 0.1e1 ./ dy .* (-D31S .* (wS_SW ./ dx ./ 0.4e1 - wS_SE ./ dx ./ 0.4e1) - (D33S .* (1 - fsxS) .* (-1 + nsxS) ./ dy))];
c1UxN = [-0.1e1 ./ dx .* ((D13E .* (1 - fsxN) .* (1 - nsxN) ./ dy) ./ 0.4e1 - (D13W .* (1 - fsxN) .* (1 - nsxN) ./ dy) ./ 0.4e1) - 0.1e1 ./ dy .* (D31N .* (wN_NW .* (1 - frxN) ./ dx ./ 0.4e1 - wN_NE .* (1 - frxN) ./ dx ./ 0.4e1) + (D33N .* (1 - fsxN) .* (1 - nsxN) ./ dy))];
c1UySW = [-0.1e1 ./ dx .* (-D13E ./ dx ./ 0.4e1 + D12W ./ dy - D13W .* (-0.1e1 ./ dx ./ 0.4e1 + (1 - fsxW) .* (1 + nsxW) ./ dx ./ 0.4e1)) - 0.1e1 ./ dy .* (-D32N .* wN_W ./ dy ./ 0.4e1 - D32S .* (-wS_W ./ dy ./ 0.4e1 + wS_SW ./ dy ./ 0.4e1) + D33S ./ dx)];
c1UySE = [-0.1e1 ./ dx .* (-D12E ./ dy + D13E .* (0.1e1 ./ dx ./ 0.4e1 + (1 - fsxE) .* (-1 - nsxE) ./ dx ./ 0.4e1) - D13W ./ dx ./ 0.4e1) - 0.1e1 ./ dy .* (-D32N .* wN_E ./ dy ./ 0.4e1 - D32S .* (-wS_E ./ dy ./ 0.4e1 + wS_SE ./ dy ./ 0.4e1) - D33S ./ dx)];
c1UyNW = [-0.1e1 ./ dx .* (-D13E ./ dx ./ 0.4e1 - D12W ./ dy - D13W .* (-0.1e1 ./ dx ./ 0.4e1 + (1 - fsxW) .* (1 + nsxW) ./ dx ./ 0.4e1)) - 0.1e1 ./ dy .* (D32N .* (wN_W ./ dy ./ 0.4e1 - wN_NW .* (1 - frxN) ./ dy ./ 0.4e1) - D33N ./ dx - D32S .* wS_W ./ dy ./ 0.4e1)];
c1UyNE = [-0.1e1 ./ dx .* (D12E ./ dy + D13E .* (0.1e1 ./ dx ./ 0.4e1 + (1 - fsxE) .* (-1 - nsxE) ./ dx ./ 0.4e1) - D13W ./ dx ./ 0.4e1) - 0.1e1 ./ dy .* (D32N .* (wN_E ./ dy ./ 0.4e1 - wN_NE .* (1 - frxN) ./ dy ./ 0.4e1) + D33N ./ dx - D32S .* wS_E ./ dy ./ 0.4e1)];
c1UxSW = cancel*[(1 ./ dx .* D13W .* (1 - fsxS) .* (-1 + nsxS) ./ dy) ./ 0.4e1 - (1 ./ dy .* D31S .* wS_SW ./ dx) ./ 0.4e1];
c1UxSE = cancel*[-(1 ./ dx .* D13E .* (1 - fsxS) .* (-1 + nsxS) ./ dy) ./ 0.4e1 + (1 ./ dy .* D31S .* wS_SE ./ dx) ./ 0.4e1];
c1UxNW = cancel*[(1 ./ dx .* D13W .* (1 - fsxN) .* (1 - nsxN) ./ dy) ./ 0.4e1 + (1 ./ dy .* D31N .* wN_NW .* (1 - frxN) ./ dx) ./ 0.4e1];
c1UxNE = cancel*[-(1 ./ dx .* D13E .* (1 - fsxN) .* (1 - nsxN) ./ dy) ./ 0.4e1 - (1 ./ dy .* D31N .* wN_NE .* (1 - frxN) ./ dx) ./ 0.4e1];
c1UySSW = cancel*[-0.1e1 ./ dy .^ 2 .* D32S .* wS_SW ./ 0.4e1];
c1UySSE = cancel*[-0.1e1 ./ dy .^ 2 .* D32S .* wS_SE ./ 0.4e1];
c1UySWW = cancel*[(1 ./ dx .^ 2 .* D13W .* (1 - fsxW) .* (-1 + nsxW)) ./ 0.4e1];
c1UySEE = cancel*[-(1 ./ dx .^ 2 .* D13E .* (1 - fsxE) .* (1 - nsxE)) ./ 0.4e1];
c1UyNWW = cancel*[(1 ./ dx .^ 2 .* D13W .* (1 - fsxW) .* (-1 + nsxW)) ./ 0.4e1];
c1UyNEE = cancel*[-(1 ./ dx .^ 2 .* D13E .* (1 - fsxE) .* (1 - nsxE)) ./ 0.4e1];
c1UyNNW = cancel*[-(1 ./ dy .^ 2 .* D32N .* wN_NW .* (1 - frxN)) ./ 0.4e1];
c1UyNNE = cancel*[-(1 ./ dy .^ 2 .* D32N .* wN_NE .* (1 - frxN)) ./ 0.4e1];

if symmetry==1
    % Contribution from normal stresses (x-momentum)
    BcK(NumUx( 2   , : ))  = BcK(NumUx(2,:))      - c1UxW( 2    , : )'.*BC.Ux_W;
    BcK(NumUx(end-1, : ))  = BcK(NumUx(end-1,:))  - c1UxE( end-1, : )'.*BC.Ux_E;
    % Contribution from shear stresses (x-momentum)
    BcK(NumUx(2:end-1,end))      = BcK(NumUx(2:end-1,end))  -  c1UyNW (2:end-1,end).*BC.Uy_N(1:end-1)   ; % UyNW
    BcK(NumUx(2:end-1,end))      = BcK(NumUx(2:end-1,end))  -  c1UyNE (2:end-1,end).*BC.Uy_N(2:end  )   ; % UyNE
    BcK(NumUx(2:end-1, 1 ))      = BcK(NumUx(2:end-1, 1 ))  -  c1UySW (2:end-1,1  ).*BC.Uy_S(1:end-1)   ; % UySW
    BcK(NumUx(2:end-1, 1 ))      = BcK(NumUx(2:end-1, 1 ))  -  c1UySE (2:end-1,1  ).*BC.Uy_S(2:end  )   ; % UySE
end

if viscous==1, BcK(NumUx( 2:end-1   , : )) = BcK(NumUx( 2:end-1   , : )) + diff(VEc.*Sxxc0,1,1)/dx + diff(VEv(2:end-1,:).*Txyv0(2:end-1,:),1,2)/dy; end

% Non conforming Dirichlets BCs
nsxN               = nsxN.*0;
nsxS               = nsxS.*0;
c1UxN1 = [-0.1e1 ./ dx .* ((D13E .* (1 - fsxN) .* (1 - nsxN) ./ dy) ./ 0.4e1 - (D13W .* (1 - fsxN) .* (1 - nsxN) ./ dy) ./ 0.4e1) - 0.1e1 ./ dy .* (D31N .* (wN_NW ./ dx ./ 0.4e1 - wN_NE ./ dx ./ 0.4e1) + (D33N .* (1 - fsxN) .* (1 - nsxN) ./ dy))];
c1UxS1 = [-0.1e1 ./ dx .* ((D13E .* (1 - fsxS) .* (-1 + nsxS) ./ dy) ./ 0.4e1 - (D13W .* (1 - fsxS) .* (-1 + nsxS) ./ dy) ./ 0.4e1) - 0.1e1 ./ dy .* (-D31S .* (wS_SW ./ dx ./ 0.4e1 - wS_SE ./ dx ./ 0.4e1) - (D33S .* (1 - fsxS) .* (-1 + nsxS) ./ dy))];
if sum(BC.nsxN(:))>0, BcK(NumUx(:,end)) = BcK(NumUx(:,end)) - 2*c1UxN1(NumUx(:,end)).*BC.Ux_N; end
if sum(BC.nsxS(:))>0, BcK(NumUx(:,  1)) = BcK(NumUx(:,  1)) - 2*c1UxS1(NumUx(:,  1)).*BC.Ux_S; end

% Set boundary conditions
sc1Ux = max(c1UxC(:));
c1UxC  ([1 end],:) = sc1Ux;
c1UxW  ([1 end],:) = 0;
c1UxE  ([1 end],:) = 0;
c1UxS  ([1 end],:) = 0; c1UxS ( :   , 1 ) = 0;
c1UxN  ([1 end],:) = 0; c1UxN ( :   ,end) = 0;
c1UxSW ([1 end],:) = 0; c1UxSW( :   , 1 ) = 0;
c1UxSE ([1 end],:) = 0; c1UxSE( :   , 1 ) = 0;
c1UxNW ([1 end],:) = 0; c1UxNW( :   ,end) = 0;
c1UxNE ([1 end],:) = 0; c1UxNE( :   ,end) = 0;
c1UySW ([1 end],:) = 0;
c1UySE ([1 end],:) = 0;
c1UyNW ([1 end],:) = 0;
c1UyNE ([1 end],:) = 0;
c1UySWW([1 end],:) = 0; c1UySWW([1 2],:) = 0;
c1UySEE([1 end],:) = 0; c1UySEE([end end-1],:) = 0;
c1UyNWW([1 end],:) = 0; c1UyNWW([1 2],:) = 0;
c1UyNEE([1 end],:) = 0; c1UyNEE([end end-1 ],:) = 0;
c1UySSW([1 end],:) = 0; c1UySSW(:,1  ) = 0;
c1UySSE([1 end],:) = 0; c1UySSE(:,1  ) = 0;
c1UyNNW([1 end],:) = 0; c1UyNNW(:,end) = 0;
c1UyNNE([1 end],:) = 0; c1UyNNE(:,end) = 0;
% Symmetry
if symmetry == 1
    c1UxW  (    2,:) = 0;
    c1UxE  (end-1,:) = 0;
    c1UySW (  :,  1) = 0;
    c1UySE (  :,  1) = 0;
    c1UyNW (  :,end) = 0;
    c1UyNE (  :,end) = 0;
end

iUx     = NumUx(:);
iUxW    = ones(size(Ux));   iUxW(2:end-1, :     ) =  NumUx(1:end-2, :     );
iUxE    = ones(size(Ux));   iUxE(2:end-1, :     ) =  NumUx(3:end  , :     );
iUxS    = ones(size(Ux));   iUxS(2:end-1,2:end  ) =  NumUx(2:end-1,1:end-1);
iUxN    = ones(size(Ux));   iUxN(2:end-1,1:end-1) =  NumUx(2:end-1,2:end  );
iUxSW   = ones(size(Ux));  iUxSW(2:end-1,2:end  ) =  NumUx(1:end-2,1:end-1);
iUxSE   = ones(size(Ux));  iUxSE(2:end-1,2:end  ) =  NumUx(3:end-0,1:end-1);
iUxNW   = ones(size(Ux));  iUxNW(2:end-1,1:end-1) =  NumUx(1:end-2,2:end  );
iUxNE   = ones(size(Ux));  iUxNE(2:end-1,1:end-1) =  NumUx(3:end-0,2:end  );
iUySW   = ones(size(Ux));  iUySW(2:end-1, :     ) = NumUyG(1:end-1,1:end-1);
iUySE   = ones(size(Ux));  iUySE(2:end-1, :     ) = NumUyG(2:end-0,1:end-1);
iUyNW   = ones(size(Ux));  iUyNW(2:end-1, :     ) = NumUyG(1:end-1,2:end  );
iUyNE   = ones(size(Ux));  iUyNE(2:end-1, :     ) = NumUyG(2:end-0,2:end  );
iUySWW  = ones(size(Ux)); iUySWW(3:end-1, :     ) = NumUyG(1:end-2,1:end-1);
iUySEE  = ones(size(Ux)); iUySEE(2:end-2, :     ) = NumUyG(3:end  ,1:end-1);
iUyNWW  = ones(size(Ux)); iUyNWW(3:end-1, :     ) = NumUyG(1:end-2,2:end  );
iUyNEE  = ones(size(Ux)); iUyNEE(2:end-2, :     ) = NumUyG(3:end  ,2:end  );
iUySSW  = ones(size(Ux)); iUySSW(2:end-1, 2:end ) = NumUyG(1:end-1,1:end-2);
iUySSE  = ones(size(Ux)); iUySSE(2:end-1, 2:end ) = NumUyG(2:end-0,1:end-2);
iUyNNW  = ones(size(Ux)); iUyNNW(2:end-1 ,1:end-1) = NumUyG(1:end-1,3:end  );
iUyNNE  = ones(size(Ux)); iUyNNE(2:end-1,1:end-1) = NumUyG(2:end-0,3:end  );
IuuJ    = [   iUx(:);   iUx(:);   iUx(:);   iUx(:);   iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:) ]';
JuuJ    = [   iUx(:);  iUxW(:);  iUxS(:);  iUxE(:);  iUxN(:);  iUxSW(:);  iUxSE(:);  iUxNW(:);  iUxNE(:);  iUySW(:);  iUySE(:);  iUyNW(:);  iUyNE(:);  iUySWW(:);  iUySEE(:);  iUyNWW(:);  iUyNEE(:);  iUySSW(:);  iUySSE(:);  iUyNNW(:);  iUyNNE(:) ]';
VuuJ    = [ c1UxC(:); c1UxW(:); c1UxS(:); c1UxE(:); c1UxN(:); c1UxSW(:); c1UxSE(:); c1UxNW(:); c1UxNE(:); c1UySW(:); c1UySE(:); c1UyNW(:); c1UyNE(:); c1UySWW(:); c1UySEE(:); c1UyNWW(:); c1UyNEE(:); c1UySSW(:); c1UySSE(:); c1UyNNW(:); c1UyNNE(:) ]';

%---------------------------------------------
if free_surf == 1
    % Elastic or elasto-plastic operators South-North
    D21S    = zeros(size(Uy));  D21S( : ,2:end-0) = D21c(:,1:end-0);% D21S(:,1) = 0;
    D21N    = zeros(size(Uy));  D21N( : ,2:end-1) = D21c(:,2:end-0); %D21N(:,end) = 0;
    D22S    = zeros(size(Uy));  D22S( : ,2:end-0) = D22c(:,1:end-0);% D22S(:,1) = 0;
    D22N    = zeros(size(Uy));  D22N( : ,2:end-1) = D22c(:,2:end-0); %D21N(:,1) = 0;
    D23S    = zeros(size(Uy));  D23S( : ,2:end-0) = D23c(:,1:end-0);% D23S(:,1) = 0;
    D23N    = zeros(size(Uy));  D23N( : ,2:end-1) = D23c(:,2:end-0);% D23N(:,end) = 0;
    % Elastic or elasto-plastic operators West-East
    D31W    = zeros(size(Uy));  D31W( : ,2:end-1 ) = D31v(1:end-1, 2:end-1); %D31W(1,:) = 0;
    D31E    = zeros(size(Uy));  D31E( : ,2:end-1 ) = D31v(2:end  , 2:end-1); %D31E(end,:) = 0;
    D32W    = zeros(size(Uy));  D32W( : ,2:end-1 ) = D32v(1:end-1, 2:end-1); %D32W(1,:) = 0;
    D32E    = zeros(size(Uy));  D32E( : ,2:end-1 ) = D32v(2:end  , 2:end-1); %D32E(end,:) = 0;
    D33W    = zeros(size(Uy));  D33W( : ,2:end-1 ) = D33v(1:end-1, 2:end-1); %D33W(1,:) = 0;
    D33E    = zeros(size(Uy));  D33E( : ,2:end-1 ) = D33v(2:end  , 2:end-1); %D33E(end,:) = 0;
end

if free_surf == 0
    % Elastic or elasto-plastic operators South-North
    D21S    = zeros(size(Uy));  D21S( : ,2:end-1) = D21c(:,1:end-1);% D21S(:,1) = 0;
    D21N    = zeros(size(Uy));  D21N( : ,2:end-1) = D21c(:,2:end-0); %D21N(:,end) = 0;
    D22S    = zeros(size(Uy));  D22S( : ,2:end-1) = D22c(:,1:end-1);% D22S(:,1) = 0;
    D22N    = zeros(size(Uy));  D22N( : ,2:end-1) = D22c(:,2:end-0); %D21N(:,1) = 0;
    D23S    = zeros(size(Uy));  D23S( : ,2:end-1) = D23c(:,1:end-1);% D23S(:,1) = 0;
    D23N    = zeros(size(Uy));  D23N( : ,2:end-1) = D23c(:,2:end-0);% D23N(:,end) = 0;
    % Elastic or elasto-plastic operators West-East
    D31W    = zeros(size(Uy));  D31W( : ,2:end-1 ) = D31v(1:end-1, 2:end-1); %D31W(1,:) = 0;
    D31E    = zeros(size(Uy));  D31E( : ,2:end-1 ) = D31v(2:end  , 2:end-1); %D31E(end,:) = 0;
    D32W    = zeros(size(Uy));  D32W( : ,2:end-1 ) = D32v(1:end-1, 2:end-1); %D32W(1,:) = 0;
    D32E    = zeros(size(Uy));  D32E( : ,2:end-1 ) = D32v(2:end  , 2:end-1); %D32E(end,:) = 0;
    D33W    = zeros(size(Uy));  D33W( : ,2:end-1 ) = D33v(1:end-1, 2:end-1); %D33W(1,:) = 0;
    D33E    = zeros(size(Uy));  D33E( : ,2:end-1 ) = D33v(2:end  , 2:end-1); %D33E(end,:) = 0;
end

% Stencil weights for normal strains interpolation
wW_SW = wSW(1:end-1,:);
wW_NW = wNW(1:end-1,:);
wW_S  = wSE(1:end-1,:);
wW_N  = wNW(1:end-1,:);
wE_S  = wSW(2:end-0,:);
wE_N  = wSW(2:end-0,:);
wE_SE = wSE(2:end-0,:);
wE_NE = wNE(2:end-0,:);
% Finite difference y-momentum balance coefficients
c2UyC = [-0.1e1 ./ dy .* (-(D22N .* (1 - fryN) ./ dy) + D23N .* (((1 - fsyW) .* (1 + nsyW) ./ dx) ./ 0.4e1 + ((1 - fsyE) .* (-1 - nsyE) ./ dx) ./ 0.4e1) - (D22S ./ dy) - D23S .* (((1 - fsyW) .* (1 + nsyW) ./ dx) ./ 0.4e1 + ((1 - fsyE) .* (-1 - nsyE) ./ dx) ./ 0.4e1)) - 0.1e1 ./ dx .* (D32E .* ((wE_S ./ dy) ./ 0.4e1 - (wE_N .* (1 - fryN) ./ dy) ./ 0.4e1) + (D33E .* (1 - fsyE) .* (-1 - nsyE) ./ dx) - D32W .* ((wW_S ./ dy) ./ 0.4e1 - (wW_N .* (1 - fryN) ./ dy) ./ 0.4e1) - (D33W .* (1 - fsyW) .* (1 + nsyW) ./ dx))];
c2UyW = [-0.1e1 ./ dy .* ((D23N .* (1 - fsyW) .* (-1 + nsyW) ./ dx) ./ 0.4e1 - (D23S .* (1 - fsyW) .* (-1 + nsyW) ./ dx) ./ 0.4e1) - 0.1e1 ./ dx .* (-D32W .* (-wW_NW .* (1 - fryN) ./ dy ./ 0.4e1 + wW_SW ./ dy ./ 0.4e1) - (D33W .* (1 - fsyW) .* (-1 + nsyW) ./ dx))];
c2UyE = [-0.1e1 ./ dy .* ((D23N .* (1 - fsyE) .* (1 - nsyE) ./ dx) ./ 0.4e1 - (D23S .* (1 - fsyE) .* (1 - nsyE) ./ dx) ./ 0.4e1) - 0.1e1 ./ dx .* (D32E .* (-wE_NE .* (1 - fryN) ./ dy ./ 0.4e1 + wE_SE ./ dy ./ 0.4e1) + (D33E .* (1 - fsyE) .* (1 - nsyE) ./ dx))];
c2UyS = [-0.1e1 ./ dy .* (D22S ./ dy - D23S .* (((1 - fsyW) .* (1 + nsyW) ./ dx) ./ 0.4e1 + ((1 - fsyE) .* (-1 - nsyE) ./ dx) ./ 0.4e1)) - 0.1e1 ./ dx .* (-D32E .* wE_S ./ dy ./ 0.4e1 + D32W .* wW_S ./ dy ./ 0.4e1)];
c2UyN = [-0.1e1 ./ dy .* ((D22N .* (1 - fryN) ./ dy) + D23N .* (((1 - fsyW) .* (1 + nsyW) ./ dx) ./ 0.4e1 + ((1 - fsyE) .* (-1 - nsyE) ./ dx) ./ 0.4e1)) - 0.1e1 ./ dx .* ((D32E .* wE_N .* (1 - fryN) ./ dy) ./ 0.4e1 - (D32W .* wW_N .* (1 - fryN) ./ dy) ./ 0.4e1)];
c2UxSW = [-0.1e1 ./ dy .* (-D23N ./ dy ./ 0.4e1 + D21S ./ dx - D23S .* (-0.1e1 ./ dy ./ 0.4e1 + (1 - fsyS) .* (1 + nsyS) ./ dy ./ 0.4e1)) - 0.1e1 ./ dx .* (-D31E .* wE_S ./ dx ./ 0.4e1 - D31W .* (-wW_S ./ dx ./ 0.4e1 + wW_SW ./ dx ./ 0.4e1) + D33W ./ dy)];
c2UxSE = [-0.1e1 ./ dy .* (-D23N ./ dy ./ 0.4e1 - D21S ./ dx - D23S .* (-0.1e1 ./ dy ./ 0.4e1 + (1 - fsyS) .* (1 + nsyS) ./ dy ./ 0.4e1)) - 0.1e1 ./ dx .* (D31E .* (wE_S ./ dx ./ 0.4e1 - wE_SE ./ dx ./ 0.4e1) - D33E ./ dy - D31W .* wW_S ./ dx ./ 0.4e1)];
c2UxNW = [-0.1e1 ./ dy .* (-(D21N .* (1 - fryN) ./ dx) + D23N .* (0.1e1 ./ dy ./ 0.4e1 + (1 - fsyN) .* (-1 - nsyN) ./ dy ./ 0.4e1) - D23S ./ dy ./ 0.4e1) - 0.1e1 ./ dx .* (-(D31E .* wE_N .* (1 - fryN) ./ dx) ./ 0.4e1 - D31W .* (-(wW_N .* (1 - fryN) ./ dx) ./ 0.4e1 + (wW_NW .* (1 - fryN) ./ dx) ./ 0.4e1) - D33W ./ dy)];
c2UxNE = [-0.1e1 ./ dy .* ((D21N .* (1 - fryN) ./ dx) + D23N .* (0.1e1 ./ dy ./ 0.4e1 + (1 - fsyN) .* (-1 - nsyN) ./ dy ./ 0.4e1) - D23S ./ dy ./ 0.4e1) - 0.1e1 ./ dx .* (D31E .* ((wE_N .* (1 - fryN) ./ dx) ./ 0.4e1 - (wE_NE .* (1 - fryN) ./ dx) ./ 0.4e1) + D33E ./ dy - (D31W .* wW_N .* (1 - fryN) ./ dx) ./ 0.4e1)];
c2UySW = cancel*[(1 ./ dy .* D23S .* (1 - fsyW) .* (-1 + nsyW) ./ dx) ./ 0.4e1 - (1 ./ dx .* D32W .* wW_SW ./ dy) ./ 0.4e1];
c2UySE = cancel*[(1 ./ dy .* D23S .* (1 - fsyE) .* (1 - nsyE) ./ dx) ./ 0.4e1 + (1 ./ dx .* D32E .* wE_SE ./ dy) ./ 0.4e1];
c2UyNW = cancel*[-(1 ./ dy .* D23N .* (1 - fsyW) .* (-1 + nsyW) ./ dx) ./ 0.4e1 + (1 ./ dx .* D32W .* wW_NW .* (1 - fryN) ./ dy) ./ 0.4e1];
c2UyNE = cancel*[-(1 ./ dy .* D23N .* (1 - fsyE) .* (1 - nsyE) ./ dx) ./ 0.4e1 - (1 ./ dx .* D32E .* wE_NE .* (1 - fryN) ./ dy) ./ 0.4e1];
c2UxSSW = cancel*[(1 ./ dy .^ 2 .* D23S .* (1 - fsyS) .* (-1 + nsyS)) ./ 0.4e1];
c2UxSSE = cancel*[(1 ./ dy .^ 2 .* D23S .* (1 - fsyS) .* (-1 + nsyS)) ./ 0.4e1];
c2UxSWW = cancel*[-0.1e1 ./ dx .^ 2 .* D31W .* wW_SW ./ 0.4e1];
c2UxSEE = cancel*[-0.1e1 ./ dx .^ 2 .* D31E .* wE_SE ./ 0.4e1];
c2UxNWW = cancel*[-(1 ./ dx .^ 2 .* D31W .* wW_NW .* (1 - fryN)) ./ 0.4e1];
c2UxNEE = cancel*[-(1 ./ dx .^ 2 .* D31E .* wE_NE .* (1 - fryN)) ./ 0.4e1];
c2UxNNW = cancel*[-(1 ./ dy .^ 2 .* D23N .* (1 - fsyN) .* (1 - nsyN)) ./ 0.4e1];
c2UxNNE = cancel*[-(1 ./ dy .^ 2 .* D23N .* (1 - fsyN) .* (1 - nsyN)) ./ 0.4e1];

% Gravity
BcK(NumUyG( : , 2:end-1 )) = BcK(NumUyG( : , 2:end-1 )) + rho0*gy;
if viscous==1, BcK(NumUyG( : , 2:end-1 )) = BcK(NumUyG( : , 2:end-1 )) + diff(VEc.*Syyc0,1,2)/dy + diff(VEv(:,2:end-1).*Txyv0(:,2:end-1),1,1)/dx; end

if symmetry == 1
    % Contribution from normal stresses
    BcK(NumUyG( : , 2   )) = BcK(NumUyG(:,2))     - c2UyS( : , 2   ).*BC.Uy_S;
    BcK(NumUyG( : ,end-1)) = BcK(NumUyG(:,end-1)) - c2UyN( : ,end-1).*BC.Uy_N;
    % Contribution from shear stresses (y-momentum)
    BcK(NumUyG(end,2:end-1))     = BcK(NumUyG(end,2:end-1)) - (c2UxSE (end,2:end-1).*BC.Ux_E(1:end-1)')'; % UxSE
    BcK(NumUyG(end,2:end-1))     = BcK(NumUyG(end,2:end-1)) - (c2UxNE (end,2:end-1).*BC.Ux_E(2:end  )')'; % UxNE
    BcK(NumUyG( 1 ,2:end-1))     = BcK(NumUyG( 1 ,2:end-1)) - (c2UxSW (1  ,2:end-1).*BC.Ux_W(1:end-1)')'; % UxSW
    BcK(NumUyG( 1 ,2:end-1))     = BcK(NumUyG( 1 ,2:end-1)) - (c2UxNW (1  ,2:end-1).*BC.Ux_W(2:end  )')'; % UxNW
end

% Non conforming Dirichlets BCs
nsyW               = 0.*nsyW;
nsyE               = 0.*nsyE;
c2UyW1 = [-0.1e1 ./ dy .* ((D23N .* (1 - fsyW) .* (-1 + nsyW) ./ dx) ./ 0.4e1 - (D23S .* (1 - fsyW) .* (-1 + nsyW) ./ dx) ./ 0.4e1) - 0.1e1 ./ dx .* (-D32W .* (-wW_NW ./ dy ./ 0.4e1 + wW_SW ./ dy ./ 0.4e1) - (D33W .* (1 - fsyW) .* (-1 + nsyW) ./ dx))];
c2UyE1 = [-0.1e1 ./ dy .* ((D23N .* (1 - fsyE) .* (1 - nsyE) ./ dx) ./ 0.4e1 - (D23S .* (1 - fsyE) .* (1 - nsyE) ./ dx) ./ 0.4e1) - 0.1e1 ./ dx .* (D32E .* (-wE_NE ./ dy ./ 0.4e1 + wE_SE ./ dy ./ 0.4e1) + (D33E .* (1 - fsyE) .* (1 - nsyE) ./ dx))];
if sum(BC.nsyE(:))>0, BcK(NumUyG(end, :)) = BcK(NumUyG(end, :)) - 2*c2UyE1(NumUy(end, :))'.*BC.Uy_E; end
if sum(BC.nsyW(:))>0, BcK(NumUyG(  1, :)) = BcK(NumUyG(  1, :)) - 2*c2UyW1(NumUy(  1, :))'.*BC.Uy_W; end

if free_surf == 0
    % Set boundary conditions and symmetrise
    sc2Uy = max(c2UyC(:));
    c2UyC  (:,[1 end]) = sc2Uy;
    c2UyW  (:,[1 end]) = 0; c2UyW  (1,  :) = 0;
    c2UyE  (:,[1 end]) = 0; c2UyE  (end,:) = 0;
    c2UyS  (:,[1 end]) = 0;
    c2UyN  (:,[1 end]) = 0;
    c2UySW (:,[1 end]) = 0; c2UySW(1  , :) = 0;
    c2UySE (:,[1 end]) = 0; c2UySE(end, :) = 0;
    c2UyNW (:,[1 end]) = 0; c2UyNW(1  , :) = 0;
    c2UyNE (:,[1 end]) = 0; c2UyNE(end, :) = 0;
    c2UxSW (:,[1 end]) = 0;
    c2UxSE (:,[1 end]) = 0;
    c2UxNW (:,[1 end]) = 0;
    c2UxNE (:,[1 end]) = 0;
    c2UxSSW(:,[1 end]) = 0; c2UxSSW(:,[1 2]   ) = 0;
    c2UxSSE(:,[1 end]) = 0; c2UxSSE(:,[1 2]   ) = 0;
    c2UxNNW(:,[1 end]) = 0; c2UxNNW(:,[end end-1]) = 0;
    c2UxNNE(:,[1 end]) = 0; c2UxNNE(:,[end end-1]) = 0;
    c2UxSWW(:,[1 end]) = 0; c2UxSWW(1  ,:)   = 0;
    c2UxNWW(:,[1 end]) = 0; c2UxNWW(1  ,:)   = 0;
    c2UxSEE(:,[1 end]) = 0; c2UxSEE(end,:)   = 0;
    c2UxNEE(:,[1 end]) = 0; c2UxNEE(end,:)   = 0;
end

if free_surf == 1
    % Set boundary conditions and symmetrise
    sc2Uy = max(c2UyC(:));
    c2UyC  (:,[1    ]) = sc2Uy;
    c2UyW  (:,[1    ]) = 0; c2UyW  (1,  :) = 0;
    c2UyE  (:,[1    ]) = 0; c2UyE  (end,:) = 0;
    c2UyS  (:,[1    ]) = 0;
    c2UyN  (:,[1 end]) = 0;
    c2UySW (:,[1    ]) = 0; c2UySW(1  , :) = 0;
    c2UySE (:,[1    ]) = 0; c2UySE(end, :) = 0;
    c2UyNW (:,[1 end]) = 0; c2UyNW(1  , :) = 0;
    c2UyNE (:,[1 end]) = 0; c2UyNE(end, :) = 0;
    c2UxSW (:,[1    ]) = 0;
    c2UxSE (:,[1    ]) = 0;
    c2UxNW (:,[1 end]) = 0;
    c2UxNE (:,[1 end]) = 0;
    c2UxSSW(:,[1    ]) = 0; c2UxSSW(:,[1 2]   ) = 0;
    c2UxSSE(:,[1    ]) = 0; c2UxSSE(:,[1 2]   ) = 0;
    c2UxNNW(:,[1 end]) = 0; c2UxNNW(:,[end]) = 0;
    c2UxNNE(:,[1 end]) = 0; c2UxNNE(:,[end ]) = 0;
    c2UxSWW(:,[1    ]) = 0; c2UxSWW(1  ,:)   = 0;
    c2UxNWW(:,[1 end]) = 0; c2UxNWW(1  ,:)   = 0;
    c2UxSEE(:,[1    ]) = 0; c2UxSEE(end,:)   = 0;
    c2UxNEE(:,[1 end]) = 0; c2UxNEE(end,:)   = 0;
end

% Symmetry
if symmetry == 1
    c2UyS( : , 2   ) = 0;
    c2UyN( : ,end-1) = 0;
    c2UxSW (1  ,  :) = 0;
    c2UxSE (end,  :) = 0;
    c2UxNW (1  ,  :) = 0;
    c2UxNE (end,  :) = 0;
end

% Equation indexes
iUy     = NumUyG(:);
iUyW    = ones(size(Uy));   iUyW(2:end  ,2:end-1) = NumUyG(1:end-1,2:end-1);
iUyE    = ones(size(Uy));   iUyE(1:end-1,2:end-1) = NumUyG(2:end  ,2:end-1);
iUyS    = ones(size(Uy));   iUyS( :     ,2:end-1) = NumUyG( :     ,1:end-2);
iUyN    = ones(size(Uy));   iUyN( :     ,2:end-1) = NumUyG( :     ,3:end  );
iUySW   = ones(size(Uy));  iUySW(2:end  ,2:end-1) = NumUyG(1:end-1,1:end-2);
iUySE   = ones(size(Uy));  iUySE(1:end-1,2:end-1) = NumUyG(2:end  ,1:end-2);
iUyNW   = ones(size(Uy));  iUyNW(2:end  ,2:end-1) = NumUyG(1:end-1,3:end-0);
iUyNE   = ones(size(Uy));  iUyNE(1:end-1,2:end-1) = NumUyG(2:end  ,3:end-0);
iUxSW   = ones(size(Uy));  iUxSW( :     ,2:end-1) = NumUx (1:end-1,1:end-1);
iUxNW   = ones(size(Uy));  iUxNW( :     ,2:end-1) = NumUx (1:end-1,2:end-0);
iUxSE   = ones(size(Uy));  iUxSE( :     ,2:end-1) = NumUx (2:end  ,1:end-1);
iUxNE   = ones(size(Uy));  iUxNE( :     ,2:end-1) = NumUx (2:end  ,2:end-0);
iUxSSW  = ones(size(Uy)); iUxSSW( :     ,3:end-1) = NumUx (1:end-1,1:end-2);
iUxSSE  = ones(size(Uy)); iUxSSE( :     ,3:end-1) = NumUx (2:end  ,1:end-2);
iUxNNW  = ones(size(Uy)); iUxNNW( :     ,2:end-2) = NumUx (1:end-1,3:end  );
iUxNNE  = ones(size(Uy)); iUxNNE( :     ,2:end-2) = NumUx (2:end  ,3:end  );
iUxSWW  = ones(size(Uy)); iUxSWW(2:end  ,2:end-1) = NumUx (1:end-2,1:end-1);
iUxNWW  = ones(size(Uy)); iUxNWW(2:end  ,2:end-1) = NumUx (1:end-2,2:end-0);
iUxSEE  = ones(size(Uy)); iUxSEE(1:end-1,2:end-1) = NumUx (3:end  ,1:end-1);
iUxNEE  = ones(size(Uy)); iUxNEE(1:end-1,2:end-1) = NumUx (3:end  ,2:end-0);

% Triplets
IvvJ    = [   iUy(:);   iUy(:);   iUy(:);   iUy(:);   iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:) ]';
JvvJ    = [   iUy(:);  iUyW(:);  iUyS(:);  iUyE(:);  iUyN(:);  iUySW(:);  iUySE(:);  iUyNW(:);  iUyNE(:);  iUxSW(:);  iUxSE(:);  iUxNW(:);  iUxNE(:);  iUxSSW(:);  iUxSSE(:);  iUxNNW(:);  iUxNNE(:);  iUxSWW(:);  iUxNWW(:);  iUxSEE(:);  iUxNEE(:) ]';
VvvJ    = [ c2UyC(:); c2UyW(:); c2UyS(:); c2UyE(:); c2UyN(:); c2UySW(:); c2UySE(:); c2UyNW(:); c2UyNE(:); c2UxSW(:); c2UxSE(:); c2UxNW(:); c2UxNE(:); c2UxSSW(:); c2UxSSE(:); c2UxNNW(:); c2UxNNE(:); c2UxSWW(:); c2UxNWW(:); c2UxSEE(:); c2UxNEE(:) ]';

Kep  = sparse ([IuuJ(:); IvvJ(:);], [JuuJ(:); JvvJ(:);], [VuuJ(:); VvvJ(:);], (nx)*ncy+(ny)*ncx, (nx)*ncy+(ny)*ncx );
if free_surf == 0
    % Indices
    ibcUxW = NumUx( 1     ,:)';      ibcUxE = NumUx(end    , : )';
    ibcUyS = NumUyG(:,1);            ibcUyN = NumUyG(:,end); % global Numbering for Uy !
    ibc    = [ ibcUxW; ibcUxE; ibcUyS; ibcUyN ];
    
    % Conforming Dirichlets
    BcK(ibc) = [BC.Ux_W*sc1Ux; BC.Ux_E*sc1Ux; BC.Uy_S*sc2Uy; BC.Uy_N*sc2Uy];
end

if free_surf == 1
    % Indices
    ibcUxW = NumUx( 1     ,:)';      ibcUxE = NumUx(end    , : )';
    ibcUyS = NumUyG(:,1);            ibcUyN = NumUyG(:,end); % global Numbering for Uy !
    ibc    = [ ibcUxW; ibcUxE; ibcUyS ];
    
    % Conforming Dirichlets
    BcK(ibc) = [BC.Ux_W*sc1Ux; BC.Ux_E*sc1Ux; BC.Uy_S*sc2Uy];
end

end