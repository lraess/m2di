
————————————————————————————————————————
 M2Di_EP README | 25.10.2018. M2Di_EP G3
————————————————————————————————————————

M2Di with Elasto-Visco-Plastic rheology using a compressible 
displacement-based formulation consistent tangent linearisation.  

Copyright (C) 2018 Thibault Duretz, Alban Souche, René de Borst, Laetitia
Le Pourhiet

This file is part of M2Di. Please refer to XXXXX for details about
the implementation.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

================================================================================
Distributed software, files in this directory:
================================================================================

M2Di_EP12_G3		M2Di with Elasto-Visco-Plastic rheology using a
			compressible displacement-based formulation
			consistent tangent linearisation.

M2Di_EP12_notebook.ipynb  Jupyter notebook containing the derivation
			  of the consistent tangent linearisation.

================================================================================

Contact: thibault.duretz@univ-rennes1.fr
