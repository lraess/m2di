# M2Di2_VEVP_Compressible

## Visco-Elasto-Viscoplastic code with compressible/dilatant elasto-plasticity, non-linear hardening/softening and power-law viscoplasticity

This folder contains 4 subfolders:

### "BENCHMARKS"

Contains scripts that can reproduce previously published models:

- V-E-P shear banding tests "Test 1" and "Test 4" of Duretz et al. (2018)

- E-VP shear banding test "Model 1" of Duretz et al. (2019)

- Crustal-scale V-E-VP shear banding reference model of Duretz et al. (2020)

### "MODELS"

Contains scripts that were used to produce the results presented in the paper:

- Systematic testing of the role of incompressibility in subfolder "Compressible".

- Systematic testing of the magnitude of bulk modulus and dilatancy in subfolder "BulkModulusDilatancy".

- Systematic testing of the role of power-law viscoplasticity in "PowerLawViscoPlasticity"

- Systematic testing of the different implementation of cohesion, friction and dilation strain softening (implicit/explicit) in "Softening"

ACHTUNG: These scripts will require that both the [ROMA colorbar](https://www.fabiocrameri.ch/colourmaps/) and the [SuiteSparse MATLAB routines](https://people.engr.tamu.edu/davis/suitesparse.html) are accessible from the workspace.

### "NONLINEARITIES"

These are extra models based on "Test 1" of Duretz et al. (2018) in which non-linear cohesion, friction, dilation strain softening and power-law viscoplasticity were integrated.


### "_M2Di2_functions"

Contains some important M2Di2 routines - essential to run simulations.