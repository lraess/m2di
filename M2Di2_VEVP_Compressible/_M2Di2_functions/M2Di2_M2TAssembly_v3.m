function M2T = M2Di2_M2TAssembly_v3( BC, D, dx, dy, nx, ny, ck, N2, NumVx, NumVyG, NumTe, Vx, Vy, symmetry, SuiteSparse )
% Boundary condition flags [W E S N]
fsxW = BC.fsxW; fsxE = BC.fsxE;
fsxS = BC.fsxS; fsxN = BC.fsxN;
nsxW = BC.nsxW; nsxE = BC.nsxE;
nsxS = BC.nsxS; nsxN = BC.nsxN;
% Boundary condition flags [W E S N]
fsyW = BC.fsyW; fsyE = BC.fsyE;
fsyS = BC.fsyS; fsyN = BC.fsyN;
nsyW = BC.nsyW; nsyE = BC.nsyE;
nsyS = BC.nsyS; nsyN = BC.nsyN;
% Neumanns/Dirichlets
NeuW = BC.NeuW; NeuE = BC.NeuE; Dirx = BC.Dirx;
NeuS = BC.NeuS; NeuN = BC.NeuN; Diry = BC.Diry;
% Stencil weights for normal strains interpolation
wSW   = ones(nx+1,ny+1); wSW(end,:) = 2; wSW(:,end) = 2; wSW(end,end) = 4; wSW(1  ,:) = 0; wSW(:,  1) = 0;
wSE   = ones(nx+1,ny+1); wSE(  1,:) = 2; wSE(:,end) = 2; wSE(  1,end) = 4; wSE(end,:) = 0; wSE(:,  1) = 0;
wNW   = ones(nx+1,ny+1); wNW(end,:) = 2; wNW(:,  1) = 2; wNW(end,  1) = 4; wNW(1  ,:) = 0; wNW(:,end) = 0;
wNE   = ones(nx+1,ny+1); wNE(  1,:) = 2; wNE(:,  1) = 2; wNE(  1,  1) = 4; wNE(end,:) = 0; wNE(:,end) = 0;
%% ------------------------ M2T x ------------------------ %
% Stencil weights for normal strains interpolation
wS_SW = wSW(:,1:end-1); wS_SE = wSE(:,1:end-1); wS_W  = wNW(:,1:end-1); wS_E  = wNE(:,1:end-1);
wN_W  = wSW(:,2:end-0); wN_E  = wSE(:,2:end-0); wN_NW = wNW(:,2:end-0); wN_NE = wNE(:,2:end-0);
% Elastic or elasto-plastic operators West-East
D15W    = zeros(size(Vx));  D15W( 2:end-0, : ) = D.D14c(1:end-0,:);
D15E    = zeros(size(Vx));  D15E( 1:end-1, : ) = D.D14c(1:end-0,:);
% Elastic or elasto-plastic operators South-North
D35S    = zeros(size(Vx));  D35S( 1:end-0 , :) = D.D34v(1:end-0, 1:end-1);  %D31S(:,1) = 0;
D35N    = zeros(size(Vx));  D35N( 1:end-0 , :) = D.D34v(1:end-0, 2:end  );  %D31N(:,end) = 0;
% Coefficicients

% c1TSW =  ((1 - fsxS) .* D35S .* wS_SW ./ dy) ./ 0.5e1;
% c1TSE =  ((1 - fsxS) .* D35S .* wS_SE ./ dy) ./ 0.5e1;
% c1TW =  D15W ./ dx - (((1 - fsxN) .* D35N .* wN_W) ./ 0.5e1 - ((1 - fsxS) .* D35S .* wS_W) ./ 0.5e1) ./ dy;
% c1TE = -D15E ./ dx - (((1 - fsxN) .* D35N .* wN_E) ./ 0.5e1 - ((1 - fsxS) .* D35S .* wS_E) ./ 0.5e1) ./ dy;
% c1TNW = -((1 - fsxN) .* D35N .* wN_NW ./ dy) ./ 0.5e1;
% c1TNE = -((1 - fsxN) .* D35N .* wN_NE ./ dy) ./ 0.4e1;

Dirx  = BC.Dirx;
Neu   = (NeuE==1 | NeuW==1)*symmetry;
half  = 1/2*(Neu+2*(1-Neu));
c1TW  = half.*(Dirx - 1).*((1/4*D35N.*wN_W - 1/4*D35S.*wS_W)./dy + (-NeuE.*(D15W - 1) + (D15W - 1).*(NeuW - 1))./dx);
c1TE  = half.*(Dirx - 1).*((1/4*D35N.*wN_E - 1/4*D35S.*wS_E)./dy + (NeuW.*(D15E - 1) + (1 - NeuE).*(D15E - 1))./dx);
c1TSW = -1/4*D35S.*half.*wS_SW.*(Dirx - 1)./dy;
c1TSE = -1/4*D35S.*half.*wS_SE.*(Dirx - 1)./dy;
c1TNW =  1/4*D35N.*half.*wN_NW.*(Dirx - 1)./dy;
c1TNE =  1/4*D35N.*half.*wN_NE.*(Dirx - 1)./dy;


c1TSW (Dirx == 1) = 0;
c1TSE (Dirx == 1) = 0;
c1TW  (Dirx == 1) = 0; %c1UxS ( :   , 1 ) = 0;
c1TE  (Dirx == 1) = 0; %c1UxN ( :   ,end) = 0;
c1TNW (Dirx == 1) = 0; %c1UxSW( :   , 1 ) = 0;
c1TNE (Dirx == 1) = 0; %c1UxSE( :   , 1 ) = 0;

iVx   = NumVx(:);
iTW   = ones(size(Vx)); iTW(2:end  ,:) = NumTe;
iTE   = ones(size(Vx)); iTE(1:end-1,:) = NumTe;
iTSW  = ones(size(Vx)); iTSW(2:end  , 2:end  ) = NumTe(:,1:end-1);
iTSE  = ones(size(Vx)); iTSE(1:end-1, 2:end  ) = NumTe(:,1:end-1);
iTNW  = ones(size(Vx)); iTNW(2:end  , 1:end-1) = NumTe(:,2:end);
iTNE  = ones(size(Vx)); iTNE(1:end-1, 1:end-1) = NumTe(:,2:end);
IuTJ  = [   iVx(:);   iVx(:);   iVx(:);   iVx(:);   iVx(:);    iVx(:) ];
JuTJ  = [   iTSW(:);  iTSE(:);  iTW(:);   iTE(:);   iTNW(:);   iTNE(:)];
VuTJ  = [  c1TSW(:); c1TSE(:); c1TW(:);  c1TE(:);  c1TNW(:);  c1TNE(:)];
%% ------------------------ M2T y ------------------------ %
% Stencil weights for normal strains interpolation
wW_SW = wSW(1:end-1,:); wW_NW = wNW(1:end-1,:); wW_S  = wSE(1:end-1,:); wW_N  = wNE(1:end-1,:);  %% BUG FIX ON 30/04/20
wE_S  = wSW(2:end-0,:); wE_N  = wNW(2:end-0,:); wE_SE = wSE(2:end-0,:); wE_NE = wNE(2:end-0,:);  %% BUG FIX ON 30/04/20
% Elastic or elasto-plastic operators South-North
D25S    = zeros(size(Vy));  D25S( : ,2:end-0) = D.D24c(:,1:end-0);% D21S(:,1) = 0;
D25N    = zeros(size(Vy));  D25N( : ,1:end-1) = D.D24c(:,1:end-0); %D21N(:,end) = 0;
% Elastic or elasto-plastic operators West-East
D35W    = zeros(size(Vy));  D35W( : ,1:end-0 ) = D.D34v(1:end-1, 1:end-0); %D31W(1,:) = 0;
D35E    = zeros(size(Vy));  D35E( : ,1:end-0 ) = D.D34v(2:end  , 1:end-0); %D31E(end,:) = 0;
% Coefficicients
% c2TSW = ((1 - fsyW) .* D35W .* wW_SW ./ dx) ./ 0.4e1;
% c2TS = D25S ./ dy - (((1 - fsyE) .* D35E .* wE_S) ./ 0.4e1 - ((1 - fsyW) .* D35W .* wW_S) ./ 0.4e1) ./ dx;
% c2TSE = -((1 - fsyE) .* D35E .* wE_SE ./ dx) ./ 0.4e1;
% c2TNW = ((1 - fsyW) .* D35W .* wW_NW ./ dx) ./ 0.4e1;
% c2TN = -D25N ./ dy - (((1 - fsyE) .* D35E .* wE_N) ./ 0.4e1 - ((1 - fsyW) .* D35W .* wW_N) ./ 0.4e1) ./ dx;
% c2TNE = -((1 - fsyE) .* D35E .* wE_NE ./ dx) ./ 0.4e1;

% max(abs(D25S(:)))
% max(abs(D35W(:)))
% max(abs(D35E(:)))

Diry  = BC.Diry;
Neu   = (NeuS==1 | NeuN==1)*symmetry;
half  = 1/2*(Neu+2*(1-Neu));
c2TS  = half.*(Diry - 1).*((-NeuN.*(D25S - 1) + (D25S - 1).*(NeuS - 1))./dy + (1/4*D35E.*wE_S - 1/4*D35W.*wW_S)./dx);
c2TN  = half.*(Diry - 1).*((NeuS.*(D25N - 1) + (1 - NeuN).*(D25N - 1))./dy + (1/4*D35E.*wE_N - 1/4*D35W.*wW_N)./dx);
c2TSW = -1/4*D35W.*half.*wW_SW.*(Diry - 1)./dx;
c2TSE = 1/4*D35E.*half.*wE_SE.*(Diry - 1)./dx;
c2TNW = -1/4*D35W.*half.*wW_NW.*(Diry - 1)./dx;
c2TNE = 1/4*D35E.*half.*wE_NE.*(Diry - 1)./dx;

c2TSW (Diry == 1) = 0;
c2TSE (Diry == 1) = 0;
c2TS  (Diry == 1) = 0; %c1UxS ( :   , 1 ) = 0;
c2TN  (Diry == 1) = 0; %c1UxN ( :   ,end) = 0;
c2TNW (Diry == 1) = 0; %c1UxSW( :   , 1 ) = 0;
c2TNE (Diry == 1) = 0; %c1UxSE( :   , 1 ) = 0;
% Equation indexes
iVy   = NumVyG(:);
iTSW  = ones(size(Vy)); iTSW(2:end  ,2:end  ) = NumTe(1:end-1,: );
iTSE  = ones(size(Vy)); iTSE(1:end-1,2:end  ) = NumTe(2:end,: );
iTS   = ones(size(Vy)); iTS ( :     ,2:end  ) = NumTe;
iTN   = ones(size(Vy)); iTN ( :     ,1:end-1) = NumTe;
iTNW  = ones(size(Vy)); iTNW(2:end  ,1:end-1) = NumTe(1:end-1,: );
iTNE  = ones(size(Vy)); iTNE(1:end-1,1:end-1) = NumTe(2:end,: );
%% Assembly of the sparse matrix
IvTJ  =  [   iVy(:);   iVy(:);   iVy(:);   iVy(:);   iVy(:);     iVy(:)];
JvTJ  =  [  iTSW(:);  iTSE(:);   iTS(:);   iTN(:);  iTNW(:);    iTNE(:)];
VvTJ  =  [ c2TSW(:); c2TSE(:);  c2TS(:);  c2TN(:); c2TNW(:);   c2TNE(:)];
if SuiteSparse == 1, M2T   = sparse2( [IuTJ(:); IvTJ(:)], [JuTJ(:); JvTJ(:)], [VuTJ(:); VvTJ(:)]);
else                 M2T   = sparse ( [IuTJ(:); IvTJ(:)], [JuTJ(:); JvTJ(:)], [VuTJ(:); VvTJ(:)]);
end
end