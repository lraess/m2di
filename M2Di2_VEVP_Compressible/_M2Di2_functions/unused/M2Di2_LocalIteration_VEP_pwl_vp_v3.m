function [ eta_0, Txx, Tyy, Txy, detadExx, detadEyy, detadExy, pl, eta_Kel, Eii_el, Eii_pwl, Eii_pl, Overstress ] = M2Di2_LocalIteration_VEP_pwl_vp_v3( litmax, tol, noisy, Exx, Eyy, Exy, Txxo, Tyyo, Txyo, B_pwl, m_pwl, eta_el, P, phi, C, eta_vp, eta_Kel0, n_vp  )

Eii_el     = 0;
Eii_pwl    = 0;
Eii_pl     = 0;
Overstress = 0;

% Strain rate components and invariant
exx    = Exx;
eyy    = Eyy;
exy    = Exy;
eII    = sqrt(1/2*(exx.^2 + eyy.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
Exx    = Exx + Txxo./2./eta_el;
Eyy    = Eyy + Tyyo./2./eta_el;
Exy    = Exy + Txyo./2./eta_el;
Eii    = sqrt(1/2*(Exx.^2 + Eyy.^2) + Exy.^2);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*eII.^(1./n_pwl-1);

% Yield stress
yield = C + P.*sin(phi);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_0     = 0.5*(eta_up+eta_lo);

if noisy>2, fprintf('Starting Visco-Elastic local iterations:\n'); end

% Local iterations
for lit=1:litmax
    
    % Function evaluation at current effective viscosity
    Tii       = 2*eta_0.*Eii;
    Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
    Eii_vis_0 = Eii_pwl_0;
    r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
    
    % Residual check
    res = max(max(abs(r_eta_0)));
    if lit == 1; res0 = res; end
    if noisy>2, fprintf('It. %02d, r = %2.2e\n', lit, res/res0); end
    if res/res0 < tol
        break;
    end
    
    % Exact derivative
    drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_0;
    
    % Update viscosity
    eta_0  = eta_0 - r_eta_0 ./ drdeta;
    
end

% Update individual deviatoric stress components
Txx = 2*eta_0.*Exx;
Tyy = 2*eta_0.*Eyy;
Txy = 2*eta_0.*Exy;
% Tii    = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + Txy.^2);

% Plasticity
X      = eta_0./eta_el;
h_phi  = 0;
F      = Tii - yield;
pl     = F >= 0;
dQdtxx = Txx./2./Tii;
dQdtyy = Tyy./2./Tii;
dQdtxy = Txy./2./Tii;
gdot   = zeros(size(Txx));
eII            = sqrt(1/2*(exx.^2 + eyy.^2) + exy.^2);
eta_Kel        = zeros(size(Txx));
eta_Kel(pl==1) = eta_Kel0*(eII(pl==1)).^(1/n_vp-1);
gdot(pl==1)    = F(pl==1)./(eta_0(pl==1) + eta_Kel(pl==1) + h_phi);
% gdot(pl==1)    = 1e-3*eII(pl==1);
res0          = max(F(:));
Fc            = F;
pl            = F>0.0;
if noisy>2, fprintf('Starting ViscoPlastic local iterations - F trial = %2.2e:\n', res0); end
% Plastic strain rates
for lit=1:litmax
    % Evaluate F
    eta_Kel(pl==1) = eta_Kel0 * gdot(pl==1) .^ (1/n_vp-1);
    Fc(pl==1)      = Tii(pl==1) - eta_0(pl==1).*gdot(pl==1) - yield(pl==1) - eta_Kel(pl==1).*gdot(pl==1);
    % Residual check
    res = max(max(abs(Fc(pl))));
    Fc_pl = Fc(pl==1);
    if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e, max(F) = %2.2e\n', lit, res/res0, res, max(Fc_pl(:))); end
    if res/res0 < tol || res < tol
        break;
    end
    % Exact derivative
    dFdgdot = -eta_Kel0.*gdot.^(1/n_vp)./(gdot.*n_vp) - eta_0;
    % Update viscosity
    gdot(pl==1)  = gdot(pl==1) - Fc(pl==1) ./ dFdgdot(pl==1);
    gdot         = real(gdot); % MATLAB tends to spit out complex numbers
end
exx_pl = gdot.*dQdtxx;
eyy_pl = gdot.*dQdtyy;
exy_pl = gdot.*dQdtxy;
% dE2pl  = sqrt( 1/2*(exx_pl*dt).^2 + 1/2*(eyy_pl*dt).^2 + (exy_pl*dt).^2 );
Txx    = 2.*eta_0.*(exx-exx_pl) +  X.*Txxo;
Tyy    = 2.*eta_0.*(eyy-eyy_pl) +  X.*Tyyo;
Txy    = 2.*eta_0.*(exy-exy_pl) +  X.*Txyo;
Tii    = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + Txy.^2);
eta_0(pl==1)  = Tii(pl==1)./2./Eii(pl==1);

if noisy>2
    
    % Check yield 1 after correction
    F1     = Tii - yield - gdot.*eta_Kel;

    % Double check wether effective viscosity is computed accurately
    Txx = 2*eta_0.*Exx;
    Tyy = 2*eta_0.*Eyy;
    Txy = 2*eta_0.*Exy;
    Tii = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + Txy.^2);
    Tii(pl==1)    = 2*eta_0(pl==1).*Eii(pl==1);
    F2  = Tii - yield - gdot.*eta_Kel;
    
    % Correct power-law viscosity
    eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
    
    % 1: strain rate components
    exx_el  = (Txx-Txxo)./2./eta_el;
    exx_pwl = Txx./2./eta_pwl;
    exx_pl  = gdot.*dQdtxx;
    exx_net = norm(exx-exx_el-exx_pwl-exx_pl);
    
    eyy_el  = (Tyy-Tyyo)./2./eta_el;
    eyy_pwl = Tyy./2./eta_pwl;
    eyy_pl  = gdot.*dQdtyy;
    eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl);
    
    exy_el  = (Txy-Txyo)./2./eta_el;
    exy_pwl = Txy./2./eta_pwl;
    exx_pl  = gdot.*dQdtyy;
    exy_net = norm(exy-exy_el-exy_pwl-exy_pl);
    
    if noisy>2
        Overstress  = Tii - yield;
        fprintf('F trial = %2.2e --- F corr1 = %2.2e --- F corr2 = %2.2e --- Over S. = %2.2e\n', max(F(:)), max(F1(:)), max(F2(:)), max(Overstress(:)))
        fprintf('net exx = %2.2e --- net eyy = %2.2e --- net exy = %2.2e\n', norm(exx_net),norm(eyy_net),norm(exy_net))
    end
    
    % 2: invariants
    Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + exy_el .^2);
    Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + exy_pwl.^2);
    Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + exy_pl .^2);
    
end

% For analytical Jacobian: not used so far
detadExx = 0;%  (m_pwl.*(exx_pwl1)) .* eta_0.^2 ./ ( eta_pwl.*Eii_pwl2);
detadEyy = 0;%  (m_pwl.*(eyy-eyy_el)) .* eta_0.^2 ./ ( eta_pwl.*Eii_pwl2);
detadExy = 0;%2*(m_pwl.*(exy-exy_el)) .* eta_0.^2 ./ ( eta_pwl.*Eii_pwl2);

end