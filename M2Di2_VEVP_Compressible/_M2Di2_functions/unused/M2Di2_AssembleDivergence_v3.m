function [ div ] = M2Di2_AssembleDivergence_v3( BC, val, NumVx, NumVyG, NumPt, dx, dy, nx, ny, symmetry, SuiteSparse )
DirE = BC.Dirx(2:end-0,:);
DirW = BC.Dirx(1:end-1,:);
DirN = BC.Diry(:,2:end-0);
DirS = BC.Diry(:,1:end-1);
NeuE = BC.NeuE(2:end-0,:);
NeuW = BC.NeuW(1:end-1,:);
NeuN = BC.NeuN(:,2:end-0);
NeuS = BC.NeuS(:,1:end-1);
iVxW = NumVx(1:end-1,:);
iVxE = NumVx(2:end-0,:);
iVyS = NumVyG(:,1:end-1);
iVyN = NumVyG(:,2:end-0);
cVxW =-val/dx;
cVxE = val/dx;
cVyS =-val/dy;
cVyN = val/dy; 
cVxW(DirW==1) = 0; % Kill connections to Dirichlets
cVxE(DirE==1) = 0;
cVyS(DirS==1) = 0;
cVyN(DirN==1) = 0;
if symmetry==0,
    cVxW(NeuW==1) = 2*cVxW(NeuW==1); % factor 2 for stress BC
    cVxE(NeuE==1) = 2*cVxE(NeuE==1);
    cVyS(NeuS==1) = 2*cVyS(NeuS==1);
    cVyN(NeuN==1) = 2*cVyN(NeuN==1);
end
iPt   = NumPt;
I     = [  iPt(:);   iPt(:);  iPt(:);  iPt(:); ];
J     = [ iVxW(:);  iVxE(:); iVyS(:); iVyN(:); ];
V     = [ cVxW(:);  cVxE(:); cVyS(:); cVyN(:); ];
if SuiteSparse==1, div = sparse2(I,J,V, nx*ny,(nx+1)*ny + nx*(ny+1) );
else,              div =  sparse(I,J,V, nx*ny,(nx+1)*ny + nx*(ny+1) ); end
end

