function [ eta_vep, K_vep, Txx, Tyy, Tzz, Txy, Pc, divp, dive, deta_ve, deta_vep, ddivp, pl, eta_vp, Eii_el, Eii_pwl, Eii_pl, Overstress, Stuff, E2pl, C, phi, psi, gdot ] = M2Di2_LocalIteration_VEP_comp_v9_inv( litmax, tol, noisy, div, Exx2, Eyy2, Ezz2, Exy2, P, B_pwl, m_pwl, eta_el, K, P0, phi, psi, C, eta_vp, eta_vp0, n_vp, dt, E2pl0, Coh, C0, Phi, phi0, Psi, psi0  )

% For GRL setup, yield is: Ty = C + sin(phi)*P;

% Yield was modified to Ty = cos(phi)*C + sin(phi)*P;
% Softening function updated 

% This one works with power law VP and cohesion softening
% Initialise
Eii_el     = 0;
Eii_pwl    = 0;
Eii_pl     = 0;
Overstress = 0;
E2pl       = E2pl0;
hcoh       = 0;
dhcoh      = 0;
hphi       = 0;
dhphi      = 0;
K_vep      = K;
divp       = 0;
dive       = 0;
dHdgdot    = 0;
dpsidgdot  = 0;

% % Strain rate components and invariant
% exx    = Exx;
% eyy    = Eyy;
% ezz    = Ezz;
% exy    = Exy;
% eII    = sqrt(1/2*(exx.^2 + eyy.^2 + ezz.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
% Exx    = Exx + Txxo./2./eta_el;
% Eyy    = Eyy + Tyyo./2./eta_el;
% Ezz    = Ezz + Tzzo./2./eta_el;
% Exy    = Exy + Txyo./2./eta_el;
Div    = div - P0./K./dt;
% Pc     = -K.*dt.*Div;
Pc     = P;
Eii    = sqrt(1/2*(Exx2 + Eyy2 + Ezz2) + Exy2);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*Eii.^(1./n_pwl-1);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_ve     = 0.5*(eta_up+eta_lo);

if noisy>2, fprintf('Starting Visco-Elastic local iterations:\n'); end

% Local iterations
for lit=1:litmax
    
    % Function evaluation at current effective viscosity
    Tii       = 2*eta_ve.*Eii;
    Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
    Eii_vis_0 = Eii_pwl_0;
    r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
    
    % Residual check
    res = max(max(abs(r_eta_0)));
    if lit == 1; res0 = res; end
    if noisy>2, fprintf('It. %02d, r abs. = %2.2e r rel. = %2.2e\n', lit, res, res/res0); end
    if res/res0 < tol || res<tol
        break;
    end
    
    % Exact derivative
    drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_ve;
    
    % Update viscosity
    eta_ve  = eta_ve - r_eta_0 ./ drdeta;
    
end

% Update individual deviatoric stress components
Txx = 2*eta_ve.*0;
Tyy = 2*eta_ve.*0;
Tzz = 2*eta_ve.*0;
Txy = 2*eta_ve.*0;
Tii = 2*eta_ve.*Eii;
% For analytical Jacobian --- VE  --- ViscosityDerivativesTest.py
Jii = Tii.^2;
detadTxx = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Txx.*eta_ve.^2.*(n_pwl - 1);
detadTyy = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Tyy.*eta_ve.^2.*(n_pwl - 1);
detadTxy = -2.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Txy.*eta_ve.^2.*(n_pwl - 1);
detadTzz = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Tzz.*eta_ve.^2.*(n_pwl - 1);

deta_ve.dExx =zeros(size(Txx));% detadTxx .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dEyy = zeros(size(Txx));%detadTyy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dExy = zeros(size(Txx));%detadTxy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dEzz = zeros(size(Txx));%detadTzz .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dP   = zeros(size(Txx));
% Secure things
deta_ve.dExx(isinf(deta_ve.dExx)) = 0; deta_ve.dExx(isnan(deta_ve.dEyy)) = 0;
deta_ve.dEyy(isinf(deta_ve.dEyy)) = 0; deta_ve.dEyy(isnan(deta_ve.dEyy)) = 0;
deta_ve.dExy(isinf(deta_ve.dExy)) = 0; deta_ve.dExy(isnan(deta_ve.dExy)) = 0;
deta_ve.dEzz(isinf(deta_ve.dEzz)) = 0; deta_ve.dEzz(isnan(deta_ve.dEzz)) = 0;

% Plasticity

% CHECK for negative pressures
np  = P<0;
nnp = sum(np(:));
if nnp>0 && noisy>2, 
    fprintf('Actung baby: there are %06d nodes with negative pressure in the box!\n', nnp);
end

% Total stress 
Sxx = -P + Txx;
Syy = -P + Tyy;
Szz = -P + Tzz;
%P2  = -1/3*(Sxx+Syy+Szz);
Tyield         = C.*cos(phi) + P.*sin(phi);
% b = C.*cos(phi);
X              = eta_ve./eta_el;
F              = Tii - Tyield;
pl             = F >= 0;
dQdtxx         = Txx./2./Tii;
dQdtyy         = Tyy./2./Tii;
dQdtzz         = Tzz./2./Tii;
dQdtxy         = Txy./1./Tii;
% psi            = zeros(size(phi));
dQ             = sqrt(2/3)*sqrt( 1/2*(dQdtxx).^2 + 1/2*(dQdtyy).^2 + 1/2*(dQdtzz).^2 + (dQdtxy/2).^2 );
gdot           = zeros(size(Txx));
eta_vp         = zeros(size(Txx));
eta_vp(pl==1)  = eta_vp0*(Eii(pl==1)).^(1/n_vp-1);
gdot(pl==1)    = F(pl==1)./(eta_ve(pl==1) + eta_vp(pl==1) + K(pl==1).*dt.*sin(phi(pl==1)).*sin(psi(pl==1)) );
res0           = max(F(:));
Fc             = F;
pl             = F>0.0;
if noisy>2, fprintf('Starting ViscoPlastic local iterations - F trial = %2.2e:\n', res0); end
if noisy>3, figure(19), clf, hold on; end
if sum(pl(:)>0)
    % Plastic strain rates
    for lit=1:litmax
        % Evaluate F
        E2pl           = E2pl0 + dQ.*gdot.*dt;
        hcoh           = -Coh.Delta.*exp(-(E2pl - Coh.Ein).^2./Coh.dE.^2)./(sqrt(pi)*Coh.dE);
        hphi           = -Phi.Delta.*exp(-(E2pl - Phi.Ein).^2./Phi.dE.^2)./(sqrt(pi)*Phi.dE);
        hpsi           = -Psi.Delta.*exp(-(E2pl - Psi.Ein).^2./Psi.dE.^2)./(sqrt(pi)*Psi.dE);
        C              = C0   + Coh.soft_on.*hcoh.*gdot.*dt.*dQ;
        phi            = phi0 + Phi.soft_on.*hphi.*gdot.*dt.*dQ;
        psi            = psi0 + Psi.soft_on.*hpsi.*gdot.*dt.*dQ;
        divp           = gdot.*sin(psi);
        Pc             = P + K.*dt.*divp;
        eta_vp(pl==1)  = eta_vp0 .* gdot(pl==1) .^ (1/n_vp-1);
        Tyield         = C.*cos(phi) + Pc.*sin(phi) + eta_vp.*gdot;
        Fc(pl==1)      = Tii(pl==1) - eta_ve(pl==1).*gdot(pl==1) - Tyield(pl==1);
        % Residual check
        res = max(max(abs(Fc(pl))));
        Fc_pl = Fc(pl==1);
        if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e, max(F) = %2.2e\n', lit, res/res0, res, max(Fc_pl(:))); end
       
        if noisy>3,
            val = log10(res/res0);
            if isinf(val), val = -15; end
            plot(lit, val, 'o'); end
        if res/res0 < tol || res < tol
            break;
        end
        % Exact derivative
        dhcoh     = -2.0*Coh.Delta.*dt.*dQ.*(Coh.Ein - E2pl).*exp(-(Coh.Ein - E2pl).^2./Coh.dE.^2)./(sqrt(pi)*Coh.dE.^3);
        dhphi     = -2.0*Phi.Delta.*dt.*dQ.*(Phi.Ein - E2pl).*exp(-(Phi.Ein - E2pl).^2./Phi.dE.^2)./(sqrt(pi)*Phi.dE.^3);
        dhpsi     = -2.0*Psi.Delta.*dt.*dQ.*(Psi.Ein - E2pl).*exp(-(Psi.Ein - E2pl).^2./Psi.dE.^2)./(sqrt(pi)*Psi.dE.^3);
        dCdgdot   = dQ.*dt.*(dhcoh.*gdot + hcoh);
        dphidgdot = dQ.*dt.*(dhphi.*gdot + hphi);
        dpsidgdot = dQ.*dt.*(dhpsi.*gdot + hpsi);
       
        dHcoh    =   Coh.soft_on.*dCdgdot.*cos(phi);
        dHphi    = - Phi.soft_on.*dphidgdot.*(C.*sin(phi) - Pc.*cos(phi));
        dHpsi    =   Psi.soft_on.*K.*dpsidgdot.*dt.*gdot.*cos(psi).*sin(phi);
        dHdgdot  = dHcoh  + dHphi + dHpsi;
        dFdgdot  = - eta_ve - eta_vp./n_vp - K.*dt.*sin(psi).*sin(phi) - dHdgdot;
        % Update plastic multiplier rate
        gdot(pl==1)  = gdot(pl==1) - Fc(pl==1) ./ dFdgdot(pl==1);
        gdot         = real(abs(gdot)); % MATLAB tends to spit out complex numbers
    end
end

if noisy>3, drawnow; end

% Post-process
% exx_pl  = gdot.*dQdtxx;
% eyy_pl  = gdot.*dQdtyy;
% ezz_pl  = gdot.*dQdtzz;
% exy_pl  = gdot.*dQdtxy*(1/2);
% dE2pl   = sqrt( 1/2*(exx_pl*dt).^2 + 1/2*(eyy_pl*dt).^2 + 1/2*(ezz_pl*dt).^2 + (exy_pl*dt).^2 );
% Txx     = 2.*eta_ve.*(exx-exx_pl) +  X.*Txxo;
% Tyy     = 2.*eta_ve.*(eyy-eyy_pl) +  X.*Tyyo;
% Tzz     = 2.*eta_ve.*(ezz-ezz_pl) +  X.*Tzzo;
% Txy     = 2.*eta_ve.*(exy-exy_pl) +  X.*Txyo;
Tii     = Tii - eta_ve.*gdot;
eta_vep = eta_ve;
eta_vep(pl==1) = Tii(pl==1)./2./Eii(pl==1);
K_vep(pl==1)   = -Pc(pl==1)./(dt.*Div(pl==1));

% Total stress 
% P   = -K.*dt.*(Div-divp);
% Sxx = -P + Txx;
% Syy = -P + Tyy;
% Szz = -P + Tzz;
% P2  = -1/3*(Sxx+Syy+Szz);
% chk2 = norm(P-P2)
% chk3 = norm(Exx+Eyy+Ezz)
% chk4 = norm(-(Exx+Eyy)- Ezz)
% chk5 = norm(Tzz - 2*eta_vep.*(-(Exx+Eyy)))

% Checks
% if noisy>2
%     
% %     sum(pl(:))
% %     Pc2 = -K.*dt.*(Div-divp);
% %     Pc1 = -K_vep.*dt.*(Div);
% %     norm(Pc1-Pc)
% %     norm(Pc2-Pc)
%     
%     % Check yield 1 after correction
%     F1     = Tii - Tyield;
% 
%     % Double check wether effective viscosity is computed accurately
%     Txx = 2*eta_vep.*Exx;
%     Tyy = 2*eta_vep.*Eyy;
%     Tzz = 2*eta_vep.*Ezz;
%     Txy = 2*eta_vep.*Exy;
%     Tii = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*Tzz.^2 + Txy.^2);
%     Tii(pl==1)    = 2*eta_vep(pl==1).*Eii(pl==1);
%     F2  = Tii - Tyield;
%     
%     % Correct power-law viscosity
%     eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
%     
%     % 1: strain rate components
%     exx_el  = (Txx-Txxo)./2./eta_el;
%     exx_pwl = Txx./2./eta_pwl;
%     exx_pl  = gdot.*dQdtxx;
%     exx_net = norm(exx-exx_el-exx_pwl-exx_pl);
%     
%     eyy_el  = (Tyy-Tyyo)./2./eta_el;
%     eyy_pwl = Tyy./2./eta_pwl;
%     eyy_pl  = gdot.*dQdtyy;
%     eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl);
%     
%     ezz_el  = (Tzz-Tzzo)./2./eta_el;
%     ezz_pwl = Tzz./2./eta_pwl;
%     ezz_pl  = gdot.*dQdtzz;
%     ezz_net = norm(ezz-ezz_el-ezz_pwl-ezz_pl);
%     
%     exy_el  = (Txy-Txyo)./2./eta_el;
%     exy_pwl = Txy./2./eta_pwl;
%     exy_pl  = gdot.*dQdtxy*(1/2);
%     exy_net = norm(exy-exy_el-exy_pwl-exy_pl);
%     
%     dive  = -(P  - P0)./(K*dt);
%     div_net = div - dive - divp;  
% 
%     if sum(pl(:))>0, 
%         gdot_chk = zeros(size(F));
%         gdot_chk(pl==1) = F(pl==1) ./ (eta_ve(pl==1) + eta_vp(pl==1) + dt*hcoh(pl==1).*cos(phi(pl==1)) + K(pl==1).*dt.*sin(psi(pl==1)).*sin(phi(pl==1)));
%     end
%     
%     if noisy>2 && sum(pl(:))>0
%         Overstress  = Tii - Tyield;
%         fprintf('F trial = %2.2e --- F corr1 = %2.2e --- F corr2 = %2.2e --- Over S. = %2.2e\n', max(F(:)), max(F1(:)), max(F2(:)), max(Overstress(:)))
%         fprintf('net exx = %2.2e --- net eyy = %2.2e --- net ezz = %2.2e --- net exy = %2.2e\n', norm(exx_net),norm(eyy_net),norm(ezz_net),norm(exy_net))
%         fprintf('net ekk = %2.2e --- net gdot = %2.2e\n', norm(div_net), norm(gdot - gdot_chk)); 
%     end
%         
%     % 2: invariants
%     Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + 1/2*ezz_el .^2 + exy_el .^2);
%     Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + 1/2*ezz_pwl.^2 + exy_pwl.^2);
%     Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + 1/2*ezz_pl .^2 + exy_pl .^2);
%     
% end
% For analytical Jacobian --- VEP --- ViscosityDerivativesTest.py
% dFdE
dFdExx        = zeros(size(Txx));%  Exx.*eta_ve./Eii + 2.*Eii.*deta_ve.dExx;
dFdEyy        = zeros(size(Txx));%  Eyy.*eta_ve./Eii + 2.*Eii.*deta_ve.dEyy;
dFdEzz        = zeros(size(Txx));%  Ezz.*eta_ve./Eii + 2.*Eii.*deta_ve.dEzz;
dFdExy        = zeros(size(Txx));%2*Exy.*eta_ve./Eii + 2.*Eii.*deta_ve.dExy;  
dFdP          = -sin(phi);
% dgdotdE
g             =  1./ (eta_ve + eta_vp./n_vp + dHdgdot + K.*dt.*sin(psi).*sin(phi) );
dlamdExx      = g .* (dFdExx - gdot .*deta_ve.dExx);
dlamdEyy      = g .* (dFdEyy - gdot .*deta_ve.dEyy);
dlamdEzz      = g .* (dFdEzz - gdot .*deta_ve.dEzz);
dlamdExy      = g .* (dFdExy - gdot .*deta_ve.dExy);
dlamdP        = g .* (dFdP                        );
% deta_vp_dE
deta_vp_dExx  = eta_vp./gdot.*dlamdExx.*(1./n_vp-1);
deta_vp_dEyy  = eta_vp./gdot.*dlamdEyy.*(1./n_vp-1);
deta_vp_dEzz  = eta_vp./gdot.*dlamdEzz.*(1./n_vp-1);
deta_vp_dExy  = eta_vp./gdot.*dlamdExy.*(1./n_vp-1);
deta_vp_dP    = eta_vp./gdot.*dlamdP  .*(1./n_vp-1);
% deta_vep_dE
a             = (dHdgdot + eta_vp + K.*dt.*sin(psi).*sin(phi));
deta_vep.dExx = zeros(size(Txx));%-Exx.*Tyield./(4*Eii.^3) + (a.*dlamdExx + gdot.*deta_vp_dExx)./(2*Eii);
deta_vep.dEyy = zeros(size(Txx));%-Eyy.*Tyield./(4*Eii.^3) + (a.*dlamdEyy + gdot.*deta_vp_dEyy)./(2*Eii);
deta_vep.dEzz = zeros(size(Txx));%-Ezz.*Tyield./(4*Eii.^3) + (a.*dlamdEzz + gdot.*deta_vp_dEzz)./(2*Eii);
deta_vep.dExy = zeros(size(Txx));%-Exy.*Tyield./(2*Eii.^3) + (a.*dlamdExy + gdot.*deta_vp_dExy)./(2*Eii);
deta_vep.dP   =                (sin(phi) +  a.*dlamdP   + gdot.*deta_vp_dP  )./(2*Eii); 
deta_vep.dExx(pl==0) = deta_ve.dExx(pl==0);
deta_vep.dEyy(pl==0) = deta_ve.dEyy(pl==0);
deta_vep.dEzz(pl==0) = deta_ve.dEzz(pl==0);
deta_vep.dExy(pl==0) = deta_ve.dExy(pl==0);
deta_vep.dP(pl==0)   = deta_ve.dP(pl==0);
% ddivp
% dQdP       = -sin(psi);
% d2QdExxdP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdExx;
% d2QdEyydP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdEyy;
% d2QdEzzdP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdEzz;
% d2QdExydP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdExy;
% d2QdPdP    = -dt.*dQ.*hpsi.*cos(psi).*dlamdP;

dQdP       = -sin(psi);
d2QdExxdP  = -dpsidgdot.*cos(psi).*dlamdExx;
d2QdEyydP  = -dpsidgdot.*cos(psi).*dlamdEyy;
d2QdEzzdP  = -dpsidgdot.*cos(psi).*dlamdEzz;
d2QdExydP  = -dpsidgdot.*cos(psi).*dlamdExy;
d2QdPdP    = -dpsidgdot.*cos(psi).*dlamdP;
ddivp.dExx = -dQdP.*dlamdExx - d2QdExxdP.*gdot; ddivp.dExx(pl==0) = 0;
ddivp.dEyy = -dQdP.*dlamdEyy - d2QdEyydP.*gdot; ddivp.dEyy(pl==0) = 0;
ddivp.dEzz = -dQdP.*dlamdEzz - d2QdEzzdP.*gdot; ddivp.dEzz(pl==0) = 0;
ddivp.dExy = -dQdP.*dlamdExy - d2QdExydP.*gdot; ddivp.dExy(pl==0) = 0;
ddivp.dP   = -dQdP.*dlamdP   - d2QdPdP  .*gdot; ddivp.dP(pl==0)   = 0;
% Secure things
deta_vep.dExx(isinf(deta_vep.dExx)) = 0;
deta_vep.dExx(isnan(deta_vep.dExx)) = 0;
deta_vep.dEyy(isinf(deta_vep.dEyy)) = 0;
deta_vep.dEyy(isnan(deta_vep.dEyy)) = 0;
deta_vep.dEzz(isinf(deta_vep.dEzz)) = 0;
deta_vep.dEzz(isnan(deta_vep.dEzz)) = 0;
deta_vep.dExy(isinf(deta_vep.dExy)) = 0;
deta_vep.dExy(isnan(deta_vep.dExy)) = 0;
deta_vep.dP(isinf(deta_vep.dP))     = 0;
deta_vep.dP(isnan(deta_vep.dP))     = 0;
% Mess
Stuff.dg     = gdot;
Stuff.dQdtxx = dQdtxx.*pl;
Stuff.dQdtyy = dQdtyy.*pl;
Stuff.dQdtxy = dQdtxy.*pl;
Stuff.dQdtzz = dQdtzz.*pl;
Stuff.dQdP   = dQdP.*pl;
Stuff.Jii    = Tii.^2;
Stuff.Txx    = Txx;
Stuff.Tyy    = Tyy;
Stuff.Txy    = Txy;
Stuff.Tzz    = Tzz;
Stuff.eta_t  = eta_ve;
Stuff.Pt     = Pc;
if sum(pl(:))>0
Stuff.H         = - K.*dt.*sin(psi).*sin(phi) - eta_vp./n_vp - dt.*(dhcoh.*gdot + hcoh) - Pc.*dt.*(dhphi.*gdot + hphi).*cos(phi); % for CTL
Stuff.H (pl==0) = 0;
else
Stuff.H         = 0;
end
end