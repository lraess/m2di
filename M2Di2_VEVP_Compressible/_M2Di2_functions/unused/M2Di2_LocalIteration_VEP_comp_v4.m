function [ eta_vep, K_vep, Txx, Tyy, Tzz, Txy, Pc, divp, deta_ve, deta_vep, ddivp, pl, eta_Kel, Eii_el, Eii_pwl, Eii_pl, Overstress, Stuff, dE2pl, dC, dphi ] = M2Di2_LocalIteration_VEP_comp_v4( litmax, tol, noisy, div, Exx, Eyy, Ezz, Exy, Txxo, Tyyo, Tzzo, Txyo, B_pwl, m_pwl, eta_el, K, P0, phi, psi, C, eta_vp, eta_Kel0, n_vp, dt, E2pl0, Coh, C0, Phi, phi0  )
% This one works with power law VP and cohesion softening
% Initialise
Eii_el     = 0;
Eii_pwl    = 0;
Eii_pl     = 0;
Overstress = 0;
dC         = zeros(size(Exx));
dphi       = zeros(size(Exx));
hcoh       = 0;
dhcoh      = 0;
hphi       = 0;
dhphi      = 0;
K_vep      = K;
divp       = 0;

% Strain rate components and invariant
exx    = Exx;
eyy    = Eyy;
ezz    = Ezz;
exy    = Exy;
eII    = sqrt(1/2*(exx.^2 + eyy.^2 + ezz.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
Exx    = Exx + Txxo./2./eta_el;
Eyy    = Eyy + Tyyo./2./eta_el;
Ezz    = Ezz + Tzzo./2./eta_el;
Exy    = Exy + Txyo./2./eta_el;
Div    = div - P0./K./dt;
Pc     = -K.*dt.*Div;
Eii    = sqrt(1/2*(Exx.^2 + Eyy.^2 + Ezz.^2) + Exy.^2);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*eII.^(1./n_pwl-1);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_ve     = 0.5*(eta_up+eta_lo);

if noisy>2, fprintf('Starting Visco-Elastic local iterations:\n'); end

% Local iterations
for lit=1:litmax
    
    % Function evaluation at current effective viscosity
    Tii       = 2*eta_ve.*Eii;
    Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
    Eii_vis_0 = Eii_pwl_0;
    r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
    
    % Residual check
    res = max(max(abs(r_eta_0)));
    if lit == 1; res0 = res; end
    if noisy>2, fprintf('It. %02d, r = %2.2e\n', lit, res/res0); end
    if res/res0 < tol
        break;
    end
    
    % Exact derivative
    drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_ve;
    
    % Update viscosity
    eta_ve  = eta_ve - r_eta_0 ./ drdeta;
    
end

% Update individual deviatoric stress components
Txx = 2*eta_ve.*Exx;
Tyy = 2*eta_ve.*Eyy;
Tzz = 2*eta_ve.*Ezz;
Txy = 2*eta_ve.*Exy;
% For analytical Jacobian --- VE  --- ViscosityDerivativesTest.py
Jii = Tii.^2;
detadTxx     = -2*C_pwl.*eta_ve.^2.*(Jii ).^(n_pwl/2 - 1/2).*(2.0*Txx + 1.0*Tyy).*(n_pwl/2 - 1/2)./(Jii );
detadTyy     = -2*C_pwl.*eta_ve.^2.*(Jii ).^(n_pwl/2 - 1/2).*(1.0*Txx + 2.0*Tyy).*(n_pwl/2 - 1/2)./(Jii );
detadTxy     = -4*C_pwl.*Txy.*eta_ve.^2.*(Jii).^(n_pwl/2 - 1/2).*(n_pwl/2 - 1/2)./(Jii);
deta_ve.dExx = detadTxx .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*(Exx) + detadTyy.*(Eyy) + detadTxy.*Exy));
deta_ve.dEyy = detadTyy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*(Eyy) + detadTxy.*Exy));
deta_ve.dExy = detadTxy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy));
deta_ve.dP   = zeros(size(Txx));
deta_ve.dEzz = zeros(size(Txx));



% Secure things
deta_ve.dExx(isinf(deta_ve.dExx)) = 0; deta_ve.dExx(isnan(deta_ve.dEyy)) = 0;
deta_ve.dEyy(isinf(deta_ve.dEyy)) = 0; deta_ve.dEyy(isnan(deta_ve.dEyy)) = 0;
deta_ve.dExy(isinf(deta_ve.dExy)) = 0; deta_ve.dExy(isnan(deta_ve.dExy)) = 0;

% Plasticity
% Total stress 
P   = -K.*dt.*(Div-divp);
Sxx = -P + Txx;
Syy = -P + Tyy;
Szz = -P + Tzz;
P2  = -1/3*(Sxx+Syy+Szz);
% chk1 = norm(P-P2)

P              = -K.*dt.*Div;
Tyield         = C + P.*sin(phi);
X              = eta_ve./eta_el;
F              = Tii - Tyield;
pl             = F >= 0;
dQdtxx         = Txx./2./Tii;
dQdtyy         = Tyy./2./Tii;
dQdtzz         = Tzz./2./Tii;
dQdtxy         = Txy./1./Tii;


psi            = zeros(size(phi));
dQdP           = -sin(psi);
dFdP           = -sin(phi);



dQ             = sqrt( 1/2*(dQdtxx).^2 + 1/2*(dQdtyy).^2 + 1/2*(dQdtzz).^2 + (dQdtxy/2).^2 );
gdot           = zeros(size(Txx));
eta_Kel        = zeros(size(Txx));
gdot(pl==1)    = F(pl==1)./(eta_ve(pl==1));
eta_Kel(pl==1) = eta_Kel0*(gdot(pl==1)).^(1/n_vp-1);
res0           = max(F(:));
Fc             = F;
pl             = F>0.0;
if noisy>2, fprintf('Starting ViscoPlastic local iterations - F trial = %2.2e:\n', res0); end

if sum(pl(:)>0)
    % Plastic strain rates
    for lit=1:litmax
        % Evaluate F
        a1 = Coh.a1; a2 = Coh.a2; a4 = E2pl0 - Coh.a0 + (dQ.*dt.*gdot)/2; % midpoint!  dE2pl = dQ.*dt.*gdot
        b1 = Phi.a1; b2 = Phi.a2; b4 = E2pl0 - Phi.a0 + (dQ.*dt.*gdot)/2; % midpoint!  dE2pl = dQ.*dt.*gdot
        hcoh           = Coh.soft_on .* -dQ.*a1.*a2 ./ ( a1.^2.*a4.^2 + 1);
        hphi           = Phi.soft_on .* -dQ.*b1.*b2 ./ ( b1.^2.*b4.^2 + 1);
        dC             = hcoh.*gdot*dt; 
        dphi           = hphi.*gdot*dt;
        
        
%         Pc              = P;
        divp           = -gdot.*dQdP;
        Pc             = -K.*dt.*(Div-divp);
        
        Tyield         = C0 + dC + Pc.*sin(phi0 + dphi);
        eta_Kel(pl==1) = eta_Kel0 .* gdot(pl==1) .^ (1/n_vp-1);
        Fc(pl==1)      = Tii(pl==1) - eta_ve(pl==1).*gdot(pl==1) - Tyield(pl==1) - eta_Kel(pl==1).*gdot(pl==1);
        % Residual check
        res = max(max(abs(Fc(pl))));
        Fc_pl = Fc(pl==1);
        if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e, max(F) = %2.2e\n', lit, res/res0, res, max(Fc_pl(:))); end
        if res/res0 < tol || res < tol
            break;
        end
        % Exact derivative
        dhcoh    =  Coh.soft_on .* a1.^3.*a2.*a4.*dt.*dQ.^2 ./(a1.^2.*a4.^2+1).^2;
        dhphi    =  Phi.soft_on .* b1.^3.*b2.*b4.*dt.*dQ.^2 ./(b1.^2.*b4.^2+1).^2;
        dFdgdot  = - K.*dt.*sin(psi).*sin(phi0 + dphi) - eta_ve - eta_Kel./n_vp - dt.*(dhcoh.*gdot + hcoh) - Pc.*cos(phi0 + dphi).*dt.*(dhphi.*gdot + hphi);
        % Update viscosity
        gdot(pl==1)  = gdot(pl==1) - Fc(pl==1) ./ dFdgdot(pl==1);
        gdot         = real(gdot); % MATLAB tends to spit out complex numbers
    end
end

% Post-process
exx_pl  = gdot.*dQdtxx;
eyy_pl  = gdot.*dQdtyy;
ezz_pl  = gdot.*dQdtzz;
exy_pl  = gdot.*dQdtxy*(1/2);
dE2pl   = sqrt( 1/2*(exx_pl*dt).^2 + 1/2*(eyy_pl*dt).^2 + 1/2*(ezz_pl*dt).^2 + (exy_pl*dt).^2 );
Txx     = 2.*eta_ve.*(exx-exx_pl) +  X.*Txxo;
Tyy     = 2.*eta_ve.*(eyy-eyy_pl) +  X.*Tyyo;
Tzz     = 2.*eta_ve.*(ezz-ezz_pl) +  X.*Tzzo;
Txy     = 2.*eta_ve.*(exy-exy_pl) +  X.*Txyo;
Tii     = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*Tzz.^2 + Txy.^2);
eta_vep = eta_ve;
eta_vep(pl==1) = Tii(pl==1)./2./Eii(pl==1);
K_vep(pl==1)   = -Pc(pl==1)./(dt.*Div(pl==1));

% Total stress 
P   = -K.*dt.*(Div-divp);
Sxx = -P + Txx;
Syy = -P + Tyy;
Szz = -P + Tzz;
P2  = -1/3*(Sxx+Syy+Szz);
% chk2 = norm(P-P2)
% chk3 = norm(Exx+Eyy+Ezz)
% chk4 = norm(-(Exx+Eyy)- Ezz)
% chk5 = norm(Tzz - 2*eta_vep.*(-(Exx+Eyy)))




% Checks
if noisy>2
    
%     sum(pl(:))
%     Pc2 = -K.*dt.*(Div-divp);
%     Pc1 = -K_vep.*dt.*(Div);
%     norm(Pc1-Pc)
%     norm(Pc2-Pc)
    
    % Check yield 1 after correction
    F1     = Tii - Tyield - gdot.*eta_Kel;

    % Double check wether effective viscosity is computed accurately
    Txx = 2*eta_vep.*Exx;
    Tyy = 2*eta_vep.*Eyy;
    Tzz = 2*eta_vep.*Ezz;
    Txy = 2*eta_vep.*Exy;
    Tii = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*(-(Tyy+Txx)).^2 + Txy.^2);
    Tii(pl==1)    = 2*eta_vep(pl==1).*Eii(pl==1);
    F2  = Tii - Tyield - gdot.*eta_Kel;
    
    % Correct power-law viscosity
    eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
    
    % 1: strain rate components
    exx_el  = (Txx-Txxo)./2./eta_el;
    exx_pwl = Txx./2./eta_pwl;
    exx_pl  = gdot.*dQdtxx;
    exx_net = norm(exx-exx_el-exx_pwl-exx_pl);
    
    eyy_el  = (Tyy-Tyyo)./2./eta_el;
    eyy_pwl = Tyy./2./eta_pwl;
    eyy_pl  = gdot.*dQdtyy;
    eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl);
    
    ezz_el  = (Tzz-Tzzo)./2./eta_el;
    ezz_pwl = Tzz./2./eta_pwl;
    ezz_pl  = gdot.*dQdtzz;
    ezz_net = norm(ezz-ezz_el-ezz_pwl-ezz_pl);
    
    exy_el  = (Txy-Txyo)./2./eta_el;
    exy_pwl = Txy./2./eta_pwl;
    exy_pl  = gdot.*dQdtxy*(1/2);
    exy_net = norm(exy-exy_el-exy_pwl-exy_pl);

    if sum(pl(:))>0, 
        gdot_chk = zeros(size(F));
        gdot_chk(pl==1) = F(pl==1) ./ (eta_ve(pl==1) + eta_Kel(pl==1) + dt*hcoh(pl==1) + K(pl==1).*dt.*sin(psi(pl==1)).*sin(phi(pl==1)));
    end
    
    if noisy>2 && sum(pl(:))>0
        Overstress  = Tii - Tyield;
        fprintf('F trial = %2.2e --- F corr1 = %2.2e --- F corr2 = %2.2e --- Over S. = %2.2e\n', max(F(:)), max(F1(:)), max(F2(:)), max(Overstress(:)))
        fprintf('net exx = %2.2e --- net eyy = %2.2e --- net ezz = %2.2e --- net exy = %2.2e\n', norm(exx_net),norm(eyy_net),norm(ezz_net),norm(exy_net))
        fprintf('net gdot = %2.2e\n', norm(gdot - gdot_chk)); 
    end
    
    % 2: invariants
    Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + 1/2*ezz_el .^2 + exy_el .^2);
    Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + 1/2*ezz_pwl.^2 + exy_pwl.^2);
    Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + 1/2*ezz_pl .^2 + exy_pl .^2);
    
end

% For analytical Jacobian --- VEP --- ViscosityDerivativesTest.py
Tyield        = Tyield + gdot.*eta_Kel;
phi           = phi0   + dphi;
% C             = C0     + dC;
% E2pl          = E2pl0  + dE2pl;
% dFdE
dFdExx        = 2.*(Exx+1/2*Eyy).*eta_ve./Eii + 2.*Eii.*deta_ve.dExx;
dFdEyy        = 2.*(Eyy+1/2*Exx).*eta_ve./Eii + 2.*Eii.*deta_ve.dEyy;
dFdExy        = 2*           Exy.*eta_ve./Eii + 2.*Eii.*deta_ve.dExy;  
dFdP          = -sin(phi);
% dgdotdE
g             =  1./ (eta_ve + eta_Kel./n_vp + dt.*(hcoh + dhcoh.*gdot + Pc.*cos(phi0 + dphi).*(dhphi.*gdot + hphi) ) + K.*dt.*sin(psi).*sin(phi) );
dlamdExx      = g .* (dFdExx - gdot .*deta_ve.dExx);
dlamdEyy      = g .* (dFdEyy - gdot .*deta_ve.dEyy);
dlamdExy      = g .* (dFdExy - gdot .*deta_ve.dExy);
dlamdP        = g .* (dFdP                        );

% deta_vp_dE
deta_vp_dExx  = eta_Kel./gdot.*dlamdExx.*(1./n_vp-1);
deta_vp_dEyy  = eta_Kel./gdot.*dlamdEyy.*(1./n_vp-1);
deta_vp_dExy  = eta_Kel./gdot.*dlamdExy.*(1./n_vp-1);
deta_vp_dP    = eta_Kel./gdot.*dlamdP  .*(1./n_vp-1);
% deta_vep_dE
a             = (dt*hcoh + dt*gdot.*dhcoh + Pc.*dt.*(dhphi.*gdot + hphi).*cos(phi0 + dphi) + eta_Kel + K.*dt.*sin(psi).*sin(phi));
deta_vep.dExx = -(Exx+1/2*Eyy).*Tyield./(2*Eii.^3) + (a.*dlamdExx + gdot.*deta_vp_dExx)./(2*Eii);
deta_vep.dEyy = -(Eyy+1/2*Exx).*Tyield./(2*Eii.^3) + (a.*dlamdEyy + gdot.*deta_vp_dEyy)./(2*Eii);
deta_vep.dExy =           -Exy.*Tyield./(2*Eii.^3) + (a.*dlamdExy + gdot.*deta_vp_dExy)./(2*Eii);
deta_vep.dP   =                    (sin(phi) +  a.*dlamdP   + gdot.*deta_vp_dP  )./(2*Eii); 
deta_vep.dExx(pl==0) = deta_ve.dExx(pl==0);
deta_vep.dEyy(pl==0) = deta_ve.dEyy(pl==0);
deta_vep.dExy(pl==0) = deta_ve.dExy(pl==0);
deta_vep.dP(pl==0)   = deta_ve.dP(pl==0);
% ddivp
ddivp.dExx = dQdP.*dlamdExx; ddivp.dExx(pl==0) = 0;
ddivp.dEyy = dQdP.*dlamdEyy; ddivp.dEyy(pl==0) = 0;
ddivp.dExy = dQdP.*dlamdExy; ddivp.dExy(pl==0) = 0;
ddivp.dP   = dQdP.*dlamdP;   ddivp.dP(pl==0)   = 0;

% Secure things
deta_vep.dExx(isinf(deta_vep.dExx)) = 0;
deta_vep.dExx(isnan(deta_vep.dExx)) = 0;
deta_vep.dEyy(isinf(deta_vep.dEyy)) = 0;
deta_vep.dEyy(isnan(deta_vep.dEyy)) = 0;
deta_vep.dExy(isinf(deta_vep.dExy)) = 0;
deta_vep.dExy(isnan(deta_vep.dExy)) = 0;
deta_vep.dP(isinf(deta_vep.dP))     = 0;
deta_vep.dP(isnan(deta_vep.dP))     = 0;
% Mess
Stuff.dg     = gdot;
Stuff.dQdtxx = dQdtxx.*pl;
Stuff.dQdtyy = dQdtyy.*pl;
Stuff.dQdtxy = dQdtxy.*pl;
Stuff.Jii    = Tii.^2;
Stuff.Txx    = Txx;
Stuff.Tyy    = Tyy;
Stuff.Txy    = Txy;
Stuff.eta_t  = eta_ve;
if sum(pl(:))>0
Stuff.H         = - K.*dt.*sin(psi).*sin(phi) - eta_Kel./n_vp - dt.*(dhcoh.*gdot + hcoh) - Pc.*dt.*(dhphi.*gdot + hphi).*cos(phi); % for CTL
Stuff.H (pl==0) = 0;
else
Stuff.H         = 0;
end
end