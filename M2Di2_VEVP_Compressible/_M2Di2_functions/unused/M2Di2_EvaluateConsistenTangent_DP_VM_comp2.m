function [D] = M2Di2_EvaluateConsistenTangent_DP_VM_comp2(plc,dgc,Txxc,Tyyc,Tzzc,Txyc,Ptc,Jiic,dQdtxxc,dQdtyyc,dQdtzzc,dQdtxyc,dQdPc,psic,phic,etac,plv,dgv,Txxv,Tyyv,Tzzv,Txyv,Ptv,Jiiv,dQdtxxv,dQdtyyv,dQdtzzv,dQdtxyv,dQdPv,psiv,phiv,etav,Newton,Kc,Kv,dt,comp, CTL_c, CTL_v)

%% CHECK FILE: CTL_CodeGen_comp_vzz.py

dQdtxx = dQdtxxc; dQdtyy = dQdtyyc; dQdtzz = dQdtzzc; dQdtxy = dQdtxyc;  dQdP = dQdPc;
dFdtxx = dQdtxxc; dFdtyy = dQdtyyc; dFdtzz = dQdtzzc; dFdtxy = dQdtxyc;  dFdP = -sin(phic).*plc;
D11 = 2*etac; D22 = 2*etac; D33 = 1*etac; D55 = 2*etac; 
if comp==0, D44 = 1     ; end
if comp==1, D44 = -Kc*dt; end

Txx = Txxc; Tyy = Tyyc; Txy = Txyc; J2 = Jiic; Tzz = Tzzc; Tii = sqrt(J2);
d2Qdtxxdtxx = 0.5./Tii - 0.25*Txx.^2./Tii.^3;
d2Qdtxxdtyy = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtxxdtxy = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxxdtzz = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtyydtxx = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtyydtyy = 0.5./Tii - 0.25*Tyy.^2./Tii.^3;
d2Qdtyydtxy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtyydtzz = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtxydtxx = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxydtyy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtxydtxy = 1./Tii - Txy.^2./Tii.^3;
d2Qdtxydtzz = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtxx = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtzzdtyy = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtzzdtxy = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtzz = 0.5./Tii - 0.25*Tzz.^2./Tii.^3;

dlam = dgc;
if Newton == 1, dlam = 0*dlam; end

M11 = 1 + dlam .* D11 .* d2Qdtxxdtxx;
M12 = dlam .* D11 .* d2Qdtxxdtyy;
M13 = dlam .* D11 .* d2Qdtxxdtxy;
M15 = dlam .* D11 .* d2Qdtxxdtzz;
M21 = dlam .* D11 .* d2Qdtyydtxx;
M22 = 1 + dlam .* D11 .* d2Qdtyydtyy;
M23 = dlam .* D11 .* d2Qdtyydtxy;
M25 = dlam .* D11 .* d2Qdtyydtzz;
M31 = dlam .* D33 .* d2Qdtxydtxx;
M32 = dlam .* D33 .* d2Qdtxydtyy;
M33 = 1 + dlam .* D33 .* d2Qdtxydtxy;
M35 = dlam .* D33 .* d2Qdtxydtzz;
M51 = dlam .* D11 .* d2Qdtzzdtxx;
M52 = dlam .* D11 .* d2Qdtzzdtyy;
M53 = dlam .* D11 .* d2Qdtzzdtxy;
M55 = 1 + dlam .* D11 .* d2Qdtzzdtzz;
[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );

%%------------------------
% % Consistent tangent
den = -CTL_c.H + D11.*dQdtxx.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi51.*dFdtzz) + D22.*dQdtyy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi52.*dFdtzz) + D33.*dQdtxy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi53.*dFdtzz) + 0*D44.*dFdP.*dQdP + D55.*dQdtzz.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi55.*dFdtzz);
den  (plc==0) = 1;

D.D11c = D11.*Mi11.*(1 - (D11.*Mi11.*dFdtxx.*dQdtxx + D11.*Mi21.*dFdtyy.*dQdtxx + D11.*Mi31.*dFdtxy.*dQdtxx + D11.*Mi51.*dFdtzz.*dQdtxx)./den) - D22.*Mi12.*(D11.*Mi11.*dFdtxx.*dQdtyy + D11.*Mi21.*dFdtyy.*dQdtyy + D11.*Mi31.*dFdtxy.*dQdtyy + D11.*Mi51.*dFdtzz.*dQdtyy)./den - D33.*Mi13.*(D11.*Mi11.*dFdtxx.*dQdtxy + D11.*Mi21.*dFdtyy.*dQdtxy + D11.*Mi31.*dFdtxy.*dQdtxy + D11.*Mi51.*dFdtzz.*dQdtxy)./den - D55.*Mi15.*(D11.*Mi11.*dFdtxx.*dQdtzz + D11.*Mi21.*dFdtyy.*dQdtzz + D11.*Mi31.*dFdtxy.*dQdtzz + D11.*Mi51.*dFdtzz.*dQdtzz)./den;
D.D12c = -D11.*Mi11.*(D22.*Mi12.*dFdtxx.*dQdtxx + D22.*Mi22.*dFdtyy.*dQdtxx + D22.*Mi32.*dFdtxy.*dQdtxx + D22.*Mi52.*dFdtzz.*dQdtxx)./den + D22.*Mi12.*(1 - (D22.*Mi12.*dFdtxx.*dQdtyy + D22.*Mi22.*dFdtyy.*dQdtyy + D22.*Mi32.*dFdtxy.*dQdtyy + D22.*Mi52.*dFdtzz.*dQdtyy)./den) - D33.*Mi13.*(D22.*Mi12.*dFdtxx.*dQdtxy + D22.*Mi22.*dFdtyy.*dQdtxy + D22.*Mi32.*dFdtxy.*dQdtxy + D22.*Mi52.*dFdtzz.*dQdtxy)./den - D55.*Mi15.*(D22.*Mi12.*dFdtxx.*dQdtzz + D22.*Mi22.*dFdtyy.*dQdtzz + D22.*Mi32.*dFdtxy.*dQdtzz + D22.*Mi52.*dFdtzz.*dQdtzz)./den;
D.D13c = -D11.*Mi11.*(D33.*Mi13.*dFdtxx.*dQdtxx + D33.*Mi23.*dFdtyy.*dQdtxx + D33.*Mi33.*dFdtxy.*dQdtxx + D33.*Mi53.*dFdtzz.*dQdtxx)./den - D22.*Mi12.*(D33.*Mi13.*dFdtxx.*dQdtyy + D33.*Mi23.*dFdtyy.*dQdtyy + D33.*Mi33.*dFdtxy.*dQdtyy + D33.*Mi53.*dFdtzz.*dQdtyy)./den + D33.*Mi13.*(1 - (D33.*Mi13.*dFdtxx.*dQdtxy + D33.*Mi23.*dFdtyy.*dQdtxy + D33.*Mi33.*dFdtxy.*dQdtxy + D33.*Mi53.*dFdtzz.*dQdtxy)./den) - D55.*Mi15.*(D33.*Mi13.*dFdtxx.*dQdtzz + D33.*Mi23.*dFdtyy.*dQdtzz + D33.*Mi33.*dFdtxy.*dQdtzz + D33.*Mi53.*dFdtzz.*dQdtzz)./den;
D.D14c = -D11.*D44.*Mi11.*dFdP.*dQdtxx./den - D22.*D44.*Mi12.*dFdP.*dQdtyy./den - D33.*D44.*Mi13.*dFdP.*dQdtxy./den - D44.*D55.*Mi15.*dFdP.*dQdtzz./den;
D.D15c = -D11.*Mi11.*(D55.*Mi15.*dFdtxx.*dQdtxx + D55.*Mi25.*dFdtyy.*dQdtxx + D55.*Mi35.*dFdtxy.*dQdtxx + D55.*Mi55.*dFdtzz.*dQdtxx)./den - D22.*Mi12.*(D55.*Mi15.*dFdtxx.*dQdtyy + D55.*Mi25.*dFdtyy.*dQdtyy + D55.*Mi35.*dFdtxy.*dQdtyy + D55.*Mi55.*dFdtzz.*dQdtyy)./den - D33.*Mi13.*(D55.*Mi15.*dFdtxx.*dQdtxy + D55.*Mi25.*dFdtyy.*dQdtxy + D55.*Mi35.*dFdtxy.*dQdtxy + D55.*Mi55.*dFdtzz.*dQdtxy)./den + D55.*Mi15.*(1 - (D55.*Mi15.*dFdtxx.*dQdtzz + D55.*Mi25.*dFdtyy.*dQdtzz + D55.*Mi35.*dFdtxy.*dQdtzz + D55.*Mi55.*dFdtzz.*dQdtzz)./den);
D.D21c = D11.*Mi21.*(1 - (D11.*Mi11.*dFdtxx.*dQdtxx + D11.*Mi21.*dFdtyy.*dQdtxx + D11.*Mi31.*dFdtxy.*dQdtxx + D11.*Mi51.*dFdtzz.*dQdtxx)./den) - D22.*Mi22.*(D11.*Mi11.*dFdtxx.*dQdtyy + D11.*Mi21.*dFdtyy.*dQdtyy + D11.*Mi31.*dFdtxy.*dQdtyy + D11.*Mi51.*dFdtzz.*dQdtyy)./den - D33.*Mi23.*(D11.*Mi11.*dFdtxx.*dQdtxy + D11.*Mi21.*dFdtyy.*dQdtxy + D11.*Mi31.*dFdtxy.*dQdtxy + D11.*Mi51.*dFdtzz.*dQdtxy)./den - D55.*Mi25.*(D11.*Mi11.*dFdtxx.*dQdtzz + D11.*Mi21.*dFdtyy.*dQdtzz + D11.*Mi31.*dFdtxy.*dQdtzz + D11.*Mi51.*dFdtzz.*dQdtzz)./den;
D.D22c = -D11.*Mi21.*(D22.*Mi12.*dFdtxx.*dQdtxx + D22.*Mi22.*dFdtyy.*dQdtxx + D22.*Mi32.*dFdtxy.*dQdtxx + D22.*Mi52.*dFdtzz.*dQdtxx)./den + D22.*Mi22.*(1 - (D22.*Mi12.*dFdtxx.*dQdtyy + D22.*Mi22.*dFdtyy.*dQdtyy + D22.*Mi32.*dFdtxy.*dQdtyy + D22.*Mi52.*dFdtzz.*dQdtyy)./den) - D33.*Mi23.*(D22.*Mi12.*dFdtxx.*dQdtxy + D22.*Mi22.*dFdtyy.*dQdtxy + D22.*Mi32.*dFdtxy.*dQdtxy + D22.*Mi52.*dFdtzz.*dQdtxy)./den - D55.*Mi25.*(D22.*Mi12.*dFdtxx.*dQdtzz + D22.*Mi22.*dFdtyy.*dQdtzz + D22.*Mi32.*dFdtxy.*dQdtzz + D22.*Mi52.*dFdtzz.*dQdtzz)./den;
D.D23c = -D11.*Mi21.*(D33.*Mi13.*dFdtxx.*dQdtxx + D33.*Mi23.*dFdtyy.*dQdtxx + D33.*Mi33.*dFdtxy.*dQdtxx + D33.*Mi53.*dFdtzz.*dQdtxx)./den - D22.*Mi22.*(D33.*Mi13.*dFdtxx.*dQdtyy + D33.*Mi23.*dFdtyy.*dQdtyy + D33.*Mi33.*dFdtxy.*dQdtyy + D33.*Mi53.*dFdtzz.*dQdtyy)./den + D33.*Mi23.*(1 - (D33.*Mi13.*dFdtxx.*dQdtxy + D33.*Mi23.*dFdtyy.*dQdtxy + D33.*Mi33.*dFdtxy.*dQdtxy + D33.*Mi53.*dFdtzz.*dQdtxy)./den) - D55.*Mi25.*(D33.*Mi13.*dFdtxx.*dQdtzz + D33.*Mi23.*dFdtyy.*dQdtzz + D33.*Mi33.*dFdtxy.*dQdtzz + D33.*Mi53.*dFdtzz.*dQdtzz)./den;
D.D24c = -D11.*D44.*Mi21.*dFdP.*dQdtxx./den - D22.*D44.*Mi22.*dFdP.*dQdtyy./den - D33.*D44.*Mi23.*dFdP.*dQdtxy./den - D44.*D55.*Mi25.*dFdP.*dQdtzz./den;
D.D25c = -D11.*Mi21.*(D55.*Mi15.*dFdtxx.*dQdtxx + D55.*Mi25.*dFdtyy.*dQdtxx + D55.*Mi35.*dFdtxy.*dQdtxx + D55.*Mi55.*dFdtzz.*dQdtxx)./den - D22.*Mi22.*(D55.*Mi15.*dFdtxx.*dQdtyy + D55.*Mi25.*dFdtyy.*dQdtyy + D55.*Mi35.*dFdtxy.*dQdtyy + D55.*Mi55.*dFdtzz.*dQdtyy)./den - D33.*Mi23.*(D55.*Mi15.*dFdtxx.*dQdtxy + D55.*Mi25.*dFdtyy.*dQdtxy + D55.*Mi35.*dFdtxy.*dQdtxy + D55.*Mi55.*dFdtzz.*dQdtxy)./den + D55.*Mi25.*(1 - (D55.*Mi15.*dFdtxx.*dQdtzz + D55.*Mi25.*dFdtyy.*dQdtzz + D55.*Mi35.*dFdtxy.*dQdtzz + D55.*Mi55.*dFdtzz.*dQdtzz)./den);

D.D41c = -D44.*(D11.*Mi11.*dFdtxx.*dQdP + D11.*Mi21.*dFdtyy.*dQdP + D11.*Mi31.*dFdtxy.*dQdP + D11.*Mi51.*dFdtzz.*dQdP)./den;
D.D42c = -D44.*(D22.*Mi12.*dFdtxx.*dQdP + D22.*Mi22.*dFdtyy.*dQdP + D22.*Mi32.*dFdtxy.*dQdP + D22.*Mi52.*dFdtzz.*dQdP)./den;
D.D43c = -D44.*(D33.*Mi13.*dFdtxx.*dQdP + D33.*Mi23.*dFdtyy.*dQdP + D33.*Mi33.*dFdtxy.*dQdP + D33.*Mi53.*dFdtzz.*dQdP)./den;
D.D44c = D44.*(-D44.*dFdP.*dQdP./den + 1);
D.D45c = -D44.*(D55.*Mi15.*dFdtxx.*dQdP + D55.*Mi25.*dFdtyy.*dQdP + D55.*Mi35.*dFdtxy.*dQdP + D55.*Mi55.*dFdtzz.*dQdP)./den;

D.D51c = D11.*Mi51.*(1 - (D11.*Mi11.*dFdtxx.*dQdtxx + D11.*Mi21.*dFdtyy.*dQdtxx + D11.*Mi31.*dFdtxy.*dQdtxx + D11.*Mi51.*dFdtzz.*dQdtxx)./den) - D22.*Mi52.*(D11.*Mi11.*dFdtxx.*dQdtyy + D11.*Mi21.*dFdtyy.*dQdtyy + D11.*Mi31.*dFdtxy.*dQdtyy + D11.*Mi51.*dFdtzz.*dQdtyy)./den - D33.*Mi53.*(D11.*Mi11.*dFdtxx.*dQdtxy + D11.*Mi21.*dFdtyy.*dQdtxy + D11.*Mi31.*dFdtxy.*dQdtxy + D11.*Mi51.*dFdtzz.*dQdtxy)./den - D55.*Mi55.*(D11.*Mi11.*dFdtxx.*dQdtzz + D11.*Mi21.*dFdtyy.*dQdtzz + D11.*Mi31.*dFdtxy.*dQdtzz + D11.*Mi51.*dFdtzz.*dQdtzz)./den;
D.D52c = -D11.*Mi51.*(D22.*Mi12.*dFdtxx.*dQdtxx + D22.*Mi22.*dFdtyy.*dQdtxx + D22.*Mi32.*dFdtxy.*dQdtxx + D22.*Mi52.*dFdtzz.*dQdtxx)./den + D22.*Mi52.*(1 - (D22.*Mi12.*dFdtxx.*dQdtyy + D22.*Mi22.*dFdtyy.*dQdtyy + D22.*Mi32.*dFdtxy.*dQdtyy + D22.*Mi52.*dFdtzz.*dQdtyy)./den) - D33.*Mi53.*(D22.*Mi12.*dFdtxx.*dQdtxy + D22.*Mi22.*dFdtyy.*dQdtxy + D22.*Mi32.*dFdtxy.*dQdtxy + D22.*Mi52.*dFdtzz.*dQdtxy)./den - D55.*Mi55.*(D22.*Mi12.*dFdtxx.*dQdtzz + D22.*Mi22.*dFdtyy.*dQdtzz + D22.*Mi32.*dFdtxy.*dQdtzz + D22.*Mi52.*dFdtzz.*dQdtzz)./den;
D.D53c = -D11.*Mi51.*(D33.*Mi13.*dFdtxx.*dQdtxx + D33.*Mi23.*dFdtyy.*dQdtxx + D33.*Mi33.*dFdtxy.*dQdtxx + D33.*Mi53.*dFdtzz.*dQdtxx)./den - D22.*Mi52.*(D33.*Mi13.*dFdtxx.*dQdtyy + D33.*Mi23.*dFdtyy.*dQdtyy + D33.*Mi33.*dFdtxy.*dQdtyy + D33.*Mi53.*dFdtzz.*dQdtyy)./den + D33.*Mi53.*(1 - (D33.*Mi13.*dFdtxx.*dQdtxy + D33.*Mi23.*dFdtyy.*dQdtxy + D33.*Mi33.*dFdtxy.*dQdtxy + D33.*Mi53.*dFdtzz.*dQdtxy)./den) - D55.*Mi55.*(D33.*Mi13.*dFdtxx.*dQdtzz + D33.*Mi23.*dFdtyy.*dQdtzz + D33.*Mi33.*dFdtxy.*dQdtzz + D33.*Mi53.*dFdtzz.*dQdtzz)./den;
D.D54c = -D11.*D44.*Mi51.*dFdP.*dQdtxx./den - D22.*D44.*Mi52.*dFdP.*dQdtyy./den - D33.*D44.*Mi53.*dFdP.*dQdtxy./den - D44.*D55.*Mi55.*dFdP.*dQdtzz./den;
D.D55c = -D11.*Mi51.*(D55.*Mi15.*dFdtxx.*dQdtxx + D55.*Mi25.*dFdtyy.*dQdtxx + D55.*Mi35.*dFdtxy.*dQdtxx + D55.*Mi55.*dFdtzz.*dQdtxx)./den - D22.*Mi52.*(D55.*Mi15.*dFdtxx.*dQdtyy + D55.*Mi25.*dFdtyy.*dQdtyy + D55.*Mi35.*dFdtxy.*dQdtyy + D55.*Mi55.*dFdtzz.*dQdtyy)./den - D33.*Mi53.*(D55.*Mi15.*dFdtxx.*dQdtxy + D55.*Mi25.*dFdtyy.*dQdtxy + D55.*Mi35.*dFdtxy.*dQdtxy + D55.*Mi55.*dFdtzz.*dQdtxy)./den + D55.*Mi55.*(1 - (D55.*Mi15.*dFdtxx.*dQdtzz + D55.*Mi25.*dFdtyy.*dQdtzz + D55.*Mi35.*dFdtxy.*dQdtzz + D55.*Mi55.*dFdtzz.*dQdtzz)./den);

%%------------------------
D.D14c = D.D14c./D44;
D.D24c = D.D24c./D44;
D.D44c = D.D44c./D44; % [ - ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dQdtxx = dQdtxxv; dQdtyy = dQdtyyv; dQdtzz = dQdtzzv; dQdtxy = dQdtxyv; dQdP = dQdPv;
dFdtxx = dQdtxxv; dFdtyy = dQdtyyv; dFdtzz = dQdtzzv; dFdtxy = dQdtxyv; dFdP = -sin(phiv).*plv;
D11 = 2*etav; D22 = 2*etav; D33 = 1*etav; D55 = 2*etav;  
if comp==0, D44 = 1     ; end
if comp==1, D44 = -Kv*dt; end

Txx = Txxv; Tyy = Tyyv; Txy = Txyv; J2 = Jiiv; Tzz = Tzzv; Tii = sqrt(J2);
d2Qdtxxdtxx = 0.5./Tii - 0.25*Txx.^2./Tii.^3;
d2Qdtxxdtyy = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtxxdtxy = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxxdtzz = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtyydtxx = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtyydtyy = 0.5./Tii - 0.25*Tyy.^2./Tii.^3;
d2Qdtyydtxy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtyydtzz = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtxydtxx = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxydtyy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtxydtxy = 1./Tii - Txy.^2./Tii.^3;
d2Qdtxydtzz = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtxx = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtzzdtyy = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtzzdtxy = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtzz = 0.5./Tii - 0.25*Tzz.^2./Tii.^3;

dlam = dgv;
if Newton==1, dlam = 0*dlam; end

M11 = 1 + dlam .* D11 .* d2Qdtxxdtxx;
M12 = dlam .* D11 .* d2Qdtxxdtyy;
M13 = dlam .* D11 .* d2Qdtxxdtxy;
M15 = dlam .* D11 .* d2Qdtxxdtzz;
M21 = dlam .* D11 .* d2Qdtyydtxx;
M22 = 1 + dlam .* D11 .* d2Qdtyydtyy;
M23 = dlam .* D11 .* d2Qdtyydtxy;
M25 = dlam .* D11 .* d2Qdtyydtzz;
M31 = dlam .* D33 .* d2Qdtxydtxx;
M32 = dlam .* D33 .* d2Qdtxydtyy;
M33 = 1 + dlam .* D33 .* d2Qdtxydtxy;
M35 = dlam .* D33 .* d2Qdtxydtzz;
M51 = dlam .* D11 .* d2Qdtzzdtxx;
M52 = dlam .* D11 .* d2Qdtzzdtyy;
M53 = dlam .* D11 .* d2Qdtzzdtxy;
M55 = 1 + dlam .* D11 .* d2Qdtzzdtzz;
[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );

%%------------------------
den = -CTL_v.H + D11.*dQdtxx.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi51.*dFdtzz) + D22.*dQdtyy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi52.*dFdtzz) + D33.*dQdtxy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi53.*dFdtzz) + 0*D44.*dFdP.*dQdP + D55.*dQdtzz.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi55.*dFdtzz);
den  (plv==0) = 1;

D.D31v = D11.*Mi31.*(1 - (D11.*Mi11.*dFdtxx.*dQdtxx + D11.*Mi21.*dFdtyy.*dQdtxx + D11.*Mi31.*dFdtxy.*dQdtxx + D11.*Mi51.*dFdtzz.*dQdtxx)./den) - D22.*Mi32.*(D11.*Mi11.*dFdtxx.*dQdtyy + D11.*Mi21.*dFdtyy.*dQdtyy + D11.*Mi31.*dFdtxy.*dQdtyy + D11.*Mi51.*dFdtzz.*dQdtyy)./den - D33.*Mi33.*(D11.*Mi11.*dFdtxx.*dQdtxy + D11.*Mi21.*dFdtyy.*dQdtxy + D11.*Mi31.*dFdtxy.*dQdtxy + D11.*Mi51.*dFdtzz.*dQdtxy)./den - D55.*Mi35.*(D11.*Mi11.*dFdtxx.*dQdtzz + D11.*Mi21.*dFdtyy.*dQdtzz + D11.*Mi31.*dFdtxy.*dQdtzz + D11.*Mi51.*dFdtzz.*dQdtzz)./den;
D.D32v = -D11.*Mi31.*(D22.*Mi12.*dFdtxx.*dQdtxx + D22.*Mi22.*dFdtyy.*dQdtxx + D22.*Mi32.*dFdtxy.*dQdtxx + D22.*Mi52.*dFdtzz.*dQdtxx)./den + D22.*Mi32.*(1 - (D22.*Mi12.*dFdtxx.*dQdtyy + D22.*Mi22.*dFdtyy.*dQdtyy + D22.*Mi32.*dFdtxy.*dQdtyy + D22.*Mi52.*dFdtzz.*dQdtyy)./den) - D33.*Mi33.*(D22.*Mi12.*dFdtxx.*dQdtxy + D22.*Mi22.*dFdtyy.*dQdtxy + D22.*Mi32.*dFdtxy.*dQdtxy + D22.*Mi52.*dFdtzz.*dQdtxy)./den - D55.*Mi35.*(D22.*Mi12.*dFdtxx.*dQdtzz + D22.*Mi22.*dFdtyy.*dQdtzz + D22.*Mi32.*dFdtxy.*dQdtzz + D22.*Mi52.*dFdtzz.*dQdtzz)./den;
D.D33v = -D11.*Mi31.*(D33.*Mi13.*dFdtxx.*dQdtxx + D33.*Mi23.*dFdtyy.*dQdtxx + D33.*Mi33.*dFdtxy.*dQdtxx + D33.*Mi53.*dFdtzz.*dQdtxx)./den - D22.*Mi32.*(D33.*Mi13.*dFdtxx.*dQdtyy + D33.*Mi23.*dFdtyy.*dQdtyy + D33.*Mi33.*dFdtxy.*dQdtyy + D33.*Mi53.*dFdtzz.*dQdtyy)./den + D33.*Mi33.*(1 - (D33.*Mi13.*dFdtxx.*dQdtxy + D33.*Mi23.*dFdtyy.*dQdtxy + D33.*Mi33.*dFdtxy.*dQdtxy + D33.*Mi53.*dFdtzz.*dQdtxy)./den) - D55.*Mi35.*(D33.*Mi13.*dFdtxx.*dQdtzz + D33.*Mi23.*dFdtyy.*dQdtzz + D33.*Mi33.*dFdtxy.*dQdtzz + D33.*Mi53.*dFdtzz.*dQdtzz)./den;
D.D34v = -D11.*D44.*Mi31.*dFdP.*dQdtxx./den - D22.*D44.*Mi32.*dFdP.*dQdtyy./den - D33.*D44.*Mi33.*dFdP.*dQdtxy./den - D44.*D55.*Mi35.*dFdP.*dQdtzz./den;
D.D35v = -D11.*Mi31.*(D55.*Mi15.*dFdtxx.*dQdtxx + D55.*Mi25.*dFdtyy.*dQdtxx + D55.*Mi35.*dFdtxy.*dQdtxx + D55.*Mi55.*dFdtzz.*dQdtxx)./den - D22.*Mi32.*(D55.*Mi15.*dFdtxx.*dQdtyy + D55.*Mi25.*dFdtyy.*dQdtyy + D55.*Mi35.*dFdtxy.*dQdtyy + D55.*Mi55.*dFdtzz.*dQdtyy)./den - D33.*Mi33.*(D55.*Mi15.*dFdtxx.*dQdtxy + D55.*Mi25.*dFdtyy.*dQdtxy + D55.*Mi35.*dFdtxy.*dQdtxy + D55.*Mi55.*dFdtzz.*dQdtxy)./den + D55.*Mi35.*(1 - (D55.*Mi15.*dFdtxx.*dQdtzz + D55.*Mi25.*dFdtyy.*dQdtzz + D55.*Mi35.*dFdtxy.*dQdtzz + D55.*Mi55.*dFdtzz.*dQdtzz)./den);

%%------------------------
D.D34v = D.D34v./D44;

end

