function T2M = M2Di2_P2MAssembly_v3( BC, D, dx, dy, nx, ny, etac, Kc, dt, NumVx, NumVyG, NumTe, Tc, SuiteSparse )
D41c = D.D41c;%./(Kc*dt);
D42c = D.D42c;%./(Kc*dt);
D43c = D.D43c;%./(Kc*dt);
D45c = D.D45c;%./(Kc*dt);
% BC flags
fs = zeros(nx+1,ny+1);
fs(:,1) = BC.fsxS(:,1);  fs(:,end) = BC.fsxN(:,end);
fs(1,:) = BC.fsyW(1,:);  fs(end,:) = BC.fsyE(end,:);
ns = zeros(nx+1,ny+1);
ns(:,1) = BC.nsxS(:,1);  ns(:,end) = BC.nsxN(:,end);
ns(1,:) = BC.nsyW(1,:);  ns(end,:) = BC.nsyE(end,:);
fsSW = fs(1:end-1,1:end-1);
fsSE = fs(2:end-0,1:end-1);
fsNW = fs(1:end-1,2:end-0);
fsNE = fs(2:end-0,2:end-0);
nsSW = ns(1:end-1,1:end-1);
nsSE = ns(2:end-0,1:end-1);
nsNW = ns(1:end-1,2:end-0);
nsNE = ns(2:end-0,2:end-0);
% Coefficients
% c3VxW  = D41c./dx - D43c.*(0.125*fsNW.*(-nsNW - 1)./dy + 0.125*fsSW.*(nsSW + 1)./dy) - 1./dx;
% c3VxE  = -D41c./dx - D43c.*(0.125*fsNE.*(-nsNE - 1)./dy + 0.125*fsSE.*(nsSE + 1)./dy) + 1./dx;
% c3VyS  = D42c./dy - D43c.*(0.125*fsSE.*(-nsSE - 1)./dx + 0.125*fsSW.*(nsSW + 1)./dx) - 1./dy;
% c3VyN  = -D42c./dy - D43c.*(0.125*fsNE.*(-nsNE - 1)./dx + 0.125*fsNW.*(nsNW + 1)./dx) + 1./dy;
% c3VxSW = -0.125*D43c.*fsSW.*(nsSW - 1)./dy;
% c3VxSE = -0.125*D43c.*fsSE.*(nsSE - 1)./dy;
% c3VxNW = -0.125*D43c.*fsNW.*(1 - nsNW)./dy;
% c3VxNE = -0.125*D43c.*fsNE.*(1 - nsNE)./dy;
% c3VySW = -0.125*D43c.*fsSW.*(nsSW - 1)./dx;
% c3VySE = -0.125*D43c.*fsSE.*(1 - nsSE)./dx;
% c3VyNW = -0.125*D43c.*fsNW.*(nsNW - 1)./dx;
% c3VyNE = -0.125*D43c.*fsNE.*(1 - nsNE)./dx;

% c3VxW = (1/4*D43c.*dx.*(fsNW.*nsNW + fsNW - fsSW.*nsSW - fsSW - nsNW + nsSW) + dy.*(-2/3*D41c + 1/3*D42c + 1/3*D45c))./(Kc.*dt.*dx.*dy);
% c3VxE = (1/4*D43c.*dx.*(fsNE.*nsNE + fsNE - fsSE.*nsSE - fsSE - nsNE + nsSE) + dy.*(2/3*D41c - 1/3*D42c - 1/3*D45c))./(Kc.*dt.*dx.*dy);
% c3VyS = (1/4*D43c.*dy.*(fsSE.*nsSE + fsSE - fsSW.*nsSW - fsSW - nsSE + nsSW) + dx.*(1/3*D41c - 2/3*D42c + 1/3*D45c))./(Kc.*dt.*dx.*dy);
% c3VyN = (1/4*D43c.*dy.*(fsNE.*nsNE + fsNE - fsNW.*nsNW - fsNW - nsNE + nsNW) + dx.*(-1/3*D41c + 2/3*D42c - 1/3*D45c))./(Kc.*dt.*dx.*dy);
% c3VxSW = 1/4*D43c.*(-fsSW.*nsSW + fsSW + nsSW - 1)./(Kc.*dt.*dy);
% c3VxSE = 1/4*D43c.*(-fsSE.*nsSE + fsSE + nsSE - 1)./(Kc.*dt.*dy);
% c3VxNW = 1/4*D43c.*(fsNW.*nsNW - fsNW - nsNW + 1)./(Kc.*dt.*dy);
% c3VxNE = 1/4*D43c.*(fsNE.*nsNE - fsNE - nsNE + 1)./(Kc.*dt.*dy);
% c3VySW = 1/4*D43c.*(-fsSW.*nsSW + fsSW + nsSW - 1)./(Kc.*dt.*dx);
% c3VySE = 1/4*D43c.*(fsSE.*nsSE - fsSE - nsSE + 1)./(Kc.*dt.*dx);
% c3VyNW = 1/4*D43c.*(-fsNW.*nsNW + fsNW + nsNW - 1)./(Kc.*dt.*dx);
% c3VyNE = 1/4*D43c.*(fsNE.*nsNE - fsNE - nsNE + 1)./(Kc.*dt.*dx);

c3VxW = (1/4*D43c.*dx.*(fsNW.*nsNW + fsNW - fsSW.*nsSW - fsSW - nsNW + nsSW) - Kc.*dt.*dy + dy.*(-2/3*D41c + 1/3*D42c + 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VxE = (1/4*D43c.*dx.*(fsNE.*nsNE + fsNE - fsSE.*nsSE - fsSE - nsNE + nsSE) + Kc.*dt.*dy + dy.*(2/3*D41c - 1/3*D42c - 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VyS = (1/4*D43c.*dy.*(fsSE.*nsSE + fsSE - fsSW.*nsSW - fsSW - nsSE + nsSW) - Kc.*dt.*dx + dx.*(1/3*D41c - 2/3*D42c + 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VyN = (1/4*D43c.*dy.*(fsNE.*nsNE + fsNE - fsNW.*nsNW - fsNW - nsNE + nsNW) + Kc.*dt.*dx + dx.*(-1/3*D41c + 2/3*D42c - 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VxSW = 1/4*D43c.*(-fsSW.*nsSW + fsSW + nsSW - 1)./(Kc.*dt.*dy);
c3VxSE = 1/4*D43c.*(-fsSE.*nsSE + fsSE + nsSE - 1)./(Kc.*dt.*dy);
c3VxNW = 1/4*D43c.*(fsNW.*nsNW - fsNW - nsNW + 1)./(Kc.*dt.*dy);
c3VxNE = 1/4*D43c.*(fsNE.*nsNE - fsNE - nsNE + 1)./(Kc.*dt.*dy);
c3VySW = 1/4*D43c.*(-fsSW.*nsSW + fsSW + nsSW - 1)./(Kc.*dt.*dx);
c3VySE = 1/4*D43c.*(fsSE.*nsSE - fsSE - nsSE + 1)./(Kc.*dt.*dx);
c3VyNW = 1/4*D43c.*(-fsNW.*nsNW + fsNW + nsNW - 1)./(Kc.*dt.*dx);
c3VyNE = 1/4*D43c.*(fsNE.*nsNE - fsNE - nsNE + 1)./(Kc.*dt.*dx);











c3VxW(1,:)   = 0;
c3VxSW(1,:)  = 0;
c3VxNW(1,:)  = 0;
c3VxE(end,:) = 0;
c3VxSE(end,:)= 0;
c3VxNE(end,:)= 0;
c3VyS(:,1)   = 0;
c3VySW(:,1)  = 0;
c3VySE(:,1)  = 0;
c3VyN(:,end) = 0;
c3VyNW(:,end)= 0;
c3VyNE(:,end)= 0;
% Equation indexes
iTe   = NumTe(:);
iVxW  = NumVx(1:end-1,:); iVyS  = NumVyG(:,1:end-1);
iVxE  = NumVx(2:end  ,:); iVyN  = NumVyG(:,2:end,:);
iVxSW = ones(size(Tc));  iVxSW(:,2:end  ) = NumVx (1:end-1, 1:end-1);
iVxSE = ones(size(Tc));  iVxSE(:,2:end  ) = NumVx (2:end  , 1:end-1);
iVxNW = ones(size(Tc));  iVxNW(:,1:end-1) = NumVx (1:end-1, 2:end  );
iVxNE = ones(size(Tc));  iVxNE(:,1:end-1) = NumVx (2:end  , 2:end  );
iVySW = ones(size(Tc));  iVySW(2:end  ,:) = NumVyG(1:end-1, 1:end-1);
iVySE = ones(size(Tc));  iVySE(1:end-1,:) = NumVyG(2:end  , 1:end-1);
iVyNW = ones(size(Tc));  iVyNW(2:end  ,:) = NumVyG(1:end-1, 2:end  );
iVyNE = ones(size(Tc));  iVyNE(1:end-1,:) = NumVyG(2:end  , 2:end  );
%% Assembly of the sparse matrix
It2m   = [   iTe(:);  iTe(:);  iTe(:);  iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:)  ]';
Jt2m   = [  iVxW(:); iVxE(:); iVyS(:); iVyN(:); iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:); iVySW(:); iVySE(:); iVyNW(:); iVyNE(:)  ]';
Vt2m   = [ c3VxW(:); c3VxE(:); c3VyS(:); c3VyN(:); c3VxSW(:); c3VxSE(:); c3VxNW(:); c3VxNE(:); c3VySW(:); c3VySE(:); c3VyNW(:); c3VyNE(:)  ]';
if SuiteSparse == 1, T2M    = sparse2( It2m, Jt2m, Vt2m );
else                 T2M    = sparse ( It2m, Jt2m, Vt2m );
end
end