function [ eta_0, Txx, Tyy, Txy, deta_ve, deta_vep, pl, eta_Kel, Eii_el, Eii_pwl, Eii_pl, Overstress, Stuff ] = M2Di2_LocalIteration_VEP_pwl_vp_v4( litmax, tol, noisy, Exx, Eyy, Exy, Txxo, Tyyo, Txyo, B_pwl, m_pwl, eta_el, P, phi, C, eta_vp, eta_Kel0, n_vp  )

Eii_el     = 0;
Eii_pwl    = 0;
Eii_pl     = 0;
Overstress = 0;

% Strain rate components and invariant
exx    = Exx;
eyy    = Eyy;
exy    = Exy;
eII    = sqrt(1/2*(exx.^2 + eyy.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
Exx    = Exx + Txxo./2./eta_el;
Eyy    = Eyy + Tyyo./2./eta_el;
Exy    = Exy + Txyo./2./eta_el;
Eii    = sqrt(1/2*(Exx.^2 + Eyy.^2) + Exy.^2);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*eII.^(1./n_pwl-1);

% Yield stress
yield = C + P.*sin(phi);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_0     = 0.5*(eta_up+eta_lo);

if noisy>2, fprintf('Starting Visco-Elastic local iterations:\n'); end


% %%%%%%
% Eii2  = Eii.^2;
% eta_0 = (1./(B_pwl.*Eii2.^m_pwl) + 1./eta_el).^(-1);
% Txx = 2*eta_0.*Exx;
% Tyy = 2*eta_0.*Eyy;
% Txy = 2*eta_0.*Exy; 
% Tii = 2*eta_0.*Eii;
% dQdtxx0 = Txx./2./Tii;
% dQdtyy0 = Tyy./2./Tii;
% dQdtxy0 = Txy./2./Tii;
% %%%%%%

% Local iterations
for lit=1:litmax
    
    % Function evaluation at current effective viscosity
    Tii       = 2*eta_0.*Eii;
    Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
    Eii_vis_0 = Eii_pwl_0;
    r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
    
    % Residual check
    res = max(max(abs(r_eta_0)));
    if lit == 1; res0 = res; end
    if noisy>2, fprintf('It. %02d, r = %2.2e\n', lit, res/res0); end
    if res/res0 < tol
        break;
    end
    
    % Exact derivative
    drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_0;
    
    % Update viscosity
    eta_0  = eta_0 - r_eta_0 ./ drdeta;
    
end

% Update individual deviatoric stress components
eta_ve = eta_0; % save eta trial
Txx = 2*eta_0.*Exx;
Tyy = 2*eta_0.*Eyy;
Txy = 2*eta_0.*Exy;

% For analytical Jacobian --- VE  --- ViscosityDerivativesTest.py
detadTxx     = -    C_pwl.*Tii.^(n_pwl - 3).*Txx.*(n_pwl - 1).*eta_0.^2;
detadTyy     = -    C_pwl.*Tii.^(n_pwl - 3).*Tyy.*(n_pwl - 1).*eta_0.^2;
detadTxy     = -2.0*C_pwl.*Tii.^(n_pwl - 3).*Txy.*(n_pwl - 1).*eta_0.^2;

% detadTxx     = -2.0*C_pwl.*Tii.^(n_pwl - 1).*Txx.*(n_pwl/2 - 1/2)./(Tii.^2.*eta_0.^-2);
% detadTyy     = -2.0*C_pwl.*Tii.^(n_pwl - 1).*Tyy.*(n_pwl/2 - 1/2)./(Tii.^2.*eta_0.^-2);
% detadTxy     = -4.0*C_pwl.*Tii.^(n_pwl - 1).*Txy.*(n_pwl/2 - 1/2)./(Tii.^2.*eta_0.^-2);
deta_ve.dExx = detadTxx .* 2.*eta_0 ./ (1 - 2.*detadTxx.*(Exx - Eyy + 2*Exy.*Exy./Exx));
deta_ve.dEyy = detadTyy .* 2.*eta_0 ./ (1 - 2.*detadTyy.*(Eyy - Exx + 2*Exy.*Exy./Eyy));
deta_ve.dExy = detadTxy .* 2.*eta_0 ./ (1 - 2.*detadTxy.*(Exy + 1/2*Exx./Exy.*( Exx - Eyy) ));
deta_ve.dExx(isinf(deta_ve.dExx)) = 0; deta_ve.dExx(isnan(deta_ve.dEyy)) = 0;
deta_ve.dEyy(isinf(deta_ve.dEyy)) = 0; deta_ve.dEyy(isnan(deta_ve.dEyy)) = 0;
deta_ve.dExy(isinf(deta_ve.dExy)) = 0; deta_ve.dExy(isnan(deta_ve.dExy)) = 0;

% Plasticity
X      = eta_0./eta_el;
h_phi  = 0;
F      = Tii - yield;
pl     = F >= 0;
dQdtxx = Txx./2./Tii;
dQdtyy = Tyy./2./Tii;
dQdtxy = Txy./1./Tii;
gdot   = zeros(size(Txx));
eII            = sqrt(1/2*(exx.^2 + eyy.^2) + exy.^2);
eta_Kel        = zeros(size(Txx));
eta_Kel(pl==1) = eta_Kel0*(eII(pl==1)).^(1/n_vp-1);
gdot(pl==1)    = F(pl==1)./(eta_0(pl==1) + eta_Kel(pl==1) + h_phi);
% gdot(pl==1)    = 1e-3*eII(pl==1);
res0          = max(F(:));
Fc            = F;
pl            = F>0.0;
if noisy>2, fprintf('Starting ViscoPlastic local iterations - F trial = %2.2e:\n', res0); end
if sum(pl(:)>0)
    % Plastic strain rates
    for lit=1:litmax
        % Evaluate F
        eta_Kel(pl==1) = eta_Kel0 .* gdot(pl==1) .^ (1/n_vp-1);
        Fc(pl==1)      = Tii(pl==1) - eta_0(pl==1).*gdot(pl==1) - yield(pl==1) - eta_Kel(pl==1).*gdot(pl==1);
        % Residual check
        res = max(max(abs(Fc(pl))));
        Fc_pl = Fc(pl==1);
        if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e, max(F) = %2.2e\n', lit, res/res0, res, max(Fc_pl(:))); end
        if res/res0 < tol || res < tol
            break;
        end
        % Exact derivative
        dFdgdot = -eta_Kel0.*gdot.^(1/n_vp)./(gdot.*n_vp) - eta_0;
        % Update viscosity
        gdot(pl==1)  = gdot(pl==1) - Fc(pl==1) ./ dFdgdot(pl==1);
        gdot         = real(gdot); % MATLAB tends to spit out complex numbers
    end
end
exx_pl = gdot.*dQdtxx;
eyy_pl = gdot.*dQdtyy;
exy_pl = gdot.*dQdtxy*(1/2);
% dE2pl  = sqrt( 1/2*(exx_pl*dt).^2 + 1/2*(eyy_pl*dt).^2 + (exy_pl*dt).^2 );
Txx    = 2.*eta_0.*(exx-exx_pl) +  X.*Txxo;
Tyy    = 2.*eta_0.*(eyy-eyy_pl) +  X.*Tyyo;
Txy    = 2.*eta_0.*(exy-exy_pl) +  X.*Txyo;
Tii    = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + Txy.^2);
eta_0(pl==1)  = Tii(pl==1)./2./Eii(pl==1);

if noisy>2
    
    % Check yield 1 after correction
    F1     = Tii - yield - gdot.*eta_Kel;

    % Double check wether effective viscosity is computed accurately
    Txx = 2*eta_0.*Exx;
    Tyy = 2*eta_0.*Eyy;
    Txy = 2*eta_0.*Exy;
    Tii = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + Txy.^2);
    Tii(pl==1)    = 2*eta_0(pl==1).*Eii(pl==1);
    F2  = Tii - yield - gdot.*eta_Kel;
    
    % Correct power-law viscosity
    eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
    
    % 1: strain rate components
    exx_el  = (Txx-Txxo)./2./eta_el;
    exx_pwl = Txx./2./eta_pwl;
    exx_pl  = gdot.*dQdtxx;
    exx_net = norm(exx-exx_el-exx_pwl-exx_pl);
    
    eyy_el  = (Tyy-Tyyo)./2./eta_el;
    eyy_pwl = Tyy./2./eta_pwl;
    eyy_pl  = gdot.*dQdtyy;
    eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl);
    
    exy_el  = (Txy-Txyo)./2./eta_el;
    exy_pwl = Txy./2./eta_pwl;
    exx_pl  = gdot.*dQdtyy;
    exy_net = norm(exy-exy_el-exy_pwl-exy_pl);
    
    if noisy>2
        Overstress  = Tii - yield;
        fprintf('F trial = %2.2e --- F corr1 = %2.2e --- F corr2 = %2.2e --- Over S. = %2.2e\n', max(F(:)), max(F1(:)), max(F2(:)), max(Overstress(:)))
        fprintf('net exx = %2.2e --- net eyy = %2.2e --- net exy = %2.2e\n', norm(exx_net),norm(eyy_net),norm(exy_net))
    end
    
    % 2: invariants
    Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + exy_el .^2);
    Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + exy_pwl.^2);
    Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + exy_pl .^2);
    
end

% For analytical Jacobian --- VEP --- ViscosityDerivativesTest.py
deta_vep.dExx = deta_ve.dExx;
deta_vep.dEyy = deta_ve.dEyy;
deta_vep.dExy = deta_ve.dExy;
yield         = yield + gdot.*eta_Kel;
dFdExx        =   Exx.*eta_ve./Eii + 2.*Eii.*deta_ve.dExx;
dFdEyy        =   Eyy.*eta_ve./Eii + 2.*Eii.*deta_ve.dEyy;
dFdExy        = 2*Exy.*eta_ve./Eii + 2.*Eii.*deta_ve.dExy;  
dFdP          = -sin(phi);
dlamdExx      = n_vp.*(dFdExx - gdot .*deta_ve.dExx)./(n_vp.*eta_ve + eta_Kel + h_phi) ;
dlamdEyy      = n_vp.*(dFdEyy - gdot .*deta_ve.dEyy)./(n_vp.*eta_ve + eta_Kel + h_phi) ;
dlamdExy      = n_vp.*(dFdExy - gdot .*deta_ve.dExy)./(n_vp.*eta_ve + eta_Kel + h_phi) ;
dlamdP        = n_vp.*(dFdP                        )./(n_vp.*eta_ve + eta_Kel + h_phi) ;

deta_vp_dExx  = eta_Kel./gdot.*dlamdExx.*(1./n_vp-1);
deta_vp_dEyy  = eta_Kel./gdot.*dlamdEyy.*(1./n_vp-1);
deta_vp_dExy  = eta_Kel./gdot.*dlamdExy.*(1./n_vp-1);
deta_vp_dP    = eta_Kel./gdot.*dlamdP  .*(1./n_vp-1);

deta_vep_dExx = -1/2*Exx.*yield./(2*Eii.^3) + (eta_Kel.*dlamdExx + gdot.*deta_vp_dExx)./(2*Eii);
deta_vep_dEyy = -1/2*Eyy.*yield./(2*Eii.^3) + (eta_Kel.*dlamdEyy + gdot.*deta_vp_dEyy)./(2*Eii);
deta_vep_dExy =     -Exy.*yield./(2*Eii.^3) + (eta_Kel.*dlamdExy + gdot.*deta_vp_dExy)./(2*Eii);
deta_vep_dP   = (sin(phi) + eta_Kel.*dlamdP + gdot.*deta_vp_dP ) ./ (2*Eii); 

deta_vep.dExx(pl==1) = deta_vep_dExx(pl==1);
deta_vep.dEyy(pl==1) = deta_vep_dEyy(pl==1);
deta_vep.dExy(pl==1) = deta_vep_dExy(pl==1);
deta_vep.dP          = zeros(size(Tii));
deta_vep.dP(pl==1)   = deta_vep_dP(pl==1);

deta_vep.dExx(isinf(deta_vep.dExx)) = 0;
deta_vep.dExx(isnan(deta_vep.dExx)) = 0;
deta_vep.dEyy(isinf(deta_vep.dEyy)) = 0;
deta_vep.dEyy(isnan(deta_vep.dEyy)) = 0;
deta_vep.dExy(isinf(deta_vep.dExy)) = 0;
deta_vep.dExy(isnan(deta_vep.dExy)) = 0;
deta_vep.dP(isinf(deta_vep.dP))     = 0;
deta_vep.dP(isnan(deta_vep.dP))     = 0;

Stuff.dg     = gdot;
Stuff.dQdtxx = dQdtxx.*pl;
Stuff.dQdtyy = dQdtyy.*pl;
Stuff.dQdtxy = dQdtxy.*pl;
Stuff.Jii    = Tii.^2;
Stuff.Txx    = Txx;
Stuff.Tyy    = Tyy;
Stuff.Txy    = Txy;
Stuff.eta_t  = eta_ve;

end