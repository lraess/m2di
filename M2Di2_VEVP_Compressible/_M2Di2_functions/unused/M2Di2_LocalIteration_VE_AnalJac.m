function [ eta_0, Txx, Tyy, Txy, detadExx, detadEyy, detadExy, Eii_pwl2, exx_pwl, eta_pwl, deta_pwl_dexx ] = M2Di2_LocalIteration_VE_AnalJac( litmax, tol, noisy, Exx, Eyy, Exy, Txxo, Tyyo, Txyo, B_pwl, m_pwl, eta_el )

% Strain rate components and invariant
exx    = Exx;
eyy    = Eyy;
exy    = Exy;
eII    = sqrt(1/2*(exx.^2 + eyy.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
Exx    = Exx + Txxo./2./eta_el;
Eyy    = Eyy + Tyyo./2./eta_el;
Exy    = Exy + Txyo./2./eta_el;
Eii    = sqrt(1/2*(Exx.^2 + Eyy.^2) + Exy.^2);
%eta_pl  = (Cref(h) + P0ref*sin(phiref(h))) / (2*Eii);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*eII.^(1./n_pwl-1);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_0     = 0.5*(eta_up+eta_lo);

if noisy>0, fprintf('Starting local iterations:\n'); end

        
        for lit = 1:litmax
            
            % Function evaluation at current effective viscosity
            Tii       = 2*eta_0.*Eii;
            Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);
            Eii_vis_0 = Eii_pwl_0;
            r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
            
            % Residual check
            
            res = max(max(abs(r_eta_0)));
            if lit == 1; res0 = res; end
            if noisy>0, fprintf('It. %02d, r = %2.2e\n', lit, res/res0); end
            if res/res0 < tol
                break;
            end
            
            % Exact derivative (Newton)
            drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_0;
            
            % Update viscosity
            eta_0  = eta_0 - r_eta_0 ./ drdeta;
            
        end
        
     
    % Update stress
    Txx  = 2*eta_0.*Exx;
    Tyy  = 2*eta_0.*Eyy;
    Txy  = 2*eta_0.*Exy;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%
    
    % Check partitioning:
    
    Eii_pl = 0;
    
    % 1: components
    exx_el  = (Txx-Txxo)./2./eta_el;
    exx_pwl = Eii_pwl_0.*Exx./Eii;
    exx_pl  = Eii_pl.*Exx./Eii;
    exx_net = norm(exx-exx_el-exx_pwl-exx_pl)
    
    eyy_el  = (Tyy-Tyyo)./2./eta_el;
    eyy_pwl = Eii_pwl_0.*Eyy./Eii;
    eyy_pl  = Eii_pl.*Eyy./Eii;
    eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl)
    
    exy_el  = (Txy-Txyo)./2./eta_el;
    exy_pwl = Eii_pwl_0.*Exy./Eii;
    exy_pl  = Eii_pl.*Exy./Eii;
    exy_net = norm(exy-exy_el-exy_pwl-exy_pl)
    
    % 2: invariant
%     Tii0    = sqrt(1/2*(Txxo.^2 + Tyyo.^2) + Txyo.^2);
%     Eii_el  = (Tii - Tii0)./2./eta_el;
    Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + exy_el .^2);
    Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + exy_pwl.^2);
    Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + exy_pl .^2);
    eII_net = norm(eII - Eii_el - Eii_pwl - Eii_pl)
    
    eta_pwl = B_pwl.*Eii_pwl.^(1./n_pwl-1);
    eta_1   = 1./(1./eta_el + 1./eta_pwl);
    eta_err = norm(eta_0 - eta_1)
    
    Eii_pwl_err = norm(Eii_pwl-Eii_pwl_0)
    
    detadExx = -(1/2 - 0.5./n_pwl).*(1.0*exx - 1.0*exx_el).*(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2).^(1/2 - 0.5./n_pwl)./(B_pwl.*(1./eta_el + (0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2).^(1/2 - 0.5./n_pwl)./B_pwl).^2.*(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2));
    detadEyy = 0;
    detadExy = 0;
    
    deta_pwl_dexx = B_pwl.*(-1/2 + 0.5./n_pwl).*(1.0*exx - 1.0*exx_el).*(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2).^(-1/2 + 0.5./n_pwl)./(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2);
    detadExx  = (deta_pwl_dexx./eta_pwl.^2) .* (eta_0.^2);
    
    deta_pwl_dexx = B_pwl.*(exx_pwl.*sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2)./(Txxo./(2*eta_el) + exx)).^(-1 + 1.0./n_pwl).*(-1 + 1.0./n_pwl).*(Txxo./(2*eta_el) + exx).*(exx_pwl.*(0.25*Txxo./eta_el + 0.5*exx)./((Txxo./(2*eta_el) + exx).*sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2)) - exx_pwl.*sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2)./(Txxo./(2*eta_el) + exx).^2)./(exx_pwl.*sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2));
%    detadExx =  -(1/2 - 0.5./n_pwl).*(1.0*exx - 1.0*exx_el).*(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2).^(1/2 - 0.5./n_pwl)./(B_pwl.*(1./eta_el + (0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2).^(1/2 - 0.5./n_pwl)./B_pwl).^2.*(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2));
%   

    deta_pwl_dexx_pwl = 1.0*B_pwl.*exx_pwl.*(-1/2 + 0.5./n_pwl).*(0.5*exx_pwl.^2 + exy_pwl + 0.5*eyy_pwl.^2).^(-1/2 + 0.5./n_pwl)./(0.5*exx_pwl.^2 + exy_pwl + 0.5*eyy_pwl.^2);
%     dexx_pwl_dexx = Eii_pwl.*(-0.25*Txxo./eta_el - 0.5*exx).*(Txxo./(2*eta_el) + exx)./(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2).^(3/2) + Eii_pwl./sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2);

    dexx_pwl_dexx =(0.5*exx - 0.5*exx_el).*(Txxo./(2*eta_el) + exx)./(sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2).*sqrt(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2)) + (-0.25*Txxo./eta_el - 0.5*exx).*(Txxo./(2*eta_el) + exx).*sqrt(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2)./(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2).^(3/2) + sqrt(0.5*(exx - exx_el).^2 + (exy - exy_el).^2 + 0.5*(eyy - eyy_el).^2)./sqrt(Exy + 0.5*Eyy.^2 + 0.5*(Txxo./(2*eta_el) + exx).^2);
    
%     deta_pwl_dexx = deta_pwl_dexx_pwl.*dexx_pwl_dexx;
    
    Eii_pwl2 = Eii_pwl.^2;
    
    detadexx_pwl = -1.0*exx_pwl.*(1/2 - 0.5./n_pwl).*(0.5*exx_pwl.^2 + exy_pwl.^2 + 0.5*eyy_pwl.^2).^(1/2 - 0.5./n_pwl)./(B_pwl.*(1./eta_el + (0.5*exx_pwl.^2 + exy_pwl.^2 + 0.5*eyy_pwl.^2).^(1/2 - 0.5./n_pwl)./B_pwl).^2.*(0.5*exx_pwl.^2 + exy_pwl.^2 + 0.5*eyy_pwl.^2));
    detadExx = detadexx_pwl .* dexx_pwl_dexx;
   
%    detadExx =    B_pwl.*Eii_pwl2.^(m_pwl-1).*m_pwl.*exx_pwl.*eta_el.^2 ./ (eta_el + B_pwl.*Eii_pwl2.^m_pwl).^2;

% % Local iterations
% for lit=1:litmax
%     
%     % Function evaluation at current effective viscosity
%     Tii       = 2*eta_0.*Eii;
%     Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
%     Eii_vis_0 = Eii_pwl_0;
%     r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
%     
%     % Residual check
%     res = max(abs(r_eta_0(:)));
%     if lit == 1; res0 = res; end
%     if noisy>0, fprintf('It. %02d, r = %2.2e\n', lit, res/res0); end
%     if res/res0 < tol
%         break;
%     end
%     
%     % Exact derivative
%     drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_0;
%     
%     % Update viscosity
%     eta_0  = eta_0 - r_eta_0 ./ drdeta;
%     
% end
% 
% % Update individual deviatoric stress components
% Txx = 2*eta_0.*Exx;
% Tyy = 2*eta_0.*Eyy;
% Txy = 2*eta_0.*Exy;
% Eii_pwl2 = Eii_pwl_0.^2;
% eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
% eta_pwl1 = B_pwl.*Eii_pwl_0.^(2*m_pwl);
% eta_pwl2 = (2.^(-2.*m_pwl).*B_pwl.*Tii.^(2.*m_pwl)) .^(1./(2.*m_pwl+1));
% diffeta = eta_0 - (1./eta_el + 1./eta_pwl).^(-1);
% 
% % Strain rates
% exx_el  = (Txx - Txxo) ./ (2*eta_el);
% eyy_el  = (Tyy - Tyyo) ./ (2*eta_el);
% exy_el  = (Txy - Txyo) ./ (2*eta_el);
% Eii_el_0= sqrt(1/2*exx_el.^2 + 1/2*eyy_el.^2 + exy_el.^2);
% 
% exx_pwl = Eii_pwl_0.*exx./eII;
% eyy_pwl = Eii_pwl_0.*eyy./eII;
% exy_pwl = Eii_pwl_0.*exy./eII;
% exx_net = exx-exx_el-exx_pwl;
% eyy_net = eyy-eyy_el-eyy_pwl;
% exy_net = exy-exy_el-exy_pwl;
% 
% exx_pwl1 = exx-exx_el;
% eyy_pwl1 = eyy-eyy_el;
% exy_pwl1 = exy-exy_el;
% 
% eII_pwl1  = sqrt(1/2*(exx_pwl1.^2 + eyy_pwl1.^2) + exy_pwl1.^2);
% 
% Eii2 = Eii.^2;
% dexx_pwldexx     = (1  - eta_0./eta_el);
% dexx_pwldexx     = 2.*Eii_pwl_0./eII - 4.*exx.^2.*Eii_pwl_0./eII.^(3/2);
% deta_pwldexx_pwl = exx_pwl1.*m_pwl.*B_pwl.*Eii_pwl2.^(m_pwl-1);
% deta_pwldexx     = dexx_pwldexx .* deta_pwldexx_pwl;
% 
% deta_pwldexx    = -2*eta_pwl .*m_pwl.*exx_pwl1.*eta_el.*(-eta_el + eta_0) ./ (eta_el.^2.*exx_pwl1.^2+eta_el.^2.*eyy_pwl1.^2+2.*eta_el.^2.*exy_pwl1.^2+ 2.*eta_pwl.* m_pwl.*exx_pwl1.*exx.*eta_el + eta_pwl .*m_pwl.*exx_pwl1.*Txxo);
% 
% deta_0deta_pwl   = eta_0.^2 .* eta_pwl.^(-2);
% 
% 
% Eii_pwl2         = Eii_pwl_0.^2;
% deta_pwldexx_pwl1 = eta_pwl.*m_pwl.*exx_pwl1 ./ Eii_pwl2;
% detadExx         = -2.*deta_pwldexx_pwl1.*(-eta_el+eta_0).*eta_el./(2.*1./(eta_0).^2.*eta_pwl.^2.*eta_el.^2+2.*deta_pwldexx_pwl1.*exx.*eta_el+deta_pwldexx_pwl1.*Txxo);
% 
% detadExx0        = -2.*eta_pwl.*m_pwl.*exx_pwl1.*eta_el.*(-eta_el+eta_0)./(eta_el.^2.*exx_pwl1.^2+eta_el.^2.*eyy_pwl1.^2+2.*eta_el.^2.*exy_pwl1.^2+2.*eta_pwl.*eta_el.*exx_pwl1.^2+2.*eta_pwl.*eta_el.*eyy_pwl1.^2+4.*eta_pwl.*eta_el.*exy_pwl1.^2+eta_pwl.^2.*exx_pwl1.^2+eta_pwl.^2.*eyy_pwl1.^2+2.*eta_pwl.^2.*exy_pwl1.^2+2.*eta_pwl.*m_pwl.*exx_pwl1.*exx.*eta_el+eta_pwl.*m_pwl.*exx_pwl1.*Txxo);
% 
% detadExx1         = eta_0.^2 .* (deta_pwldexx./eta_pwl.^2);
% 
% detadExx2         = (deta_pwldexx.*eta_el.^2) ./ (eta_pwl + eta_el).^2;
% 
% detadExx3         = deta_0deta_pwl .* deta_pwldexx;
% 
% detadExx =   (m_pwl.*(exx_pwl)) .* eta_0.^2 ./ ( eta_pwl.*Eii_pwl2);
% detadEyy =   (m_pwl.*(eyy-eyy_el)) .* eta_0.^2 ./ ( eta_pwl.*Eii_pwl2);
% detadExy = 2*(m_pwl.*(exy-exy_el)) .* eta_0.^2 ./ ( eta_pwl.*Eii_pwl2);
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Tii0   = sqrt(1/2*(Txxo.^2 + Tyyo.^2) + Txyo.^2);
% Eii_el_0 = (Tii - Tii0) ./ 2 ./ eta_el;
% 
% norm( eII - Eii_pwl_0 - Eii_el_0)
% norm( exx - exx_pwl - exx_el)


aaa = 1


% figure(89), clf
% subplot(311)
% imagesc(eta_pwl), colorbar
% subplot(312)
% imagesc(eta_pwl1), colorbar
% subplot(313)
% imagesc(eta_pwl2), colorbar
% drawnow

% figure(89), clf
% subplot(211)
% imagesc(eta_0), colorbar
% subplot(212)
% imagesc(diffeta), colorbar
% drawnow

% figure(90), clf
% subplot(311)
% imagesc(exx_net), colorbar
% subplot(312)
% imagesc(eyy_net), colorbar
% subplot(313)
% imagesc(eII_pwl1-Eii_pwl_0), colorbar
% drawnow

end