function [KPf] = M2Di2_DiffusionAssembly_v4_steady_grad_plasticity( c, pl, fact, NumPf, BC, dx, dy, dt, nx, ny, Pf, SuiteSparse )
%% Block PF (Pressure Lower) with variable coeff

plC   = pl;
plW   = zeros(size(pl)); plW(2:end-0,:) = pl(1:end-1,:);
plE   = zeros(size(pl)); plE(1:end-1,:) = pl(2:end-0,:);
plS   = zeros(size(pl)); plS(:,2:end-0) = pl(:,1:end-1);
plN   = zeros(size(pl)); plN(:,1:end-1) = pl(:,2:end-0);

k11_W = c;% kx(1:end-1,:);
k11_E = c;%kx(2:end-0,:);
k22_S = c;%ky(:,1:end-1);
k22_N = c;%ky(:,2:end-0);
iPf   = NumPf;
iPfW  = ones(size(Pf)); iPfW(2:end  ,:) = NumPf(1:end-1, :     );
iPfE  = ones(size(Pf)); iPfE(1:end-1,:) = NumPf(2:end  , :     );
iPfS  = ones(size(Pf)); iPfS(:,2:end  ) = NumPf( :     ,1:end-1);
iPfN  = ones(size(Pf)); iPfN(:,1:end-1) = NumPf( :     ,2:end  );
NeuW  = BC.PfNeuW;
NeuE  = BC.PfNeuE; 
NeuS  = BC.PfNeuS;
NeuN  = BC.PfNeuN;
DirW  = BC.PfDirW; 
DirE  = BC.PfDirE; 
DirS  = BC.PfDirS;
DirN  = BC.PfDirN;
% cPfC = fact.*( (k22_N.*(-DirN - 1).*(NeuN - 1)./dy - k22_S.*(DirS + 1).*(NeuS - 1)./dy)./dy + (k11_E.*(-DirE - 1).*(NeuE - 1)./dx - k11_W.*(DirW + 1).*(NeuW - 1)./dx)./dx );
% cPfE = fact.*(k11_E.*(1 - DirE).*(NeuE - 1)./dx.^2 );
% cPfW = fact.*(-k11_W.*(DirW - 1).*(NeuW - 1)./dx.^2 );
% cPfS = fact.*(-k22_S.*(DirS - 1).*(NeuS - 1)./dy.^2 );
% cPfN = fact.*(k22_N.*(1 - DirN).*(NeuN - 1)./dy.^2 );

cPfC = fact.*plC.*((k22_N.*(1 - NeuN).*(-DirN - 1).*(plN - 1)./dy - k22_S.*(1 - NeuS).*(DirS + 1).*(plS - 1)./dy)./dy + (k11_E.*(1 - NeuW).*(-DirE - 1).*(plE - 1)./dx - k11_W.*(1 - NeuW).*(DirW + 1).*(plW - 1)./dx)./dx) - plC + 1;
cPfW = -fact.*k11_W.*plC.*(1 - NeuW).*(DirW - 1).*(plW - 1)./dx.^2;
cPfE = fact.*k11_E.*plC.*(1 - DirE).*(1 - NeuW).*(plE - 1)./dx.^2;
cPfS = -fact.*k22_S.*plC.*(1 - NeuS).*(DirS - 1).*(plS - 1)./dy.^2;
cPfN = fact.*k22_N.*plC.*(1 - DirN).*(1 - NeuN).*(plN - 1)./dy.^2;

I     = [  iPf(:);   iPf(:);  iPf(:);  iPf(:);  iPf(:); ]';
J     = [  iPf(:);  iPfW(:); iPfS(:); iPfE(:); iPfN(:); ]';
V     = [ cPfC(:);  cPfW(:); cPfS(:); cPfE(:); cPfN(:); ]';
if SuiteSparse==1, KPf = sparse2(I,J,V, nx*ny,nx*ny);
else,              KPf =  sparse(I,J,V, nx*ny,nx*ny); end
end