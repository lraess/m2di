function [D] = M2Di2_EvaluateConsistenTangent_DP_VM_comp1(plc,dgc,Txxc,Tyyc,Tzzc,Txyc,Ptc,Jiic,dQdtxxc,dQdtyyc,dQdtzzc,dQdtxyc,dQdPc,psic,phic,etac,plv,dgv,Txxv,Tyyv,Tzzv,Txyv,Ptv,Jiiv,dQdtxxv,dQdtyyv,dQdtzzv,dQdtxyv,dQdPv,psiv,phiv,etav,Newton,Kc,Kv,dt,comp)
dQdtxx = dQdtxxc; dQdtyy = dQdtyyc; dQdtzz = dQdtzzc; dQdtxy = dQdtxyc;  dQdP = dQdPc;
dFdtxx = dQdtxxc; dFdtyy = dQdtyyc; dFdtzz = dQdtzzc; dFdtxy = dQdtxyc;  dFdP = -sin(phic).*plc;
D11 = 2*etac; D22 = 2*etac; D33 = 1*etac; D55 = 2*etac; 
if comp==0, D44 = 1         ; end
if comp==1, D44 = (Kc*dt);    end


Txx = Txxc; Tyy = Tyyc; Txy = Txyc; J2 = Jiic; Tzz = Tzzc; Tii = sqrt(J2);
dQdtxx = 0.5*Txx./Tii.*plc;
dQdtyy = 0.5*Tyy./Tii.*plc;
dQdtxy = Txy./Tii.*plc;
dQdtzz = 0.5*Tzz./Tii.*plc;
dQdP   = -sin(psic).*plc;
d2Qdtxxdtxx = 0.5./Tii - 0.25*Txx.^2./Tii.^3;
d2Qdtxxdtyy = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtxxdtxy = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxxdtzz = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtyydtxx = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtyydtyy = 0.5./Tii - 0.25*Tyy.^2./Tii.^3;
d2Qdtyydtxy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtyydtzz = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtxydtxx = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxydtyy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtxydtxy = 1./Tii - Txy.^2./Tii.^3;
d2Qdtxydtzz = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtxx = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtzzdtyy = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtzzdtxy = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtzz = 0.5./Tii - 0.25*Tzz.^2./Tii.^3;

% txx = Txxc; tyy = Tyyc; txy = Txyc; J2 = Jiic; tzz = Tzzc;
% d2Qdtxxdtxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
% d2Qdtxxdtyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
% d2Qdtxxdtzz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
% d2Qdtxxdtxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
% d2Qdtyydtxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
% d2Qdtyydtyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
% d2Qdtyydtzz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
% d2Qdtyydtxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
% d2Qdtzzdtxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
% d2Qdtzzdtyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
% d2Qdtzzdtzz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
% d2Qdtzzdtxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
% d2Qdtxydtxx = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
% d2Qdtxydtyy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
% d2Qdtxydtzz = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
% d2Qdtxydtxy = -0.100e1 .* J2 .^ (-0.3e1 / 0.2e1) .* txy .^ 2 + J2 .^ (-0.1e1 / 0.2e1);



dlam = dgc;
if Newton == 1, dlam = 0*dlam; end

M11 = 1 + dlam .* D11 .* d2Qdtxxdtxx;
M12 = dlam .* D11 .* d2Qdtxxdtyy;
M13 = dlam .* D11 .* d2Qdtxxdtxy;
M14 = zeros(size(D11));
M15 = dlam .* D11 .* d2Qdtxxdtzz;
M21 = dlam .* D11 .* d2Qdtyydtxx;
M22 = 1 + dlam .* D11 .* d2Qdtyydtyy;
M23 = dlam .* D11 .* d2Qdtyydtxy;
M24 = zeros(size(D11));
M25 = dlam .* D11 .* d2Qdtyydtzz;
M31 = dlam .* D33 .* d2Qdtxydtxx;
M32 = dlam .* D33 .* d2Qdtxydtyy;
M33 = 1 + dlam .* D33 .* d2Qdtxydtxy;
M34 = zeros(size(D11));
M35 = dlam .* D33 .* d2Qdtxydtzz;
M41 = zeros(size(D11));
M42 = zeros(size(D11));
M43 = zeros(size(D11));
M44 = ones(size(D11));
M45 = zeros(size(D11));
M51 = dlam .* D11 .* d2Qdtzzdtxx;
M52 = dlam .* D11 .* d2Qdtzzdtyy;
M53 = dlam .* D11 .* d2Qdtzzdtxy;
M54 = zeros(size(D11));
M55 = 1 + dlam .* D11 .* d2Qdtzzdtzz;
%[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );
[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );
Mi44 = ones(size(D11));
% Mi44 = ones(size(D11));% D44.^-1;
% dQdP = dQdP.*Mi44;

% d2QdtxxdP =0;
% d2QdtyydP = 0;
% d2QdtzzdP = 0;
% d2QdtxydP = 0;
% d2QdPdtxx =0;
% d2QdPdtyy =0;
% d2QdPdtxy =0;
% d2QdPdtzz =0;
% d2QdPdP =0;
% M11 = D11.*d2Qdtxxdtxx.*dlam + 1;
% M12 = D11.*d2Qdtxxdtyy.*dlam;
% M13 = D11.*d2Qdtxxdtxy.*dlam;
% M14 = D11.*d2QdtxxdP.*dlam;
% M15 = D11.*d2Qdtxxdtzz.*dlam;
% M21 = D22.*d2Qdtyydtxx.*dlam;
% M22 = D22.*d2Qdtyydtyy.*dlam + 1;
% M23 = D22.*d2Qdtyydtxy.*dlam;
% M24 = D22.*d2QdtyydP.*dlam;
% M25 = D22.*d2Qdtyydtzz.*dlam;
% M31 = D33.*d2Qdtxydtxx.*dlam;
% M32 = D33.*d2Qdtxydtyy.*dlam;
% M33 = D33.*d2Qdtxydtxy.*dlam + 1;
% M34 = D33.*d2QdtxydP.*dlam;
% M35 = D33.*d2Qdtxydtzz.*dlam;
% M41 = D44.*d2QdPdtxx.*dlam;
% M42 = D44.*d2QdPdtyy.*dlam;
% M43 = D44.*d2QdPdtxy.*dlam;
% M44 = D44.*d2QdPdP.*dlam + 1;
% M45 = D44.*d2QdPdtzz.*dlam;
% M51 = D55.*d2Qdtzzdtxx.*dlam;
% M52 = D55.*d2Qdtzzdtyy.*dlam;
% M53 = D55.*d2Qdtzzdtxy.*dlam;
% M54 = D55.*d2QdtzzdP.*dlam;
% M55 = D55.*d2Qdtzzdtzz.*dlam + 1;
% [ Mi11,Mi12,Mi13,Mi14,Mi15, Mi21,Mi22,Mi24,Mi23,Mi25, Mi31,Mi32,Mi33,Mi34,Mi35, Mi41,Mi42,Mi43,Mi44,Mi45, Mi51,Mi52,Mi53,Mi54,Mi55  ] = M2Di2_inverse_5x5( M11,M12,M13,M14,M15, M21,M22,M23,M24,M25, M31,M32,M33,M34,M35, M41,M42,M43,M44,M45, M51,M52,M53,M54,M55  );


% Mi41 = 0;
% Mi42 = 0;
% Mi43 = 0;
% Mi14 = 0;
% Mi24 = 0;
% Mi34 = 0;
% Mi54 = 0;
% Mi45 = 0;
% % % Consistent tangent - deviator
% den = D11.*dQdtxx.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + D22.*dQdtyy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + D33.*dQdtxy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + D44.*dQdP.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + D55.*dQdtzz.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz);
% den  (plc==0) = 1;
% 
% D.D11c = D11.*(-D22.*Mi12.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi11.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D12c = D22.*(-D11.*Mi11.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi12.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D13c = D33.*(-D11.*Mi11.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi12.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi13.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D14c = D44.*(-D11.*Mi11.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi12.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi14.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D15c = D55.*(-D11.*Mi11.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi12.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi15.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D21c = D11.*(-D22.*Mi22.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi21.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D22c = D22.*(-D11.*Mi21.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi22.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D23c = D33.*(-D11.*Mi21.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi22.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi23.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D24c = D44.*(-D11.*Mi21.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi22.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi24.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D25c = D55.*(-D11.*Mi21.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi22.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi25.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D31c = D11.*(-D22.*Mi32.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi31.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D32c = D22.*(-D11.*Mi31.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi32.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D33c = D33.*(-D11.*Mi31.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi32.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi33.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D34c = D44.*(-D11.*Mi31.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi32.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi34.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D35c = D55.*(-D11.*Mi31.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi32.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi35.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D41c = D11.*(-D22.*Mi42.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi41.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D42c = D22.*(-D11.*Mi41.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi42.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D43c = D33.*(-D11.*Mi41.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi42.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi43.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D44c = D44.*(-D11.*Mi41.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi42.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi44.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D45c = D55.*(-D11.*Mi41.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi42.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi45.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D51c = D11.*(-D22.*Mi52.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi51.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D52c = D22.*(-D11.*Mi51.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi52.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D53c = D33.*(-D11.*Mi51.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi52.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi53.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D54c = D44.*(-D11.*Mi51.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi52.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi54.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D55c = D55.*(-D11.*Mi51.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi52.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi55.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;

% Old
den  = (dFdtxx.*Mi11+dFdtyy.*Mi21+dFdtxy.*Mi31+dFdtzz.*Mi51).*D11.*dQdtxx + (dFdtxx.*Mi12+dFdtyy.*Mi22+dFdtxy.*Mi32+dFdtzz.*Mi52).*D11.*dQdtyy + (dFdtxx.*Mi13+dFdtyy.*Mi23+dFdtxy.*Mi33+dFdtzz.*Mi53).*D33.*dQdtxy + dFdP.*Mi44.*D44.*dQdP+(dFdtxx.*Mi15+dFdtyy.*Mi25+dFdtxy.*Mi35+dFdtzz.*Mi55).*D55.*dQdtzz;
den  (plc==0) = 1;

D.D11c = Mi11 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi12 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi13 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi15 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D12c = -Mi11 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi12 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi13 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi15 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D13c = -Mi11 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi12 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi13 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi15 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D14c = -Mi11 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi12 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi13 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi15 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44;
D.D15c = -Mi11 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi12 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi13 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi15 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D21c = Mi21 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi22 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi23 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi25 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D22c = -Mi21 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi22 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi23 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi25 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D23c = -Mi21 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi22 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi23 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi25 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D24c = -Mi21 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi22 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi23 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi25 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44;
D.D25c = -Mi21 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi22 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi23 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi25 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D31c = Mi31 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D32c = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi32 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D33c = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi33 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D34c = -Mi31 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi32 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi33 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi35 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44;
D.D35c = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi35 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D41c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi11 .* D11 + dQdP .* dFdtyy .* Mi21 .* D11 + dQdP .* dFdtxy .* Mi31 .* D11 + dQdP .* dFdtzz .* Mi51 .* D11);
D.D42c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi12 .* D11 + dQdP .* dFdtyy .* Mi22 .* D11 + dQdP .* dFdtxy .* Mi32 .* D11 + dQdP .* dFdtzz .* Mi52 .* D11);
D.D43c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi13 .* D33 + dQdP .* dFdtyy .* Mi23 .* D33 + dQdP .* dFdtxy .* Mi33 .* D33 + dQdP .* dFdtzz .* Mi53 .* D33);
D.D44c =  Mi44 .* D44 .* (1 - 1 ./ den .* dFdP .* Mi44 .* D44 .* dQdP); % [ Pa.s ]
D.D45c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi15 .* D55 + dQdP .* dFdtyy .* Mi25 .* D55 + dQdP .* dFdtxy .* Mi35 .* D55 + dQdP .* dFdtzz .* Mi55 .* D55);

D.D14c = D.D14c./D44;
D.D24c = D.D24c./D44;
D.D34c = D.D34c./D44;
D.D44c = D.D44c./D44; % [ - ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dQdtxx = dQdtxxv; dQdtyy = dQdtyyv; dQdtzz = dQdtzzv; dQdtxy = dQdtxyv; dQdP = dQdPv;
dFdtxx = dQdtxxv; dFdtyy = dQdtyyv; dFdtzz = dQdtzzv; dFdtxy = dQdtxyv; dFdP = -sin(phiv).*plv;
D11 = 2*etav; D22 = 2*etav; D33 = 1*etav; D55 = 2*etav;  
if comp==0, D44 = 1         ; end
if comp==1, D44 = 1./(Kv*dt); end
if comp==1, D44 = (Kv*dt); end

Txx = Txxv; Tyy = Tyyv; Txy = Txyv; J2 = Jiiv; Tzz = Tzzv; Tii = sqrt(J2);
dQdtxx = 0.5*Txx./Tii.*plv;
dQdtyy = 0.5*Tyy./Tii.*plv;
dQdtxy = Txy./Tii.*plv;
dQdtzz = 0.5*Tzz./Tii.*plv;
dQdP   = -sin(psiv).*plv;
d2Qdtxxdtxx = 0.5./Tii - 0.25*Txx.^2./Tii.^3;
d2Qdtxxdtyy = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtxxdtxy = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxxdtzz = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtyydtxx = -0.25*Txx.*Tyy./Tii.^3;
d2Qdtyydtyy = 0.5./Tii - 0.25*Tyy.^2./Tii.^3;
d2Qdtyydtxy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtyydtzz = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtxydtxx = -0.5*Txx.*Txy./Tii.^3;
d2Qdtxydtyy = -0.5*Txy.*Tyy./Tii.^3;
d2Qdtxydtxy = 1./Tii - Txy.^2./Tii.^3;
d2Qdtxydtzz = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtxx = -0.25*Txx.*Tzz./Tii.^3;
d2Qdtzzdtyy = -0.25*Tyy.*Tzz./Tii.^3;
d2Qdtzzdtxy = -0.5*Txy.*Tzz./Tii.^3;
d2Qdtzzdtzz = 0.5./Tii - 0.25*Tzz.^2./Tii.^3;

% txx = Txxv; tyy = Tyyv; txy = Txyv; J2 = Jiiv; tzz = Tzzv;
% d2Qdtxxdtxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
% d2Qdtxxdtyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
% d2Qdtxxdtzz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
% d2Qdtxxdtxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
% d2Qdtyydtxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
% d2Qdtyydtyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
% d2Qdtyydtzz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
% d2Qdtyydtxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
% d2Qdtzzdtxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
% d2Qdtzzdtyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
% d2Qdtzzdtzz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
% d2Qdtzzdtxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
% d2Qdtxydtxx = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
% d2Qdtxydtyy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
% d2Qdtxydtzz = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
% d2Qdtxydtxy = -0.100e1 .* J2 .^ (-0.3e1 / 0.2e1) .* txy .^ 2 + J2 .^ (-0.1e1 / 0.2e1);

dlam = dgv;
if Newton==1, dlam = 0*dlam; end

M11 = 1 + dlam .* D11 .* d2Qdtxxdtxx;
M12 = dlam .* D11 .* d2Qdtxxdtyy;
M13 = dlam .* D11 .* d2Qdtxxdtxy;
M14 = zeros(size(D11));
M15 = dlam .* D11 .* d2Qdtxxdtzz;
M21 = dlam .* D11 .* d2Qdtyydtxx;
M22 = 1 + dlam .* D11 .* d2Qdtyydtyy;
M23 = dlam .* D11 .* d2Qdtyydtxy;
M24 = zeros(size(D11));
M25 = dlam .* D11 .* d2Qdtyydtzz;
M31 = dlam .* D33 .* d2Qdtxydtxx;
M32 = dlam .* D33 .* d2Qdtxydtyy;
M33 = 1 + dlam .* D33 .* d2Qdtxydtxy;
M34 = zeros(size(D11));
M35 = dlam .* D33 .* d2Qdtxydtzz;
M41 = zeros(size(D11));
M42 = zeros(size(D11));
M43 = zeros(size(D11));
M44 = ones(size(D11));
M45 = zeros(size(D11));
M51 = dlam .* D11 .* d2Qdtzzdtxx;
M52 = dlam .* D11 .* d2Qdtzzdtyy;
M53 = dlam .* D11 .* d2Qdtzzdtxy;
M54 = zeros(size(D11));
M55 = 1 + dlam .* D11 .* d2Qdtzzdtzz;
[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );
Mi44 = ones(size(D44));%D44.^-1;
% dQdP = dQdP.*Mi44;

% d2QdtxxdP =0;
% d2QdtyydP = 0;
% d2QdtzzdP = 0;
% d2QdtxydP = 0;
% d2QdPdtxx =0;
% d2QdPdtyy =0;
% d2QdPdtxy =0;
% d2QdPdtzz =0;
% d2QdPdP =0;
% M11 = D11.*d2Qdtxxdtxx.*dlam + 1;
% M12 = D11.*d2Qdtxxdtyy.*dlam;
% M13 = D11.*d2Qdtxxdtxy.*dlam;
% M14 = D11.*d2QdtxxdP.*dlam;
% M15 = D11.*d2Qdtxxdtzz.*dlam;
% M21 = D22.*d2Qdtyydtxx.*dlam;
% M22 = D22.*d2Qdtyydtyy.*dlam + 1;
% M23 = D22.*d2Qdtyydtxy.*dlam;
% M24 = D22.*d2QdtyydP.*dlam;
% M25 = D22.*d2Qdtyydtzz.*dlam;
% M31 = D33.*d2Qdtxydtxx.*dlam;
% M32 = D33.*d2Qdtxydtyy.*dlam;
% M33 = D33.*d2Qdtxydtxy.*dlam + 1;
% M34 = D33.*d2QdtxydP.*dlam;
% M35 = D33.*d2Qdtxydtzz.*dlam;
% M41 = D44.*d2QdPdtxx.*dlam;
% M42 = D44.*d2QdPdtyy.*dlam;
% M43 = D44.*d2QdPdtxy.*dlam;
% M44 = D44.*d2QdPdP.*dlam + 1;
% M45 = D44.*d2QdPdtzz.*dlam;
% M51 = D55.*d2Qdtzzdtxx.*dlam;
% M52 = D55.*d2Qdtzzdtyy.*dlam;
% M53 = D55.*d2Qdtzzdtxy.*dlam;
% M54 = D55.*d2QdtzzdP.*dlam;
% M55 = D55.*d2Qdtzzdtzz.*dlam + 1;
% [ Mi11,Mi12,Mi13,Mi14,Mi15, Mi21,Mi22,Mi23,Mi24,Mi25, Mi31,Mi32,Mi33,Mi34,Mi35, Mi41,Mi42,Mi43,Mi44,Mi45, Mi51,Mi52,Mi53,Mi54,Mi55  ] = M2Di2_inverse_5x5( M11,M12,M13,M14,M15, M21,M22,M23,M24,M25, M31,M32,M33,M34,M35, M41,M42,M43,M44,M45, M51,M52,M53,M54,M55  );


den  = (dFdtxx.*Mi11+dFdtyy.*Mi21+dFdtxy.*Mi31+dFdtzz.*Mi51).*D11.*dQdtxx+(dFdtxx.*Mi12+dFdtyy.*Mi22+dFdtxy.*Mi32+dFdtzz.*Mi52).*D11.*dQdtyy+(dFdtxx.*Mi13+dFdtyy.*Mi23+dFdtxy.*Mi33+dFdtzz.*Mi53).*D33.*dQdtxy+dFdP.*Mi44.*D44.*dQdP+(dFdtxx.*Mi15+dFdtyy.*Mi25+dFdtxy.*Mi35+dFdtzz.*Mi55).*D55.*dQdtzz;

den  (plv==0) = 1;

D.D31v =  Mi31 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D32v = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi32 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D33v = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi33 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D34v =(-Mi31 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi32 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi33 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi35 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44);
D.D35v = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi35 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));


% den = D11.*dQdtxx.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + D22.*dQdtyy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + D33.*dQdtxy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + D44.*dQdP.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + D55.*dQdtzz.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz);
% den  (plv==0) = 1;
% 
% D.D11v = D11.*(-D22.*Mi12.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi11.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D12v = D22.*(-D11.*Mi11.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi12.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D13v = D33.*(-D11.*Mi11.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi12.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi13.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D14v = D44.*(-D11.*Mi11.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi12.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi15.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi14.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D15v = D55.*(-D11.*Mi11.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi12.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi13.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi14.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi15.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D21v = D11.*(-D22.*Mi22.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi21.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D22v = D22.*(-D11.*Mi21.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi22.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D23v = D33.*(-D11.*Mi21.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi22.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi23.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D24v = D44.*(-D11.*Mi21.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi22.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi25.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi24.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D25v = D55.*(-D11.*Mi21.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi22.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi23.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi24.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi25.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D31v = D11.*(-D22.*Mi32.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi31.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D32v = D22.*(-D11.*Mi31.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi32.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D33v = D33.*(-D11.*Mi31.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi32.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi33.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D34v = D44.*(-D11.*Mi31.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi32.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi35.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi34.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D35v = D55.*(-D11.*Mi31.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi32.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi33.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi34.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi35.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D41v = D11.*(-D22.*Mi42.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi41.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D42v = D22.*(-D11.*Mi41.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi42.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D43v = D33.*(-D11.*Mi41.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi42.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi43.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D44v = D44.*(-D11.*Mi41.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi42.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi45.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi44.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D45v = D55.*(-D11.*Mi41.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi42.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi43.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi44.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi45.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;
% D.D51v = D11.*(-D22.*Mi52.*dQdtyy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi11.*dFdtxx + Mi21.*dFdtyy + Mi31.*dFdtxy + Mi41.*dFdP + Mi51.*dFdtzz) + Mi51.*(-D11.*Mi11.*dFdtxx.*dQdtxx - D11.*Mi21.*dFdtyy.*dQdtxx - D11.*Mi31.*dFdtxy.*dQdtxx - D11.*Mi41.*dFdP.*dQdtxx - D11.*Mi51.*dFdtzz.*dQdtxx + den))./den;
% D.D52v = D22.*(-D11.*Mi51.*dQdtxx.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi12.*dFdtxx + Mi22.*dFdtyy + Mi32.*dFdtxy + Mi42.*dFdP + Mi52.*dFdtzz) + Mi52.*(-D22.*Mi12.*dFdtxx.*dQdtyy - D22.*Mi22.*dFdtyy.*dQdtyy - D22.*Mi32.*dFdtxy.*dQdtyy - D22.*Mi42.*dFdP.*dQdtyy - D22.*Mi52.*dFdtzz.*dQdtyy + den))./den;
% D.D53v = D33.*(-D11.*Mi51.*dQdtxx.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D22.*Mi52.*dQdtyy.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi13.*dFdtxx + Mi23.*dFdtyy + Mi33.*dFdtxy + Mi43.*dFdP + Mi53.*dFdtzz) + Mi53.*(-D33.*Mi13.*dFdtxx.*dQdtxy - D33.*Mi23.*dFdtyy.*dQdtxy - D33.*Mi33.*dFdtxy.*dQdtxy - D33.*Mi43.*dFdP.*dQdtxy - D33.*Mi53.*dFdtzz.*dQdtxy + den))./den;
% D.D54v = D44.*(-D11.*Mi51.*dQdtxx.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D22.*Mi52.*dQdtyy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) - D55.*Mi55.*dQdtzz.*(Mi14.*dFdtxx + Mi24.*dFdtyy + Mi34.*dFdtxy + Mi44.*dFdP + Mi54.*dFdtzz) + Mi54.*(-D44.*Mi14.*dFdtxx.*dQdP - D44.*Mi24.*dFdtyy.*dQdP - D44.*Mi34.*dFdtxy.*dQdP - D44.*Mi44.*dFdP.*dQdP - D44.*Mi54.*dFdtzz.*dQdP + den))./den;
% D.D55v = D55.*(-D11.*Mi51.*dQdtxx.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D22.*Mi52.*dQdtyy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D33.*Mi53.*dQdtxy.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) - D44.*Mi54.*dQdP.*(Mi15.*dFdtxx + Mi25.*dFdtyy + Mi35.*dFdtxy + Mi45.*dFdP + Mi55.*dFdtzz) + Mi55.*(-D55.*Mi15.*dFdtxx.*dQdtzz - D55.*Mi25.*dFdtyy.*dQdtzz - D55.*Mi35.*dFdtxy.*dQdtzz - D55.*Mi45.*dFdP.*dQdtzz - D55.*Mi55.*dFdtzz.*dQdtzz + den))./den;


D.D34v = D.D34v./D44;



end

