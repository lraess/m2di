function [D] = M2Di2_EvaluateConsistenTangent_DP_VM_comp_v1(plc,dgc,Txxc,Tyyc,Txyc,Tzzc,Jiic,dQdtxxc,dQdtyyc,dQdtzzc,dQdtxyc,dQdPc,Kc,phic,psic, etac, eta_vp_c,plv,dgv,Txxv,Tyyv, Txyv,Tzzv,Jiiv,dQdtxxv,dQdtyyv,dQdtzzv,dQdtxyv,dQdPv,Kv, phiv,psiv, etav, eta_vp_v,Newton ,Exxc1,Eyyc1,Gxyc1,Ezzc1,Exxv1,Eyyv1,Gxyv1,Ezzv1,detadexxc,detadeyyc,detadgxyc,detadpc,detadexxv,detadeyyv,detadgxyv,detadpv,n_vp, Cohc, Cohv, dt, E2_plc0, E2_plv0, CTL_c, CTL_v, comp)
dQdtxx = dQdtxxc; dQdtyy = dQdtyyc; dQdtzz = dQdtzzc; dQdtxy = dQdtxyc;  dQdP = dQdPc;
dFdtxx = dQdtxxc; dFdtyy = dQdtyyc; dFdtzz = dQdtzzc; dFdtxy = dQdtxyc;  dFdP = -sin(phic).*plc;
D11 = 2*etac; D22 = 2*etac; D33 = 1*etac; D55 = 2*etac; 
if comp==0, D44 = 1         ; end
if comp==1, D44 = (Kc*dt);    end

txx = Txxc; tyy = Tyyc; txy = Txyc; J2 = Jiic; tzz = Tzzc;
d2Qdsxxdsxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
d2Qdsxxdsyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
d2Qdsxxdszz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
d2Qdsxxdsxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
d2Qdsyydsxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
d2Qdsyydsyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
d2Qdsyydszz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
d2Qdsyydsxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
d2Qdszzdsxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
d2Qdszzdsyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
d2Qdszzdszz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
d2Qdszzdsxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
d2Qdsxydsxx = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
d2Qdsxydsyy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
d2Qdsxydszz = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
d2Qdsxydsxy = -0.100e1 .* J2 .^ (-0.3e1 / 0.2e1) .* txy .^ 2 + J2 .^ (-0.1e1 / 0.2e1);

dlam = dgc;
if Newton == 1, dlam = 0*dlam; end
M11 = 1 + dlam .* D11 .* d2Qdsxxdsxx;
M12 = dlam .* D11 .* d2Qdsxxdsyy;
M13 = dlam .* D11 .* d2Qdsxxdsxy;
M14 = zeros(size(D11));
M15 = dlam .* D11 .* d2Qdsxxdszz;
M21 = dlam .* D11 .* d2Qdsyydsxx;
M22 = 1 + dlam .* D11 .* d2Qdsyydsyy;
M23 = dlam .* D11 .* d2Qdsyydsxy;
M24 = zeros(size(D11));
M25 = dlam .* D11 .* d2Qdsyydszz;
M31 = dlam .* D33 .* d2Qdsxydsxx;
M32 = dlam .* D33 .* d2Qdsxydsyy;
M33 = 1 + dlam .* D33 .* d2Qdsxydsxy;
M34 = zeros(size(D11));
M35 = dlam .* D33 .* d2Qdsxydszz;
M41 = zeros(size(D11));
M42 = zeros(size(D11));
M43 = zeros(size(D11));
M44 = ones(size(D11));
M45 = zeros(size(D11));
M51 = dlam .* D11 .* d2Qdszzdsxx;
M52 = dlam .* D11 .* d2Qdszzdsyy;
M53 = dlam .* D11 .* d2Qdszzdsxy;
M54 = zeros(size(D11));
M55 = 1 + dlam .* D11 .* d2Qdszzdszz;
[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );
Mi44 = ones(size(D11));

% Consistent tangent - deviator
den  = (dFdtxx.*Mi11+dFdtyy.*Mi21+dFdtxy.*Mi31+dFdtzz.*Mi51).*D11.*dQdtxx + (dFdtxx.*Mi12+dFdtyy.*Mi22+dFdtxy.*Mi32+dFdtzz.*Mi52).*D11.*dQdtyy + (dFdtxx.*Mi13+dFdtyy.*Mi23+dFdtxy.*Mi33+dFdtzz.*Mi53).*D33.*dQdtxy + dFdP.*Mi44.*D44.*dQdP+(dFdtxx.*Mi15+dFdtyy.*Mi25+dFdtxy.*Mi35+dFdtzz.*Mi55).*D55.*dQdtzz;
den  (plc==0) = 1;

D.D11c = Mi11 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi12 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi13 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi15 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D12c = -Mi11 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi12 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi13 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi15 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D13c = -Mi11 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi12 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi13 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi15 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D14c = -Mi11 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi12 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi13 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi15 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44;
D.D15c = -Mi11 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi12 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi13 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi15 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D21c = Mi21 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi22 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi23 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi25 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D22c = -Mi21 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi22 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi23 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi25 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D23c = -Mi21 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi22 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi23 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi25 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D24c = -Mi21 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi22 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi23 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi25 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44;
D.D25c = -Mi21 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi22 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi23 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi25 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D31c = Mi31 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D32c = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi32 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D33c = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi33 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D34c = -Mi31 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi32 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi33 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi35 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44;
D.D35c = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi35 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D41c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi11 .* D11 + dQdP .* dFdtyy .* Mi21 .* D11 + dQdP .* dFdtxy .* Mi31 .* D11 + dQdP .* dFdtzz .* Mi51 .* D11);
D.D42c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi12 .* D11 + dQdP .* dFdtyy .* Mi22 .* D11 + dQdP .* dFdtxy .* Mi32 .* D11 + dQdP .* dFdtzz .* Mi52 .* D11);
D.D43c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi13 .* D33 + dQdP .* dFdtyy .* Mi23 .* D33 + dQdP .* dFdtxy .* Mi33 .* D33 + dQdP .* dFdtzz .* Mi53 .* D33);
D.D44c =  Mi44 .* D44 .* (1 - 1 ./ den .* dFdP .* Mi44 .* D44 .* dQdP); % [ Pa.s ]
D.D45c = -Mi44 .* D44 ./ den .* (dQdP .* dFdtxx .* Mi15 .* D55 + dQdP .* dFdtyy .* Mi25 .* D55 + dQdP .* dFdtxy .* Mi35 .* D55 + dQdP .* dFdtzz .* Mi55 .* D55);

D.D14c = D.D14c./D44;
D.D24c = D.D24c./D44;
D.D34c = D.D34c./D44;
D.D44c = D.D44c./D44; % [ - ]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dQdtxx = dQdtxxv; dQdtyy = dQdtyyv; dQdtzz = dQdtzzv; dQdtxy = dQdtxyv; dQdP = dQdPv;
dFdtxx = dQdtxxv; dFdtyy = dQdtyyv; dFdtzz = dQdtzzv; dFdtxy = dQdtxyv; dFdP = -sin(phiv).*plv;
D11 = 2*etav; D22 = 2*etav; D33 = 1*etav; D55 = 2*etav;  
if comp==0, D44 = 1         ; end
if comp==1, D44 = (Kv*dt); end

txx = Txxv; tyy = Tyyv; txy = Txyv; J2 = Jiiv; tzz = Tzzv;
d2Qdsxxdsxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
d2Qdsxxdsyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
d2Qdsxxdszz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
d2Qdsxxdsxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
d2Qdsyydsxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tyy;
d2Qdsyydsyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
d2Qdsyydszz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
d2Qdsyydsxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
d2Qdszzdsxx = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* tzz;
d2Qdszzdsyy = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* tzz;
d2Qdszzdszz = -0.25e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .^ 2 + J2 .^ (-0.1e1 / 0.2e1) / 0.2e1;
d2Qdszzdsxy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
d2Qdsxydsxx = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* txx .* txy;
d2Qdsxydsyy = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tyy .* txy;
d2Qdsxydszz = -0.50e0 .* J2 .^ (-0.3e1 / 0.2e1) .* tzz .* txy;
d2Qdsxydsxy = -0.100e1 .* J2 .^ (-0.3e1 / 0.2e1) .* txy .^ 2 + J2 .^ (-0.1e1 / 0.2e1);

dlam = dgv;
if Newton==1, dlam = 0*dlam; end
M11 = 1 + dlam .* D11 .* d2Qdsxxdsxx;
M12 = dlam .* D11 .* d2Qdsxxdsyy;
M13 = dlam .* D11 .* d2Qdsxxdsxy;
M14 = zeros(size(D11));
M15 = dlam .* D11 .* d2Qdsxxdszz;
M21 = dlam .* D11 .* d2Qdsyydsxx;
M22 = 1 + dlam .* D11 .* d2Qdsyydsyy;
M23 = dlam .* D11 .* d2Qdsyydsxy;
M24 = zeros(size(D11));
M25 = dlam .* D11 .* d2Qdsyydszz;
M31 = dlam .* D33 .* d2Qdsxydsxx;
M32 = dlam .* D33 .* d2Qdsxydsyy;
M33 = 1 + dlam .* D33 .* d2Qdsxydsxy;
M34 = zeros(size(D11));
M35 = dlam .* D33 .* d2Qdsxydszz;
M41 = zeros(size(D11));
M42 = zeros(size(D11));
M43 = zeros(size(D11));
M44 = ones(size(D11));
M45 = zeros(size(D11));
M51 = dlam .* D11 .* d2Qdszzdsxx;
M52 = dlam .* D11 .* d2Qdszzdsyy;
M53 = dlam .* D11 .* d2Qdszzdsxy;
M54 = zeros(size(D11));
M55 = 1 + dlam .* D11 .* d2Qdszzdszz;
[ Mi11,Mi12,Mi13,Mi15, Mi21,Mi22,Mi23,Mi25, Mi31,Mi32,Mi33,Mi35, Mi51,Mi52,Mi53,Mi55  ] = M2Di2_inverse_4x4( M11,M12,M13,M15, M21,M22,M23,M25, M31,M32,M33,M35, M51,M52,M53,M55  );
Mi44 = ones(size(D44));

den  = (dFdtxx.*Mi11+dFdtyy.*Mi21+dFdtxy.*Mi31+dFdtzz.*Mi51).*D11.*dQdtxx+(dFdtxx.*Mi12+dFdtyy.*Mi22+dFdtxy.*Mi32+dFdtzz.*Mi52).*D11.*dQdtyy+(dFdtxx.*Mi13+dFdtyy.*Mi23+dFdtxy.*Mi33+dFdtzz.*Mi53).*D33.*dQdtxy+dFdP.*Mi44.*D44.*dQdP+(dFdtxx.*Mi15+dFdtyy.*Mi25+dFdtxy.*Mi35+dFdtzz.*Mi55).*D55.*dQdtzz;

den  (plv==0) = 1;

D.D31v =  Mi31 .* D11 .* (1 - 1 ./ den .* (dQdtxx .* dFdtxx .* Mi11 .* D11 + dQdtxx .* dFdtyy .* Mi21 .* D11 + dQdtxx .* dFdtxy .* Mi31 .* D11 + dQdtxx .* dFdtzz .* Mi51 .* D11)) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi11 .* D11 + dQdtyy .* dFdtyy .* Mi21 .* D11 + dQdtyy .* dFdtxy .* Mi31 .* D11 + dQdtyy .* dFdtzz .* Mi51 .* D11) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi11 .* D11 + dQdtxy .* dFdtyy .* Mi21 .* D11 + dQdtxy .* dFdtxy .* Mi31 .* D11 + dQdtxy .* dFdtzz .* Mi51 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi11 .* D11 + dQdtzz .* dFdtyy .* Mi21 .* D11 + dQdtzz .* dFdtxy .* Mi31 .* D11 + dQdtzz .* dFdtzz .* Mi51 .* D11);
D.D32v = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi12 .* D11 + dQdtxx .* dFdtyy .* Mi22 .* D11 + dQdtxx .* dFdtxy .* Mi32 .* D11 + dQdtxx .* dFdtzz .* Mi52 .* D11) + Mi32 .* D11 .* (1 - 1 ./ den .* (dQdtyy .* dFdtxx .* Mi12 .* D11 + dQdtyy .* dFdtyy .* Mi22 .* D11 + dQdtyy .* dFdtxy .* Mi32 .* D11 + dQdtyy .* dFdtzz .* Mi52 .* D11)) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi12 .* D11 + dQdtxy .* dFdtyy .* Mi22 .* D11 + dQdtxy .* dFdtxy .* Mi32 .* D11 + dQdtxy .* dFdtzz .* Mi52 .* D11) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi12 .* D11 + dQdtzz .* dFdtyy .* Mi22 .* D11 + dQdtzz .* dFdtxy .* Mi32 .* D11 + dQdtzz .* dFdtzz .* Mi52 .* D11);
D.D33v = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi13 .* D33 + dQdtxx .* dFdtyy .* Mi23 .* D33 + dQdtxx .* dFdtxy .* Mi33 .* D33 + dQdtxx .* dFdtzz .* Mi53 .* D33) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi13 .* D33 + dQdtyy .* dFdtyy .* Mi23 .* D33 + dQdtyy .* dFdtxy .* Mi33 .* D33 + dQdtyy .* dFdtzz .* Mi53 .* D33) + Mi33 .* D33 .* (1 - 1 ./ den .* (dQdtxy .* dFdtxx .* Mi13 .* D33 + dQdtxy .* dFdtyy .* Mi23 .* D33 + dQdtxy .* dFdtxy .* Mi33 .* D33 + dQdtxy .* dFdtzz .* Mi53 .* D33)) - Mi35 .* D55 ./ den .* (dQdtzz .* dFdtxx .* Mi13 .* D33 + dQdtzz .* dFdtyy .* Mi23 .* D33 + dQdtzz .* dFdtxy .* Mi33 .* D33 + dQdtzz .* dFdtzz .* Mi53 .* D33);
D.D34v =(-Mi31 .* D11 ./ den .* dQdtxx .* dFdP .* Mi44 .* D44 - Mi32 .* D11 ./ den .* dQdtyy .* dFdP .* Mi44 .* D44 - Mi33 .* D33 ./ den .* dQdtxy .* dFdP .* Mi44 .* D44 - Mi35 .* D55 ./ den .* dQdtzz .* dFdP .* Mi44 .* D44);
D.D35v = -Mi31 .* D11 ./ den .* (dQdtxx .* dFdtxx .* Mi15 .* D55 + dQdtxx .* dFdtyy .* Mi25 .* D55 + dQdtxx .* dFdtxy .* Mi35 .* D55 + dQdtxx .* dFdtzz .* Mi55 .* D55) - Mi32 .* D11 ./ den .* (dQdtyy .* dFdtxx .* Mi15 .* D55 + dQdtyy .* dFdtyy .* Mi25 .* D55 + dQdtyy .* dFdtxy .* Mi35 .* D55 + dQdtyy .* dFdtzz .* Mi55 .* D55) - Mi33 .* D33 ./ den .* (dQdtxy .* dFdtxx .* Mi15 .* D55 + dQdtxy .* dFdtyy .* Mi25 .* D55 + dQdtxy .* dFdtxy .* Mi35 .* D55 + dQdtxy .* dFdtzz .* Mi55 .* D55) + Mi35 .* D55 .* (1 - 1 ./ den .* (dQdtzz .* dFdtxx .* Mi15 .* D55 + dQdtzz .* dFdtyy .* Mi25 .* D55 + dQdtzz .* dFdtxy .* Mi35 .* D55 + dQdtzz .* dFdtzz .* Mi55 .* D55));

D.D34v = D.D34v./D44;

end

