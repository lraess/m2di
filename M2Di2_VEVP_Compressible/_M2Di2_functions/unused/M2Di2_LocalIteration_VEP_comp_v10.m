function [ eta_vep, K_vep, Txx, Tyy, Tzz, Txy, Pc, divp, dive, deta_ve, deta_vep, ddivp, pl, eta_vp, Eii_el, Eii_pwl, Eii_pl, Overstress, Stuff, E2pl, C_s, phi, psi, dg ] = M2Di2_LocalIteration_VEP_comp_v10( litmax, tol, noisy, div, Exx, Eyy, Ezz, Exy, P, Txxo, Tyyo, Tzzo, Txyo, B_pwl, m_pwl, eta_el, K, P0, phi, psi, C_s, eta_vp, eta_vp0, n_vp, dt, E2pl0, Coh, C0, Phi, phi0, Psi, psi0, C_t  )

% Yield was modified to Ty = cos(phi)*C_s + sin(phi)*P;
% Softening function updated 
% adding tensile criteria

% This one works with power law VP and cohesion softening
% Initialise
Eii_el     = 0;
Eii_pwl    = 0;
Eii_pl     = 0;
Overstress = 0;
E2pl       = E2pl0;
hcoh       = 0;
dhcoh      = 0;
hphi       = 0;
dhphi      = 0;
K_vep      = K;
divp       = 0;
dive       = 0;
dHddg      = 0;
dpsiddg    = 0;

% Strain rate components and invariant
exx    = Exx;
eyy    = Eyy;
ezz    = Ezz;
exy    = Exy;
eII    = sqrt(1/2*(exx.^2 + eyy.^2 + ezz.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
Exx    = Exx + Txxo./2./eta_el;
Eyy    = Eyy + Tyyo./2./eta_el;
Ezz    = Ezz + Tzzo./2./eta_el;
Exy    = Exy + Txyo./2./eta_el;
Div    = div - P0./K./dt;
% Pc     = -K.*dt.*Div;
Pc     = P;
Eii    = sqrt(1/2*(Exx.^2 + Eyy.^2 + Ezz.^2) + Exy.^2);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*eII.^(1./n_pwl-1);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_ve     = 0.5*(eta_up+eta_lo);

if noisy>2, fprintf('Starting Visco-Elastic local iterations:\n'); end

% Local iterations
for lit=1:litmax
    
    % Function evaluation at current effective viscosity
    Tii       = 2*eta_ve.*Eii;
    Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
    Eii_vis_0 = Eii_pwl_0;
    r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
    
    % Residual check
    res = max(max(abs(r_eta_0)));
    if lit == 1; res0 = res; end
    if noisy>2, fprintf('It. %02d, r abs. = %2.2e r rel. = %2.2e\n', lit, res, res/res0); end
    if res/res0 < tol || res<tol
        break;
    end
    
    % Exact derivative
    drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_ve;
    
    % Update viscosity
    eta_ve  = eta_ve - r_eta_0 ./ drdeta;
    
end

% Update individual deviatoric stress components
Txx = 2*eta_ve.*Exx;
Tyy = 2*eta_ve.*Eyy;
Tzz = 2*eta_ve.*Ezz;
Txy = 2*eta_ve.*Exy;
Tii = 2*eta_ve.*Eii;
% For analytical Jacobian --- VE  --- ViscosityDerivativesTest.py
Jii = Tii.^2;
detadTxx = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Txx.*eta_ve.^2.*(n_pwl - 1);
detadTyy = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Tyy.*eta_ve.^2.*(n_pwl - 1);
detadTxy = -2.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Txy.*eta_ve.^2.*(n_pwl - 1);
detadTzz = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Tzz.*eta_ve.^2.*(n_pwl - 1);

deta_ve.dExx = detadTxx .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dEyy = detadTyy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dExy = detadTxy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dEzz = detadTzz .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dP   = zeros(size(Txx));
% Secure things
deta_ve.dExx(isinf(deta_ve.dExx)) = 0; deta_ve.dExx(isnan(deta_ve.dEyy)) = 0;
deta_ve.dEyy(isinf(deta_ve.dEyy)) = 0; deta_ve.dEyy(isnan(deta_ve.dEyy)) = 0;
deta_ve.dExy(isinf(deta_ve.dExy)) = 0; deta_ve.dExy(isnan(deta_ve.dExy)) = 0;
deta_ve.dEzz(isinf(deta_ve.dEzz)) = 0; deta_ve.dEzz(isnan(deta_ve.dEzz)) = 0;

% Plasticity

% CHECK for negative pressures
np  = P<0;
nnp = sum(np(:));
if nnp>0 && noisy>2, 
    fprintf('Actung baby: there are %06d nodes with negative pressure in the box!\n', nnp);
end

% Total stress 
Sxx = -P + Txx;
Syy = -P + Tyy;
Szz = -P + Tzz;
%P2  = -1/3*(Sxx+Syy+Szz);
Ty_shear       = C_s.*cos(phi) + P.*sin(phi);
Ty_tens        = C_t + P;
X              = eta_ve./eta_el;
F_s            = Tii - Ty_shear;
F_t            = Tii - Ty_tens;
pl               = zeros(size(F_s));
pl(F_s >0 & F_t <0) = 1; % shear
pl(F_s <0 & F_t >0) = 2; % tensile
pl(F_s >0 & F_t >0) = 3; % corner
shear   = F_s >0 & F_t <0;
tensile = F_s <0 & F_t >0;
corner  = F_s >0 & F_t >0;
dQdtxx         = Txx./2./Tii; % same for shear and tension
dQdtyy         = Tyy./2./Tii;
dQdtzz         = Tzz./2./Tii;
dQdtxy         = Txy./1./Tii;
% psi            = zeros(size(phi));
dQ             = sqrt(2/3)*sqrt( 1/2*(dQdtxx).^2 + 1/2*(dQdtyy).^2 + 1/2*(dQdtzz).^2 + (dQdtxy/2).^2 );
dg             = zeros(size(Txx));
eta_vp         = zeros(size(Txx));
eta_vp(pl>0)   = eta_vp0*(eII(pl>0)).^(1/n_vp-1);
dg(pl>0)       = F_s(pl>0)./(eta_ve(pl>0) + eta_vp(pl>0) + K(pl>0).*dt.*sin(phi(pl>0)).*sin(psi(pl>0)) );
res0           = max(F_s(:));
Fc             = F_s;
if noisy>2, fprintf('Starting ViscoPlastic local iterations - F trial = %2.2e:\n', res0); end
if noisy>3, figure(19), clf, hold on; end

%% Tensile limit
if sum(sum(tensile(:))>0)
    dg(pl==2)      = F_t(pl==2)./(eta_ve(pl==2) + eta_vp(pl==2) + K(pl==2).*dt );
    divp           = dg; % sin(psi = 1 in tension
    Pc             = P + K.*dt.*divp;
    Ty_tens        = C_t + Pc + eta_vp.*dg;
    Fc(pl==2)      = Tii(pl==2) - eta_ve(pl==2).*dg(pl==2) - Ty_tens(pl==2);
    Fc_pl          = Fc(pl==2);
    if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e, max(F) = %2.2e\n', lit, res/res0, res, max(Fc_pl(:))); end
end

% F_shear     = Tii - eta_ve.*dg - Ty_shear;
% F_tens      = Tii - eta_ve.*dg - Ty_tens;

if  noisy>2, 
    fprintf('%04d nodes over %04d in tensile limit\n', sum(tensile(:)), size(dQ,1)*size(dQ,2))
    fprintf('%04d nodes over %04d in shear limit\n'  , sum(shear (:)), size(dQ,1)*size(dQ,2))
    fprintf('%04d nodes over %04d in both limits\n'  , sum(corner (:)), size(dQ,1)*size(dQ,2))
%     error('tensile limit'); 
end

%% Shear
if sum(sum(shear(:))>0)
    % Plastic strain rates
    for lit=1:litmax
        % Evaluate F
        E2pl           = E2pl0 + dQ.*dg.*dt;
        hcoh           = -Coh.Delta.*exp(-(E2pl - Coh.Ein).^2./Coh.dE.^2)./(sqrt(pi)*Coh.dE);
        hphi           = -Phi.Delta.*exp(-(E2pl - Phi.Ein).^2./Phi.dE.^2)./(sqrt(pi)*Phi.dE);
        hpsi           = -Psi.Delta.*exp(-(E2pl - Psi.Ein).^2./Psi.dE.^2)./(sqrt(pi)*Psi.dE);
        C_s              = C0   + Coh.soft_on.*hcoh.*dg.*dt.*dQ;
        phi            = phi0 + Phi.soft_on.*hphi.*dg.*dt.*dQ;
        psi            = psi0 + Psi.soft_on.*hpsi.*dg.*dt.*dQ;
        divp           = dg.*sin(psi);
        Pc             = P + K.*dt.*divp;
        eta_vp(pl==1)  = eta_vp0 .* dg(pl==1) .^ (1/n_vp-1);
        Ty_shear        = C_s.*cos(phi) + Pc.*sin(phi) + eta_vp.*dg;
        Fc(pl==1)      = Tii(pl==1) - eta_ve(pl==1).*dg(pl==1) - Ty_shear(pl==1);
        % Residual check
        res = max(max(abs(Fc(pl))));
        Fc_pl = Fc(pl==1);
        if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e, max(F) = %2.2e\n', lit, res/res0, res, max(Fc_pl(:))); end
       
        if noisy>3,
            val = log10(res/res0);
            if isinf(val), val = -15; end
            plot(lit, val, 'o'); end
        if res/res0 < tol || res < tol
            break;
        end
        % Exact derivative
        dhcoh     = -2.0*Coh.Delta.*dt.*dQ.*(Coh.Ein - E2pl).*exp(-(Coh.Ein - E2pl).^2./Coh.dE.^2)./(sqrt(pi)*Coh.dE.^3);
        dhphi     = -2.0*Phi.Delta.*dt.*dQ.*(Phi.Ein - E2pl).*exp(-(Phi.Ein - E2pl).^2./Phi.dE.^2)./(sqrt(pi)*Phi.dE.^3);
        dhpsi     = -2.0*Psi.Delta.*dt.*dQ.*(Psi.Ein - E2pl).*exp(-(Psi.Ein - E2pl).^2./Psi.dE.^2)./(sqrt(pi)*Psi.dE.^3);
        dCddg   = dQ.*dt.*(dhcoh.*dg + hcoh);
        dphiddg = dQ.*dt.*(dhphi.*dg + hphi);
        dpsiddg = dQ.*dt.*(dhpsi.*dg + hpsi);
       
        dHcoh    =   Coh.soft_on.*dCddg.*cos(phi);
        dHphi    = - Phi.soft_on.*dphiddg.*(C_s.*sin(phi) - Pc.*cos(phi));
        dHpsi    =   Psi.soft_on.*K.*dpsiddg.*dt.*dg.*cos(psi).*sin(phi);
        dHddg  = dHcoh  + dHphi + dHpsi;
        dFddg  = - eta_ve - eta_vp./n_vp - K.*dt.*sin(psi).*sin(phi) - dHddg;
        % Update plastic multiplier rate
        dg(pl==1)  = dg(pl==1) - Fc(pl==1) ./ dFddg(pl==1);
        dg         = real(abs(dg)); % MATLAB tends to spit out complex numbers
    end
end

%% Corner
if sum(sum(corner(:))>0)
    
   dg_s = 0*dg;
   dg_t = 0*dg;
   F_s0 = -C_s.*cos(phi) + Tii - eta_ve.*(dg_s + dg_t) - eta_vp.*(dg_s + dg_t) - (K.*dt.*(dg_s.*sin(psi) + dg_t) + P).*sin(phi);
   F_t0 = -C_t - K.*dt.*(dg_s.*sin(psi) + dg_t) - P + Tii - eta_ve.*(dg_s + dg_t) - eta_vp.*(dg_s + dg_t);

    
   dg_s = ( F_s.*(K.*dt + eta_ve + eta_vp) - F_t.*(K.*dt.*sin(phi) + eta_ve + eta_vp))./((K.*dt + eta_ve + eta_vp).*(K.*dt.*sin(phi).*sin(psi) + eta_ve + eta_vp) - (K.*dt.*sin(phi) + eta_ve + eta_vp).*(K.*dt.*sin(psi) + eta_ve + eta_vp));
   dg_t = (-F_s.*(K.*dt.*sin(psi) + eta_ve + eta_vp) + F_t.*(K.*dt.*sin(phi).*sin(psi) + eta_ve + eta_vp))./((K.*dt + eta_ve + eta_vp).*(K.*dt.*sin(phi).*sin(psi) + eta_ve + eta_vp) - (K.*dt.*sin(phi) + eta_ve + eta_vp).*(K.*dt.*sin(psi) + eta_ve + eta_vp));
   
   F_s1 = -C_s.*cos(phi) + Tii - eta_ve.*(dg_s + dg_t) - eta_vp.*(dg_s + dg_t) - (K.*dt.*(dg_s.*sin(psi) + dg_t) + P).*sin(phi);
   F_t1 = -C_t - K.*dt.*(dg_s.*sin(psi) + dg_t) - P + Tii - eta_ve.*(dg_s + dg_t) - eta_vp.*(dg_s + dg_t);

   
   dg_s(pl==3)
   dg_t(pl==3)
   
   F_s(pl==3)
   F_t(pl==3)
   
   F_s0(pl==3)
   F_t0(pl==3)
   
   F_s1(pl==3)
   F_t1(pl==3)
   
   Pc          = P   + K.*dt.*(dg_t + sin(psi).*dg_s);
   Ty_shear    = C_s.*cos(phi) + Pc.*sin(phi) + eta_vp.*(dg_s + dg_t);
   Ty_tens     = C_t + Pc + eta_vp.*(dg_s + dg_t);
   F_s         = Tii - eta_ve.*(dg_s + dg_t) - Ty_shear;
   F_t         = Tii - eta_ve.*(dg_s + dg_t) - Ty_tens;

   F_s(pl==3)
   F_t(pl==3)
   
    error('CORNER')
end

if noisy>3, drawnow; end

% Post-process
exx_pl  = dg.*dQdtxx;
eyy_pl  = dg.*dQdtyy;
ezz_pl  = dg.*dQdtzz;
exy_pl  = dg.*dQdtxy*(1/2);
% dE2pl   = sqrt( 1/2*(exx_pl*dt).^2 + 1/2*(eyy_pl*dt).^2 + 1/2*(ezz_pl*dt).^2 + (exy_pl*dt).^2 );
Txx     = 2.*eta_ve.*(exx-exx_pl) +  X.*Txxo;
Tyy     = 2.*eta_ve.*(eyy-eyy_pl) +  X.*Tyyo;
Tzz     = 2.*eta_ve.*(ezz-ezz_pl) +  X.*Tzzo;
Txy     = 2.*eta_ve.*(exy-exy_pl) +  X.*Txyo;
Tii_corr= sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*Tzz.^2 + Txy.^2);
eta_vep = eta_ve;
eta_vep(pl>0) = Tii_corr(pl>0)./2./Eii(pl>0);
K_vep(pl>0)   = -Pc(pl>0)./(dt.*Div(pl>0));

% Total stress 
% P   = -K.*dt.*(Div-divp);
% Sxx = -P + Txx;
% Syy = -P + Tyy;
% Szz = -P + Tzz;
% P2  = -1/3*(Sxx+Syy+Szz);
% chk2 = norm(P-P2)
% chk3 = norm(Exx+Eyy+Ezz)
% chk4 = norm(-(Exx+Eyy)- Ezz)
% chk5 = norm(Tzz - 2*eta_vep.*(-(Exx+Eyy)))


% Checks
if noisy>2
    
%     sum(pl(:))
%     Pc2 = -K.*dt.*(Div-divp);
%     Pc1 = -K_vep.*dt.*(Div);
%     norm(Pc1-Pc)
%     norm(Pc2-Pc)
    
    % Check yield 1 after correction
    F_shear     = Tii_corr - Ty_shear;
    F_tens      = Tii_corr - Ty_tens;
    F1          = max(F_shear,F_tens);

    % Double check wether effective viscosity is computed accurately
    Txx = 2*eta_vep.*Exx;
    Tyy = 2*eta_vep.*Eyy;
    Tzz = 2*eta_vep.*Ezz;
    Txy = 2*eta_vep.*Exy;
    Tii = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*Tzz.^2 + Txy.^2);
    Tii(pl>0) = 2*eta_vep(pl>0).*Eii(pl>0);
    F_shear   = Tii - Ty_shear;
    F_tens    = Tii - Ty_tens;
    F2        = max(F_shear,F_tens);
    
    % Correct power-law viscosity
    eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
    
    % 1: strain rate components
    exx_el  = (Txx-Txxo)./2./eta_el;
    exx_pwl = Txx./2./eta_pwl;
    exx_pl  = dg.*dQdtxx;
    exx_net = norm(exx-exx_el-exx_pwl-exx_pl);
    
    eyy_el  = (Tyy-Tyyo)./2./eta_el;
    eyy_pwl = Tyy./2./eta_pwl;
    eyy_pl  = dg.*dQdtyy;
    eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl);
    
    ezz_el  = (Tzz-Tzzo)./2./eta_el;
    ezz_pwl = Tzz./2./eta_pwl;
    ezz_pl  = dg.*dQdtzz;
    ezz_net = norm(ezz-ezz_el-ezz_pwl-ezz_pl);
    
    exy_el  = (Txy-Txyo)./2./eta_el;
    exy_pwl = Txy./2./eta_pwl;
    exy_pl  = dg.*dQdtxy*(1/2);
    exy_net = norm(exy-exy_el-exy_pwl-exy_pl);
    
    dive    = -(P  - P0)./(K*dt);
    div_net = div - dive - divp;  

    if sum(pl(:))>0, 
        dg_chk = zeros(size(F_s));
        dg_chk(pl==1) = F_s(pl==1) ./ (eta_ve(pl==1) + eta_vp(pl==1) + dt*hcoh(pl==1).*cos(phi(pl==1)) + K(pl==1).*dt.*sin(psi(pl==1)).*sin(phi(pl==1)));
    end
    
    if noisy>2 && sum(pl(:))>0
        Overstress  = eta_vp.*dg;
        fprintf('F trial = %2.2e --- F corr1 = %2.2e --- F corr2 = %2.2e --- Over S. = %2.2e\n', max(F_s(:)), max(F1(:)), max(F2(:)), max(Overstress(:)))
        fprintf('net exx = %2.2e --- net eyy = %2.2e --- net ezz = %2.2e --- net exy = %2.2e\n', norm(exx_net),norm(eyy_net),norm(ezz_net),norm(exy_net))
        fprintf('net ekk = %2.2e --- net dg = %2.2e\n', norm(div_net), norm(dg - dg_chk)); 
    end
        
    % 2: invariants
    Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + 1/2*ezz_el .^2 + exy_el .^2);
    Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + 1/2*ezz_pwl.^2 + exy_pwl.^2);
    Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + 1/2*ezz_pl .^2 + exy_pl .^2);
    
end
% For analytical Jacobian --- VEP --- ViscosityDerivativesTest.py


Tyield = Ty_tens;
psi(pl==2) = 1;
phi(pl==2) = 1;

% dFdE
dFdExx        =   Exx.*eta_ve./Eii + 2.*Eii.*deta_ve.dExx;
dFdEyy        =   Eyy.*eta_ve./Eii + 2.*Eii.*deta_ve.dEyy;
dFdEzz        =   Ezz.*eta_ve./Eii + 2.*Eii.*deta_ve.dEzz;
dFdExy        = 2*Exy.*eta_ve./Eii + 2.*Eii.*deta_ve.dExy;  
dFdP          = -sin(phi);
% ddgdE
g             =  1./ (eta_ve + eta_vp./n_vp + dHddg + K.*dt.*sin(psi).*sin(phi) );
dlamdExx      = g .* (dFdExx - dg .*deta_ve.dExx);
dlamdEyy      = g .* (dFdEyy - dg .*deta_ve.dEyy);
dlamdEzz      = g .* (dFdEzz - dg .*deta_ve.dEzz);
dlamdExy      = g .* (dFdExy - dg .*deta_ve.dExy);
dlamdP        = g .* (dFdP                        );
% deta_vp_dE
deta_vp_dExx  = eta_vp./dg.*dlamdExx.*(1./n_vp-1);
deta_vp_dEyy  = eta_vp./dg.*dlamdEyy.*(1./n_vp-1);
deta_vp_dEzz  = eta_vp./dg.*dlamdEzz.*(1./n_vp-1);
deta_vp_dExy  = eta_vp./dg.*dlamdExy.*(1./n_vp-1);
deta_vp_dP    = eta_vp./dg.*dlamdP  .*(1./n_vp-1);
% deta_vep_dE
a             = (dHddg + eta_vp + K.*dt.*sin(psi).*sin(phi));
deta_vep.dExx = -Exx.*Tyield./(4*Eii.^3) + (a.*dlamdExx + dg.*deta_vp_dExx)./(2*Eii);
deta_vep.dEyy = -Eyy.*Tyield./(4*Eii.^3) + (a.*dlamdEyy + dg.*deta_vp_dEyy)./(2*Eii);
deta_vep.dEzz = -Ezz.*Tyield./(4*Eii.^3) + (a.*dlamdEzz + dg.*deta_vp_dEzz)./(2*Eii);
deta_vep.dExy = -Exy.*Tyield./(2*Eii.^3) + (a.*dlamdExy + dg.*deta_vp_dExy)./(2*Eii);
deta_vep.dP   =                (sin(phi) +  a.*dlamdP   + dg.*deta_vp_dP  )./(2*Eii); 
deta_vep.dExx(pl==0) = deta_ve.dExx(pl==0);
deta_vep.dEyy(pl==0) = deta_ve.dEyy(pl==0);
deta_vep.dEzz(pl==0) = deta_ve.dEzz(pl==0);
deta_vep.dExy(pl==0) = deta_ve.dExy(pl==0);
deta_vep.dP(pl==0)   = deta_ve.dP(pl==0);
% ddivp
% dQdP       = -sin(psi);
% d2QdExxdP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdExx;
% d2QdEyydP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdEyy;
% d2QdEzzdP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdEzz;
% d2QdExydP  = -dt.*dQ.*hpsi.*cos(psi).*dlamdExy;
% d2QdPdP    = -dt.*dQ.*hpsi.*cos(psi).*dlamdP;

dQdP       = -sin(psi);
d2QdExxdP  = -dpsiddg.*cos(psi).*dlamdExx;
d2QdEyydP  = -dpsiddg.*cos(psi).*dlamdEyy;
d2QdEzzdP  = -dpsiddg.*cos(psi).*dlamdEzz;
d2QdExydP  = -dpsiddg.*cos(psi).*dlamdExy;
d2QdPdP    = -dpsiddg.*cos(psi).*dlamdP;
ddivp.dExx = -dQdP.*dlamdExx - d2QdExxdP.*dg; ddivp.dExx(pl==0) = 0;
ddivp.dEyy = -dQdP.*dlamdEyy - d2QdEyydP.*dg; ddivp.dEyy(pl==0) = 0;
ddivp.dEzz = -dQdP.*dlamdEzz - d2QdEzzdP.*dg; ddivp.dEzz(pl==0) = 0;
ddivp.dExy = -dQdP.*dlamdExy - d2QdExydP.*dg; ddivp.dExy(pl==0) = 0;
ddivp.dP   = -dQdP.*dlamdP   - d2QdPdP  .*dg; ddivp.dP(pl==0)   = 0;
% Secure things
deta_vep.dExx(isinf(deta_vep.dExx)) = 0;
deta_vep.dExx(isnan(deta_vep.dExx)) = 0;
deta_vep.dEyy(isinf(deta_vep.dEyy)) = 0;
deta_vep.dEyy(isnan(deta_vep.dEyy)) = 0;
deta_vep.dEzz(isinf(deta_vep.dEzz)) = 0;
deta_vep.dEzz(isnan(deta_vep.dEzz)) = 0;
deta_vep.dExy(isinf(deta_vep.dExy)) = 0;
deta_vep.dExy(isnan(deta_vep.dExy)) = 0;
deta_vep.dP(isinf(deta_vep.dP))     = 0;
deta_vep.dP(isnan(deta_vep.dP))     = 0;
% Mess
Stuff.dg     = dg;
Stuff.dQdtxx = dQdtxx.*pl;
Stuff.dQdtyy = dQdtyy.*pl;
Stuff.dQdtxy = dQdtxy.*pl;
Stuff.dQdtzz = dQdtzz.*pl;
Stuff.dQdP   = dQdP.*pl;
Stuff.Jii    = Tii.^2;
Stuff.Txx    = Txx;
Stuff.Tyy    = Tyy;
Stuff.Txy    = Txy;
Stuff.Tzz    = Tzz;
Stuff.eta_t  = eta_ve;
Stuff.Pt     = Pc;
if sum(pl(:))>0
Stuff.H         = - K.*dt.*sin(psi).*sin(phi) - eta_vp./n_vp - dt.*(dhcoh.*dg + hcoh) - Pc.*dt.*(dhphi.*dg + hphi).*cos(phi); % for CTL
Stuff.H (pl==0) = 0;
else
Stuff.H         = 0;
end
end