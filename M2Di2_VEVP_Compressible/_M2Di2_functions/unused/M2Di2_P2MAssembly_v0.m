function T2M = M2Di2_P2MAssembly_v0( BC, D, dx, dy, nx, ny, etac, Kc, dt, NumVx, NumVyG, NumTe, Tc, SuiteSparse )
D41c = D.D41c;%./(Kc*dt);
D42c = D.D42c;%./(Kc*dt);
D43c = D.D43c;%./(Kc*dt);
D45c = D.D45c;%./(Kc*dt);
% BC flags
fs = zeros(nx+1,ny+1);
fs(:,1) = BC.fsxS(:,1);  fs(:,end) = BC.fsxN(:,end);
fs(1,:) = BC.fsyW(1,:);  fs(end,:) = BC.fsyE(end,:);
ns = zeros(nx+1,ny+1);
ns(:,1) = BC.nsxS(:,1);  ns(:,end) = BC.nsxN(:,end);
ns(1,:) = BC.nsyW(1,:);  ns(end,:) = BC.nsyE(end,:);
fsSW = fs(1:end-1,1:end-1);
fsSE = fs(2:end-0,1:end-1);
fsNW = fs(1:end-1,2:end-0);
fsNE = fs(2:end-0,2:end-0);
nsSW = ns(1:end-1,1:end-1);
nsSE = ns(2:end-0,1:end-1);
nsNW = ns(1:end-1,2:end-0);
nsNE = ns(2:end-0,2:end-0);

% Coefficients
% c3VxW  = -D41c.*(-2/3/dx) - D42c.*( 1/3/dx);
% c3VxE  = -D41c.*( 2/3/dx) - D42c.*(-1/3/dx);
% c3VyS  = -D41c.*( 1/3/dy) - D42c.*(-2/3/dy);
% c3VyN  = -D41c.*(-1/3/dy) - D42c.*( 2/3/dy);
% c3VxSW = -0*(N2 .* (1 - ck) .* (1 - fsSW) .* (-1 + nsSW) ./ dy .* D43c) ./ 0.4e1;
% c3VxSE = -0*(N2 .* (1 - ck) .* (1 - fsSE) .* (-1 + nsSE) ./ dy .* D43c) ./ 0.4e1;
% c3VxNW = -0*(N2 .* (1 - ck) .* (1 - fsNW) .* (1 - nsNW) ./ dy .* D43c) ./ 0.4e1;
% c3VxNE = -0*(N2 .* (1 - ck) .* (1 - fsNE) .* (1 - nsNE) ./ dy .* D43c) ./ 0.4e1;
% c3VySW = -0*(N2 .* (1 - ck) .* (1 - fsSW) .* (-1 + nsSW) ./ dx .* D43c) ./ 0.4e1;
% c3VySE = -0*(N2 .* (1 - ck) .* (1 - fsSE) .* (1 - nsSE) ./ dx .* D43c) ./ 0.4e1;
% c3VyNW = -0*(N2 .* (1 - ck) .* (1 - fsNW) .* (-1 + nsNW) ./ dx .* D43c) ./ 0.4e1;
% c3VyNE = -0*(N2 .* (1 - ck) .* (1 - fsNE) .* (1 - nsNE) ./ dx .* D43c) ./ 0.4e1;

% c3VxW  = -1 .* (1 - 0) .* (-0.2e1 ./ 0.3e1 ./ dx .* D41c + 0.1e1 ./ dx .* D42c ./ 0.3e1 + (((1 - fsNW) .* (-1 - nsNW) ./ dy) ./ 0.4e1 + ((1 - fsSW) .* (1 + nsSW) ./ dy) ./ 0.4e1) .* D43c);
% c3VxE  = -1 .* (1 - 0) .* (0.2e1 ./ 0.3e1 ./ dx .* D41c - 0.1e1 ./ dx .* D42c ./ 0.3e1 + (((1 - fsNE) .* (-1 - nsNE) ./ dy) ./ 0.4e1 + ((1 - fsSE) .* (1 + nsSE) ./ dy) ./ 0.4e1) .* D43c);
% c3VyS  = -1 .* (1 - 0) .* (0.1e1 ./ dy .* D41c ./ 0.3e1 - 0.2e1 ./ 0.3e1 ./ dy .* D42c + (((1 - fsSW) .* (1 + nsSW) ./ dx) ./ 0.4e1 + ((1 - fsSE) .* (-1 - nsSE) ./ dx) ./ 0.4e1) .* D43c);
% c3VyN  = -1 .* (1 - 0) .* (-0.1e1 ./ dy .* D41c ./ 0.3e1 + 0.2e1 ./ 0.3e1 ./ dy .* D42c + (((1 - fsNW) .* (1 + nsNW) ./ dx) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dx) ./ 0.4e1) .* D43c);
% c3VxSW = -(1 .* (1 - 0) .* (1 - fsSW) .* (-1 + nsSW) ./ dy .* D43c) ./ 0.4e1;
% c3VxSE = -(1 .* (1 - 0) .* (1 - fsSE) .* (-1 + nsSE) ./ dy .* D43c) ./ 0.4e1;
% c3VxNW = -(1 .* (1 - 0) .* (1 - fsNW) .* (1 - nsNW) ./ dy .* D43c) ./ 0.4e1;
% c3VxNE = -(1 .* (1 - 0) .* (1 - fsNE) .* (1 - nsNE) ./ dy .* D43c) ./ 0.4e1;
% c3VySW = -(1 .* (1 - 0) .* (1 - fsSW) .* (-1 + nsSW) ./ dx .* D43c) ./ 0.4e1;
% c3VySE = -(1 .* (1 - 0) .* (1 - fsSE) .* (1 - nsSE) ./ dx .* D43c) ./ 0.4e1;
% c3VyNW = -(1 .* (1 - 0) .* (1 - fsNW) .* (-1 + nsNW) ./ dx .* D43c) ./ 0.4e1;
% c3VyNE = -(1 .* (1 - 0) .* (1 - fsNE) .* (1 - nsNE) ./ dx .* D43c) ./ 0.4e1;

% Kc  = 1;
% dt = 1;
% c3VxW = (-0.4e1 ./ 0.3e1 .* D41c .* etac ./ dx + 0.2e1 ./ 0.3e1 .* D42c .* etac ./ dx + 0.2e1 .* D43c .* etac .* (((1 - fsSW) .* (1 + nsSW) ./ dy) ./ 0.4e1 + ((1 - fsNW) .* (-1 - nsNW) ./ dy) ./ 0.4e1)) ./ Kc ./ dt - 0.1e1 ./ dx;
% c3VxE = ( 0.4e1 ./ 0.3e1 .* D41c .* etac ./ dx - 0.2e1 ./ 0.3e1 .* D42c .* etac ./ dx + 0.2e1 .* D43c .* etac .* (((1 - fsSE) .* (1 + nsSE) ./ dy) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dy) ./ 0.4e1)) ./ Kc ./ dt + 0.1e1 ./ dx;
% c3VyS = ( 0.2e1 ./ 0.3e1 .* D41c .* etac ./ dy - 0.4e1 ./ 0.3e1 .* D42c .* etac ./ dy + 0.2e1 .* D43c .* etac .* (((1 - fsSW) .* (1 + nsSW) ./ dx) ./ 0.4e1 + ((1 - fsSE) .* (-1 + nsSE) ./ dx) ./ 0.4e1)) ./ Kc ./ dt - 0.1e1 ./ dy;
% c3VyN = (-0.2e1 ./ 0.3e1 .* D41c .* etac ./ dy + 0.4e1 ./ 0.3e1 .* D42c .* etac ./ dy + 0.2e1 .* D43c .* etac .* (((1 - fsNW) .* (1 - nsNW) ./ dx) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dx) ./ 0.4e1)) ./ Kc ./ dt + 0.1e1 ./ dy;
% c3VxSW = (D43c .* etac .* (1 - fsSW) .* (-1 + nsSW) ./ dy ./ Kc ./ dt) ./ 0.2e1;
% c3VxSE = (D43c .* etac .* (1 - fsSE) .* (-1 + nsSE) ./ dy ./ Kc ./ dt) ./ 0.2e1;
% c3VxNW = (D43c .* etac .* (1 - fsNW) .* ( 1 - nsNW) ./ dy ./ Kc ./ dt) ./ 0.2e1;
% c3VxNE = (D43c .* etac .* (1 - fsNE) .* ( 1 - nsNE) ./ dy ./ Kc ./ dt) ./ 0.2e1;
% c3VySW = (D43c .* etac .* (1 - fsSW) .* (-1 + nsSW) ./ dx ./ Kc ./ dt) ./ 0.2e1;
% c3VySE = (D43c .* etac .* (1 - fsSE) .* ( 1 + nsSE) ./ dx ./ Kc ./ dt) ./ 0.2e1;
% c3VyNW = (D43c .* etac .* (1 - fsNW) .* (-1 - nsNW) ./ dx ./ Kc ./ dt) ./ 0.2e1;
% c3VyNE = (D43c .* etac .* (1 - fsNE) .* ( 1 - nsNE) ./ dx ./ Kc ./ dt) ./ 0.2e1;


% c3VxW = (-0.4e1 ./ 0.3e1 .* D41c .* etac ./ dx + 0.2e1 ./ 0.3e1 .* D42c .* etac ./ dx + 0.2e1 .* D43c .* etac .* (((1 - fsSW) .* (1 + nsSW) ./ dy) ./ 0.4e1 + ((1 - fsNW) .* (-1 - nsNW) ./ dy) ./ 0.4e1) + 0.2e1 .* D45c .* etac .* (((1 - fsSW) .* (1 + nsSW) ./ dy) ./ 0.4e1 + ((1 - fsNW) .* (-1 - nsNW) ./ dy) ./ 0.4e1)) ./ Kc ./ dt - 0.1e1 ./ dx;
% c3VxE = (0.4e1 ./ 0.3e1 .* D41c .* etac ./ dx - 0.2e1 ./ 0.3e1 .* D42c .* etac ./ dx + 0.2e1 .* D43c .* etac .* (((1 - fsSE) .* (1 + nsSE) ./ dy) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dy) ./ 0.4e1) + 0.2e1 .* D45c .* etac .* (((1 - fsSE) .* (1 + nsSE) ./ dy) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dy) ./ 0.4e1)) ./ Kc ./ dt + 0.1e1 ./ dx;
% c3VyS = (0.2e1 ./ 0.3e1 .* D41c .* etac ./ dy - 0.4e1 ./ 0.3e1 .* D42c .* etac ./ dy + 0.2e1 .* D43c .* etac .* (((1 - fsSW) .* (1 + nsSW) ./ dx) ./ 0.4e1 + ((1 - fsSE) .* (-1 + nsSE) ./ dx) ./ 0.4e1) + 0.2e1 .* D45c .* etac .* (((1 - fsSW) .* (1 + nsSW) ./ dx) ./ 0.4e1 + ((1 - fsSE) .* (-1 + nsSE) ./ dx) ./ 0.4e1)) ./ Kc ./ dt - 0.1e1 ./ dy;
% c3VyN = (-0.2e1 ./ 0.3e1 .* D41c .* etac ./ dy + 0.4e1 ./ 0.3e1 .* D42c .* etac ./ dy + 0.2e1 .* D43c .* etac .* (((1 - fsNW) .* (1 - nsNW) ./ dx) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dx) ./ 0.4e1) + 0.2e1 .* D45c .* etac .* (((1 - fsNW) .* (1 - nsNW) ./ dx) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dx) ./ 0.4e1)) ./ Kc ./ dt + 0.1e1 ./ dy;
% c3VxSW = ((D43c .* etac .* (1 - fsSW) .* (-1 + nsSW) ./ dy) ./ 0.2e1 + (D45c .* etac .* (1 - fsSW) .* (-1 + nsSW) ./ dy) ./ 0.2e1) ./ Kc ./ dt;
% c3VxSE = ((D43c .* etac .* (1 - fsSE) .* (-1 + nsSE) ./ dy) ./ 0.2e1 + (D45c .* etac .* (1 - fsSE) .* (-1 + nsSE) ./ dy) ./ 0.2e1) ./ Kc ./ dt;
% c3VxNW = ((D43c .* etac .* (1 - fsNW) .* (1 - nsNW) ./ dy) ./ 0.2e1 + (D45c .* etac .* (1 - fsNW) .* (1 - nsNW) ./ dy) ./ 0.2e1) ./ Kc ./ dt;
% c3VxNE = ((D43c .* etac .* (1 - fsNE) .* (1 - nsNE) ./ dy) ./ 0.2e1 + (D45c .* etac .* (1 - fsNE) .* (1 - nsNE) ./ dy) ./ 0.2e1) ./ Kc ./ dt;
% c3VySW = ((D43c .* etac .* (1 - fsSW) .* (-1 + nsSW) ./ dx) ./ 0.2e1 + (D45c .* etac .* (1 - fsSW) .* (-1 + nsSW) ./ dx) ./ 0.2e1) ./ Kc ./ dt;
% c3VySE = ((D43c .* etac .* (1 - fsSE) .* (1 + nsSE) ./ dx) ./ 0.2e1 + (D45c .* etac .* (1 - fsSE) .* (1 + nsSE) ./ dx) ./ 0.2e1) ./ Kc ./ dt;
% c3VyNW = ((D43c .* etac .* (1 - fsNW) .* (-1 - nsNW) ./ dx) ./ 0.2e1 + (D45c .* etac .* (1 - fsNW) .* (-1 - nsNW) ./ dx) ./ 0.2e1) ./ Kc ./ dt;
% c3VyNE = ((D43c .* etac .* (1 - fsNE) .* (1 - nsNE) ./ dx) ./ 0.2e1 + (D45c .* etac .* (1 - fsNE) .* (1 - nsNE) ./ dx) ./ 0.2e1) ./ Kc ./ dt;
% 
% c3VxW  = -0.2e1 ./ 0.3e1 .* D41c ./ dx + D42c ./ dx ./ 0.3e1 + D43c .* (((1 - fsSW) .* (1 + nsSW) ./ dy) ./ 0.4e1 + ((1 - fsNW) .* (-1 - nsNW) ./ dy) ./ 0.4e1) + D45c .* (((1 - fsSW) .* (1 + nsSW) ./ dy) ./ 0.4e1 + ((1 - fsNW) .* (-1 - nsNW) ./ dy) ./ 0.4e1);
% c3VxE  = 0.2e1 ./ 0.3e1 .* D41c ./ dx - D42c ./ dx ./ 0.3e1 + D43c .* (((1 - fsSE) .* (1 + nsSE) ./ dy) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dy) ./ 0.4e1) + D45c .* (((1 - fsSE) .* (1 + nsSE) ./ dy) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dy) ./ 0.4e1);
% c3VyS  =  D41c ./ dy ./ 0.3e1 - 0.2e1 ./ 0.3e1 .* D42c ./ dy + D43c .* (((1 - fsSW) .* (1 + nsSW) ./ dx) ./ 0.4e1 + ((1 - fsSE) .* (-1 + nsSE) ./ dx) ./ 0.4e1) + D45c .* (((1 - fsSW) .* (1 + nsSW) ./ dx) ./ 0.4e1 + ((1 - fsSE) .* (-1 + nsSE) ./ dx) ./ 0.4e1);
% c3VyN  = -D41c ./ dy ./ 0.3e1 + 0.2e1 ./ 0.3e1 .* D42c ./ dy + D43c .* (((1 - fsNW) .* (1 - nsNW) ./ dx) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dx) ./ 0.4e1) + D45c .* (((1 - fsNW) .* (1 - nsNW) ./ dx) ./ 0.4e1 + ((1 - fsNE) .* (-1 - nsNE) ./ dx) ./ 0.4e1);
% c3VxSW = (D43c .* (1 - fsSW) .* (-1 + nsSW) ./ dy) ./ 0.4e1 + (D45c .* (1 - fsSW) .* (-1 + nsSW) ./ dy) ./ 0.4e1;
% c3VxSE = (D43c .* (1 - fsSE) .* (-1 + nsSE) ./ dy) ./ 0.4e1 + (D45c .* (1 - fsSE) .* (-1 + nsSE) ./ dy) ./ 0.4e1;
% c3VxNW = (D43c .* (1 - fsNW) .* (1 - nsNW) ./ dy) ./ 0.4e1 + (D45c .* (1 - fsNW) .* (1 - nsNW) ./ dy) ./ 0.4e1;
% c3VxNE = (D43c .* (1 - fsNE) .* (1 - nsNE) ./ dy) ./ 0.4e1 + (D45c .* (1 - fsNE) .* (1 - nsNE) ./ dy) ./ 0.4e1;
% c3VySW = (D43c .* (1 - fsSW) .* (-1 + nsSW) ./ dx) ./ 0.4e1 + (D45c .* (1 - fsSW) .* (-1 + nsSW) ./ dx) ./ 0.4e1;
% c3VySE = (D43c .* (1 - fsSE) .* (1 + nsSE) ./ dx) ./ 0.4e1 + (D45c .* (1 - fsSE) .* (1 + nsSE) ./ dx) ./ 0.4e1;
% c3VyNW = (D43c .* (1 - fsNW) .* (-1 - nsNW) ./ dx) ./ 0.4e1 + (D45c .* (1 - fsNW) .* (-1 - nsNW) ./ dx) ./ 0.4e1;
% c3VyNE = (D43c .* (1 - fsNE) .* (1 - nsNE) ./ dx) ./ 0.4e1 + (D45c .* (1 - fsNE) .* (1 - nsNE) ./ dx) ./ 0.4e1;

c3VxW = (1/4*D43c.*dx.*(fsNW.*nsNW + fsNW - fsSW.*nsSW - fsSW - nsNW + nsSW) + dy.*(-2/3*D41c + 1/3*D42c + 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VxE = (1/4*D43c.*dx.*(fsNE.*nsNE + fsNE - fsSE.*nsSE - fsSE - nsNE + nsSE) + dy.*(2/3*D41c - 1/3*D42c - 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VyS = (1/4*D43c.*dy.*(fsSE.*nsSE + fsSE - fsSW.*nsSW - fsSW - nsSE + nsSW) + dx.*(1/3*D41c - 2/3*D42c + 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VyN = (1/4*D43c.*dy.*(fsNE.*nsNE + fsNE - fsNW.*nsNW - fsNW - nsNE + nsNW) + dx.*(-1/3*D41c + 2/3*D42c - 1/3*D45c))./(Kc.*dt.*dx.*dy);
c3VxSW = 1/4*D43c.*(-fsSW.*nsSW + fsSW + nsSW - 1)./(Kc.*dt.*dy);
c3VxSE = 1/4*D43c.*(-fsSE.*nsSE + fsSE + nsSE - 1)./(Kc.*dt.*dy);
c3VxNW = 1/4*D43c.*(fsNW.*nsNW - fsNW - nsNW + 1)./(Kc.*dt.*dy);
c3VxNE = 1/4*D43c.*(fsNE.*nsNE - fsNE - nsNE + 1)./(Kc.*dt.*dy);
c3VySW = 1/4*D43c.*(-fsSW.*nsSW + fsSW + nsSW - 1)./(Kc.*dt.*dx);
c3VySE = 1/4*D43c.*(fsSE.*nsSE - fsSE - nsSE + 1)./(Kc.*dt.*dx);
c3VyNW = 1/4*D43c.*(-fsNW.*nsNW + fsNW + nsNW - 1)./(Kc.*dt.*dx);
c3VyNE = 1/4*D43c.*(fsNE.*nsNE - fsNE - nsNE + 1)./(Kc.*dt.*dx);

c3VxW(1,:)   = 0;
c3VxSW(1,:)  = 0;
c3VxNW(1,:)  = 0;
c3VxE(end,:) = 0;
c3VxSE(end,:)= 0;
c3VxNE(end,:)= 0;
c3VyS(:,1)   = 0;
c3VySW(:,1)  = 0;
c3VySE(:,1)  = 0;
c3VyN(:,end) = 0;
c3VyNW(:,end)= 0;
c3VyNE(:,end)= 0;
% Equation indexes
iTe   = NumTe(:);
iVxW  = NumVx(1:end-1,:); iVyS  = NumVyG(:,1:end-1);
iVxE  = NumVx(2:end  ,:); iVyN  = NumVyG(:,2:end,:);
iVxSW = ones(size(Tc));  iVxSW(:,2:end  ) = NumVx (1:end-1, 1:end-1);
iVxSE = ones(size(Tc));  iVxSE(:,2:end  ) = NumVx (2:end  , 1:end-1);
iVxNW = ones(size(Tc));  iVxNW(:,1:end-1) = NumVx (1:end-1, 2:end  );
iVxNE = ones(size(Tc));  iVxNE(:,1:end-1) = NumVx (2:end  , 2:end  );
iVySW = ones(size(Tc));  iVySW(2:end  ,:) = NumVyG(1:end-1, 1:end-1);
iVySE = ones(size(Tc));  iVySE(1:end-1,:) = NumVyG(2:end  , 1:end-1);
iVyNW = ones(size(Tc));  iVyNW(2:end  ,:) = NumVyG(1:end-1, 2:end  );
iVyNE = ones(size(Tc));  iVyNE(1:end-1,:) = NumVyG(2:end  , 2:end  );
%% Assembly of the sparse matrix
It2m   = [   iTe(:);  iTe(:);  iTe(:);  iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:);   iTe(:)  ]';
Jt2m   = [  iVxW(:); iVxE(:); iVyS(:); iVyN(:); iVxSW(:); iVxSE(:); iVxNW(:); iVxNE(:); iVySW(:); iVySE(:); iVyNW(:); iVyNE(:)  ]';
Vt2m   = [ c3VxW(:); c3VxE(:); c3VyS(:); c3VyN(:); c3VxSW(:); c3VxSE(:); c3VxNW(:); c3VxNE(:); c3VySW(:); c3VySE(:); c3VyNW(:); c3VyNE(:)  ]';
if SuiteSparse == 1, T2M    = sparse2( It2m, Jt2m, Vt2m );
else                 T2M    = sparse ( It2m, Jt2m, Vt2m );
end
end