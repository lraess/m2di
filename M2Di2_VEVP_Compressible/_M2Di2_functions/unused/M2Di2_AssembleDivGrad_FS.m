function [ grad, div, BcD ] = M2Di2_AssembleDivGrad_FS( BC, dx, dy, nx, ny,NumVx, NumVyG, NumPt, SuiteSparse )
% Normal stress
frxN = BC.frxN; fryN = BC.fryN;
free_surf = 0;
if (sum(frxN(:,end))>0), free_surf = 1; end
iVxC   = NumVx;                                                              % dP/dx
% iPtW   =  ones(size(iVxC));    iPtW(1:end-1,:) = NumPt;
% iPtE   =  ones(size(iVxC));    iPtE(2:end-0 ,:) = NumPt;
iPtW   =  ones(size(iVxC));    iPtW(2:end-0,:) = NumPt;
iPtE   =  ones(size(iVxC));    iPtE(1:end-1 ,:) = NumPt;
cPtW   = -ones(size(iVxC))/dx; cPtW([1 end],:) = 0;
cPtE   =  ones(size(iVxC))/dx; cPtE([1 end],:) = 0;
Idx    = [ iVxC(:); iVxC(:) ]';
Jdx    = [ iPtW(:); iPtE(:) ]';
Vdx    = [ cPtW(:); cPtE(:) ]';
iVyC   = NumVyG;                                                             % dP/dy
% iPtS   =  ones(size(iVyC));    iPtS(:,1:end-1 ) = NumPt;
% iPtN   =  ones(size(iVyC));    iPtN(:,2:end-0 ) = NumPt;
iPtS   =  ones(size(iVyC));    iPtS(:,2:end-0 ) = NumPt;
iPtN   =  ones(size(iVyC));    iPtN(:,1:end-1 ) = NumPt;
cPtS   = -ones(size(iVyC))/dy;
cPtN   =  ones(size(iVyC))/dy;
if free_surf==0
    cPtS(:,[1 end]) = 0;
    cPtN(:,[1 end]) = 0;
else
    cPtS(:,[1    ]) = 0;
    cPtN(:,[1 end]) = 0;
end
Idy    = [ iVyC(:); iVyC(:) ]';
Jdy    = [ iPtS(:); iPtN(:) ]';
Vdy    = [ cPtS(:); cPtN(:) ]';
%% Assemble grad and divV
if SuiteSparse==1, grad = sparse2( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny );
else               grad = sparse ( [Idx(:); Idy(:)], [Jdx(:); Jdy(:)], [Vdx(:); Vdy(:)], (nx+1)*ny+(ny+1)*nx, nx*ny ); end
div    = -grad';
%% Build BC's for divergence
if free_surf==0
    BcD = zeros(size(div,1),1);
    BcD(NumPt( 1 , : )) = BcD(NumPt(1  ,:  )) + 1/dx*BC.Ux_W;
    BcD(NumPt(end, : )) = BcD(NumPt(end,:  )) - 1/dx*BC.Ux_E;
    BcD(NumPt(:  , 1 )) = BcD(NumPt(:  ,1  )) + 1/dy*BC.Uy_S;
    BcD(NumPt(:  ,end)) = BcD(NumPt(:  ,end)) - 1/dy*BC.Uy_N;
else
    BcD = zeros(size(div,1),1);
    BcD(NumPt( 1 , : )) = BcD(NumPt(1  ,:  )) + 1/dx*BC.Ux_W;
    BcD(NumPt(end, : )) = BcD(NumPt(end,:  )) - 1/dx*BC.Ux_E;
    BcD(NumPt(:  , 1 )) = BcD(NumPt(:  ,1  )) + 1/dy*BC.Uy_S;
end