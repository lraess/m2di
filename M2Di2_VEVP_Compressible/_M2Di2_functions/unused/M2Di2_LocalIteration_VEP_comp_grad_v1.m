function [ eta_vep, K_vep, Txx, Tyy, Tzz, Txy, Pc, divp, dive, deta_ve, deta_vep, ddivp, pl, eta_vp, Eii_el, Eii_pwl, Eii_pl, Overstress, Stuff, E2pl, C, phi, psi, gdot, dQ ] = M2Di2_LocalIteration_VEP_comp_grad_v1( c, dx, dy, SuiteSparse, litmax, tol, noisy, div, Exx, Eyy, Ezz, Exy, P, Txxo, Tyyo, Tzzo, Txyo, B_pwl, m_pwl, eta_el, K, P0, phi, psi, C, eta_vp, eta_vp0, n_vp, dt, E2pl0, Coh, C0, Phi, phi0, Psi, psi0, expl_soft  )

% For GRL setup, yield is: Ty = C + sin(phi)*P;

% Yield was modified to Ty = cos(phi)*C + sin(phi)*P;
% Softening function updated 

% This one works with power law VP and cohesion softening
% Initialise
Eii_el     = 0;
Eii_pwl    = 0;
Eii_pl     = 0;
Overstress = 0;
E2pl       = E2pl0;
K_vep      = K;
divp       = 0;
dive       = 0;
dHdgdot    = 0;
hpsi       = 0;
hcoh       = 0;

% Strain rate components and invariant
exx    = Exx;
eyy    = Eyy;
ezz    = Ezz;
exy    = Exy;
eII    = sqrt(1/2*(exx.^2 + eyy.^2 + ezz.^2) + exy.^2);

% Viscoelastic strain rate components and invariant
Exx    = Exx + Txxo./2./eta_el;
Eyy    = Eyy + Tyyo./2./eta_el;
Ezz    = Ezz + Tzzo./2./eta_el;
Exy    = Exy + Txyo./2./eta_el;
Div    = div - P0./K./dt;
% Pc     = -K.*dt.*Div;
Pc     = P;
Eii    = sqrt(1/2*(Exx.^2 + Eyy.^2 + Ezz.^2) + Exy.^2);

% Use n instead of m
n_pwl   = 1./(2*m_pwl+1);
C_pwl   = (2.*B_pwl).^(-n_pwl);

% Isolated viscosities
eta_pwl = B_pwl.*eII.^(1./n_pwl-1);

% Define viscosity bounds
eta_up = eta_pwl;
eta_up = min(eta_up, eta_el);
eta_lo = 1./( 1./eta_pwl  + 1./eta_el );

% Start at midpoint (initial guess)
eta_ve     = 0.5*(eta_up+eta_lo);

if noisy>2, fprintf('Starting Visco-Elastic local iterations:\n'); end

% Local iterations
for lit=1:litmax
    
    % Function evaluation at current effective viscosity
    Tii       = 2*eta_ve.*Eii;
    Eii_pwl_0 = C_pwl.*Tii.^(n_pwl);  
    Eii_vis_0 = Eii_pwl_0;
    r_eta_0   = Eii - Tii./(2*eta_el) - Eii_vis_0;
    
    % Residual check
    res = max(max(abs(r_eta_0)));
    if lit == 1; res0 = res; end
    if noisy>2, fprintf('It. %02d, r abs. = %2.2e r rel. = %2.2e\n', lit, res, res/res0); end
    if res/res0 < tol || res<tol
        break;
    end
    
    % Exact derivative
    drdeta = -2*Eii./(2*eta_el) - C_pwl .* Tii.^n_pwl.*n_pwl./eta_ve;
    
    % Update viscosity
    eta_ve  = eta_ve - r_eta_0 ./ drdeta;
    
end

% Update individual deviatoric stress components
Txx = 2*eta_ve.*Exx;
Tyy = 2*eta_ve.*Eyy;
Tzz = 2*eta_ve.*Ezz;
Txy = 2*eta_ve.*Exy;
Tii = 2*eta_ve.*Eii;
% For analytical Jacobian --- VE  --- ViscosityDerivativesTest.py
Jii = Tii.^2;
detadTxx = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Txx.*eta_ve.^2.*(n_pwl - 1);
detadTyy = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Tyy.*eta_ve.^2.*(n_pwl - 1);
detadTxy = -2.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Txy.*eta_ve.^2.*(n_pwl - 1);
detadTzz = -1.0*C_pwl.*Jii.^(n_pwl/2 - 3/2).*Tzz.*eta_ve.^2.*(n_pwl - 1);

deta_ve.dExx = detadTxx .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dEyy = detadTyy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dExy = detadTxy .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dEzz = detadTzz .* 2.*eta_ve ./ (1 - 2.*(detadTxx.*Exx + detadTyy.*Eyy + detadTxy.*Exy + detadTzz.*Ezz));
deta_ve.dP   = zeros(size(Txx));
% Secure things
deta_ve.dExx(isinf(deta_ve.dExx)) = 0; deta_ve.dExx(isnan(deta_ve.dEyy)) = 0;
deta_ve.dEyy(isinf(deta_ve.dEyy)) = 0; deta_ve.dEyy(isnan(deta_ve.dEyy)) = 0;
deta_ve.dExy(isinf(deta_ve.dExy)) = 0; deta_ve.dExy(isnan(deta_ve.dExy)) = 0;
deta_ve.dEzz(isinf(deta_ve.dEzz)) = 0; deta_ve.dEzz(isnan(deta_ve.dEzz)) = 0;

% Plasticity

% CHECK for negative pressures
np  = P<0;
nnp = sum(np(:));
if nnp>0 && noisy>2, 
    fprintf('Achtung baby: there are %06d nodes with negative pressure in the box!\n', nnp);
end

% Total stress 
Sxx = -P + Txx;
Syy = -P + Tyy;
Szz = -P + Tzz;
%P2  = -1/3*(Sxx+Syy+Szz);
Tyield         = C.*cos(phi) + P.*sin(phi);
% b = C.*cos(phi);
X              = eta_ve./eta_el;
F              = Tii - Tyield;     F0 = F;
pl             = F >= 0;
dQdtxx         = Txx./2./Tii;
dQdtyy         = Tyy./2./Tii;
dQdtzz         = Tzz./2./Tii;
dQdtxy         = Txy./1./Tii;
dQ             = sqrt(2/3)*sqrt( 1/2*(dQdtxx).^2 + 1/2*(dQdtyy).^2 + 1/2*(dQdtzz).^2 + (dQdtxy/2).^2 );
gdot           = zeros(size(Txx));
eta_vp         = zeros(size(Txx));
eta_vp(pl==1)  = eta_vp0*(eII(pl==1)).^(1/n_vp-1);
gdot(pl==1)    = F(pl==1)./(eta_ve(pl==1) + eta_vp(pl==1) + K(pl==1).*dt.*sin(phi(pl==1)).*sin(psi(pl==1)) );
res0           = max(F(:));
Fc             = F;
pl             = F>0.0;


E2pl           = E2pl0 + dQ.*gdot.*dt;
nx  = size(pl,1); ny  = size(pl,2);
Num = reshape(1:(nx)*ny,nx,ny  );
BC.PfNeuW = zeros(nx,ny); BC.PfNeuW(  1,:) = 1;
BC.PfNeuE = zeros(nx,ny); BC.PfNeuE(end,:) = 1;
BC.PfNeuS = zeros(nx,ny); BC.PfNeuS(:,  1) = 1;
BC.PfNeuN = zeros(nx,ny); BC.PfNeuN(:,end) = 1;
BC.PfDirW = zeros(nx,ny); %BC.PfDirW(  1,:) = 1;
BC.PfDirE = zeros(nx,ny); %BC.PfDirE(end,:) = 1;
BC.PfDirS = zeros(nx,ny); %BC.PfDirS(:,  1) = 1;
BC.PfDirN = zeros(nx,ny); %BC.PfDirN(:,end) = 1;

plC   = pl;
plW   = zeros(size(pl)); plW(2:end-0,:) = pl(1:end-1,:);
plE   = zeros(size(pl)); plE(1:end-1,:) = pl(2:end-0,:);
plS   = zeros(size(pl)); plS(:,2:end-0) = pl(:,1:end-1);
plN   = zeros(size(pl)); plN(:,1:end-1) = pl(:,2:end-0);

PfC   = E2pl;
PfW   = zeros(size(pl)); PfW(2:end-0,:) = E2pl(1:end-1,:);
PfE   = zeros(size(pl)); PfE(1:end-1,:) = E2pl(2:end-0,:);
PfS   = zeros(size(pl)); PfS(:,2:end-0) = E2pl(:,1:end-1);
PfN   = zeros(size(pl)); PfN(:,1:end-1) = E2pl(:,2:end-0);

k11_W = c;% kx(1:end-1,:);
k11_E = c;%kx(2:end-0,:);
k22_S = c;%ky(:,1:end-1);
k22_N = c;%ky(:,2:end-0);

PfW1 = (1-BC.PfDirW).*PfW + BC.PfDirW.*(-PfC);
PfE1 = (1-BC.PfDirE).*PfE + BC.PfDirE.*(-PfC);
PfS1 = (1-BC.PfDirS).*PfS + BC.PfDirS.*(-PfC);
PfN1 = (1-BC.PfDirN).*PfN + BC.PfDirN.*(-PfC);
qxW  = -(1-plW).*(1-BC.PfNeuW).*k11_W .* (PfC - PfW1) / dx;
qxE  = -(1-plE).*(1-BC.PfNeuW).*k11_E .* (PfE1 - PfC) / dx;
qyS  = -(1-plS).*(1-BC.PfNeuS).*k22_S .* (PfC - PfS1) / dy;
qyN  = -(1-plN).*(1-BC.PfNeuN).*k22_N .* (PfN1 - PfC) / dy;
grad = plC.* ((qxE - qxW) / dx + (qyN - qyS) / dy) + (1-plC).*PfC; 

divp           = gdot.*sin(psi);
Pc             = P + pl.*K.*dt.*divp;
Tyield         = C.*cos(phi) + Pc.*sin(phi);

Fr   = Tii - pl.*eta_ve.*gdot - Tyield + pl.*grad;


[KM] = M2Di2_DiffusionAssembly_v4_steady_grad_plasticity( -1*c, pl, dQ.*dt, Num, BC, dx, dy, dt, nx, ny, E2pl, SuiteSparse );

diag =  -( - eta_ve - K.*dt.*sin(psi).*sin(phi) ); %- eta_vp./n_vp 
iPt   = Num;  I = iPt(:)';  J = I;                                     % Eq. index center (pressure diagonal)
V     = diag;                                                          % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V);                                % Matrix assembly
else               PP = sparse (I,J,V); end
KM    = KM + PP;

Fr(pl==0) = 0; % make sure no correction is added to non plastic points
ddg   = KM\Fr(:);
dgdot = reshape(ddg,nx,ny);
gdot  = gdot + dgdot;

E2pl  = E2pl0 + dQ.*gdot.*dt;
PfC   = E2pl;
PfW   = zeros(size(pl)); PfW(2:end-0,:) = E2pl(1:end-1,:);
PfE   = zeros(size(pl)); PfE(1:end-1,:) = E2pl(2:end-0,:);
PfS   = zeros(size(pl)); PfS(:,2:end-0) = E2pl(:,1:end-1);
PfN   = zeros(size(pl)); PfN(:,1:end-1) = E2pl(:,2:end-0);
PfW1 = (1-BC.PfDirW).*PfW + BC.PfDirW.*(-PfC);
PfE1 = (1-BC.PfDirE).*PfE + BC.PfDirE.*(-PfC);
PfS1 = (1-BC.PfDirS).*PfS + BC.PfDirS.*(-PfC);
PfN1 = (1-BC.PfDirN).*PfN + BC.PfDirN.*(-PfC);
qxW  = -(1-plW).*(1-BC.PfNeuW).*k11_W .* (PfC - PfW1) / dx;
qxE  = -(1-plE).*(1-BC.PfNeuW).*k11_E .* (PfE1 - PfC) / dx;
qyS  = -(1-plS).*(1-BC.PfNeuS).*k22_S .* (PfC - PfS1) / dy;
qyN  = -(1-plN).*(1-BC.PfNeuN).*k22_N .* (PfN1 - PfC) / dy;
grad = plC.* ((qxE - qxW) / dx + (qyN - qyS) / dy) + (1-plC).*PfC; 

divp           = gdot.*sin(psi);
Pc             = P + pl.*K.*dt.*divp;
Tyield         = C.*cos(phi) + Pc.*sin(phi);

Fr1   = Tii - pl.*eta_ve.*gdot - Tyield + pl.*grad;

% max(F(:))
% max(Fr(:))
% max(Fr1(:))
% 
% figure(201), clf
% subplot(211)
% imagesc(grad), colorbar
% subplot(212)
% imagesc(gdot), colorbar
% drawnow

if noisy>2, fprintf('Starting ViscoPlastic local iterations - F trial = %2.2e --- grad = %2.2e:\n', max(Fr(:)), norm(grad)); end

% if noisy>3, figure(19), clf, hold on; end
% if sum(pl(:)>0)
%     % Plastic strain rates
%     for lit=1:litmax
%         % Evaluate F
%         E2pl           = E2pl0 + dQ.*gdot.*dt;
%         if expl_soft==0, C              = Coh.ini - Coh.soft_on.*Coh.Delta/2 .* erfc( -(E2pl-Coh.Ein)./Coh.dE); end
%         if expl_soft==0, phi            = Phi.ini - Phi.soft_on.*Phi.Delta/2 .* erfc( -(E2pl-Phi.Ein)./Phi.dE); end
%         if expl_soft==0, psi            = Psi.ini - Psi.soft_on.*Psi.Delta/2 .* erfc( -(E2pl-Psi.Ein)./Psi.dE); end
%         hcoh           = Coh.soft_on.*dQ.*dt.*Coh.Delta.*exp(-(Coh.Ein-E2pl).^2./Coh.dE.^2)./(sqrt(pi)*Coh.dE);
%         hphi           = Phi.soft_on.*dQ.*dt.*Phi.Delta.*exp(-(Phi.Ein-E2pl).^2./Phi.dE.^2)./(sqrt(pi)*Phi.dE);
%         hpsi           = Psi.soft_on.*dQ.*dt.*Psi.Delta.*exp(-(Psi.Ein-E2pl).^2./Psi.dE.^2)./(sqrt(pi)*Psi.dE);
%         divp           = gdot.*sin(psi);
%         Pc             = P + K.*dt.*divp;
%         eta_vp(pl==1)  = eta_vp0 .* gdot(pl==1) .^ (1/n_vp-1);
%         Tyield         = C.*cos(phi) + Pc.*sin(phi) + eta_vp.*gdot;
%         Fc(pl==1)      = Tii(pl==1) - eta_ve(pl==1).*gdot(pl==1) - Tyield(pl==1);
%         % Residual check
%         Fc_pl = Fc(pl==1);
%         res   = max(Fc_pl(:)); 
%         if noisy>2, fprintf('It. %02d, r_rel = %2.2e r_abs = %2.2e\n', lit, res/res0, res ); end
%        
%         if noisy>3,
%             val = log10(res/res0);
%             if isinf(val), val = -15; end
%             plot(lit, val, '+k', 'LineWidth', 16); end
%         if res/res0 < tol || res < tol
%             break;
%         end
%         % Exact derivative
%         dHcoh    = -Coh.soft_on.*hcoh.*cos(phi);
%         dHphi    =  Phi.soft_on.*hphi.*(C.*sin(phi) - Pc.*cos(phi));
%         dHpsi    = -Psi.soft_on.*K.*hpsi.*dt.*gdot.*cos(psi).*sin(phi);
%         dHdgdot  = dHcoh  + dHphi + dHpsi;
%         dFdgdot  = - eta_ve - eta_vp./n_vp - K.*dt.*sin(psi).*sin(phi) - dHdgdot;
%         % Update plastic multiplier rate
%         gdot(pl==1)  = gdot(pl==1) - Fc(pl==1) ./ dFdgdot(pl==1);
%         gdot         = real(abs(gdot)); % MATLAB tends to spit out complex numbers
%     end
% end
% 
% max(gdot(:))
% 
% if noisy>3, drawnow; end

% Post-process
exx_pl  = gdot.*dQdtxx;
eyy_pl  = gdot.*dQdtyy;
ezz_pl  = gdot.*dQdtzz;
exy_pl  = gdot.*dQdtxy*(1/2);
% dE2pl   = sqrt( 1/2*(exx_pl*dt).^2 + 1/2*(eyy_pl*dt).^2 + 1/2*(ezz_pl*dt).^2 + (exy_pl*dt).^2 );
Txx     = 2.*eta_ve.*(exx-exx_pl) +  X.*Txxo;
Tyy     = 2.*eta_ve.*(eyy-eyy_pl) +  X.*Tyyo;
Tzz     = 2.*eta_ve.*(ezz-ezz_pl) +  X.*Tzzo;
Txy     = 2.*eta_ve.*(exy-exy_pl) +  X.*Txyo;
Tii     = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*Tzz.^2 + Txy.^2);
eta_vep = eta_ve;
eta_vep(pl==1) = Tii(pl==1)./2./Eii(pl==1);
K_vep(pl==1)   = -Pc(pl==1)./(dt.*Div(pl==1));

% Checks
if noisy>2
    
    % Check yield 1 after correction
    F1     = Tii - Tyield;

    % Double check wether effective viscosity is computed accurately
    Txx = 2*eta_vep.*Exx;
    Tyy = 2*eta_vep.*Eyy;
    Tzz = 2*eta_vep.*Ezz;
    Txy = 2*eta_vep.*Exy;
    Tii = sqrt(1/2*Txx.^2 + 1/2*Tyy.^2 + 1/2*Tzz.^2 + Txy.^2);
    Tii(pl==1)    = 2*eta_vep(pl==1).*Eii(pl==1);
    F2  = Tii - Tyield;
    
    % Correct power-law viscosity
    eta_pwl = B_pwl.*Eii_pwl_0.^(1./n_pwl-1);
    
    % 1: strain rate components
    exx_el  = (Txx-Txxo)./2./eta_el;
    exx_pwl = Txx./2./eta_pwl;
    exx_pl  = gdot.*dQdtxx;
    exx_net = norm(exx-exx_el-exx_pwl-exx_pl);
    
    eyy_el  = (Tyy-Tyyo)./2./eta_el;
    eyy_pwl = Tyy./2./eta_pwl;
    eyy_pl  = gdot.*dQdtyy;
    eyy_net = norm(eyy-eyy_el-eyy_pwl-eyy_pl);
    
    ezz_el  = (Tzz-Tzzo)./2./eta_el;
    ezz_pwl = Tzz./2./eta_pwl;
    ezz_pl  = gdot.*dQdtzz;
    ezz_net = norm(ezz-ezz_el-ezz_pwl-ezz_pl);
    
    exy_el  = (Txy-Txyo)./2./eta_el;
    exy_pwl = Txy./2./eta_pwl;
    exy_pl  = gdot.*dQdtxy*(1/2);
    exy_net = norm(exy-exy_el-exy_pwl-exy_pl);
    
    dive  = -(Pc  - P0)./(K*dt);
    div_net = div - dive - divp;  

    if sum(pl(:))>0, 
        gdot_chk = zeros(size(F));
%         gdot_chk(pl==1) = F(pl==1) ./ (eta_ve(pl==1) + eta_vp(pl==1) + dt*hcoh(pl==1).*cos(phi(pl==1)) + K(pl==1).*dt.*sin(psi(pl==1)).*sin(phi(pl==1)));
        gdot_chk(pl==1) = F(pl==1) ./ (eta_ve(pl==1) + eta_vp(pl==1) + K(pl==1).*dt.*sin(psi(pl==1)).*sin(phi(pl==1)) - grad(pl==1)./gdot(pl==1) );
    end
    
    if noisy>2 && sum(pl(:))>0
        Overstress  = eta_vp.*gdot;
        fprintf('F trial = %2.2e --- F corr1 = %2.2e --- F corr2 = %2.2e --- Over S. = %2.2e\n', max(F(:)), max(F1(:)), max(F2(:)), max(Overstress(:)))
        fprintf('net exx = %2.2e --- net eyy = %2.2e --- net ezz = %2.2e --- net exy = %2.2e\n', norm(exx_net),norm(eyy_net),norm(ezz_net),norm(exy_net))
        fprintf('net ekk = %2.2e --- net gdot = %2.2e\n', norm(div_net), norm(gdot - gdot_chk)); 
    end
        
    % 2: invariants
    Eii_el  = sqrt(1/2*exx_el .^2 + 1/2*eyy_el .^2 + 1/2*ezz_el .^2 + exy_el .^2);
    Eii_pwl = sqrt(1/2*exx_pwl.^2 + 1/2*eyy_pwl.^2 + 1/2*ezz_pwl.^2 + exy_pwl.^2);
    Eii_pl  = sqrt(1/2*exx_pl .^2 + 1/2*eyy_pl .^2 + 1/2*ezz_pl .^2 + exy_pl .^2);
    
end

dgraddlam = grad./gdot;

% gdot_step = 1e-5.*gdot;
% E2pl  = E2pl0 + dQ.*(gdot+gdot_step).*dt;
% PfC   = E2pl;
% PfW   = zeros(size(pl)); PfW(2:end-0,:) = E2pl(1:end-1,:);
% PfE   = zeros(size(pl)); PfE(1:end-1,:) = E2pl(2:end-0,:);
% PfS   = zeros(size(pl)); PfS(:,2:end-0) = E2pl(:,1:end-1);
% PfN   = zeros(size(pl)); PfN(:,1:end-1) = E2pl(:,2:end-0);
% PfW1 = (1-BC.PfDirW).*PfW + BC.PfDirW.*(-PfC);
% PfE1 = (1-BC.PfDirE).*PfE + BC.PfDirE.*(-PfC);
% PfS1 = (1-BC.PfDirS).*PfS + BC.PfDirS.*(-PfC);
% PfN1 = (1-BC.PfDirN).*PfN + BC.PfDirN.*(-PfC);
% qxW  = -(1-plW).*(1-BC.PfNeuW).*k11_W .* (PfC - PfW1) / dx;
% qxE  = -(1-plE).*(1-BC.PfNeuW).*k11_E .* (PfE1 - PfC) / dx;
% qyS  = -(1-plS).*(1-BC.PfNeuS).*k22_S .* (PfC - PfS1) / dy;
% qyN  = -(1-plN).*(1-BC.PfNeuN).*k22_N .* (PfN1 - PfC) / dy;
% grad_pert = plC.* ((qxE - qxW) / dx + (qyN - qyS) / dy) + (1-plC).*PfC; 
% dgraddlam_num = (grad_pert-grad)./gdot_step;

% figure(23), clf
% subplot(211)
% imagesc(dgraddlam), colorbar
% subplot(212)
% imagesc(dgraddlam_num), colorbar
% drawnow

% For analytical Jacobian --- VEP --- ViscosityDerivativesTest.py
% dFdE
dFdExx        =   Exx.*eta_ve./Eii + 2.*Eii.*deta_ve.dExx;
dFdEyy        =   Eyy.*eta_ve./Eii + 2.*Eii.*deta_ve.dEyy;
dFdEzz        =   Ezz.*eta_ve./Eii + 2.*Eii.*deta_ve.dEzz;
dFdExy        = 2*Exy.*eta_ve./Eii + 2.*Eii.*deta_ve.dExy;  
dFdP          = -sin(phi);
% dgdotdE
% g             =  1./ (eta_ve + eta_vp./n_vp + dHdgdot + K.*dt.*sin(psi).*sin(phi) );
% dlamdExx      = g .* (dFdExx - gdot .*deta_ve.dExx);
% dlamdEyy      = g .* (dFdEyy - gdot .*deta_ve.dEyy);
% dlamdEzz      = g .* (dFdEzz - gdot .*deta_ve.dEzz);
% dlamdExy      = g .* (dFdExy - gdot .*deta_ve.dExy);
% dlamdP        = g .* (dFdP                        );
dlamdExx      = gdot.*(F.*deta_ve.dExx.*gdot - K.*dFdExx.*dt.*gdot.*sin(phi).*sin(psi) - dFdExx.*eta_ve.*gdot + dFdExx.*grad)./(F.*dgraddlam.*gdot - F.*grad - K.^2.*dt.^2.*gdot.^2.*sin(phi).^2.*sin(psi).^2 - 2*K.*dt.*eta_ve.*gdot.^2.*sin(phi).*sin(psi) + 2*K.*dt.*gdot.*grad.*sin(phi).*sin(psi) - eta_ve.^2.*gdot.^2 + 2*eta_ve.*gdot.*grad - grad.^2);
dlamdEyy      = gdot.*(F.*deta_ve.dEyy.*gdot - K.*dFdEyy.*dt.*gdot.*sin(phi).*sin(psi) - dFdEyy.*eta_ve.*gdot + dFdEyy.*grad)./(F.*dgraddlam.*gdot - F.*grad - K.^2.*dt.^2.*gdot.^2.*sin(phi).^2.*sin(psi).^2 - 2*K.*dt.*eta_ve.*gdot.^2.*sin(phi).*sin(psi) + 2*K.*dt.*gdot.*grad.*sin(phi).*sin(psi) - eta_ve.^2.*gdot.^2 + 2*eta_ve.*gdot.*grad - grad.^2);
dlamdEzz      = gdot.*(F.*deta_ve.dEzz.*gdot - K.*dFdEzz.*dt.*gdot.*sin(phi).*sin(psi) - dFdEzz.*eta_ve.*gdot + dFdEzz.*grad)./(F.*dgraddlam.*gdot - F.*grad - K.^2.*dt.^2.*gdot.^2.*sin(phi).^2.*sin(psi).^2 - 2*K.*dt.*eta_ve.*gdot.^2.*sin(phi).*sin(psi) + 2*K.*dt.*gdot.*grad.*sin(phi).*sin(psi) - eta_ve.^2.*gdot.^2 + 2*eta_ve.*gdot.*grad - grad.^2);
dlamdExy      = gdot.*(F.*deta_ve.dExy.*gdot - K.*dFdExy.*dt.*gdot.*sin(phi).*sin(psi) - dFdExy.*eta_ve.*gdot + dFdExy.*grad)./(F.*dgraddlam.*gdot - F.*grad - K.^2.*dt.^2.*gdot.^2.*sin(phi).^2.*sin(psi).^2 - 2*K.*dt.*eta_ve.*gdot.^2.*sin(phi).*sin(psi) + 2*K.*dt.*gdot.*grad.*sin(phi).*sin(psi) - eta_ve.^2.*gdot.^2 + 2*eta_ve.*gdot.*grad - grad.^2);
dlamdP        = gdot.*(                      - K.*dFdP  .*dt.*gdot.*sin(phi).*sin(psi) - dFdP  .*eta_ve.*gdot + dFdP  .*grad)./(F.*dgraddlam.*gdot - F.*grad - K.^2.*dt.^2.*gdot.^2.*sin(phi).^2.*sin(psi).^2 - 2*K.*dt.*eta_ve.*gdot.^2.*sin(phi).*sin(psi) + 2*K.*dt.*gdot.*grad.*sin(phi).*sin(psi) - eta_ve.^2.*gdot.^2 + 2*eta_ve.*gdot.*grad - grad.^2);

% deta_vp_dE
deta_vp_dExx  = eta_vp./gdot.*dlamdExx.*(1./n_vp-1);
deta_vp_dEyy  = eta_vp./gdot.*dlamdEyy.*(1./n_vp-1);
deta_vp_dEzz  = eta_vp./gdot.*dlamdEzz.*(1./n_vp-1);
deta_vp_dExy  = eta_vp./gdot.*dlamdExy.*(1./n_vp-1);
deta_vp_dP    = eta_vp./gdot.*dlamdP  .*(1./n_vp-1);
% deta_vep_dE
a             = (dHdgdot + eta_vp + K.*dt.*sin(psi).*sin(phi) - dgraddlam);
deta_vep.dExx = -Exx.*Tyield./(4*Eii.^3) + (a.*dlamdExx + gdot.*deta_vp_dExx)./(2*Eii);
deta_vep.dEyy = -Eyy.*Tyield./(4*Eii.^3) + (a.*dlamdEyy + gdot.*deta_vp_dEyy)./(2*Eii);
deta_vep.dEzz = -Ezz.*Tyield./(4*Eii.^3) + (a.*dlamdEzz + gdot.*deta_vp_dEzz)./(2*Eii);
deta_vep.dExy = -Exy.*Tyield./(2*Eii.^3) + (a.*dlamdExy + gdot.*deta_vp_dExy)./(2*Eii);
deta_vep.dP   =                (sin(phi) +  a.*dlamdP   + gdot.*deta_vp_dP  )./(2*Eii); 
deta_vep.dExx(pl==0) = deta_ve.dExx(pl==0);
deta_vep.dEyy(pl==0) = deta_ve.dEyy(pl==0);
deta_vep.dEzz(pl==0) = deta_ve.dEzz(pl==0);
deta_vep.dExy(pl==0) = deta_ve.dExy(pl==0);
deta_vep.dP(pl==0)   = deta_ve.dP(pl==0);
% ddivs
dQdP       = -sin(psi);
d2QdExxdP  =  Psi.soft_on.*hpsi.*cos(psi).*dlamdExx;
d2QdEyydP  =  Psi.soft_on.*hpsi.*cos(psi).*dlamdEyy;
d2QdEzzdP  =  Psi.soft_on.*hpsi.*cos(psi).*dlamdEzz;
d2QdExydP  =  Psi.soft_on.*hpsi.*cos(psi).*dlamdExy;
d2QdPdP    =  Psi.soft_on.*hpsi.*cos(psi).*dlamdP;
ddivp.dExx = -dQdP.*dlamdExx - d2QdExxdP.*gdot; ddivp.dExx(pl==0) = 0;
ddivp.dEyy = -dQdP.*dlamdEyy - d2QdEyydP.*gdot; ddivp.dEyy(pl==0) = 0;
ddivp.dEzz = -dQdP.*dlamdEzz - d2QdEzzdP.*gdot; ddivp.dEzz(pl==0) = 0;
ddivp.dExy = -dQdP.*dlamdExy - d2QdExydP.*gdot; ddivp.dExy(pl==0) = 0;
ddivp.dP   = -dQdP.*dlamdP   - d2QdPdP  .*gdot; ddivp.dP(pl==0)   = 0;
% Secure things
deta_vep.dExx(isinf(deta_vep.dExx)) = 0;
deta_vep.dExx(isnan(deta_vep.dExx)) = 0;
deta_vep.dEyy(isinf(deta_vep.dEyy)) = 0;
deta_vep.dEyy(isnan(deta_vep.dEyy)) = 0;
deta_vep.dEzz(isinf(deta_vep.dEzz)) = 0;
deta_vep.dEzz(isnan(deta_vep.dEzz)) = 0;
deta_vep.dExy(isinf(deta_vep.dExy)) = 0;
deta_vep.dExy(isnan(deta_vep.dExy)) = 0;
deta_vep.dP(isinf(deta_vep.dP))     = 0;
deta_vep.dP(isnan(deta_vep.dP))     = 0;
% Mess
Stuff.dg     = gdot;
Stuff.dQdtxx = dQdtxx.*pl;
Stuff.dQdtyy = dQdtyy.*pl;
Stuff.dQdtxy = dQdtxy.*pl;
Stuff.dQdtzz = dQdtzz.*pl;
Stuff.dQdP   = dQdP.*pl;
Stuff.Jii    = Tii.^2;
Stuff.Txx    = Txx;
Stuff.Tyy    = Tyy;
Stuff.Txy    = Txy;
Stuff.Tzz    = Tzz;
Stuff.eta_t  = eta_ve;
Stuff.Pt     = Pc;
if sum(pl(:))>0
Stuff.H         = - K.*dt.*sin(psi).*sin(phi) - eta_vp./n_vp - dHdgdot; % for CTL
Stuff.H (pl==0) = 0;
else
Stuff.H         = 0;
end
end