function [ K ] = M2Di2_GeneralAnisotropicVVAssembly_v4( D, BC, NumUx, NumUy, NumUyG, nx, ny, dx, dy, symmetry, Ux, Uy, ps, SuiteSparse )
%% ACHTUNG Tzz has moved to 44 position (used to be 55) 
% Boundary condition flags [W E S N]
fsxW = BC.fsxW; fsxE = BC.fsxE;
fsxS = BC.fsxS; fsxN = BC.fsxN;
nsxW = BC.nsxW; nsxE = BC.nsxE;
nsxS = BC.nsxS; nsxN = BC.nsxN;
% Boundary condition flags [W E S N]
fsyW = BC.fsyW; fsyE = BC.fsyE;
fsyS = BC.fsyS; fsyN = BC.fsyN;
nsyW = BC.nsyW; nsyE = BC.nsyE;
nsyS = BC.nsyS; nsyN = BC.nsyN;
% Additional guys
fsyN1 = fsyN; nsyN1 = nsyN; fsyN1(:, end-1) = 0; nsyN1(:, end-1) = 0;
fsyS1 = fsyS; nsyS1 = nsyS; fsyS1(:, 2    ) = 0; nsyS1(:, 2    ) = 0;
fsxE1 = fsxE; nsxE1 = nsxE; fsxE1(end-1, :) = 0; nsxE1(end-1, :) = 0;
fsxW1 = fsxW; nsxW1 = nsxW; fsxW1( 2 , :  ) = 0; nsxW1( 2, :   ) = 0;
% Neumanns/Dirichlets
NeuW = BC.NeuW; NeuE = BC.NeuE; Dirx = BC.Dirx;
NeuS = BC.NeuS; NeuN = BC.NeuN; Diry = BC.Diry;
% Stencil weights for normal strains interpolation
wSW   = ones(nx+1,ny+1); wSW(end,:) = 2; wSW(:,end) = 2; wSW(end,end) = 4; wSW(1  ,:) = 0; wSW(:,  1) = 0;
wSE   = ones(nx+1,ny+1); wSE(  1,:) = 2; wSE(:,end) = 2; wSE(  1,end) = 4; wSE(end,:) = 0; wSE(:,  1) = 0;
wNW   = ones(nx+1,ny+1); wNW(end,:) = 2; wNW(:,  1) = 2; wNW(end,  1) = 4; wNW(1  ,:) = 0; wNW(:,end) = 0;
wNE   = ones(nx+1,ny+1); wNE(  1,:) = 2; wNE(:,  1) = 2; wNE(  1,  1) = 4; wNE(end,:) = 0; wNE(:,end) = 0;
%% Block UU --- v2
% Stencil weights for normal strains interpolation
wS_SW = wSW(:,1:end-1); wS_SE = wSE(:,1:end-1); wS_W  = wNW(:,1:end-1); wS_E  = wNE(:,1:end-1); 
wN_W  = wSW(:,2:end-0); wN_E  = wSE(:,2:end-0); wN_NW = wNW(:,2:end-0); wN_NE = wNE(:,2:end-0);
% Elastic or elasto-plastic operators West-East
D11W    = zeros(size(Ux));  D11W( 2:end-0, : ) = D.D11c(1:end-0,:);
D11E    = zeros(size(Ux));  D11E( 1:end-1, : ) = D.D11c(1:end,:);
D12W    = zeros(size(Ux));  D12W( 2:end-0, : ) = D.D12c(1:end-0,:);
D12E    = zeros(size(Ux));  D12E( 1:end-1, : ) = D.D12c(1:end,:);
D13W    = zeros(size(Ux));  D13W( 2:end-0, : ) = D.D13c(1:end-0,:);
D13E    = zeros(size(Ux));  D13E( 1:end-1, : ) = D.D13c(1:end,:);
D14W    = zeros(size(Ux));  D14W( 2:end-0, : ) = D.D15c(1:end-0,:); % reads 14
D14E    = zeros(size(Ux));  D14E( 1:end-1, : ) = D.D15c(1:end,:);
% Elastic or elasto-plastic operators South-North
D31S    = zeros(size(Ux));  D31S( 1:end-0 , :) = D.D31v(1:end-0, 1:end-1);  
D31N    = zeros(size(Ux));  D31N( 1:end-0 , :) = D.D31v(1:end-0, 2:end  );  
D32S    = zeros(size(Ux));  D32S( 1:end-0 , :) = D.D32v(1:end-0, 1:end-1); 
D32N    = zeros(size(Ux));  D32N( 1:end-0 , :) = D.D32v(1:end-0, 2:end  ); 
D33S    = zeros(size(Ux));  D33S( 1:end-0 , :) = D.D33v(1:end-0, 1:end-1);  
D33N    = zeros(size(Ux));  D33N( 1:end-0 , :) = D.D33v(1:end-0, 2:end  );  
D34S    = zeros(size(Ux));  D34S( 1:end-0 , :) = D.D35v(1:end-0, 1:end-1);  % reads 34
D34N    = zeros(size(Ux));  D34N( 1:end-0 , :) = D.D35v(1:end-0, 2:end  );  
% x momentum
sc1Ux = 1;
Neu   = (NeuE==1 | NeuW==1)*symmetry;
half  = 1/2*(Neu+2*(1-Neu));
c1UxC = half.*(-Dirx.*sc1Ux + (Dirx - 1).*((D31N.*(1/4*wN_E.*(ps./dx - 1./dx) + 1/4*wN_W.*(-ps./dx + 1./dx)) - D31S.*(1/4*wS_E.*(ps./dx - 1./dx) + 1/4*wS_W.*(-ps./dx + 1./dx)) + D32N.*(1/4*ps.*wN_E./dx - 1/4*ps.*wN_W./dx) - D32S.*(1/4*ps.*wS_E./dx - 1/4*ps.*wS_W./dx) + D33N.*(1 - fsxN).*(-nsxN - 1)./dy - D33S.*(1 - fsxS).*(nsxS + 1)./dy + D34N.*(1/4*ps.*wN_E./dx - 1/4*ps.*wN_W./dx) - D34S.*(1/4*ps.*wS_E./dx - 1/4*ps.*wS_W./dx))./dy + (-NeuE.*(D11W.*(-ps./dx + 1./dx) - D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D14W.*ps./dx) + NeuW.*(D11E.*(ps./dx - 1./dx) + D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D14E.*ps./dx) + (1 - NeuE).*(D11E.*(ps./dx - 1./dx) + D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D14E.*ps./dx) + (NeuW - 1).*(D11W.*(-ps./dx + 1./dx) - D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D14W.*ps./dx))./dx));
c1UxW = half.*(Dirx - 1).*((1/4*D31N.*wN_W.*(ps./dx - 1./dx) - 1/4*D31S.*wS_W.*(ps./dx - 1./dx) + 1/4*D32N.*ps.*wN_W./dx - 1/4*D32S.*ps.*wS_W./dx + 1/4*D34N.*ps.*wN_W./dx - 1/4*D34S.*ps.*wS_W./dx)./dy + (-NeuE.*(D11W.*(ps./dx - 1./dx) + D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D14W.*ps./dx) + (NeuW - 1).*(D11W.*(ps./dx - 1./dx) + D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D14W.*ps./dx))./dx);
c1UxE = half.*(Dirx - 1).*((1/4*D31N.*wN_E.*(-ps./dx + 1./dx) - 1/4*D31S.*wS_E.*(-ps./dx + 1./dx) - 1/4*D32N.*ps.*wN_E./dx + 1/4*D32S.*ps.*wS_E./dx - 1/4*D34N.*ps.*wN_E./dx + 1/4*D34S.*ps.*wS_E./dx)./dy + (NeuW.*(D11E.*(-ps./dx + 1./dx) - D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D14E.*ps./dx) + (1 - NeuE).*(D11E.*(-ps./dx + 1./dx) - D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D14E.*ps./dx))./dx);
c1UxS = half.*(Dirx - 1).*((-D31S.*(1/4*wS_SE.*(ps./dx - 1./dx) + 1/4*wS_SW.*(-ps./dx + 1./dx)) - D32S.*(1/4*ps.*wS_SE./dx - 1/4*ps.*wS_SW./dx) - D33S.*(1 - fsxS).*(nsxS - 1)./dy - D34S.*(1/4*ps.*wS_SE./dx - 1/4*ps.*wS_SW./dx))./dy + (1/4*D13E.*NeuW.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxS).*(nsxS - 1)./dy - 1/4*D13W.*NeuE.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13W.*(1 - fsxS).*(NeuW - 1).*(nsxS - 1)./dy)./dx);
c1UxN = half.*(Dirx - 1).*((D31N.*(1/4*wN_NE.*(ps./dx - 1./dx) + 1/4*wN_NW.*(-ps./dx + 1./dx)) + D32N.*(1/4*ps.*wN_NE./dx - 1/4*ps.*wN_NW./dx) + D33N.*(1 - fsxN).*(1 - nsxN)./dy + D34N.*(1/4*ps.*wN_NE./dx - 1/4*ps.*wN_NW./dx))./dy + (1/4*D13E.*NeuW.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxN).*(1 - nsxN)./dy - 1/4*D13W.*NeuE.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13W.*(1 - fsxN).*(1 - nsxN).*(NeuW - 1)./dy)./dx);
c1UySW = half.*(Dirx - 1).*((1/4*D31N.*ps.*wN_W./dy - D31S.*(-1/4*ps.*wS_SW./dy + 1/4*ps.*wS_W./dy) + 1/4*D32N.*wN_W.*(ps./dy - 1./dy) - D32S.*(1/4*wS_SW.*(-ps./dy + 1./dy) + 1/4*wS_W.*(ps./dy - 1./dy)) - D33S.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*D34N.*ps.*wN_W./dy - D34S.*(-1/4*ps.*wS_SW./dy + 1/4*ps.*wS_W./dy))./dy + (1/4*D13E.*NeuW.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*D13E.*(1 - NeuE).*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx - NeuE.*(D11W.*ps./dy + D12W.*(ps./dy - 1./dy) + D13W.*(1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*(1 - fsxW).*(nsxW + 1)./dx) + D14W.*ps./dy) + (NeuW - 1).*(D11W.*ps./dy + D12W.*(ps./dy - 1./dy) + D13W.*(1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*(1 - fsxW).*(nsxW + 1)./dx) + D14W.*ps./dy))./dx);
c1UySE = half.*(Dirx - 1).*((1/4*D31N.*ps.*wN_E./dy - D31S.*(1/4*ps.*wS_E./dy - 1/4*ps.*wS_SE./dy) + 1/4*D32N.*wN_E.*(ps./dy - 1./dy) - D32S.*(1/4*wS_E.*(ps./dy - 1./dy) + 1/4*wS_SE.*(-ps./dy + 1./dy)) - D33S.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx + 1/4*D34N.*ps.*wN_E./dy - D34S.*(1/4*ps.*wS_E./dy - 1/4*ps.*wS_SE./dy))./dy + (-1/4*D13W.*NeuE.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx + 1/4*D13W.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(NeuW - 1).*(nsxW1 + 1)./dx + NeuW.*(D11E.*ps./dy + D12E.*(ps./dy - 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx) + D14E.*ps./dy) + (1 - NeuE).*(D11E.*ps./dy + D12E.*(ps./dy - 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx) + D14E.*ps./dy))./dx);
c1UyNW = half.*(Dirx - 1).*((D31N.*(1/4*ps.*wN_NW./dy - 1/4*ps.*wN_W./dy) + 1/4*D31S.*ps.*wS_W./dy + D32N.*(1/4*wN_NW.*(ps./dy - 1./dy) + 1/4*wN_W.*(-ps./dy + 1./dy)) - 1/4*D32S.*wS_W.*(-ps./dy + 1./dy) + D33N.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + D34N.*(1/4*ps.*wN_NW./dy - 1/4*ps.*wN_W./dy) + 1/4*D34S.*ps.*wS_W./dy)./dy + (1/4*D13E.*NeuW.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*D13E.*(1 - NeuE).*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx - NeuE.*(-D11W.*ps./dy + D12W.*(-ps./dy + 1./dy) + D13W.*(1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*(1 - fsxW).*(nsxW + 1)./dx) - D14W.*ps./dy) + (NeuW - 1).*(-D11W.*ps./dy + D12W.*(-ps./dy + 1./dy) + D13W.*(1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxW1).*(-nsxE1 - 1)./dx + 1/4*(1 - fsxW).*(nsxW + 1)./dx) - D14W.*ps./dy))./dx);
c1UyNE = half.*(Dirx - 1).*((D31N.*(-1/4*ps.*wN_E./dy + 1/4*ps.*wN_NE./dy) + 1/4*D31S.*ps.*wS_E./dy + D32N.*(1/4*wN_E.*(-ps./dy + 1./dy) + 1/4*wN_NE.*(ps./dy - 1./dy)) - 1/4*D32S.*wS_E.*(-ps./dy + 1./dy) + D33N.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx + D34N.*(-1/4*ps.*wN_E./dy + 1/4*ps.*wN_NE./dy) + 1/4*D34S.*ps.*wS_E./dy)./dy + (-1/4*D13W.*NeuE.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx + 1/4*D13W.*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(NeuW - 1).*(nsxW1 + 1)./dx + NeuW.*(-D11E.*ps./dy + D12E.*(-ps./dy + 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx) - D14E.*ps./dy) + (1 - NeuE).*(-D11E.*ps./dy + D12E.*(-ps./dy + 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4*(1 - fsxE1).*(1 - fsxW1).*(1 - nsxE1).*(nsxW1 + 1)./dx) - D14E.*ps./dy))./dx);
c1UxSW = half.*(Dirx - 1).*((-1/4*D31S.*wS_SW.*(ps./dx - 1./dx) - 1/4*D32S.*ps.*wS_SW./dx - 1/4*D34S.*ps.*wS_SW./dx)./dy + (-1/4*D13W.*NeuE.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13W.*(1 - fsxS).*(NeuW - 1).*(nsxS - 1)./dy)./dx);
c1UxSE = half.*(Dirx - 1).*((-1/4*D31S.*wS_SE.*(-ps./dx + 1./dx) + 1/4*D32S.*ps.*wS_SE./dx + 1/4*D34S.*ps.*wS_SE./dx)./dy + (1/4*D13E.*NeuW.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxS).*(nsxS - 1)./dy)./dx);
c1UxNW = half.*(Dirx - 1).*((1/4*D31N.*wN_NW.*(ps./dx - 1./dx) + 1/4*D32N.*ps.*wN_NW./dx + 1/4*D34N.*ps.*wN_NW./dx)./dy + (-1/4*D13W.*NeuE.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13W.*(1 - fsxN).*(1 - nsxN).*(NeuW - 1)./dy)./dx);
c1UxNE = half.*(Dirx - 1).*((1/4*D31N.*wN_NE.*(-ps./dx + 1./dx) - 1/4*D32N.*ps.*wN_NE./dx - 1/4*D34N.*ps.*wN_NE./dx)./dy + (1/4*D13E.*NeuW.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxN).*(1 - nsxN)./dy)./dx);
c1UySSW = half.*(Dirx - 1).*(-1/4*D31S.*ps.*wS_SW./dy - 1/4*D32S.*wS_SW.*(ps./dy - 1./dy) - 1/4*D34S.*ps.*wS_SW./dy)./dy;
c1UySSE = half.*(Dirx - 1).*(-1/4*D31S.*ps.*wS_SE./dy - 1/4*D32S.*wS_SE.*(ps./dy - 1./dy) - 1/4*D34S.*ps.*wS_SE./dy)./dy;
c1UySWW = half.*(Dirx - 1).*(-1/4*D13W.*NeuE.*(1 - fsxW).*(nsxW - 1)./dx + 1/4*D13W.*(1 - fsxW).*(NeuW - 1).*(nsxW - 1)./dx)./dx;
c1UySEE = half.*(Dirx - 1).*(1/4*D13E.*NeuW.*(1 - fsxE).*(1 - nsxE)./dx + 1/4*D13E.*(1 - NeuE).*(1 - fsxE).*(1 - nsxE)./dx)./dx;
c1UyNWW = half.*(Dirx - 1).*(-1/4*D13W.*NeuE.*(1 - fsxW).*(nsxW - 1)./dx + 1/4*D13W.*(1 - fsxW).*(NeuW - 1).*(nsxW - 1)./dx)./dx;
c1UyNEE = half.*(Dirx - 1).*(1/4*D13E.*NeuW.*(1 - fsxE).*(1 - nsxE)./dx + 1/4*D13E.*(1 - NeuE).*(1 - fsxE).*(1 - nsxE)./dx)./dx;
c1UyNNW = half.*(Dirx - 1).*(-1/4*D31N.*ps.*wN_NW./dy + 1/4*D32N.*wN_NW.*(-ps./dy + 1./dy) - 1/4*D34N.*ps.*wN_NW./dy)./dy;
c1UyNNE = half.*(Dirx - 1).*(-1/4*D31N.*ps.*wN_NE./dy + 1/4*D32N.*wN_NE.*(-ps./dy + 1./dy) - 1/4*D34N.*ps.*wN_NE./dy)./dy;

% Scale Dirichlets
sc1Ux = max(c1UxC(:));
c1UxC( Dirx==1 ) = sc1Ux;
% Symmetry: only for Picard operator
if symmetry == 1
    ind = zeros(size(NumUx)); ind(2:end-0,:) = Dirx(1:end-1,:); % Kill connections with Vx W Dirichlet
    c1UxW  ( ind==1 ) = 0;
    ind = zeros(size(NumUx)); ind(1:end-1,:) = Dirx(2:end-0,:); % Kill connections with Vx E Dirichlet
    c1UxE  ( ind==1 ) = 0;
    ind = zeros(size(NumUx)); ind(2:end-0,:) = Diry(:,1:end-1); % Kill connections with Vy SW Dirichlet
    c1UySW ( ind== 1 ) = 0;
    ind = zeros(size(NumUx)); ind(1:end-1,:) = Diry(:,1:end-1); % Kill connections with Vy SE Dirichlet
    c1UySE ( ind== 1 ) = 0;
    ind = zeros(size(NumUx)); ind(2:end-0,:) = Diry(:,2:end-0); % Kill connections with Vy NW Dirichlet
    c1UyNW ( ind== 1 ) = 0;
    ind = zeros(size(NumUx)); ind(1:end-1,:) = Diry(:,2:end-0); % Kill connections with Vy NE Dirichlet
    c1UyNE ( ind== 1 ) = 0;
end
% Equation indexes
iUx     = NumUx(:);
iUxW    = ones(size(Ux));   iUxW(2:end-0, :     ) =  NumUx(1:end-1, :     );
iUxE    = ones(size(Ux));   iUxE(1:end-1, :     ) =  NumUx(2:end  , :     );
% mod
iUxS    = ones(size(Ux));   iUxS(1:end-0,2:end  ) =  NumUx(1:end-0,1:end-1);
iUxN    = ones(size(Ux));   iUxN(1:end-0,1:end-1) =  NumUx(1:end-0,2:end  );
%---
iUxSW   = ones(size(Ux));  iUxSW(2:end-0,2:end  ) =  NumUx(1:end-1,1:end-1);
iUxSE   = ones(size(Ux));  iUxSE(1:end-1,2:end  ) =  NumUx(2:end-0,1:end-1);
iUxNW   = ones(size(Ux));  iUxNW(2:end-0,1:end-1) =  NumUx(1:end-1,2:end  );
iUxNE   = ones(size(Ux));  iUxNE(1:end-1,1:end-1) =  NumUx(2:end-0,2:end  );
% mod
iUySW   = ones(size(Ux));  iUySW(2:end-0, :     ) = NumUyG(1:end-0,1:end-1);
iUySE   = ones(size(Ux));  iUySE(1:end-1, :     ) = NumUyG(1:end-0,1:end-1);
iUyNW   = ones(size(Ux));  iUyNW(2:end-0, :     ) = NumUyG(1:end-0,2:end  );
iUyNE   = ones(size(Ux));  iUyNE(1:end-1, :     ) = NumUyG(1:end-0,2:end  );
% ---
% iUySWW  = ones(size(Ux)); iUySWW(3:end-1, :     ) = NumUyG(1:end-2,1:end-1);
% iUySEE  = ones(size(Ux)); iUySEE(2:end-2, :     ) = NumUyG(3:end  ,1:end-1);
% iUyNWW  = ones(size(Ux)); iUyNWW(3:end-1, :     ) = NumUyG(1:end-2,2:end  );
% iUyNEE  = ones(size(Ux)); iUyNEE(2:end-2, :     ) = NumUyG(3:end  ,2:end  );
% iUySSW  = ones(size(Ux)); iUySSW(2:end-1, 2:end ) = NumUyG(1:end-1,1:end-2);
% iUySSE  = ones(size(Ux)); iUySSE(2:end-1, 2:end ) = NumUyG(2:end-0,1:end-2);
% iUyNNW  = ones(size(Ux)); iUyNNW(2:end-1,1:end-1) = NumUyG(1:end-1,3:end  );
% iUyNNE  = ones(size(Ux)); iUyNNE(2:end-1,1:end-1) = NumUyG(2:end-0,3:end  );
iUySWW  = ones(size(Ux)); iUySWW(3:end-0, :     ) = NumUyG(1:end-1,1:end-1);
iUySEE  = ones(size(Ux)); iUySEE(1:end-2, :     ) = NumUyG(2:end  ,1:end-1);
iUyNWW  = ones(size(Ux)); iUyNWW(3:end-0, :     ) = NumUyG(1:end-1,2:end  );
iUyNEE  = ones(size(Ux)); iUyNEE(1:end-2, :     ) = NumUyG(2:end  ,2:end  );
iUySSW  = ones(size(Ux)); iUySSW(2:end-0, 2:end ) = NumUyG(1:end-0,1:end-2);
iUySSE  = ones(size(Ux)); iUySSE(1:end-1, 2:end ) = NumUyG(1:end-0,1:end-2);
iUyNNW  = ones(size(Ux)); iUyNNW(2:end-0,1:end-1) = NumUyG(1:end-0,3:end  );
iUyNNE  = ones(size(Ux)); iUyNNE(1:end-1,1:end-1) = NumUyG(1:end-0,3:end  );
% Triplets
IuuJ_2    = [   iUx(:);   iUx(:);   iUx(:);   iUx(:);   iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:) ]';
JuuJ_2    = [   iUx(:);  iUxW(:);  iUxS(:);  iUxE(:);  iUxN(:);  iUxSW(:);  iUxSE(:);  iUxNW(:);  iUxNE(:);  iUySW(:);  iUySE(:);  iUyNW(:);  iUyNE(:);  iUySWW(:);  iUySEE(:);  iUyNWW(:);  iUyNEE(:);  iUySSW(:);  iUySSE(:);  iUyNNW(:);  iUyNNE(:) ]';
VuuJ_2    = [ c1UxC(:); c1UxW(:); c1UxS(:); c1UxE(:); c1UxN(:); c1UxSW(:); c1UxSE(:); c1UxNW(:); c1UxNE(:); c1UySW(:); c1UySE(:); c1UyNW(:); c1UyNE(:); c1UySWW(:); c1UySEE(:); c1UyNWW(:); c1UyNEE(:); c1UySSW(:); c1UySSE(:); c1UyNNW(:); c1UyNNE(:) ]';
%% Block VV --- v2
% Elastic or elasto-plastic operators South-North
D21S    = zeros(size(Uy));  D21S( : ,2:end-0) = D.D21c(:,1:end-0);
D21N    = zeros(size(Uy));  D21N( : ,1:end-1) = D.D21c(:,1:end-0); 
D22S    = zeros(size(Uy));  D22S( : ,2:end-0) = D.D22c(:,1:end-0);
D22N    = zeros(size(Uy));  D22N( : ,1:end-1) = D.D22c(:,1:end-0);
D23S    = zeros(size(Uy));  D23S( : ,2:end-0) = D.D23c(:,1:end-0);
D23N    = zeros(size(Uy));  D23N( : ,1:end-1) = D.D23c(:,1:end-0);
D24S    = zeros(size(Uy));  D24S( : ,2:end-0) = D.D25c(:,1:end-0); % reads 24
D24N    = zeros(size(Uy));  D24N( : ,1:end-1) = D.D25c(:,1:end-0);
% Elastic or elasto-plastic operators West-East
D31W    = zeros(size(Uy));  D31W( : ,1:end-0 ) = D.D31v(1:end-1, 1:end-0); 
D31E    = zeros(size(Uy));  D31E( : ,1:end-0 ) = D.D31v(2:end  , 1:end-0);
D32W    = zeros(size(Uy));  D32W( : ,1:end-0 ) = D.D32v(1:end-1, 1:end-0); 
D32E    = zeros(size(Uy));  D32E( : ,1:end-0 ) = D.D32v(2:end  , 1:end-0); 
D33W    = zeros(size(Uy));  D33W( : ,1:end-0 ) = D.D33v(1:end-1, 1:end-0); 
D33E    = zeros(size(Uy));  D33E( : ,1:end-0 ) = D.D33v(2:end  , 1:end-0); 
D34W    = zeros(size(Uy));  D34W( : ,1:end-0 ) = D.D35v(1:end-1, 1:end-0);  % reads 34
D34E    = zeros(size(Uy));  D34E( : ,1:end-0 ) = D.D35v(2:end  , 1:end-0); 
% Stencil weights for normal strains interpolation
wW_SW = wSW(1:end-1,:); wW_NW = wNW(1:end-1,:); wW_S  = wSE(1:end-1,:); wW_N  = wNE(1:end-1,:);  %% BUG FIX ON 30/04/20
wE_S  = wSW(2:end-0,:); wE_N  = wNW(2:end-0,:); wE_SE = wSE(2:end-0,:); wE_NE = wNE(2:end-0,:);
% y - momentum
Neu  = (NeuS==1 | NeuN==1)*symmetry;
half = 1/2*(Neu+2*(1-Neu));
sc2Uy = 1;
c2UyC = half.*(-Diry.*sc2Uy + (Diry - 1).*((-NeuN.*(-D21S.*ps./dy + D22S.*(-ps./dy + 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D24S.*ps./dy) + NeuS.*(D21N.*ps./dy + D22N.*(ps./dy - 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D24N.*ps./dy) + (1 - NeuN).*(D21N.*ps./dy + D22N.*(ps./dy - 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D24N.*ps./dy) + (NeuS - 1).*(-D21S.*ps./dy + D22S.*(-ps./dy + 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D24S.*ps./dy))./dy + (D31E.*(1/4*ps.*wE_N./dy - 1/4*ps.*wE_S./dy) - D31W.*(1/4*ps.*wW_N./dy - 1/4*ps.*wW_S./dy) + D32E.*(1/4*wE_N.*(ps./dy - 1./dy) + 1/4*wE_S.*(-ps./dy + 1./dy)) - D32W.*(1/4*wW_N.*(ps./dy - 1./dy) + 1/4*wW_S.*(-ps./dy + 1./dy)) + D33E.*(1 - fsyE).*(-nsyE - 1)./dx - D33W.*(1 - fsyW).*(nsyW + 1)./dx + D34E.*(1/4*ps.*wE_N./dy - 1/4*ps.*wE_S./dy) - D34W.*(1/4*ps.*wW_N./dy - 1/4*ps.*wW_S./dy))./dx));
c2UyW = half.*(Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyW).*(nsyW - 1)./dx - 1/4*D23S.*NeuN.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23S.*(1 - fsyW).*(NeuS - 1).*(nsyW - 1)./dx)./dy + (-D31W.*(1/4*ps.*wW_NW./dy - 1/4*ps.*wW_SW./dy) - D32W.*(1/4*wW_NW.*(ps./dy - 1./dy) + 1/4*wW_SW.*(-ps./dy + 1./dy)) - D33W.*(1 - fsyW).*(nsyW - 1)./dx - D34W.*(1/4*ps.*wW_NW./dy - 1/4*ps.*wW_SW./dy))./dx);
c2UyE = half.*(Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyE).*(1 - nsyE)./dx - 1/4*D23S.*NeuN.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23S.*(1 - fsyE).*(1 - nsyE).*(NeuS - 1)./dx)./dy + (D31E.*(1/4*ps.*wE_NE./dy - 1/4*ps.*wE_SE./dy) + D32E.*(1/4*wE_NE.*(ps./dy - 1./dy) + 1/4*wE_SE.*(-ps./dy + 1./dy)) + D33E.*(1 - fsyE).*(1 - nsyE)./dx + D34E.*(1/4*ps.*wE_NE./dy - 1/4*ps.*wE_SE./dy))./dx);
c2UyS = half.*(Diry - 1).*((-NeuN.*(D21S.*ps./dy + D22S.*(ps./dy - 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D24S.*ps./dy) + (NeuS - 1).*(D21S.*ps./dy + D22S.*(ps./dy - 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D24S.*ps./dy))./dy + (1/4*D31E.*ps.*wE_S./dy - 1/4*D31W.*ps.*wW_S./dy + 1/4*D32E.*wE_S.*(ps./dy - 1./dy) - 1/4*D32W.*wW_S.*(ps./dy - 1./dy) + 1/4*D34E.*ps.*wE_S./dy - 1/4*D34W.*ps.*wW_S./dy)./dx);
c2UyN = half.*(Diry - 1).*((NeuS.*(-D21N.*ps./dy + D22N.*(-ps./dy + 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D24N.*ps./dy) + (1 - NeuN).*(-D21N.*ps./dy + D22N.*(-ps./dy + 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D24N.*ps./dy))./dy + (-1/4*D31E.*ps.*wE_N./dy + 1/4*D31W.*ps.*wW_N./dy + 1/4*D32E.*wE_N.*(-ps./dy + 1./dy) - 1/4*D32W.*wW_N.*(-ps./dy + 1./dy) - 1/4*D34E.*ps.*wE_N./dy + 1/4*D34W.*ps.*wW_N./dy)./dx);
c2UxSW = half.*(Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*D23N.*(1 - NeuN).*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy - NeuN.*(D21S.*(ps./dx - 1./dx) + D22S.*ps./dx + D23S.*(1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*(1 - fsyS).*(nsyS + 1)./dy) + D24S.*ps./dx) + (NeuS - 1).*(D21S.*(ps./dx - 1./dx) + D22S.*ps./dx + D23S.*(1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*(1 - fsyS).*(nsyS + 1)./dy) + D24S.*ps./dx))./dy + (1/4*D31E.*wE_S.*(ps./dx - 1./dx) - D31W.*(1/4*wW_S.*(ps./dx - 1./dx) + 1/4*wW_SW.*(-ps./dx + 1./dx)) + 1/4*D32E.*ps.*wE_S./dx - D32W.*(1/4*ps.*wW_S./dx - 1/4*ps.*wW_SW./dx) - D33W.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*D34E.*ps.*wE_S./dx - D34W.*(1/4*ps.*wW_S./dx - 1/4*ps.*wW_SW./dx))./dx);
c2UxSE = half.*(Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*D23N.*(1 - NeuN).*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy - NeuN.*(D21S.*(-ps./dx + 1./dx) - D22S.*ps./dx + D23S.*(1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*(1 - fsyS).*(nsyS + 1)./dy) - D24S.*ps./dx) + (NeuS - 1).*(D21S.*(-ps./dx + 1./dx) - D22S.*ps./dx + D23S.*(1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + 1/4*(1 - fsyS).*(nsyS + 1)./dy) - D24S.*ps./dx))./dy + (D31E.*(1/4*wE_S.*(-ps./dx + 1./dx) + 1/4*wE_SE.*(ps./dx - 1./dx)) - 1/4*D31W.*wW_S.*(-ps./dx + 1./dx) + D32E.*(-1/4*ps.*wE_S./dx + 1/4*ps.*wE_SE./dx) + 1/4*D32W.*ps.*wW_S./dx + D33E.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyS1).*(-nsyN1 - 1)./dy + D34E.*(-1/4*ps.*wE_S./dx + 1/4*ps.*wE_SE./dx) + 1/4*D34W.*ps.*wW_S./dx)./dx);
c2UxNW = half.*(Diry - 1).*((-1/4*D23S.*NeuN.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy + 1/4*D23S.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(NeuS - 1).*(nsyS1 + 1)./dy + NeuS.*(D21N.*(ps./dx - 1./dx) + D22N.*ps./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy) + D24N.*ps./dx) + (1 - NeuN).*(D21N.*(ps./dx - 1./dx) + D22N.*ps./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy) + D24N.*ps./dx))./dy + (1/4*D31E.*wE_N.*(ps./dx - 1./dx) - D31W.*(1/4*wW_N.*(ps./dx - 1./dx) + 1/4*wW_NW.*(-ps./dx + 1./dx)) + 1/4*D32E.*ps.*wE_N./dx - D32W.*(1/4*ps.*wW_N./dx - 1/4*ps.*wW_NW./dx) - D33W.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy + 1/4*D34E.*ps.*wE_N./dx - D34W.*(1/4*ps.*wW_N./dx - 1/4*ps.*wW_NW./dx))./dx);
c2UxNE = half.*(Diry - 1).*((-1/4*D23S.*NeuN.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy + 1/4*D23S.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(NeuS - 1).*(nsyS1 + 1)./dy + NeuS.*(D21N.*(-ps./dx + 1./dx) - D22N.*ps./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy) - D24N.*ps./dx) + (1 - NeuN).*(D21N.*(-ps./dx + 1./dx) - D22N.*ps./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy) - D24N.*ps./dx))./dy + (D31E.*(1/4*wE_N.*(-ps./dx + 1./dx) + 1/4*wE_NE.*(ps./dx - 1./dx)) - 1/4*D31W.*wW_N.*(-ps./dx + 1./dx) + D32E.*(-1/4*ps.*wE_N./dx + 1/4*ps.*wE_NE./dx) + 1/4*D32W.*ps.*wW_N./dx + D33E.*(1 - fsyN1).*(1 - fsyS1).*(1 - nsyN1).*(nsyS1 + 1)./dy + D34E.*(-1/4*ps.*wE_N./dx + 1/4*ps.*wE_NE./dx) + 1/4*D34W.*ps.*wW_N./dx)./dx);
c2UySW = half.*(Diry - 1).*((-1/4*D23S.*NeuN.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23S.*(1 - fsyW).*(NeuS - 1).*(nsyW - 1)./dx)./dy + (-1/4*D31W.*ps.*wW_SW./dy - 1/4*D32W.*wW_SW.*(ps./dy - 1./dy) - 1/4*D34W.*ps.*wW_SW./dy)./dx);
c2UySE = half.*(Diry - 1).*((-1/4*D23S.*NeuN.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23S.*(1 - fsyE).*(1 - nsyE).*(NeuS - 1)./dx)./dy + (1/4*D31E.*ps.*wE_SE./dy + 1/4*D32E.*wE_SE.*(ps./dy - 1./dy) + 1/4*D34E.*ps.*wE_SE./dy)./dx);
c2UyNW = half.*(Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyW).*(nsyW - 1)./dx)./dy + (1/4*D31W.*ps.*wW_NW./dy - 1/4*D32W.*wW_NW.*(-ps./dy + 1./dy) + 1/4*D34W.*ps.*wW_NW./dy)./dx);
c2UyNE = half.*(Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyE).*(1 - nsyE)./dx)./dy + (-1/4*D31E.*ps.*wE_NE./dy + 1/4*D32E.*wE_NE.*(-ps./dy + 1./dy) - 1/4*D34E.*ps.*wE_NE./dy)./dx);
c2UxSSW = half.*(Diry - 1).*(-1/4*D23S.*NeuN.*(1 - fsyS).*(nsyS - 1)./dy + 1/4*D23S.*(1 - fsyS).*(NeuS - 1).*(nsyS - 1)./dy)./dy;
c2UxSSE = half.*(Diry - 1).*(-1/4*D23S.*NeuN.*(1 - fsyS).*(nsyS - 1)./dy + 1/4*D23S.*(1 - fsyS).*(NeuS - 1).*(nsyS - 1)./dy)./dy;
c2UxSWW = half.*(Diry - 1).*(-1/4*D31W.*wW_SW.*(ps./dx - 1./dx) - 1/4*D32W.*ps.*wW_SW./dx - 1/4*D34W.*ps.*wW_SW./dx)./dx;
c2UxSEE = half.*(Diry - 1).*(1/4*D31E.*wE_SE.*(-ps./dx + 1./dx) - 1/4*D32E.*ps.*wE_SE./dx - 1/4*D34E.*ps.*wE_SE./dx)./dx;
c2UxNWW = half.*(Diry - 1).*(-1/4*D31W.*wW_NW.*(ps./dx - 1./dx) - 1/4*D32W.*ps.*wW_NW./dx - 1/4*D34W.*ps.*wW_NW./dx)./dx;
c2UxNEE = half.*(Diry - 1).*(1/4*D31E.*wE_NE.*(-ps./dx + 1./dx) - 1/4*D32E.*ps.*wE_NE./dx - 1/4*D34E.*ps.*wE_NE./dx)./dx;
c2UxNNW = half.*(Diry - 1).*(1/4*D23N.*NeuS.*(1 - fsyN).*(1 - nsyN)./dy + 1/4*D23N.*(1 - NeuN).*(1 - fsyN).*(1 - nsyN)./dy)./dy;
c2UxNNE = half.*(Diry - 1).*(1/4*D23N.*NeuS.*(1 - fsyN).*(1 - nsyN)./dy + 1/4*D23N.*(1 - NeuN).*(1 - fsyN).*(1 - nsyN)./dy)./dy;

% Scale Dirichlets
sc2Uy = max(c2UyC(:));
c2UyC( Diry==1 ) = sc2Uy;
% Symmetry: only for Picard operator
if symmetry == 1
    ind = zeros(size(NumUy)); ind(:,2:end-0) = Diry(:,1:end-1); % Kill connections with Vy S Dirichlet
    c2UyS  ( ind==1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,1:end-1) = Diry(:,2:end-0); % Kill connections with Vy N Dirichlet
    c2UyN  ( ind==1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,2:end-0) = Dirx(1:end-1,:); % Kill connections with Vx SW Dirichlet
    c2UxSW ( ind== 1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,1:end-1) = Dirx(2:end-0,:); % Kill connections with Vx SE Dirichlet
    c2UxSE ( ind== 1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,2:end-0) = Dirx(1:end-1,:); % Kill connections with Vx NW Dirichlet
    c2UxNW ( ind== 1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,1:end-1) = Dirx(2:end-0,:); % Kill connections with Vx NE Dirichlet
    c2UxNE ( ind== 1 ) = 0;
end
% Equation indexes
iUy     = NumUyG(:);
% mod
iUyW    = ones(size(Uy));   iUyW(2:end  ,1:end-0) = NumUyG(1:end-1,1:end-0);
iUyE    = ones(size(Uy));   iUyE(1:end-1,1:end-0) = NumUyG(2:end  ,1:end-0);
%---
iUyS    = ones(size(Uy));   iUyS( :     ,2:end-0) = NumUyG( :     ,1:end-1);
iUyN    = ones(size(Uy));   iUyN( :     ,1:end-1) = NumUyG( :     ,2:end  );
iUySW   = ones(size(Uy));  iUySW(2:end  ,2:end-0) = NumUyG(1:end-1,1:end-1);
iUySE   = ones(size(Uy));  iUySE(1:end-1,2:end-0) = NumUyG(2:end  ,1:end-1);
iUyNW   = ones(size(Uy));  iUyNW(2:end  ,1:end-1) = NumUyG(1:end-1,2:end-0);
iUyNE   = ones(size(Uy));  iUyNE(1:end-1,1:end-1) = NumUyG(2:end  ,2:end-0);
% mod
iUxSW   = ones(size(Uy));  iUxSW( :     ,2:end-0) = NumUx (1:end-1,1:end-0);
iUxNW   = ones(size(Uy));  iUxNW( :     ,1:end-1) = NumUx (1:end-1,1:end-0);
iUxSE   = ones(size(Uy));  iUxSE( :     ,2:end-0) = NumUx (2:end  ,1:end-0);
iUxNE   = ones(size(Uy));  iUxNE( :     ,1:end-1) = NumUx (2:end  ,1:end-0);
%---
% iUxSSW  = ones(size(Uy)); iUxSSW( :     ,3:end-1) = NumUx (1:end-1,1:end-2);
% iUxSSE  = ones(size(Uy)); iUxSSE( :     ,3:end-1) = NumUx (2:end  ,1:end-2);
% iUxNNW  = ones(size(Uy)); iUxNNW( :     ,2:end-2) = NumUx (1:end-1,3:end  );
% iUxNNE  = ones(size(Uy)); iUxNNE( :     ,2:end-2) = NumUx (2:end  ,3:end  );
% iUxSWW  = ones(size(Uy)); iUxSWW(2:end  ,2:end-1) = NumUx (1:end-2,1:end-1);
% iUxNWW  = ones(size(Uy)); iUxNWW(2:end  ,2:end-1) = NumUx (1:end-2,2:end-0);
% iUxSEE  = ones(size(Uy)); iUxSEE(1:end-1,2:end-1) = NumUx (3:end  ,1:end-1);
% iUxNEE  = ones(size(Uy)); iUxNEE(1:end-1,2:end-1) = NumUx (3:end  ,2:end-0);
iUxSSW  = ones(size(Uy)); iUxSSW( :     ,3:end-0) = NumUx (1:end-1,1:end-1);
iUxSSE  = ones(size(Uy)); iUxSSE( :     ,3:end-0) = NumUx (2:end  ,1:end-1);
iUxNNW  = ones(size(Uy)); iUxNNW( :     ,1:end-2) = NumUx (1:end-1,2:end  );
iUxNNE  = ones(size(Uy)); iUxNNE( :     ,1:end-2) = NumUx (2:end  ,2:end  );
iUxSWW  = ones(size(Uy)); iUxSWW(2:end  ,2:end-0) = NumUx (1:end-2,1:end-0);
iUxNWW  = ones(size(Uy)); iUxNWW(2:end  ,1:end-1) = NumUx (1:end-2,1:end-0);
iUxSEE  = ones(size(Uy)); iUxSEE(1:end-1,2:end-0) = NumUx (3:end  ,1:end-0);
iUxNEE  = ones(size(Uy)); iUxNEE(1:end-1,1:end-1) = NumUx (3:end  ,1:end-0);
% Triplets
IvvJ_2    = [   iUy(:);   iUy(:);   iUy(:);   iUy(:);   iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:) ]';
JvvJ_2    = [   iUy(:);  iUyW(:);  iUyS(:);  iUyE(:);  iUyN(:);  iUySW(:);  iUySE(:);  iUyNW(:);  iUyNE(:);  iUxSW(:);  iUxSE(:);  iUxNW(:);  iUxNE(:);  iUxSSW(:);  iUxSSE(:);  iUxNNW(:);  iUxNNE(:);  iUxSWW(:);  iUxNWW(:);  iUxSEE(:);  iUxNEE(:) ]';
VvvJ_2    = [ c2UyC(:); c2UyW(:); c2UyS(:); c2UyE(:); c2UyN(:); c2UySW(:); c2UySE(:); c2UyNW(:); c2UyNE(:); c2UxSW(:); c2UxSE(:); c2UxNW(:); c2UxNE(:); c2UxSSW(:); c2UxSSE(:); c2UxNNW(:); c2UxNNE(:); c2UxSWW(:); c2UxNWW(:); c2UxSEE(:); c2UxNEE(:) ]';
%% Assembly of the sparse matrix
if SuiteSparse==1, K  = sparse2([IuuJ_2(:); IvvJ_2(:);], [JuuJ_2(:); JvvJ_2(:);], [VuuJ_2(:); VvvJ_2(:);], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
else               K  = sparse ([IuuJ_2(:); IvvJ_2(:);], [JuuJ_2(:); JvvJ_2(:);], [VuuJ_2(:); VvvJ_2(:);], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
end
% % spy
% figure(1), clf
% subplot(211)
% spy(K)
% subplot(212)
% spy(K-K')
% end

