function [ K ] = M2Di2_GeneralAnisotropicVVAssembly_v3( D, BC, NumUx, NumUy, NumUyG, nx, ny, dx, dy, symmetry, Ux, Uy, ps, SuiteSparse )
% Boundary condition flags [W E S N]
fsxW = BC.fsxW; fsxE = BC.fsxE;
fsxS = BC.fsxS; fsxN = BC.fsxN;
nsxW = BC.nsxW; nsxE = BC.nsxE;
nsxS = BC.nsxS; nsxN = BC.nsxN;
% Boundary condition flags [W E S N]
fsyW = BC.fsyW; fsyE = BC.fsyE;
fsyS = BC.fsyS; fsyN = BC.fsyN;
nsyW = BC.nsyW; nsyE = BC.nsyE;
nsyS = BC.nsyS; nsyN = BC.nsyN;
% Neumanns/Dirichlets
NeuW = BC.NeuW; NeuE = BC.NeuE; Dirx = BC.Dirx;
NeuS = BC.NeuS; NeuN = BC.NeuN; Diry = BC.Diry;
% Stencil weights for normal strains interpolation
wSW   = ones(nx+1,ny+1); wSW(end,:) = 2; wSW(:,end) = 2; wSW(end,end) = 4; wSW(1  ,:) = 0; wSW(:,  1) = 0;
wSE   = ones(nx+1,ny+1); wSE(  1,:) = 2; wSE(:,end) = 2; wSE(  1,end) = 4; wSE(end,:) = 0; wSE(:,  1) = 0;
wNW   = ones(nx+1,ny+1); wNW(end,:) = 2; wNW(:,  1) = 2; wNW(end,  1) = 4; wNW(1  ,:) = 0; wNW(:,end) = 0;
wNE   = ones(nx+1,ny+1); wNE(  1,:) = 2; wNE(:,  1) = 2; wNE(  1,  1) = 4; wNE(end,:) = 0; wNE(:,end) = 0;
%% Block UU --- v2
% Stencil weights for normal strains interpolation
wS_SW = wSW(:,1:end-1); wS_SE = wSE(:,1:end-1); wS_W  = wNW(:,1:end-1); wS_E  = wNE(:,1:end-1); 
wN_W  = wSW(:,2:end-0); wN_E  = wSE(:,2:end-0); wN_NW = wNW(:,2:end-0); wN_NE = wNE(:,2:end-0);
% Elastic or elasto-plastic operators West-East
D11W    = zeros(size(Ux));  D11W( 2:end-0, : ) = D.D11c(1:end-0,:);
D11E    = zeros(size(Ux));  D11E( 1:end-1, : ) = D.D11c(1:end,:);
D12W    = zeros(size(Ux));  D12W( 2:end-0, : ) = D.D12c(1:end-0,:);
D12E    = zeros(size(Ux));  D12E( 1:end-1, : ) = D.D12c(1:end,:);
D13W    = zeros(size(Ux));  D13W( 2:end-0, : ) = D.D13c(1:end-0,:);
D13E    = zeros(size(Ux));  D13E( 1:end-1, : ) = D.D13c(1:end,:);
D15W    = zeros(size(Ux));  D15W( 2:end-0, : ) = D.D15c(1:end-0,:);
D15E    = zeros(size(Ux));  D15E( 1:end-1, : ) = D.D15c(1:end,:);
% Elastic or elasto-plastic operators South-North
D31S    = zeros(size(Ux));  D31S( 1:end-0 , :) = D.D31v(1:end-0, 1:end-1);  %D31S(:,1) = 0;
D31N    = zeros(size(Ux));  D31N( 1:end-0 , :) = D.D31v(1:end-0, 2:end  );  %D31N(:,end) = 0;
D32S    = zeros(size(Ux));  D32S( 1:end-0 , :) = D.D32v(1:end-0, 1:end-1);  %D32S(:,1) = 0;
D32N    = zeros(size(Ux));  D32N( 1:end-0 , :) = D.D32v(1:end-0, 2:end  );  %D32N(:,end) = 0;
D33S    = zeros(size(Ux));  D33S( 1:end-0 , :) = D.D33v(1:end-0, 1:end-1);  %D33S(:,1) = 0;
D33N    = zeros(size(Ux));  D33N( 1:end-0 , :) = D.D33v(1:end-0, 2:end  );  %D33N(:,end) = 0;
D35S    = zeros(size(Ux));  D35S( 1:end-0 , :) = D.D35v(1:end-0, 1:end-1);  %D33S(:,1) = 0;
D35N    = zeros(size(Ux));  D35N( 1:end-0 , :) = D.D35v(1:end-0, 2:end  );  %D33N(:,end) = 0;
% x momentum
% c1UxC = (dx.^2.*(-D33N.*fsxN.*nsxN - D33N.*fsxN + D33N.*nsxN + D33N - D33S.*fsxS.*nsxS - D33S.*fsxS + D33S.*nsxS + D33S) + dx.*dy.*(-1/4*D13E.*fsxN.*nsxN - 1/4*D13E.*fsxN + 1/4*D13E.*fsxS.*nsxS + 1/4*D13E.*fsxS + 1/4*D13E.*nsxN - 1/4*D13E.*nsxS + 1/4*D13W.*fsxN.*nsxN + 1/4*D13W.*fsxN - 1/4*D13W.*fsxS.*nsxS - 1/4*D13W.*fsxS - 1/4*D13W.*nsxN + 1/4*D13W.*nsxS + 1/6*D31N.*wN_E - 1/6*D31N.*wN_W - 1/6*D31S.*wS_E + 1/6*D31S.*wS_W - 1/12*D32N.*wN_E + 1/12*D32N.*wN_W + 1/12*D32S.*wS_E - 1/12*D32S.*wS_W - 1/12*D35N.*wN_E + 1/12*D35N.*wN_W + 1/12*D35S.*wS_E - 1/12*D35S.*wS_W) + dy.^2.*(2/3*D11E + 2/3*D11W - 1/3*D12E - 1/3*D12W - 1/3*D15E - 1/3*D15W))./(dx.^2.*dy.^2);
% c1UxW = (dx.*(1/4*D13W.*fsxN.*nsxN + 1/4*D13W.*fsxN - 1/4*D13W.*fsxS.*nsxS - 1/4*D13W.*fsxS - 1/4*D13W.*nsxN + 1/4*D13W.*nsxS + 1/6*D31N.*wN_W - 1/6*D31S.*wS_W - 1/12*D32N.*wN_W + 1/12*D32S.*wS_W - 1/12*D35N.*wN_W + 1/12*D35S.*wS_W) + dy.*(-2/3*D11W + 1/3*D12W + 1/3*D15W))./(dx.^2.*dy);
% c1UxE = (dx.*(-1/4*D13E.*fsxN.*nsxN - 1/4*D13E.*fsxN + 1/4*D13E.*fsxS.*nsxS + 1/4*D13E.*fsxS + 1/4*D13E.*nsxN - 1/4*D13E.*nsxS - 1/6*D31N.*wN_E + 1/6*D31S.*wS_E + 1/12*D32N.*wN_E - 1/12*D32S.*wS_E + 1/12*D35N.*wN_E - 1/12*D35S.*wS_E) + dy.*(-2/3*D11E + 1/3*D12E + 1/3*D15E))./(dx.^2.*dy);
% c1UxS = (D33S.*dx.*(-fsxS.*nsxS + fsxS + nsxS - 1) + dy.*(1/4*D13E.*fsxS.*nsxS - 1/4*D13E.*fsxS - 1/4*D13E.*nsxS + 1/4*D13E - 1/4*D13W.*fsxS.*nsxS + 1/4*D13W.*fsxS + 1/4*D13W.*nsxS - 1/4*D13W - 1/6*D31S.*wS_SE + 1/6*D31S.*wS_SW + 1/12*D32S.*wS_SE - 1/12*D32S.*wS_SW + 1/12*D35S.*wS_SE - 1/12*D35S.*wS_SW))./(dx.*dy.^2);
% c1UxN = (D33N.*dx.*(-fsxN.*nsxN + fsxN + nsxN - 1) + dy.*(-1/4*D13E.*fsxN.*nsxN + 1/4*D13E.*fsxN + 1/4*D13E.*nsxN - 1/4*D13E + 1/4*D13W.*fsxN.*nsxN - 1/4*D13W.*fsxN - 1/4*D13W.*nsxN + 1/4*D13W - 1/6*D31N.*frxN.*wN_NE + 1/6*D31N.*frxN.*wN_NW + 1/6*D31N.*wN_NE - 1/6*D31N.*wN_NW + 1/12*D32N.*frxN.*wN_NE - 1/12*D32N.*frxN.*wN_NW - 1/12*D32N.*wN_NE + 1/12*D32N.*wN_NW + 1/12*D35N.*frxN.*wN_NE - 1/12*D35N.*frxN.*wN_NW - 1/12*D35N.*wN_NE + 1/12*D35N.*wN_NW))./(dx.*dy.^2);
% c1UySW = (dx.^2.*(-1/12*D31N.*wN_W - 1/12*D31S.*wS_SW + 1/12*D31S.*wS_W + 1/6*D32N.*wN_W + 1/6*D32S.*wS_SW - 1/6*D32S.*wS_W - 1/12*D35N.*wN_W - 1/12*D35S.*wS_SW + 1/12*D35S.*wS_W) + dx.*dy.*(1/3*D11W - 2/3*D12W + 1/3*D15W - D33S) + 1/4*dy.^2.*(D13E - D13W.*fsxW.*nsxW - D13W.*fsxW + D13W.*nsxW))./(dx.^2.*dy.^2);
% c1UySE = (dx.^2.*(-1/12*D31N.*wN_E + 1/12*D31S.*wS_E - 1/12*D31S.*wS_SE + 1/6*D32N.*wN_E - 1/6*D32S.*wS_E + 1/6*D32S.*wS_SE - 1/12*D35N.*wN_E + 1/12*D35S.*wS_E - 1/12*D35S.*wS_SE) + dx.*dy.*(-1/3*D11E + 2/3*D12E - 1/3*D15E + D33S) + 1/4*dy.^2.*(-D13E.*fsxE.*nsxE - D13E.*fsxE + D13E.*nsxE + D13W))./(dx.^2.*dy.^2);
% c1UyNW = (dx.^2.*(1/12*D31N.*frxN.*wN_NW - 1/12*D31N.*wN_NW + 1/12*D31N.*wN_W - 1/12*D31S.*wS_W - 1/6*D32N.*frxN.*wN_NW + 1/6*D32N.*wN_NW - 1/6*D32N.*wN_W + 1/6*D32S.*wS_W + 1/12*D35N.*frxN.*wN_NW - 1/12*D35N.*wN_NW + 1/12*D35N.*wN_W - 1/12*D35S.*wS_W) + dx.*dy.*(-1/3*D11W + 2/3*D12W - 1/3*D15W + D33N) + 1/4*dy.^2.*(D13E - D13W.*fsxW.*nsxW - D13W.*fsxW + D13W.*nsxW))./(dx.^2.*dy.^2);
% c1UyNE = (dx.^2.*(1/12*D31N.*frxN.*wN_NE + 1/12*D31N.*wN_E - 1/12*D31N.*wN_NE - 1/12*D31S.*wS_E - 1/6*D32N.*frxN.*wN_NE - 1/6*D32N.*wN_E + 1/6*D32N.*wN_NE + 1/6*D32S.*wS_E + 1/12*D35N.*frxN.*wN_NE + 1/12*D35N.*wN_E - 1/12*D35N.*wN_NE - 1/12*D35S.*wS_E) + dx.*dy.*(1/3*D11E - 2/3*D12E + 1/3*D15E - D33N) + 1/4*dy.^2.*(-D13E.*fsxE.*nsxE - D13E.*fsxE + D13E.*nsxE + D13W))./(dx.^2.*dy.^2);
% c1UxSW = (-1/4*D13W.*fsxS.*nsxS + 1/4*D13W.*fsxS + 1/4*D13W.*nsxS - 1/4*D13W - 1/6*D31S.*wS_SW + 1/12*D32S.*wS_SW + 1/12*D35S.*wS_SW)./(dx.*dy);
% c1UxSE = (1/4*D13E.*fsxS.*nsxS - 1/4*D13E.*fsxS - 1/4*D13E.*nsxS + 1/4*D13E + 1/6*D31S.*wS_SE - 1/12*D32S.*wS_SE - 1/12*D35S.*wS_SE)./(dx.*dy);
% c1UxNW = (1/4*D13W.*fsxN.*nsxN - 1/4*D13W.*fsxN - 1/4*D13W.*nsxN + 1/4*D13W - 1/6*D31N.*frxN.*wN_NW + 1/6*D31N.*wN_NW + 1/12*D32N.*frxN.*wN_NW - 1/12*D32N.*wN_NW + 1/12*D35N.*frxN.*wN_NW - 1/12*D35N.*wN_NW)./(dx.*dy);
% c1UxNE = (-1/4*D13E.*fsxN.*nsxN + 1/4*D13E.*fsxN + 1/4*D13E.*nsxN - 1/4*D13E + 1/6*D31N.*frxN.*wN_NE - 1/6*D31N.*wN_NE - 1/12*D32N.*frxN.*wN_NE + 1/12*D32N.*wN_NE - 1/12*D35N.*frxN.*wN_NE + 1/12*D35N.*wN_NE)./(dx.*dy);
% c1UySSW = wS_SW.*(1/12*D31S - 1/6*D32S + 1/12*D35S)./dy.^2;
% c1UySSE = wS_SE.*(1/12*D31S - 1/6*D32S + 1/12*D35S)./dy.^2;
% c1UySWW = 1/4*D13W.*(-fsxW.*nsxW + fsxW + nsxW - 1)./dx.^2;
% c1UySEE = 1/4*D13E.*(-fsxE.*nsxE + fsxE + nsxE - 1)./dx.^2;
% c1UyNWW = 1/4*D13W.*(-fsxW.*nsxW + fsxW + nsxW - 1)./dx.^2;
% c1UyNEE = 1/4*D13E.*(-fsxE.*nsxE + fsxE + nsxE - 1)./dx.^2;
% c1UyNNW = wN_NW.*(-1/12*D31N.*frxN + 1/12*D31N + 1/6*D32N.*frxN - 1/6*D32N - 1/12*D35N.*frxN + 1/12*D35N)./dy.^2;
% c1UyNNE = wN_NE.*(-1/12*D31N.*frxN + 1/12*D31N + 1/6*D32N.*frxN - 1/6*D32N - 1/12*D35N.*frxN + 1/12*D35N)./dy.^2;
sc1Ux = 1;
c1UxC = -Dirx.*sc1Ux + (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(D31N.*(1/4*wN_E.*(ps./dx - 1./dx) + 1/4*wN_W.*(-ps./dx + 1./dx)) - D31S.*(1/4*wS_E.*(ps./dx - 1./dx) + 1/4*wS_W.*(-ps./dx + 1./dx)) + D32N.*(1/4*ps.*wN_E./dx - 1/4*ps.*wN_W./dx) - D32S.*(1/4*ps.*wS_E./dx - 1/4*ps.*wS_W./dx) + D33N.*(1 - fsxN).*(-nsxN - 1)./dy - D33S.*(1 - fsxS).*(nsxS + 1)./dy + D35N.*(1/4*ps.*wN_E./dx - 1/4*ps.*wN_W./dx) - D35S.*(1/4*ps.*wS_E./dx - 1/4*ps.*wS_W./dx))./dy + (-NeuE.*(D11W.*(-ps./dx + 1./dx) - D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D15W.*ps./dx) + NeuW.*(D11E.*(ps./dx - 1./dx) + D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D15E.*ps./dx) + (1 - NeuE).*(D11E.*(ps./dx - 1./dx) + D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D15E.*ps./dx) + (NeuW - 1).*(D11W.*(-ps./dx + 1./dx) - D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D15W.*ps./dx))./dx);
c1UxW = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(1/4*D31N.*wN_W.*(ps./dx - 1./dx) - 1/4*D31S.*wS_W.*(ps./dx - 1./dx) + 1/4*D32N.*ps.*wN_W./dx - 1/4*D32S.*ps.*wS_W./dx + 1/4*D35N.*ps.*wN_W./dx - 1/4*D35S.*ps.*wS_W./dx)./dy + (-NeuE.*(D11W.*(ps./dx - 1./dx) + D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D15W.*ps./dx) + (NeuW - 1).*(D11W.*(ps./dx - 1./dx) + D12W.*ps./dx + D13W.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) + D15W.*ps./dx))./dx);
c1UxE = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(1/4*D31N.*wN_E.*(-ps./dx + 1./dx) - 1/4*D31S.*wS_E.*(-ps./dx + 1./dx) - 1/4*D32N.*ps.*wN_E./dx + 1/4*D32S.*ps.*wS_E./dx - 1/4*D35N.*ps.*wN_E./dx + 1/4*D35S.*ps.*wS_E./dx)./dy + (NeuW.*(D11E.*(-ps./dx + 1./dx) - D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D15E.*ps./dx) + (1 - NeuE).*(D11E.*(-ps./dx + 1./dx) - D12E.*ps./dx + D13E.*(1/4*(1 - fsxN).*(-nsxN - 1)./dy + 1/4*(1 - fsxS).*(nsxS + 1)./dy) - D15E.*ps./dx))./dx);
c1UxS = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(-D31S.*(1/4*wS_SE.*(ps./dx - 1./dx) + 1/4*wS_SW.*(-ps./dx + 1./dx)) - D32S.*(1/4*ps.*wS_SE./dx - 1/4*ps.*wS_SW./dx) - D33S.*(1 - fsxS).*(nsxS - 1)./dy - D35S.*(1/4*ps.*wS_SE./dx - 1/4*ps.*wS_SW./dx))./dy + (1/4*D13E.*NeuW.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxS).*(nsxS - 1)./dy - 1/4*D13W.*NeuE.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13W.*(1 - fsxS).*(NeuW - 1).*(nsxS - 1)./dy)./dx);
c1UxN = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(D31N.*(1/4*wN_NE.*(ps./dx - 1./dx) + 1/4*wN_NW.*(-ps./dx + 1./dx)) + D32N.*(1/4*ps.*wN_NE./dx - 1/4*ps.*wN_NW./dx) + D33N.*(1 - fsxN).*(1 - nsxN)./dy + D35N.*(1/4*ps.*wN_NE./dx - 1/4*ps.*wN_NW./dx))./dy + (1/4*D13E.*NeuW.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxN).*(1 - nsxN)./dy - 1/4*D13W.*NeuE.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13W.*(1 - fsxN).*(1 - nsxN).*(NeuW - 1)./dy)./dx);
c1UySW = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(1/4*D31N.*ps.*wN_W.*(1 - NeuE)./dy - D31S.*(-1/4*ps.*wS_SW.*(1 - NeuE)./dy + 1/4*ps.*wS_W.*(1 - NeuE)./dy) + 1/4*D32N.*wN_W.*(ps.*(1 - NeuE)./dy - 1./dy) - D32S.*(1/4*wS_SW.*(-ps.*(1 - NeuE)./dy + 1./dy) + 1/4*wS_W.*(ps.*(1 - NeuE)./dy - 1./dy)) + D33S./dx + 1/4*D35N.*ps.*wN_W.*(1 - NeuE)./dy - D35S.*(-1/4*ps.*wS_SW.*(1 - NeuE)./dy + 1/4*ps.*wS_W.*(1 - NeuE)./dy))./dy + (-1/4*D13E.*NeuW./dx - 1/4*D13E.*(1 - NeuE)./dx - NeuE.*(D11W.*ps.*(1 - NeuE)./dy + D12W.*(ps.*(1 - NeuE)./dy - 1./dy) + D13W.*(1/4*(1 - fsxW).*(nsxW + 1)./dx - 1/4./dx) + D15W.*ps.*(1 - NeuE)./dy) + (NeuW - 1).*(D11W.*ps.*(1 - NeuE)./dy + D12W.*(ps.*(1 - NeuE)./dy - 1./dy) + D13W.*(1/4*(1 - fsxW).*(nsxW + 1)./dx - 1/4./dx) + D15W.*ps.*(1 - NeuE)./dy))./dx);
c1UySE = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(1/4*D31N.*ps.*wN_E.*(1 - NeuW)./dy - D31S.*(1/4*ps.*wS_E.*(1 - NeuW)./dy - 1/4*ps.*wS_SE.*(1 - NeuW)./dy) + 1/4*D32N.*wN_E.*(ps.*(1 - NeuW)./dy - 1./dy) - D32S.*(1/4*wS_E.*(ps.*(1 - NeuW)./dy - 1./dy) + 1/4*wS_SE.*(-ps.*(1 - NeuW)./dy + 1./dy)) - D33S./dx + 1/4*D35N.*ps.*wN_E.*(1 - NeuW)./dy - D35S.*(1/4*ps.*wS_E.*(1 - NeuW)./dy - 1/4*ps.*wS_SE.*(1 - NeuW)./dy))./dy + (-1/4*D13W.*NeuE./dx + 1/4*D13W.*(NeuW - 1)./dx + NeuW.*(D11E.*ps.*(1 - NeuW)./dy + D12E.*(ps.*(1 - NeuW)./dy - 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4./dx) + D15E.*ps.*(1 - NeuW)./dy) + (1 - NeuE).*(D11E.*ps.*(1 - NeuW)./dy + D12E.*(ps.*(1 - NeuW)./dy - 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4./dx) + D15E.*ps.*(1 - NeuW)./dy))./dx);
c1UyNW = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(D31N.*(1/4*ps.*wN_NW.*(1 - NeuE)./dy - 1/4*ps.*wN_W.*(1 - NeuE)./dy) + 1/4*D31S.*ps.*wS_W.*(1 - NeuE)./dy + D32N.*(1/4*wN_NW.*(ps.*(1 - NeuE)./dy - 1./dy) + 1/4*wN_W.*(-ps.*(1 - NeuE)./dy + 1./dy)) - 1/4*D32S.*wS_W.*(-ps.*(1 - NeuE)./dy + 1./dy) - D33N./dx + D35N.*(1/4*ps.*wN_NW.*(1 - NeuE)./dy - 1/4*ps.*wN_W.*(1 - NeuE)./dy) + 1/4*D35S.*ps.*wS_W.*(1 - NeuE)./dy)./dy + (-1/4*D13E.*NeuW./dx - 1/4*D13E.*(1 - NeuE)./dx - NeuE.*(-D11W.*ps.*(1 - NeuE)./dy + D12W.*(-ps.*(1 - NeuE)./dy + 1./dy) + D13W.*(1/4*(1 - fsxW).*(nsxW + 1)./dx - 1/4./dx) - D15W.*ps.*(1 - NeuE)./dy) + (NeuW - 1).*(-D11W.*ps.*(1 - NeuE)./dy + D12W.*(-ps.*(1 - NeuE)./dy + 1./dy) + D13W.*(1/4*(1 - fsxW).*(nsxW + 1)./dx - 1/4./dx) - D15W.*ps.*(1 - NeuE)./dy))./dx);
c1UyNE = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(D31N.*(-1/4*ps.*wN_E.*(1 - NeuW)./dy + 1/4*ps.*wN_NE.*(1 - NeuW)./dy) + 1/4*D31S.*ps.*wS_E.*(1 - NeuW)./dy + D32N.*(1/4*wN_E.*(-ps.*(1 - NeuW)./dy + 1./dy) + 1/4*wN_NE.*(ps.*(1 - NeuW)./dy - 1./dy)) - 1/4*D32S.*wS_E.*(-ps.*(1 - NeuW)./dy + 1./dy) + D33N./dx + D35N.*(-1/4*ps.*wN_E.*(1 - NeuW)./dy + 1/4*ps.*wN_NE.*(1 - NeuW)./dy) + 1/4*D35S.*ps.*wS_E.*(1 - NeuW)./dy)./dy + (-1/4*D13W.*NeuE./dx + 1/4*D13W.*(NeuW - 1)./dx + NeuW.*(-D11E.*ps.*(1 - NeuW)./dy + D12E.*(-ps.*(1 - NeuW)./dy + 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4./dx) - D15E.*ps.*(1 - NeuW)./dy) + (1 - NeuE).*(-D11E.*ps.*(1 - NeuW)./dy + D12E.*(-ps.*(1 - NeuW)./dy + 1./dy) + D13E.*(1/4*(1 - fsxE).*(-nsxE - 1)./dx + 1/4./dx) - D15E.*ps.*(1 - NeuW)./dy))./dx);
c1UxSW = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(-1/4*D31S.*wS_SW.*(ps./dx - 1./dx) - 1/4*D32S.*ps.*wS_SW./dx - 1/4*D35S.*ps.*wS_SW./dx)./dy + (-1/4*D13W.*NeuE.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13W.*(1 - fsxS).*(NeuW - 1).*(nsxS - 1)./dy)./dx);
c1UxSE = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(-1/4*D31S.*wS_SE.*(-ps./dx + 1./dx) + 1/4*D32S.*ps.*wS_SE./dx + 1/4*D35S.*ps.*wS_SE./dx)./dy + (1/4*D13E.*NeuW.*(1 - fsxS).*(nsxS - 1)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxS).*(nsxS - 1)./dy)./dx);
c1UxNW = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(1/4*D31N.*wN_NW.*(ps./dx - 1./dx) + 1/4*D32N.*ps.*wN_NW./dx + 1/4*D35N.*ps.*wN_NW./dx)./dy + (-1/4*D13W.*NeuE.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13W.*(1 - fsxN).*(1 - nsxN).*(NeuW - 1)./dy)./dx);
c1UxNE = (Dirx - 1).*((1 - NeuE).*(1 - NeuW).*(1/4*D31N.*wN_NE.*(-ps./dx + 1./dx) - 1/4*D32N.*ps.*wN_NE./dx - 1/4*D35N.*ps.*wN_NE./dx)./dy + (1/4*D13E.*NeuW.*(1 - fsxN).*(1 - nsxN)./dy + 1/4*D13E.*(1 - NeuE).*(1 - fsxN).*(1 - nsxN)./dy)./dx);
c1UySSW = (1 - NeuE).*(1 - NeuW).*(Dirx - 1).*(-1/4*D31S.*ps.*wS_SW.*(1 - NeuE)./dy - 1/4*D32S.*wS_SW.*(ps.*(1 - NeuE)./dy - 1./dy) - 1/4*D35S.*ps.*wS_SW.*(1 - NeuE)./dy)./dy;
c1UySSE = (1 - NeuE).*(1 - NeuW).*(Dirx - 1).*(-1/4*D31S.*ps.*wS_SE.*(1 - NeuW)./dy - 1/4*D32S.*wS_SE.*(ps.*(1 - NeuW)./dy - 1./dy) - 1/4*D35S.*ps.*wS_SE.*(1 - NeuW)./dy)./dy;
c1UySWW = (Dirx - 1).*(-1/4*D13W.*NeuE.*(1 - fsxW).*(nsxW - 1)./dx + 1/4*D13W.*(1 - fsxW).*(NeuW - 1).*(nsxW - 1)./dx)./dx;
c1UySEE = (Dirx - 1).*(1/4*D13E.*NeuW.*(1 - fsxE).*(1 - nsxE)./dx + 1/4*D13E.*(1 - NeuE).*(1 - fsxE).*(1 - nsxE)./dx)./dx;
c1UyNWW = (Dirx - 1).*(-1/4*D13W.*NeuE.*(1 - fsxW).*(nsxW - 1)./dx + 1/4*D13W.*(1 - fsxW).*(NeuW - 1).*(nsxW - 1)./dx)./dx;
c1UyNEE = (Dirx - 1).*(1/4*D13E.*NeuW.*(1 - fsxE).*(1 - nsxE)./dx + 1/4*D13E.*(1 - NeuE).*(1 - fsxE).*(1 - nsxE)./dx)./dx;
c1UyNNW = (1 - NeuE).*(1 - NeuW).*(Dirx - 1).*(-1/4*D31N.*ps.*wN_NW.*(1 - NeuE)./dy + 1/4*D32N.*wN_NW.*(-ps.*(1 - NeuE)./dy + 1./dy) - 1/4*D35N.*ps.*wN_NW.*(1 - NeuE)./dy)./dy;
c1UyNNE = (1 - NeuE).*(1 - NeuW).*(Dirx - 1).*(-1/4*D31N.*ps.*wN_NE.*(1 - NeuW)./dy + 1/4*D32N.*wN_NE.*(-ps.*(1 - NeuW)./dy + 1./dy) - 1/4*D35N.*ps.*wN_NE.*(1 - NeuW)./dy)./dy;


% Scale Dirichlets
sc1Ux = max(c1UxC(:));
c1UxC( Dirx==1 ) = sc1Ux;
% Symmetry
if symmetry == 1
    c1UxC(NeuW==1 | NeuE==1) = 1/2*c1UxC(NeuW==1 | NeuE==1); % Half coefficient for stress BC
    c1UxW(NeuW==1 | NeuE==1) = 1/2*c1UxW(NeuW==1 | NeuE==1);
    c1UxE(NeuW==1 | NeuE==1) = 1/2*c1UxE(NeuW==1 | NeuE==1);
    c1UxS(NeuW==1 | NeuE==1) = 1/2*c1UxS(NeuW==1 | NeuE==1);
    c1UxN(NeuW==1 | NeuE==1) = 1/2*c1UxN(NeuW==1 | NeuE==1);
    ind = zeros(size(NumUx)); ind(2:end-0,:) = Dirx(1:end-1,:); % Kill connections with Vx W Dirichlet
    c1UxW  ( ind==1 ) = 0;
    ind = zeros(size(NumUx)); ind(1:end-1,:) = Dirx(2:end-0,:); % Kill connections with Vx E Dirichlet
    c1UxE  ( ind==1 ) = 0;
    ind = zeros(size(NumUx)); ind(2:end-0,:) = Diry(:,1:end-1); % Kill connections with Vy SW Dirichlet
    c1UySW ( ind== 1 ) = 0;
    ind = zeros(size(NumUx)); ind(1:end-1,:) = Diry(:,1:end-1); % Kill connections with Vy SE Dirichlet
    c1UySE ( ind== 1 ) = 0;
    ind = zeros(size(NumUx)); ind(2:end-0,:) = Diry(:,2:end-0); % Kill connections with Vy NW Dirichlet
    c1UyNW ( ind== 1 ) = 0;
    ind = zeros(size(NumUx)); ind(1:end-1,:) = Diry(:,2:end-0); % Kill connections with Vy NE Dirichlet
    c1UyNE ( ind== 1 ) = 0;
end
% Equation indexes
iUx     = NumUx(:);
iUxW    = ones(size(Ux));   iUxW(2:end-0, :     ) =  NumUx(1:end-1, :     );
iUxE    = ones(size(Ux));   iUxE(1:end-1, :     ) =  NumUx(2:end  , :     );
iUxS    = ones(size(Ux));   iUxS(2:end-0,2:end  ) =  NumUx(2:end-0,1:end-1);
iUxN    = ones(size(Ux));   iUxN(2:end-0,1:end-1) =  NumUx(2:end-0,2:end  );
iUxSW   = ones(size(Ux));  iUxSW(2:end-0,2:end  ) =  NumUx(1:end-1,1:end-1);
iUxSE   = ones(size(Ux));  iUxSE(1:end-1,2:end  ) =  NumUx(2:end-0,1:end-1);
iUxNW   = ones(size(Ux));  iUxNW(2:end-0,1:end-1) =  NumUx(1:end-1,2:end  );
iUxNE   = ones(size(Ux));  iUxNE(1:end-1,1:end-1) =  NumUx(2:end-0,2:end  );
iUySW   = ones(size(Ux));  iUySW(2:end-0, :     ) = NumUyG(1:end-0,1:end-1);
iUySE   = ones(size(Ux));  iUySE(2:end-1, :     ) = NumUyG(2:end-0,1:end-1);
iUyNW   = ones(size(Ux));  iUyNW(2:end-0, :     ) = NumUyG(1:end-0,2:end  );
iUyNE   = ones(size(Ux));  iUyNE(2:end-1, :     ) = NumUyG(2:end-0,2:end  );
iUySWW  = ones(size(Ux)); iUySWW(3:end-1, :     ) = NumUyG(1:end-2,1:end-1);
iUySEE  = ones(size(Ux)); iUySEE(2:end-2, :     ) = NumUyG(3:end  ,1:end-1);
iUyNWW  = ones(size(Ux)); iUyNWW(3:end-1, :     ) = NumUyG(1:end-2,2:end  );
iUyNEE  = ones(size(Ux)); iUyNEE(2:end-2, :     ) = NumUyG(3:end  ,2:end  );
iUySSW  = ones(size(Ux)); iUySSW(2:end-1, 2:end ) = NumUyG(1:end-1,1:end-2);
iUySSE  = ones(size(Ux)); iUySSE(2:end-1, 2:end ) = NumUyG(2:end-0,1:end-2);
iUyNNW  = ones(size(Ux)); iUyNNW(2:end-1 ,1:end-1) = NumUyG(1:end-1,3:end  );
iUyNNE  = ones(size(Ux)); iUyNNE(2:end-1,1:end-1) = NumUyG(2:end-0,3:end  );
% Triplets
IuuJ_2    = [   iUx(:);   iUx(:);   iUx(:);   iUx(:);   iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);    iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:);     iUx(:) ]';
JuuJ_2    = [   iUx(:);  iUxW(:);  iUxS(:);  iUxE(:);  iUxN(:);  iUxSW(:);  iUxSE(:);  iUxNW(:);  iUxNE(:);  iUySW(:);  iUySE(:);  iUyNW(:);  iUyNE(:);  iUySWW(:);  iUySEE(:);  iUyNWW(:);  iUyNEE(:);  iUySSW(:);  iUySSE(:);  iUyNNW(:);  iUyNNE(:) ]';
VuuJ_2    = [ c1UxC(:); c1UxW(:); c1UxS(:); c1UxE(:); c1UxN(:); c1UxSW(:); c1UxSE(:); c1UxNW(:); c1UxNE(:); c1UySW(:); c1UySE(:); c1UyNW(:); c1UyNE(:); c1UySWW(:); c1UySEE(:); c1UyNWW(:); c1UyNEE(:); c1UySSW(:); c1UySSE(:); c1UyNNW(:); c1UyNNE(:) ]';
%% Block VV --- v2
% Elastic or elasto-plastic operators South-North
D21S    = zeros(size(Uy));  D21S( : ,2:end-0) = D.D21c(:,1:end-0);% D21S(:,1) = 0;
D21N    = zeros(size(Uy));  D21N( : ,1:end-1) = D.D21c(:,1:end-0); %D21N(:,end) = 0;
D22S    = zeros(size(Uy));  D22S( : ,2:end-0) = D.D22c(:,1:end-0);% D22S(:,1) = 0;
D22N    = zeros(size(Uy));  D22N( : ,1:end-1) = D.D22c(:,1:end-0); %D21N(:,1) = 0;
D23S    = zeros(size(Uy));  D23S( : ,2:end-0) = D.D23c(:,1:end-0);% D23S(:,1) = 0;
D23N    = zeros(size(Uy));  D23N( : ,1:end-1) = D.D23c(:,1:end-0);% D23N(:,end) = 0;
D25S    = zeros(size(Uy));  D25S( : ,2:end-1) = D.D25c(:,1:end-1);% D23S(:,1) = 0;
D25N    = zeros(size(Uy));  D25N( : ,1:end-1) = D.D25c(:,1:end-0);% D23N(:,end) = 0
% Elastic or elasto-plastic operators West-East
D31W    = zeros(size(Uy));  D31W( : ,1:end-0 ) = D.D31v(1:end-1, 1:end-0); %D31W(1,:) = 0;
D31E    = zeros(size(Uy));  D31E( : ,1:end-0 ) = D.D31v(2:end  , 1:end-0); %D31E(end,:) = 0;
D32W    = zeros(size(Uy));  D32W( : ,1:end-0 ) = D.D32v(1:end-1, 1:end-0); %D32W(1,:) = 0;
D32E    = zeros(size(Uy));  D32E( : ,1:end-0 ) = D.D32v(2:end  , 1:end-0); %D32E(end,:) = 0;
D33W    = zeros(size(Uy));  D33W( : ,1:end-0 ) = D.D33v(1:end-1, 1:end-0); %D33W(1,:) = 0;
D33E    = zeros(size(Uy));  D33E( : ,1:end-0 ) = D.D33v(2:end  , 1:end-0); %D33E(end,:) = 0;
D35W    = zeros(size(Uy));  D35W( : ,1:end-0 ) = D.D35v(1:end-1, 1:end-0); %D33W(1,:) = 0;
D35E    = zeros(size(Uy));  D35E( : ,1:end-0 ) = D.D35v(2:end  , 1:end-0); %D33E(end,:) = 0;
% Stencil weights for normal strains interpolation
wW_SW = wSW(1:end-1,:); wW_NW = wNW(1:end-1,:); wW_S  = wSE(1:end-1,:); wW_N  = wNE(1:end-1,:);  %% BUG FIX ON 30/04/20
wE_S  = wSW(2:end-0,:); wE_N  = wNW(2:end-0,:); wE_SE = wSE(2:end-0,:); wE_NE = wNE(2:end-0,:);
% % y - momentum
% c2UyC = (dx.^2.*(1/3*D21N.*fryN - 1/3*D21N - 1/3*D21S - 2/3*D22N.*fryN + 2/3*D22N + 2/3*D22S + 1/3*D25N.*fryN - 1/3*D25N - 1/3*D25S) + dx.*dy.*(-1/4*D23N.*fsyE.*nsyE - 1/4*D23N.*fsyE + 1/4*D23N.*fsyW.*nsyW + 1/4*D23N.*fsyW + 1/4*D23N.*nsyE - 1/4*D23N.*nsyW + 1/4*D23S.*fsyE.*nsyE + 1/4*D23S.*fsyE - 1/4*D23S.*fsyW.*nsyW - 1/4*D23S.*fsyW - 1/4*D23S.*nsyE + 1/4*D23S.*nsyW + 1/12*D31E.*fryN.*wE_N - 1/12*D31E.*wE_N + 1/12*D31E.*wE_S - 1/12*D31W.*fryN.*wW_N + 1/12*D31W.*wW_N - 1/12*D31W.*wW_S - 1/6*D32E.*fryN.*wE_N + 1/6*D32E.*wE_N - 1/6*D32E.*wE_S + 1/6*D32W.*fryN.*wW_N - 1/6*D32W.*wW_N + 1/6*D32W.*wW_S + 1/12*D35E.*fryN.*wE_N - 1/12*D35E.*wE_N + 1/12*D35E.*wE_S - 1/12*D35W.*fryN.*wW_N + 1/12*D35W.*wW_N - 1/12*D35W.*wW_S) + dy.^2.*(-D33E.*fsyE.*nsyE - D33E.*fsyE + D33E.*nsyE + D33E - D33W.*fsyW.*nsyW - D33W.*fsyW + D33W.*nsyW + D33W))./(dx.^2.*dy.^2);
% c2UyW = (D33W.*dy.*(-fsyW.*nsyW + fsyW + nsyW - 1) + dx.*(1/4*D23N.*fsyW.*nsyW - 1/4*D23N.*fsyW - 1/4*D23N.*nsyW + 1/4*D23N - 1/4*D23S.*fsyW.*nsyW + 1/4*D23S.*fsyW + 1/4*D23S.*nsyW - 1/4*D23S - 1/12*D31W.*fryN.*wW_NW + 1/12*D31W.*wW_NW - 1/12*D31W.*wW_SW + 1/6*D32W.*fryN.*wW_NW - 1/6*D32W.*wW_NW + 1/6*D32W.*wW_SW - 1/12*D35W.*fryN.*wW_NW + 1/12*D35W.*wW_NW - 1/12*D35W.*wW_SW))./(dx.^2.*dy);
% c2UyE = (D33E.*dy.*(-fsyE.*nsyE + fsyE + nsyE - 1) + dx.*(-1/4*D23N.*fsyE.*nsyE + 1/4*D23N.*fsyE + 1/4*D23N.*nsyE - 1/4*D23N + 1/4*D23S.*fsyE.*nsyE - 1/4*D23S.*fsyE - 1/4*D23S.*nsyE + 1/4*D23S + 1/12*D31E.*fryN.*wE_NE - 1/12*D31E.*wE_NE + 1/12*D31E.*wE_SE - 1/6*D32E.*fryN.*wE_NE + 1/6*D32E.*wE_NE - 1/6*D32E.*wE_SE + 1/12*D35E.*fryN.*wE_NE - 1/12*D35E.*wE_NE + 1/12*D35E.*wE_SE))./(dx.^2.*dy);
% c2UyS = (dx.*(1/3*D21S - 2/3*D22S + 1/3*D25S) + dy.*(1/4*D23S.*fsyE.*nsyE + 1/4*D23S.*fsyE - 1/4*D23S.*fsyW.*nsyW - 1/4*D23S.*fsyW - 1/4*D23S.*nsyE + 1/4*D23S.*nsyW - 1/12*D31E.*wE_S + 1/12*D31W.*wW_S + 1/6*D32E.*wE_S - 1/6*D32W.*wW_S - 1/12*D35E.*wE_S + 1/12*D35W.*wW_S))./(dx.*dy.^2);
% c2UyN = (dx.*(-1/3*D21N.*fryN + 1/3*D21N + 2/3*D22N.*fryN - 2/3*D22N - 1/3*D25N.*fryN + 1/3*D25N) + dy.*(-1/4*D23N.*fsyE.*nsyE - 1/4*D23N.*fsyE + 1/4*D23N.*fsyW.*nsyW + 1/4*D23N.*fsyW + 1/4*D23N.*nsyE - 1/4*D23N.*nsyW - 1/12*D31E.*fryN.*wE_N + 1/12*D31E.*wE_N + 1/12*D31W.*fryN.*wW_N - 1/12*D31W.*wW_N + 1/6*D32E.*fryN.*wE_N - 1/6*D32E.*wE_N - 1/6*D32W.*fryN.*wW_N + 1/6*D32W.*wW_N - 1/12*D35E.*fryN.*wE_N + 1/12*D35E.*wE_N + 1/12*D35W.*fryN.*wW_N - 1/12*D35W.*wW_N))./(dx.*dy.^2);
% c2UxSW = (1/4*dx.^2.*(D23N - D23S.*fsyS.*nsyS - D23S.*fsyS + D23S.*nsyS) + dx.*dy.*(-2/3*D21S + 1/3*D22S + 1/3*D25S - D33W) + dy.^2.*(1/6*D31E.*wE_S - 1/6*D31W.*wW_S + 1/6*D31W.*wW_SW - 1/12*D32E.*wE_S + 1/12*D32W.*wW_S - 1/12*D32W.*wW_SW - 1/12*D35E.*wE_S + 1/12*D35W.*wW_S - 1/12*D35W.*wW_SW))./(dx.^2.*dy.^2);
% c2UxSE = (1/4*dx.^2.*(D23N - D23S.*fsyS.*nsyS - D23S.*fsyS + D23S.*nsyS) + dx.*dy.*(2/3*D21S - 1/3*D22S - 1/3*D25S + D33E) + dy.^2.*(-1/6*D31E.*wE_S + 1/6*D31E.*wE_SE + 1/6*D31W.*wW_S + 1/12*D32E.*wE_S - 1/12*D32E.*wE_SE - 1/12*D32W.*wW_S + 1/12*D35E.*wE_S - 1/12*D35E.*wE_SE - 1/12*D35W.*wW_S))./(dx.^2.*dy.^2);
% c2UxNW = (1/4*dx.^2.*(-D23N.*fsyN.*nsyN - D23N.*fsyN + D23N.*nsyN + D23S) + dx.*dy.*(-2/3*D21N.*fryN + 2/3*D21N + 1/3*D22N.*fryN - 1/3*D22N + 1/3*D25N.*fryN - 1/3*D25N + D33W) + dy.^2.*(-1/6*D31E.*fryN.*wE_N + 1/6*D31E.*wE_N + 1/6*D31W.*fryN.*wW_N - 1/6*D31W.*fryN.*wW_NW - 1/6*D31W.*wW_N + 1/6*D31W.*wW_NW + 1/12*D32E.*fryN.*wE_N - 1/12*D32E.*wE_N - 1/12*D32W.*fryN.*wW_N + 1/12*D32W.*fryN.*wW_NW + 1/12*D32W.*wW_N - 1/12*D32W.*wW_NW + 1/12*D35E.*fryN.*wE_N - 1/12*D35E.*wE_N - 1/12*D35W.*fryN.*wW_N + 1/12*D35W.*fryN.*wW_NW + 1/12*D35W.*wW_N - 1/12*D35W.*wW_NW))./(dx.^2.*dy.^2);
% c2UxNE = (1/4*dx.^2.*(-D23N.*fsyN.*nsyN - D23N.*fsyN + D23N.*nsyN + D23S) + dx.*dy.*(2/3*D21N.*fryN - 2/3*D21N - 1/3*D22N.*fryN + 1/3*D22N - 1/3*D25N.*fryN + 1/3*D25N - D33E) + dy.^2.*(1/6*D31E.*fryN.*wE_N - 1/6*D31E.*fryN.*wE_NE - 1/6*D31E.*wE_N + 1/6*D31E.*wE_NE - 1/6*D31W.*fryN.*wW_N + 1/6*D31W.*wW_N - 1/12*D32E.*fryN.*wE_N + 1/12*D32E.*fryN.*wE_NE + 1/12*D32E.*wE_N - 1/12*D32E.*wE_NE + 1/12*D32W.*fryN.*wW_N - 1/12*D32W.*wW_N - 1/12*D35E.*fryN.*wE_N + 1/12*D35E.*fryN.*wE_NE + 1/12*D35E.*wE_N - 1/12*D35E.*wE_NE + 1/12*D35W.*fryN.*wW_N - 1/12*D35W.*wW_N))./(dx.^2.*dy.^2);
% c2UySW = (-1/4*D23S.*fsyW.*nsyW + 1/4*D23S.*fsyW + 1/4*D23S.*nsyW - 1/4*D23S + 1/12*D31W.*wW_SW - 1/6*D32W.*wW_SW + 1/12*D35W.*wW_SW)./(dx.*dy);
% c2UySE = (1/4*D23S.*fsyE.*nsyE - 1/4*D23S.*fsyE - 1/4*D23S.*nsyE + 1/4*D23S - 1/12*D31E.*wE_SE + 1/6*D32E.*wE_SE - 1/12*D35E.*wE_SE)./(dx.*dy);
% c2UyNW = (1/4*D23N.*fsyW.*nsyW - 1/4*D23N.*fsyW - 1/4*D23N.*nsyW + 1/4*D23N + 1/12*D31W.*fryN.*wW_NW - 1/12*D31W.*wW_NW - 1/6*D32W.*fryN.*wW_NW + 1/6*D32W.*wW_NW + 1/12*D35W.*fryN.*wW_NW - 1/12*D35W.*wW_NW)./(dx.*dy);
% c2UyNE = (-1/4*D23N.*fsyE.*nsyE + 1/4*D23N.*fsyE + 1/4*D23N.*nsyE - 1/4*D23N - 1/12*D31E.*fryN.*wE_NE + 1/12*D31E.*wE_NE + 1/6*D32E.*fryN.*wE_NE - 1/6*D32E.*wE_NE - 1/12*D35E.*fryN.*wE_NE + 1/12*D35E.*wE_NE)./(dx.*dy);
% c2UxSSW = 1/4*D23S.*(-fsyS.*nsyS + fsyS + nsyS - 1)./dy.^2;
% c2UxSSE = 1/4*D23S.*(-fsyS.*nsyS + fsyS + nsyS - 1)./dy.^2;
% c2UxSWW = wW_SW.*(-1/6*D31W + 1/12*D32W + 1/12*D35W)./dx.^2;
% c2UxSEE = wE_SE.*(-1/6*D31E + 1/12*D32E + 1/12*D35E)./dx.^2;
% c2UxNWW = wW_NW.*(1/6*D31W.*fryN - 1/6*D31W - 1/12*D32W.*fryN + 1/12*D32W - 1/12*D35W.*fryN + 1/12*D35W)./dx.^2;
% c2UxNEE = wE_NE.*(1/6*D31E.*fryN - 1/6*D31E - 1/12*D32E.*fryN + 1/12*D32E - 1/12*D35E.*fryN + 1/12*D35E)./dx.^2;
% c2UxNNW = 1/4*D23N.*(-fsyN.*nsyN + fsyN + nsyN - 1)./dy.^2;
% c2UxNNE = 1/4*D23N.*(-fsyN.*nsyN + fsyN + nsyN - 1)./dy.^2;
sc2Uy = 1;
c2UyC = -Diry.*sc2Uy + (Diry - 1).*((-NeuN.*(-D21S.*ps./dy + D22S.*(-ps./dy + 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D25S.*ps./dy) + NeuS.*(D21N.*ps./dy + D22N.*(ps./dy - 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D25N.*ps./dy) + (1 - NeuN).*(D21N.*ps./dy + D22N.*(ps./dy - 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D25N.*ps./dy) + (NeuS - 1).*(-D21S.*ps./dy + D22S.*(-ps./dy + 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D25S.*ps./dy))./dy + (1 - NeuN).*(1 - NeuS).*(D31E.*(1/4*ps.*wE_N./dy - 1/4*ps.*wE_S./dy) - D31W.*(1/4*ps.*wW_N./dy - 1/4*ps.*wW_S./dy) + D32E.*(1/4*wE_N.*(ps./dy - 1./dy) + 1/4*wE_S.*(-ps./dy + 1./dy)) - D32W.*(1/4*wW_N.*(ps./dy - 1./dy) + 1/4*wW_S.*(-ps./dy + 1./dy)) + D33E.*(1 - fsyE).*(-nsyE - 1)./dx - D33W.*(1 - fsyW).*(nsyW + 1)./dx + D35E.*(1/4*ps.*wE_N./dy - 1/4*ps.*wE_S./dy) - D35W.*(1/4*ps.*wW_N./dy - 1/4*ps.*wW_S./dy))./dx);
c2UyW = (Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyW).*(nsyW - 1)./dx - 1/4*D23S.*NeuN.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23S.*(1 - fsyW).*(NeuS - 1).*(nsyW - 1)./dx)./dy + (1 - NeuN).*(1 - NeuS).*(-D31W.*(1/4*ps.*wW_NW./dy - 1/4*ps.*wW_SW./dy) - D32W.*(1/4*wW_NW.*(ps./dy - 1./dy) + 1/4*wW_SW.*(-ps./dy + 1./dy)) - D33W.*(1 - fsyW).*(nsyW - 1)./dx - D35W.*(1/4*ps.*wW_NW./dy - 1/4*ps.*wW_SW./dy))./dx);
c2UyE = (Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyE).*(1 - nsyE)./dx - 1/4*D23S.*NeuN.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23S.*(1 - fsyE).*(1 - nsyE).*(NeuS - 1)./dx)./dy + (1 - NeuN).*(1 - NeuS).*(D31E.*(1/4*ps.*wE_NE./dy - 1/4*ps.*wE_SE./dy) + D32E.*(1/4*wE_NE.*(ps./dy - 1./dy) + 1/4*wE_SE.*(-ps./dy + 1./dy)) + D33E.*(1 - fsyE).*(1 - nsyE)./dx + D35E.*(1/4*ps.*wE_NE./dy - 1/4*ps.*wE_SE./dy))./dx);
c2UyS = (Diry - 1).*((-NeuN.*(D21S.*ps./dy + D22S.*(ps./dy - 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D25S.*ps./dy) + (NeuS - 1).*(D21S.*ps./dy + D22S.*(ps./dy - 1./dy) + D23S.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) + D25S.*ps./dy))./dy + (1 - NeuN).*(1 - NeuS).*(1/4*D31E.*ps.*wE_S./dy - 1/4*D31W.*ps.*wW_S./dy + 1/4*D32E.*wE_S.*(ps./dy - 1./dy) - 1/4*D32W.*wW_S.*(ps./dy - 1./dy) + 1/4*D35E.*ps.*wE_S./dy - 1/4*D35W.*ps.*wW_S./dy)./dx);
c2UyN = (Diry - 1).*((NeuS.*(-D21N.*ps./dy + D22N.*(-ps./dy + 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D25N.*ps./dy) + (1 - NeuN).*(-D21N.*ps./dy + D22N.*(-ps./dy + 1./dy) + D23N.*(1/4*(1 - fsyE).*(-nsyE - 1)./dx + 1/4*(1 - fsyW).*(nsyW + 1)./dx) - D25N.*ps./dy))./dy + (1 - NeuN).*(1 - NeuS).*(-1/4*D31E.*ps.*wE_N./dy + 1/4*D31W.*ps.*wW_N./dy + 1/4*D32E.*wE_N.*(-ps./dy + 1./dy) - 1/4*D32W.*wW_N.*(-ps./dy + 1./dy) - 1/4*D35E.*ps.*wE_N./dy + 1/4*D35W.*ps.*wW_N./dy)./dx);
c2UxSW = (Diry - 1).*((-1/4*D23N.*NeuS./dy - 1/4*D23N.*(1 - NeuN)./dy - NeuN.*(D21S.*(ps.*(1 - NeuN)./dx - 1./dx) + D22S.*ps.*(1 - NeuN)./dx + D23S.*(1/4*(1 - fsyS).*(nsyS + 1)./dy - 1/4./dy) + D25S.*ps.*(1 - NeuN)./dx) + (NeuS - 1).*(D21S.*(ps.*(1 - NeuN)./dx - 1./dx) + D22S.*ps.*(1 - NeuN)./dx + D23S.*(1/4*(1 - fsyS).*(nsyS + 1)./dy - 1/4./dy) + D25S.*ps.*(1 - NeuN)./dx))./dy + (1 - NeuN).*(1 - NeuS).*(1/4*D31E.*wE_S.*(ps.*(1 - NeuN)./dx - 1./dx) - D31W.*(1/4*wW_S.*(ps.*(1 - NeuN)./dx - 1./dx) + 1/4*wW_SW.*(-ps.*(1 - NeuN)./dx + 1./dx)) + 1/4*D32E.*ps.*wE_S.*(1 - NeuN)./dx - D32W.*(1/4*ps.*wW_S.*(1 - NeuN)./dx - 1/4*ps.*wW_SW.*(1 - NeuN)./dx) + D33W./dy + 1/4*D35E.*ps.*wE_S.*(1 - NeuN)./dx - D35W.*(1/4*ps.*wW_S.*(1 - NeuN)./dx - 1/4*ps.*wW_SW.*(1 - NeuN)./dx))./dx);
c2UxSE = (Diry - 1).*((-1/4*D23N.*NeuS./dy - 1/4*D23N.*(1 - NeuN)./dy - NeuN.*(D21S.*(-ps.*(1 - NeuN)./dx + 1./dx) - D22S.*ps.*(1 - NeuN)./dx + D23S.*(1/4*(1 - fsyS).*(nsyS + 1)./dy - 1/4./dy) - D25S.*ps.*(1 - NeuN)./dx) + (NeuS - 1).*(D21S.*(-ps.*(1 - NeuN)./dx + 1./dx) - D22S.*ps.*(1 - NeuN)./dx + D23S.*(1/4*(1 - fsyS).*(nsyS + 1)./dy - 1/4./dy) - D25S.*ps.*(1 - NeuN)./dx))./dy + (1 - NeuN).*(1 - NeuS).*(D31E.*(1/4*wE_S.*(-ps.*(1 - NeuN)./dx + 1./dx) + 1/4*wE_SE.*(ps.*(1 - NeuN)./dx - 1./dx)) - 1/4*D31W.*wW_S.*(-ps.*(1 - NeuN)./dx + 1./dx) + D32E.*(-1/4*ps.*wE_S.*(1 - NeuN)./dx + 1/4*ps.*wE_SE.*(1 - NeuN)./dx) + 1/4*D32W.*ps.*wW_S.*(1 - NeuN)./dx - D33E./dy + D35E.*(-1/4*ps.*wE_S.*(1 - NeuN)./dx + 1/4*ps.*wE_SE.*(1 - NeuN)./dx) + 1/4*D35W.*ps.*wW_S.*(1 - NeuN)./dx)./dx);
c2UxNW = (Diry - 1).*((-1/4*D23S.*NeuN./dy + 1/4*D23S.*(NeuS - 1)./dy + NeuS.*(D21N.*(ps.*(1 - NeuS)./dx - 1./dx) + D22N.*ps.*(1 - NeuS)./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4./dy) + D25N.*ps.*(1 - NeuS)./dx) + (1 - NeuN).*(D21N.*(ps.*(1 - NeuS)./dx - 1./dx) + D22N.*ps.*(1 - NeuS)./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4./dy) + D25N.*ps.*(1 - NeuS)./dx))./dy + (1 - NeuN).*(1 - NeuS).*(1/4*D31E.*wE_N.*(ps.*(1 - NeuS)./dx - 1./dx) - D31W.*(1/4*wW_N.*(ps.*(1 - NeuS)./dx - 1./dx) + 1/4*wW_NW.*(-ps.*(1 - NeuS)./dx + 1./dx)) + 1/4*D32E.*ps.*wE_N.*(1 - NeuS)./dx - D32W.*(1/4*ps.*wW_N.*(1 - NeuS)./dx - 1/4*ps.*wW_NW.*(1 - NeuS)./dx) - D33W./dy + 1/4*D35E.*ps.*wE_N.*(1 - NeuS)./dx - D35W.*(1/4*ps.*wW_N.*(1 - NeuS)./dx - 1/4*ps.*wW_NW.*(1 - NeuS)./dx))./dx);
c2UxNE = (Diry - 1).*((-1/4*D23S.*NeuN./dy + 1/4*D23S.*(NeuS - 1)./dy + NeuS.*(D21N.*(-ps.*(1 - NeuS)./dx + 1./dx) - D22N.*ps.*(1 - NeuS)./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4./dy) - D25N.*ps.*(1 - NeuS)./dx) + (1 - NeuN).*(D21N.*(-ps.*(1 - NeuS)./dx + 1./dx) - D22N.*ps.*(1 - NeuS)./dx + D23N.*(1/4*(1 - fsyN).*(-nsyN - 1)./dy + 1/4./dy) - D25N.*ps.*(1 - NeuS)./dx))./dy + (1 - NeuN).*(1 - NeuS).*(D31E.*(1/4*wE_N.*(-ps.*(1 - NeuS)./dx + 1./dx) + 1/4*wE_NE.*(ps.*(1 - NeuS)./dx - 1./dx)) - 1/4*D31W.*wW_N.*(-ps.*(1 - NeuS)./dx + 1./dx) + D32E.*(-1/4*ps.*wE_N.*(1 - NeuS)./dx + 1/4*ps.*wE_NE.*(1 - NeuS)./dx) + 1/4*D32W.*ps.*wW_N.*(1 - NeuS)./dx + D33E./dy + D35E.*(-1/4*ps.*wE_N.*(1 - NeuS)./dx + 1/4*ps.*wE_NE.*(1 - NeuS)./dx) + 1/4*D35W.*ps.*wW_N.*(1 - NeuS)./dx)./dx);
c2UySW = (Diry - 1).*((-1/4*D23S.*NeuN.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23S.*(1 - fsyW).*(NeuS - 1).*(nsyW - 1)./dx)./dy + (1 - NeuN).*(1 - NeuS).*(-1/4*D31W.*ps.*wW_SW./dy - 1/4*D32W.*wW_SW.*(ps./dy - 1./dy) - 1/4*D35W.*ps.*wW_SW./dy)./dx);
c2UySE = (Diry - 1).*((-1/4*D23S.*NeuN.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23S.*(1 - fsyE).*(1 - nsyE).*(NeuS - 1)./dx)./dy + (1 - NeuN).*(1 - NeuS).*(1/4*D31E.*ps.*wE_SE./dy + 1/4*D32E.*wE_SE.*(ps./dy - 1./dy) + 1/4*D35E.*ps.*wE_SE./dy)./dx);
c2UyNW = (Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyW).*(nsyW - 1)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyW).*(nsyW - 1)./dx)./dy + (1 - NeuN).*(1 - NeuS).*(1/4*D31W.*ps.*wW_NW./dy - 1/4*D32W.*wW_NW.*(-ps./dy + 1./dy) + 1/4*D35W.*ps.*wW_NW./dy)./dx);
c2UyNE = (Diry - 1).*((1/4*D23N.*NeuS.*(1 - fsyE).*(1 - nsyE)./dx + 1/4*D23N.*(1 - NeuN).*(1 - fsyE).*(1 - nsyE)./dx)./dy + (1 - NeuN).*(1 - NeuS).*(-1/4*D31E.*ps.*wE_NE./dy + 1/4*D32E.*wE_NE.*(-ps./dy + 1./dy) - 1/4*D35E.*ps.*wE_NE./dy)./dx);
c2UxSSW = (Diry - 1).*(-1/4*D23S.*NeuN.*(1 - fsyS).*(nsyS - 1)./dy + 1/4*D23S.*(1 - fsyS).*(NeuS - 1).*(nsyS - 1)./dy)./dy;
c2UxSSE = (Diry - 1).*(-1/4*D23S.*NeuN.*(1 - fsyS).*(nsyS - 1)./dy + 1/4*D23S.*(1 - fsyS).*(NeuS - 1).*(nsyS - 1)./dy)./dy;
c2UxSWW = (1 - NeuN).*(1 - NeuS).*(Diry - 1).*(-1/4*D31W.*wW_SW.*(ps.*(1 - NeuN)./dx - 1./dx) - 1/4*D32W.*ps.*wW_SW.*(1 - NeuN)./dx - 1/4*D35W.*ps.*wW_SW.*(1 - NeuN)./dx)./dx;
c2UxSEE = (1 - NeuN).*(1 - NeuS).*(Diry - 1).*(1/4*D31E.*wE_SE.*(-ps.*(1 - NeuN)./dx + 1./dx) - 1/4*D32E.*ps.*wE_SE.*(1 - NeuN)./dx - 1/4*D35E.*ps.*wE_SE.*(1 - NeuN)./dx)./dx;
c2UxNWW = (1 - NeuN).*(1 - NeuS).*(Diry - 1).*(-1/4*D31W.*wW_NW.*(ps.*(1 - NeuS)./dx - 1./dx) - 1/4*D32W.*ps.*wW_NW.*(1 - NeuS)./dx - 1/4*D35W.*ps.*wW_NW.*(1 - NeuS)./dx)./dx;
c2UxNEE = (1 - NeuN).*(1 - NeuS).*(Diry - 1).*(1/4*D31E.*wE_NE.*(-ps.*(1 - NeuS)./dx + 1./dx) - 1/4*D32E.*ps.*wE_NE.*(1 - NeuS)./dx - 1/4*D35E.*ps.*wE_NE.*(1 - NeuS)./dx)./dx;
c2UxNNW = (Diry - 1).*(1/4*D23N.*NeuS.*(1 - fsyN).*(1 - nsyN)./dy + 1/4*D23N.*(1 - NeuN).*(1 - fsyN).*(1 - nsyN)./dy)./dy;
c2UxNNE = (Diry - 1).*(1/4*D23N.*NeuS.*(1 - fsyN).*(1 - nsyN)./dy + 1/4*D23N.*(1 - NeuN).*(1 - fsyN).*(1 - nsyN)./dy)./dy;
% Scale Dirichlets
sc2Uy = max(c2UyC(:));
c2UyC( Diry==1 ) = sc2Uy;
% Symmetry
if symmetry == 1
    c2UyC(NeuS==1 | NeuN==1) = 1/2*c2UyC(NeuS==1 | NeuN==1); % Half coefficient for stress BC
    c2UyW(NeuS==1 | NeuN==1) = 1/2*c2UyW(NeuS==1 | NeuN==1);
    c2UyE(NeuS==1 | NeuN==1) = 1/2*c2UyE(NeuS==1 | NeuN==1);
    c2UyS(NeuS==1 | NeuN==1) = 1/2*c2UyS(NeuS==1 | NeuN==1);
    c2UyN(NeuS==1 | NeuN==1) = 1/2*c2UyN(NeuS==1 | NeuN==1);
    ind = zeros(size(NumUy)); ind(:,2:end-0) = Diry(:,1:end-1); % Kill connections with Vy S Dirichlet
    c2UyS  ( ind==1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,1:end-1) = Diry(:,2:end-0); % Kill connections with Vy N Dirichlet
    c2UyN  ( ind==1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,2:end-0) = Dirx(1:end-1,:); % Kill connections with Vx SW Dirichlet
    c2UxSW ( ind== 1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,1:end-1) = Dirx(2:end-0,:); % Kill connections with Vx SE Dirichlet
    c2UxSE ( ind== 1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,2:end-0) = Dirx(1:end-1,:); % Kill connections with Vx NW Dirichlet
    c2UxNW ( ind== 1 ) = 0;
    ind = zeros(size(NumUy)); ind(:,1:end-1) = Dirx(2:end-0,:); % Kill connections with Vx NE Dirichlet
    c2UxNE ( ind== 1 ) = 0;
end
% Equation indexes
iUy     = NumUyG(:);
iUyW    = ones(size(Uy));   iUyW(2:end  ,2:end-1) = NumUyG(1:end-1,2:end-1);
iUyE    = ones(size(Uy));   iUyE(1:end-1,2:end-1) = NumUyG(2:end  ,2:end-1);
iUyS    = ones(size(Uy));   iUyS( :     ,2:end-0) = NumUyG( :     ,1:end-1);
iUyN    = ones(size(Uy));   iUyN( :     ,1:end-1) = NumUyG( :     ,2:end  );
iUySW   = ones(size(Uy));  iUySW(2:end  ,2:end-0) = NumUyG(1:end-1,1:end-1);
iUySE   = ones(size(Uy));  iUySE(1:end-1,2:end-0) = NumUyG(2:end  ,1:end-1);
iUyNW   = ones(size(Uy));  iUyNW(2:end  ,1:end-1) = NumUyG(1:end-1,2:end-0);
iUyNE   = ones(size(Uy));  iUyNE(1:end-1,1:end-1) = NumUyG(2:end  ,2:end-0);
iUxSW   = ones(size(Uy));  iUxSW( :     ,2:end-1) = NumUx (1:end-1,1:end-1);
iUxNW   = ones(size(Uy));  iUxNW( :     ,2:end-1) = NumUx (1:end-1,2:end-0);
iUxSE   = ones(size(Uy));  iUxSE( :     ,2:end-1) = NumUx (2:end  ,1:end-1);
iUxNE   = ones(size(Uy));  iUxNE( :     ,2:end-1) = NumUx (2:end  ,2:end-0);
iUxSSW  = ones(size(Uy)); iUxSSW( :     ,3:end-1) = NumUx (1:end-1,1:end-2);
iUxSSE  = ones(size(Uy)); iUxSSE( :     ,3:end-1) = NumUx (2:end  ,1:end-2);
iUxNNW  = ones(size(Uy)); iUxNNW( :     ,2:end-2) = NumUx (1:end-1,3:end  );
iUxNNE  = ones(size(Uy)); iUxNNE( :     ,2:end-2) = NumUx (2:end  ,3:end  );
iUxSWW  = ones(size(Uy)); iUxSWW(2:end  ,2:end-1) = NumUx (1:end-2,1:end-1);
iUxNWW  = ones(size(Uy)); iUxNWW(2:end  ,2:end-1) = NumUx (1:end-2,2:end-0);
iUxSEE  = ones(size(Uy)); iUxSEE(1:end-1,2:end-1) = NumUx (3:end  ,1:end-1);
iUxNEE  = ones(size(Uy)); iUxNEE(1:end-1,2:end-1) = NumUx (3:end  ,2:end-0);
% Triplets
IvvJ_2   = [   iUy(:);   iUy(:);   iUy(:);   iUy(:);   iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);    iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:);     iUy(:) ]';
JvvJ_2    = [   iUy(:);  iUyW(:);  iUyS(:);  iUyE(:);  iUyN(:);  iUySW(:);  iUySE(:);  iUyNW(:);  iUyNE(:);  iUxSW(:);  iUxSE(:);  iUxNW(:);  iUxNE(:);  iUxSSW(:);  iUxSSE(:);  iUxNNW(:);  iUxNNE(:);  iUxSWW(:);  iUxNWW(:);  iUxSEE(:);  iUxNEE(:) ]';
VvvJ_2    = [ c2UyC(:); c2UyW(:); c2UyS(:); c2UyE(:); c2UyN(:); c2UySW(:); c2UySE(:); c2UyNW(:); c2UyNE(:); c2UxSW(:); c2UxSE(:); c2UxNW(:); c2UxNE(:); c2UxSSW(:); c2UxSSE(:); c2UxNNW(:); c2UxNNE(:); c2UxSWW(:); c2UxNWW(:); c2UxSEE(:); c2UxNEE(:) ]';
%% Assembly of the sparse matrix
if SuiteSparse==1, K  = sparse2([IuuJ_2(:); IvvJ_2(:);], [JuuJ_2(:); JvvJ_2(:);], [VuuJ_2(:); VvvJ_2(:);], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
else               K  = sparse ([IuuJ_2(:); IvvJ_2(:);], [JuuJ_2(:); JvvJ_2(:);], [VuuJ_2(:); VvvJ_2(:);], (nx+1)*ny+(ny+1)*nx, (nx+1)*ny+(ny+1)*nx );
end
% % spy
% figure(1), clf
% subplot(211)
% spy(K)
% subplot(212)
% spy(K-K')
% end

