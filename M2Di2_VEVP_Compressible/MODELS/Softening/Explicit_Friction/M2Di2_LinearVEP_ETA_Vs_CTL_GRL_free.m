% =========================================================================
% Compressible Visco-Elasto-ViscoPlastic Consistent Tangent
% and Effective Viscosity approaches --- Power law & Drucker-Prager

% Copyright (C) 2020  Thibault Duretz

% This file is part of M2Di.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

function M2Di2_LinearVEP_ETA_Vs_CTL_GRL_free
% This one works with power law VP and cohesion softening
addpath('../_M2Di2_functions/')
addpath('../../_M2Di2_functions/')
addpath('../../../_M2Di2_functions/')
addpath('../')
addpath('../../')
addpath(genpath('/home/tduretz/soft/SuiteSparse-4.5.6-mexed/'));
addpath('/home/tduretz/soft/SuiteSparse-4.5.6-mexed/CXSparse/MATLAB/CSparse/');
addpath('/home/tduretz/soft/SuiteSparse-4.5.6-mexed/UMFPACK/MATLAB/');


%M2Di_EP = load('/Users/tduretz/Desktop/M2Di_v2/M2Di_EP12_g3/DataM2Di_EP_test01');
close all
clc

free_surf = 1;

%% Switches
SuiteSparse    = 0;                                                             % 0=NO SuiteSparse  1=SuiteSparse
noisy          = 2;                                                             % 0=NO Output       1=Std output     2=Very noisy  3=Very noisy + kspgcr output
Newton         = 2;                                                             % 0=Picard          1=Newton
mat_pert       = 1;                                                             % Thermal or material perturbation
CTL            = 0;
comp           = 1;
restart        = 0;
symmetry       = 1;
ps             = 1/3;
SaveBreakpoint = 0;
saveRunData    = 1;
ShowFigs       = 1;
GRL_cohesion   = 1;
expl_soft      = 1;
soft_inc_form  = 0;
%% Numerics
nx          = 4*100+1;                                                            % Number of cells in x
ny          = 4*30+1;% Number of cells in y
nt          = 101;
dt          = 4e11 ;
%% Physics
xmin        =-5e4;                                                           % min x
xmax        = 5e4;                                                           % max x
ymin        =-3e4;                                                           % min y
ymax        = 0e3;                                                           % max y
Rg          = 8.314;                                                         % Gas constant
g           =-10; 
r           = 2e3;                                                           % Inclusion radius
Ebg         = 1e-15;
Tbg         = 1;                                                             % Background temperature
% Viscoplasticity
dS_vp       = 1e6; 
n_vp        = 1.0;
eta_vp_0    = dS_vp * abs(Ebg)^(-1/n_vp);
% Material 1 - MATRIX
rho(1)      = 2700;
ndis(1)     = 3.3;                                                           % Power Law exponent
Qdis(1)     = 186.5e3;                                                            % Activation energy
Adis(1)     = 3.1623e-26;                                                    % Pre-exponent (Power law model)                                                        % Viscosity for infinite strain rate (Carreau model)
G(1)        = 1e10;              
K(1)        = 5e10;
Coh_soft(1)  = 0;
Coh_ini(1)   = 5e7;
Coh_end(1)   = Coh_ini(1)/2;
Coh_Ein(1)   = 4e-3;
Coh_dE(1)    = 2e-3;
Phi_soft(1)  = 1;
Phi_ini(1)   = atan(0.6);
Phi_end(1)   = 20/180*pi;
Phi_Ein(1)   = 8e-3;
Phi_dE(1)    = 8e-3;
Psi_soft(1)  = 0;
Psi_ini(1)   = 10*pi/180;
Psi_end(1)   = Psi_ini(1)/2;
Psi_Ein(1)   = 4e-3;
Psi_dE(1)    = 2e-3;
% Material 2 - INCLUSION
rho(2)      = 2700;
ndis(2)     = 1.0;
Qdis(2)     = 0;
Adis(2)     = (1/1e20)^ndis(2)*exp(Qdis(2)/Rg/Tbg);
G(2)        = 1e10;
K(2)        = 5e10;
Coh_soft(2)  = 0;
Coh_ini(2)   = 1e4;
Coh_end(2)   = Coh_ini(2)/2;
Coh_Ein(2)   = 1e-3;
Coh_dE(2)    = 4e-4;
Phi_soft(2)  = 0;
Phi_ini(2)   = 0;
Phi_end(2)   = Phi_ini(2)/2;
Phi_Ein(2)   = 1e-3;
Phi_dE(2)    = 4e-4;
Psi_soft(2)  = 0;
Psi_ini(2)   = 0*10*pi/180;
Psi_end(2)   = Psi_ini(2)/2;
Psi_Ein(2)   = 1e-3;
Psi_dE(2)    = 4e-4;

if comp==0, 
    Psi_ini(1) = 0.0;
    Psi_ini(2) = 0.0;
end

% Non-linear solver settings
nmax_glob   = 20;                                                          % Max. number of non-linear iterations
tol_glob    = 5e-12;                                                         % Global nonlinear tolerance
eps_kspgcr  = 1e-4;                                                          % KSPGCR solver tolerance
LineSearch  = 1;                                                             % Line search algorithm
alpha       = 1.0;                                                           % Default step (in case LineSearch = 0)
% Linear solver settings
lsolver     = 0;                                                             % 0=Backslash 1=Powell-Hestenes
gamma       = 1e-5;                                                          % Numerical compressibility
nPH         = 30;                                                            % Max. number of linear iterations
tol_linu    = tol_glob/5000;                                                 % Velocity tolerance
tol_linp    = tol_glob/1000;                                                 % Divergence tolerance
% Local iterations setting
lit    = 1;
litmax = 100;
littol = 1e-13;
%% Dimensionalisation
h     = 1; % Material phase used for scaling
% Characterictic units
muc   = 1e22;
Tc    = 1;                                                          % Kelvins
Lc    = (xmax-xmin);                                                         % Meters
tc    = 1/Ebg;     
% Derived units
tauc  = muc*(1/tc);                                                          % Pascals
mc    = tauc*Lc*tc^2;                                                        % Kilograms
Jc    = mc*Lc^2/tc^2;                                                        % Joules
Vc    = Lc/tc;
% Scaling
Adis  = Adis./(tauc.^-ndis.*1./tc);
Qdis  = Qdis./Jc;
Rg    = Rg/(Jc/Tc);
Ebg   = Ebg/(1/tc);
Tbg   = Tbg/Tc;
r     = r/Lc;
% C     = C./tauc;
G       = G./tauc;
K       = K./tauc;
g       = g/(Lc/tc^2);
rho     = rho/(mc/Lc^3);
Coh_ini = Coh_ini./tauc;
Coh_end = Coh_end./tauc;
xmin    = xmin/Lc;  xmax = xmax/Lc;
ymin    = ymin/Lc;  ymax = ymax/Lc;
dt       = dt/tc;
eta_vp_0 = eta_vp_0/(tauc*(1/tc)^(-1/n_vp));
%% Preprocessing
tic
Lx = xmax-xmin;     dx = Lx/nx;                                              % Grid spacing x
Ly = ymax-ymin;     dy = Ly/ny;                                              % Grid spacing y
xv = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;                             % Nodal coordinates x
yv = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;                             % Nodal coordinates y
[xv2,  yv2] = ndgrid(xv,yv);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xv,yc);
[xvy2,yvy2] = ndgrid(xc,yv);
rxvec  = zeros(nmax_glob,1);     rpvec  = zeros(nmax_glob,1); cpu = zeros(9,1);
nitervec  = zeros(nt,1);         timev  = zeros(nt,1);
rxvec_rel = zeros(nmax_glob,1);  rxvec_abs = zeros(nmax_glob,1);
rvec_abs  = zeros(nt,nmax_glob); rvec_rel  = zeros(nt,nmax_glob);
stress = zeros(nt,1); stressmin = zeros(nt,1); stressmax = zeros(nt,1); 
press  = zeros(nt,1); pressmin  = zeros(nt,1); pressmax  = zeros(nt,1);
time      = 0;
%% Initial arrays
Vx     = -Ebg.*xvx2;                                                         % Initial solutions for velocity in x
Vy     =  Ebg.*yvy2;                                                         % Initial solutions for velocity in y
Ptc    =    0.* xc2 + 1e-13;%eps;                                                         % Initial solutions for pressure
if comp == 1
    rhoc  = rho(1)*ones(nx  ,ny);                                         % Gravity 1
    rhoc((xc2+0*Lx/2).^2+(yc2+Ly/2).^2<r^2)  = rho(2);
    Ptc    = -cumsum(rhoc.*g.*dy, 2, 'reverse');
end
% close all, imagesc(xc*Lc, yc*Lc,Ptc'*tauc), colorbar, set(gca,'ydir','normal')

phc    =     ones(nx  ,ny  );                                                % Material phase on centroids
phv    =     ones(nx+1,ny+1);                                                % Material phase on vertices
Tsurf  = 293/Tc;                                                           % Surface temperature
gradT  =-15/1e3 / (Tc/Lc);                                                 % Temperature gradient
Tec    =  Tsurf + yc2.*(gradT);                                            % Temperature field                                               % Temperature on centroids
Txxc   =    zeros(nx  ,ny  );
Tyyc   =    zeros(nx  ,ny  );
Tzzc   =    zeros(nx  ,ny  );
Txyv   =    zeros(nx+1,ny+1);
E2_plc   =    zeros(nx  ,ny  );
E2_plv   =    zeros(nx+1,ny+1);
Eiiacc =    zeros(nx  ,ny  );
if mat_pert==0, Tec((xc2+0*Lx/2).^2+(yc2-0*Ly/2).^2<r^2) = Tbg + 0.1*Tbg;    % Thermal perturbation
else            phc((xc2+0*Lx/2).^2+(yc2+Ly/2).^2<r^2) = 2;                % Material perturbation
    phv((xv2+0*Lx/2).^2+(yv2+Ly/2).^2<r^2) = 2;                % Material perturbation
end
% % Initial configuration - Set perturbation of shear modulus
% Gc    =     G(1)*ones(nx  ,ny  ); 
% Gc((xc2).^2+(yc2+Ly/2).^2<r^2) = G(1)/4;
% Kdiff     = 1e-4 /(Lc^2/tc);
% dt_diff   = dx^2/Kdiff/4.1/5;
% diff_time = 1e5 / tc;
% nt_diff   = ceil(diff_time/dt_diff);
% dt_diff   = diff_time/nt_diff;
% for i=1:nt_diff
%     Gc_x = [Gc(1,:); Gc; Gc(end,:)]; 
%     Gc_y = [Gc(:,1), Gc, Gc(:,end)];
%     Gc   =  Gc + Kdiff*dt_diff/dx^2 * diff(Gc_x,2,1) + Kdiff*dt_diff/dy^2 * diff(Gc_y,2,2);
% end
% fprintf('diffusion of jump over a duration of %2.2f and using %02d steps\n', nt_diff*dt_diff, nt_diff);
% [ Gv ] = M2Di2_centroids2vertices( Gc );

Gc     = G(phc);
Gv     = G(phv);
Kc     = K(phc);
Kv     = K(phv);
rhoc   = rho(1)*ones(nx,ny);
Plithc =-cumsum( g*rhoc*dy, 2, 'reverse' );
rhogy  = g*rho(1)*ones(nx  ,ny+1);                                         % Gravity 1
rhogy((xvy2+0*Lx/2).^2+(yvy2-0*Ly/2).^2<r^2)  = g*rho(2);                  % Gravity 2                     
rhogy(:,[1 end]) = 0;                                                      % No body force for Vy Dirichlet nodes  
etaec  = Gc.*dt.*ones(nx  ,ny  );
etaev  = Gv.*dt.*ones(nx+1,ny+1);
Cc     = Coh_ini(phc);
Cv     = Coh_ini(phv);
phic   = Phi_ini(phc);
phiv   = Phi_ini(phv);
psic   = Psi_ini(phc);
psiv   = Psi_ini(phv);
if GRL_cohesion == 1
   Cc = Cc./cos(phic); 
   Cv = Cv./cos(phiv); 
end

% Prepare softening
[Cohc] = CohesionSofteningParams(Coh_soft,Coh_ini,Coh_end,Coh_dE,Coh_Ein,phc);
[Cohv] = CohesionSofteningParams(Coh_soft,Coh_ini,Coh_end,Coh_dE,Coh_Ein,phv);
[Phic] = CohesionSofteningParams(Phi_soft,Phi_ini,Phi_end,Phi_dE,Phi_Ein,phc);
[Phiv] = CohesionSofteningParams(Phi_soft,Phi_ini,Phi_end,Phi_dE,Phi_Ein,phv);
[Psic] = CohesionSofteningParams(Psi_soft,Psi_ini,Psi_end,Psi_dE,Psi_Ein,phc);
[Psiv] = CohesionSofteningParams(Psi_soft,Psi_ini,Psi_end,Psi_dE,Psi_Ein,phv);
% Initial condition for softening
Cc   = Cohc.ini - Cohc.soft_on .* Cohc.Delta/2 .* erfc( -(E2_plc-Cohc.Ein)./Cohc.dE);
Cv   = Cohv.ini - Cohv.soft_on .* Cohv.Delta/2 .* erfc( -(E2_plv-Cohv.Ein)./Cohv.dE);
phic = Phic.ini - Phic.soft_on .* Phic.Delta/2 .* erfc( -(E2_plc-Phic.Ein)./Phic.dE);
phiv = Phiv.ini - Phiv.soft_on .* Phiv.Delta/2 .* erfc( -(E2_plv-Phiv.Ein)./Phiv.dE);
psic = Psic.ini - Psic.soft_on .* Psic.Delta/2 .* erfc( -(E2_plc-Psic.Ein)./Psic.dE);
psiv = Psiv.ini - Psiv.soft_on .* Psiv.Delta/2 .* erfc( -(E2_plv-Psiv.Ein)./Psiv.dE);
if expl_soft == 1
    Cohc.soft_on_expl = Cohc.soft_on; Cohc.soft_on = 0*Cohc.soft_on; 
    Cohv.soft_on_expl = Cohv.soft_on; Cohv.soft_on = 0*Cohv.soft_on; 
    Phic.soft_on_expl = Phic.soft_on; Phic.soft_on = 0*Phic.soft_on; 
    Phiv.soft_on_expl = Phiv.soft_on; Phiv.soft_on = 0*Phiv.soft_on; 
    Psic.soft_on_expl = Psic.soft_on; Psic.soft_on = 0*Psic.soft_on; 
    Psiv.soft_on_expl = Psiv.soft_on; Psiv.soft_on = 0*Psiv.soft_on; 
end
%% Numbering Ptc and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));
cpu(1)=toc;
%% Define BC's ---------------- NEW STUFF
% x momentum
BC.nsxS    =  zeros(size(Vx)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Vx)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Vx)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Vx)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Vx)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Vx)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Vx)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Vx)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
BC.Dirx    =  zeros(size(Vx)); BC.Dirx([1     end], :  ) = 1;
BC.NeuW    =  zeros(size(Vx));
BC.NeuE    =  zeros(size(Vx)); 
% y momentum
BC.nsyW    =  zeros(size(Vy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Vy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Vy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Vy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Vy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Vy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Vy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Vy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
if free_surf == 0
    BC.Diry    =  zeros(size(Vy)); BC.Diry( : ,[1     end]) = 1;
    BC.NeuS    =  zeros(size(Vy));
    BC.NeuN    =  zeros(size(Vy)); 
else
    BC.Diry    =  zeros(size(Vy)); BC.Diry( : ,[1        ]) = 1;
    BC.NeuS    =  zeros(size(Vy));
    BC.NeuN    =  zeros(size(Vy)); BC.NeuN( : ,[      end]) = 1;  BC.PtN = 0;
end
% % Free surface
BC.frxN    =  zeros(size(Vx)); %BC.frxN(2:end-1,end) = 1;
BC.fryN    =  zeros(size(Vy)); %BC.fryN( :     ,end) = 1;
%% Boundary values - Dirichlets
Vx_W   = -Ebg.*xvx2(1  ,:)'.*ones(1 ,ny  )';                                 % BC value Vx West
Vx_E   = -Ebg.*xvx2(end,:)'.*ones(1 ,ny  )';                                 % BC value Vx East
Vy_S   =  Ebg.*yvy2(:,  1) .*ones(nx,1   );                                  % BC value Vy South
Vy_N   =  Ebg.*yvy2(:,end) .*ones(nx,1   );                                  % BC value Vy North
Vx_S = zeros(nx+1,1);  Vx_N = zeros(nx+1,1);
Vy_W = zeros(1,ny+1)'; Vy_E = zeros(1,ny+1)';
%% Construct BC structure ---------------- NEW STUFF
BC.Ux_W = Vx_W; BC.Ux_E = Vx_E; BC.Ux_S = Vx_S; BC.Ux_N = Vx_N;
BC.Uy_S = Vy_S; BC.Uy_N = Vy_N; BC.Uy_W = Vy_W; BC.Uy_E = Vy_E;
cpu(2)=toc;
%% Assemble pressure gradient and divergence operator
tic
[ div ] = M2Di2_AssembleDivergence_v3( BC, ones(size(Ptc)), NumVx, NumVyG, NumPt, dx, dy, nx, ny, symmetry, SuiteSparse );
grad = -div';
%% Assemble Block PP
iPt   = NumPt;  I = iPt(:)';  J = I;                                     % Eq. index center (pressure diagonal)
if comp==0, V     =        gamma.*ones(size(Ptc(:))); end                    % Center coeff.
if comp==1, V     = 1./Kc(:)./dt.*ones(size(Ptc(:))); end                    % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V);                                  % Matrix assembly
else               PP = sparse (I,J,V); end
PPI   = spdiags(1./diag(PP),0,PP);                                       % Trivial inverse of diagonal PP matrix
cpu(3)=toc;

it0    = 1;
Txxc   =    zeros(nx  ,ny  );
Tyyc   =    zeros(nx  ,ny  );
Tzzc   =    zeros(nx  ,ny  );
Txyv   =    zeros(nx+1,ny+1);
Txxv   =    zeros(nx+1,ny+1);
Tyyv   =    zeros(nx+1,ny+1);
Tzzv   =    zeros(nx+1,ny+1);
Txyc   =    zeros(nx  ,ny  );
Ptv    =    zeros(nx+1,ny+1);

if restart>0,
    load('Breakpoint');
    SaveBreakpoint = 0;
    it0   = it+1;
    nt    = it0;
    
    
    
%     eta_vp_0 = 1e21/muc;
%     dt = dt/100;
%     LineSearch  = 0;
%     symmetry = 0;
%     nmax_glob = 100
%     CTL   = 0;
%     saveRunData = 0;
%     LineSearch  = 1; 
%     noisy       = 3;
   
end
%% Time loop
for it=it0:nt
        plastic = 0;
        fprintf('\n------------- Time step #%03d -------------\n',it);% Initial solutions for pressure
        %% Fields from previous step
        Txxc0  = Txxc;    Txxv0   = Txxv;
        Tyyc0  = Tyyc;    Tyyv0   = Tyyv;
        Tzzc0  = Tzzc;    Tzzv0   = Tzzv;
        Txyv0  = Txyv;    Txyc0   = Txyc;
        Ptc0   = Ptc;     Ptv0    = Ptv;
        E2_plc0 = E2_plc; E2_plv0 = E2_plv;
        Cc0     = Cc;     Cv0     = Cv;
        phic0   = phic;   phiv0   = phiv;
        psic0   = psic;   psiv0   = psiv;
        %% reset fields
%         Vx     = -Ebg.*xvx2;                                                         % Initial solutions for velocity in x
%         Vy     =  Ebg.*yvy2;                                                         % Initial solutions for velocity in y
%         Ptc    =    0.* xc2 + 1e-13;%eps;                                                         % Initial solutions for pressure
%         if comp == 1
%             rhoc  = rho(1)*ones(nx  ,ny);                                         % Gravity 1
%             rhoc((xc2+0*Lx/2).^2+(yc2+Ly/2).^2<r^2)  = rho(2);
%             Ptc    = -cumsum(rhoc.*g.*dy, 2, 'reverse');
%         end
        %% Non linear iterations
        iter=0; resnlu=2*tol_glob; resnlu0=2*tol_glob;
        du = [0*Vx(:); 0*Vy(:)];         dp = zeros(max(NumPt(:)),1);
        rxvec_rel = zeros(nmax_glob,1);    rxvec_abs = zeros(nmax_glob,1);
        rpvec_rel = zeros(nmax_glob,1);    rpvec_abs = zeros(nmax_glob,1);
        % Constant 2D field (temperature perturbation) - Interpolate from centers to nodes and extrapolates boundaries
        Tei = 0.25*(Tec(1:end-1,1:end-1) + Tec(2:end,1:end-1) + Tec(1:end-1,2:end) + Tec(2:end,2:end));
        Tev = zeros(nx+1,ny+1);  Tev(2:end-1,2:end-1)=Tei; Tev([1,end],:)=Tev([2,end-1],:); Tev(:,[1,end])=Tev(:,[2,end-1]);
        % Global Newton-Raphson iterations
        for iter=1:nmax_glob
            %iter = iter+1; 
            if noisy>=1, fprintf('\n *** Nonlin iter %d *** \n', iter ); end
            tic
            % Initial guess or iterative solution for strain increments
            divc        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
            Exxc        = diff(Vx,1,1)/dx - ps*divc;
            Eyyc        = diff(Vy,1,2)/dy - ps*divc;
            Ezzc        =                 - ps*divc;
            Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
            Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
            dVxdy       = diff(Vx_exp,1,2)/dy;
            dVydx       = diff(Vy_exp,1,1)/dx;
            Exyv        = 0.5*( dVxdy + dVydx );
            % Extrapolate trial strain components
            Exyc      = 0.25*(Exyv (1:end-1,1:end-1) + Exyv (2:end,1:end-1) + Exyv (1:end-1,2:end) + Exyv (2:end,2:end));
            [ Exxv  ] = M2Di2_centroids2vertices( Exxc );
            [ Eyyv  ] = M2Di2_centroids2vertices( Eyyc );
            [ Ezzv  ] = M2Di2_centroids2vertices( Ezzc );
            [  divv ] = M2Di2_centroids2vertices(  divc);
            [  Ptv  ] = M2Di2_centroids2vertices(  Ptc );
            
            
%             if iter==1
%                 Plithc = Ptc; Plithv = Ptv;
%             else
%                 Plithc =-cumsum( g*rhoc*dy, 2, 'reverse' );
%                 [  Plithv  ] = M2Di2_centroids2vertices(  Plithc );
%             end
            
            % Engineering convention
            Gxyc = 2*Exyc; Gxyv = 2*Exyv;
            % Viscosity
            mc   = 1/2*( 1./ndis(phc) - 1);
            mv   = 1/2*( 1./ndis(phv) - 1);
            Bc   = Adis(phc).^(-1./ndis(phc)) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
            Bv   = Adis(phv).^(-1./ndis(phv)) .* exp(Qdis(phv)/Rg./Tev./ndis(phv));
            if soft_inc_form == 1
                [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,Eii_elc, Eii_pwlc, Eii_plc,OverS_c,CTL_c, E2_plc, Cc, phic, psic, gdotc, dQc ] = M2Di2_LocalIteration_VEP_comp_v11( litmax, littol, noisy, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_vp_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0, Psic, psic0 );
                [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, E2_plv, Cv, phiv, psiv, gdotv, dQv ] = M2Di2_LocalIteration_VEP_comp_v11( litmax, littol, noisy, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_vp_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0, Psiv, psiv0 );
            else
                [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,Eii_elc, Eii_pwlc, Eii_plc,OverS_c,CTL_c, E2_plc, Cc, phic, psic, gdotc, dQc ] = M2Di2_LocalIteration_VEP_comp_v12_NoInc( litmax, littol, noisy, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_vp_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0, Psic, psic0, expl_soft );
                [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, E2_plv, Cv, phiv, psiv, gdotv, dQv ] = M2Di2_LocalIteration_VEP_comp_v12_NoInc( litmax, littol, noisy, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_vp_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0, Psiv, psiv0, expl_soft );
            end
            if (sum(plc(:))>0 || sum(plv(:))>0), fprintf('ACHTUNG: On the yield!\n'); plastic=1; end

            if Newton > 0
                % Derivatives of viscosity
                if CTL==0
                    % Derivatives of corrected VEP viscosity
                    detadexxc =     detac_vep.dExx;   ddivpdexxc =     ddivpc.dExx;
                    detadeyyc =     detac_vep.dEyy;   ddivpdeyyc =     ddivpc.dEyy;
                    detadgxyc = 1/2*detac_vep.dExy;   ddivpdgxyc = 1/2*ddivpc.dExy;
                    detadpc   =     detac_vep.dP;     ddivpdpc   =     ddivpc.dP;
                    detadezzc =     detac_vep.dEzz;   ddivpdezzc =     ddivpc.dEzz;
                else
                    % Derivatives of trial VE viscosity
                    detadexxc =     detac_ve.dExx;
                    detadeyyc =     detac_ve.dEyy;
                    detadgxyc = 1/2*detac_ve.dExy;
                    detadpc   = zeros(size(Ptc));
                    detadezzc =     detac_ve.dEzz;
                end
                
                % Derivatives of viscosity
                if CTL==0
                    % Derivatives of corrected VEP viscosity
                    detadexxv =     detav_vep.dExx;
                    detadeyyv =     detav_vep.dEyy;
                    detadgxyv = 1/2*detav_vep.dExy;
                    detadpv   =       detav_vep.dP;
                    detadezzv =     detav_vep.dEzz;
                else
                    % Derivatives of trial VE viscosity
                    detadexxv =     detav_ve.dExx;
                    detadeyyv =     detav_ve.dEyy;
                    detadgxyv = 1/2*detav_ve.dExy;
                    detadpv   = zeros(size(Ptv));
                    detadezzv =     detav_ve.dEzz;
                end
            end
            %% Rheological coefficients for the Picard operator
            D.D11c = 2*etac; D.D12c = 0*etac; D.D13c = 0*etac; D.D14c = 0*etac;    D.D15c = 0*etac;
            D.D21c = 0*etac; D.D22c = 2*etac; D.D23c = 0*etac; D.D24c = 0*etac;    D.D25c = 0*etac;
            D.D31v = 0*etav; D.D32v = 0*etav; D.D33v = 1*etav; D.D34v = 0*etav;    D.D35v = 0*etav;
            D.D41c = 0*etac; D.D42c = 0*etac; D.D43c = 0*etac; D.D44c = 1;         D.D45c = 0*etac;
            %% Assemble Picard operator
            [ K ] = M2Di2_GeneralAnisotropicVVAssembly_v4( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, symmetry, Vx, Vy, ps, SuiteSparse );
                        
            %% Rheological coefficients for the Jacobian
            if Newton > 0
                Exxc1 = Exxc + Txxc0./2./etaec;
                Eyyc1 = Eyyc + Tyyc0./2./etaec;
                Gxyv1 = Gxyv + Txyv0./1./etaev;
                if CTL==1
                    % Compute consistent tangent operator
                    %% IN PROGRESS
                    [D]   = M2Di2_EvaluateConsistenTangent_DP_VM_comp1        (plc,CTL_c.dg,CTL_c.Txx,CTL_c.Tyy,CTL_c.Tzz,CTL_c.Txy,CTL_c.Pt,CTL_c.Jii,CTL_c.dQdtxx,CTL_c.dQdtyy,CTL_c.dQdtzz,CTL_c.dQdtxy,CTL_c.dQdP,psic,phic1,CTL_c.eta_t,         plv,CTL_v.dg,CTL_v.Txx,CTL_v.Tyy,CTL_v.Tzz,CTL_v.Txy,CTL_v.Pt,CTL_v.Jii,CTL_v.dQdtxx,CTL_v.dQdtyy,CTL_v.dQdtzz,CTL_v.dQdtxy,CTL_v.dQdP,psiv,phiv1,CTL_v.eta_t,Newton,Kc,Kv,dt,comp,CTL_c,CTL_v);
                else
                    % Newton: Derivative of VEP rheological operator
                    dd = 1;
                    bb = 0;
                    D.D11c = 2*etac +   2*detadexxc.*Exxc1-dd*Kc.*dt.*ddivpdexxc; D.D12c =            2*detadeyyc.*Exxc1-dd*Kc.*dt.*ddivpdeyyc; D.D13c =            2*detadgxyc.*Exxc1-dd*Kc.*dt.*ddivpdgxyc; D.D14c = 2*detadpc.*Exxc1-dd*Kc.*dt.*ddivpdpc;     D.D15c =  2*detadezzc.*Exxc1-dd*Kc.*dt.*ddivpdezzc;
                    D.D21c =            2*detadexxc.*Eyyc1-dd*Kc.*dt.*ddivpdexxc; D.D22c = 2*etac +   2*detadeyyc.*Eyyc1-dd*Kc.*dt.*ddivpdeyyc; D.D23c =            2*detadgxyc.*Eyyc1-dd*Kc.*dt.*ddivpdgxyc; D.D24c = 2*detadpc.*Eyyc1-dd*Kc.*dt.*ddivpdpc;     D.D25c =  2*detadezzc.*Eyyc1-dd*Kc.*dt.*ddivpdezzc;
                    D.D31v =            1*detadexxv.*Gxyv1; D.D32v =            1*detadeyyv.*Gxyv1; D.D33v = 1*etav +   1*detadgxyv.*Gxyv1; D.D34v = 1*detadpv.*Gxyv1;     D.D35v =  1*detadezzv.*Gxyv1;
                    D.D41c =           -1*bb*Kc.*dt.*ddivpdexxc; D.D42c =            -1*bb*Kc.*dt.*ddivpdeyyc; D.D43c =            -1*bb*Kc.*dt.*ddivpdgxyc; D.D44c = 1 - 1*bb*Kc.*dt.*ddivpdpc; D.D45c =  -1*bb*Kc.*dt.*ddivpdezzc;
                end
            end 
            %% Assemble Jacobian
            [ J ] = M2Di2_GeneralAnisotropicVVAssembly_v4( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, symmetry, Vx, Vy, ps, SuiteSparse );
%             V2PJ = grad;
            %% Assemble Jacobian V2P - old
%             V2PJ       = M2Di2_M2TAssembly( BC, D, dx, dy, nx, ny, 0, 0, NumVx, NumVyG, NumPt, Vx, Vy, SuiteSparse );
%             V2PJ       = V2PJ + grad ;
            %% Assemble Jacobian V2P - new
            V2PJ       = M2Di2_M2TAssembly_v3( BC, D, dx, dy, nx, ny, 0, 0, NumVx, NumVyG, NumPt, Vx, Vy, symmetry, SuiteSparse );
            %% Jacobian of pressure block: PPJ operator
            if comp==1,
                cPC    = (D.D44c) ./ (Kc*dt);
                D.D41c = D.D41c;
                D.D42c = D.D42c;
                D.D43c = D.D43c;
                D.D45c = D.D45c;
                Ipj = NumPt(:);
                Jpj = NumPt(:);
                Vpj = cPC(:);
                PPJ = sparse2( Ipj, Jpj, Vpj );
                %% Jacobian of thermo-mechanical coupling: T2MJ operator
                P2VJ = M2Di2_P2MAssembly( BC, D, dx, dy, nx, ny, etac, Kc, dt, NumVx, NumVyG, NumPt, Ptc, SuiteSparse );
                P2VJ = P2VJ + div;
            end
               
            %% Residual in matrix-free form
            Res_x      = [zeros(1,ny); diff(-Ptc1 + Txxc,1,1)/dx + diff(Txyv(2:end-1,:),1,2)/dy; zeros(1,ny)];
   
            if free_surf == 0
                Res_y      = [zeros(nx,1), diff(-Ptc1 + Tyyc,1,2)/dy + diff(Txyv(:,2:end-1),1,1)/dx, zeros(nx,1)] + rhogy;
            else
                syy          = -Ptc1 + Tyyc;
                syy_ex     = [syy, -2*BC.PtN-syy(:,end)]; % Pressure boundary condition
                Res_y      = [zeros(nx,1), diff(syy_ex,1,2)/dy + diff(Txyv(:,2:end),1,1)/dx] + rhogy;
                if symmetry==1, Res_y(:,end) =   Res_y(:,end)/2; end
            end
%             Res_x = Res_x(2:end-1,:);
%             Res_y = Res_(:,2:end-1);
            % Evaluate non-Linear residual using matrix-vector products
            fu = [Res_x(:); Res_y(:)];      resnlu = norm(fu)/length(fu);
            u  = [Vx(:); Vy(:)]; p = Ptc(:); 
         
            Divc           =  divc - Ptc0./Kc./dt;
            if comp==0, fp = -divc                           ; resnlp = norm(fp(:))/length(fp); end
%             if comp==1, fp = -((Divc - divpc)   + (Ptc)./Kc./dt); resnlp = norm(fp(:))/length(fp); end  % Pressure non-linear residuals

            if comp==1, fp = -((Divc - divpc)   + (Ptc)./Kc./dt); resnlp = norm(fp(:))/length(fp); end  % Pressure non-linear residuals
            if comp==1, fp = 0*divpc - Divc - (Ptc)./Kc./dt;  resnlp = norm(fp(:))/length(fp); end
            
            
            
            
            if free_surf == 0
                val          = 0;
                equ          = NumPt((nx-1)/2+1, end);
                fp(equ )     = val;
            end
            resnlp = norm(fp(:))/length(fp);
            
            
            
            
            %         if comp==1, fp = -(divc   + (Ptc-Ptc0)./Kvepc./dt); resnlp = norm(fp(:))/length(fp); end  % Pressure non-linear residuals
            fp = fp(:);

            if iter==1, resnlu0=resnlu; resnlp0=resnlp; end
            rxvec_abs(iter) = resnlu;
            rxvec_rel(iter) = resnlu/resnlu0;
            rpvec_abs(iter) = resnlp;
            rpvec_rel(iter) = resnlp/resnlp0;
            if noisy>=1,fprintf('Chk: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', resnlu, resnlu/resnlu0 );
                fprintf('Chk: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', resnlp, resnlp/resnlp0 ); rxvec(iter) = resnlu/resnlu0; rpvec(iter) = resnlp/resnlp0; end
            %         if (resnlu/resnlu0 < tol_glob  || resnlu<tol_glob), break; end
            if (resnlu < tol_glob && resnlp < tol_glob ), break; end
            cpu(4)=cpu(4)+toc;
            
            %% Linear solver - obtain velocity and pressure corrections for current non linear iteration
            if lsolver==0  % Backslash coupled solver ------------------------------
                tic
                                
                if comp == 0
                    P2VJ      =  div;
                    PPJ       = 0*PP;
                end
                
                % Set 0 pressure correction at the surface, in the center
                if free_surf == 0
                    % BC
                    equ          = NumPt((nx-1)/2+1, end);
                    val          = 0*dy/2*rho(1)*g;
                    %             bu           = bu - grad(:,equ)*val;
                    V2PJ(:,equ)  = 0;
                    P2VJ(equ,:)  = 0;
                    fp(equ )     = val;
                    PPd          = 0*PP;
                    PPJ(equ,equ) = 1;
                    V2PJ(:,equ)  = 0; 
                end


                
                Ms  = [ J   , V2PJ ; ...
                       P2VJ , PPJ ];                                                 % Assemble entire Jacobian
                f   = [ fu  ; fp   ];                                                % Assemble entire residual vector
                cpu(5)=cpu(5)+toc;
                tic
                dX  = Ms\f;                                                          % Call direct solver
                du  = dX(1:max(NumVyG(:)));                                          % Extract velocity correction
                dp  = dX(NumPtG(:));                                                 % Extract pressure correction
                f1  = Ms*dX - f;                                                     % Compute entire linear residual
                fu1 = f1(1:max(NumVyG(:)));                                          % Extract velocity linear residual
                fp1 = f1(NumPtG(:));                                                 % Extract pressure linear residual
                cpu(6)=cpu(6)+toc;
            elseif lsolver==1 % Powell-Hestenes INCOMPRESSIBLE - Decoupled/segregated solve --------
                if Newton==0, J = K; end
                tic
                Kt  = K - grad*(PPI*div);                                            % Velocity Schur complement of Picard operator (Kt)
                Jt  = J - grad*(PPI*div);                                            % Velocity Schur complement of Jacobian operator
                [Kc,e,s] = chol(Kt,'lower','vector');                                % Choleski factorization of Kt
                cpu(5)=cpu(5)+toc;
                tic
                % Powell-Hestenes iterations
                fu0 = fu;                                                                    % Save linear norm 0
                for itPH=1:nPH
                    fut  = fu - grad*dp - grad*PPI*fp;                                       % Iterative right hand side
                    [du,norm_r,its] = M2Di2_kspgcr_m(Jt,fut,du,Kc,s,eps_kspgcr,noisy,SuiteSparse); % Apply inverse of Schur complement
                    dp   = dp + PPI*(fp -  div*du);                                          % Pressure corrctions
                    fu1  = fu -   J*du  - grad*dp;                                           % Compute linear velocity residual
                    fp1  = fp - div*du;                                                      % Compute linear pressure residual
                    if noisy>1, fprintf('--- iteration %d --- \n',itPH);
                        fprintf('  Res. |u| = %2.2e \n',norm(fu1)/length(fu1));
                        fprintf('  Res. |p| = %2.2e \n',norm(fp1)/length(fp1));
                        fprintf('  KSP GCR: its=%1.4d, Resid=%1.4e \n',its,norm_r); end
                    if ((norm(fu1)/length(fu1)) < tol_linu) && ((norm(fp1)/length(fp1)) < tol_linp), break; end
                    if ((norm(fu1)/length(fu1)) > (norm(fu0)/length(fu1)) && norm(fu1)/length(fu1) < tol_glob), fprintf(' > Linear residuals do no converge further:\n'); break; end
                    fu0 = fu1;
                end
                cpu(6)=cpu(6)+toc;
            end
            tic
            if noisy>=1, fprintf(' - Linear res. ||res.u||=%2.2e\n', norm(fu1)/length(fu1) );
                fprintf(' - Linear res. ||res.p||=%2.2e\n', norm(fp1)/length(fp1) ); end
            % Call line search - globalization procedure
            if LineSearch==1, [alpha,~] = LineSearch_Direct(soft_inc_form,dd,ps,BC,nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,phic,phiv,psic,psiv,Cc,Cv,Newton,0,1/3,noisy,0,resnlu0,resnlp0,resnlu,resnlp,etaec,etaev,Kc,Kv,Ptc0,Ptv0,Txxc0,Tyyc0,Tzzc0,Txyc0,Txxv0,Tyyv0,Tzzv0,Txyv0,lit,litmax,littol, eta_vp_0, n_vp, dt, E2_plc0, E2_plv0, Cohc, Cohv, Cc0, Cv0, Phic, Phiv, phic0, phiv0, Psic, Psiv, psic0, psiv0, rhogy, free_surf, symmetry, expl_soft ); end
            u    = u + alpha*du;                                                     % Velocity update from non-linear iterations
            p    = p + alpha*dp;                                                     % Pressure update from non-linear iterations
            Vx   = reshape(u(NumVx(:)) ,[nx+1,ny  ]);                                % Velocity in x (2D array)
            Vy   = reshape(u(NumVyG(:)),[nx  ,ny+1]);                                % Velocity in y (2D array)
            Ptc  = reshape(p(NumPt(:)) ,[nx  ,ny  ]);                                % Pressure      (2D array)
            cpu(7)=cpu(7)+toc;
            
                    

        end

        
%         if iter==nmax_glob && (resnlu > tol_glob || resnlp > tol_glob )
%             if SaveBreakpoint==1, save(['Breakpoint']); end
%             error('Failed to converge...')            
%         end
        
        
        if lit == 0
            Txxc      = 2*etac.*Exxc + etac./etaec.*Txxc0;
            Tyyc      = 2*etac.*Eyyc + etac./etaec.*Tyyc0;
            Txyv      = 2*etav.*Exyv + etav./etaev.*Txyv0;
            Txyc      = 0.25*(Txyv (1:end-1,1:end-1) + Txyv (2:end,1:end-1) + Txyv (1:end-1,2:end) + Txyv (2:end,2:end));
        end
        % pressure check
        divc           = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
        P_trial        = -Kc.*dt.*(divc - Ptc0./Kc./dt);
        P_chk2         =    -Kc.*dt.*(divc - divpc - Ptc0./Kc./dt);
        P_chk3         = -Kvepc.*dt.*(divc - Ptc0./Kc./dt);
        divec          = -(Ptc - Ptc0) ./Kc./dt;
        
        if expl_soft == 1
            if soft_inc_form == 1
                hcoh           = -Cohc.Delta.*exp(-(E2_plc - Cohc.Ein).^2./Cohc.dE.^2)./(sqrt(pi)*Cohc.dE);
                hphi           = -Phic.Delta.*exp(-(E2_plc - Phic.Ein).^2./Phic.dE.^2)./(sqrt(pi)*Phic.dE);
                hpsi           = -Psic.Delta.*exp(-(E2_plc - Psic.Ein).^2./Psic.dE.^2)./(sqrt(pi)*Psic.dE);
                Cc             = Cc   + Cohc.soft_on_expl.*hcoh.*gdotc.*dt.*dQc;
                phic           = phic + Phic.soft_on_expl.*hphi.*gdotc.*dt.*dQc;
                psic           = psic + Psic.soft_on_expl.*hpsi.*gdotc.*dt.*dQc;
                hcoh           = -Cohv.Delta.*exp(-(E2_plv - Cohv.Ein).^2./Cohv.dE.^2)./(sqrt(pi)*Cohv.dE);
                hphi           = -Phiv.Delta.*exp(-(E2_plv - Phiv.Ein).^2./Phiv.dE.^2)./(sqrt(pi)*Phiv.dE);
                hpsi           = -Psiv.Delta.*exp(-(E2_plv - Psiv.Ein).^2./Psiv.dE.^2)./(sqrt(pi)*Psiv.dE);
                Cv             = Cv   + Cohv.soft_on_expl.*hcoh.*gdotv.*dt.*dQv;
                phiv           = phiv + Phiv.soft_on_expl.*hphi.*gdotv.*dt.*dQv;
                psiv           = psiv + Psiv.soft_on_expl.*hpsi.*gdotv.*dt.*dQv;
            else
                phic           = Phic.ini - Phic.soft_on_expl.*Phic.Delta/2 .* erfc( -(E2_plc-Phic.Ein)./Phic.dE);
                phiv           = Phiv.ini - Phiv.soft_on_expl.*Phiv.Delta/2 .* erfc( -(E2_plv-Phiv.Ein)./Phiv.dE);
                psic           = Psic.ini - Psic.soft_on_expl.*Psic.Delta/2 .* erfc( -(E2_plc-Psic.Ein)./Psic.dE);
                psiv           = Psiv.ini - Psiv.soft_on_expl.*Psiv.Delta/2 .* erfc( -(E2_plv-Psiv.Ein)./Psiv.dE);
                Cc             = Cohc.ini - Cohc.soft_on_expl.*Cohc.Delta/2 .* erfc( -(E2_plc-Cohc.Ein)./Cohc.dE);
                Cv             = Cohv.ini - Cohv.soft_on_expl.*Cohv.Delta/2 .* erfc( -(E2_plv-Cohv.Ein)./Cohv.dE);
            end
        end

        % Updates
        time           = time + dt;
        Ptc            = Ptc1;
        Tiic           = sqrt(1/2*Txxc.^2 + 1/2*Tyyc.^2 + 1/2*Tzzc.^2 + Txyc.^2);
        Eiic2          = 1/2*(Exxc.^2 + Eyyc.^2 + Ezzc.^2) + Exyc.^2;
        Eiiacc         = Eiiacc + dt*sqrt(Eiic2);
        timev(it)      = time;
        stress(it)     = mean(Tiic(:));
        stressmin(it)  = min(Tiic(:));
        stressmax(it)  = max(Tiic(:));
        press(it)      = mean(Ptc(:));
        pressmin(it)   = min(Ptc(:));
        pressmax(it)   = max(Ptc(:));
        nitervec(it)   = iter;
        
        tic
        

        if plastic == 1 %it == nt
            %% Post-processing
            
%              if mod(it,5) ==0 

%                 figure(5),clf,colormap('jet'),set(gcf,'Color','white'), hold on
%                 plot((1:it)*dt, (stress(1:it)*tauc),'bx-')
%                 %         plot((1:it)*dt, M2Di_EP.Tiivec(1:it),'or')
%                 xlabel('Time'),ylabel('Stress'),drawnow
%                 %         end

%             if ShowFigs == 1
            figure(2),clf,colormap('jet'),set(gcf,'Color','white')
            plot(1:iter, log10(rxvec(1:iter)/rxvec(1)),'rx-', 1:iter, log10(rpvec(1:iter)/rpvec(1)),'bo-')
            xlabel('Iterations'),ylabel('Residuals'),drawnow
            if saveRunData==1, print(['Convergence',num2str(it,'%04d')], '-r300','-dpng'); end
%             end

%             figure(29)
%             subplot(211)
%             imagesc(xv*Lc/1e3, yc*Lc/1e3,Res_x'), axis image, colorbar, set(gca, 'ydir', 'normal')
%             subplot(212)
%             imagesc(xc*Lc/1e3, yv*Lc/1e3,Res_y'), axis image, colorbar, set(gca, 'ydir', 'normal'), drawnow
%             if saveRunData==1, print(['Residuals',num2str(it,'%04d')], '-r300','-dpng'); end
           
            % Invariants
            Eiic2    = 1/2*(Exxc.^2 + Eyyc.^2 + Ezzc.^2) + Exyc.^2;
            Eiiv2    = 1/2*(Exxv.^2 + Eyyv.^2 + Ezzv.^2) + Exyv.^2;
            Tiic2    = 1/2*(Txxc.^2 + Tyyc.^2 + Tzzc.^2) + Txyc.^2;
            
            load('roma.mat')
            if ShowFigs==0, figure('visible','off'), clf; end
            if ShowFigs==1, figure(1              ), clf; end
            subplot(311), set(gcf,'Color','white'), %colormap(jet);
            colormap(flipud(roma))
            imagesc(xc*Lc/1e3, yc*Lc/1e3,(log10(sqrt(Eiic2)'*(1/tc)))),colorbar,axis image,title(['max Eii = ', num2str(max(sqrt(Eiic2(:))*(1/tc)), '%2.4e')]), caxis([-16 -14])
            ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex'), set(gca, 'ydir', 'normal')
            title(['$\dot{\epsilon}_{II}$  [s$^{-1}$] --- $t = $', num2str(time*tc / (1e6*365*24*3600), '%1.4e My')], 'interpreter', 'latex'), set(gca,'FontSize',15),
            subplot(312),set(gcf,'Color','white')
            imagesc(xc*Lc/1e3, yc*Lc/1e3,sqrt(Tiic2)'*tauc/1e6),colorbar,axis image,title(['max tau = ', num2str(max(Tiic(:)*tauc), '%2.4e')]), caxis([20 300]) %caxis([20 450])
            ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex') , set(gca, 'ydir', 'normal')
            title(['$\tau_{II}$ [MPa]'], 'interpreter', 'latex'),set(gca, 'FontSize', 15)
            subplot(313), set(gcf,'Color','white')
            imagesc(xc*Lc/1e3, yc*Lc/1e3,Ptc'*tauc/1e6),colorbar,axis image,title(['max Ptc = ', num2str(max(Ptc(:)*tauc), '%2.4e')]), caxis([0 800])%caxis([0 1000])
            xlabel('$x$ [km]','FontSize', 15, 'interpreter', 'latex'),ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex') , set(gca, 'ydir', 'normal')
            title(['$P$ [MPa]'], 'interpreter', 'latex'),set(gca, 'FontSize', 15)
            drawnow
            if saveRunData==1, print(['GRL',num2str(it,'%04d')], '-r300','-dpng'); end
            
            load('roma.mat')
            if ShowFigs==0, figure('visible','off'), clf; end
            if ShowFigs==1, figure(1              ), clf; end
            subplot(211), set(gcf,'Color','white'), %colormap(jet);
            colormap(flipud(roma))
            imagesc(xc*Lc/1e3, yc*Lc/1e3,plc'),colorbar,axis image,%title(['max Eii = ', num2str(max(sqrt(Eiic2(:))*(1/tc)), '%2.4e')]), caxis([-16 -14])
            ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex'), set(gca, 'ydir', 'normal')
            title(['Plastic centroids --- $t = $', num2str(time*tc / (1e6*365*24*3600), '%1.4e My')], 'interpreter', 'latex'), set(gca,'FontSize',15),
            subplot(212),set(gcf,'Color','white')
            imagesc(xv*Lc/1e3, yv*Lc/1e3,plv'),colorbar,axis image,%title(['max tau = ', num2str(max(Tiic(:)*tauc), '%2.4e')]), caxis([20 300]) %caxis([20 450])
            ylabel('$y$ [km]','FontSize', 15, 'interpreter', 'latex') , set(gca, 'ydir', 'normal')
            title(['Plastic vertices'], 'interpreter', 'latex'),set(gca, 'FontSize', 15)
            drawnow
            if saveRunData==1, print(['Plastic',num2str(it,'%04d')], '-r300','-dpng'); end

            
            if n_vp > 1
                figure(3),clf,colormap('jet'),set(gcf,'Color','white')
                subplot(211)
                imagesc(xc*Lc, yc*Lc, log10(eta_vp_c*muc)),colorbar, axis image, set(gca, 'ydir', 'normal'),%title(['min C = ', num2str(min(Cc_pred(:)*tauc/1e6), '%2.4e')]), caxis([Cend(1)*tauc/1e6 Cini(1)*tauc/1e6])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
            end
            
            
            if Coh_soft(1) == 1 
                figure(3),clf,colormap('jet'),set(gcf,'Color','white')
                subplot(311)
                imagesc(xc*Lc, yc*Lc, E2_plc'),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['min E2pl = ', num2str(min(E2_plc(:)), '%2.4e'), ' max E2pl = ', num2str(max(E2_plc(:)), '%2.4e')])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                subplot(312)
                Cc_pred = Cohc.ini - Cohc.Delta/2 .* erfc( -(E2_plc-Cohc.Ein)./Cohc.dE);
                imagesc(xc*Lc, yc*Lc, Cc_pred'*tauc/1e6),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['pred. min C = ', num2str(min(Cc_pred(:)*tauc/1e6), '%2.4e')]), caxis([Coh_end(1)*tauc/1e6 Coh_ini(1)*tauc/1e6])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                subplot(313)
                imagesc(xc*Lc, yc*Lc, Cc'*tauc/1e6),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['Mod. min C = ', num2str(min(Cc(:)*tauc/1e6), '%2.4e')]), caxis([Coh_end(1)*tauc/1e6 Coh_ini(1)*tauc/1e6])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                drawnow
                if saveRunData==1, print(['Cohesion',num2str(it,'%04d')], '-r300','-dpng'); end
            end
                
            if Phi_soft(1) == 1
                figure(3),clf,colormap('jet'),set(gcf,'Color','white')
                subplot(311)
                imagesc(xc*Lc, yc*Lc, E2_plc'),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['min E2pl = ', num2str(min(E2_plc(:)), '%2.4e'), ' max E2pl = ', num2str(max(E2_plc(:)), '%2.4e')])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                subplot(312)
                Phic_pred = Phic.ini - Phic.Delta/2 .* erfc( -(E2_plc-Phic.Ein)./Phic.dE);
                imagesc(xc*Lc, yc*Lc, Phic_pred'*180/pi),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['pred. min phi = ', num2str(min(Phic_pred(:)*180/pi), '%2.4e')]), caxis([Phi_end(1)*180/pi Phi_ini(1)*180/pi])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                subplot(313)
                imagesc(xc*Lc, yc*Lc, phic'*180/pi),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['Mod. min phi = ', num2str(min(phic(:)*180/pi), '%2.4e')]), caxis([Phi_end(1)*180/pi Phi_ini(1)*180/pi])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                drawnow
                if saveRunData==1, print(['Friction',num2str(it,'%04d')], '-r300','-dpng'); end
            end
            
            if Psi_soft(1) == 1
                figure(3),clf,colormap('jet'),set(gcf,'Color','white')
                subplot(311)
                imagesc(xc*Lc, yc*Lc, E2_plc'),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['min E2pl = ', num2str(min(E2_plc(:)), '%2.4e'), ' max E2pl = ', num2str(max(E2_plc(:)), '%2.4e')])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                subplot(312)
                Psic_pred = Psic.ini - Psic.Delta/2 .* erfc( -(E2_plc-Psic.Ein)./Psic.dE);
                imagesc(xc*Lc, yc*Lc, Psic_pred'*180/pi),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['pred. min psi = ', num2str(min(Psic_pred(:)*180/pi), '%2.4e')]), caxis([Psi_end(1)*180/pi Psi_ini(1)*180/pi])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                subplot(313)
                imagesc(xc*Lc, yc*Lc, psic'*180/pi),colorbar,axis image, set(gca, 'ydir', 'normal')
                title(['Mod. min psi = ', num2str(min(psic(:)*180/pi), '%2.4e')]), caxis([Psi_end(1)*180/pi Psi_ini(1)*180/pi])
                xlabel('x','FontSize',15),ylabel('y','FontSize',15)
                drawnow
                if saveRunData==1, print(['Dilation',num2str(it,'%04d')], '-r300','-dpng'); end
            end
            
%             if ShowFigs==1
%             figure(5), hold on
%             plot(time*tc/1e6/3600/24/365, log10(resnlu), 'o')
%             drawnow
%             end
    

%             figure(111), clf
%             subplot(221)
%             imagesc(divc' - divpc' - divec'), colorbar
%             subplot(222)
%             imagesc(divc'), colorbar
%             subplot(223)
%             imagesc(divpc'), colorbar
%             subplot(224)
%             imagesc(divec'), colorbar
        
            
            %     cpu(8)=toc; cpu(9)=sum(cpu(1:7));
            %     display([' Time preprocess   = ', num2str(cpu(1))]);
            %     display([' Time BC           = ', num2str(cpu(2))]);
            %     display([' Time cst block    = ', num2str(cpu(3))]);
            %     display([' Time assemble     = ', num2str(cpu(4))]);
            %     display([' Time precondition = ', num2str(cpu(5))]);
            %     display([' Time solve        = ', num2str(cpu(6))]);
            %     display([' => WALLTIME      == ', num2str(cpu(9))]);
        end
        if saveRunData==1, save(['Output',num2str(it,'%04d')], 'Lc','tauc','tc','Ptc','Tiic2','Eiic2', 'Eiiacc','dt','stress','stressmax','stressmin','press','pressmax','pressmin','xc','yc', 'Cc', 'phic', 'psic','Eii_elc', 'Eii_pwlc', 'Eii_plc','OverS_c'); end
        if SaveBreakpoint==1, save(['Breakpoint']); end
    end
% save('VEP_ETA', 'timev', 'stress', 'rvec_rel', 'rvec_abs', 'nitervec')
if saveRunData==1, save(['DataRun',num2str(it,'%04d')], 'Lc','tauc','tc','Ptc','Tiic2','Eiic2', 'Eiiacc','dt','stress','stressmax','stressmin','press','pressmax','pressmin','xc','yc', 'Cc', 'phic', 'psic'); end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    FUNCTIONS USED IN MAIN CODE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [alpha, LSsuccess] = LineSearch_Direct(soft_inc_form,dd,ps,BC,nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,phic,phiv,psic,psiv,Cc,Cv,Newton,comp,pls,noisy,INV,resnlu0,resnlp0,resnlu,resnlp,etaec,etaev,Kc,Kv,Ptc0,Ptv0,Txxc0,Tyyc0,Tzzc0,Txyc0,Txxv0,Tyyv0,Tzzv0,Txyv0,lit,litmax,littol, eta_vp_0, n_vp, dt, E2_plc0, E2_plv0, Cohc, Cohv, Cc0, Cv0, Phic, Phiv, phic0, phiv0, Psic, Psiv, psic0, psiv0, rhogy, free_surf, symmetry, expl_soft )
% Line search explicit algorithm
% nsteps = 25;                                   % number of steps
% amin   = 0.05;
%nsteps = 11;                                   % number of steps
%amin   = 0.0;                                  % minimum step
nsteps = 6;                                   % number of steps
amin   = 0.05; 
if Newton>0, amax = 1.0; else amax = 2.0; end % maximum step
dalpha = (amax-amin)/(nsteps-1);
alphav = amin:dalpha:amax;
nVx    = (nx+1)*ny; nRMe   = zeros(nsteps,1); nRCTe  = zeros(nsteps,1);
u0 = u; p0 = p;
% Compute non linear residual for different steps
for ils=1:nsteps;
    % Primitive variables with correction step
    u       = u0 + alphav(ils).*du;
    p       = p0 + alphav(ils).*dp;
    % Evaluate non-Linear residual in matrix-free form
    Ptc     = reshape(p           ,[nx  ,ny  ]);
    Vx      = reshape(u(1:nVx)    ,[nx+1,ny  ]);
    Vy      = reshape(u(nVx+1:end),[nx  ,ny+1]);
    % Initial guess or iterative solution for strain increments
    divc        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
    Exxc        = diff(Vx,1,1)/dx - ps*divc;
    Eyyc        = diff(Vy,1,2)/dy - ps*divc;
    Ezzc        =                 - ps*divc;
%                 Ezzc        = ps*divc         - ps*divc;
% 
    Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
    Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
    dVxdy       = diff(Vx_exp,1,2)/dy;
    dVydx       = diff(Vy_exp,1,1)/dx;
    Exyv        = 0.5*( dVxdy + dVydx );
    % Extrapolate trial strain components
    Exyc      = 0.25*(Exyv (1:end-1,1:end-1) + Exyv (2:end,1:end-1) + Exyv (1:end-1,2:end) + Exyv (2:end,2:end));
    [ Exxv  ] = M2Di2_centroids2vertices( Exxc );
    [ Eyyv  ] = M2Di2_centroids2vertices( Eyyc );
    [ Ezzv  ] = M2Di2_centroids2vertices( Ezzc );
    [  divv ] = M2Di2_centroids2vertices(  divc);
    [  Ptv  ] = M2Di2_centroids2vertices(  Ptc );
    % Engineering convention
    Gxyc = 2*Exyc; Gxyv = 2*Exyv;
    % Viscosity
    mc   = 1/2*( 1./ndis(phc) - 1);
    mv   = 1/2*( 1./ndis(phv) - 1);
    Bc   = Adis(phc).^(-1./ndis(phc)) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
    Bv   = Adis(phv).^(-1./ndis(phv)) .* exp(Qdis(phv)/Rg./Tev./ndis(phv));
    if soft_inc_form == 1
        [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,Eii_elc, Eii_pwlc, Eii_plc,OverS_c,CTL_c, E2_plc, Cc, phic, psic, gdotc, dQc ] = M2Di2_LocalIteration_VEP_comp_v11( litmax, littol, noisy, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_vp_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0, Psic, psic0 );
        [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, E2_plv, Cv, phiv, psiv, gdotv, dQv ] = M2Di2_LocalIteration_VEP_comp_v11( litmax, littol, noisy, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_vp_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0, Psiv, psiv0 );
    else
        [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,Eii_elc, Eii_pwlc, Eii_plc,OverS_c,CTL_c, E2_plc, Cc, phic, psic, gdotc, dQc ] = M2Di2_LocalIteration_VEP_comp_v12_NoInc( litmax, littol, noisy, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_vp_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0, Psic, psic0, expl_soft );
        [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, E2_plv, Cv, phiv, psiv, gdotv, dQv ] = M2Di2_LocalIteration_VEP_comp_v12_NoInc( litmax, littol, noisy, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_vp_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0, Psiv, psiv0, expl_soft );
    end
%     [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,~,~,~,~,CTL_c, E2_plc, Cc, phic ] = M2Di2_LocalIteration_VEP_comp_v9( litmax, littol, 0, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_vp_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0, Psic, psic0 );
%     [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, E2_plv, Cv, phiv ] = M2Di2_LocalIteration_VEP_comp_v9( litmax, littol, 0, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_vp_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0, Psiv, psiv0 );
    % Momentum residual
    Txxc     = 2*etac.*Exxc + etac./etaec.*Txxc0;
    Tyyc     = 2*etac.*Eyyc + etac./etaec.*Tyyc0;
    Txyv     = 2*etav.*Exyv + etav./etaev.*Txyv0;
    
    Ptc1 = Ptc + dd*dt*Kc.*divpc;
    % Momentum residual
%     Res_x      = diff(-Ptc1 + Txxc,1,1)/dx + diff(Txyv(2:end-1,:),1,2)/dy;
%     Res_y      = diff(-Ptc1 + Tyyc,1,2)/dy + diff(Txyv(:,2:end-1),1,1)/dx + rhogy(:,2:end-1);

    Res_x      = [zeros(1,ny); diff(-Ptc1 + Txxc,1,1)/dx + diff(Txyv(2:end-1,:),1,2)/dy; zeros(1,ny)];


    if free_surf == 0
        Res_y      = [zeros(nx,1), diff(-Ptc1 + Tyyc,1,2)/dy + diff(Txyv(:,2:end-1),1,1)/dx, zeros(nx,1)] + rhogy;
    else
        syy          = -Ptc1 + Tyyc;
        syy_ex     = [syy, -2*BC.PtN-syy(:,end)]; % Pressure boundary condition
        Res_y      = [zeros(nx,1), diff(syy_ex,1,2)/dy + diff(Txyv(:,2:end),1,1)/dx] + rhogy;
        if symmetry==1, Res_y(:,end) =   Res_y(:,end)/2; end
    end
    RMe        = [Res_x(:) ; Res_y(:)];
    nRMe(ils)  = norm(RMe)/((nx+1)*ny + (ny+1)*nx);
    % Continuity residual
    
     Divc           =  divc - Ptc0./Kc./dt;
     if comp==0, fp = -divc                           ; resnlp = norm(fp(:))/length(fp); end
     %             if comp==1, fp = -((Divc - divpc)   + (Ptc)./Kc./dt); resnlp = norm(fp(:))/length(fp); end  % Pressure non-linear residuals
     
     if comp==1, fp = -((Divc - 0*divpc)   + (Ptc1)./Kc./dt); resnlp = norm(fp(:))/length(fp); end  % Pres
     if comp==1, fp = 0*divpc - Divc - (Ptc)./Kc./dt;  end
%     RCTe       = -(diff(Vx,1,1)/dx+diff(Vy,1,2)/dy);
    nRCTe(ils) = norm(fp(:))/(nx*ny);
    
    if ils == 1
        fprintf(' LS: it. = %02d\n', ils );
        fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ils), nRMe (ils)/resnlu0 );
        fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ils), nRCTe(ils)/resnlp0 );
    end
end
% Find optimal step (i.e. yielding to lowest residuals)
[~,ibestM] = min(nRMe);
if ibestM==1, LSsuccess=0; alpha=0; fprintf('No descent found - continuing ...')
    nRMe(1) = 2*max(nRMe); [~,ibestM] = min(nRMe);
    LSsuccess=1; alpha = alphav(ibestM);
else          LSsuccess=1; alpha = alphav(ibestM); end
if noisy>=1
    fprintf(' LS: Selected alpha = %2.2f\n', alpha );
    fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ibestM), nRMe (ibestM)/resnlu0 );
    fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ibestM), nRCTe(ibestM)/resnlp0 );
end
end

function [Coh] = CohesionSofteningParams(Csoft,Cini,Cend,CdE, CEin, ph)
Coh.soft_on = Csoft(ph);
Coh.Delta   = Cini(ph) - Cend(ph);
Coh.Ein     = CEin(ph);
Coh.dE      = CdE(ph);
Coh.ini     = Cini(ph);
end
