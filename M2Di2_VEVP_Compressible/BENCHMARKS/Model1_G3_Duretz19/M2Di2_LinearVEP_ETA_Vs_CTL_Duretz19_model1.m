% =========================================================================
% Compressible Visco-Elasto-ViscoPlastic Consistent Tangent
% and Effective Viscosity approaches --- Power law & Drucker-Prager

% Copyright (C) 2020  Thibault Duretz

% This file is part of M2Di.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================

function M2Di2_LinearVEP_ETA_Vs_CTL_Duretz19_model1
% ACHTUNG: set correct paths below
addpath('../../_M2Di2_functions/')
M2Di_EP = load('DataM2Di_EVP_model1');
clc
%% Switches
SuiteSparse = 0;                                                             % 0=NO SuiteSparse  1=SuiteSparse
noisy       = 3;                                                             % 0=NO Output       1=Std output     2=Very noisy  3=Very noisy + kspgcr output
Newton      = 2;                                                             % 0=Picard          1=Newton
mat_pert    = 1;                                                             % Thermal or material perturbation
eta_kel_0   = 2.5e2;
n_vp        = 1.0;
comp        = 1;
restart     = 0;
SaveBreakpoint = 0;
symmetry    = 1;
ps          = 1/3;
saveRunData = 0;
%% Physics
xmin        = 0;                                                           % min x
xmax        = 1;                                                           % max x
ymin        =-0.75+.065;                                                     % min y
ymax        = 0e1;                                                           % max y
Rg          = 1;                                                             % Gas constant
r           = 5e-2;                                                          % Inclusion radius
Ebg         = 1;                                                             % Background strain rate
Tbg         = 1;                                                             % Background temperature
% Material 1 - MATRIX
ndis(1)     = 1.0;                                                           % Power Law exponent
Qdis(1)     = 0;                                                             % Activation energy
Adis(1)     = 1/(2e50);                                                      % Pre-exponent (Power law model)
G(1)        = 1;
C(1)        = 1.75e-4;
K(1)        = 2;
phi(1)      = 30*pi/180;
psi(1)      = 10*pi/180;
Csoft(1)    = 0;
Cini(1)     = C(1);
Cend(1)     = C(1)/2;
Cfact(1)    = 1;
Cepsi(1)    = 0.1;
phisoft(1)  = 0;
phiini(1)   = phi(1);
phiend(1)   = phi(1)/2;
phifact(1)  = 1;
phiepsi(1)  = 0.1;
% Material 2 - INCLUSION
ndis(2)     = 1.0;
Qdis(2)     = 0;
Adis(2)     = 1/(2e50);
G(2)        = 1/4;
C(2)        = 1.75e-4;
K(2)        = 2;
phi(2)      = 30*pi/180;
psi(2)      = 10*pi/180;
Csoft(2)    = 0;
Cini(2)     = C(2);
Cend(2)     = C(2);
Cfact(2)    = 50;
Cepsi(2)    = 0.1;
phisoft(2)  = 0;
phiini(2)   = phi(2);
phiend(2)   = phi(2)/2;
phifact(2)  = 1;
phiepsi(2)  = 0.1;
%% Numerics
nx          = 101;                                                            % Number of cells in x
ny          = ceil((ymax-ymin)*nx/(xmax-xmin));                               % Number of cells in y
nx          = nx-1;
ny          = ny-1;
nt          = 40;
dt          = 1e4;
Ebg         = 5e-6/dt;
% Non-linear solver settings
nmax_glob   = 50;                                                          % Max. number of non-linear iterations
tol_glob    = 5e-12;                                                         % Global nonlinear tolerance
eps_kspgcr  = 1e-4;                                                          % KSPGCR solver tolerance
LineSearch  = 0;                                                             % Line search algorithm
alpha       = 1.0;                                                           % Default step (in case LineSearch = 0)
% Linear solver settings
lsolver     = 0;                                                             % 0=Backslash 1=Powell-Hestenes
gamma       = 1e-5;                                                          % Numerical compressibility
nPH         = 30;                                                            % Max. number of linear iterations
tol_linu    = tol_glob/5000;                                                 % Velocity tolerance
tol_linp    = tol_glob/1000;                                                 % Divergence tolerance
% Local iterations setting
lit    = 1;
litmax = 100;
littol = 1e-13;
%% Dimensionalisation
h     = 1; % Material phase used for scaling
% Characterictic units
muc   = G(h)*dt;
Tc    = 100;                                                                 % Kelvins
Lc    = (xmax-xmin);                                                         % Meters
tc    = 1/Ebg;
% derived units
tauc  = muc*(1/tc);                                                          % Pascals
mc    = tauc*Lc*tc^2;                                                        % Kilograms
Jc    = mc*Lc^2/tc^2;                                                        % Joules
Vc    = Lc/tc;
% Scaling
Adis  = Adis./(tauc.^-ndis.*1./tc);
Qdis  = Qdis./Jc;
Rg    = Rg/(Jc/Tc);
Ebg   = Ebg/(1/tc);
Tbg   = Tbg/Tc;
r     = r/Lc;
C     = C./tauc;
G     = G./tauc;
K     = K./tauc;
Cini  = Cini./tauc;
Cend  = Cend./tauc;
xmin  = xmin/Lc;  xmax = xmax/Lc;
ymin  = ymin/Lc;  ymax = ymax/Lc;
dt    = dt/tc;
eta_kel_0 = eta_kel_0/muc;
%% Preprocessing
tic
Lx = xmax-xmin;     dx = Lx/nx;                                              % Grid spacing x
Ly = ymax-ymin;     dy = Ly/ny;                                              % Grid spacing y
xv = xmin:dx:xmax;  xc = xmin+dx/2:dx:xmax-dx/2;                             % Nodal coordinates x
yv = ymin:dy:ymax;  yc = ymin+dy/2:dy:ymax-dy/2;                             % Nodal coordinates y
[xv2,  yv2] = ndgrid(xv,yv);
[xc2,  yc2] = ndgrid(xc,yc);
[xvx2,yvx2] = ndgrid(xv,yc);
[xvy2,yvy2] = ndgrid(xc,yv);
rxvec  = zeros(nmax_glob,1);     rpvec  = zeros(nmax_glob,1); cpu = zeros(9,1);
nitervec  = zeros(nt,1);         timev  = zeros(nt,1);
rxvec_rel = zeros(nmax_glob,1);  rxvec_abs = zeros(nmax_glob,1);
rvec_abs  = zeros(nt,nmax_glob); rvec_rel  = zeros(nt,nmax_glob);
time      = 0;
%% Initial arrays
Vx     = -Ebg.*xvx2;                                                         % Initial solutions for velocity in x
Vy     =  Ebg.*yvy2;                                                         % Initial solutions for velocity in y
Ptc    =    0.* xc2 + eps;                                                         % Initial solutions for pressure
phc    =     ones(nx  ,ny  );                                                % Material phase on centroids
phv    =     ones(nx+1,ny+1);                                                % Material phase on vertices
Tec    = Tbg*ones(nx  ,ny  );                                                % Temperature on centroids
E2_plc =    zeros(nx  ,ny  );
E2_plv =    zeros(nx+1,ny+1);
Eiiacc =    zeros(nx  ,ny  );
stress = zeros(nt,1);
if mat_pert==0, Tec((xc2+0*Lx/2).^2+(yc2-0*Ly/2).^2<r^2) = Tbg + 0.1*Tbg;    % Thermal perturbation
else            phc((xc2+0*Lx/2).^2+(yc2+Ly/2).^2<r^2) = 2;                % Material perturbation
    phv((xv2+0*Lx/2).^2+(yv2+Ly/2).^2<r^2) = 2;                % Material perturbation
end
Av   =   Adis(1)*ones(nx+1,ny+1);
Ac   =   Adis(1)*ones(nx ,ny );
%--------------------------------------------------------------------------
% Set inclusion (shear modulus)
Gv   =   G(1)*ones(nx+1,ny+1);
Gv((xv2+0*Lx/2).^2+(yv2-ymin).^2<r^2) = G(2);
Gc = 0.25*(Gv(2:end,2:end) + Gv(1:end-1,2:end) + Gv(2:end,1:end-1) + Gv(1:end-1,1:end-1));
Kdiff  = 1e-6  /(Lc^2/tc);
dt_diff   = dx^2/Kdiff/4.1/5;
diff_time = 15 / tc;
nt_diff   = ceil(diff_time/dt_diff);
dt_diff   = diff_time/nt_diff;
for i=1:nt_diff
    Gc_x = [Gc(1,:); Gc; Gc(end,:)];
    Gc_y = [Gc(:,1), Gc, Gc(:,end)];
    Gc   =  Gc + Kdiff*dt_diff/dx^2 * diff(Gc_x,2,1) + Kdiff*dt_diff/dy^2 * diff(Gc_y,2,2);
end
fprintf('diffusion of jump over a duration of %2.2f and using %02d steps\n', nt_diff*dt_diff, nt_diff);
[ Gv ] = M2Di2_centroids2vertices( Gc );
%--------------------------------------------------------------------------
etaec  = Gc.*dt.*ones(nx  ,ny  );
etaev  = Gv.*dt.*ones(nx+1,ny+1);
Cc     = C(phc);
Cv     = C(phv);
phic   = phi(phc);
phiv   = phi(phv);
psic   = psi(phc);
psiv   = psi(phv);
Kc     = K(phc);
Kv     = K(phv);
% Prepare softening
[Cohc] = CohesionSofteningParams(Csoft,Cini,Cend,Cepsi,Cfact,phc);
[Cohv] = CohesionSofteningParams(Csoft,Cini,Cend,Cepsi,Cfact,phv);
[Phic] = CohesionSofteningParams(phisoft,phiini,phiend,phiepsi,phifact,phc);
[Phiv] = CohesionSofteningParams(phisoft,phiini,phiend,phiepsi,phifact,phv);
%% Numbering Ptc and Vx,Vy
NumVx  = reshape(1:(nx+1)*ny,nx+1,ny  );
NumVy  = reshape(1:nx*(ny+1),nx  ,ny+1); NumVyG = NumVy + max(NumVx(:));
NumPt  = reshape(1:nx*ny    ,nx  ,ny  ); NumPtG = NumPt + max(NumVyG(:));
cpu(1)=toc;
%% Define BC's ---------------- NEW STUFF
% x momentum
BC.nsxS    =  zeros(size(Vx)); %BC.nsxS( :         , 1  ) = 1;
BC.nsxN    =  zeros(size(Vx)); %BC.nsxN( :         , end) = 1;
BC.nsxW    =  zeros(size(Vx)); %BC.nsxW([1     2  ], :  ) = 1;
BC.nsxE    =  zeros(size(Vx)); %BC.nsxE([end-1 end], :  ) = 1;
BC.fsxS    =  zeros(size(Vx)); BC.fsxS( :         , 1  ) = 1;  % Free slip S
BC.fsxN    =  zeros(size(Vx)); BC.fsxN( :         , end) = 1;  % Free slip N
BC.fsxW    =  zeros(size(Vx)); BC.fsxW([1     2  ], :  ) = 1;  % Free slip W
BC.fsxE    =  zeros(size(Vx)); BC.fsxE([end-1 end], :  ) = 1;  % Free slip E
BC.Dirx    =  zeros(size(Vx)); BC.Dirx([1     end], :  ) = 1;
BC.NeuW    =  zeros(size(Vx));
BC.NeuE    =  zeros(size(Vx));
% y momentum
BC.nsyW    =  zeros(size(Vy)); %BC.nsyW( 1 , :         ) = 1;
BC.nsyE    =  zeros(size(Vy)); %BC.nsyE(end, :         ) = 1;
BC.nsyS    =  zeros(size(Vy)); %BC.nsyS( : ,[1     2]  ) = 1;
BC.nsyN    =  zeros(size(Vy)); %BC.nsyN( : ,[end-1 end]) = 1;
BC.fsyW    =  zeros(size(Vy)); BC.fsyW( 1 , :         ) = 1;  % Free slip W
BC.fsyE    =  zeros(size(Vy)); BC.fsyE(end, :         ) = 1;  % Free slip E
BC.fsyS    =  zeros(size(Vy)); BC.fsyS( : ,[1     2  ]) = 1;  % Free slip S
BC.fsyN    =  zeros(size(Vy)); BC.fsyN( : ,[end-1 end]) = 1;  % Free slip N
BC.Diry    =  zeros(size(Vy)); BC.Diry( : ,[1     end]) = 1;
BC.NeuS    =  zeros(size(Vy));
BC.NeuN    =  zeros(size(Vy));
% % Free surface
BC.frxN    =  zeros(size(Vx)); %BC.frxN(2:end-1,end) = 1;
BC.fryN    =  zeros(size(Vy)); %BC.fryN( :     ,end) = 1;
%% Boundary values - Dirichlets
Vx_W   = -Ebg.*xvx2(1  ,:)'.*ones(1 ,ny  )';                                 % BC value Vx West
Vx_E   = -Ebg.*xvx2(end,:)'.*ones(1 ,ny  )';                                 % BC value Vx East
Vy_S   =  Ebg.*yvy2(:,  1) .*ones(nx,1   );                                  % BC value Vy South
Vy_N   =  Ebg.*yvy2(:,end) .*ones(nx,1   );                                  % BC value Vy North
Vx_S = zeros(nx+1,1);  Vx_N = zeros(nx+1,1);
Vy_W = zeros(1,ny+1)'; Vy_E = zeros(1,ny+1)';
%% Construct BC structure ---------------- NEW STUFF
BC.Ux_W = Vx_W; BC.Ux_E = Vx_E; BC.Ux_S = Vx_S; BC.Ux_N = Vx_N;
BC.Uy_S = Vy_S; BC.Uy_N = Vy_N; BC.Uy_W = Vy_W; BC.Uy_E = Vy_E;
cpu(2)=toc;
%% Assemble pressure gradient and divergence operator
tic
[ div ] = M2Di2_AssembleDivergence_v3( BC, ones(size(Ptc)), NumVx, NumVyG, NumPt, dx, dy, nx, ny, symmetry, SuiteSparse );
grad = -div';
%% Assemble Block PP
iPt   = NumPt;  I = iPt(:)';  J = I;                                     % Eq. index center (pressure diagonal)
if comp==0, V     =        gamma.*ones(size(Ptc(:))); end                    % Center coeff.
if comp==1, V     = 1./Kc(:)./dt.*ones(size(Ptc(:))); end                    % Center coeff.
if SuiteSparse==1, PP = sparse2(I,J,V);                                  % Matrix assembly
else               PP = sparse (I,J,V); end
PPI   = spdiags(1./diag(PP),0,PP);                                       % Trivial inverse of diagonal PP matrix
cpu(3)=toc;
it0 = 1;
Txxc   =    zeros(nx  ,ny  );
Tyyc   =    zeros(nx  ,ny  );
Tzzc   =    zeros(nx  ,ny  );
Txyv   =    zeros(nx+1,ny+1);
Txxv   =    zeros(nx+1,ny+1);
Tyyv   =    zeros(nx+1,ny+1);
Tzzv   =    zeros(nx+1,ny+1);
Txyc   =    zeros(nx  ,ny  );
Ptv    =    zeros(nx+1,ny+1);
Ebg_acc     = 0;
if restart>0,
    load('Breakpoint');
    it0   = it+1;
end
%% Time loop
for it=it0:nt
    plastic = 0;
    fprintf('\n------------- Time step #%03d -------------\n',it);% Initial solutions for pressure
    %% Fields from previous step
    Txxc0  = Txxc;    Txxv0   = Txxv;
    Tyyc0  = Tyyc;    Tyyv0   = Tyyv;
    Tzzc0  = Tzzc;    Tzzv0   = Tzzv;
    Txyv0  = Txyv;    Txyc0   = Txyc;
    Ptc0   = Ptc;     Ptv0    = Ptv;
    E2_plc0 = E2_plc; E2_plv0 = E2_plv;
    Cc0     = Cc;     Cv0     = Cv;
    phic0   = phic;   phiv0   = phiv;
    %% Non linear iterations
    iter=0; resnlu=2*tol_glob; resnlu0=2*tol_glob;
    du = [0*Vx(:); 0*Vy(:)];         dp = zeros(max(NumPt(:)),1);
    rxvec_rel = zeros(nmax_glob,1);    rxvec_abs = zeros(nmax_glob,1);
    rpvec_rel = zeros(nmax_glob,1);    rpvec_abs = zeros(nmax_glob,1);
    % Constant 2D field (temperature perturbation) - Interpolate from centers to nodes and extrapolates boundaries
    Tei = 0.25*(Tec(1:end-1,1:end-1) + Tec(2:end,1:end-1) + Tec(1:end-1,2:end) + Tec(2:end,2:end));
    Tev = zeros(nx+1,ny+1);  Tev(2:end-1,2:end-1)=Tei; Tev([1,end],:)=Tev([2,end-1],:); Tev(:,[1,end])=Tev(:,[2,end-1]);
    for iter=1:nmax_glob
        %iter = iter+1;
        if noisy>=1, fprintf('\n *** Nonlin iter %d *** \n', iter ); end
        tic
        % Initial guess or iterative solution for strain increments
        divc        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
        Exxc        = diff(Vx,1,1)/dx - ps*divc;
        Eyyc        = diff(Vy,1,2)/dy - ps*divc;
        Ezzc        =                 - ps*divc;
        Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
        Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
        dVxdy       = diff(Vx_exp,1,2)/dy;
        dVydx       = diff(Vy_exp,1,1)/dx;
        Exyv        = 0.5*( dVxdy + dVydx );
        % Extrapolate trial strain components
        Exyc      = 0.25*(Exyv (1:end-1,1:end-1) + Exyv (2:end,1:end-1) + Exyv (1:end-1,2:end) + Exyv (2:end,2:end));
        [ Exxv  ] = M2Di2_centroids2vertices( Exxc );
        [ Eyyv  ] = M2Di2_centroids2vertices( Eyyc );
        [ Ezzv  ] = M2Di2_centroids2vertices( Ezzc );
        [  divv ] = M2Di2_centroids2vertices(  divc);
        [  Ptv  ] = M2Di2_centroids2vertices(  Ptc );
        % Engineering convention
        Gxyc = 2*Exyc; Gxyv = 2*Exyv;
        % Viscosity
        mc   = 1/2*( 1./ndis(phc) - 1);
        mv   = 1/2*( 1./ndis(phv) - 1);
        Bc   = Ac.^(-1./ndis(phc)) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
        Bv   = Av.^(-1./ndis(phv)) .* exp(Qdis(phv)/Rg./Tev./ndis(phv));
        [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,~,~,~,~,CTL_c, dE2_plc, dCc, dphic ] = M2Di2_LocalIteration_VEP_comp_v6( litmax, littol, noisy, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_kel_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0 );
        [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, dE2_plv, dCv, dphiv ] = M2Di2_LocalIteration_VEP_comp_v6( litmax, littol, noisy, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_kel_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0 );
        if (sum(plc(:))>0 || sum(plv(:))>0), fprintf('ACHTUNG: On the yield!\n'); plastic=1; end
        if Newton > 0
            % Derivatives of corrected VEP viscosity
            detadexxc =     detac_vep.dExx;   ddivpdexxc =     ddivpc.dExx;
            detadeyyc =     detac_vep.dEyy;   ddivpdeyyc =     ddivpc.dEyy;
            detadgxyc = 1/2*detac_vep.dExy;   ddivpdgxyc = 1/2*ddivpc.dExy;
            detadpc   =     detac_vep.dP;     ddivpdpc   =     ddivpc.dP;
            detadezzc =     detac_vep.dEzz;   ddivpdezzc =     ddivpc.dEzz;
            % Derivatives of corrected VEP viscosity
            detadexxv =     detav_vep.dExx;
            detadeyyv =     detav_vep.dEyy;
            detadgxyv = 1/2*detav_vep.dExy;
            detadpv   =       detav_vep.dP;
            detadezzv =     detav_vep.dEzz;
        end
        %% Rheological coefficients for the Picard operator
        D.D11c = 2*etac; D.D12c = 0*etac; D.D13c = 0*etac; D.D14c = 0*etac;    D.D15c = 0*etac;
        D.D21c = 0*etac; D.D22c = 2*etac; D.D23c = 0*etac; D.D24c = 0*etac;    D.D25c = 0*etac;
        D.D31v = 0*etav; D.D32v = 0*etav; D.D33v = 1*etav; D.D34v = 0*etav;    D.D35v = 0*etav;
        D.D41c = 0*etac; D.D42c = 0*etac; D.D43c = 0*etac; D.D44c = 1;         D.D45c = 0*etac;
        %% Assemble Picard operator
        [ K ] = M2Di2_GeneralAnisotropicVVAssembly_v3( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, symmetry, Vx, Vy, ps, SuiteSparse );
        %% Rheological coefficients for the Jacobian
        if Newton > 0
            Exxc1 = Exxc + Txxc0./2./etaec;
            Eyyc1 = Eyyc + Tyyc0./2./etaec;
            Gxyv1 = Gxyv + Txyv0./1./etaev;
            phic1 = phic0 + dphic;
            phiv1 = phiv0 + dphiv;
            % Newton: Derivative of VEP rheological operator
            dd = 1; bb = 0;
            D.D11c = 2*etac +   2*detadexxc.*Exxc1-dd*Kc.*dt.*ddivpdexxc; D.D12c =            2*detadeyyc.*Exxc1-dd*Kc.*dt.*ddivpdeyyc; D.D13c =            2*detadgxyc.*Exxc1-dd*Kc.*dt.*ddivpdgxyc; D.D14c = 2*detadpc.*Exxc1-dd*Kc.*dt.*ddivpdpc;     D.D15c =  2*detadezzc.*Exxc1-dd*Kc.*dt.*ddivpdezzc;
            D.D21c =            2*detadexxc.*Eyyc1-dd*Kc.*dt.*ddivpdexxc; D.D22c = 2*etac +   2*detadeyyc.*Eyyc1-dd*Kc.*dt.*ddivpdeyyc; D.D23c =            2*detadgxyc.*Eyyc1-dd*Kc.*dt.*ddivpdgxyc; D.D24c = 2*detadpc.*Eyyc1-dd*Kc.*dt.*ddivpdpc;     D.D25c =  2*detadezzc.*Eyyc1-dd*Kc.*dt.*ddivpdezzc;
            D.D31v =            1*detadexxv.*Gxyv1;                       D.D32v =            1*detadeyyv.*Gxyv1;                       D.D33v = 1*etav +   1*detadgxyv.*Gxyv1;                       D.D34v = 1*detadpv.*Gxyv1;                         D.D35v =  1*detadezzv.*Gxyv1;
            D.D41c =           -1*bb*Kc.*dt.*ddivpdexxc;                  D.D42c =            -1*bb*Kc.*dt.*ddivpdeyyc;                 D.D43c =            -1*bb*Kc.*dt.*ddivpdgxyc;                 D.D44c = 1 - 1*bb*Kc.*dt.*ddivpdpc;                D.D45c =  -1*bb*Kc.*dt.*ddivpdezzc;
        end
        %% Assemble Jacobian
        [ J ] = M2Di2_GeneralAnisotropicVVAssembly_v3( D, BC, NumVx, NumVy, NumVyG, nx, ny, dx, dy, symmetry, Vx, Vy, ps, SuiteSparse );
        %% Assemble Jacobian V2P;
        V2PJ       = M2Di2_M2TAssembly( BC, D, dx, dy, nx, ny, 0, 0, NumVx, NumVyG, NumPt, Vx, Vy, SuiteSparse );
        V2PJ       = V2PJ + grad;
        %% Jacobian of pressure block: PPJ operator
        if comp==1,
            cPC    = (D.D44c) ./ (Kc*dt);
            D.D41c = D.D41c;
            D.D42c = D.D42c;
            D.D43c = D.D43c;
            D.D45c = D.D45c;
            Ipj = NumPt(:);
            Jpj = NumPt(:);
            Vpj = cPC(:);
            PPJ = sparse2( Ipj, Jpj, Vpj );
            %% Jacobian of thermo-mechanical coupling: T2MJ operator
            P2VJ = M2Di2_P2MAssembly( BC, D, dx, dy, nx, ny, etac, Kc, dt, NumVx, NumVyG, NumPt, Ptc, SuiteSparse );
            P2VJ = P2VJ + div;
        end
        %% Residual in matrix-free form
        Res_x      = [zeros(1,ny); diff(-Ptc1 + Txxc,1,1)/dx + diff(Txyv(2:end-1,:),1,2)/dy; zeros(1,ny)];
        Res_y      = [zeros(nx,1), diff(-Ptc1 + Tyyc,1,2)/dy + diff(Txyv(:,2:end-1),1,1)/dx, zeros(nx,1)];
        % Evaluate non-Linear residual using matrix-vector products
        fu = [Res_x(:); Res_y(:)];      resnlu = norm(fu)/length(fu);
        u  = [Vx(:); Vy(:)]; p = Ptc(:);
        Divc           =  divc - Ptc0./Kc./dt;
        if comp==0, fp = -divc                           ; resnlp = norm(fp(:))/length(fp); end
        if comp==1, fp = -((Divc - divpc)   + (Ptc)./Kc./dt); resnlp = norm(fp(:))/length(fp); end  % Pressure non-linear residuals
        if comp==1, fp = 0*divpc - Divc - (Ptc)./Kc./dt;  resnlp = norm(fp(:))/length(fp); end
        fp = fp(:);
        if iter==1, resnlu0=resnlu; resnlp0=resnlp; end
        rxvec_abs(iter) = resnlu;
        rxvec_rel(iter) = resnlu/resnlu0;
        rpvec_abs(iter) = resnlp;
        rpvec_rel(iter) = resnlp/resnlp0;
        if noisy>=1,fprintf('Chk: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', resnlu, resnlu/resnlu0 );
            fprintf('Chk: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', resnlp, resnlp/resnlp0 ); rxvec(iter) = resnlu/resnlu0; rpvec(iter) = resnlp/resnlp0; end
        if (resnlu < tol_glob && resnlp < tol_glob ), break; end
        cpu(4)=cpu(4)+toc;
        %% Linear solver - obtain velocity and pressure corrections for current non linear iteration
        if lsolver==0  % Backslash coupled solver ------------------------------
            tic
            if comp == 0
                P2VJ      =  div;
                PPJ       = 0*PP;
                PPJ(1,1)  = 1;
                P2VJ(1,:) = 0;
                fp(1) = 0;
            end
            Ms  = [ J   , V2PJ ; ...
                P2VJ , PPJ ];                                                 % Assemble entire Jacobian
            f   = [ fu  ; fp   ];                                                % Assemble entire residual vector
            cpu(5)=cpu(5)+toc;
            tic
            dX  = Ms\f;                                                          % Call direct solver
            du  = dX(1:max(NumVyG(:)));                                          % Extract velocity correction
            dp  = dX(NumPtG(:));                                                 % Extract pressure correction
            f1  = Ms*dX - f;                                                     % Compute entire linear residual
            fu1 = f1(1:max(NumVyG(:)));                                          % Extract velocity linear residual
            fp1 = f1(NumPtG(:));                                                 % Extract pressure linear residual
            cpu(6)=cpu(6)+toc;
        elseif lsolver==1 % Powell-Hestenes INCOMPRESSIBLE - Decoupled/segregated solve --------
            if Newton==0, J = K; end
            tic
            Kt  = K - grad*(PPI*div);                                            % Velocity Schur complement of Picard operator (Kt)
            Jt  = J - grad*(PPI*div);                                            % Velocity Schur complement of Jacobian operator
            [Kf,e,s] = chol(Kt,'lower','vector');                                % Choleski factorization of Kt
            cpu(5)=cpu(5)+toc;
            tic
            % Powell-Hestenes iterations
            fu0 = fu;                                                                    % Save linear norm 0
            for itPH=1:nPH
                fut  = fu - grad*dp - grad*PPI*fp;                                       % Iterative right hand side
                [du,norm_r,its] = M2Di2_kspgcr_m(Jt,fut,du,Kf,s,eps_kspgcr,noisy,SuiteSparse); % Apply inverse of Schur complement
                dp   = dp + PPI*(fp -  div*du);                                          % Pressure corrctions
                fu1  = fu -   J*du  - grad*dp;                                           % Compute linear velocity residual
                fp1  = fp - div*du  - PP*dp;                                                      % Compute linear pressure residual
                if noisy>1, fprintf('--- iteration %d --- \n',itPH);
                    fprintf('  Res. |u| = %2.2e \n',norm(fu1)/length(fu1));
                    fprintf('  Res. |p| = %2.2e \n',norm(fp1)/length(fp1));
                    fprintf('  KSP GCR: its=%1.4d, Resid=%1.4e \n',its,norm_r); end
                if ((norm(fu1)/length(fu1)) < tol_linu) && ((norm(fp1)/length(fp1)) < tol_linp), break; end
                if ((norm(fu1)/length(fu1)) > (norm(fu0)/length(fu1)) && norm(fu1)/length(fu1) < tol_glob), fprintf(' > Linear residuals do no converge further:\n'); break; end
                fu0 = fu1;
            end
            cpu(6)=cpu(6)+toc;
        end
        tic
        if noisy>=1, fprintf(' - Linear res. ||res.u||=%2.2e\n', norm(fu1)/length(fu1) );
            fprintf(' - Linear res. ||res.p||=%2.2e\n', norm(fp1)/length(fp1) ); end
        % Call line search - globalization procedure
        if LineSearch==1, [alpha,~] = LineSearch_Direct(dd,ps,BC,nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,phic,phiv,psic,psiv,Cc,Cv,Newton,0,1/3,noisy,0,resnlu0,resnlp0,resnlu,resnlp,etaec,etaev,Kc,Kv,Ptc0,Ptv0,Txxc0,Tyyc0,Tzzc0,Txyc0,Txxv0,Tyyv0,Tzzv0,Txyv0,lit,litmax,littol, eta_kel_0, n_vp, dt, E2_plc0, E2_plv0, Cohc, Cohv, Cc0, Cv0, Phic, Phiv, phic0, phiv0); end
        u    = u + alpha*du;                                                     % Velocity update from non-linear iterations
        p    = p + alpha*dp;                                                     % Pressure update from non-linear iterations
        Vx   = reshape(u(NumVx(:)) ,[nx+1,ny  ]);                                % Velocity in x (2D array)
        Vy   = reshape(u(NumVyG(:)),[nx  ,ny+1]);                                % Velocity in y (2D array)
        Ptc  = reshape(p(NumPt(:)) ,[nx  ,ny  ]);                                % Pressure      (2D array)
        cpu(7)=cpu(7)+toc;
    end
    if lit == 0
        Txxc      = 2*etac.*Exxc + etac./etaec.*Txxc0;
        Tyyc      = 2*etac.*Eyyc + etac./etaec.*Tyyc0;
        Txyv      = 2*etav.*Exyv + etav./etaev.*Txyv0;
        Txyc      = 0.25*(Txyv (1:end-1,1:end-1) + Txyv (2:end,1:end-1) + Txyv (1:end-1,2:end) + Txyv (2:end,2:end));
    end
    % Updates
    Ptc            = Ptc1;
    Cc             = Cc0 + dCc;
    Cv             = Cv0 + dCv;
    phic           = phic0 + dphic;
    phiv           = phiv0 + dphiv;
    E2_plc         = E2_plc0 + dE2_plc;
    E2_plv         = E2_plv0 + dE2_plv;
    Tiic           = sqrt(1/2*Txxc.^2 + 1/2*Tyyc.^2 + 1/2*Tzzc.^2 + Txyc.^2);
    Eiic2          = 1/2*(Exxc.^2 + Eyyc.^2 + Ezzc.^2) + Exyc.^2;
    Eiiacc         = Eiiacc + dt*sqrt(Eiic2);
    timev(it)      = time;
    stress(it)     = mean(Tiic(:));
    stressmin(it)  = min(Tiic(:));
    stressmax(it)  = max(Tiic(:));
    press(it)      = mean(Ptc(:));
    pressmin(it)   = min(Ptc(:));
    pressmax(it)   = max(Ptc(:));
    nitervec(it)   = iter;
    tic
    Ebg_acc = Ebg_acc + dt*Ebg;
    time    = time + dt;
    if plastic == 1 %it == nt
        %% Post-processing
        % Residuals
        figure(1),clf,colormap('jet'),set(gcf,'Color','white')
        plot(1:iter, log10(rxvec(1:iter)/rxvec(1)),'rx-', 1:iter, log10(rpvec(1:iter)/rpvec(1)),'bo-')
        xlabel('Iterations'),ylabel('Residuals'),drawnow
        % Pressure, Strain
        figure(2), clf
        subplot(211),
        imagesc(xc*Lc, yc*Lc, log10(abs(Eiiacc'))), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. Eii = ', num2str(min(Eiiacc(:))), ' max. Eii = ', num2str(max(Eiiacc(:)))])
        subplot(212),
        imagesc(xc*Lc, yc*Lc, (Ptc'*tauc)), colorbar, axis image, set(gca, 'ydir', 'normal'), title(['min. P = ', num2str(min(Ptc(:)*tauc)), ' max. P = ', num2str(max(Ptc(:)*tauc))])
        if saveRunData==1, print(['Strain',num2str(it,'%04d')], '-r300','-dpng'); end
        if Csoft(1) == 1
            figure(3),clf,colormap('jet'),set(gcf,'Color','white')
            subplot(211)
            Cc_pred = Cohc.a2.*atan(-(E2_plc-Cohc.a0).*Cohc.a1) + Cohc.a3;
            imagesc(flipud(Cc_pred')*tauc),colorbar,axis image,title(['min C = ', num2str(min(Cc_pred(:)*tauc), '%2.4e')]), caxis([10 20])
            xlabel('x','FontSize',15),ylabel('y','FontSize',15)
            subplot(212)
            imagesc(flipud(Cc')*tauc),colorbar,axis image,title(['min C = ', num2str(min(Cc(:)*tauc), '%2.4e')]), caxis([10 20])
            xlabel('x','FontSize',15),ylabel('y','FontSize',15)
        end
        if phisoft(1) == 1
            figure(4),clf,colormap('jet'),set(gcf,'Color','white')
            subplot(211)
            phic_pred = Phic.a2.*atan(-(E2_plc-Phic.a0).*Phic.a1) + Phic.a3;
            imagesc(flipud(phic_pred')),colorbar,axis image,title(['min phi = ', num2str(min(phic_pred(:)), '%2.4e')]), %caxis([10 20])
            xlabel('x','FontSize',15),ylabel('y','FontSize',15)
            subplot(212)
            imagesc(flipud(phic')),colorbar,axis image,title(['min phi = ', num2str(min(phic(:)), '%2.4e')]), %caxis([10 20])
            xlabel('x','FontSize',15),ylabel('y','FontSize',15)
        end
        figure(20),clf,colormap('jet'),set(gcf,'Color','white')
        subplot(211), hold on
        plot((1:it)*dt, (stress(1:it)*tauc),'bx-')
        plot((1:it)*dt, (stressmax(1:it)*tauc),'rx-')
        plot((1:it)*dt, (stressmin(1:it)*tauc),'gx-')
        plot((1:it)*dt, M2Di_EP.Tiivec(1:it),'ob')
        plot((1:it)*dt, M2Di_EP.Tiimin(1:it),'og')
        plot((1:it)*dt, M2Di_EP.Tiimax(1:it),'or')
        xlabel('Time'),ylabel('Stress')
        title('Comparison with Duretz et al., 2019')
        subplot(212), hold on
        plot((1:it)*dt, (press(1:it)*tauc),'bx-')
        plot((1:it)*dt, (pressmax(1:it)*tauc),'rx-')
        plot((1:it)*dt, (pressmin(1:it)*tauc),'gx-')
        plot((1:it)*dt, M2Di_EP.Pvec(1:it),'bo')
        plot((1:it)*dt, M2Di_EP.Pminv(1:it),'go')
        plot((1:it)*dt, M2Di_EP.Pmaxv(1:it),'ro')
        xlabel('Time'),ylabel('Pressure')
        drawnow
    end
    % % save('VEP_ETA', 'timev', 'stress', 'rvec_rel', 'rvec_abs', 'nitervec')
    if SaveBreakpoint==1, save(['Breakpoint']); end
end
if saveRunData==1, save(['DataRun',num2str(it,'%04d')], 'Lc','tauc','tc','Ptc','Eiic2', 'Eiiacc','dt','stress','stressmax','stressmin','press','pressmax','pressmin','xc','yc'); end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%    FUNCTIONS USED IN MAIN CODE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [alpha, LSsuccess] = LineSearch_Direct(dd,ps,BC,nx,ny,dx,dy,u,p,du,dp,Tec,Tev,phc,phv,Rg,Adis,Qdis,ndis,phic,phiv,psic,psiv,Cc,Cv,Newton,comp,pls,noisy,INV,resnlu0,resnlp0,resnlu,resnlp,etaec,etaev,Kc,Kv,Ptc0,Ptv0,Txxc0,Tyyc0,Tzzc0,Txyc0,Txxv0,Tyyv0,Tzzv0,Txyv0,lit,litmax,littol, eta_kel_0, n_vp, dt, E2_plc0, E2_plv0, Cohc, Cohv, Cc0, Cv0, Phic, Phiv, phic0, phiv0 )
% Line search explicit algorithm
nsteps = 25;                                   % number of steps
amin   = 0.05;
if Newton>0, amax = 1.0; else amax = 2.0; end % maximum step
dalpha = (amax-amin)/(nsteps-1);
alphav = amin:dalpha:amax;
nVx    = (nx+1)*ny; nRMe   = zeros(nsteps,1); nRCTe  = zeros(nsteps,1);
u0 = u; p0 = p;
% Compute non linear residual for different steps
for ils=1:nsteps;
    % Primitive variables with correction step
    u       = u0 + alphav(ils).*du;
    p       = p0 + alphav(ils).*dp;
    % Evaluate non-Linear residual in matrix-free form
    Ptc     = reshape(p           ,[nx  ,ny  ]);
    Vx      = reshape(u(1:nVx)    ,[nx+1,ny  ]);
    Vy      = reshape(u(nVx+1:end),[nx  ,ny+1]);
    % Initial guess or iterative solution for strain increments
    divc        = diff(Vx,1,1)/dx + diff(Vy,1,2)/dy;
    Exxc        = diff(Vx,1,1)/dx - ps*divc;
    Eyyc        = diff(Vy,1,2)/dy - ps*divc;
    Ezzc        =                 - ps*divc;
    Vx_exp      = [2*BC.nsxS(:,1).*BC.Ux_S-BC.nsxS(:,1).*Vx(:,1) + BC.fsxS(:,1).*Vx(:,1), Vx, 2*BC.nsxN(:,end).*BC.Ux_N-BC.nsxN(:,end).*Vx(:,end) + BC.fsxN(:,end).*Vx(:,end)];
    Vy_exp      = [2*BC.nsyW(1,:).*BC.Uy_W'-BC.nsyW(1,:).*Vy(1,:) + BC.fsyW(1,:).*Vy(1,:); Vy; 2*BC.nsyE(end,:).*BC.Uy_E'-BC.nsyE(end,:).*Vy(end,:) + BC.fsyE(end,:).*Vy(end,:)];
    dVxdy       = diff(Vx_exp,1,2)/dy;
    dVydx       = diff(Vy_exp,1,1)/dx;
    Exyv        = 0.5*( dVxdy + dVydx );
    % Extrapolate trial strain components
    Exyc      = 0.25*(Exyv (1:end-1,1:end-1) + Exyv (2:end,1:end-1) + Exyv (1:end-1,2:end) + Exyv (2:end,2:end));
    [ Exxv  ] = M2Di2_centroids2vertices( Exxc );
    [ Eyyv  ] = M2Di2_centroids2vertices( Eyyc );
    [ Ezzv  ] = M2Di2_centroids2vertices( Ezzc );
    [  divv ] = M2Di2_centroids2vertices(  divc);
    [  Ptv  ] = M2Di2_centroids2vertices(  Ptc );
    % Engineering convention
    Gxyc = 2*Exyc; Gxyv = 2*Exyv;
    % Viscosity
    mc   = 1/2*( 1./ndis(phc) - 1);
    mv   = 1/2*( 1./ndis(phv) - 1);
    Bc   = Adis(phc).^(-1./ndis(phc)) .* exp(Qdis(phc)/Rg./Tec./ndis(phc));
    Bv   = Adis(phv).^(-1./ndis(phv)) .* exp(Qdis(phv)/Rg./Tev./ndis(phv));
    [ etac, Kvepc, Txxc, Tyyc, Tzzc, Txyc, Ptc1, divpc, divec, detac_ve, detac_vep, ddivpc, plc, eta_vp_c,~,~,~,~,CTL_c, dE2_plc, dCc, dphic ] = M2Di2_LocalIteration_VEP_comp_v6( litmax, littol, 0, divc, Exxc, Eyyc, Ezzc, Exyc, Ptc, Txxc0, Tyyc0, Tzzc0, Txyc0, Bc, mc, etaec, Kc, Ptc0, phic, psic, Cc, 0, eta_kel_0, n_vp, dt, E2_plc0, Cohc, Cc0, Phic, phic0 );
    [ etav, Kvepv, Txxv, Tyyv, Tzzv, Txyv, Ptv1, divpv, divev, detav_ve, detav_vep, ddivpv, plv, eta_vp_v,~,~,~,~,CTL_v, dE2_plv, dCv, dphiv ] = M2Di2_LocalIteration_VEP_comp_v6( litmax, littol, 0, divv, Exxv, Eyyv, Ezzv, Exyv, Ptv, Txxv0, Tyyv0, Tzzv0, Txyv0, Bv, mv, etaev, Kv, Ptv0, phiv, psiv, Cv, 0, eta_kel_0, n_vp, dt, E2_plv0, Cohv, Cv0, Phiv, phiv0 );
    % Momentum residual
    Txxc     = 2*etac.*Exxc + etac./etaec.*Txxc0;
    Tyyc     = 2*etac.*Eyyc + etac./etaec.*Tyyc0;
    Txyv     = 2*etav.*Exyv + etav./etaev.*Txyv0;
    Ptc1 = Ptc + dd*dt*Kc.*divpc;
    % Momentum residual
    Res_x      = diff(-Ptc1 + Txxc,1,1)/dx + diff(Txyv(2:end-1,:),1,2)/dy;
    Res_y      = diff(-Ptc1 + Tyyc,1,2)/dy + diff(Txyv(:,2:end-1),1,1)/dx;
    RMe        = [Res_x(:) ; Res_y(:)];
    nRMe(ils)  = norm(RMe)/((nx+1)*ny + (ny+1)*nx);
    % Continuity residual
    Divc           =  divc - Ptc0./Kc./dt;
    if comp==0, fp = -divc                           ; resnlp = norm(fp(:))/length(fp); end
    if comp==1, fp = -((Divc - 0*divpc)   + (Ptc1)./Kc./dt); resnlp = norm(fp(:))/length(fp); end  % Pres
    nRCTe(ils) = norm(fp(:))/(nx*ny);
    if ils == 1
        fprintf(' LS: it. = %02d\n', ils );
        fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ils), nRMe (ils)/resnlu0 );
        fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ils), nRCTe(ils)/resnlp0 );
    end
end
% Find optimal step (i.e. yielding to lowest residuals)
[~,ibestM] = min(nRMe);
if ibestM==1, LSsuccess=0; alpha=0; fprintf('No descent found - continuing ...')
    nRMe(1) = 2*max(nRMe); [~,ibestM] = min(nRMe);
    LSsuccess=1; alpha = alphav(ibestM);
else          LSsuccess=1; alpha = alphav(ibestM); end
if noisy>=1
    fprintf(' LS: Selected alpha = %2.2f\n', alpha );
    fprintf(' LS: NonLin res. ||res.u||=%2.4e, ||res.u||/||res.u0||=%2.4e\n', nRMe (ibestM), nRMe (ibestM)/resnlu0 );
    fprintf(' LS: NonLin res. ||res.p||=%2.4e, ||res.p||/||res.p0||=%2.4e\n', nRCTe(ibestM), nRCTe(ibestM)/resnlp0 );
end
end

function [Coh] = CohesionSofteningParams(Csoft,C_ini,C_end,Onset,Intensity,ph)
% Shift in x
a0      = Onset;
a1      = Intensity;
a2      = (C_ini-C_end) / pi;
Epl_f1  = a2.*atan(-(0-a0).*a1);
% Shift in y - start at correct initial value
a3          = (C_ini-Epl_f1(1));
Coh.a0      = a0(ph); Coh.a1 = a1(ph); Coh.a2 = a2(ph); Coh.a3 = a3(ph);
Coh.soft_on = Csoft(ph);
end