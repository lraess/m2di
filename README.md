# M2Di

## Concise and efficient MATLAB 2D multi-physics Stokes solver.

Copyright (C) 2019  Ludovic Raess, Thibault Duretz, Yury Podladchikov.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

### Current version of M2Di can be found at:

https://bitbucket.org/lraess/m2di/

http://www.unil.ch/geocomputing/software/

### M2Di suite (M2Di2, M2Dpt, M2Di_EP) was released in:

Räss et al. (2017)  https://doi.org/10.1002/2016GC006727.

Duretz et al. (2019)  https://doi.org/10.1093/gji/ggy434.

Räss et al. (2019)  https://doi.org/10.1093/gji/ggz239.

Duretz et al. (2018)  https://doi.org/10.1029/2018GC007877

Duretz et al. (2019)  https://doi.org/10.1029/2019GC008531

de Borst and Duretz (2020) https://doi.org/10.1002/nag.3046

Duretz et al. (2020)  https://doi.org/10.1029/2019GL086027

Duretz et al. (202X)  tbc

### Distributed software, directory content:

M2Di | concise and efficient MATLAB 2D Stokes solver for linear and power law viscous flow.

M2Di2 | concise and efficient MATLAB 2D Stokes solver for linear and power law viscous flow, thermo-mechanical and hydro-mechanical coupling.

M2Dpt | Pseudo-transient algorithm used in the publication and benchmarked against the M2Di2 routines.

M2Di_EP | M2Di with Elasto-Visco-Plastic rheology using a compressible displacement-based formulation consistent tangent linearisation.

M2Di_EP_KelvinViscoplastic | Numerical modeling of shear banding with Kelvin-type elasto-viscoplastic rheology. 

M2Di2_VEVP_CrustalShearBanding | Visco-Elasto-Viscoplastic crustal-scale shear banding

M2Di2_VEVP_CompressibleNonLinear | Visco-Elasto-Viscoplastic crustal-scale shear banding with compressible elasto-plasticty,  non-linear hardening/softening and power-law viscoplasticity

### QUICK START:

Open Matlab, select the routine and enjoy!

SuiteSparse: For optimal performance, download the SuiteSparse package at
http://www.suitesparse.com, run the Matlab install and set the
SuiteSparse switch to 1.

### Contact: ludovic.rass@gmail.com, thibault.duretz@univ-rennes1.fr
