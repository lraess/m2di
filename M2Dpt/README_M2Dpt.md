# M2Dpt

## 2-D pseudo-transient routines, part of the M2Di software distribution.

Copyright (C) 2019  Ludovic Raess, Thibault Duretz, Yury Podladchikov.

M2Di is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

M2Di is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with M2Di.  If not, see <http://www.gnu.org/licenses/>.

### Current version of M2Dpt can be found at:

https://bitbucket.org/lraess/m2di/

http://www.unil.ch/geocomputing/software/

### M2Dpt was released in:

Duretz, T., Räss, L., Podladchikov, Y. Y., and Schmalholz, S. M. (2019). 
Resolving thermomechanical coupling in two and three dimensions: spontaneous 
strain localisation owing to strain heating.
Geophysical Journal International, ggy434, https://doi.org/10.1093/gji/ggy434.

Räss, L., Duretz, T. and Podladchikov, Y. Y. (2019). 
Resolving hydromechanical coupling in two and three dimensions: spontaneous
channelling of porous fluids owing to decompaction weakening.
Geophysical Journal international, ggz239, https://doi.org/10.1093/gji/ggz239.

### Distributed software, directory content:

TM2Dpt_GJI.m | Pseudo-transient algorithm used in the publication and benchmarked against the TM2Di2_Newton_GJI routines.

HM2Dpt_GJI.m | Pseudo-transient algorithm used in the publication and benchmarked against the HM2Di2_Newton_GJI routines.

### Contact: ludovic.rass@gmail.com, thibault.duretz@univ-rennes1.fr
