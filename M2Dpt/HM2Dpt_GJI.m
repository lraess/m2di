% =========================================================================
% Nonlinear Hydro-mechanical Two-Phase flow solver 2D Pseudo-Transient.

% Copyright (C) 2019  Ludovic Raess, Thibault Duretz, Yury Podladchikov

% This file is part of M2Di. Please refer to:
% Räss, L., Duretz, T. and Podladchikov, Y. Y. (2019). 
% Resolving hydromechanical coupling in two and three dimensions: 
% spontaneous channelling of porous fluids owing to decompaction weakening.
% Geophysical Journal international, ggz239, https://doi.org/10.1093/gji/ggz239.

% M2Di is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.

% M2Di is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.

% You should have received a copy of the GNU General Public License
% along with M2Di.  If not, see <http://www.gnu.org/licenses/>.
% =========================================================================
clear
noisy    = 0;            % (1) print residual convergence, (0) no print
% Physics - scales
rhofg    = 1;            % fluid rho*g
k_muf0   = 1;            % reference permeability
etaC0    = 1;            % reference bulk viscosity
% Physics - non-dimensional parameters
eta2mus  = 10;           % bulk/shear viscosity ration
R        = 500;          % Compaction/decompaction strength ratio for bulk rheology 
nperm    = 3;            % Carman-Kozeny exponent
phi0     = 0.01;         % reference porosity
ra       = 2;            % radius of initial porosity perturbation 
lam0     = 1.0;          % standard deviation of initial porosity perturbation  
ttot     = 0.025;        % total time
% Physics - dependent scales
rhosg    = 2*rhofg;      % solid rho*g 
Lx       = 20;           % domain size x  
Ly       = ra*Lx;        % domain size y
phiA     = 2*phi0;       % amplitude of initial porosity perturbation       
lamPe    = 0.01;         % effective pressure transition zone
dt       = 1e-5;         % physical time-step
% Numerics
CN       = 0.5;          % Crank-Nicolson CN=0.5, Backward Euler CN=0.0
nx       = 64;           % number of cells x
ny       = ra*nx;        % number of cells y
epsi     = 1e-5;         % non-linear tolerance
iterMax  = 5e4;          % max nonlinear iterations
eta_b    = 1.0;          % numerical compressibility
Vdmp     = 5.0;          % velocity damping for momentum equations
Pfdmp    = 0.8;          % fluid pressure damping for momentum equations
Vsc      = 2;            % reduction of PT steps for velocity
Ptsc     = 2;            % reduction of PT steps for total pressure
Pfsc     = 8;            % reduction of PT steps for fluid pressure
teta_e   = 9e-1;         % relaxation factor for non-linear viscosity
teta_k   = 1e-1;         % relaxation factor for non-linear permeability
dt_red   = 1e-3;         % reduction of physical timestep
% Preprocessing
dx       = Lx/nx;        % grid step in x
dy       = Ly/ny;        % grid step in y
mus      = etaC0*phi0/eta2mus;                   % solid shear viscosity
lam      = lam0*sqrt(k_muf0*etaC0);              % initial perturbation width
rogBG    = rhofg*phi0 + rhosg*(1-phi0);          % Background density
dtauV    = min(dx,dy).^2./mus/(1+eta_b)/4.1/Vsc; % PT time step for velocity
xc       = dx/2:dx:Lx-dx/2;
yc       = dy/2:dy:Ly-dy/2;
[Xc,Yc]  = ndgrid(xc,yc);
% Arrays allocation
Pt       = zeros(nx  ,ny  );
Pf       = zeros(nx  ,ny  );
txy      = zeros(nx+1,ny+1);
Vx       = zeros(nx+1,ny  );
Vy       = zeros(nx  ,ny+1);
divV     = zeros(nx  ,ny  );
qDx      = zeros(nx+1,ny  );
qDy      = zeros(nx  ,ny+1);
RPf      = zeros(nx  ,ny  );
dVxdt    = zeros(nx-1,ny  );
dVydt    = zeros(nx  ,ny-1);
%% Initial conditions
radc     = ((Xc - Lx*.5)/lam/4).^2 + ((Yc - Ly*.25)/lam).^2;
phi      = phi0*ones(nx,ny); phi(radc<1) = phi(radc<1) + phiA;
etaC     = mus./phi*eta2mus;
k_muf    = k_muf0*(phi./phi0);
phi0bc   = mean(phi(:,end));
qDy(:,[1 end]) = (rhosg-rhofg)*(1-phi0bc)*k_muf0*(phi0bc./phi0).^nperm;
time = 0; it = 0;
%% Action
while time<ttot % -------------------------- Physical timesteps
    it     = it + 1;
    phi_o  = phi;
    divV_o = divV;
    for iter = 1:iterMax % -------------------------- Pseudo-Transient cycles
        % Total density
        rog     = rhofg*phi + rhosg*(1-phi) - rogBG;     
        rog_avy = 0.5*(rog(:,1:end-1)+rog(:,2:end));
        % Nonlinear quantities continuation
        etaC    =  etaC*(1-teta_e) + teta_e*( mus./phi*eta2mus.*(1+0.5*(1./R-1).*(1+tanh((Pf-Pt)./lamPe))) );
        k_muf   = k_muf*(1-teta_k) + teta_k*( k_muf0*(phi./phi0).^nperm );
        % Divergences
        divV    = diff( Vx,1,1)/dx + diff( Vy,1,2)/dy;
        divqD   = diff(qDx,1,1)/dx + diff(qDy,1,2)/dy;
        % Porosity update: CN=0.5 - Crank Nicolson, CN=0.0 - Backward Euler
        phi     = phi_o + (1-phi).*(CN*divV_o + (1-CN)*divV)*dt;
        % Total and fluid continuity residual
        RPt     =           - divV  - (Pt-Pf)./etaC./(1-phi);
        RPf     = RPf*Pfdmp - divqD + (Pt-Pf)./etaC./(1-phi);
        % PT time steps dtau
        dtauPt  = 4.1*mus*(1+eta_b)/max(nx,ny)/Ptsc;
        dtauPf  = min(dx,dy).^2./k_muf./4.1./Pfsc;
        % min out of the local neighbours
        dtauPf2 = dtauPf; dtauPf2(2:end-1,2:end-1) = min( min( min(dtauPf(1:end-2,2:end-1) , dtauPf(3:end  ,2:end-1)) , dtauPf(2:end-1,2:end-1) ) , min(dtauPf(2:end-1,1:end-2) , dtauPf(2:end-1,3:end  )) );
        % Total and fluid pressure updates (Mass balance)
        Pt      = Pt + RPt.*dtauPt;
        Pf      = Pf + RPf.*dtauPf2;
        % Deviatoric viscous rheology
        txx     = 2*mus*( diff(Vx,1,1)./dx - 1/3*divV  - eta_b*RPt);
        tyy     = 2*mus*( diff(Vy,1,2)./dy - 1/3*divV  - eta_b*RPt);
        txy(2:end-1,2:end-1) = mus*(diff(Vx(2:end-1,:),1,2)./dy + diff(Vy(:,2:end-1),1,1)./dx);
        % Momentum balance residual
        Rx      = diff(txx,1,1)/dx + diff(txy(2:end-1,:),1,2)/dy - diff(Pt,1,1)/dx          ;
        Ry      = diff(txy(:,2:end-1),1,1)/dx + diff(tyy,1,2)/dy - diff(Pt,1,2)/dy - rog_avy;
        resid   = max([ norm(Ry(:))/length(Ry(:)), norm(RPf(:))/length(RPf(:)) ]); if(max(resid)<epsi),break,end
        % Velocities and Darcy fluxes updates
        dVxdt   = (1-Vdmp/nx)*dVxdt + Rx;
        dVydt   = (1-Vdmp/ny)*dVydt + Ry;
        Vx(2:end-1,:)  = Vx(2:end-1,:) + dVxdt.*dtauV;
        Vy(:,2:end-1)  = Vy(:,2:end-1) + dVydt.*dtauV;
        qDx(2:end-1,:) = -0.5*(k_muf(1:end-1,:)+k_muf(2:end,:)).* diff(Pf,1,1)/dx;
        qDy(:,2:end-1) = -0.5*(k_muf(:,1:end-1)+k_muf(:,2:end)).*(diff(Pf,1,2)/dy + (rhofg-rogBG));
        if mod(iter,50)==1 && noisy==1,figure(2),if iter==1,clf;end; semilogy(iter,resid,'-o'),hold on,drawnow; end
    end%iter
    dt    = dt_red/(1e-10+max(abs(divV(:))));
    time  = time + dt; if time>ttot, break, end
    figure(1),clf,colormap(jet),set(gcf,'color','white')
    subplot(121),imagesc(xc,yc,(phi/phi0)'),axis xy,axis image;colorbar,title({['it=' num2str(it)];['time=' num2str(time)]}),xlabel('phi')
    subplot(122),imagesc(xc,yc,(Pt-Pf)'),axis xy,axis image;colorbar,title(['iter=' num2str(iter)]),xlabel('Pt-Pf')
    drawnow
end%while
